import matplotlib.pyplot as plt
import os
import numpy as np

atrib = float(-1)

def f(row,ls,last):
	if(max(row) != atrib):
		max_value = max(row)
		max_index = row.index(max_value)
		count = row.count(max_value)

		if(count == 1):#ranks non repeated values
			row[max_index]= atrib
			last = last+1
			ls[max_index] = last

		else:#ranks repeated values
			aux1 = last
			aux2 = 0
			for i in range(count):
				aux1 = aux1 +1
				aux2 = aux2 + aux1
				last = last + 1

			rank = aux2/float(count)
			for i in range(len(row)):
				if(row[i] == max_value):
					ls[i]=rank
					row[i] = atrib

		f(row,ls,last)
	return


def friedman_rank(aux):
	ncols = len(aux[0])
	nrows = len(aux)

	tab = []
	print '\nTable of ranks'
	for row in aux:#build table of ranks
		ls = np.zeros(ncols)
		last  = 0
		f(row,ls,last)
		print ls
		tab.append(ls)

	average_rank = np.zeros(ncols)
	for j in range(len(tab[0])):#computing average ranks
		for i in range(nrows):
			average_rank[j] = average_rank[j] + tab[i][j]
		average_rank[j] = average_rank[j]/float(nrows)

	print '\nAverage ranks'
	print '[',
	for var in average_rank:
		print str(var)+',',
	print ']\n'

	return average_rank, tab


