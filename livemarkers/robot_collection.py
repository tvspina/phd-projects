# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
from usis.npimage import *
from usis.features import *
from usis.morphology import *
from usis.segmentation import *
from usis.view import *
from usis.evaluation import *
from usis.adjacency import *
from usis.fuzzyopf import *
from usis.gradient import *

from math import *

from robot_gui import *
from simulation_data import *
from region_robots.region_robots import *
#from region_robots.superpixel_segmentation import *
from boundary_robots.boundary_robots import *
from boundary_robots.hybrid_methods_robots import *

from os.path import *

def is_image(f):
	ext = splitext(f)[1].lower()
	return ext in ['.jpg','.bmp','.png','.ppm','.pgm']

class RobotCollection(object):
	def __init__(self, methods, img, gt, simulation_data, exp_key,
					random_generator, frame_side = None,
					marker_img_name = None):
		self._robots = {}
		self.img = img
		self.gt = gt

		self.obj = None
		self.grad = None
		self.markers = None
		self.labeled_contour_comps = None

		intelligent = simulation_data[methods[0]][exp_key].simulation_config['intelligent_enhancement']
		wobj = simulation_data[methods[0]][exp_key].simulation_config['wobj']

		# Loading markers
		if marker_img_name is not None and exists(marker_img_name):
			self.markers = self.__load_markers__(marker_img_name, gt, frame_side)

		# Computing object membership map, if markers are given,
		# and the resulting linearly combined gradient/arc weight
		obj, grad = self.__compute_objmap_and_gradient__(self.img, self.markers,
														wobj, intelligent,
														random_generator)
		self.grad = grad
		self.obj = obj

		# Computing all of the groundtruth contours
		contours = image_contours(self.gt)
		self.labeled_contour_comps = label_bin_comp(self.gt, 1.5)

		# Setting a minimum working contour length
		# as threshold to avoid spurious contours.
		reasonable_contours = []
		for i in xrange(len(contours)):
			if len(contours[i]) >= 15:
				reasonable_contours.append(contours[i])
				contours[i] = None

		if len(reasonable_contours) <= 0:
			print 'No reasonable contours computed for image!!!'
			raise RuntimeError

		# Sorting all the contours in non-increasing order
		# of length.
		self.contours = reasonable_contours

		# Initializing robots for each method

		method = 'riverbed'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config

			riverbed = Riverbed(self.grad, self.obj, method_config['adj_radius'], method_config) # Using 8-neighbor graph
			riverbed.init_data(method_config)
			self._robots[method] =  ContourTrackingRobot(img, gt, self.grad, riverbed,
																method_config, self.contours,
																self.labeled_contour_comps,
																simulation_data[method][exp_key])
		method = 'lwof'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config
			lwof = LWOF(self.grad, self.obj, method_config['adj_radius'], method_config) # Using 8-neighbor graph
			lwof.init_data(method_config)
			self._robots[method] = ContourTrackingRobot(img, gt, self.grad, lwof,
														method_config, self.contours,
														self.labeled_contour_comps,
														simulation_data[method][exp_key])
		method = 'livemarkers'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config
			livemarkers = LiveMarkers(self.grad, self.obj, method_config['adj_radius'], method_config)
			livemarkers.init_data(method_config, grad = self.grad)
			self._robots[method] = LiveMarkersRobot(img, gt, self.grad, livemarkers,
																method_config, self.contours,
																self.labeled_contour_comps,
																simulation_data[method][exp_key])

		method = 'rivercut'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config
			rivercut = RiverCut(self.grad, self.obj, method_config['adj_radius'], method_config)
			rivercut.init_data(method_config, grad = self.grad, obj = self.obj)

			self._robots[method] = LiveMarkersRobot(img, gt, self.grad, rivercut,
															method_config, self.contours,
															self.labeled_contour_comps,
															simulation_data[method][exp_key])


		method = 'livecut'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config
			livecut = LiveCut(self.grad, self.obj, method_config['adj_radius'], method_config)
			livecut.init_data(method_config, grad = self.grad, obj = self.obj)
			self._robots[method] = LiveMarkersRobot(img, gt, self.grad, livecut,
													method_config, self.contours,
													self.labeled_contour_comps,
													simulation_data[method][exp_key])

		method = 'rivermarkers'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config
			rivermarkers = RiverMarkers(self.grad, self.obj, method_config['adj_radius'],
										method_config)
			rivermarkers.init_data(method_config, grad = self.grad)

			self._robots[method] = LiveMarkersRobot(img, gt, self.grad, rivermarkers,
															method_config, self.contours,
															self.labeled_contour_comps,
															simulation_data[method][exp_key])
		method = 'dift'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config

			if method_config['dift_cost_function'] == 'FSUM':
				dift_cost_function = FSUM
			else:
				dift_cost_function = FMAX

			dift = DIFT(self.grad, method_config['adj_radius'],
						method_config['dift_power'],
						method_config['dift_heap'],
						dift_cost_function)

			self._robots[method] = RegionRobot(img, gt, self.grad,
												dift, method_config,
												simulation_data[method][exp_key])


		method = 'mflow'
		if method in methods:
			method_config = simulation_data[method][exp_key].method_config

			mflow = MaxFlow(self.grad, self.obj, method_config['adj_radius'],
							method_config['gc_lambda'],
							method_config['gc_power'])

			self._robots[method] = RegionRobot(img, gt, self.grad,
												mflow, method_config,
												simulation_data[method][exp_key])


#		method = 'superpixel_ift'
#		if method in methods:
#			method_config = simulation_data[method][exp_key].method_config
#
#			superpixel = SuperpixelIFT(self.grad, img, method_config['adj_radius'],
#										method_config['volume_threshold'])
#
#			self._robots[method] = RegionRobot(img, gt, self.grad,
#												superpixel, method_config,
#												simulation_data[method][exp_key])



	def __getitem__(self, key):
		return self._robots[key]


	def __load_markers__(self, filename, gt, frame_side = None,
							default_mk_id = 1):
		markers = {}
		size_error_msg = 'Marker image with size different than the ground truth!'

		# Loading markers saved as images
		if is_image(filename):
			marker_img = CImage8()
			marker_img.from_file(filename)

			w,h = marker_img.size()

			if ((w,h) != gt.size() and (frame_side is not None and
				(w+2*frame_side,h+2*frame_side) != gt.size())):
					raise RuntimeError(size_error_msg)

			for y in xrange(h):
				for x in xrange(w):
					p = marker_img.toindex((x,y))
					if frame_side is not None:
						p1 = gt.toindex((x+frame_side, y + frame_side))
					else:
						p1 = gt.toindex((x,y))

					# If any image channel is greater than zero
					# then a marker was drawn on pixel i
					if any(marker_img[p]):
						lb = 1 if gt[p1] > 0 else 0

						markers[p1] = (default_mk_id,lb)

		# Loading markers saved in text file
		elif splitext(filename)[1].lower() == '.txt':
			f = open(filename,'r')
			lines = f.readlines()
			nseeds,w,h = map(int,lines[0].split())

			if ((w,h) != gt.size() and (frame_side is not None and
				(w+2*frame_side,h+2*frame_side) != gt.size())):
					raise RuntimeError(size_error_msg)

			for l in lines[1:]:
				x,y,mk,lb = map(int, l.split())

				# We only add frame_side if the
				# marker image does not consider it
				# already.
				if (frame_side is not None
					and (w,h) != gt.size()):

					x = x + frame_side
					y = y + frame_side

				if gt.valid_pixel((x,y)):
					p = gt.toindex((x,y))
					markers[p] = (mk+default_mk_id,lb)

		return markers

	def __compute_objmap_and_gradient__(self, cimg, markers,
											wobj, intelligent_enhancement,
											random_generator):
		feats = Features()
		nsamples = 500

		obj_map = None
		grad = None

		# Feature extraction
		if not cimg.is_grayscale():
			feats.ycbcr_feats(cimg, (1.0, 1.0, 1.0))
			# Decreasing weight of Y feature
			feat_weight = (0.2,1.0,1.0)
		else:
			feats.ms_gauss_image8_feats(cimg.to_image8(), 3)
			feat_weight = (1.0,1.0,1.0)

		# It is important to normalize the features since
		# OPF considers LogEuclDist
		feats.normalize()

		Gfeat, Fmax = weighted_feature_gradient(feats, 1.5, feat_weight)
		Gobj = None
		Omax = 0.0

		# Supervised fuzzy classification
		if markers is not None:
			classifier = FOPFClassifier()

			if not intelligent_enhancement:
				# Using all markers to train the classifier
				success = classifier.from_markers(markers, feats, nsamples, random_generator,
													feat_weight = feat_weight)
			else:
				# Intelligently separating the best markers for enhancement
				# (i.e., foreground and background markers with the most
				# distinctive appearance)
				success = classifier.from_intelligent_markers(markers, feats, nsamples, random_generator,
																feat_weight = feat_weight)
				if not success:
					print 'Intelligent enhancement decided not to compute object map.'


			if success:
				obj_map = classifier.fuzzy_classify(feats)

				Gobj, Omax = vimage_gradient(obj_map, 1.5)

		# Linearly combining the image and object membership map gradients
		grad = feat_obj_weight_image(Gfeat, Gobj, wobj, Fmax,
										Omax)


		return obj_map, grad
