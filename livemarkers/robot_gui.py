# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
from usis.npimage import *
from usis.view import *
from usis.segmentation import *
from usis.evaluation import *
from usis.morphology import *
from usis.adjacency import *
from usis.draw import *
from usis.radiometric import *

import pylab as pl
import matplotlib as mpl
import matplotlib.pyplot as plt
import random

import numpy as np
import scipy as sp
from os.path import *

# TrueType font for generating vectorial graphs that
# do not use Type 3 fonts (matplotlib's default)
# Tip from: http://www.phyletica.com/?p=308
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

def inverse_color(a):
	return (255-a[0],255-a[1],255-a[2])

def triplet_to_mp_color(color):
	rgb = triplet2rgb(color)

	return (rgb[0]/255., rgb[1]/255., rgb[2]/255.)

def display_seeds(img, seeds, colormap):
	if seeds is not None:
		for i in seeds:
			lb = seeds[i][1]
			c = triplet2rgb(colormap[lb])

			if type(img) == CImage8:
				img[i] = c
			else:
				xp, yp = (i % img.shape[1], i/ img.shpae[0])

				img[yp,xp,0] = c[0]
				img[yp,xp,1] = c[1]
				img[yp,xp,2] = c[2]

class ContourTrackingGUI:
	def __init__(self, robot, img_name, interactive):
		# 1 -> original image, 2 -> gradient, 3 -> object membership map
		self.colormap = initialize_colormap(True)
		self.path_color = triplet_to_mp_color(self.colormap[1])

		self.set_robot(robot, img_name)

		self.copy_disp_img()
		self.create_figures()



		# If set to False, then the marker's size and
		# label will be determined from ground truth.
		# Also, marker addition by dragging is forbidden.
		self.interactive_experiment = interactive

	def __dealloc__(self):
		pl.close()

	def set_robot(self, robot, img_name):
		self.robot = robot
		self.disp_img_code = 1
		self.dragging = False

		self.all_seeds = {}
		self.del_seeds = {}
		self.del_anchors = set()
		self.img_name = img_name

		self.__issue_hybrid_delineation__ = False
		self.__invert_img_pixel_color__ = False
		self.__display_anchor_points__ = True

		self.iteration = 0
		self.nbkg_seeds = 0
		self.nobj_seeds = 0

	####################
	### Mouse events ###
	####################

	def on_click(self, event):
		# Verifying if a tool from the toolbar is trying
		# to access the mouse. If it is, then we hand over control
		if self.ax_fig.canvas.manager.toolbar._active is not None:
			return

		method_type = self.robot.method.method_type
		is_hybrid = method_type == 'hybrid'

		if event.inaxes!=self.ax.axes:
			return

		x = int(event.xdata)
		y = int(event.ydata)

		## Deleting seeds if pressing 'control'
		if(event.key == 'control' and event.button == 1
			and method_type != 'boundary'):
			if is_hybrid:
				self.robot.end_segment()
				# If the current method is hybrid then
				# the underlying region-based method should be issued
				# instantaneously on button release to update the result
				self.__issue_hybrid_delineation__ = True

			radius = self.robot.method_config['lm_mk_radius']
			self.delete_seeds((x,y), radius)
			self.display_overlayed_data([], self.all_seeds)
		## Adding anchor point for hybrid and boundary-based methods
		elif(method_type == 'hybrid' and event.key != 'shift'
			or method_type == 'boundary'):
			if event.button == 1:
				self.add_anchor((x,y), self.robot.simulation_config['contour_ori'])
			elif event.button == 2:
				if event.key == 'control' or method_type == 'boundary':
					self.deconfirm_path((x,y))
				elif method_type == 'hybrid':
					self.robot.end_segment()
				self.show_motion([])
			else:
				print 'Closing the contour'
				self.close_contour((x,y))
		## Adding brush stroke for region-based methods
		else:
			if self.interactive_experiment:
				self.dragging = True
				self.add_region_marker((x,y),event)
				# If the current method is hybrid then
				# the underlying region-based method should be issued
				# instantaneously on button release to update the result
				if method_type == 'hybrid':
					self.__issue_hybrid_delineation__ = True
			else:
				self.add_region_marker_from_gt((x,y),event)

	def on_release(self, event):
		# Verifying if a tool from the toolbar is trying
		# to access the mouse. If it is, then we hand over control
		if self.ax_fig.canvas.manager.toolbar._active is not None:
			return

		method_type = self.robot.method.method_type
		## Delineating with newly added brush strokes
		## and/or deleting seeds for hybrid approaches
		if method_type == 'hybrid' and self.__issue_hybrid_delineation__:
			# __run_hybrid_delineation__ already considers self.all_seeds
			# and self.del_seeds for delieation, so we pass an empty
			# dictionary of path seeds for it
			self.__run_hybrid_delineation__({})
			self.__issue_hybrid_delineation__ = False
			self.display_overlayed_data([], self.all_seeds)
			self.dragging = False

			self.iteration = self.iteration + 1

			if self.robot.simulation_config['save_fig']:
				self.savefig(self.iteration, prefix = '%s_%s_delineation' %
							(splitext(basename(self.img_name))[0],
							self.robot.method))

		## Running the region-based method upon button release
		elif method_type == 'region':
			nadded_seeds = self.nbkg_seeds + self.nobj_seeds
			# The region-based method only runs when the number of allowed
			# seeds per iteration has been added (e.g., 2: one fg and one bg)
			# or if the mouse's middle button is pressed
			run_delineation = nadded_seeds == self.robot.simulation_config['seeds_per_iteration']

			if event.button == 1 or event.button == 3 or run_delineation:
				self.nbkg_seeds = 0
				self.nobj_seeds = 0
				accuracy_measure = self.robot.simulation_config['accuracy_measure']

				self.robot.method.run(self.all_seeds, self.del_seeds)
				self.display_overlayed_data([], self.all_seeds)

				if len(self.all_seeds) > 0:
					# Computing the accuracy for the current segmentation.
					accuracy = self.robot.simulation_data.add_accuracy(self.img_name,
																		self.robot.method.label,
																		self.robot.gt,
																		self.iteration,
																		accuracy_measure)

					self.robot.simulation_data.add_label(self.img_name, self.robot.method.label,
														self.iteration)
					self.robot.simulation_data.add_control_measures(self.img_name)
					self.robot.simulation_data.add_interaction_amount(self.img_name, 1)

					self.iteration += 1

					if self.robot.simulation_config['save_fig']:
						self.savefig(self.iteration, prefix = '%s_%s_delineation' %
									(splitext(basename(self.img_name))[0],
									self.robot.method))


			self.dragging = False

	def on_motion(self, event):
		# Verifying if a tool from the toolbar is trying
		# to access the mouse. If it is, then we hand over control
		if self.ax_fig.canvas.manager.toolbar._active is not None:
			return

		method_type = self.robot.method.method_type
		is_hybrid = method_type == 'hybrid'
		if event.xdata is None:
			return

		x = int(event.xdata)
		y = int(event.ydata)

		if not self.dragging:
			if(event.key == 'control' and event.button == 1
				and method_type != 'boundary'):
				if is_hybrid:
					self.robot.end_segment()
				radius = self.robot.method_config['lm_mk_radius']
				self.delete_seeds((x,y), radius)
				self.display_overlayed_data([], self.all_seeds)
			elif method_type != 'region':
				self.compute_path((x,y))
		elif self.interactive_experiment:
			self.add_region_marker((x,y),event)

	###########################
	### Key-pressing events ###
	###########################

	def on_key_press(self, event):
		# Verifying if a tool from the toolbar is trying
		# to access the mouse. If it is, then we hand over control
		if self.ax_fig.canvas.manager.toolbar._active is not None:
			return

		if self.interactive_experiment:
			lm_mk_radius = self.robot.method_config['lm_mk_radius']
			lm_max_mk_radius = self.robot.method_config['lm_max_mk_radius']

			# Changing the brush stroke's radius
			if event.key == 'up':
				if lm_mk_radius < lm_max_mk_radius:
					if lm_mk_radius + 1 > lm_max_mk_radius:
						lm_mk_radius = lm_max_mk_radius
					else:
						lm_mk_radius += 1
					self.robot.method_config['lm_mk_radius'] = lm_mk_radius
			elif event.key == 'down':
				if lm_mk_radius > 1.5:
					if lm_mk_radius - 1 < 1.5:
						lm_mk_radius = 1.5
					else:
						lm_mk_radius -= 1

					self.robot.method_config['lm_mk_radius'] = lm_mk_radius
			# Changing the foreground color
			elif event.key == 'f':
				c = self.colormap[1]
				self.colormap[1] = triplet(255-t0(c), 255 - t1(c), 255 - t2(c))
				self.path_color = triplet_to_mp_color(self.colormap[1])
				self.display_overlayed_data([], self.all_seeds)
				self.show_motion([])
			# Changing orientation
			elif event.key == '>'or event.key == '<':
				self.change_orientation('cw' if event.key == '>' else 'ccw')
			# Toggles the color used to display the path.
			# If True, self.__invert_img_pixel_color__ will force the
			# path pixels to display the inverted color of the underlying
			# pixels (like in GIMP). Otherwise, the entire path will be
			# drawn with the foreground's color
			elif event.key == 'i':
				self.__invert_img_pixel_color__ = not self.__invert_img_pixel_color__
			# Toggles the display of anchor points
			elif event.key == 'a':
				self.__display_anchor_points__ = not self.__display_anchor_points__
			elif event.key == 'enter':
				fast_smooth_ift(self.robot.method.label, self.robot.method.grad, 10)
				self.display_overlayed_data([], self.all_seeds)
			elif event.key == 'w':
				# Saving the raw label when finished
				print 'Saving label'
				fname = 'robot_%s_%s_label.pgm' % (splitext(basename(self.img_name))[0], self.robot.method)
				self.robot.method.label.write(fname)
				h,_ = self.robot.method.label.arr.shape
				self.ax.text(0, h+20, 'Raw label image saved to: %s' % fname)
				self.ax_fig.canvas.draw()
			# Changing the display image
			elif event.key.isdigit():
				code = int(event.key)

				if code >= 1 and code <= 8:
					self.disp_img_code = code
					self.show_motion([])
	######################################
	### Methods called by mouse events ###
	######################################

	## Methods for controlling region-based techniques ##
	def add_region_marker(self, xy, event):
		label = None
		mk_id = self.robot.method_config['mk_id']
		lm_mk_radius = self.robot.method_config['lm_mk_radius']

		# Left click
		if event.button == 1:
			label = self.robot.method_config['primary_label']
		# Right click
		elif event.button == 3:
			label = self.robot.method_config['right_pixel_label']

		if label is not None:
			self.all_seeds.update(self.add_seeds(xy, lm_mk_radius, mk_id, label))

			self.display_overlayed_data([], self.all_seeds)

## UNCOMMENT: faster drawing, but flickers the figure's text
#			if lm_mk_radius > 0:
#				# Displaying region marker
#				c = triplet_to_mp_color(self.colormap[label])
#				circle = plt.Circle(xy,lm_mk_radius,color=c)
#				self.ax_fig.gca().add_artist(circle)
#				self.ax_fig.canvas.draw()

	def add_region_marker_from_gt(self, xy, event):
		mk_id = self.robot.method_config['mk_id']
		lm_mk_radius = self.robot.method_config['lm_mk_radius']
		min_marker = self.robot.simulation_config['min_marker']
		safe_distance = self.robot.simulation_config['safe_distance']

		primary_label = self.robot.method_config['primary_label']
		secondary_label = self.robot.method_config['right_pixel_label']

		label = self.robot.method.label
		gt_image = self.robot.gt
		if event.button == 1:
			cur_seed = label.toindex(xy)
			nmarkers, lm_mk_radius, lb = markers_from_gt(self.all_seeds, cur_seed, gt_image, label,
													safe_distance, lm_mk_radius, min_marker,
													mk_id, primary_label,
													secondary_label)
			if lm_mk_radius > 0:
				if lb == secondary_label:
					self.nbkg_seeds += 1
				else:
					self.nobj_seeds += 1

				# Displaying region marker
				c = triplet_to_mp_color(self.colormap[lb])
				circle = plt.Circle(xy,lm_mk_radius,color=c)
				self.ax_fig.gca().add_artist(circle)
				self.ax_fig.canvas.draw()

	## Methods for controlling boundary-tracking techniques ##
	def compute_path(self, xy):
			contour_ori = self.robot.simulation_config['contour_ori']
			try:
				path = self.robot.motion(xy, contour_ori)
			except:
				pass
			else:
				if path is not None:
					spike_detected, _ = self.robot.detect_error_spike(path, contour_ori)

					invert_img_pixel_color = self.__invert_img_pixel_color__
					self.show_motion(path, invert_img_pixel_color = invert_img_pixel_color)

					nerrors = len(self.robot.past_errors)
					if nerrors > 0 and spike_detected:
						circle2 = plt.Circle((nerrors-1,
									  self.robot.past_errors[-1]),20,color='g')

					if self.robot.method.prev_anchor < 0:
						self.ax2.plot(self.robot.contour_cost)

	def add_anchor(self, xy, contour_ori):
		is_hybrid = self.robot.method.method_type == 'hybrid'

		primary_label = self.robot.method_config['primary_label']

		prev_anchor = self.robot.method.prev_anchor

		new_anchor = self.robot.grad.toindex(xy)
		prev_contour_src_index = -1
		if prev_anchor >= 0:
			prev_contour_src_index = self.robot.contour_src_index

		# Using the marker id as the anchor pixel
		# to facilitate seed removal
		if is_hybrid:
			old_mk_id = self.robot.method_config['mk_id']
			self.robot.method_config['mk_id'] = new_anchor

		try:
			success, path, path_seeds = self.robot.add_anchor(xy, contour_ori)
		except:
			return False, None, None
		else:
			coherent_path = None
			if path is not None:
				last_strip, dst_strip = self.robot.__find_last_coherent_strip__(path, new_anchor,
																				prev_anchor,
																				contour_ori)
				coherent_path = int(last_strip == dst_strip)

			if not success:
				if is_hybrid:
					self.robot.method_config['mk_id'] = old_mk_id
			else:
				anchor = self.robot.anchor_list[-1]

				err = 0.0
				if prev_contour_src_index >= 0:
					err = self.robot.mean_square_error(path, contour_ori, prev_contour_src_index)
				if is_hybrid and path_seeds is not None:
					self.delineate(anchor, path, path_seeds, path_error = err,
									coherent_path = coherent_path)
				else:
					self.robot.set_path_as_label(path, primary_label)

					# Storing anchor, path, and label for this iteration
					self.robot.simulation_data.add_anchor(self.img_name, anchor,
															path, path_error = err,
															coherent_path = coherent_path)

				# We only count the number of border segment added as forms
				# of interaction for hybrid methods. Hence, we only increase the amount of
				# interaction if a previous anchor was selected before this one.
				# For boundary methods, we count the anchor anyway since at least
				# two anchors are necessary to close the contour and the user must
				# perform the action of determining whether the path from the last anchor
				# actually reaches the first one.
				if prev_anchor >= 0 or not is_hybrid:
					self.robot.simulation_data.add_interaction_amount(self.img_name, 1)


				# Displaying data
				self.display_overlayed_data(path, self.all_seeds)

				if(is_hybrid and path_seeds is not None
					and self.robot.simulation_config['save_fig']):
					self.savefig(self.iteration, prefix = '%s_%s_delineation' %
									(splitext(basename(self.img_name))[0],
									self.robot.method))

			return success, path, path_seeds

	def delineate(self, anchor, path, path_seeds, path_error, coherent_path):
		accuracy_measure = self.robot.simulation_config['accuracy_measure']

		if anchor >= 0:
			self.iteration = self.iteration + 1

			self.__run_hybrid_delineation__(path_seeds)

			# Computing the accuracy for the current segmentation.
			accuracy = self.robot.simulation_data.add_accuracy(self.img_name,
																self.robot.method.label,
																self.robot.gt,
																self.iteration,
																accuracy_measure)

			# Storing anchor, path seeds since the method is hybrid,
			# and label for this iteration
			self.robot.simulation_data.add_anchor(self.img_name, anchor,
													path, path_seeds,
													path_error = path_error,
													coherent_path = coherent_path)

			self.robot.simulation_data.add_label(self.img_name, self.robot.method.label,
												self.iteration)
			self.robot.simulation_data.add_control_measures(self.img_name)

	def __run_hybrid_delineation__(self, path_seeds):
		# Delineating using all contour seeds
		# to ensure proper usage of delineation
		# methods that do not perform incremental recomputation.
		self.all_seeds.update(path_seeds)
		self.robot.method.delineate(self.all_seeds, self.del_seeds)
		self.del_seeds = {}

	def deconfirm_path(self, xy):
		method_type = self.robot.method.method_type

		mk_id = self.robot.method_config['mk_id']
		secondary_label = self.robot.method_config['right_pixel_label']
		contour_ori = self.robot.simulation_config['contour_ori']

		old_anchor = self.robot.method.prev_anchor
		yet_old_anchor = self.robot.method.yet_prev_anchor

		# Resetting label for the removed segment
		old_path = self.robot.method.path(old_anchor, yet_old_anchor)

		success, path = self.robot.deconfirm_path(xy, contour_ori)
		if success:
			# Resetting path label with background value
			self.robot.set_path_as_label(old_path, secondary_label)
			self.robot.simulation_data.del_anchor(self.img_name, old_anchor)

			self.robot.simulation_data.add_interaction_amount(self.img_name, -1)

			if method_type != 'boundary':
				# The iteration corresponds to the number of
				# added segments minus 1
				self.iteration = max(self.iteration-1,0)

				# Resetting the marker id if all anchors have been removed
				if len(self.robot.anchor_list) > 0:
					self.robot.method_config['mk_id'] = self.robot.anchor_list[-1]
				else:
					self.robot.method_config['mk_id'] = 0

				# Selecting seeds for deletion
				for p in self.all_seeds:
					if self.all_seeds[p][0] == mk_id:
						self.del_seeds[p] = self.all_seeds[p]

				# Removing seeds marked for deletion from the set
				# of all seeds
				for p in self.del_seeds:
					if p in self.all_seeds:
						del self.all_seeds[p]

				self.robot.simulation_data.del_label(self.img_name, self.iteration)
				self.robot.simulation_data.add_control_measures(self.img_name)

				# Resetting the entire label since it will be recomputed
				self.robot.method.label.arr[:] = 0
				self.robot.method.delineate(self.all_seeds, self.del_seeds)
				self.del_seeds = {}

		self.display_overlayed_data(path, self.all_seeds)

	def close_contour(self, xy):
		is_hybrid = self.robot.method.method_type == 'hybrid'

		contour_ori = self.robot.simulation_config['contour_ori']
		primary_label = self.robot.method_config['primary_label']

		accuracy_measure = self.robot.simulation_config['accuracy_measure']

		old_first_anchor = self.robot.anchor_list[0]

		try:
			self.add_anchor(xy, contour_ori)
			prev_anchor = self.robot.method.prev_anchor

			first, last = self.robot.close_contour(None)
		except DelineationError as e:
			print 'Unable to close the contour due to ' + str(e)
		else:

			if first >= 0 and last >= 0:
				# Removing the old first anchor since it was replaced
				# by anchor 'first'
				if old_first_anchor not in self.robot.anchor_list:
					self.robot.simulation_data.del_anchor(self.img_name, old_first_anchor)

				# Last iteration
				self.iteration = self.iteration + 1

				path = self.robot.method.path(last, -1)
				last_segment = self.robot.method.path(last, prev_anchor)

				coherent_path = None
				err = 0.0

				if last_segment is not None:
					_, err = self.robot.detect_error_spike(last_segment, contour_ori)

					if path is not None:
						last_strip, dst_strip = self.robot.__find_last_coherent_strip__(last_segment, last,
																						prev_anchor,
																						contour_ori)
						coherent_path = int(last_strip == dst_strip)

				self.all_seeds = {}
				if not is_hybrid:
					# Closing label
					self.robot.set_path_as_label(path, primary_label)
					closed = close_holes(self.robot.method.label)
					label_union(self.robot.method.label, closed, -1)
				else:

					left_pixel_label = self.robot.method_config['left_pixel_label']
					right_pixel_label = self.robot.method_config['right_pixel_label']
					mk_id = self.robot.method_config['mk_id']
					lm_mk_radius = self.robot.method_config['lm_mk_radius']

					self.all_seeds = self.robot.method.path_seeds([(-1, lm_mk_radius),
																		(last, lm_mk_radius)],
																		primary_label,
																		left_pixel_label,
																		right_pixel_label,
																		mk_id)
					self.robot.method.delineate(self.all_seeds, self.del_seeds)
					self.del_seeds = {}

				# Computing the accuracy for the current segmentation.
				self.robot.simulation_data.add_accuracy(self.img_name,
														self.robot.method.label,
														self.robot.gt,
														self.iteration,
														accuracy_measure)



				self.robot.simulation_data.add_anchor(self.img_name, first, None, in_front = True)
				self.robot.simulation_data.add_anchor(self.img_name, last, last_segment,
														path_error = err,
														coherent_path = coherent_path)


				self.robot.simulation_data.add_label(self.img_name,
													self.robot.method.label,
													self.iteration)
				self.robot.simulation_data.add_control_measures(self.img_name)

				print 'Accuracy: ', self.robot.simulation_data.accuracy[self.img_name][self.iteration]
				print 'Interaction amount: ', self.robot.simulation_data.interactions[self.img_name]

				# Displaying data
				self.display_overlayed_data(path, self.all_seeds)


	def add_seeds(self, xy, radius, mk_id, label):
		A = AdjRel(radius = radius)

		px_q = np.array(xy, dtype = int)

		seeds = {}
		for i in A:
			px_adj = px_q + i

			adj = self.robot.grad.toindex(px_adj)

			if self.robot.grad.valid_pixel(px_adj):
				seeds[adj] = (mk_id, label)

		return seeds

	def delete_seeds(self, xy, radius):
		A = AdjRel(radius = radius)

		px_q = np.array(xy, dtype = int)

		for i in A:
			px_adj = px_q + i

			adj = self.robot.grad.toindex(px_adj)

			if self.robot.method.method_type == 'region':
				if adj in self.all_seeds:
					self.del_seeds[adj] = self.all_seeds[adj]
					del self.all_seeds[adj]

			elif self.robot.method.method_type == 'hybrid':
				if adj in self.all_seeds:
					if adj in self.robot.anchor_list:
						self.del_anchors.add(adj)

					self.del_seeds[adj] = self.all_seeds[adj]

					del self.all_seeds[adj]


	def change_orientation(self, contour_ori):
		method_type = self.robot.method.method_type

		if method_type == 'boundary' or  method_type == 'hybrid':
			prev_ori = self.robot.method_config['lwof_ori']
			left_label = self.robot.method_config['left_pixel_label']
			right_label = self.robot.method_config['right_pixel_label']


			# prev_ori == 0 indicates that the boundary-tracking method
			# is not using an oriented path-cost function
			if prev_ori != 0:
				prev_ori = 2 if contour_ori == 'ccw' else 1

			self.robot.method_config['right_pixel_label'] = left_label
			self.robot.method_config['left_pixel_label'] = right_label

			self.robot.method_config['lwof_ori'] = prev_ori
			self.robot.simulation_config['contour_ori'] = contour_ori

			self.robot.method.reset_current_path()

	###########################
	### GUI-related methods ###
	###########################

	def create_figures(self):
		fig = plt.figure()
		ax = fig.add_subplot(111)

		self.ax = ax
		self.ax_fig = fig

		fig2 = plt.figure()
		ax2 = fig2.add_subplot(111)

		self.ax2 = ax2
		self.ax2_fig = fig2

		self.ax.get_xaxis().set_visible(False)
		self.ax.get_yaxis().set_visible(False)

		self.ax2.get_xaxis().set_visible(False)
		self.ax2.get_yaxis().set_visible(False)

		self.cid = fig.canvas.mpl_connect('button_press_event', self.on_click)
		self.cidrelease = fig.canvas.mpl_connect('button_release_event', self.on_release)
		self.cidmotion = fig.canvas.mpl_connect('motion_notify_event', self.on_motion)
		self.cidkey = fig.canvas.mpl_connect('key_press_event', self.on_key_press)

	def display_default_images(self, maximize_window):
		# Recreating figures every time this method is called
		# to ensure that they be reopened.
		self.close()
		self.create_figures()

		self.display_overlayed_data([], {})

		if maximize_window:
			mng = self.ax_fig.canvas.manager
			mng.resize(*mng.window.maxsize())

		pl.show()

	def close(self):
		self.ax_fig.canvas.mpl_disconnect(self.cid)
		self.ax_fig.canvas.mpl_disconnect(self.cidmotion)
		self.ax_fig.canvas.mpl_disconnect(self.cidkey)

		pl.close(self.ax_fig)
		pl.close(self.ax2_fig)

	def copy_disp_img(self):
		if self.disp_img_code == 2:
			self.disp_img = self.robot.orig_img.copy()
			self.display_image(self.disp_img, self.robot.method.label, 0, 1.5, True)
			setattr(self, 'disp_img_msg', 'Segmentation result')
		elif self.disp_img_code == 3:
			self.disp_img = self.robot.orig_img.copy()
			highlight_bkg(self.disp_img, self.robot.method.label, 1.0, self.colormap, False)
			setattr(self, 'disp_img_msg', 'Segmentation result')
		elif self.disp_img_code == 4:
			self.disp_img = CImage8()
			self.disp_img.from_image8(linear_stretch8(self.robot.method.label,
										0, np.max(self.robot.method.label.arr),
										0, 255))
			setattr(self, 'disp_img_msg', 'Segmentation mask')
		elif self.disp_img_code == 8:
			self.disp_img = self.robot.orig_img.copy()
			highlight_bkg(self.disp_img, self.robot.method.label, 1.0, self.colormap, True)
			setattr(self, 'disp_img_msg', 'Segmentation result')
		elif self.disp_img_code == 5:
			self.disp_img = CImage8()
			self.disp_img.from_image8(self.robot.grad)
			setattr(self, 'disp_img_msg', 'Combined gradient')
		elif (self.disp_img_code == 6 and hasattr(self.robot.method, 'obj')
				and self.robot.method.obj is not None):
			self.disp_img = CImage8()
			self.disp_img.from_image8(self.robot.method.obj)
			setattr(self, 'disp_img_msg', 'Object membership map')
		elif self.disp_img_code == 7 and hasattr(self,'markers'):
			self.disp_img = self.robot.orig_img.copy()
			display_seeds(self.disp_img, self.markers, self.colormap)
			setattr(self, 'disp_img_msg', 'Arc-weight estimation markers')
		else:
			self.disp_img = self.robot.img.copy()
			setattr(self, 'disp_img_msg', 'Main canvas')

	def show_motion(self, path, c = None, anchors = None, path_thickness = 0.0,
					invert_img_pixel_color = False):
		self.copy_disp_img()

		if c is None:
			c = self.path_color

		self.display_path(self.disp_img, path, c, path_thickness,
							invert_img_pixel_color)
		self.ax.clear()

		if (self.robot.simulation_config['show_images']
			or self.robot.simulation_config['interactive']):

			if self.robot.method.method_type != 'region':
				if self.__display_anchor_points__:
					if anchors is not None:
						self.display_anchors(self.disp_img, anchors)
					else:
						self.display_anchors(self.disp_img, self.robot.anchor_list)

			self.ax.imshow(self.disp_img.to_array(), cmap=plt.get_cmap('gray'))

			if self.robot.simulation_config['maximize_window']:
				mng = self.ax_fig.canvas.manager
				mng.resize(*mng.window.maxsize())

			if self.robot.method.method_type != 'region':
				if not self.robot.simulation_config['interactive']:
					self.ax2.clear()
#					self.ax2.autoscale_view(True, True, True)
					self.ax2.semilogy(self.robot.past_errors,lw=2)

					err_thresh = self.robot.simulation_config['err_thresh']

					max_y = (err_thresh**2)*1.1
					if len(self.robot.past_errors) > 0:
						max_error = max(self.robot.past_errors)
						min_error = min(self.robot.past_errors)
						max_y = max(max_y, max(self.robot.past_errors))

						self.ax2.axhline(err_thresh**2,color='g',lw=2,ls='--')

					self.ax2.set_ylim(ymin = 0, ymax=max_y)
					self.ax2_fig.canvas.draw()

			self.ax.set_title('%s: %s (%s)' % (self.robot.method.method_type.capitalize(),
											self.robot.method.nickname,
											self.disp_img_msg))

			# Closing graph figure
			if self.robot.simulation_config['interactive']:
				pl.close(self.ax2_fig)

			self.ax_fig.canvas.draw()
#			self.ax.autoscale_view(True, True, True)

	def display_anchors(self, img, anchor_list):
		if anchor_list is not None and self.disp_img_code != 2:
			n = len	(anchor_list)
			for i,anchor in zip(xrange(n),anchor_list):
				## Only displaying anchor points whose
				## corresponding seed pixel has not
				## been removed. Note that it will still
				## existing in the robot's internal code
				if anchor not in self.del_anchors:
					color = (0,180,0)
					draw_cimage_circle(img, anchor, 3.0, triplet(*color))

	def display_path(self, img, path, c, path_thickness,
						invert_img_pixel_color):
		if path is not None:
			A = AdjRel(radius = path_thickness)
			if type(img) == CImage8:
				for i in path:
					p = self.robot.grad.topixel(i)
					for j in A:
						u = p + j

						# Showing the path with the inverted color of the
						# underlying pixels in interactive mode
						if invert_img_pixel_color:
							c = img[self.robot.grad.toindex(u)]
							img[self.robot.grad.toindex(u)] = (255-c[0],255-c[1],255-c[2])
						else:
							img[self.robot.grad.toindex(u)] = (255*c[0],255*c[1],255*c[2])
			else:
				for i in path:
					p = self.robot.grad.topixel(i)
					for j in A:
						u = p + j
						xp,yp = u

						# Showing the path with the inverted color of the
						# underlying pixels in interactive mode
						if invert_img_pixel_color:
							c = img[yp,xp]/255.0
							c = (1.0-c[0],1.0-c[1],1.0-c[2])

						img[yp,xp,0] = 255*c[0]
						img[yp,xp,1] = 255*c[1]
						img[yp,xp,2] = 255*c[2]

#	def display_path(self, img, path, c, path_thickness):
#		if path is not None:
#			A = AdjRel(radius = path_thickness)
#			if type(img) == CImage8:
#				for i in path:
#					p = self.robot.grad.topixel(i)
#					for j in A:
#						u = p + j
#
#						img[self.robot.grad.toindex(u)] = (255*c[0],255*c[1],255*c[2])
#			else:
#				for i in path:
#					p = self.robot.grad.topixel(i)
#					for j in A:
#						u = p + j
#						xp,yp = u
#
#						img[yp,xp,0] = 255*c[0]
#						img[yp,xp,1] = 255*c[1]
#						img[yp,xp,2] = 255*c[2]



	def display_overlayed_data(self, path, seeds, label = None,
									anchors = None):
		primary_label = self.robot.method_config['primary_label']

		# Displaying data
		self.robot.img = self.robot.orig_img.copy()
		# Ovelaying GT over the label and seeds
		gt_color = inverse_color(triplet2rgb(self.colormap[primary_label]))

		display_label = self.robot.method.label

		if 	label is not None:
			display_label = label

		if self.robot.simulation_config['overlay_gt']:
			self.display_image(self.robot.img, self.robot.gt, 0, 1.5,
								False, color = gt_color)

		self.display_image(self.robot.img, display_label, 0, 1.5, True)

		display_seeds(self.robot.img, seeds, self.colormap)

		self.show_motion(path, anchors = anchors)

	def display_image(self, cimg, label, highlight_opt,
							radius, fill, color = None, regions = None):
		if color is None:
			if regions is not None:
				highlight_lb_regions(cimg, label, regions, radius,
									self.colormap, fill)
			else:
				highlight_label(cimg, label, self.colormap,
								highlight_opt, radius, fill)
		else:
			highlight_gt(cimg, label, radius, color, fill)

	def savefig(self, frame_id, format = 'png', prefix = ''):
		if prefix != '':
			name = 'robot_%s_%05d.%s' % (prefix, frame_id, format)
		else:
			name = 'robot_%05d.%s' % (frame_id, format)

		self.ax.set_title('')
		self.ax.get_xaxis().set_ticks([])
		self.ax.get_yaxis().set_ticks([])
		self.disp_img.write(name)

	def savegraph(self, frame_id, format = 'png', prefix = ''):
		if prefix != '':
			name = 'robot_%s_%05d_graph.%s' % (prefix, frame_id, format)
		else:
			name = 'robot_%05d_graph.%s' % (frame_id, format)

		if not self.robot.simulation_config['interactive']:
			self.ax2_fig.savefig(name)
