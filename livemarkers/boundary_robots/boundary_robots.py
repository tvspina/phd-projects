# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
from usis.npimage import *
from usis.features import *
from usis.morphology import *
from usis.segmentation import *
from usis.view import *
from usis.evaluation import *
from usis.adjacency import *
from usis.fuzzyopf import *
from usis.gradient import *

import numpy as np
import scipy as sp

import os,sys, time, copy, random

from operator import itemgetter
from math import *
from collections import deque

class ContourStrip(object):
	def __init__(self, strip, dilated_strip, order):
		self.strip = strip
		self.dilated_strip = dilated_strip

		self.order = order
		self.avg_grad = 0.0
		self.grad_sum = 0.0
		self.min_grad = 0.0
		self.max_grad = 0.0

		# Maximum marker radius allowed for
		# this strip that will not cross
		# other strips/the contour
		self.max_mk_radius = float('inf')

	def compute_grad_data(self, grad):
		self.grad_sum = 0.0
		self.min_grad = 256.0
		self.max_grad = 0.0
		for x in self.dilated_strip:
			self.grad_sum = self.grad_sum + grad[x]
			self.min_grad = min(self.min_grad, grad[x])
			self.max_grad = max(self.max_grad, grad[x])

		self.avg_grad = self.grad_sum/len(self.dilated_strip)

		return self.avg_grad, self.grad_sum, self.min_grad, self.max_grad

class ContourTrackingRobot(object):

	def __init__(self, img, gt, grad, method, method_config,
					contours, labeled_components, simulation_data):
		self.orig_img = img
		self.img = img.copy()
		self.gt = gt

		self.grad = grad

		self.contour_img = Image8()
		self.contour_img.from_array(np.zeros(self.grad.arr.shape, np.uint8))

		self.contour_dil_strip_map = Image32()
		self.contour_dil_strip_map.from_array(np.zeros(self.grad.arr.shape, np.int32))

		self.method = method
		self.method_config = method_config

		self.contours = contours
		self.labeled_components = labeled_components

		self.error_components = None
		self.contour_signed_edt = None

		self.simulation_data = simulation_data

	## Methods for computing object contour-related data
	def compute_cur_contour_data(self, simulation_config):
		strip_length = simulation_config['strip_length']
		strip_width = simulation_config['strip_width']

		w,h = self.contour_img.size()

		# Making sure to compute EDT only for the current contour
		for x in xrange(w*h):
			self.contour_img[x] = 0

		for x in self.cur_contour:
			self.contour_img[x] = 1

		self.contour_edt_root = Image32()

		# Keeping edt for future reference
		self.contour_signed_edt = signed_edtdb(self.contour_img, self.gt,
												self.contour_edt_root)

		self.strips = self.compute_contour_strips(self.cur_contour,
													strip_length, strip_width)
		self.compute_contour_cost(self.strips)

		# Storing an image with the dilated contour
		# strip keys on it. We initialize all pixels to -1
		# since our strips indices start from 0.
		self.contour_dil_strip_map.arr[:] = -1

		for key in self.strips:
			for p in self.strips[key].dilated_strip:
				self.contour_dil_strip_map[p] = key

		# Computing the maximum radius for each
		# strip of the contour.
		self.__fit_mk_radii_to_contour__()

	def compute_contour_strips(self, contour, strip_length, strip_width):

		strips = {}

		len_contour = len(contour)

		if len_contour % strip_length == 0:
			nstrips = len_contour/strip_length
		else:
			nstrips = int(ceil(len_contour/strip_length))+1

		# The strips are numbered from 0
		for o in xrange(nstrips):
			i0 = o*strip_length
			i1 = min(i0+strip_length,len_contour)

			strip = contour[i0:i1]

			dilated_strip = dilate_contour_strip(self.contour_signed_edt,
												self.contour_edt_root,
												 strip,	strip_width)
			cs = ContourStrip(strip, dilated_strip, o)

			strips[o] = cs

		return strips

	def compute_contour_cost(self, strips):

		for key in strips:
			strips[key].compute_grad_data(self.grad)

	def __fit_mk_radii_to_contour__(self):
		strip_radii = {}
		# Resetting radii
		for i in self.strips:
			cs = self.strips[i]

			for p in cs.strip:
				strip_radii[p] = 0

		w,h = self.contour_edt_root.size()

		for p in xrange(w*h):
			p_root = self.contour_edt_root[p]
			dist = abs(self.contour_signed_edt[p])
			strip_radii[p_root] = max(strip_radii[p_root], dist)

		# Computing the maximum radius for the
		# strip by defining it as the minimum among
		# the maximum distances for all of its pixels.
		for i in self.strips:
			cs = self.strips[i]
			cs.max_mk_radius = 0
			for p in cs.strip:
				cs.max_mk_radius += strip_radii[p]

			cs.max_mk_radius /= len(cs.strip)

	# Methods for determining the next circular index when
	# summing i and cur_strip. Note that strip_index is just an application
	# of contour_index using len(self.strips) as the length of the
	# contour
	def strip_index(self, cur_strip, i, contour_ori):
		return self.contour_index(cur_strip, i, contour_ori, self.strips)

	def contour_index(self, cur_index, i, contour_ori, contour):
		next_i = i if contour_ori == 'cw' else -i

		return (cur_index + next_i) % len(contour)

	## Methods related to running the boundary-based method
	def motion(self, xy, contour_ori):
		success = 0

		path = None

		if self.method.prev_anchor >= 0:
			p0 = self.method.prev_anchor

			if type(xy) == tuple:
				w,h = self.grad.size()
				p = self.grad.toindex(xy)
			else:
				p = xy

			success, path = self.method.run(p, self.method_config)

			if success < 0:
				err_msg = 'Unable to compute path to pixel ',self.grad.topixel(p)
				raise DelineationError(err_msg)
		return path

	def add_anchor(self, xy, contour_ori, **kwargs):
		success = False

		if type(xy) == tuple:
			p = self.grad.toindex(xy)
		else:
			p = xy

		path = None

		self.past_errors = []

		if self.method.prev_anchor >= 0:
			p0 = self.method.prev_anchor
			success, path, path_seeds = self.method.add_anchor(p, self.method_config, **kwargs)
		else:
			success, path, path_seeds = self.method.add_anchor(p, self.method_config, **kwargs) # Adding first anchor

		if success:
			self.anchor_list.append(p)

			# self.contour_src_index is the contour pixel index where the
			# new anchor is positioned. This variable is used to compute
			# the path's error.
			self.contour_src_index = np.argmax(self.cur_contour == self.contour_edt_root[p])

		return bool(success), path, path_seeds

	def close_contour(self, xy):
		p = -1

		if xy is not None:
			if type(xy) == tuple:
				if xy[0] >= 0 and xy[1] >= 0:
					p = self.grad.toindex(xy)
			else:
				p = xy

		self.past_errors = []

		try:
			first_last = self.method.close_contour(p, self.method_config)
			return first_last
		except DelineationError:
			raise

	def deconfirm_path(self, xy, contour_ori, **kwargs):
		nanchors = len(self.anchor_list)
		success = False
		if nanchors > 1 and self.method.prev_anchor >= 0:
			yet_prev_anchor = -1
			cur_anchor = self.anchor_list[-1]
			prev_anchor = self.anchor_list[-2]

			if nanchors > 2:
				yet_prev_anchor = self.anchor_list[-3]

			if (cur_anchor >= 0 and self.method.pred[cur_anchor] >= 0):
				path = self.method.path(cur_anchor, prev_anchor)
				self.method.deconfirm_path(cur_anchor, prev_anchor, yet_prev_anchor)

				# Resetting the label only for the deconfirmed path
				for i in path:
					self.method.label[i] = self.method_config['right_pixel_label']

				del self.anchor_list[-1]
				success = True
			elif self.method.pred[cur_anchor] < 0:
				self.end_segment()
				success = True
		elif nanchors == 1:
			self.end_segment()
			success = True

		return success, self.motion(xy, contour_ori)

	def end_segment(self):

		self.past_errors = []

		self.method.end_segment()

		# Removing the last anchor in case it
		# was not used to form a new contour
		# segment
		if len(self.anchor_list) > 0:
			if self.method.pred[self.anchor_list[-1]] < 0:
				del self.anchor_list[-1]

	## Methods related to measuring errors ##

	def detect_error_spike(self, path, contour_ori):
		err = self.mean_square_error(path, contour_ori, self.contour_src_index)

		# We expect err_thresh to be in pixel unit, therefore
		# we raise it to power 2
		err_thresh = self.simulation_config['err_thresh']**2

		# Assuming that the error is not quadratic
		spike_detected = self.__check_path_error__(err, err_thresh)

		# Not adding error to the past errors list
		# as soon as a spike is detected
#		if not spike_detected:
		self.past_errors.append(err)

		return spike_detected, err

	# VERY IMPORTANT!!: The error should ALWAYS be computed
	# for the entire path, sice we do not know where it actually
	# occurred. Upon 'moving' the mouse, a new path might be computed
	# that takes a completely different course from previous iterations,
	# thus leading to a large contour distance error. Furthermore,
	# the first and last points of the current path might be accurately
	# located on the contour as expected, but the path between them might
	# be arbitraly irregular and, again, different from previous courses.
	def mean_square_error(self, path, contour_ori, contour_src_index):
		err = np.zeros((len(path)), np.float)

		for x in xrange(len(path)):
			err[x] = self.__dist_2_expected_cont_point__(x, path, contour_src_index,
														contour_ori)
		return np.mean(err**2)

	def max_possible_error(self):
		w,h = self.img.size()
		n = w*h

		return n**2 # The maximum possible individual error for a single
					 # path node is n**2, thus the maximum mean
					 # possible error is (n**2)*n/n, where the maxium path
					 # has size n

	def __dist_2_expected_cont_point__(self, i, path, contour_src_index, contour_ori):

		w,h = self.grad.size()

		# Since the path is organized backwards, from dst to src, we compute
		# the relative index from the last element in the path (i.e., the first
		# node from the path)
		p_index = len(path)-i-1

		p = path[i]

		# Testing for CCW orientation
		p_index = self.contour_index(contour_src_index, p_index,
									contour_ori, self.cur_contour)

		p_contour = self.cur_contour[p_index]

		px = np.array((p%w, p/w), dtype = np.int)
		px_contour = np.array((p_contour%w, p_contour/w), dtype = np.int)

		return sqrt(np.sum((px - px_contour)**2))

	def __check_path_error__(self, err, err_thresh):
		if len(self.past_errors) > 5:

#			avg = np.mean(self.past_errors)
#			stdev = np.std(self.past_errors)
#
#			return err >= avg + 3*stdev
			return err >= err_thresh
		else:
			return False

	## First anchor selection policy ##
	def __first_anchor_max_grad__(self):
		first_strip = 0
		# Selecting the first pixel from the strip highest gradient
		min_avg_grad = -1

		for i in self.strips:
			if self.strips[i].avg_grad > min_avg_grad:
				first_strip = i
				min_avg_grad = self.strips[i].avg_grad

		p = self.strips[first_strip].strip[0]

		return p, first_strip

	def __first_anchor_min_grad__(self):
		first_strip = 0
		# Selecting the first pixel from the strip with lowest gradient
		key = max(self.strips.iteritems(),key = lambda x: x[1].avg_grad)[0]
		max_avg_grad = self.strips[key].avg_grad+1

		for i in self.strips:
			if self.strips[i].avg_grad < max_avg_grad:
				first_strip = i
				max_avg_grad = self.strips[i].avg_grad

		p = self.strips[first_strip].strip[0]

		return p, first_strip

	# Select a strip at random and picks the first pixel from it
	# as an anchor point
	def __first_anchor_random__(self):
		# Setting constant seed (or not) to allow reproducible results
		if not self.simulation_config['truly_random_first_anchor']:
			random.seed(145871)

		first_strip = random.randint(1,len(self.strips))

		p = self.strips[first_strip].strip[0]

		return p, first_strip

	## Simulation-related methods ##
	def run_simulation(self, img_name, simulation_config, gui = None):
		primary_label = self.method_config['primary_label']
		secondary_label = self.method_config['right_pixel_label']

		# simulation_config has already been deep copied
		# for this method at this point
		self.simulation_config = simulation_config

		if self.simulation_config['first_anchor_choice'] == 'low_grad':
			select_first_anchor_fun = self.__first_anchor_min_grad__
		elif self.simulation_config['first_anchor_choice'] == 'high_grad':
			select_first_anchor_fun = self.__first_anchor_max_grad__
		else:
			select_first_anchor_fun = self.__first_anchor_random__

		accuracy_measure = self.simulation_config['accuracy_measure']

		novice_user = self.simulation_config['novice_user']

		# Simulating for all contours in non-increasing order of length.
		# We first determine all the contours that lie in a same
		# connected component of the groundtruth.
		component_contours = self.sort_component_contours()

		# We finally assume that the largest contour (first one in the list)
		# represents the object, while the remaining contours of the same
		# component are holes in the contour. We thus treat them in this
		# fashion.
		final_label = None
		for lb in component_contours:
			label = None

			# i is the id of the current contour
			for i in component_contours[lb]:
				number_of_segments,_ = self.simulate(i, img_name, select_first_anchor_fun,
													novice_user, gui=gui)

				if number_of_segments is None:
					print 'Error! Ignoring erroneous and/or spurious contour.'
				else:
					closed = close_holes(self.method.label)

					# Assuming that only the first contour represents the object
					if label is None:
						label = closed
					else:
						intersect_labels(label, closed, secondary_label)

					# Resetting the method's label for each new contour
					self.method.label.arr[:] = 0

					self.simulation_data.add_interaction_amount(img_name, number_of_segments)

					self.gui_show_path(gui, [], label, sleep = 1)

			# Intersecting the recently computed label for
			# the connected component of label lb with
			# the result of previous connected components.
			if final_label is None:
				final_label = label
			else:
				intersect_labels(final_label, label, primary_label)

			self.gui_show_path(gui, [], final_label, sleep = 1)

			if self.simulation_config['save_video']:
				self.gui_save_video(gui, img_name)
			else:
				self.gui_save_figures(gui, img_name, len(self.anchor_list), 'final')

		acc = self.simulation_data.add_accuracy(img_name, final_label, self.gt, 0,
												accuracy_measure)

		print '%s for %s: %f' % (accuracy_measure, self.method, acc)

		# Storing labels to compute control measures
		self.simulation_data.add_label(img_name, final_label, 0)
		self.simulation_data.add_control_measures(img_name)

		# Replacing the method's label with the final result
		self.method.label = final_label

		if gui is not None:
			time.sleep(3)

	def sort_component_contours(self):
		# We sort the contours of of each component by length,
		# and assume the secondary contours as holes that need
		# to be cut out.
		component_contours = {}
		for i in xrange(len(self.contours)):
			p = self.contours[i][0]
			lb = self.labeled_components[p]

			if lb not in component_contours:
				component_contours[lb] = []

			component_contours[lb].append(i)

		# Afterwards, we sort the contours of each component by length.
		for lb in component_contours:
			contours = component_contours[lb]

			if len(contours) > 1:
				component_contours[lb] = sorted(contours,
											key = lambda x: len(self.contours[contours[x]]),
											reverse = True)

		return component_contours

	# Initializing simulation data
	def init_simulation(self, contour_index, simulation_config):
		# Error related attributes
		self.past_errors = []

		self.contour_src_index = -1

		print '\nInitializing simulation data for %s\n' % str(self.method).capitalize()

		self.cur_contour = self.contours[contour_index]
		self.compute_cur_contour_data(simulation_config)
		self.anchor_list = []

		self.error_components = None

		self.simulation_config = simulation_config

		self.method.reset()

	# Simulator for the boundary-based method
	def simulate(self, contour_index, img_name, select_first_anchor_fun,
					novice_user, niterations = 0, gui = None):

		primary_label = self.method_config['primary_label']
		self.init_simulation(contour_index, self.simulation_config)

		contour_ori = self.simulation_config['contour_ori']
		err_thresh = self.simulation_config['err_thresh']
		verbose = self.simulation_config['verbose']

		p, first_strip = select_first_anchor_fun()
		first_anchor = p

		if verbose:
			print 'First anchor %s ' % str(self.grad.topixel(first_anchor))

		success, _, _ = self.add_anchor(first_anchor, contour_ori)

		self.simulation_data.add_anchor(img_name, first_anchor, None, path_error = 0.0)

		self.gui_show_path(gui, None, self.method.label)

		if self.simulation_config['save_video']:
			self.gui_save_video(gui, img_name)
#		else:
#			self.gui_save_figures(gui, img_name, len(self.anchor_list))

		if not success:
			raise DelineationError('No success when adding the first anchor, weird...')

		# Storing the first anchor and strip as the
		# best candidates so far, since that's the "last"
		# selection that was made. This must be done
		# to handle the case when a failure occurs when
		# evaluating the first strip after the current
		# one.
		best_anchor_candidate = first_anchor
		best_strip_candidate = first_strip

		cur_strip = self.strip_index(first_strip, 1, contour_ori) # starting from the next strip

		find_least_worse = False
		# Iterating until the whole contour is cycled through.
		full_loop = False
		first_iteration = niterations

		while not full_loop:
			cs = self.strips[cur_strip]

			strip_pixels = cs.dilated_strip

			if verbose:
				print 'Cur_strip %d, first strip %d' % (cur_strip,first_strip)

			valid_path = False
			selected_path = None

			# Since we are forcing the method to find a
			# new anchor regardless of the path error,
			# we fallback to __eval_anchor_candidates__
			# ignoring error spikes. This is naturally
			# accomplished when find_least_worse == True
			if find_least_worse or not novice_user:
				(valid_path, selected_path,
				candidate_anchor, full_loop) = self.__eval_anchor_candidates__(strip_pixels,
																				contour_ori,
																				find_least_worse,
																				first_anchor)
			else:
				(valid_path, selected_path,
				candidate_anchor, full_loop) = self.__eval_coherent_anchor_candidates__(strip_pixels,
																				contour_ori,
																				first_anchor)


			if not full_loop:
				if valid_path:
					# We allow some error to be added by searching
					# for the furthest strip from the current anchor point
					# that has at least one pixel within the acceptable
					# error margin
					best_anchor_candidate = candidate_anchor
					best_strip_candidate = cur_strip
					# adding 1 to the current strip index to move
					# to the next strip in the proper orientation

					cur_strip = self.strip_index(cur_strip, 1, contour_ori)

					self.gui_show_path(gui, selected_path, None)

					if self.simulation_config['save_video']:
						self.gui_save_video(gui, img_name)
#					else:
#						self.gui_save_figures(gui, img_name, len(self.anchor_list)+1,
#											prefix = 'moving')

				else:
					if not find_least_worse:
						anchor = best_anchor_candidate
						cur_strip = best_strip_candidate
					else:
						# In case all else has failed, the
						# least worse candidate anchor (current)
						# is the one that should be used along
						# with the current strip.
						anchor = candidate_anchor

					if anchor in self.anchor_list or anchor < 0:
						if verbose:
							print 'Searching for the least worse anchor '\
									'candidate in the next iteration.'
						# Activating flag that indicates that the least
						# worse anchor candidate must be found
						find_least_worse = True
						cur_strip = self.strip_index(cur_strip, 1, contour_ori)
					else:
						# When a suitable anchor is found, we turn off the
						# find_least_worse flag
						find_least_worse = False

						if verbose:
							print '\nNew anchor %s' % str(self.grad.topixel(anchor))
							print 'New anchor strip %d\n' % cur_strip

						# Displaying the wrong path dilated and with inverted color
						inv_color = None
						if gui is not None:
							inv_color = gui.path_color
							inv_color = (1.0-inv_color[0], 1.0-inv_color[1], 1.0-inv_color[2])
						self.gui_show_path(gui, selected_path, None, color = inv_color,
											path_thickness = 2.0)

						if self.simulation_config['save_video']:
							self.gui_save_video(gui, img_name)
#						else:
#							self.gui_save_figures(gui, img_name, len(self.anchor_list)+1,
#													prefix = 'spike')

						prev_contour_src_index = self.contour_src_index
						_, path, _  = self.add_anchor(anchor, contour_ori)

						self.set_path_as_label(path, primary_label)

						self.gui_show_path(gui, path, self.method.label)

						if self.simulation_config['save_video']:
							self.gui_save_video(gui, img_name)
						else:
							self.gui_save_figures(gui, img_name, len(self.anchor_list),
													prefix='accepted', save_graph = False)

						# Storing anchor, path, and label for this iteration
						err = self.mean_square_error(path, contour_ori, prev_contour_src_index)
						self.simulation_data.add_anchor(img_name, anchor,path, path_error = err)

						# Moving to the next strip
						cur_strip = self.strip_index(cur_strip, 1, contour_ori)

						niterations = niterations + 1
		number_of_segments = self.end_simulation(img_name, best_anchor_candidate,
													primary_label, gui)

		return number_of_segments, niterations

	def end_simulation(self, img_name, anchor_candidate, primary_label,	gui):
		print '\nClosing the contour'

		# We always close from best_anchor_candidate because, since the original
		# first anchor will now become the last one of the contour,
		# because we cannot guarantee that the path from the current
		# previous anchor to the orignal first anchor point will be the desired
		# one. By closing from best_anchor_candidate this is mitigated.
		try:
			prev_anchor = self.method.prev_anchor

			first,last = self.try_to_close_contour((None,anchor_candidate), prev_anchor)
		except DelineationError:
			raise
		else:
			path = self.method.path(last,-1)

			# Replacing the first anchor point by the new one
			# after closing the contour
			first_anchor_time = self.simulation_data.clocks[img_name][self.anchor_list[0]]
			self.simulation_data.del_anchor(img_name, self.anchor_list[0])
			del self.anchor_list[0]

			path_to_last = self.method.path(last, self.method.prev_anchor)

			if first not in self.anchor_list:
				self.anchor_list.insert(0,first)
				# Storing anchor, path, and label for this iteration
				self.simulation_data.add_anchor(img_name, first, None,
												in_front = True,
												anchor_time = first_anchor_time,
												path_error = 0.0)

			self.anchor_list.append(last) # Adding the last contour pixel
										  # as an "anchor" point.
										  # Note that best_anchor_candidate
										  # might have been added as an anchor
										  # point too.

			# Adding the last anchor point to ensure maximum
			# reproducibility, even though it is just
			# the neighbor of the first anchor
			self.simulation_data.add_anchor(img_name, last, path_to_last)



			self.set_path_as_label(path, primary_label)

			self.gui_show_path(gui, path, self.method.label)

			if self.simulation_config['save_video']:
				self.gui_save_video(gui, img_name)
#			else:
#				self.gui_save_figures(gui, img_name, len(self.anchor_list))

			# The number of segments is always equal to the number of anchors.
			# Even though the last segment closes the contour, the user
			# must determine if the method reaches the first point of
			# the contour properly. Therefore, the last segment also requires
			# user interaction, which we count here. We ignore the last
			# contour pixel as an anchor point. The last contour pixel
			# is only necessary for hybrid segmentation methods.
			number_of_segments = len(self.anchor_list) -1

			self.finish_segmentation_msg(number_of_segments)

			return number_of_segments

	# @param anchor_test_order indicates a tuple of pixels that should
	# be used to try to close the contour from. None indicates
	# that the previous anchor will be used instead.
	def try_to_close_contour(self, anchor_test_order, prev_anchor):
		first = -1
		last = -1
		verbose = self.simulation_config['verbose']

		# 'None' below represents that the
		# contour should be closed from the previous
		# anchor point, without attempting to add a new
		# one.
		for p in anchor_test_order:
			try:
				if p is not None and self.simulation_config['verbose']:
					print 'New anchor candidate %s' % str(self.grad.topixel(p))

				prev_anchor = self.method.prev_anchor
				first,last = self.close_contour(p)
			except DelineationError as e:
				if verbose:
					print e
					print 'Trying again...'
			else:
				# The contour was only successfully closed when
				# it has at least two pixels. Hence, pixels first and
				# last must be valid (greater or equal to 0)
				if first >= 0 and last >= 0:
					if p is None:
						p = prev_anchor

					# In case p is given, i.e., p is not None,
					# then a new anchor should have been added by the method.
					# For safety, we check whether p is in anchor_list
					# before adding it to the list.
					if p not in self.anchor_list:
						self.anchor_list.append(p)

					if verbose:
						print 'Success when closing the contour from %s' % self.grad.topixel(p)

					return first, last

		raise DelineationError('Shoot.. no success when closing the contour due to self loop.')

	def __eval_anchor_candidates__(self, strip_pixels, contour_ori,
										find_least_worse, first_anchor):
		valid_path = False

		min_error = self.max_possible_error()

		best_anchor_candidate = -1

		best_path = None
		worst_path = None

		full_loop = False
		for p in strip_pixels:

			if p == first_anchor:
				full_loop = True
				break

			try: # If self-loop occured for the current Pixel
				  # then ignore it and try to move on
				path = self.motion(p, contour_ori)
			except DelineationError:
				continue

			A = AdjRel(radius=1.5)
			if path is not None:
				spike_detected, path_err = self.detect_error_spike(path, contour_ori)

				dst_strip = self.contour_dil_strip_map[p]
				anchor_strip = self.contour_dil_strip_map[self.method.prev_anchor]

				# Searching for all the strips
				# that are intersected by the path
				intersecting_strips = {}

				for i in path:
					strip_key = self.contour_dil_strip_map[i]
					# We test strip_key >= 0 since we initialize contour_dil_strip_map
					# to -1 for all p in order to allow the strips to be numbered from 0
					if strip_key >= 0:
						if strip_key not in intersecting_strips:
							intersecting_strips[strip_key] = set()

						intersecting_strips[strip_key].add(i)


				strip_key = self.strip_index(dst_strip, 1, contour_ori)
				consistent = True

				future_strips = set()
				while strip_key != anchor_strip:
					if strip_key in intersecting_strips:
						future_strips.add(strip_key)
					strip_key = self.strip_index(strip_key, 1, contour_ori)

				# If the contour blocks a "future strip",
				# i.e., one that is ahead of dst_strip,
				# then we should consider that the current
				# path is not coherent to avoid self-blocking
				for strip_key in future_strips:
					if self.__is_strip_blocked__(strip_key, intersecting_strips, A):
						consistent = False
						break
				# find_least_worse simply provides a way
				# to find the least worse anchor candidate in
				# case all else fails.
				if (spike_detected and not find_least_worse) or (not consistent):
					worst_path = path
					if self.simulation_config['verbose']:
						if not consistent:
							print 'Inconsistent path to %s!' % str(self.grad.topixel(p))
						else:
							print 'Spike during motion in %s!' % str(self.grad.topixel(p))

				else:
					valid_path = True

					if path_err < min_error:
						best_anchor_candidate = p
						min_error = path_err
						best_path = path


		if valid_path:
			selected_path = best_path
		else:
			selected_path = worst_path

		if find_least_worse:
			valid_path = False

		return valid_path, selected_path, best_anchor_candidate, full_loop

	def __is_strip_blocked__(self, strip_key, intersecting_strips, A):
		strip_pixels = self.strips[strip_key].dilated_strip

		p_free = None
		# Finding a pixel that belongs to the strip
		# but is not on the intersecting path.
		for p in strip_pixels:
			if p not in intersecting_strips[strip_key]:
				p_free = p
				break

		if p_free is not None:
			Q = deque([p_free])

			# BFS to visit the pixels in the component.
			visited = set([p_free])
			while len(Q) > 0:
				p = Q.popleft()

				px = self.method.grad.topixel(p)
				for adj in A:
					px_adj = px + adj

					if self.method.grad.valid_pixel(px_adj):
						p_adj = self.method.grad.toindex(px_adj)
						if (p_adj in strip_pixels and p_adj not in visited
							and p_adj not in intersecting_strips[strip_key]):
							visited.add(p_adj)
							Q.append(p_adj)

			max_component_size = len(strip_pixels) - len(intersecting_strips[strip_key])

			# If the number of visited pixels is equal to the
			# size of the component encompassed by all the
			# strip pixels minus the pixels on the intersecting
			# path, then the path does not cut the strip in two
			# and a single component exists. Hence, the strip is not
			# blocked.
			return len(visited) != max_component_size
		else:
			return False



	def __eval_coherent_anchor_candidates__(self, strip_pixels, contour_ori, first_anchor):
		valid_path = False
		has_valid_path = False
		verbose = self.simulation_config['verbose']

		min_error = self.max_possible_error()

		best_anchor_candidate = -1

		best_path = None
		worst_path = None
		selected_path = None

		full_loop = False
		i = 0

		for p in strip_pixels:
			i = i + 1
			if p == first_anchor:
				full_loop = True

				break
			try:
				path = self.motion(p, contour_ori)
			except DelineationError as e:
				if verbose:
					print e
				continue

			if path is not None:
				spike_detected, path_err = self.detect_error_spike(path, contour_ori)

				if spike_detected:
					worst_path = path
					if verbose:
						print 'Spike during motion in %s! %f' % (str(self.grad.topixel(p)),
															path_err)

				last_strip, dst_strip = self.__find_last_coherent_strip__(path, p,
																self.method.prev_anchor,
																contour_ori)
				valid_path = last_strip == dst_strip

				if not spike_detected and valid_path:
					has_valid_path = True

					if path_err < min_error:
						best_anchor_candidate = p
						min_error = path_err
						best_path = path


		if valid_path:
			selected_path = best_path
		else:
			selected_path = worst_path

		return has_valid_path, selected_path, best_anchor_candidate, full_loop

	def __find_last_coherent_strip__(self, path, dst, anchor, contour_ori):
		dst_strip = self.contour_dil_strip_map[dst]
		anchor_strip = self.contour_dil_strip_map[anchor]

		# Searching for all the strips
		# that are intersected by the path
		intersecting_strips = set()

		for p in path:
			strip_key = self.contour_dil_strip_map[p]
			# We test strip_key >= 0 since we initialize contour_dil_strip_map
			# to -1 for all p in order to allow the strips to be numbered from 0
			if strip_key >= 0 and strip_key not in intersecting_strips:
				intersecting_strips.add(strip_key)

		# Searching for the last strip in the
		# longest sequence of adjacent strips
		# that are intersected by the path
		strip_key = anchor_strip

		# A contour segment starting from a single anchor might
		# loop through all strips in small contours.
		# Hence, we need to test this case to avoid infinite
		# loops.
		last_strip = -1

		if len(intersecting_strips) != len(self.strips):
			while last_strip != dst_strip and strip_key in intersecting_strips:
				last_strip = strip_key
				strip_key = self.strip_index(strip_key, 1, contour_ori)

			# If the contour intersects a "future strip",
			# i.e., one that is ahead of dst_strip,
			# then we should consider that the current
			# path is not coherent to avoid self-blocking
			strip_key = self.strip_index(dst_strip, 1, contour_ori)
			while strip_key != anchor_strip and last_strip >= 0:
				if strip_key in intersecting_strips:
					last_strip = -1
				strip_key = self.strip_index(strip_key, 1, contour_ori)
		else:
			last_strip = dst_strip

		return last_strip, dst_strip

	## Miscellaneous code for exhibiting data
	def finish_segmentation_msg(self, number_of_segments):
		msg = '** Simulation finished.'
		nsegments_msg = '** Number of segments %d.' % number_of_segments
		diff = len(nsegments_msg)-len(msg)
		msg = msg + ' '*diff + '**'
		nsegments_msg = nsegments_msg + '**'
		nstars = max(len(msg),len(nsegments_msg))
		print '\n' + '*'*nstars
		print msg + '\n' + nsegments_msg
		print '*'*nstars + '\n\n'

	def set_path_as_label(self, path, primary_label):
		if path is not None:
			for p in path:
				self.method.label[p] = primary_label

	def gui_show_path(self, gui, path, primary_label, path_seeds = None, color = None,
						sleep = None, anchors = None, path_thickness = 1.0):
		# GUI operations
		if gui is not None:
			if primary_label is not None:
				gui.display_overlayed_data(path, path_seeds, label = primary_label,
											anchors = anchors)
			else:
				if color is not None:
					gui.show_motion(path, c = color, anchors = anchors,
									path_thickness = path_thickness)
				else:
					gui.show_motion(path, anchors = anchors,
									path_thickness = path_thickness)


		if sleep is not None:
			time.sleep(sleep)

	def gui_save_figures(self, gui, img_name, fig_id, prefix='',
							save_fig = None, save_graph = True):
		if save_fig is None:
			save_fig = self.simulation_config['save_fig']

		if gui is not None and save_fig:
			img_key = os.path.split(img_name)[1]
			img_key = os.path.splitext(img_key)[0]
			if 'prefix' != '':
				prefix = '%s_%s_%s' % (self.method, img_key, prefix)
			else:
				'%s_%s' % (self.method, img_key)

			if save_graph:
				gui.savegraph(fig_id, prefix = prefix)

			gui.savefig(fig_id, prefix = prefix)

	def gui_save_video(self, gui, img_name, prefix = ''):
		if not hasattr(self, 'video_frame_id'):
			setattr(self, 'video_frame_id', 0)

		self.gui_save_figures(gui, img_name, self.video_frame_id, prefix,
								True)
		self.video_frame_id = self.video_frame_id + 1
