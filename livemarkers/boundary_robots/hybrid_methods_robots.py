# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
from boundary_robots import *

class LiveMarkersRobot(ContourTrackingRobot):
	## Simulation-related methods ##
	def run_simulation(self, img_name, simulation_config, gui = None):
		# simulation_config has already been deep copied
		# for this method at this point
		self.simulation_config = simulation_config

		# Customizing simulation criteria
		if self.simulation_config['first_anchor_choice'] == 'low_grad':
			select_first_anchor_fun = self.__first_anchor_min_grad__
		elif self.simulation_config['first_anchor_choice'] == 'high_grad':
			select_first_anchor_fun = self.__first_anchor_max_grad__
		else:
			select_first_anchor_fun = self.__first_anchor_random__

		if self.simulation_config['stop_criterion'] == 'niterations':
			stop_criterion_fun = self.__stop_by_number_of_iterations__
		else:
			stop_criterion_fun = self.__stop_by_accuracy__

		marker_choice = self.simulation_config['marker_choice']
		if marker_choice == 'markers_on_low_grad':
			prepare_segment_selection_fun = self.__prepare_path_selection_on_low_grad__
			next_segment_fun = self.__next_path_by_grad__
		elif marker_choice == 'markers_on_high_grad':
			prepare_segment_selection_fun = self.__prepare_path_selection_on_high_grad__
			next_segment_fun = self.__next_path_by_grad__
		elif marker_choice == 'error_component':
			prepare_segment_selection_fun = self.__prepare_path_selection_by_error_comps__
			next_segment_fun = self.__next_path_by_error_comps__
		else:
			print 'Error! Marker choice of \'%s\' invalid for %s' % (marker_choice,
																	self.method)
			exit()

		if self.simulation_config['first_marker_choice'] == 'first_from_anchor_list':
			first_segment_fun = self.__first_segment_from_anchor_list__
		else:
			first_segment_fun = next_segment_fun

		novice_user = self.simulation_config['novice_user']
		accuracy_measure = self.simulation_config['accuracy_measure']

		# Simulating the underlying boundary segmentation method to acquire
		# the path seeds
		print 'Pre-computing path seeds'
		boundary_method_data = self.precompute_path_seeds(img_name,
														select_first_anchor_fun,
														novice_user,
														gui = None)

		print 'Simulating with pre-computed path seeds'

		# Computing all path seeds and all paths from the anchor
		# points computed by the boundary-method robot, for all contours
		for contour_index in boundary_method_data:
			(anchor_list,contour_ori,
			path_nodes, dilated_paths,
			contour_seeds, inv_seed_index) = boundary_method_data[contour_index]

			# Adding first contour seed
			anchor0 = anchor_list[0]

			self.add_anchor(anchor0, contour_ori)

			path_collection = {}

			# The last anchor point represents
			# the last closed contour pixel. Hence
			# We consider pairwise segments between
			# i,i+1 from 0 to len(anchor_list)-1
			for i in xrange(len(anchor_list)-1):
				i1 = i+1

				anchor1 = anchor_list[i1]
				anchor0 = anchor_list[i]
				if simulation_config['verbose']:
					print 'Anchor1 %s to anchor0 %s' % (self.grad.topixel(anchor1),
													self.grad.topixel(anchor0))

				# Since we have pre-computed all anchor points using the result of the
				# boundary-based robot, we just need to use the computed path nodes.
				# Note that the paths in path_collection will only point to path_nodes.
				path = path_nodes[anchor1]

				# Determining the markers/seeds for the current segment/path
				# from the seeds generated for the entire contour
				path_seeds = {}
				for r in path:
					# Path node r is not necessarily a seed,
					# depending on whether we choose to use
					# the first anchor as seed or not.
					if r in contour_seeds:
						path_seeds[r] = contour_seeds[r]
					# We test if the current path node
					# is the root of at least one marker pixel.
					# If true, then we add all marker pixels rooted
					# at r as path seeds.
					if r in inv_seed_index:
						for s in inv_seed_index[r]:
							path_seeds[s] = contour_seeds[s]

				# Dilating the path to obtain a thin strip around it.
				dilated_path = dilated_paths[anchor1]

				# Some paths might have just two pixels. In this case,
				# path seeds might not be generated and we should not
				# consider the corresponding path during simulation.
				if(path_seeds is not None and len(path_seeds) > 0
					and dilated_path is not None and len(dilated_path) > 0):
					path_collection[anchor1] = [anchor0, path, path_seeds, dilated_path]

			# Removing the contour seeds and inverted index,
			# since we don't need them anymore. Both are at the
			# end of boundary_method_data.
			del boundary_method_data[contour_index][-1]
			del boundary_method_data[contour_index][-1]

			# Adding the paths available for simulation after the
			# filtering previously performed.
			boundary_method_data[contour_index].append(path_collection)

			# Resetting the method at each iteration to make sure it is completely
			# clear for the next
			self.method.reset()

		# Actual simulation of LiveMarkers, using the pre-computed paths and seeds
		# from the boundary-method robot.
		niterations = 0
		# contour_index is the id of the current contour. Since we have precomputed
		# all contour orientations, we just need to simulate the method
		# with the proper anchorlist and path seeds
		for contour_index in boundary_method_data:
			print 'Simulating for contour with index %d' % contour_index
			contour_ori = boundary_method_data[contour_index][1]
			self.simulation_config['contour_ori'] = contour_ori

			(interactions,
			niterations) = self.simulate_with_pre_selected_segments(contour_index, img_name,
																	boundary_method_data[contour_index],
																	select_first_anchor_fun,
																	stop_criterion_fun,
																	prepare_segment_selection_fun,
																	first_segment_fun,
																	next_segment_fun,
																	accuracy_measure,
																	niterations, gui=gui)

			self.simulation_data.add_interaction_amount(img_name, interactions)

		self.simulation_data.add_control_measures(img_name)

	def precompute_path_seeds(self, img_name, select_first_anchor_fun,
								novice_user, gui):

		primary_label = self.method_config['primary_label']
		left_pixel_label = self.method_config['left_pixel_label']
		right_pixel_label = self.method_config['right_pixel_label']
		mk_id = 1
		lm_mk_radius = self.method_config['lm_mk_radius']

		orig_contour_ori = self.simulation_config['contour_ori']

		contour_ori = orig_contour_ori

		boundary_method_data = {}

		# Running the boundary-method robot to obtain the anchor points
		# for all contours. We do this because we assume the secondary
		# contours, of each component, as holes, and their seeds
		# will have the opposite orientation
		# from the major contour since we want to cut out those holes
		# using the region-based method.
		component_contours = self.sort_component_contours()

		# We finally assume that the largest contour (first one in the list)
		# represents the object, while the remaining contours of the same
		# component are holes in the contour. We thus treat them in this
		# fashion.
		for lb in component_contours:
			# Restoring the original contour orientation when segmenting
			# a new connected component.
			self.simulation_config['contour_ori'] = orig_contour_ori

			# i is the id of the current contour
			for i,contour_index in zip(component_contours[lb],
										xrange(len(component_contours[lb]))):
				# We assume that the largest contour, contour_index == 0, is the one
				# that subsumes all others, while the remaining contours
				# are holes. Therefore, for the next contour, contour_index == 1, we
				# invert the orientation to avoid destroying the segmentation
				# during live markers.
				if contour_index == 1:
					contour_ori = self.simulation_config['contour_ori']
					self.simulation_config['contour_ori'] = 'cw' if contour_ori == 'ccw' else 'ccw'

				ContourTrackingRobot.simulate(self,i, img_name,
												select_first_anchor_fun,
												novice_user, gui=gui)

				closed = close_holes(self.method.label)

				# Obtaining the inverted seed index and seeds
				# for the entire contour at once. This ensures that
				# the path seeds will completely surround the contour
				# and will not leak to the background (or foreground).
				inv_seed_index = {}
				anchorf = self.anchor_list[-1]
				path_nodes = {}
				dilated_paths = {}

				anchors_radii = [(self.anchor_list[0],1.5)]

				for x in xrange(1,len(self.anchor_list)):
					anchor1 = self.anchor_list[x]
					anchor0 = self.anchor_list[x-1]
					# Computing the path nodes
					path = self.method.path(anchor1, anchor0)
					path_nodes[anchor1] = path
					# Dilating the path to obtain a thin strip around it.
					anchors = [(anchor0, 1.5), (anchor1, 1.5)]
					dilated_paths[anchor1] = self.method.path_seeds(anchors, 1,1,1,1)

					radius = self.__max_radius_for_segment__(path)
					radius = lm_mk_radius if radius >= lm_mk_radius else 1.5

					anchors_radii.append((anchor1, radius))

				contour_seeds = self.method.path_seeds(anchors_radii, primary_label,
														left_pixel_label, right_pixel_label,
														mk_id, inv_seed_index = inv_seed_index)

				# Keeping the anchor list and contour orientation for simulating live markers
				boundary_method_data[i] = [copy.deepcopy(self.anchor_list), contour_ori,
											path_nodes, dilated_paths, contour_seeds, inv_seed_index]

				# Resetting the method at each iteration to make sure it is completely
				# clear for the next one
				self.method.reset()

		# Resetting the data saved by the boundary-based method
		self.simulation_data.reset_image_data(img_name, self.img)

		# Computing accuracy threshold if it has not been set before
		self.__compute_accuracy_threshold__(component_contours,
											boundary_method_data)

		self.simulation_config['contour_ori'] = orig_contour_ori

		# Displaying seeds
		self.gui_show_path(gui, [], self.method.label, contour_seeds, sleep = 3)

		return boundary_method_data


	def __compute_accuracy_threshold__(self, component_contours,
											boundary_method_data):
		# Computing accuracy threshold if it has not been set before
		if self.simulation_config['stop_criterion'] == 'accuracy':

			# if 'common_accuracy_thresh is not set then we compute it
			# from our parent's method accuracy
			if 'common_accuracy_thresh' not in self.simulation_config:
				print 'Determining accuracy from %s\'s parent' % (self.method)
##		 THIS FUNCTION IS WRONG! ACCURACY SHOULD BE SET PER CONTOUR.
##		 For now, the accuracy for the whole image is considered.
				# Going through all connected components contour seeds
				# to make sure that the delineation accuracy
				# reflects the best that can be achieved by
				# the hybrid method as observed from its
				# parent (boundary-based method)
				for lb in component_contours:
					# i is the id of the current contour
					for i,contour_index in zip(component_contours[lb],
												xrange(len(component_contours[lb]))):
						# Using all contour seeds to ensure that the segmentation has high accuracy.
						# The contour seeds naturally prevent leakings that might occur when a new
						# segment is addded.
						contour_seeds = boundary_method_data[i][-2]
						self.method.delineate(contour_seeds, None)

				final_label = self.method.label.copy()

				if self.simulation_config['accuracy_measure'] == 'hamming':
					acc = compute_hamming_dist_norm_by_gt(final_label, self.gt)
				elif self.simulation_config['accuracy_measure'] == 'f-measure':
					acc = compute_fmeasure(final_label, self.gt)[0]
				else:
					# The first index of the tuple represents the boundary error.
					# The others represent false positives/negatives and other
					# measures.
					acc = compute_sgn_edt_error_measures(final_label, self.gt)[0]

				print '\nSetting accuracy threshold to %f, ' % acc + \
						'the upper limit obtained from the pre-computed path segments\n'
				self.simulation_config['accuracy_thresh'] = acc
			else:
				self.simulation_config['accuracy_thresh'] = self.simulation_config['common_accuracy_thresh']


	def __max_radius_for_segment__(self, path):
		radius = float('inf')
		for p in path:
			p_strip = self.contour_dil_strip_map[p]

			if p_strip >= 0:
				radius = min(radius, self.strips[p_strip].max_mk_radius)

		return radius

	def simulate_with_pre_selected_segments(self, contour_index, img_name, contour_data,
						select_first_anchor_fun, stop_criterion_fun,
						prepare_segment_selection_fun,
						first_segment_fun, next_segment_fun,
						accuracy_measure,
						niterations = 0, gui = None):

		lm_mk_radius = self.method_config['lm_mk_radius']

		# Simulating LWOF or other boundary-based method
		self.init_simulation(contour_index, self.simulation_config)

		number_of_segments = 0

		anchor_list = contour_data[0] # The anchor list is always the first
		path_collection = contour_data[-1] # The path collection is always the last

		prepared_paths = prepare_segment_selection_fun(anchor_list, path_collection)

		accuracy = -1.0

		# Selecting the contour path seeds in non-decreasing order of
		# average gradient cost.
		selected_seeds = {}

		anchor1 = first_segment_fun(anchor_list, prepared_paths)

		# Showing pre-computed paths, in order, to save
		# the pre_computed_paths image
		used_anchors = set()
		for anchor in path_collection:
			_, path, _,_ = path_collection[anchor]

			used_anchors.add(anchor)

			self.set_path_as_label(path, 1)
			self.gui_show_path(gui, path, self.method.label, selected_seeds,
								sleep = 1, anchors = used_anchors)

		self.method.label.arr[:] = 0

		if self.simulation_config['save_video']:
			self.gui_save_video(gui, img_name, prefix = 'pre_computed_paths')
		else:
			self.gui_save_figures(gui, img_name, 0, prefix = 'pre_computed_paths')

		used_anchors = set()
		while (not stop_criterion_fun(img_name, number_of_segments)
				and anchor1 is not None):
			_, path, path_seeds,_ = path_collection[anchor1]

			selected_seeds.update(path_seeds)

			# Delineating using the path seeds from all iterations
			# to avoid dealing with methods that do not support
			# iterative updates.
			self.method.delineate(selected_seeds, None, **self.method_config)

			# Computing the accuracy for the current segmentation.
			accuracy = self.simulation_data.add_accuracy(img_name, self.method.label,
														self.gt, niterations,
														accuracy_measure)
			seeds = None
			if self.simulation_config['save_seeds']:
				seeds = path_seeds

			self.simulation_data.add_anchor(img_name, anchor1, path, seeds)
			self.simulation_data.add_label(img_name, self.method.label, niterations)

			if self.simulation_config['verbose']:
				print '%s for %s: %f' % (accuracy_measure, self.method, accuracy)

			# Displaying images
			used_anchors.add(anchor1)
			used_anchors.add(path_collection[anchor1][0])
			self.gui_show_path(gui, path, self.method.label, selected_seeds,
								sleep = 1, anchors = used_anchors)

			if self.simulation_config['save_video']:
				self.gui_save_video(gui, img_name)
			else:
				self.gui_save_figures(gui, img_name, niterations)

			anchor1 = next_segment_fun(anchor_list, prepared_paths)

			number_of_segments = number_of_segments + 1
			niterations = niterations + 1

		self.finish_segmentation_msg(number_of_segments)

		return number_of_segments, niterations

	# Stop criteria must return
	def __stop_by_accuracy__(self, img_name, iteration):
		stop = False
		accuracy_thresh = self.simulation_config['accuracy_thresh']
		acc_discount = None

		accuracy_discount = 0.0
		if 'accuracy_discount' in self.simulation_config:
			accuracy_discount = self.simulation_config['accuracy_discount']
		# Since we are testing the accuracy of the previous iteration
		# *before* allowing the current iteration to proceed, we must test
		# the accuracy of iteration - 1. Note that the iteration counter
		# starts from 0.
		if iteration-1 in self.simulation_data.accuracy[img_name]:
			acc = self.simulation_data.accuracy[img_name][iteration-1]
			if self.simulation_config['accuracy_measure'] == 'f-measure':
				stop = acc >= accuracy_thresh - accuracy_discount
			else:
				stop = acc <= accuracy_thresh + accuracy_discount


		return stop

	def __stop_by_number_of_iterations__(self, img_name, iteration):
		iteration_thresh = self.simulation_config['iteration_thresh']

		# Iterations start from 0.
		return iteration >= iteration_thresh

	# Path selection criteria
	def __first_segment_from_anchor_list__(self, anchor_list, path_collection):
		if len(anchor_list) < 2:
			return None

		# Note that the segments are always represented by their last
		# anchor point
		return anchor_list[1]

	def __prepare_path_selection_on_low_grad__(self, anchor_list,
													path_collection):
		return self.__prepare_path_selection_by_grad__(anchor_list, path_collection,
														False)

	def __prepare_path_selection_on_high_grad__(self, anchor_list,
														path_collection):
		return self.__prepare_path_selection_by_grad__(anchor_list, path_collection,
														True)

	def __prepare_path_selection_by_grad__(self, anchor_list, path_collection,
												reverse):
		# Other functions might alter this to max
		self.__next_path = 0
		# Computing the path seeds for all the contour segments
		# computed solely by LWOF (or other boundary-based method).
		for anchor1 in path_collection:
			path = path_collection[anchor1][1]

			# Computing the average gradient cost for a thin strip (1.5 radius)
			# around the contour segment. We do this instead of computing
			# the average gradient of the path seeds to make the method
			# independent of the marker radius selected by the simulation
			# configuration. Assuming that dilated_path (the fine strip)
			# is the last one in the list
			dilated_path = path_collection[anchor1][-1]

			# Using the anchor's index as the order in which its segment
			# appears.
			order = anchor_list.index(anchor1)
			cs = ContourStrip(path, dilated_path, order)
			cs.compute_grad_data(self.grad)

			path_collection[anchor1].append(cs)


		sorted_paths = sorted(path_collection,
								key = lambda x:path_collection[x][-1].avg_grad,
								reverse = reverse)

		# Removing the average gradient since it is not necessary anymore.
		for i in path_collection:
			del path_collection[i][-1]

		return sorted_paths, path_collection

#
#	def __path_seeds_on_error__(self, path_seeds):
#		counter = 0
#		for p in path_seeds:
#			if (self.gt[p] > 0 and self.method.label[p] == 0
#				or self.gt[p] == 0 and self.method.label[p] > 0):
#					counter += 1
#
#		if counter >= 0.3*len(path_seeds):
#			print 'Path seeds with error'
#			return True
#
#		print 'Path seeds without error'
#		return False

#	def __next_path_by_grad__(self, anchor_list, sorted_paths):
#		anchor = None
#		ordered_paths = sorted_paths[0]
#		path_collection = sorted_paths[1]
#
#		while self.__next_path < len(ordered_paths):
#			anchor = ordered_paths[self.__next_path]
#			path_seeds = path_collection[anchor][2]
#
#			self.__next_path += 1
#
#			if self.__path_seeds_on_error__(path_seeds):
#				break
#
#		return anchor

	def __next_path_by_grad__(self, anchor_list, sorted_paths):
		anchor = None
		ordered_paths = sorted_paths[0]
		path_collection = sorted_paths[1]

		if self.__next_path < len(ordered_paths):
			anchor = ordered_paths[self.__next_path]

			path_seeds = path_collection[anchor][2]

			self.__next_path += 1

		return anchor

	def __prepare_path_selection_by_error_comps__(self, anchor_list, path_collection):
		self.__used_segments = set()
		# We return path_collection because that's what we need
		# to receive as parameter in next_path_by_error_comps, as opposed
		# to the sorted list of paths by gradient value required by
		# next_path_by_grad.
		return path_collection

	def __next_path_by_error_comps__(self, anchor_list, path_collection):
		strips = {}
		# Computing the path seeds for all the contour segments
		# computed solely by LWOF (or other boundary-based method).
		for anchor1 in path_collection:
			if anchor1 not in self.__used_segments:
				path = path_collection[anchor1][1]
				path_seeds = path_collection[anchor1][2]

				# Using the anchor's index as the order in which its segment
				# appears.
				order = anchor_list.index(anchor1)
				cs = ContourStrip(path, path_seeds, order)

				strips[order] = cs

		# Recomputing the error components map at each iteration.
		self.compute_error_components()

		intersecting_components = self.__contour_intersecting_error_comps__(strips)

		best_anchor = None
		# The error components might not intersect the path seeds at all.
		# We must therefore only select path seeds that intersect error
		# components.
		if len(intersecting_components) > 0:
			best_comp = None
			# Selecting paths in components sorted by non-increasing order of size.
			# Note that since
			for comp in sorted(intersecting_components.itervalues(),
								key=itemgetter(0),
								reverse=True):
				max_path_seeds_len = 0
#				max_path_len = 0

				# Selecting the first path/anchor with the largest
				# number of valid seeds that intersects the error
				# component with largest size.
				for segment in comp[1]:
					anchor1 = anchor_list[segment]
					path_seeds = path_collection[anchor1][2]
#					path = path_collection[anchor1][1]
					npath_seeds = len(path_seeds)
#					path_len = len(path)

					if npath_seeds > max_path_seeds_len:
#					if path_len > max_path_len:
						max_path_seeds_len = npath_seeds
#						max_path_len = path_len
						best_anchor = anchor1


				self.__used_segments.add(best_anchor)

				if best_anchor is not None:
					break

		return best_anchor

	def compute_error_components(self):
			bin_error_components = Image8()
			bin_error_components.from_array(np.zeros(self.method.label.arr.shape,np.uint8))
			error_components(self.method.label, self.gt, bin_error_components)
			self.error_components = label_bin_comp(bin_error_components, 1.5)

	def __selected_strip_limits__(self, selected_strips, contour_ori):
		min_key = min(selected_strips)
		max_key = max(selected_strips)

		limits_first = set()
		limits_last = set()

		# Either the error component might touch *all* strips
		# or one might want to select as limits the first and
		# last strip of the contour. In both cases the
		# condition below is True.
		if len(selected_strips) == len(self.strips):
			if contour_ori == 'cw':
				limits_first.add(0)
				limits_last.add(len(self.strips)-1)
			else:
				limits_first.add(len(self.strips)-1)
				limits_last.add(0)
		else:
			for key in selected_strips:
				next_key = self.strip_index(key,1,contour_ori)
				prev_key = self.strip_index(key,-1,contour_ori)

				q0 = next_key in selected_strips
				q1 = prev_key in selected_strips
				if (q0 and not q1):
					limits_first.add(key)
				elif (q1 and not q0):
					limits_last.add(key)
				elif not q0 and not q1:
					limits_first.add(key)
					limits_last.add(key)

		limits = []
		for first in limits_first:
			key = first
			while key not in limits_last:
				key = self.strip_index(key,1, contour_ori)

			limits.append((first, key))

		return limits

	def first_pixel_from_error_comp(self, component, strips, used_strips,
										contour_ori):

		limits = self.__selected_strip_limits__(component[1], contour_ori)

		# returning the first pixel from the strip as
		# the new anchor point, as well as the first key id
		p = -1
		selected_strip = -1

		for first_key,last_key in limits:
			key = first_key

			counter = 0
			# If first_key == last_key then the
			# error component is only one strip long
			# and two anchor points cannot be selected
			# on it.

			if first_key != last_key:
				while p < 0 and counter < len(strips):
					if key in component[1] and key not in used_strips:
						for i in strips[key].dilated_strip:
							# This test ensures that pixels will
							# be selected as anchor neither on the
							# contour nor on top of another anchor
							# point or
							if self.method.cost[i] > 0:
								p = i
								selected_strip = key

					key = self.strip_index(key,1,contour_ori)
					counter = counter + 1
			if p >= 0:
				break


		return p, selected_strip

	def select_next_anchor(self, anchor_pixel_criterion, strips,
								used_strips, contour_ori):
		if self.method.prev_anchor < 0:
			return None

		intersecting_components = self.__contour_intersecting_error_comps__(strips)

		# Selecting components in non-increasing order of size
		for comp in sorted(intersecting_components.itervalues(),
							key=itemgetter(0),
							reverse=True):

			p, strip, = anchor_pixel_criterion(comp, strips, used_strips, contour_ori)

			if p >= 0:
				return p, strip

		# If no seed candidate is found, return -1
		return -1, -1

	def __contour_intersecting_error_comps__(self, strips):
		if self.error_components is None:
			return None

		intersecting_components = {}
		for key in strips:
			cs = strips[key]
			for p in cs.dilated_strip:
				lb = self.error_components[p]

				if lb > 0:
					if lb not in intersecting_components:
						# initializing the component area to 0
						# and adding 'key' as the first strip
						# intersected by the contour. The
						# component's area will be calculated later.
						intersecting_components[lb] = [0, set()]

						# Storing the key of the strips that
					# intersect the error component lb
					intersecting_components[lb][1].add(key)

		w,h = self.error_components.size()

		# There might be no error components
		if len(intersecting_components) > 0:
			for p in xrange(w*h):
				lb = self.error_components[p]

				# determining the areas of the contour intersecting
				# error components
				if lb in intersecting_components:
					intersecting_components[lb][0] = intersecting_components[lb][0] + 1

		return intersecting_components

