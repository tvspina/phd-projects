from pyift import *
from usis.npimage import *

def usis_image_to_ift_image(usis_img):
	w,h = usis_img.size()

	ift_img = iftCreateImage(w,h,1)

	for p in xrange(ift_img.n):
		ift_img[p] = (int(usis_img[p]), 0, 0)

	return ift_img

def usis_cimage_to_ift_image(usis_cimg):
	w,h  = usis_cimg.size()

	ift_img = iftCreateImage(w,h,1)
	ift_img.load_cbcr()

	for p in xrange(ift_img.n):
		c = iftColor()
		c.set_values(usis_cimg[p])
		ycbcr = iftRGBtoYCbCr(c)
		ift_img[p] = ycbcr.get_values()

	return ift_img

def ift_image_to_usis_cimage(ift_img):
	usis_img = CImage8(size = (ift_img.xsize,ift_img.ysize))

	for i in xrange(usis_img.n):
		usis_img[i] = ift_img[i]

	return usis_img

def ift_image_to_usis_image8(ift_img):
	usis_img = Image8()
	usis_img.initialize(ift_img.xsize, ift_img.ysize)

	for i in xrange(usis_img.n):
		usis_img[i] = ift_img[i][0]

	return usis_img

def ift_image_to_usis_image32(ift_img):
	usis_img = Image32()
	usis_img.initialize(ift_img.xsize, ift_img.ysize)

	for i in xrange(usis_img.n):
		usis_img[i] = ift_img[i][0]

	return usis_img


def seeds_from_image(img, mk_id):
	seeds = {}

	for p in xrange(img.n):
		# Negative values mean unassigned seeds
		if img[p][0] >= 0:
			seeds[p] = (mk_id, img[p][0]) # labels starting from 1

	return seeds

def usis_seeds_to_ift_seeds(seeds):
	S = None
	for s in seeds:
		new_s = iftLabeledSet()
		new_s.elem = s
		new_s.label = seeds[s][1]

		if S is None:
			S = new_s
		else:
			S = S + new_s

	return S
