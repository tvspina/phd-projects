/*
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
*/
// pyift.i: declaration of ift structs and functions for wrapping.
// Read the comments carefully before modifying.

%module pyift
%{
  #include "ift.h"
%}

#ifndef PYIFT_DEBUG
//	#define PYIFT_DEBUG
#endif

// Structs:
// Declare the structs as "typedef struct{...} name;", (not "typedef struct struct_name{...} name;")
// Otherwise, the correct destructor won't be called by swig.
// To extend a struct (define methods for its corresponding python object) use the directive %extend following the examples bellow.
// You should also override the destructor (~structName) to free dynamically allocated memory
// Note: among other things, you can't index an array inside a struct from python. You can work around this by defining struct methods, as bellow.
// Swig reference: http://www.swig.org/Doc1.3/Python.html.
// Python special method names (useful for overriding): http://docs.python.org/3.3/reference/datamodel.html
// Creating python objects: http://www.swig.org/Doc1.3/Python.html#Python_nn57

//Struct iftColor

typedef struct {
  int val[3];
} iftColor;

%extend iftColor{
	void __setitem__(int i, int value){
		if(i >= 0 && i < 3){
			($self)->val[i] = value;
		}
	}

	void set_values(PyObject *tuple){
		iftColor *color = ($self);

		color->val[0] = (int)PyInt_AsLong(PyTuple_GetItem(tuple,0));
		color->val[1] = (int)PyInt_AsLong(PyTuple_GetItem(tuple,1));
		color->val[2] = (int)PyInt_AsLong(PyTuple_GetItem(tuple,2));
	}

	PyObject* get_values(){
		iftColor *color = ($self);
		PyObject *tuple = PyTuple_New(3);

		PyTuple_SetItem(tuple,0,PyInt_FromLong(color->val[0]));
		PyTuple_SetItem(tuple,1,PyInt_FromLong(color->val[1]));
		PyTuple_SetItem(tuple,2,PyInt_FromLong(color->val[2]));

		return tuple;
	}
};

typedef struct{
  int    *val;
  ushort *Cb,*Cr;
  int xsize,ysize,zsize;
  float dx,dy,dz;
  int *tby, *tbz;        // speed-up voxel access tables
  int maxval, minval, n; // minimum and maximum values, and number of voxels
} iftImage;

//Struct iftImage

%extend iftImage{
	PyObject* __getitem__(int pixel){
		iftImage *image = ($self);
		PyObject *color = PyTuple_New(3);

		PyTuple_SetItem(color, 0, PyInt_FromLong(image->val[pixel]));

		if(iftIsColorImage(image)){
			PyTuple_SetItem(color, 1, PyInt_FromLong((long)image->Cb[pixel]));
			PyTuple_SetItem(color, 2, PyInt_FromLong((long)image->Cr[pixel]));
		}else{
			PyTuple_SetItem(color, 1, PyInt_FromLong(0));
			PyTuple_SetItem(color, 2, PyInt_FromLong(0));
		}

		return color;
	}

	void __setitem__(int pixel, PyObject* color){
		iftImage *image = ($self);

		image->val[pixel] = (int)PyInt_AsLong(PyTuple_GetItem(color,0));
		if(iftIsColorImage(image)){
			image->Cb[pixel] = (ushort)PyInt_AsLong(PyTuple_GetItem(color,1));
			image->Cr[pixel] = (ushort)PyInt_AsLong(PyTuple_GetItem(color,2));
		}
	}

	void load_cbcr(){
		iftImage *s = ($self);
		iftSetCbCr(s, 0);
	}

	int get_voxel_index(iftVoxel* voxel){
		iftImage *s = ($self);
		iftVoxel v = *voxel;
		return iftGetVoxelIndex(s,v);
	}

	iftVoxel get_voxel(int p){
		iftVoxel v = iftGetVoxelCoord(($self),p);
		return v;
	}

	~iftImage(){
		iftImage *ptr = ($self);
		iftDestroyImage(&ptr);

		#ifdef PYIFT_DEBUG
			printf("iftImage deleted\n");
		#endif
	}
}

//Struct iftAdjRel

typedef struct{
  int *dx,*dy,*dz; /* displacements to achieve the n adjacent voxels. */
  int  n; /* number of adjacent voxels. */
} iftAdjRel;

%extend iftAdjRel{
	PyObject* __getitem__(int i){
		PyObject *displacement = PyTuple_New(3);

		PyTuple_SetItem(displacement,0, PyInt_FromLong(($self)->dx[i]));
		PyTuple_SetItem(displacement,1, PyInt_FromLong(($self)->dy[i]));
		PyTuple_SetItem(displacement,2, PyInt_FromLong(($self)->dz[i]));

		return displacement;
	}

	~iftAdjRel(){
		iftAdjRel *ptr = ($self);
		iftDestroyAdjRel(&ptr);

		#ifdef PYIFT_DEBUG
			printf("iftAdjRel deleted\n");
		#endif
	}
}


//Struct iftLabeledSet

typedef struct {
  int elem;
  int label;
  struct ift_labeledset *next;
} iftLabeledSet;

%extend iftLabeledSet{
	~iftLabeledSet(){
		iftLabeledSet *ptr = ($self);
		iftDestroyLabeledSet(&ptr);

		#ifdef PYIFT_DEBUG
			printf("iftLabeledSet deleted\n");
		#endif
	}

	iftLabeledSet* __add__(iftLabeledSet* lset){
		iftLabeledSet *copy = iftCopyOrderedLabeledSet(($self));
		iftConcatLabeledSet(&copy,&lset);
		return copy;
	}


	iftLabeledSet* __sub__(iftLabeledSet* lset){
		iftLabeledSet *copy = iftCopyOrderedLabeledSet(($self));
		iftRemoveSubsetLabeledSet(&copy,&lset);
		return copy;
	}

	PyObject* to_dict(){
		iftLabeledSet* s = ($self);

		PyObject *dict = PyDict_New();
		while(s != NULL){
			PyDict_SetItem(dict, PyInt_FromLong(s->elem), PyInt_FromLong(s->label));
			s = s->next;
		}

		return dict;
	}

	void print_seeds(){
		iftLabeledSet *ptr = ($self);

		while(ptr != NULL){
			printf("%d,%d\n",ptr->elem,ptr->label);
			ptr = ptr->next;
		}
	}
}



//Struct iftVoxel

typedef struct {
  int x,y,z;
} iftVoxel;


typedef struct {
  char *val;
  int   n, nbytes;
} iftBMap;

%extend iftBMap{
	~iftBMap(){
		iftBMap *ptr = ($self);

		iftDestroyBMap(&ptr);
		#ifdef PYIFT_DEBUG
			printf("iftBMap deleted\n");
		#endif
	}
};




typedef struct {
  int elem;
  struct ift_set *next;
} iftSet;

%extend iftSet{
	PyObject* to_list(){
		iftSet* s = ($self);

		PyObject *list = PyList_New(iftSetSize(s));
		int i = 0;
		while(s != NULL){
			PyList_SetItem(list, i, PyInt_FromLong(s->elem));
			s = s->next;
			i++;
		}

		return list;
	}

	~iftSet(){
		iftSet *ptr = ($self);

		iftDestroySet(&ptr);
		#ifdef PYIFT_DEBUG
			printf("iftSet deleted\n");
		#endif
	}
};


// Functions:
// If your function returns a dynamically allocated object that should be deleted when its respective python object
// is deleted, insert the directive %newobject functionName above it.

int iftXSize(iftImage *img);
int iftYSize(iftImage *img);
int iftZSize(iftImage *img);

%newobject iftCreateBMap;
iftBMap  *iftCreateBMap(int n);

void      iftDestroyBMap(iftBMap **b);

%newobject iftCreateImage;
iftImage *iftCreateImage(int xsize,int ysize,int zsize);

%newobject iftCircular;
iftAdjRel *iftCircular(float r);

%newobject iftCopyImage;
iftImage  *iftCopyImage(iftImage *img);

%newobject iftRGBtoYCbCr;
iftColor iftRGBtoYCbCr(iftColor cin);

%newobject iftYCbCrtoRGB;
iftColor iftYCbCrtoRGB(iftColor cin);



%newobject iftGeodesicMarkersForSegmentation;
iftLabeledSet *iftGeodesicMarkersForSegmentation(iftImage *gt_image, iftImage *classification_image);

%newobject iftBorderMarkersForPixelSegmentation;
iftLabeledSet *iftBorderMarkersForPixelSegmentation(iftImage *grad_image, iftImage *gt_image, float border_distance);

%newobject iftGeodesicCenters;
iftLabeledSet* iftGeodesicCenters(iftImage* label_image);

int       iftMaximumValue(iftImage *img);
int       iftMinimumValue(iftImage *img);


int iftMarkersFromMisclassifiedSeeds(iftImage* seed_image, iftLabeledSet* all_seeds, iftBMap* used_seeds, int nseeds, int number_of_objects, iftImage* gt_image, iftImage* cl_image, int dist_border, int max_marker_radius, int min_marker_radius);
