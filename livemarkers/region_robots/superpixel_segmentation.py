from pyift import *
from usis.npimage import *
from new_ift_interface import *

class SuperpixelIFT:
	def __init__(self, grad, cimg, spatial_radius, volume_threshold):
		self.method_type = 'region'

		basins = usis_image_to_ift_image(grad)
		self.marker = iftVolumeClose(basins, volume_threshold)

		self.label = Image8(size = grad.size())
		self.grad = grad
		self.image = usis_cimage_to_ift_image(cimg)

		adj_relation = iftCircular(spatial_radius);
		self.label_image = iftWaterGray(basins,self.marker,adj_relation)
		self.regions = ift_image_to_usis_image32(self.label_image)

	def run(self, seeds, del_seeds):

		dataset = iftSupervoxelsToDataSet(self.image, self.label_image)
		if dataset.nfeats == 3:
			dataset.set_alpha([0.2,1.0,1.0])

		adj_relation = iftCircular(1)
		region_graph = iftRegionGraphFromLabelImage(self.label_image,dataset,
													adj_relation)
		S = usis_seeds_to_ift_seeds(seeds)
		iftSuperpixelClassification(region_graph, self.label_image, S);

		classification_image = iftCreateImage(self.image.xsize,self.image.ysize,
												self.image.zsize)
		for p in range(0,classification_image.n):
			pixel_label = self.label_image[p][0] - 1
			color = dataset.get_sample_label(pixel_label)
			classification_image[p] = (color, 0, 0)

		iftWriteSeedsOnImage(classification_image,S)

		self.label = ift_image_to_usis_image8(classification_image)

		for p in xrange(self.label.n):
			self.label[p] = max(self.label[p]-1,0)

	def __repr__(self):
		return "superpixel_ift"

	def reset(self):
		self.label.arr[:] = 0
