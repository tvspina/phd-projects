# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
import os
from pyift import *
from usis.segmentation import *
from usis.npimage import *
from usis.gradient import *

from new_ift_interface import *

import numpy as np, copy
import robot_experiments as re

class RegionRobot:
	def __init__(self, img, gt, grad, method, method_config,
					simulation_data):
		self.orig_img = img
		self.img = img.copy()
		self.gt = gt

		self.grad = grad

		self.method = method
		self.method_config = method_config

		self.error_components = None

		self.simulation_data = simulation_data

	# Geodesic robot methods
	def create_geodesic_seeds(self, gt_image, grad):
		return None

	def geodesic_seeds(self, gt_image, segmentation, seed_image,
						seeds_bitmap, available_seeds):
		available_seeds = iftGeodesicMarkersForSegmentation(gt_image, segmentation)
		nadded_markers = iftMarkersFromMisclassifiedSeeds(seed_image,available_seeds,
														seeds_bitmap, 2,#Binary segmentation
														self.simulation_config['seeds_per_iteration'],
														gt_image,segmentation,
														self.simulation_config['safe_distance'],
														self.simulation_config['max_marker'],
														self.simulation_config['min_marker'])
		return available_seeds, nadded_markers

	# Robot based on low gradient choice of markers
	def create_low_grad_seeds(self, gt_image, grad):
		# IMPORTANT: basins' values are changed in the process
		basins = usis_image_to_ift_image(grad)
		return iftBorderMarkersForPixelSegmentation(basins,gt_image,
        											self.simulation_config['border_distance'])


	def misclassified_seeds(self, gt_image, segmentation, seed_image,
								seeds_bitmap, available_seeds):
		nadded_markers = iftMarkersFromMisclassifiedSeeds(seed_image, available_seeds,
															seeds_bitmap, 2,#Binary segmentation
															self.simulation_config['seeds_per_iteration'],
															gt_image, segmentation,
															self.simulation_config['safe_distance'],
															self.simulation_config['max_marker'],
															self.simulation_config['min_marker'])

		return available_seeds, nadded_markers
#
#	def create_superpixels_seeds(self, gt_image, grad):
#		adj_relation = iftCircular(1.5)
#		basins = usis_image_to_ift_image(grad)
#		marker = iftVolumeClose(basins,self.method_config['volume_threshold'])
#		label_image = iftWaterGray(basins,marker,adj_relation)
#		image = usis_cimage_to_ift_image(self.img)
#		dataset = iftSupervoxelsToDataSet(image, label_image)
#		if dataset.nfeats == 3:
#			dataset.set_alpha([0.2,1.0,1.0])
#
#		return iftBorderMarkersForSuperpixelSegmentation(label_image,
#														gt_image, dataset)

	# Initializing simulation data
	def init_simulation(self, contour_index, simulation_config):
		print '\nInitializing simulation\n'

		self.simulation_config = simulation_config

		self.method.reset()


	def run_simulation(self, img_name, simulation_config, gui = None):
		print 'Simulating for method ' + str(self.method)

		self.simulation_config = copy.deepcopy(simulation_config) # making a deep copy of the configuration

		stop_criterion = self.simulation_config['stop_criterion']
		if stop_criterion == 'niterations':
			stop_criterion_fun = self.__stop_by_number_of_iterations__
		else:
			stop_criterion_fun = self.__stop_by_accuracy__

		marker_choice = self.simulation_config['marker_choice']
		if (marker_choice == 'markers_on_low_grad'):
			create_seeds_fun = self.create_low_grad_seeds
			next_seeds_fun = self.misclassified_seeds
		elif marker_choice == 'error_component':
			create_seeds_fun = self.create_geodesic_seeds
			next_seeds_fun = self.geodesic_seeds
#		elif marker_choice == 'superpixel':
#			create_seeds_fun = self.create_superpixels_seeds
#			next_seeds_fun = self.misclassified_seeds
		else:
			print 'Error! Marker choice of \'%s\' invalid for %s' % (marker_choice,
																	self.method)
			exit()

		accuracy_measure = self.simulation_config['accuracy_measure']

		if stop_criterion == 'accuracy':
			if 'common_accuracy_thresh' not in self.simulation_config:
				print 'Error!! Accuracy threshold not determined for %s' % self.method
				exit()
			else:
				self.simulation_config['accuracy_thresh'] = self.simulation_config['common_accuracy_thresh']

		niterations	= 0
		number_of_markers,_ = self.simulate(img_name, stop_criterion_fun,
											accuracy_measure, niterations,
											create_seeds_fun,
											next_seeds_fun, gui = gui)

		self.simulation_data.add_interaction_amount(img_name, number_of_markers)
		self.simulation_data.add_control_measures(img_name)

		return None


	def simulate(self, img_name, stop_criterion_fun, accuracy_measure,
					niterations, create_seeds_fun, next_seeds_fun,
					gui = None):
		primary_label = self.method_config['primary_label']
		left_pixel_label = self.method_config['left_pixel_label']
		right_pixel_label = self.method_config['right_pixel_label']

		gt_image = usis_image_to_ift_image(self.gt)
		segmentation = None
		seeds_bitmap = iftCreateBMap(gt_image.n)

		available_seeds = create_seeds_fun(gt_image, self.grad)
		seed_image = iftCreateImage(gt_image.xsize,gt_image.ysize, gt_image.zsize)

		for p in xrange(seed_image.n):
			seed_image[p] = (-1,0,0)

		number_of_markers = 0

		available_seeds, nadded_markers = next_seeds_fun(gt_image, segmentation,
														seed_image,
														seeds_bitmap,
														available_seeds)
		seeds = seeds_from_image(seed_image, 1)

		prev_seeds = {}

		self.gui_show(gui, self.method.label, seeds, sleep = 3)

		if len(seeds) == 0:
			raise DelineationError('No initial seeds computed for %s on region robot.' % self.method)

		# While new seeds are added and the stop criterion wasn't met
		while(len(prev_seeds) != len(seeds)
			and not stop_criterion_fun(img_name, niterations)):

			segmentation = self.segment(seeds)
			# Computing the accuracy for the current segmentation.
			accuracy = self.simulation_data.add_accuracy(img_name,
														self.method.label,
														self.gt, niterations,
														accuracy_measure)

			number_of_markers = number_of_markers + nadded_markers

			if self.simulation_config['verbose']:
				print '%s for %s: %f num markers %d' % (accuracy_measure,
														self.method,
														accuracy,
														number_of_markers)


			self.simulation_data.add_label(img_name, self.method.label,
											niterations)
			self.gui_show(gui, self.method.label, seeds, sleep = 1)
			self.gui_save_figures(gui, img_name, niterations)

			# Preparing for next iteration
			prev_seeds = seeds

			available_seeds, nadded_markers = next_seeds_fun(gt_image,
															segmentation,
															seed_image,
															seeds_bitmap,
															available_seeds)
			seeds = seeds_from_image(seed_image, niterations)
			niterations = niterations + 1

		return number_of_markers, niterations

	def segment(self, seeds):
		# The method considers seeds starting from 0,
		# while the robot produces seeds starting from 1
		self.method.run(seeds, None)

		ift_img = usis_image_to_ift_image(self.method.label)

		return ift_img


	# Stop criteria must return
	def __stop_by_accuracy__(self, img_name, iteration):
		stop = False
		accuracy_thresh = self.simulation_config['accuracy_thresh']
		acc_discount = None

		accuracy_discount = 0.0
		if 'accuracy_discount' in self.simulation_config:
			accuracy_discount = self.simulation_config['accuracy_discount']

		# Since we are testing the accuracy of the previous iteration
		# *before* allowing the current iteration to proceed, we must test
		# the accuracy of iteration - 1. Note that the iteration counter
		# starts from 0.
		if iteration-1 in self.simulation_data.accuracy[img_name]:
			acc = self.simulation_data.accuracy[img_name][iteration-1]

			if self.simulation_config['accuracy_measure'] == 'f-measure':
				stop = acc >= accuracy_thresh - accuracy_discount
			else:
				stop = acc <= accuracy_thresh + accuracy_discount


		return stop

	def __stop_by_number_of_iterations__(self, img_name, iteration):
		iteration_thresh = self.simulation_config['iteration_thresh']

		# Iterations start from 0.
		return iteration >= iteration_thresh

	def gui_show(self, gui, primary_label,
						path_seeds,
						color = (1.0,1.0,0.0),
						sleep = None):
		# GUI operations
		if gui is not None:
			if primary_label is not None:
				gui.display_overlayed_data([], path_seeds)
			else:
				gui.show_motion(path, c = color)

		if sleep is not None:
			time.sleep(sleep)


	def gui_save_figures(self, gui, img_name, fig_id, prefix=''):
		if (gui is not None and
			(self.simulation_config['save_fig'] or self.simulation_config['save_video'])):

			img_key = os.path.split(img_name)[1]
			img_key = os.path.splitext(img_key)[0]
			gui.savefig(fig_id, prefix = '%s_%s_%s' %(self.method, img_key, prefix))

