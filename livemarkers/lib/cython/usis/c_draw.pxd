from libcpp cimport bool

from c_common cimport *
from image cimport *

cdef extern from "usisdraw.h" namespace "usis":
	void blend_draw_image(Image8 *img, Image8 *draw, Image8 *alpha)
	void blend_draw_cimage(CImage8 *cimg, CImage8 *cdraw, Image8 *alpha)

	void blend_draw_sub_image(Image8 *img, Image8 *sub, Image8 *alpha, int x, int y)
	void blend_draw_sub_image(Image8 *bkg, Image8 *fg, Image8 *alpha, int x, int y)

	# The alpha image should have the same size as the subimage
	void blend_draw_sub_cimage(CImage8 *cimg, CImage8 *csub, Image8 *alpha, int x, int y)

	void blend_draw_sub_cimage(CImage8 *bkg, CImage8 *fg, Image8 *alpha, int x, int y)

	void paste_image(Image8 *dst, Image8 *src, int x, int y)
	void paste_image(CImage8 *bkg, CImage8 *fg, int x, int y)


	void draw_circumference(CImage8 *draw, int c, float r, int color)

	void draw_circumference(Image8 *draw, int c, float r, uchar color)

	void draw_cimage_circle(CImage8 *draw, int c, float r, int color)
	void draw_image_circle(Image8 *draw, int c, float r, uchar color)

	void draw_line(Image8 *img, int x1, int y1, int xn, int yn,
						uchar val, float thickness)

	void draw_line(Image8 *img, int x1, int y1, double r, double theta,
						uchar val, float thickness)

	void draw_line(CImage8 *img, int x1, int y1, int xn, int yn,
						int color, float thickness)
	void draw_line(CImage8 *img, int x1, int y1, double r, double theta,
						int color, float thickness)

	void draw_box(Image8 *img, int x1, int y1, int xn, int yn,
						uchar val, float thickness)

	void draw_box(int val, float thickness)

	void draw_box(CImage8 *img, int x1, int y1, int xn, int yn,
                     int val, float thickness)

	void draw_image_arrow(CImage8 *draw,
                          int p, int q,
                          float r, float w, float h,
                          int val)

	void draw_image_arrow(Image8 *draw,
                          int p, int q,
                          float r, float w, float h,
                          int val)

