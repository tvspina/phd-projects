cimport image
cimport c_common
cimport c_evaluation
cimport c_geometry
from morphology import *
from npimage cimport *

cpdef compute_fmeasure(Image8 label, Image8 mask):
	cdef int tp, tn, fp, fn
	cdef double fm
	cdef c_geometry.Rectangle bb = c_geometry.Rectangle()

	c_evaluation.conf_matrix(label.img, mask.img, &tp, &tn, &fp, &fn,
							c_common.NIL, c_common.NIL, bb)
	fm = c_evaluation.fmeasure(c_evaluation.precision(tp, fp),
								c_evaluation.recall(tp, fn))

	return fm, tp, tn, fp, fn

# Hamming distance normalized by the ground truth area
cpdef double compute_hamming_dist_norm_by_gt(Image8 label, Image8 mask):
	cdef int i
	cdef double dist = 0, gt_area = 0

	for i in xrange(label.img.n):
		if ((label[i] > 0 and mask[i] == 0) or
			(label[i] == 0 and mask[i] > 0)):

			dist = dist + 1.0
		if mask[i] > 0:
			gt_area = gt_area + 1

	return dist/gt_area

## Boundary-based accuracy measures

# Given a label and a groundtruth, computes the mean squared distance
# between the groundtruth border and the object's segmentation border,
# thus representing the mean squared error (positive and negative errors
# depending on the EDT sign).
# It also computes the maximum positive and negative distances.
cpdef compute_sgn_edt_error_measures(Image8 label, Image8 gt):
	cdef float max_dist_pos, max_dist_neg, fn_dist_avg, fp_dist_avg
	cdef float fn_dist_sum, fp_dist_sum, boundary_error
	cdef int fn_size, fp_size, border_size

	c_evaluation.sgn_edt_error_measures(label.img, gt.img, &max_dist_pos, &max_dist_neg,
											&fp_dist_sum, &fn_dist_sum, &fp_size, &fn_size,
											&border_size)
	fp_dist_avg = fp_dist_sum/max(1.0, fp_size)
	fn_dist_avg = fn_dist_sum/max(1.0, fn_size)

	# The total boundary error is the sum of false positives and negatives
	# divided by the total length of the segmentation boundary
	boundary_error = (fp_dist_sum + fn_dist_sum)/max(1.0,border_size)

	return (boundary_error, fp_dist_avg, fn_dist_avg,
			fp_size, fn_size, border_size,
			max_dist_pos, max_dist_neg)

## Controllability measures

cpdef compute_control_measures(labels):
	cdef Image32 changes
	cdef int n_changed_pixels, n_lb_changes
	cdef int max_nchanges, k, npixels, ncols, nrows

	changes = count_label_changes(labels)

	# storing the kth iteration
	k = len(labels)

	return controllability_measures(changes, k)

cpdef count_label_changes(labels):
	cdef int ncols, nrows, i, p
	cdef Image32 changes
	cdef Image8 next_label, final_label, label

	iterations = sorted(labels.viewkeys())

	final_label = labels[iterations[-1]]

	changes = Image32(size = final_label.size())

	for i in xrange(len(iterations)-1):
		label = labels[iterations[i]]
		next_label = labels[iterations[i+1]]
		for p in xrange(changes.img.n):
			if(label[p] == final_label[p] and
				label[p] != next_label[p]):
				changes[p] += 1

	return changes

cpdef controllability_measures(Image32 changes, int niterations):
	cdef float avg_changes, avg_changed_pixels
	cdef float avg_changes_by_changed_pixels
	cdef float controllability
	cdef int nchanges, nchanged_pixels

	c_evaluation.controllability_measures(changes.img, niterations,
											&avg_changes, &avg_changed_pixels,
											&avg_changes_by_changed_pixels,
											&controllability, &nchanges,
											&nchanged_pixels)

	return (controllability, nchanges, nchanged_pixels,
			avg_changes, avg_changed_pixels,
			avg_changes_by_changed_pixels)
