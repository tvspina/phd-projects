from libcpp cimport bool

cdef extern from "<climits>":

	int INT_MIN
	int INT_MAX
	float FLT_MAX
	double DBL_MAX
	unsigned char UCHAR_MAX

cdef extern from "common.h":

	float USIS_FINFTY
	double USIS_DINFTY

	enum:
		WHITE = 0
		IFT_GRAY,
		IFT_BLACK

	ctypedef unsigned short ushort
	ctypedef unsigned int uint
	ctypedef unsigned char uchar


	int NIL
	int    *AllocIntArray(int n)   # It allocates 1D array of n integers
	float  *AllocFloatArray(int n) # It allocates 1D array of n floats
	char   *AllocCharArray(int n)  # It allocates 1D array of n characters
	uchar  *AllocUCharArray(int n)  # It allocates 1D array of n characters
	ushort *AllocUShortArray(int n)  # It allocates 1D array of n ushorts
	uint   *AllocUIntArray(int n) # It allocates 1D array of n uints
	double *AllocDoubleArray(int n) # It allocates 1D array of n doubles

	void* random_seed()
	void destroy_random_generator(void **generator)

cdef extern from "common.h":
	struct Pixel:
		int x, y

	cppclass Rectangle:
		Pixel beg
		Pixel end

		Rectangle()
		Rectangle(int x0, int y0, int xf, int yf)
		Rectangle(Pixel px0, Pixel pxf)

		bool InRectangle(Pixel px)
