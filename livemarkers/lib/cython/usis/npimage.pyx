from libcpp cimport bool
from usis.c_common cimport *
from usis.color cimport *
cimport usis.image
from npimage cimport *

import numpy as np
cimport numpy as np
import scipy as sp
import scipy.misc

import os

cimport cython

import commands
import tempfile
import pylab as pl
import math

from cpython cimport PyObject, Py_INCREF

np.import_array()

cdef class UcharArrayWrapper:

	cdef set_data(self, int shape, void* data_ptr,
					bool own_data):
		""" Set the data of the array

		This cannot be done in the constructor as it must recieve C-level
		arguments.

		Parameters:
		-----------
		size: int
			Length of the array.
		data_ptr: void*
			Pointer to the data
		own_data:
			If True, then data_ptr is freed
			when deallocing the memory for it belongs to this
			class
		"""
		self.data_ptr = data_ptr
		self.shape = shape
		self.own_data = own_data


	def __array__(self):
		""" Here we use the __array__ method, that is called when numpy
			tries to get an array from the object."""

		cdef np.npy_intp shape[1]

		shape[0] = <np.npy_intp> self.shape


		# Create a ND array, of length 'size'
		ndarray = np.PyArray_SimpleNewFromData(1, shape,
											   np.NPY_UINT8, self.data_ptr)
		return ndarray

	def __dealloc__(self):

		if self.own_data:
			free(<void*>self.data_ptr)

cdef class UcharArray2DWrapper:

	cdef set_data(self, tuple shape, void* data_ptr,
					bool own_data):
		""" Set the data of the array

		This cannot be done in the constructor as it must recieve C-level
		arguments.

		Parameters:
		-----------
		size: int
			Length of the array.
		data_ptr: void*
			Pointer to the data
		own_data:
			If True, then data_ptr is freed
			when deallocing the memory for it belongs to this
			class
		"""
		self.data_ptr = data_ptr
		self.shape = shape
		self.own_data = own_data


	def __array__(self):
		""" Here we use the __array__ method, that is called when numpy
			tries to get an array from the object."""

		cdef np.npy_intp shape[2]

		shape[0] = <np.npy_intp> self.shape[0]
		shape[1] = <np.npy_intp> self.shape[1]

		# Create a ND array, of length 'size'
		ndarray = np.PyArray_SimpleNewFromData(2, shape,
											   np.NPY_UINT8, self.data_ptr)
		return ndarray

	def __dealloc__(self):

		if self.own_data:
			free(<void*>self.data_ptr)

cdef class IntArrayWrapper:

	cdef set_data(self, int shape, void* data_ptr,
					bool own_data):

		self.data_ptr = data_ptr
		self.shape = shape
		self.own_data = own_data


	def __array__(self):

		cdef np.npy_intp shape[1]

		shape[0] = <np.npy_intp> self.shape


		# Create a ND array, of length 'size'
		ndarray = np.PyArray_SimpleNewFromData(1, shape,
											   np.NPY_INT, self.data_ptr)
		return ndarray

	def __dealloc__(self):

		if self.own_data:
			free(<void*>self.data_ptr)

cdef class IntArray2DWrapper:

	cdef set_data(self, tuple shape, void* data_ptr,
					bool own_data):

		self.data_ptr = data_ptr
		self.shape = shape
		self.own_data = own_data


	def __array__(self):

		cdef np.npy_intp shape[2]

		shape[0] = <np.npy_intp> self.shape[0]
		shape[1] = <np.npy_intp> self.shape[1]

		# Create a ND array, of length 'size'
		ndarray = np.PyArray_SimpleNewFromData(2, shape,
											   np.NPY_INT, self.data_ptr)
		return ndarray

	def __dealloc__(self):

		if self.own_data:
			free(<void*>self.data_ptr)

cdef class DbArrayWrapper:

	cdef set_data(self, int shape, void* data_ptr,
					bool own_data):

		self.data_ptr = data_ptr
		self.shape = shape
		self.own_data = own_data


	def __array__(self):

		cdef np.npy_intp shape[1]

		shape[0] = <np.npy_intp> self.shape


		# Create a ND array, of length 'size'
		ndarray = np.PyArray_SimpleNewFromData(1, shape,
											   np.NPY_DOUBLE, self.data_ptr)
		return ndarray

	def __dealloc__(self):

		if self.own_data:
			free(<void*>self.data_ptr)

cdef class DbArray2DWrapper:

	cdef set_data(self, tuple shape, void* data_ptr,
					bool own_data):

		self.data_ptr = data_ptr
		self.shape = shape
		self.own_data = own_data


	def __array__(self):

		cdef np.npy_intp shape[2]

		shape[0] = <np.npy_intp> self.shape[0]
		shape[1] = <np.npy_intp> self.shape[1]

		# Create a ND array, of length 'size'
		ndarray = np.PyArray_SimpleNewFromData(2, shape,
											   np.NPY_DOUBLE, self.data_ptr)
		return ndarray

	def __dealloc__(self):

		if self.own_data:
			free(<void*>self.data_ptr)


cdef np.ndarray from_uchar_array(shape, uchar *data,
									bool own_data):
	cdef UcharArray2DWrapper array2d_wrapper
	cdef UcharArrayWrapper array_wrapper
	cdef np.ndarray arr

	if type(shape) == tuple:
		array2d_wrapper = UcharArray2DWrapper()

		""" IMPORTANT!!: The image is freed either by this class or by the
		calling third party. Therefore, the array wrapper classes should NOT
		free anything. """
		array2d_wrapper.set_data(shape, <void*> data, own_data)
		arr = np.array(array2d_wrapper, copy=False)
		# Assign our object to the 'base' of the ndarray object
		arr.base = <PyObject*> array2d_wrapper
		# Increment the reference count, as the above assignement was done in
		# C, and Python does not know that there is this additional reference
		Py_INCREF(array2d_wrapper)
	else:
		array_wrapper = UcharArrayWrapper()

		""" IMPORTANT!!: The image is freed either by this class or by the
		calling third party. Therefore, the array wrapper classes should NOT
		free anything. """
		array_wrapper.set_data(shape, <void*> data, own_data)
		arr = np.array(array_wrapper, copy=False)
		# Assign our object to the 'base' of the ndarray object
		arr.base = <PyObject*> array_wrapper
		# Increment the reference count, as the above assignement was done in
		# C, and Python does not know that there is this additional reference
		Py_INCREF(array_wrapper)

	return arr

cdef np.ndarray from_int_array(shape, int *data,
									bool own_data):
	cdef IntArray2DWrapper array2d_wrapper
	cdef IntArrayWrapper array_wrapper
	cdef np.ndarray arr

	if type(shape) == tuple:
		array2d_wrapper = IntArray2DWrapper()

		""" IMPORTANT!!: The image is freed either by this class or by the
		calling third party. Therefore, the array wrapper classes should NOT
		free anything. """
		array2d_wrapper.set_data(shape, <void*> data, own_data)
		arr = np.array(array2d_wrapper, copy=False)
		# Assign our object to the 'base' of the ndarray object
		arr.base = <PyObject*> array2d_wrapper
		# Increment the reference count, as the above assignement was done in
		# C, and Python does not know that there is this additional reference
		Py_INCREF(array2d_wrapper)
	else:
		array_wrapper = IntArrayWrapper()

		""" IMPORTANT!!: The image is freed either by this class or by the
		calling third party. Therefore, the array wrapper classes should NOT
		free anything. """
		array_wrapper.set_data(shape, <void*> data, own_data)
		arr = np.array(array_wrapper, copy=False)
		# Assign our object to the 'base' of the ndarray object
		arr.base = <PyObject*> array_wrapper
		# Increment the reference count, as the above assignement was done in
		# C, and Python does not know that there is this additional reference
		Py_INCREF(array_wrapper)

	return arr

cdef np.ndarray from_double_array(shape, double *data,
									bool own_data):
	cdef DbArray2DWrapper array2d_wrapper
	cdef DbArrayWrapper array_wrapper
	cdef np.ndarray arr

	if type(shape) == tuple:
		array2d_wrapper = DbArray2DWrapper()

		""" IMPORTANT!!: The image is freed either by this class or by the
		calling third party. Therefore, the array wrapper classes should NOT
		free anything. """
		array2d_wrapper.set_data(shape, <void*> data, own_data)
		arr = np.array(array2d_wrapper, copy=False)
		# Assign our object to the 'base' of the ndarray object
		arr.base = <PyObject*> array2d_wrapper
		# Increment the reference count, as the above assignement was done in
		# C, and Python does not know that there is this additional reference
		Py_INCREF(array2d_wrapper)
	else:
		array_wrapper = DbArrayWrapper()

		""" IMPORTANT!!: The image is freed either by this class or by the
		calling third party. Therefore, the array wrapper classes should NOT
		free anything. """
		array_wrapper.set_data(shape, <void*> data, own_data)
		arr = np.array(array_wrapper, copy=False)
		# Assign our object to the 'base' of the ndarray object
		arr.base = <PyObject*> array_wrapper
		# Increment the reference count, as the above assignement was done in
		# C, and Python does not know that there is this additional reference
		Py_INCREF(array_wrapper)

	return arr

cdef class Image8:
	def __cinit__(self, **kwargs):
		self.img = NULL
		self.arr = None
		self._it = 0

		if 'size' in kwargs:
			self.initialize(*kwargs['size'])
		elif 'filename' in kwargs:
			self.from_file(kwargs['filename'])

	def __getitem__(self,i):
		return self.img.val[i]

	def __setitem__(self,i,val):
		self.img.val[i] = val

	def __dealloc__(self):
		self.__free__()

	def __iter__(self):
		self._it = 0
		return self

	def __next__(self):
		if self._it >= self.n:
			raise StopIteration
		else:
			v = self[self._it]
			self._it = self._it + 1
			return v

	def size(self):
		return (self.img.ncols, self.img.nrows)

	@property
	def n(self):
		return self.img.n

	cpdef topixel(self, int p):
		cdef Pixel px
		px = usis.image.to_pixel(self.img, p)

		return np.array([px.x, px.y],np.int)

	cpdef valid_pixel(self, xy):
		cdef int x = int(xy[0]), y = int(xy[1])
		return usis.image.valid_pixel(self.img, x,y)

	cpdef toindex(self, xy):
		cdef int x,y
		x,y = xy[0],xy[1]
		return usis.image.to_index(self.img, x,y)

	cdef void __free__(self):
		if self.img is not NULL:
			if self.__created_from_img__:
				if self.__own_image__:
					usis.image.destroy_image(&self.img)
			else:
				if self.img is not NULL:
					self.img.val = NULL
					free(self.img)
					self.img = NULL

	cpdef initialize(self, int ncols, int nrows):
		cdef usis.image.Image8 *img = usis.image.create_image8(ncols, nrows)
		self.from_image(img, True)

	cpdef from_file(self, filename):
		cdef bytes_filename = bytes(filename)

		self.__free__()

		if filename.lower().rfind(".pgm") >= 0:
			self.from_image(usis.image.read_image8(<char*>bytes_filename), True)
		else:
			img = pl.imread(filename)

			h,w = img.shape

			img8bits = None

			if img.dtype == np.float32:
				# Converting the image to uchar,
				# since pylab reads pngs as float
				img8bits = np.zeros(img.shape,np.uint8)

			if img8bits is None:
				# Numpy reads the image upside down to
				# fit with the array coordinate system.
				# We have to take this into account
				# by rearranging the image properly.
				for y in xrange(int(math.floor(h/2))):
					for x in xrange(w):
						tmp = img[y,x].copy()
						img[y,x] = img[h-y-1,x].copy()
						img[h-y-1,x] = tmp
				self.from_array(img)
			else:
				# Converting from float32 to 8bit unsigned int.
				# Pylab reads PNG images using the proper coordinate
				# system. Hence, there's no need to do the rearranging
				# above.
				for y in xrange(h):
					for x in xrange(w):
						img8bits[y,x] = 255*img[y,x]

				self.from_array(img8bits)

	cpdef write(self, filename):
		if os.path.splitext(filename)[1].lower() == '.pgm':
			usis.image.write_image(self.img, <char*>filename)
		else:
			scipy.misc.imsave(filename, self.arr)

	cpdef from_array(self, np.ndarray[np.uint8_t, ndim=2, mode='c'] array):
		self.__free__()

		self.img = <usis.image.Image8*>calloc(1, sizeof(usis.image.Image8))
		self.arr = array

		self.__created_from_img__ = False

		self.img.ncols = self.arr.shape[1]
		self.img.nrows = self.arr.shape[0]

		self.img.val = <unsigned char*>self.arr.data


	cdef from_image(self, usis.image.Image8 *img, bool own_image):
		self.__free__()

		self.img = img

		self.__created_from_img__ = True
		self.__own_image__ = own_image

		self.arr = from_uchar_array((img.nrows, img.ncols),
									img.val, False)
	def copy(self):
		cdef Image8 new_img = Image8()

		new_img.from_image(usis.image.copy_image8(self.img), True)

		return new_img

	def __reduce__(self):
		cdef np.ndarray[np.uint8_t, ndim=2, mode='c'] arr_cpy

		arr_cpy = self.arr.copy()

		return Image8_unpickle, (arr_cpy,) # single element tuple

	cpdef add_frame(self, int sz, unsigned char value):
		cdef Image8 frame = Image8()

		frame.from_image(usis.image.add_frame(self.img, sz, value), True)

		return frame

def Image8_unpickle(*args):
	img = Image8()

	img.from_array(args[0])

	return img

cdef class Image32:
	def __cinit__(self, **kwargs):
		self.img = NULL
		self.arr = None
		self._it = 0

		if 'size' in kwargs:
			self.initialize(*kwargs['size'])
		elif 'filename' in kwargs:
			self.from_file(kwargs['filename'])
		elif 'img8' in kwargs:
			self.from_8bits(kwargs['img8'])

	def __dealloc__(self):
		self.__free__()

	def __getitem__(self,i):
		return self.img.val[i]

	def __setitem__(self,i,val):
		self.img.val[i] = val

	def __iter__(self):
		self._it = 0
		return self

	def __next__(self):
		if self._it >= self.n:
			raise StopIteration
		else:
			v = self[self._it]
			self._it = self._it + 1
			return v

	def size(self):
		return (self.img.ncols, self.img.nrows)

	@property
	def n(self):
		return self.img.n

	cpdef topixel(self, int p):
		cdef Pixel px
		px = usis.image.to_pixel(self.img, p)

		return np.array([px.x, px.y],np.int)

	cpdef valid_pixel(self, xy):
		cdef int x = int(xy[0]), y = int(xy[1])
		return usis.image.valid_pixel(self.img, x,y)

	cpdef toindex(self, xy):
		cdef int x,y
		x,y = xy[0],xy[1]
		return usis.image.to_index(self.img, x,y)

	cdef void __free__(self):
		if self.img is not NULL:
			if self.__created_from_img__:
				if self.__own_image__:
					usis.image.destroy_image(&self.img)
			else:
				if self.img is not NULL:
					self.img.val = NULL
					free(self.img)
					self.img = NULL

	cpdef initialize(self, int ncols, int nrows):
		cdef usis.image.Image32 *img = usis.image.create_image32(ncols, nrows)
		self.from_image(img, True)

	cpdef from_file(self, filename):
		cdef bytes_filename = bytes(filename)

		self.__free__()

		if filename.lower().rfind(".pgm") >= 0:
			self.from_image(usis.image.read_image32(<char*>bytes_filename), True)
		else:
			img = pl.imread(filename)
			h,w = img.shape

			# Numpy reads the image upside down to
			# fit with the array coordinate system.
			# We have to take this into account
			# by rearranging the image properly.
			for y in xrange(int(math.floor(h/2))):
				for x in xrange(w):
					tmp = img[y,x].copy()
					img[y,x] = img[h-y-1,x].copy()
					img[h-y-1,x] = tmp

			self.from_array(img)


	cpdef from_array(self, np.ndarray[np.int32_t, ndim=2, mode='c'] array):
		self.__free__()

		self.img = <usis.image.Image32*>calloc(1, sizeof(usis.image.Image32))
		self.arr = array

		self.__created_from_img__ = False

		self.img.ncols = self.arr.shape[1]
		self.img.nrows = self.arr.shape[0]

		self.img.val = <int*>self.arr.data


	cdef from_image(self, usis.image.Image32 *img, bool own_image):
		self.__free__()

		self.img = img

		self.__created_from_img__ = True
		self.__own_image__ = own_image

		self.arr = from_int_array((img.nrows, img.ncols),
									img.val, False)
	def copy(self):
		cdef Image32 new_img = Image32()

		new_img.from_image(usis.image.copy_image32(self.img), True)

		return new_img

	cpdef add_frame(self, int sz, int value):
		cdef Image32 frame = Image32()

		frame.from_image(usis.image.add_frame(self.img, sz, value), True)

		return frame

	def __reduce__(self):
		cdef np.ndarray[np.int32_t, ndim=2, mode='c'] arr_cpy

		arr_cpy = self.arr.copy()

		return Image32_unpickle, (arr_cpy,) # single element tuple

	cpdef write(self, filename):
		if os.path.splitext(filename)[1].lower() == '.pgm':
			usis.image.write_image(self.img, <char*>filename)
		else:
			scipy.misc.imsave(filename, self.arr)

	cpdef to_8bits(self):
		cdef Image8 img8 = Image8()

		img8.from_image(usis.image.convert_2_8bits(self.img), True)

		return img8

	cpdef from_8bits(self, Image8 img8):
		self.from_image(usis.image.cast_2_image32(img8.img), True)


def Image32_unpickle(*args):
	img = Image32()

	img.from_array(args[0])

	return img

cdef class DbImage:
	def __cinit__(self, **kwargs):
		self.img = NULL
		self.arr = None

		if 'size' in kwargs:
			self.initialize(*kwargs['size'])
		elif 'filename' in kwargs:
			self.from_file(kwargs['filename'])

	def __dealloc__(self):
		self.__free__()

	cdef void __free__(self):
		if self.__created_from_img__:
			if self.__own_image__:
				usis.image.destroy_dbimage(&self.img)
		else:
			if self.img is not NULL:
				self.img.val = NULL
				free(self.img)
				self.img = NULL

	def __getitem__(self,i):
		return self.img.val[i]

	def __setitem__(self,i,val):
		self.img.val[i] = val

	def __iter__(self):
		self._it = 0
		return self

	def __next__(self):
		if self._it >= self.n:
			raise StopIteration
		else:
			v = self[self._it]
			self._it = self._it + 1
			return v

	def size(self):
		return (self.img.ncols, self.img.nrows)

	@property
	def n(self):
		return self.img.n

	cpdef topixel(self, int p):
		cdef Pixel px
		px = usis.image.to_pixel(self.img, p)

		return np.array([px.x, px.y],np.int)

	cpdef valid_pixel(self, xy):
		cdef int x = int(xy[0]), y = int(xy[1])
		return usis.image.valid_pixel(self.img, x,y)

	cpdef toindex(self, xy):
		cdef int x,y
		x,y = xy[0],xy[1]
		return usis.image.to_index(self.img, x,y)

	cpdef initialize(self, int ncols, int nrows):
		cdef usis.image.DbImage *img = usis.image.create_dbimage(ncols, nrows)
		self.from_image(img, True)


	cpdef from_array(self, np.ndarray[np.float64_t, ndim=2, mode='c'] array):
		self.__free__()

		self.img = <usis.image.DbImage*>calloc(1, sizeof(usis.image.DbImage))
		self.arr = array

		self.__created_from_img__ = False

		self.img.ncols = self.arr.shape[1]
		self.img.nrows = self.arr.shape[0]

		self.img.val = <double*>self.arr.data


	cdef from_image(self, usis.image.DbImage *img, bool own_image):
		self.__free__()

		self.img = img

		self.__created_from_img__ = True
		self.__own_image__ = own_image

		self.arr = from_double_array((img.nrows, img.ncols),
										img.val, False)

	def copy(self):
		cdef DbImage new_img = DbImage()

		new_img.from_image(usis.image.copy_dbimage(self.img), True)

		return new_img

	def __reduce__(self):
		cdef np.ndarray[np.float64_t, ndim=2, mode='c'] arr_cpy

		arr_cpy = self.arr.copy()

		return DbImage_unpickle, (arr_cpy,) # single element tuple

def DbImage_unpickle(*args):
	img = DbImage()

	img.from_array(args[0])

	return img

cdef class CImage8:
	def __cinit__(self, **kwargs):
		self.img = NULL

		if 'size' in kwargs:
			self.initialize(*kwargs['size'])
		elif 'filename' in kwargs:
			self.from_file(kwargs['filename'])
		elif 'img8' in kwargs:
			self.from_image8(kwargs['img8'])

	def __dealloc__(self):
		self.__free__()

	def __getitem__(self,i):
		return (self.img.C[0].val[i],
				self.img.C[1].val[i],
				self.img.C[2].val[i])

	def __setitem__(self,i,val):
		self.img.C[0].val[i] = <uchar>val[0]
		self.img.C[1].val[i] = <uchar>val[1]
		self.img.C[2].val[i] = <uchar>val[2]

	def size(self):
		return (self.img.ncols, self.img.nrows)

	cpdef bool is_grayscale(self):
		return usis.image.is_cimage_gray(self.img)

	cpdef topixel(self, int p):
		cdef Pixel px
		px = usis.image.to_pixel(self.img, p)

		return np.array([px.x, px.y],np.int)

	cpdef valid_pixel(self, xy):
		cdef int x = int(xy[0]), y = int(xy[1])
		return usis.image.valid_pixel(self.img, x,y)

	cpdef toindex(self, xy):
		cdef int x,y
		x,y = xy[0],xy[1]
		return usis.image.to_index(self.img, x,y)

	cdef void __free__(self):
		usis.image.destroy_cimage(&self.img)

	cpdef initialize(self, int ncols, int nrows):
		cdef usis.image.CImage8 *img = usis.image.create_cimage8(ncols, nrows)
		self.from_image(img)

	cpdef from_file(self, filename):
		cdef bytes bytes_filename = bytes(filename)
		cdef int h,w,x,y,c

		self.__free__()

		if filename.lower().rfind(".ppm") >= 0:
			self.from_image(usis.image.read_cimage8(<char*>bytes_filename))
		else:
			img = pl.imread(filename)
			img8bits = None

			if img.dtype == np.float32:
				# Converting the image to uchar,
				# since pylab reads pngs as float
				img8bits = np.zeros(img.shape,np.uint8)

			h,w = img.shape[0],img.shape[1]

			if img8bits is None:
				# Numpy reads the image upside down to
				# fit with the array coordinate system.
				# We have to take this into account
				# by rearranging the image properly.
				for y in xrange(int(math.floor(h/2))):
					for x in xrange(w):
						tmp = img[y,x].copy()
						img[y,x] = img[h-y-1,x].copy()
						img[h-y-1,x] = tmp

				self.from_array(img)
			else:
				# Converting from float32 to 8bit unsigned int.
				# Pylab reads PNG images using the proper coordinate
				# system. Hence, there's no need to do the rearranging
				# above.
				for y in xrange(h):
					for x in xrange(w):
						for c in xrange(3):
							img8bits[y,x,c] = 255*img[y,x,c]

				self.from_array(img8bits)

	cpdef from_array(self, np.ndarray[np.uint8_t, ndim = 3, mode = 'c'] arr):
		self.__free__()

		self.img = ndarray_2_cimage8(arr)

	cdef from_image(self, usis.image.CImage8 *cimg):
		self.__free__()

		self.img = cimg

	def to_array(self):
		return cimage8_2_ndarray(self.img)

	def copy(self):
		cdef CImage8 new_img = CImage8()

		new_img.from_image(usis.image.copy_cimage8(self.img))

		return new_img

	def to_ycbcr(self):
		cdef usis.image.CImage8 *ycbcr = usis.image.cimage_ycbcr_to_rgb(self.img)

		new_img = CImage8()

		new_img.from_image(ycbcr)

		return new_img

	def to_image8(self):
		cdef Image8 img8 = Image8()

		img8.from_image(usis.image.copy_image8(self.img.C[0]), True)

		return img8

	def to_grayscale(self):
		cdef CImage8 gray = CImage8()

		gray.from_image(usis.image.convert_2_grayscale(self.img))

		return gray

	def __reduce__(self):
		cdef np.ndarray[np.uint8_t, ndim = 3, mode='c'] arr_cpy

		arr_cpy = self.to_array()

		return CImage8_unpickle, (arr_cpy,) # single element tuple

	cpdef from_image8(self, Image8 img):
		self.from_image(usis.image.cast_2_cimage8(img.img))


	cpdef add_frame(self, int sz, unsigned char r, unsigned char g,
					unsigned char b):
		cdef CImage8 frame = CImage8()

		frame.from_image(usis.image.add_frame(self.img, sz, r, g, b))

		return frame

	cpdef write(self, filename):
		if os.path.splitext(filename)[1].lower() == '.ppm':
			usis.image.write_cimage(self.img, <char*>filename)
		else:
			scipy.misc.imsave(filename, self.to_array())

def CImage8_unpickle(*args):
	img = CImage8()

	img.from_array(args[0])

	return img

cpdef np.ndarray cimage8_file_2_ndarray(filename):
	cdef bytes bytes_filename = bytes(filename)
	cdef usis.image.CImage8 *cimg = usis.image.read_cimage8(<char*>filename)

	output = cimage8_2_ndarray(cimg)

	usis.image.destroy_cimage(&cimg)

	return output

@cython.boundscheck(False)
cdef np.ndarray cimage8_2_ndarray(usis.image.CImage8 *cimg):
	cdef int i, c
	cdef np.ndarray[np.uint8_t, ndim = 3, mode = 'c'] arr
	cdef Pixel px

	arr = np.zeros((cimg.nrows, cimg.ncols, 3), np.uint8)

	for c in range(0,3):
		for i in range(0, cimg.nrows*cimg.ncols):
			px = usis.image.to_pixel(cimg, i)
			arr[px.y, px.x, c] = cimg.C[c].val[i]

	return arr

@cython.boundscheck(False)
cdef usis.image.CImage8*  ndarray_2_cimage8(np.ndarray[np.uint8_t, ndim = 3, mode = 'c'] arr):
	cdef int i, c, w, h
	cdef Pixel px
	cdef usis.image.CImage8 *cimg = NULL

	h,w = arr.shape[0],arr.shape[1]

	cimg = usis.image.create_cimage8(w, h)

	for c in range(0,3):
		for i in range(0, cimg.nrows*cimg.ncols):
			px = usis.image.to_pixel(cimg, i)
			cimg.C[c].val[i] = arr[px.y, px.x, c]

	return cimg

cpdef CImage8 concat(CImage8 img1, CImage8 img2):
	cdef usis.image.CImage8 *cimg = usis.image.create_cimage8(img1.img.ncols + img2.img.ncols,
																max(img1.img.nrows,img2.img.nrows))
	cdef CImage8 cat = CImage8()

	cat.from_image(cimg)

	usis.image.concatenate(img1.img, img2.img, cat.img)

	return cat


#cpdef concat_inplace(CImage8 img1, CImage8 img2, CImage8 dst):
#	usis.image.concatenate(img1.img, img2.img, dst.img)

cpdef random_color():
	cdef int color = randomColor()

	return color

cpdef triplet_py(int a,int b,int c):
	return triplet(a,b,c)

