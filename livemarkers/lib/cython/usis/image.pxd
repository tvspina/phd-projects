from libcpp cimport bool
from c_common cimport *

cdef extern from "usisimage.h" namespace "usis":

#	#/ General template for any kind of grey image with a single channel

	struct Image8:

		unsigned char *val
		int ncols, nrows, n
		int n_alloced

		Rectangle bb

	#/ General template for any kind of grey image with a single channel
	struct Image32:
		int *val
		int ncols, nrows, n
		int n_alloced

		Rectangle bb

	struct DbImage:
		double *val
		int ncols, nrows, n

		int n_alloced

		Rectangle bb

	ctypedef Image8* Image8Ptr
	ctypedef Image32* Image32Ptr
	ctypedef DbImage* DbImagePtr


	Image8* create_image8( int w,  int h)
	#/ Copy ructor
	Image8* copy_image8(Image8 *old_img)
		#/ Constructor that reads from an PGM image file
	Image8* read_image8( char *filename)

	Image32* create_image32( int w,  int h)

	DbImage *create_dbimage( int w,  int h)

	# writes an 8-bit image to a PGM file
	bool write_image(Image8 *img, char *format, ...)
	# writes an 32-bit image to a PGM file
	bool write_image(Image32 *img, char *format, ...)

	#/ Copy ructor
	Image32* copy_image32(Image32 *old_img)

	#/ Constructor that reads from an PGM image file
	Image32* read_image32( char *filename)

	DbImage* copy_dbimage(DbImage *old_img)

	void destroy_image(Image8 **img)
	void destroy_image(Image32 **img)

	void destroy_dbimage(DbImage **img)

	unsigned char maximum_value(Image8 *img)
	int maximum_value(Image32 *img)
	unsigned char minimum_value(Image8 *img)
	int minimum_value(Image32 *img)

	double maximum_value(DbImage *img)
	double minimum_value(DbImage *img)

	void set_image(Image8 *img, unsigned char val)
	void set_image(Image32 *img, int val)
	void set_image(DbImage *img, double val)

	Image8 *add_frame(Image8 *img, int sz, unsigned char value)
	Image8 *rem_frame(Image8 *fimg, int sz)
	Image32 *add_frame(Image32 *img, int sz, int value)
	Image32 *rem_frame(Image32 *fimg, int sz)


	#/ Only copies the content from a 32-bit image
	#/ (with non-checked max value of 255) to an
	#/ Image8
	Image8 *cast_2_image8(Image32 *img32)
	#/ Only copies the content from an 8-bit image
	#/ (with non-checked max value of 255) to an
	#/ Image32
	Image32 *cast_2_image32(Image8 *img8)

	#/ Converts the content from an 8-bit image
	#/ to an 8-bit image (stretching the values)
	Image8 *convert_2_8bits(Image8 *img)
	#/ Converts the content from a 32-bit image
	#/ to an 8-bit image (stretching the values)
	Image8 *convert_2_8bits(Image32 *img)
	#/ Converts the content from a 32-bit image
	#/ to an N-bit image (stretching the values)
	Image32 *convert_2_Nbits(Image32 *img, int N)

	Image8 *convert_2_image8(DbImage *dimg)
	Image32 *convert_2_image32(DbImage *dimg)

	#/ copies each pixel value from src to dest
	void copy_inplace(Image8 *dest, Image8 *src)
	#/ copies each pixel value from src to dest
	void copy_inplace(Image32 *dest, Image32 *src)
	void copy_inplace(DbImage *dest, DbImage *src)

	Image8 *scale(Image8 *img, float Sx, float Sy)
	Image32 *scale(Image32 *img, float Sx, float Sy)

	double eucl_dist2(Pixel u, Pixel v)
	double eucl_dist2(double x0,  double y0, double x1, double y1)

	double eucl_dist2(Image32 *img, int p, int q)
	double eucl_dist2(Image8 *img, int p, int q)

	#*********************************************************/
	#*						Color image 					 **/
	#*********************************************************/

	struct CImage8:
		Image8 *C[3]
		int ncols, nrows, n

		unsigned char *data
		Rectangle bb

	struct CImage32:
		Image32 *C[3]
		int ncols, nrows, n

		int *data
		Rectangle bb

	ctypedef CImage8* CImage8Ptr
	ctypedef CImage32* CImage32Ptr

	#/ Basic ructor
	CImage8* create_cimage8(int w,  int h)
	CImage8* create_cimage8(int w,  int h,
								unsigned char *data,
								int n)

	#/ Copy ructor
	CImage8* copy_cimage8(CImage8 *old_img)
		#/ copies each pixel value from src to dest
	void copy_inplace(CImage8 *dest, CImage8 *src)
	#/ Constructur that reads from a PPM file
	CImage8* read_cimage8(char *filename)

	#/ Basic ructor
	CImage32* create_cimage32(int w,  int h)

	CImage32* create_cimage32(int w,  int h,
									int *data,
									int n)
	#/ Copy ructor
	CImage32* copy_cimage32(CImage32 *old_img)

	CImage32* convert_2_cimage32(Image32 *img)
	#/ Constructur that reads from a PPM file
	Image32* read_cimage32( char *filename)


	# writes an 8-bit image to a PGM file
	bool write_cimage(CImage8 *img, char *format, ...)
	# writes an 32-bit image to a PGM file
	bool write_cimage(CImage32 *img, char *format, ...)

	CImage8* concatenate(CImage8 *img1, CImage8 *img2)
	void concatenate(CImage8 *img1, CImage8 *img2, CImage8 *cat)


	CImage8 *add_frame(CImage8 *img, int sz, unsigned char r,
							unsigned char f, unsigned char b)

	#/ deletes the cimage object and frees the pointer
	void destroy_cimage(CImage8 **cimg)
	void destroy_cimage(CImage32 **cimg)

	bool is_cimage_gray(CImage8 *cimg)


	CImage8 *cimage_ycbcr_to_rgb(CImage8 *cimg)
	CImage8 *cimage_rgb_to_ycbcr(CImage8 *cimg)
	CImage8 *cimage_rgb_to_lab(CImage8 *cimg)
	CImage8 *cimage_lab_to_rgb(CImage8 *cimg)

	#/ val is represented by a triplet
	void set_cimage(CImage8 *img, int val)
	#/ val is represented by a triplet
	void set_cimage(CImage32 *img, int val)
	#/ each channel value is provided separately
	void set_cimage(CImage32 *img, int c0, int c1, int c2)


	#/ Only copies the content from a 32-bit image
	#/ (with non-checked max value of 255) to a
	#/ CImage8
	CImage8 *cast_2_cimage8(Image32 *img32)
	CImage8 *cast_2_cimage8(Image8 *img)
	CImage8 *convert_2_cimage8(Image32 *img)

	CImage8 *convert_2_grayscale(CImage8 *color)

	#/ copies each pixel value from src to dest
	void copy_inplace(CImage8 *dest, CImage8 *src)
	void copy_inplace(CImage32 *dest, CImage32 *src)

	CImage8 *scale(CImage8 *img, float Sx, float Sy)
	CImage32 *scale(CImage32 *img, float Sx, float Sy)

	inline bool valid_pixel(Image8 *img, Pixel px)

	inline bool valid_pixel(Image8 *img,  int x,  int y)

	inline bool valid_pixel(Image32 *img, Pixel px)

	inline bool valid_pixel(Image32 *img,  int x,  int y)

	inline bool valid_pixel(DbImage *img, Pixel px)

	inline bool valid_pixel(DbImage *img,  int x,  int y)

	inline bool valid_pixel(CImage8 *img, Pixel px)

	inline bool valid_pixel(CImage8 *img,  int x,  int y)

	inline bool valid_pixel(CImage32 *img, Pixel px)

	inline bool valid_pixel(CImage32 *img,  int x,  int y)

	inline int to_index(Image8 *img, int x, int y)

	inline int to_index(Image8 *img, Pixel px)

	inline Pixel to_pixel(Image8 *img, int p)

	inline void to_pixel(Image8 *img, int p, int *x, int *y)

	inline int to_index(Image32 *img, int x, int y)

	inline int to_index(Image32 *img, Pixel px)

	inline Pixel to_pixel(Image32 *img, int p)

	inline void to_pixel(Image32 *img, int p, int *x, int *y)

	inline int to_index(DbImage *img, int x, int y)

	inline int to_index(DbImage *img, Pixel px)

	inline Pixel to_pixel(DbImage *img, int p)

	inline void to_pixel(DbImage *img, int p, int *x, int *y)

	inline int to_index(CImage8 *img, int x, int y)

	inline int to_index(CImage8 *img, Pixel px)

	inline Pixel to_pixel(CImage8 *img, int p)

	inline void to_pixel(CImage8 *img, int p, int *x, int *y)

	inline int to_index(CImage32 *img, int x, int y)

	inline int to_index(CImage32 *img, Pixel px)

	inline Pixel to_pixel(CImage32 *img, int p)

	inline void to_pixel(CImage32 *img, int p, int *x, int *y)

	inline void set_rgb(CImage8 *img, int p, unsigned char r, unsigned char g, unsigned char b)

	inline void set_rgb(CImage8 *img, int p, int rgb_triplet)

	inline void set_rgb(CImage8 *img, Pixel px, int rgb_triplet)

	inline void set_rgb(CImage8 *img, Pixel px,  unsigned char r, unsigned char g, unsigned char b)

	inline void set_rgb(CImage8 *img, int x, int y,  unsigned char r, unsigned char g, unsigned char b)

	inline void set_rgb(CImage8 *img, int x, int y,  int rgb_triplet)

	inline void get_rgb(CImage8 *img, int p)


	#* Misc **/

	void and_image(Image8 *mask1, Image8 *mask2, unsigned char val)
	void xor_image(Image8 *mask1, Image8 *mask2, unsigned char val)

	void and_image(Image32 *bkg, Image32 *fg, int x, int y)
	void and_image(Image8 *bkg, Image8 *fg, int x, int y)
	void and_image(CImage8 *bkg, CImage8 *fg, int x, int y)

	CImage8 *blur(CImage8 *cimg, double radius, Rectangle bb)
	void blur_inplace(CImage8 *cimg, double radius, Rectangle bb)


