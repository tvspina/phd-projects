from c_adjacency cimport *
cimport c_common
cimport c_gradient
cimport c_features
from npimage cimport *
from features cimport *

import numpy as np

cpdef image_gradient(Image8 img, float radius):
	cdef Image8 grad = Image8()

	grad.from_image(c_gradient.image_gradient(img.img, radius), True)

	return grad

cpdef image_max_band_gradient(CImage8 img, float radius):
	cdef Image8 grad
	cdef c_features.Features *f = c_features.lab_feats(img.img)
	cdef float Fmax
	cdef c_features.Features *Gfeat = c_gradient.vfeatures_gradient(f,radius, &Fmax)

	grad = Image8(size = img.size())

	c_gradient.feat_obj_weight_image(Gfeat, NULL, grad.img,
                               0.0, Fmax, 0.0,
                               Rectangle());

	c_features.DestroyFeatures(&f)
	c_features.DestroyFeatures(&Gfeat)

	return grad



cpdef weighted_image_gradient(CImage8 cimg, float radius, channel_weight):
	cdef int c
	cdef float weight[3]
	cdef Image8 grad = Image8()
	cdef AdjRel *A = circular(radius)

	for c in xrange(3):
		weight[c] = channel_weight[c]

	grad.from_image(c_gradient.weighted_image_gradient(cimg.img, A, weight), True)

	destroy_adjrel(&A)

	return grad

cpdef weighted_feature_gradient(Features f, float radius, channel_weight):
	cdef int c
	cdef float Fmax
	cdef float *weight = c_common.AllocFloatArray(f.f.nfeats)
	cdef Features grad = Features()
	cdef AdjRel *A = circular(radius)

	for c in xrange(f.f.nfeats):
		weight[c] = channel_weight[c]

	grad.from_feats(c_gradient.weighted_feature_gradient(f.f, &Fmax, A, weight))

	destroy_adjrel(&A)

	free(weight)

	return grad, Fmax

cpdef vimage_gradient(Image8 img, float radius, bb = None):
	cdef Features feats = Features()
	cdef float Omax
	cdef Rectangle c_bb = Rectangle()

	if bb is not None:
		c_bb.beg.x = bb[0]
		c_bb.beg.y = bb[1]
		c_bb.end.x = bb[2]
		c_bb.end.y = bb[3]

	feats.from_feats(c_gradient.vimage_gradient(img.img, radius, &Omax, c_bb))

	return feats, Omax

cpdef vfeatures_gradient(Features f, float radius):
	cdef Features Gfeat = Features()
	cdef float Fmax

	Gfeat.from_feats(c_gradient.vfeatures_gradient(f.f, radius, &Fmax))

	return Gfeat, Fmax


cpdef feat_obj_weight_image(Features Gfeat, Features Gobj,
							float Wobj, float Fmax, float Omax,
							bb = None):
	cdef Image8 grad = Image8()
	cdef Rectangle c_bb = Rectangle()

	if bb is not None:
		c_bb.beg.x = bb[0]
		c_bb.beg.y = bb[1]
		c_bb.end.x = bb[2]
		c_bb.end.y = bb[3]

	if Gobj is not None:
		grad.from_image(c_gradient.feat_obj_weight_image(Gfeat.f, Gobj.f, Wobj,
														Fmax, Omax, c_bb), True)
	else:
		grad.from_image(c_gradient.feat_obj_weight_image(Gfeat.f, NULL, 0.0,
														Fmax, 0.0, c_bb), True)
	return grad
