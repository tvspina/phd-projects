cdef extern from "usisoptimization.h":
	ctypedef double (*problem) ( double *x, void *context)

	cdef struct optContext:
		problem f                 # The fitness function to be optimized.
		void *fContext            # The problem's context.
		size_t fDim               # Dimensionality of problem.
		double *theta             # Theta to be initially considered and to be return
									# as the best one in the end of the optimization.
		double * lowerInit   # Lower initialization boundary.
		double * upperInit   # Upper initialization boundary.
		double * lowerBound  # Lower search-space boundary.
		double * upperBound  # Upper search-space boundary.
		size_t numIterations      # Number of fitness-evaluations.
		char verbose              # Whether it prints information from the optmiation or not.

cdef extern from "usisoptimization.h" namespace "usis":

	cdef double MSPS_integer( double *param, void* context)

