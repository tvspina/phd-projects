cimport c_common

cdef class RandomGenerator:
	def __cinit__(self):
		self.generator = NULL

	def __dealloc__(self):
		if self.generator is not NULL:
			c_common.destroy_random_generator(&self.generator)


	cdef initialize(self, void *generator):
		if self.generator is not NULL:
			c_common.destroy_random_generator(&self.generator)

		self.generator = generator

def random_seed():
	gen = RandomGenerator()

	gen.initialize(c_common.random_seed())

	return gen
