from image cimport *
from c_common cimport *

cdef extern from "usisradiometric.h" namespace "usis":
	cdef Image8 *linear_stretch(Image8 *img, uchar f1, uchar f2,
							   uchar g1, uchar g2)

	cdef void linear_stretch_inplace(Image8 *img, uchar f1, uchar f2,
									uchar g1, uchar g2)

	cdef Image32 *linear_stretch(Image32 *img, int f1, int f2, int g1, int g2)

	cdef void linear_stretch_inplace(Image32 *img, int f1, int f2, int g1, int g2)

	cdef void log_stretch(Image32 *img, int fmin, int fmax, int gmin, int gmax)
	cdef void log_stretch(Image8 *img, int fmin, int fmax, int gmin, int gmax)

	# Sigmoidal rescaling of values.
	# Since negative values are considered to be inside the signed tde of the mask
	# then sigma and beta must be negative
	cdef Image8 *sigmoidal_stretch(Image32 *img, int neg_thresh,
								  int pos_thresh, double beta, double sigma,
								  uchar maxval)

	# Setting brighness and contrast
	cdef int update_brightness_contrast(int value, int B, int C)

	# updates brighness and contrast in place
	cdef void update_image_brightness_contrast(Image8 *img, int B, int C)
	cdef void update_image_brightness_contrast(Image32 *img, int B, int C)
	cdef void update_cimage_brightness_contrast(CImage8 *cimg, int B, int C)


	cdef inline double negative_sigmoid(double x, double min_val, double max_val,
										  double neg_thresh, double pos_thresh,
										  double beta, double sigma)

	cdef inline double sigmoid(double x, double min_val, double max_val,
								double low_thresh, double high_thresh,
								double beta, double sigma)
