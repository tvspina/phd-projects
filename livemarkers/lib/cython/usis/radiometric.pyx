cimport image
from npimage cimport *
cimport radiometric


def update_image_bri_con(Image8 img, int B, int C):
	radiometric.update_image_brightness_contrast(img.img, B, C)

def update_cimage_bri_con(CImage8 cimg, int B, int C):
	radiometric.update_cimage_brightness_contrast(cimg.img, B, C)

def linear_stretch8(Image8 img, int min_val, int max_val, int final_min_val,
						int final_max_val):
	cdef Image8 stretched = Image8()

	stretched.from_image(radiometric.linear_stretch(img.img, min_val, max_val, final_min_val, final_max_val), True)

	return stretched
