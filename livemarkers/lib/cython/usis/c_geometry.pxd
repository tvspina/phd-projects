from image cimport *
from libcpp cimport set as cppset

cdef extern from "usisgeometry.h" namespace "usis":
	cppclass Vector2D:

		double x
		double y

		Vector2D()
		Vector2D(double, double)

		Vector2D(Pixel px, Pixel center)

		bool operator==(Vector2D &)
		bool operator!=(Vector2D &)

		Vector2D operator*(double val)
		Vector2D operator/(double val)
		Vector2D operator+(Vector2D &vec1)
		Vector2D operator+(double val)
		Vector2D operator-(Vector2D &vec1)
		Vector2D operator-(double val)

        # Operator overload for the dot product **/
		double operator*(Vector2D &vec1)

		double Norm()
		Vector2D Normalize()

        # @return the angle [0, M_PI] between this vector and @param vec. **/
		double Angle(Vector2D &vec)

        # @return the global direction [0, 2*M_PI] of this vector w.r.t.,
		double Direction(Vector2D origin)

        # @return the direction {1,-1} of vector @param vec w.r.t this one
		# if they are concurrent, {0} otherwise.
		int Concurrent(Vector2D &vec)

		Pixel ToPixel(Pixel center)
		int ToIndex(Pixel center, Image8 *img)
		int ToIndex(Pixel center, Image32 *img)


	Pixel mask_centroid(Image8 *mask, int label)
	Pixel mask_centroid(Image32 *mask, int label)

	void mask_bounding_box(Image8 *mask, Pixel *beg, Pixel *end, int val)
	void mask_bounding_box(Image32 *mask, Pixel *beg, Pixel *end, int val)

	void mask_bounding_box(Image8 *mask, Pixel *beg, Pixel *end, cppset[int] val)
	void mask_bounding_box(Image32 *mask, Pixel *beg, Pixel *end, cppset[int] val)

	void mask_bounding_box(Image8 *mask, int *pmin, int *pmax, int val)
	void mask_bounding_box(Image32 *mask, int *pmin, int *pmax, int val)

	void mask_bounding_box(Image8 *mask, int *pmin, int *pmax, cppset[int] val)
	void mask_bounding_box(Image32 *mask, int *pmin, int *pmax, cppset[int] val)

	Image8* mask_bounding_box(Image8 *mask, int val)
	Image32* mask_bounding_box(Image32 *mask, int val)

	Image8* mask_bounding_box(Image8 *mask, cppset[int] val)
	Image32* mask_bounding_box(Image32 *mask, cppset[int] val)

	CImage8 *transform_image(CImage8 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels)
	void transform_image(CImage8 *src, CImage8 *out, float M[4][4],
						Image8 **selected_pixels)

	inline Rectangle mask_bb(Image8 *mask, int val)
	inline Rectangle mask_bb(Image32 *mask, int val)

	inline Rectangle mask_bb(Image8 *mask, cppset[int] values)
	inline Rectangle mask_bb(Image32 *mask, cppset[int] values)

	inline double degrees(double rad)
	inline double radians(double deg)

	# Transformations
	Vector2D transform_vector(float M[4][4], Vector2D p, bool round_val)
	Pixel transform_point(float M[4][4], Pixel px)

	Image8 *transform_image(Image8 *img, float M[4][4], int ncols, int nrows)
	void transform_image(Image8 *src, Image8 *out, float M[4][4],
						Image8 **selected_pixels)

	Image32 *transform_image(Image32 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels)
	void transform_image(Image32 *src, Image32 *dst, float M[4][4],
						Image8 **selected_pixels)

	void scl_rot_trans(float sx, float sy, float th, float dx,float dy,float T[4][4],
                       int x_in, int y_in, int x_out, int y_out, bool direct)


    # Matrix operations
	void inverse(float M[4][4], float IM[4][4])
	float cofactor(float M[4][4], int l, int c)
	void trans_matrix(float M1[4][4],float M2[4][4])
	void translation(float T[4][4], float dx, float dy, float dz)
	void rot_x(float Rx[4][4], float thx)
	void rot_y(float Ry[4][4], float thy)
	void rot_z(float Rz[4][4], float thz)
