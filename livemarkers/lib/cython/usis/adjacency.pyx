cimport c_adjacency
from libcpp cimport bool

import numpy as np

cdef class AdjRel:
	def __cinit__(self, **kwargs):
		cdef float radius
		if 'radius' in kwargs:
			radius = kwargs['radius']
			if 'fast' in kwargs and kwargs['fast']:
				self.A = c_adjacency.fast_circular(radius)
			else:
				self.A = c_adjacency.circular(radius)
		elif 'ncols' in kwargs and 'nrows' in kwargs:
			self.A = c_adjacency.box(kwargs['ncols'], kwargs['nrows'])

		else:
			self.A = NULL

		self._i = 0

	def __dealloc__(self):
		c_adjacency.destroy_adjrel(&self.A)

	def __getitem__(self, i):
		return np.array([self.A.dx[i], self.A.dy[i]])

	def __iter__(self):
		self._i = 0
		return self

	def __next__(self):
		if self._i >= self.n:
			raise StopIteration
		else:
			dxdy = self[self._i]
			self._i = self._i + 1
			return dxdy

	@property
	def n(self):
		return self.A.n
