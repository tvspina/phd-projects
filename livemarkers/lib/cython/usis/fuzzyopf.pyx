'''
Copyright © 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
cimport usis.set
cimport subgraph
cimport c_geometry
cimport c_common
cimport c_fuzzyopf

from common cimport *
from features cimport *
from npimage cimport *
from math import *

cdef class FOPFClassifier(object):
	cdef subgraph.Subgraph *sgtrainobj
	cdef subgraph.Subgraph *sgtrainbkg

	def __cinit__(self):
		self.sgtrainobj = NULL
		self.sgtrainbkg = NULL

	def __dealloc__(self):
		subgraph.DestroySubgraph(&self.sgtrainobj)
		subgraph.DestroySubgraph(&self.sgtrainbkg)

	def from_markers(self, markers, Features feats, int nsamples,
						RandomGenerator gen, int lb_bkg = 0,
						feat_weight = None):
		cdef usis.set.Set *Si = NULL, *Se = NULL
		cdef int i
		cdef double max_arc_weight
		cdef bool success = False

		for i in markers:
			if markers[i][1] == lb_bkg:
				usis.set.InsertSet(&Se, i)
			else:
				usis.set.InsertSet(&Si, i)

		max_arc_weight = feats.max_arcweight()

		subgraph.DestroySubgraph(&self.sgtrainobj)
		subgraph.DestroySubgraph(&self.sgtrainbkg)

		if feat_weight is not None:
			c_fuzzyopf.opf_FeatWeight = c_common.AllocFloatArray(feats.f.nfeats)
			for i in xrange(feats.f.nfeats):
				c_fuzzyopf.opf_FeatWeight[i] = feat_weight[i]

		if Si is not NULL and Se is not NULL:
			c_fuzzyopf.fopf_on_markers(feats.f, Si, Se, &self.sgtrainobj,
										&self.sgtrainbkg, nsamples,
										max_arc_weight,
										gen.generator)
			success = True

		usis.set.DestroySet(&Si)
		usis.set.DestroySet(&Se)

		if feat_weight is not None:
			free(c_fuzzyopf.opf_FeatWeight)
			c_fuzzyopf.opf_FeatWeight = NULL

		return success


	def from_intelligent_markers(self, markers, Features feats,
									int nsamples, RandomGenerator gen,
									int lb_bkg = 0, feat_weight = None):
		cdef usis.set.Set *Si = NULL, *Se = NULL
		cdef int i, kmax, area, dx, dy
		cdef double max_arc_weight
		cdef bool success = False
		cdef Image32 clusters = Image32(size = (feats.f.ncols, feats.f.nrows))
		cdef subgraph.Subgraph *sg
		cdef c_geometry.Rectangle bb

		bb = c_geometry.Rectangle()

		for i in markers:
			if markers[i][1] == lb_bkg:
				usis.set.InsertSet(&Se, i)
			else:
				usis.set.InsertSet(&Si, i)

		max_arc_weight = feats.max_arcweight()

		subgraph.DestroySubgraph(&self.sgtrainobj)

		subgraph.DestroySubgraph(&self.sgtrainbkg)

		kmax = 40
		area = 10

		# Using around nsamples from images sized over 500k pixels
		if feats.f.ncols * feats.f.nrows > 500000:
			dx = max(1, <int>floor(<float>feats.f.ncols / sqrt(nsamples)))
			dy = max(1, <int>floor(<float>feats.f.nrows / sqrt(nsamples)))
		else:
			dx = 16 if feats.f.ncols > 32 else 1;
			dy = 16 if feats.f.nrows > 32 else 1;

		sg = c_fuzzyopf.uniform_sampling(clusters.img, NULL, dx, dy, bb)
		c_features.SetSubgraphFeatures(sg, feats.f)

		if feat_weight is not None:
			c_fuzzyopf.opf_FeatWeight = c_common.AllocFloatArray(feats.f.nfeats)
			for i in xrange(feats.f.nfeats):
				c_fuzzyopf.opf_FeatWeight[i] = feat_weight[i]

		clusters.from_image(c_fuzzyopf.image_opf_clustering(feats.f, sg, 5,
															kmax, area), True)

		subgraph.DestroySubgraph(&sg)

		if Si is not NULL and Se is not NULL:
			c_fuzzyopf.fopf_by_clustered_regions(feats.f, clusters.img,	&Si, &Se,
												1, 1, &self.sgtrainobj,
												&self.sgtrainbkg, nsamples,
												max_arc_weight,
												gen.generator);
			if(self.sgtrainobj is not NULL and
				self.sgtrainbkg is not NULL):
				success = True

		usis.set.DestroySet(&Si)
		usis.set.DestroySet(&Se)

		if feat_weight is not None:
			free(c_fuzzyopf.opf_FeatWeight)
			c_fuzzyopf.opf_FeatWeight = NULL

		return success


	def fuzzy_classify(self, Features f, int nthreads=4,
						Image8 existing_objMap = None):
		cdef Image8 objMap

		if existing_objMap is not None:
			objMap = existing_objMap
		else:
			objMap = Image8()

		objMap.from_image(c_fuzzyopf.tfopf_membership_map_sse(self.sgtrainobj,
																self.sgtrainbkg,
																f.f,
																nthreads),
																True)
		return objMap
