from libcpp cimport bool

cdef extern from "set.h":
	cdef struct Set:
		int elem
		Set *next

	cdef void InsertSet(Set **S, int elem)
	cdef int  RemoveSet(Set **S)
	cdef int  GetSetSize(Set *S)
	cdef Set *CloneSet(Set *S)
	cdef void DestroySet(Set **S)

	cdef void MergeSets(Set **S, Set **T)
	cdef bool IsInSet(Set *S, int elem)
