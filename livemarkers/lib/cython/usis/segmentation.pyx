'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
from libcpp cimport bool
from usis.c_common cimport *
cimport usis.image
cimport c_segmentation
cimport c_adjacency
from usis.realheap cimport *
from usis.gqueue cimport *
from usis.c_geometry cimport *

from npimage cimport *
from adjacency cimport *

from libcpp.pair cimport *
from libcpp.queue cimport *
from libcpp.stack cimport *
cimport libcpp.set as cppset
from libcpp.vector cimport *
from tr1.unordered_map cimport *
from tr1.unordered_set cimport *

import numpy as np
cimport numpy as np

import sys,math,time

from cython.operator cimport dereference as deref
from cython.operator cimport preincrement as preinc

from libc.stdlib cimport free, malloc, calloc
from libc.string cimport memcpy

class DelineationError(Exception):
	def __init__(self, msg):
		self.msg = msg
	def __str__(self):
		return str(self.msg)

cdef class LWOF(object):
	cdef public Image32 pred, cost
	cdef public Image8 grad, obj, label
	cdef c_adjacency.AdjRel *_A
	cdef GQueue *_Q
	cdef double power
	cdef int Wmax, Cmax

	cdef int _prev_anchor
	cdef int _yet_prev_anchor

	# Riverbed attributes, must be placed here otherwise it won't compile
	cdef double _duct_dist
	cdef float _adj_radius

	cdef public str method_type
	cdef public str nickname

	def __cinit__(self, Image8 grad, Image8 obj, float radius, method_config, **kwargs):
		cdef int w,h

		self.method_type = 'boundary'

		self._Q = NULL
		self._A = c_adjacency.circular(radius)
		self._adj_radius = radius

		self.obj = obj
		self.grad = grad

		w,h = self.grad.size()

		self.pred = Image32()
		self.pred.from_image(create_image32(w,h), True)

		self.cost = Image32()
		self.cost.from_image(create_image32(w,h), True)

		self.label = Image8()
		self.label.from_image(create_image8(w,h), True)

		self.Wmax = UCHAR_MAX
		key = 'lwof_power'

		if key in method_config:
			self.power = method_config[key]
		else:
			self.power = 1.0

		self.Cmax = <int>(self.Wmax**self.power)

		self.nickname = 'LWOF'

	def __repr__(self):
		return 'lwof'

	def __dealloc__(self):
		c_adjacency.destroy_adjrel(&self._A)
		DestroyGQueue(&self._Q)

	cdef __init_path_cost_data__(self):
		cdef int w,h

		w,h = self.grad.size()

		if self._Q is not NULL:
			DestroyGQueue(&self._Q)

		self._Q = CreateGQueue(self.Cmax+1, w*h, self.cost.img.val)

		self.reset()

	def init_data(self, method_config, **kwargs):
		# Initializing data *after* the proper values for Cmax and Wmax have
		# been set in __cinit__.
		self.__init_path_cost_data__()

	cpdef run(self, int dst, method_config) except +:
		cdef int success = 0

		cdef int lwof_ori = method_config['lwof_ori']
		cdef bool force_ori = method_config['force_ori']
		cdef usis.image.Image8 *obj = NULL

		path = None
		if self.cost[dst] > INT_MIN:
			if self._prev_anchor >= 0:

				if self.obj is not None:
					obj = self.obj.img

				c_segmentation.contour_tracking(self.grad.img, self.cost.img,
													self.pred.img, obj, &self._Q,
													self._A, self.Wmax,
													<c_segmentation.USIS_LW_ORI>lwof_ori,
													dst, self.power, force_ori,
													c_segmentation.lwof_cost)

				# The boundary method self-crosses if the dst
				# was not reached from self._prev_anchor. Hence,
				# dst's cost is infinity.
				if self.cost[dst] == INT_MAX:
					success = -1
				else:
					success = 1
					path = self.path(dst, self._prev_anchor)

		return success, path

	def add_anchor(self, int anchor, method_config, **kwargs):
		cdef bool success = False
		path = None
		# Checking to determine whether anchor is in a previously
		# selected part of the path. If so, no anchor is added
		if self.cost[anchor] > INT_MIN:
			success, path = self.__add_anchor__(anchor, method_config)

		return success, path, None

	def __add_anchor__(self, int anchor, method_config):
		cdef bool success = False

		path = None
		if anchor != self._prev_anchor:
			# IMPORTANT: At this point, it should  be
			# guaranteed that self.run will work until
			# anchor or a delineation error is issued.
			if self._prev_anchor >= 0:
				# Making sure that the path until anchor
				# is calculated

				run_success, path = self.run(anchor, method_config)
				if run_success < 0:
					raise DelineationError('Unable to add anchor at pixel %s' %
									self.grad.topixel(anchor))

				# Confirming path from the new anchor to
				# the _previous_anchor
				self.confirm_path(anchor)

			else:
				# Making sure that the first
				# node from each new segment/contour
				# has NIL as its predecessor. Note
				# that this will only be the case
				# when a new segment is created
				# separated from previous segments.
				# New anchor points for a continuous
				# segment remain with their original
				# predecessors intact
				self.pred[anchor] = NIL
				self._prev_anchor = NIL
				self._yet_prev_anchor = NIL

				# Making sure that the path until anchor
				# is calculated.
				run_success, path = self.run(anchor, method_config)

				if run_success < 0:
					raise DelineationError('Unable to add anchor at pixel %s' %
									self.grad.topixel(anchor))

			# Storing two anchors ago to make sure
			# that the contour is only closed when two points
			# are selected
			self._yet_prev_anchor = self._prev_anchor

			# Storing the current anchor as the _previous_anchor for
			# when a new anchor is added.
			self._prev_anchor = anchor

			self.reset_non_segment_pixels()

			ResetGQueue(self._Q) # resetting _Q to avoid errors

			self.cost.img.val[anchor] = 0 # setting prev as seed
			InsertGQueue(&self._Q, anchor)

			success = True

		return success, path

	cdef set_path(self, int anchor, int prev_anchor, int val):
		cdef int p = anchor

		if prev_anchor >= 0:
			# Confirming current path
			while p != NIL and p != self.pred[prev_anchor]:
				self.cost[p] = val
				p = self.pred[p]

	cdef confirm_path(self, int anchor):
		self.set_path(anchor, self._prev_anchor, INT_MIN)

	# Resetting the current path in case of error
	cpdef deconfirm_path(self, int anchor, int prev_anchor, int yet_prev_anchor):
		if prev_anchor >= 0:
			self.set_path(anchor, prev_anchor, INT_MAX)

			self._yet_prev_anchor = yet_prev_anchor
			self._prev_anchor = prev_anchor

			self.reset_current_path()

	# Resets the current path in case the orientation has changed
	# and/or the path has been diconfirmed
	cpdef reset_current_path(self):
		self.reset_non_segment_pixels()
		ResetGQueue(self._Q)
		self.cost[self._prev_anchor] = 0
		InsertGQueue(&self._Q, self._prev_anchor)

	cpdef reset_non_segment_pixels(self):
		cdef int q

		for q in xrange(self.cost.img.n):
			if self.cost.img.val[q] != INT_MIN:
				self.cost.img.val[q] = INT_MAX

	cpdef reset_set_of_pixels(self, seeds):
		cdef int q

		# We can only reset path segments if boundary-tracking
		# is not currently active for computing a new segment.
		# This prevents inconsistencies in the path costs
		# being computed according to the Bellman principle
		# of optimality. This function is only useful for
		# Hybrid approaches that need to delete path seeds.
		if self._prev_anchor == NIL:
			if seeds is not None and len(seeds) > 0:
				for q in seeds:
					self.cost.img.val[q] = INT_MAX


	cpdef reset(self):
		self._yet_prev_anchor = NIL
		self._prev_anchor = NIL
		set_image(self.pred.img, NIL)
		set_image(self.cost.img, INT_MAX)
		ResetGQueue(self._Q)

	cpdef end_segment(self, int anchor = NIL):
		if anchor >= 0:
			self.confirm_path(anchor)

		self._yet_prev_anchor = NIL
		self._prev_anchor = NIL
		self.reset_non_segment_pixels()
		ResetGQueue(self._Q)

	cpdef close_contour(self, int dst, method_config) except +:
		cdef int p=NIL,q=NIL,src=NIL,i,adj, old_anchor
		cdef int prev_anchor = NIL, yet_prev_anchor = NIL
		cdef Pixel px, px_adj
		cdef c_adjacency.AdjRel *A = c_adjacency.fast_circular(1.5)

		# Closing first the segment from the previous anchor to the
		# current point, if dst is provided and at least one
		# anchor point was provided.
		if dst >= 0 and self._prev_anchor >= 0:
			try:
				# Storing the previous anchor and
				# yet previous anchor in case
				# the anchor is not added successfully
				prev_anchor = self._prev_anchor
				yet_prev_anchor = self._yet_prev_anchor

				success, _, _ = self.add_anchor(dst, method_config)
				if not success:
					raise DelineationError('Unable to close contour when adding anchor pixel %s' %
										self.grad.topixel(dst))
			except:
				raise DelineationError('Unable to close contour when adding anchor pixel %s' %
										self.grad.topixel(dst))
			else:
				src = dst
		# Again, if there are at least two anchors
		# then the contour can be closed. Otherwise,
		# an "error" should be raised.
		elif self._yet_prev_anchor >= 0:
			src = self._prev_anchor

		print 'Trying to close contour from pixel %s' % str(self.grad.topixel(src))

		if src >= 0:
			# Searching for the first and second contour
			# points.
			q = src
			p = self.pred[q]
			path_nodes = set()

			while self.pred[p] != NIL:
				path_nodes.add(q) # adding from src to the successor of
								  # the new first node of the path
				q = p # second contour point
				p = self.pred[p] # first contour point
			# Adding the new first node of the path
			# since the loop stops before that can be done
			path_nodes.add(q)

			# Resetting non-contour pixels, the queue,
			# and reinserting src in the queue because
			# the path to p needs to be recalculated,
			# since it was previously part of an older segment.
			self.reset_non_segment_pixels()
			ResetGQueue(self._Q)
			self.cost[src] = 0
			InsertGQueue(&self._Q, src)

			# Defining q as the new first contour point
			self.pred[q] = NIL

			px = usis.image.to_pixel(self.pred.img, p)
			# Making sure that all of p's neighbors are
			# available to be conquered as long as
			# they are not part of the contour segment.
			# We cannot guarantee this solely by evaluating
			# if cost[adj] != INT_MIN since in Riverbed's
			# case nodes within 1.5 pixels of the contour segment
			# are set to INT_MIN to prevent self crossing.
			for i in xrange(1,A.n):
				px_adj = c_adjacency.next_adjacent(self.pred.img, A, px, i)
				if usis.image.valid_pixel(self.pred.img, px_adj):
					adj = usis.image.to_index(self.pred.img, px_adj)
					if adj not in path_nodes:
						self.cost[adj] = INT_MAX

			# Making p available to be conquered
			# in order to close the contour
			self.cost[p] = INT_MAX

			#Attempting to run LWOF to confirm path
			success, path = self.run(p, method_config)

			# Making sure that the predecessor map is accurate
			# after closing the contour. This is to deal with
			# the case when a build is duct to close the contour.
			if path is not None:
				for i in xrange(len(path)-1):
					self.pred[path[i]] = path[i+1]

			if success < 0:
				# Since a new anchor was blindly added,
				# if dst >=0, then we remove it as anchor
				# because there was no way to reach the beginning
				# of the contour from it. Then we /irm the path
				# to dst and raise an exception for self-loop.
				if prev_anchor >= 0:
					self.deconfirm_path(src, prev_anchor, yet_prev_anchor)

				# Restoring p and q as the first and second contour pixels,
				# respectively
				self.cost[p] = INT_MIN
				self.pred[p] = NIL
				self.cost[q] = INT_MIN
				self.pred[q] = p

				raise DelineationError('Unable to close contour after adding anchor pixel %s' %
										self.grad.topixel(dst))
			self.end_segment(p) # The current path is preserved
								# along with all the other paths
								# that were not reset
		c_adjacency.destroy_adjrel(&A)

		return q,p # returning the first and last contour nodes

	def path(self, int dst, int prev):
		cdef int *cur_path = c_segmentation.path2array(self.pred.img, dst, prev)

		if cur_path is not NULL:
			arr = np.zeros(cur_path[0],np.int32)

			for x in xrange(len(arr)):
				arr[x] = cur_path[x+1]

			free(cur_path)

			return arr
		else:
			return None

	def path_seeds(self, anchors, int primary_label, int left_pixel_label,
						int right_pixel_label, int mk_id,
						inv_seed_index = None):
		cdef int root, p
		cdef unordered_map[int, c_segmentation.MarkerData] *seed_map
		cdef unordered_map[int, c_segmentation.MarkerData].iterator it
		cdef unordered_multimap[int, int] inv_index
		cdef unordered_multimap[int, int].iterator inv_it
		cdef vector[pair[int, double] ] c_anchors
		cdef pair[int,double] new_anchor

		for anchor in anchors:
			new_anchor.first = <int>anchor[0]
			new_anchor.second = <double>anchor[1]
			c_anchors.push_back(new_anchor)

		seed_map = c_segmentation.path_seeds(c_anchors,
											self.pred.img, self.cost.img,
											primary_label, left_pixel_label,
											right_pixel_label, mk_id,
											&inv_index)

		it = deref(seed_map).begin()

		seeds = {}
		while it != deref(seed_map).end():
			seeds[deref(it).first] = (mk_id, deref(it).second.lb)

			preinc(it)

		if inv_seed_index is not None:
			inv_it = inv_index.begin()

			while inv_it != inv_index.end():
				root = deref(inv_it).first
				p = deref(inv_it).second

				if root not in inv_seed_index:
					inv_seed_index[root] = set()

				inv_seed_index[root].add(p)

				preinc(inv_it)

		return seeds

	@property
	def prev_anchor(self):
		return self._prev_anchor

	@property
	def yet_prev_anchor(self):
		return self._yet_prev_anchor


cdef class Riverbed(LWOF):

	def __cinit__(self, Image8 grad, Image8 obj, float radius, method_config, **kwargs):

		self.Wmax = UCHAR_MAX
		self.power = 1.0 # ignoring the function argument since Riverbed's
						 # cost is always the regular arc weight
		self.Cmax = self.Wmax

		self._duct_dist = 15

		self.nickname = 'Riverbed'

	def __repr__(self):
		return 'riverbed'

	cpdef run(self, int dst, method_config) except +:
		cdef int success = 0
		cdef int lwof_ori = method_config['lwof_ori']
		cdef bool force_ori = method_config['force_ori']
		cdef double dist
		cdef usis.image.Image8 *obj = NULL

		path = None
		if self._prev_anchor >= 0:
			dist = <double>math.sqrt(eucl_dist2(self.grad.img, dst,
										self._prev_anchor))
			if dist > 0.0 and dist <= self._duct_dist:
				# The duct is only built as a path. If an anchor is
				# to be added then the corresponding function self.add_anchor
				# should ensure that the predecessor map be corrected.
				path = self.__build_duct__(dst, self._prev_anchor)

				if path is not None:
					success = 1
				else:
					# issuing an error since the contour self-crosses
					success = -1

			if success <= 0:
				if self.cost[dst] > INT_MIN:
					if self.obj is not None:
						obj = self.obj.img

					c_segmentation.contour_tracking(self.grad.img, self.cost.img,
										self.pred.img, obj, &self._Q,
										self._A, self.Wmax, <c_segmentation.USIS_LW_ORI>lwof_ori,
										dst, self.power, force_ori,
										c_segmentation.riverbed_cost)

				# The boundary method self-crosses if the dst
				# was not reached from self._prev_anchor. Hence,
				# dst's cost is infinity. In this case we return -1
				# to signal an error. Note that this only
				# happens when a duct was not build, hence, success == 0
				if success == 0 and self.cost[dst] == INT_MAX:
					success = -1
				else:
					success = 1
					if path is None:
						path = self.path(dst, self._prev_anchor)

		return success, path

	def add_anchor(self, int anchor, method_config, **kwargs):
		cdef double dist
		cdef int success = 0, i

		if self._prev_anchor >= 0:
			dist = <double>math.sqrt(eucl_dist2(self.grad.img, anchor,
												self._prev_anchor))

			if dist > 0 and dist <= self._duct_dist:
				path = self.__build_duct__(anchor, self._prev_anchor)

				# Making sure that the predecessor map is accurate
				# after building and confirming the duct.
				if path is not None:
					for i in xrange(len(path)-1):
						self.pred[path[i]] = path[i+1]

		success, path = self.__add_anchor__(anchor, method_config)

		return success, path, None

	# Redefining the confirm path function since Riverbed must
	# create barriers around the current segment to avoid self-loops.
	cdef confirm_path(self, int anchor):
		cdef int i, p, p_adj
		cdef float radius = 1.5
		cdef c_adjacency.AdjRel *A = NULL
		cdef Pixel px, px_adj

		if self._prev_anchor >= 0:
			A = c_adjacency.fast_circular(radius)
			path_nodes = set()

			# Confirming at each iteration the current path
			# all the way until the very first
			# path node, since we *must* ensure that
			# the cost of all surrounding pixels be properly
			# set to INT_MIN to avoid self-crossing.
			p = anchor
			while p != NIL:
				self.cost[p] = INT_MIN
				path_nodes.add(p)
				p = self.pred[p]
			# Freezing all 8-neighbor pixels surrounding the current
			# contour segment by setting their cost to INT_MIN to make
			# them unavailable for the next iteration
			p = anchor
			while p != NIL:
				px = usis.image.to_pixel(self.pred.img, p)
				for i in xrange(1,A.n):
					px_adj.x = px.x + A.dx[i]
					px_adj.y = px.y + A.dy[i]
					if usis.image.valid_pixel(self.pred.img, px_adj):
						p_adj = usis.image.to_index(self.pred.img, px_adj)
						self.cost[p_adj] = INT_MIN

				p = self.pred[p]

			px = usis.image.to_pixel(self.grad.img,anchor)

			# Making all neighbors of <anchor>
			# available for the next iteration of
			# Riverbed. This operation prevents
			# the neighboring pixels to block
			# the new path starting from <anchor>.
			for i in xrange(1,A.n):
				px_adj.x = px.x + A.dx[i]
				px_adj.y = px.y + A.dy[i]
				if usis.image.valid_pixel(self.grad.img, px_adj):
					p_adj = usis.image.to_index(self.grad.img, px_adj)

					if p_adj not in path_nodes:
						self.cost[p_adj] = INT_MAX

		c_adjacency.destroy_adjrel(&A)

	cpdef deconfirm_path(self, int anchor, int prev_anchor, int yet_prev_anchor):
		cdef int i, p, p_adj
		cdef float radius = 1.5
		cdef c_adjacency.AdjRel *A = NULL
		cdef Pixel px, px_adj

		if prev_anchor >= 0:
			A = c_adjacency.fast_circular(radius)
			path_nodes = set()

			# Selecting all path nodes from prev_anchor
			# to the beginning of the segment to keep
			# them intact
			p = prev_anchor
			while p != NIL:
				path_nodes.add(p)
				p = self.pred[p]

			# Defreezing all 8-neighbor pixels surrounding the current
			# contour segment between anchor and prev_anchor
			# by setting their cost to INT_MAX to make
			# them available for the next iteration
			p = anchor

			# Deconfirming the thiker frozen segment around
			# the entire contour segment all the way to the
			# beginning.
			while p != NIL:
				px = usis.image.to_pixel(self.pred.img, p)
				for i in xrange(0,A.n):
					px_adj.x = px.x + A.dx[i]
					px_adj.y = px.y + A.dy[i]
					if (usis.image.valid_pixel(self.pred.img, px_adj)):
						p_adj = usis.image.to_index(self.pred.img, px_adj)
						if p_adj not in path_nodes:
							self.cost[p_adj] = INT_MAX

				p = self.pred[p]

			self._yet_prev_anchor = yet_prev_anchor
			self._prev_anchor = prev_anchor

			# Reconfirming the path until prev_anchor
			self.confirm_path(prev_anchor)

			# Preparing for a new iteration from prev_anchor
			self.reset_non_segment_pixels()
			# Resetting prev_anchor as the "new" anchor
			ResetGQueue(self._Q)
			self.cost[prev_anchor] = 0
			InsertGQueue(&self._Q, prev_anchor)

		c_adjacency.destroy_adjrel(&A)

	cpdef __build_duct__(self, int dst, int prev_anchor):
		cdef int vx, vy
		cdef float Dx, Dy
		cdef int amostras
		cdef float m
		cdef int i, p, x1,y1,xn,yn
		cdef float xk, yk

		path_nodes = set()

		p = prev_anchor

		while p != NIL:
			path_nodes.add(p)
			p = self.pred[p]

		usis.image.to_pixel(self.grad.img, prev_anchor, &xn, &yn)
		usis.image.to_pixel(self.grad.img, dst, &x1, &y1)

		vx = xn - x1
		vy = yn - y1

		if vx == 0:
			Dx = 0.0
			Dy = 1.0 if (vy >= 0) else -1.0
			amostras = abs(vy)+1
		else:
			m = (<float>vy )/(<float>vx)
			if ( abs(vx) > abs(vy)):
				Dx = 1.0 if (vx >= 0) else -1.0
				Dy = m * Dx
				amostras = abs(vx)+1
			else:
				Dy = 1.0 if (vy >= 0) else -1.0
				Dx = Dy / m
				amostras = abs(vy)+1

		xk = <float> x1
		yk = <float> y1
		path = []
		for _ in xrange(amostras):
			if usis.image.valid_pixel(self.grad.img, <int>round(xk),<int>round(yk)):
				p = usis.image.to_index(self.grad.img, <int>round(xk),<int>round(yk))

				# If the duct self-crosses the contour, return None
				# to issue an error.
				if p in path_nodes and p != prev_anchor:
					return None
				else:
					path.append(p)

			xk = xk + Dx
			yk = yk + Dy

		arr_path = np.array(path,dtype=np.int)

		return arr_path

cdef public enum COST_FUNCTION:
	FSUM=0
	FMAX


cdef class DIFT:
	cdef public Image32 pred, root, regions
	cdef public Image8 grad, label

	cdef Image32 _cost
	cdef GQueue *_Q
	cdef RealHeap *_H
	cdef c_adjacency.AdjRel *_A
	cdef int Cmax, Wmax
	cdef float *_fcost

	cdef float (*cost_fun)(c_segmentation.SegData *data, float, float)

	cdef public str method_type
	cdef public str nickname

	def __cinit__(self, Image8 grad, float radius, float power, bool use_heap,
					int cost_function):
		cdef int w,h

		self.method_type = 'region'

		w,h = grad.size()

		self.pred = Image32()
		self.root = Image32()
		self.regions = Image32()
		self.label = Image8()

		self.pred.from_image(usis.image.create_image32(w,h), True)
		self.root.from_image(usis.image.create_image32(w,h), True)
		self.regions.from_image(usis.image.create_image32(w,h), True)
		self.label.from_image(usis.image.create_image8(w,h), True)

		self.grad = grad

		self._Q = NULL
		self._H = NULL
		self._fcost = NULL

		self._A = c_adjacency.circular(radius)

		self.Wmax = UCHAR_MAX

		if cost_function == FMAX:
			self.cost_fun = c_segmentation.fmax
		else:
			self.cost_fun = c_segmentation.fsum_power

		if use_heap:
			self._fcost = AllocFloatArray(self.pred.img.n)
			self._H = CreateRealHeap(self.pred.img.n, self._fcost)
		else:
			self._cost = Image32()
			self._cost.from_image(create_image32(w,h), True)
			self.Cmax = self.Wmax if power <= 1.0 else int(self.Wmax**power)
			self._Q = CreateGQueue(self.Cmax+1, w*h, self._cost.img.val)

		self.reset()

		self.nickname = 'DIFT-SC'

	def __repr__(self):
		return 'dift'

	def __dealloc__(self):
		if self._fcost is not NULL:
			free(self._fcost)

		DestroyRealHeap(&self._H)
		DestroyGQueue(&self._Q)
		c_adjacency.destroy_adjrel(&self._A)

	def reset(self):
		cdef int p

		if self._fcost is not NULL:
			for p in xrange(self.pred.img.n):
				self._fcost[p] = FLT_MAX
		else:
			set_image(self._cost.img,INT_MAX)

		set_image(self.label.img, 0)
		set_image(self.pred.img, NIL)
		set_image(self.root.img,NIL)


	def get_cost(self,p):
		if self._fcost is not NULL:
			return self._fcost[p]
		else:
			return self._cost[p]

	def set_cost(self,p,val):
		if self._fcost is not NULL:
			self._fcost[p] = <float>val
		else:
			self._cost[p] = <int>val

	def run(self, seeds, del_seeds):
		cdef int s
		cdef Pixel px
		cdef unordered_map[int, c_segmentation.MarkerData] S
		cdef c_segmentation.SegData *data
		cdef unordered_map[int, c_segmentation.MarkerData] delSet
		cdef c_segmentation.MarkerData mk

		S = unordered_map[int, c_segmentation.MarkerData]()
		delSet = unordered_map[int, c_segmentation.MarkerData]()
		data = new c_segmentation.SegData(False)

		if seeds is not None:
			for s in seeds:
				px = usis.image.to_pixel(self.pred.img, s)
				if usis.image.valid_pixel(self.pred.img, px):
					mk = c_segmentation.MarkerData(seeds[s][0],seeds[s][1])
					S[s] = mk

		if del_seeds is not None:
			for s in del_seeds:
				mk = c_segmentation.MarkerData(del_seeds[s][0], del_seeds[s][1])
				delSet[s] = mk

		data.label = self.label.img
		data.pred = self.pred.img
		data.root = self.root.img
		data.grad = self.grad.img
		data.regions = self.regions.img
		data.Wmax = self.Wmax
		data.seeds = S

		if self._fcost is not NULL:
			data.fcost = self._fcost
			ResetRealHeap(self._H)
			c_segmentation.dift(data, self._A, delSet, &self._H,
									c_segmentation.dist_grad, self.cost_fun)
		else:
			data.cost = self._cost.img
			ResetGQueue(self._Q)
			c_segmentation.dift(data, self._A,  delSet,
									&self._Q, c_segmentation.dist_grad,
									self.cost_fun)

		del data



cdef class MaxFlow:
	cdef public Image8 grad, label, obj
	cdef float lamb, power

	cdef Image32 _cost
	cdef c_adjacency.AdjRel *_A
	cdef int Cmax, Wmax

	cdef public str method_type
	cdef public str nickname
	cdef c_segmentation.Graph *g
	cdef void* *nodes

	def __cinit__(self, Image8 grad, Image8 obj, float radius,
					float lamb, float power):

		cdef int w,h

		self.method_type = 'region'

		self.label = Image8(size = grad.size())

		self.grad = grad
		self.obj = obj

		self.lamb = lamb
		self.power = power

		self._A = c_adjacency.circular(radius)

		self.g = NULL
		self.nodes = NULL

		self.reset()

		self.nickname = 'GCMF'

	def __dealloc__(self):
		c_adjacency.destroy_adjrel(&self._A)
		self.reset()

	def __repr__(self):
		return 'mflow'

	def reset(self):
		set_image(self.label.img, 0)
		if self.g is not NULL:
			del self.g
		if self.nodes is not NULL:
			free(self.nodes)

		self.nodes = NULL
		self.g = NULL

	def run(self, seeds, del_seeds = None):
		cdef int s
		cdef Pixel px
		cdef unordered_map[int, c_segmentation.MarkerData] S
		cdef c_segmentation.SegData *data
		cdef unordered_map[int, c_segmentation.MarkerData] delSet
		cdef c_segmentation.MarkerData mk

		S = unordered_map[int, c_segmentation.MarkerData]()
		data = new c_segmentation.SegData(False)
		delSet = unordered_map[int, c_segmentation.MarkerData]()

		if seeds is not None:
			for s in seeds:
				px = usis.image.to_pixel(self.grad.img, s)
				if usis.image.valid_pixel(self.grad.img, px):
					mk = c_segmentation.MarkerData(seeds[s][0],seeds[s][1])
					S[s] = mk

		data.label = self.label.img
		data.grad = self.grad.img
		if self.obj is not None:
			data.objMap = self.obj.img
		else:
			data.objMap = usis.image.create_image8(self.grad.img.ncols,
													self.grad.img.nrows)

		data.seeds = S

		c_segmentation.max_flow(data, self._A, self.lamb, self.power,
								c_segmentation.dist_grad,
								&self.g, &self.nodes)

		if self.obj is None:
			usis.image.destroy_image(&data.objMap)

		del data

cdef class LiveMarkers(LWOF):
	cdef public DIFT dift

	def __cinit__(self, Image8 grad, Image8 obj, float radius, method_config, **kwargs):
		self.method_type = 'hybrid'
		self.dift = None
		self.nickname = 'LiveMarkers'

	def __repr__(self):
		return 'livemarkers'

	def init_data(self, method_config, **kwargs):
		cdef Image8 grad = kwargs['grad']
		cdef float radius = method_config['adj_radius']
		cdef float dift_power = method_config['dift_power']
		cdef bool dift_heap = method_config['dift_heap']
		cdef int dift_cost_function = FMAX

		if method_config['dift_cost_function'] == 'FSUM':
			dift_cost_function = FSUM
		else:
			dift_cost_function = FMAX

		self.dift = DIFT(grad, radius, dift_power, dift_heap, dift_cost_function)
		self.label = self.dift.label # Overriding label from LWOF

		self.__init_path_cost_data__()

	def add_anchor(self, int anchor, method_config, **kwargs):
		cdef int primary_label = method_config['primary_label']
		cdef int left_pixel_label = method_config['left_pixel_label']
		cdef int right_pixel_label = method_config['right_pixel_label']
		cdef int mk_id = method_config['mk_id']
		cdef int old_anchor = self._prev_anchor
		cdef double lm_mk_radius = method_config['lm_mk_radius']
		cdef bool success = False

		# Calling the boundary method for adding anchors
		success, path, _ = LWOF.add_anchor(self, anchor, method_config)

		path_seeds = None

		inv_seed_index = None

		if 'inv_seed_index' in kwargs:
			inv_seed_index = kwargs['inv_seed_index']

		# self.__add_anchor__ alters prev_anchor, so when adding the first
		# anchor an error might occur, thus we must test if self.prev_anchor != anchor
		if (old_anchor >= 0 and success):
			anchors = [(old_anchor, lm_mk_radius), (anchor, lm_mk_radius)]
			path_seeds = self.path_seeds(anchors,
											primary_label,
											left_pixel_label,
											right_pixel_label,
											mk_id,
											inv_seed_index = inv_seed_index)

		del_seeds = None
		if 'del_seeds' in kwargs:
			del_seeds = kwargs['del_seeds']

		if path_seeds is not None or del_seeds is not None:
		    # Postponing delineation
			if 'delineate' not in kwargs or kwargs['delineate']:
				self.delineate(path_seeds, del_seeds)

		return success, path, path_seeds

	def delineate(self, seeds, del_seeds, **kwargs	):
		if seeds is not None or del_seeds is not None:
			self.dift.run(seeds, del_seeds)

			# Resetting deleted seeds that were part of
			# path segments
			self.reset_set_of_pixels(del_seeds)

	cpdef reset(self):
		LWOF.reset(self)
		self.dift.reset()

	@property
	def root(self):
		return self.dift.root

cdef class RiverCut(Riverbed):
	cdef public float gc_lambda, gc_power
	cdef public MaxFlow max_flow
	cdef dict seeds

	def __cinit__(self, Image8 grad, Image8 obj,
					float radius, method_config,
					**kwargs):
		self.method_type = 'hybrid'
		self.max_flow = None
		self.seeds = None

		self.nickname = 'RiverCut'

	def __repr__(self):
		return 'rivercut'

	def init_data(self, method_config, **kwargs):
		cdef Image8 grad = kwargs['grad']
		cdef Image8 obj = None
		cdef float radius = method_config['adj_radius']

		self.gc_power = method_config['gc_power']
		self.gc_lambda = method_config['gc_lambda']

		if 'obj' in kwargs:
			obj = kwargs['obj']

		self.max_flow = MaxFlow(grad, obj, radius, self.gc_lambda,
								self.gc_power)
		self.label = self.max_flow.label # Overriding label from Riverbed

		self.__init_path_cost_data__()
		self.seeds = {}

	def add_anchor(self, int anchor, method_config, **kwargs):
		cdef int p
		cdef int primary_label = method_config['primary_label']
		cdef int left_pixel_label = method_config['left_pixel_label']
		cdef int right_pixel_label = method_config['right_pixel_label']
		cdef int mk_id = method_config['mk_id']
		cdef int old_anchor = self._prev_anchor
		cdef double lm_mk_radius = method_config['lm_mk_radius']
		cdef bool success = False

		# Calling the boundary method for adding anchors
		success, path, _ = Riverbed.add_anchor(self, anchor, method_config)

		path_seeds = None

		inv_seed_index = None

		if 'inv_seed_index' in kwargs:
			inv_seed_index = kwargs['inv_seed_index']

		# self.__add_anchor__ alters prev_anchor, so when adding the first
		# anchor an error might occur, thus we must test if self.prev_anchor != anchor
		if (old_anchor >= 0 and success):
			anchors = [(old_anchor, lm_mk_radius), (anchor, lm_mk_radius)]
			path_seeds = self.path_seeds(anchors, primary_label,
											left_pixel_label,
											right_pixel_label,
											mk_id,
											inv_seed_index = inv_seed_index)
		del_seeds = None
		if 'del_seeds' in kwargs:
			del_seeds = kwargs['del_seeds']


		if path_seeds is not None:
			for p in path_seeds:
				self.seeds[p] = path_seeds[p]

		if del_seeds is not None:
			for p in del_seeds:
				del self.seeds[p]

		if path_seeds is not None or del_seeds is not None:
		    # Postponing delineation
			if 'delineate' not in kwargs or kwargs['delineate']:
				self.delineate(self.seeds, del_seeds)

		return success, path, path_seeds

	def delineate(self, seeds, del_seeds, **kwargs):
		cdef int s
		if seeds is not None:
			# Since MaxFlow is recomputed from scratch
			# we just remove the seeds added for deletion
			# from the seed set
			if del_seeds is not None:
				for s in del_seeds:
					if s in seeds:
						del seeds[s]
				# Resetting since seeds were deleted
				self.max_flow.reset()

			self.max_flow.run(seeds)

			# Resetting deleted seeds that were part of
			# path segments
			self.reset_set_of_pixels(del_seeds)

	cpdef reset(self):
		Riverbed.reset(self)
		self.max_flow.reset()
		self.seeds = {}

# Random hybrid methods for testing purposes
cdef class LiveCut(LiveMarkers):
	cdef public float gc_lambda, gc_power
	cdef public MaxFlow max_flow
	cdef dict seeds

	def __cinit__(self, Image8 grad, Image8 obj,
				float radius, method_config,
				**kwargs):
		self.method_type = 'hybrid'
		self.max_flow = None
		self.seeds = None
		self.nickname = 'LiveCut'

	def __repr__(self):
		return 'livecut'

	def init_data(self, method_config, **kwargs):
		cdef Image8 grad = kwargs['grad']
		cdef Image8 obj = None
		cdef float radius = method_config['adj_radius']

		self.gc_power = method_config['gc_power']
		self.gc_lambda = method_config['gc_lambda']

		if 'obj' in kwargs:
			obj = kwargs['obj']

		self.max_flow = MaxFlow(grad, obj, radius, self.gc_lambda,
								self.gc_power)
		self.label = self.max_flow.label # Overriding label from Riverbed

		self.__init_path_cost_data__()
		self.seeds = {}

	def delineate(self, seeds, del_seeds, **kwargs):
		if seeds is not None:
			# Since MaxFlow is recomputed from scratch
			# we just remove the seeds added for deletion
			# from the seed set
			if del_seeds is not None:
				for s in del_seeds:
					if s in seeds:
						del seeds[s]
				# Resetting since seeds were deleted
				self.max_flow.reset()

			self.max_flow.run(seeds)

			# Resetting deleted seeds that were part of
			# path segments
			self.reset_set_of_pixels(del_seeds)

	cpdef reset(self):
		LWOF.reset(self)
		self.max_flow.reset()
		self.seeds = {}

cdef class RiverMarkers(RiverCut):
	cdef public DIFT dift

	def __cinit__(self, Image8 grad, Image8 obj, float radius, method_config, **kwargs):
		self.method_type = 'hybrid'
		self.dift = None
		self.seeds = None
		self.nickname = 'RiverMarkers'

	def __repr__(self):
		return 'rivermarkers'

	def init_data(self, method_config, **kwargs):
		cdef Image8 grad = kwargs['grad']
		cdef float radius = method_config['adj_radius']
		cdef float dift_power = method_config['dift_power']
		cdef bool dift_heap = method_config['dift_heap']
		cdef int dift_cost_function = FMAX

		if method_config['dift_cost_function'] == 'FSUM':
			dift_cost_function = FSUM
		else:
			dift_cost_function = FMAX

		self.dift = DIFT(grad, radius, dift_power, dift_heap, dift_cost_function)
		self.label = self.dift.label # Overriding label from LWOF

		self.__init_path_cost_data__()
		self.seeds = {}

	def delineate(self, seeds, del_seeds, **kwargs	):
		if seeds is not None or del_seeds is not None:
			self.dift.run(seeds, del_seeds)

			# Resetting deleted seeds that were part of
			# path segments
			self.reset_set_of_pixels(del_seeds)


	cpdef reset(self):
		Riverbed.reset(self)
		self.dift.reset()
		self.seeds = {}

	@property
	def root(self):
		return self.dift.root


def image_contours(Image8 bin, int thrsh = 0):
	cdef vector[int*].iterator it
	cdef vector[int*] contours
	cdef int x, n
	cdef int *cur_contour

	contours = c_segmentation.image_contours(bin.img, thrsh)

	it = contours.begin()

	contour_list = []
	while it != contours.end():
		n = deref(it)[0]
		cur_contour = AllocIntArray(n)

		memcpy(cur_contour, deref(it) + 1, sizeof(int)*n)

		contour_list.append(from_int_array(n, cur_contour, True))

		free(deref(it))

		preinc(it)

	return contour_list

cpdef dilate_contour_strip(DbImage edt, Image32 root, strip, double strip_width):
	cdef int p,q,i
	cdef double dist
	cdef Pixel px, px_adj, px_root
	cdef c_adjacency.AdjRel *A = c_adjacency.fast_circular(1.5)

	dilated_strip = set()
	Q = []

	for p in strip:
		Q.append(p)
		dilated_strip.add(p)

	while len(Q) > 0:
		p = Q.pop()

		px = usis.image.to_pixel(root.img, p)
		px_root = usis.image.to_pixel(root.img, <int>root[p])

		for i in xrange(1,A.n):
			px_adj = c_adjacency.next_adjacent(root.img, A, px, i)

			if usis.image.valid_pixel(root.img, px_adj):
				q = usis.image.to_index(root.img, px_adj)

				dist = abs(edt[q]) # Distance to root (EDT might be signed for
								   # evaluation purposes

				# Testing the root of q to make sure that
				# each pixel is only added once to the
				# dilated strip, and to exactly one strip
				if (dist <= strip_width and root[q] == root[p]
						and q not in dilated_strip):
					Q.append(q)
					dilated_strip.add(q)

	c_adjacency.destroy_adjrel(&A)

	return dilated_strip

def error_components(Image8 label, Image8 gt, Image8 errors):
	c_segmentation.error_components(label.img, gt.img, errors.img)

def label_bin_comp(Image8 bin, float radius):
	cdef c_adjacency.AdjRel *A = c_adjacency.fast_circular(radius)
	cdef Image32 label_comp = Image32()

	label_comp.from_image(c_segmentation.label_bin_comp(bin.img, A), True)

	c_adjacency.destroy_adjrel(&A)

	return label_comp

def label_union(Image8 dst, Image8 src, int label):
	c_segmentation.label_union(dst.img,  src.img, label)

def intersect_labels(Image8 label1, Image8 label2, int value):
	c_segmentation.intersect_labels(label1.img, label2.img, value)

def threshold(Image8 img, int low, int high):
	cdef Image8 thresh = Image8()

	thresh.from_image(c_segmentation.threshold(img.img, low, high), True)

	return thresh

def fast_smooth_ift(Image8 label, Image8 grad, int niterations):
	cdef c_segmentation.SegData *data = new c_segmentation.SegData(False)

	data.label = label.img
	data.grad = grad.img

	c_segmentation.fast_smooth_ift(data, niterations,
									c_segmentation.dist_grad)

	del data

# Binary Segmentation only.
def markers_from_gt(seeds, int cur_seed, Image8 gt_image,
						Image8 label, int dist_border,
						int max_marker_radius, int min_marker_radius,
						int mk_id, int primary_label,
						int secondary_label):

	cdef int total_seeds = 0
	cdef int i, q, p, chosen_lb = -1
	cdef double dist, closest_distance = -1
	cdef bool misclassified
	cdef AdjRel distance_border = AdjRel(radius = <float>(dist_border + max_marker_radius))
	cdef AdjRel marker_size

	p = cur_seed

	misclassified = ( (label is None) or ((gt_image[p] == 0) and (label[p] > 0) )
						or ( (gt_image[p] > 0) and (label[p] == 0) ))

	if p not in seeds and misclassified:
		closest_distance = INT_MAX

		u = gt_image.topixel(p)
		for i in xrange(1,distance_border.n):
			v = u + distance_border[i]
			if gt_image.valid_pixel(v):
				q = gt_image.toindex(v)
				dist = math.floor(math.sqrt(np.sum((u-v)**2)))
				if (gt_image[p] != gt_image[q]) and (closest_distance > dist):
					closest_distance = dist

		closest_distance = min(closest_distance - dist_border, max_marker_radius)

		if closest_distance >= min_marker_radius:
			marker_size = AdjRel(radius = closest_distance)

			for dxdy in marker_size:
				v = u + dxdy

				if gt_image.valid_pixel(v):
					q = gt_image.toindex(v)

					if gt_image[q] == 0:
						chosen_lb = secondary_label
					else:
						chosen_lb = primary_label

					seeds[q] = (mk_id, chosen_lb)

			total_seeds += 1

	return total_seeds, closest_distance, chosen_lb

