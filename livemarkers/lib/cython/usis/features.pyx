cimport c_features
from features cimport *
from npimage cimport *

cdef class Features:
	def __cinit__(self):
		self.f = NULL

	def __dealloc__(self):
		c_features.DestroyFeatures(&self.f)

	cdef from_feats(self, c_features.Features *feats):
		c_features.DestroyFeatures(&self.f)

		self.f = feats

	cpdef ycbcr_feats(self, CImage8 img, alpha):
		c_features.DestroyFeatures(&self.f)

		self.f = c_features.ycbcr_feats(img.img)

		self.weigh_features(alpha)

	cpdef ycbcr_feats2(self, Features rgb, alpha):
		c_features.DestroyFeatures(&self.f)

		self.f = c_features.ycbcr_feats(rgb.f)

		self.weigh_features(alpha)

	cpdef lab_feats(self, CImage8 cimg, alpha):
		c_features.DestroyFeatures(&self.f)

		self.f = c_features.lab_feats(cimg.img)

		self.weigh_features(alpha)

	cpdef ms_gauss_cimage8_feats(self, CImage8 cimg, int nscales):
		c_features.DestroyFeatures(&self.f)

		self.f = c_features.ms_gauss_cimage8_feats(cimg.img, nscales)

	cpdef ms_gauss_image8_feats(self, Image8 img, int nscales):
		c_features.DestroyFeatures(&self.f)

		self.f = c_features.ms_gauss_image8_feats(img.img, nscales)


	cpdef double max_arcweight(self):
		return c_features.max_arcweight(self.f)

	cpdef lab_feats2(self, Features rgb, alpha):
		c_features.DestroyFeatures(&self.f)

		self.f = c_features.lab_feats(rgb.f)

		self.weigh_features(alpha)

	cpdef normalize(self):
		cdef int i,z
		cdef float Fmax = float(self.f.Fmax)

		for i in xrange(self.f.nelems):
			for z in xrange(self.f.nfeats):
				self.f.elem[i].feat[z] = self.f.elem[i].feat[z]/Fmax

	cpdef weigh_features(self, alpha):
		cdef int p, x

		if alpha is not None:
			for p in xrange(self.f.nelems):
				for x in xrange(self.f.nfeats):
					self.f.elem[p].feat[x] *= alpha[x]

