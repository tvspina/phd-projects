
cdef extern from "gqueue.h":
	cdef struct GQNode:
		int  next  # next node
		int  prev  # prev node
		char color # WHITE=0, IFT_GRAY=1, IFT_BLACK=2


	cdef struct GDoublyLinkedLists:
		GQNode *elem  # all possible doubly-linked lists of the circular queue
		int nelems  # total number of elements
		int *value   # the value of the nodes in the graph


	cdef struct GCircularQueue:
		int  *first   # list of the first elements of each doubly-linked list
		int  *last    # list of the last  elements of each doubly-linked list
		int  nbuckets # number of buckets in the circular queue
		int  minvalue  # minimum value of a node in queue
		int  maxvalue  # maximum value of a node in queue
		char tiebreak # 1 is LIFO, 0 is FIFO (default)
		char removal_policy # 0 is MINVALUE and 1 is MAXVALUE


	cdef struct GQueue: # Priority queue by Dial implemented as
						# proposed by A. Falcao
		GCircularQueue C
		GDoublyLinkedLists L


	GQueue *CreateGQueue(int nbuckets, int nelems, int *value)
	void   DestroyGQueue(GQueue **Q)
	void   ResetGQueue(GQueue *Q)
	int    EmptyGQueue(GQueue *Q)
	void   InsertGQueue(GQueue **Q, int elem)
	int    RemoveGQueue(GQueue *Q)
	void   RemoveGQueueElem(GQueue *Q, int elem)
	void   UpdateGQueue(GQueue **Q, int elem, int newvalue)
	GQueue *GrowGQueue(GQueue **Q, int nbuckets)
