from c_common cimport *
from image cimport *
from libcpp cimport bool

cdef extern from "usisview.h" namespace "usis":

	cdef CImage8 *cwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, bool fill)
	cdef CImage8 *cwide_highlight(CImage8 *cimg, Image32 *regions, float radius, int color, bool fill)
	cdef CImage8 *cb_wide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, int bkgcolor, bool fill)

	cdef CImage8 *cwide_highlight_lb(CImage8 *cimg, Image8 *label, float radius, int *colormap, bool fill)
	cdef CImage8 *highlight_obj_bkg_lb(CImage8 *cimg, Image8 *objlabel, Image8 *bkglabel, float radius, int objcolor, int bkgcolor, bool fill)
	cdef CImage8 *highlight_lb_n_bkg_regions(CImage8 *cimg, Image8 *label, Image32 *regions, float radius, int objcolor, int bkgcolor, bool fill)
	cdef CImage8 *wb_highlight_lb_n_regions(CImage8 *cimg, Image8 *label, Image32 *regions, float radius, int objcolor, int bkgcolor, bool fill)


	#In-place functions **/
	cdef void icwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, bool fill)
	cdef void icwide_highlight(CImage8 *cimg, Image32 *label, float radius, int color, bool fill)

	cdef void icwide_highlight_bin_mask(CImage8 *cimg, Image8 *gt, float radius, int color, bool fill)

	cdef void icwide_highlight_lb(CImage8 *cimg, Image8 *label, float radius, int *colormap, bool fill)
	cdef void icwide_highlight_lb(CImage8 *cimg, Image32 *label, float radius, int *colormap, bool fill)
	cdef void icwide_highlight_bkg(CImage8 *cimg, Image8 *label, float radius, int *colormap, bool border,
									float alpha)

	cdef void icbwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, int bkgcolor, bool fill)
	# combines cimg with the label colors using an alpha value
	cdef void icfill_lb(CImage8 *cimg, Image8 *label, int *colormap, int sel_label, float alpha)

	cdef void ihighlight_lb_n_bkg_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
											float radius, int objcolor, int bkgcolor, bool fill)
	cdef void ihighlight_lb_n_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
										float radius, int* colormap, bool fill)

