# Interface with Numpy
from libcpp cimport bool
from libc.stdlib cimport *
from usis.c_common cimport *
cimport usis.image
from color cimport *
import numpy as np
cimport numpy as np

from cpython cimport PyObject, Py_INCREF

cdef class UcharArrayWrapper:
	cdef void* data_ptr
	cdef int shape
	cdef bool own_data

	cdef set_data(self, int shape, void* data_ptr,
					bool own_data)

cdef class UcharArray2DWrapper:
	cdef void* data_ptr
	cdef tuple shape
	cdef bool own_data

	cdef set_data(self, tuple shape, void* data_ptr,
					bool own_data)

cdef class IntArrayWrapper:
	cdef void* data_ptr
	cdef int shape
	cdef bool own_data

	cdef set_data(self, int shape, void* data_ptr,
					bool own_data)

cdef class IntArray2DWrapper:
	cdef void* data_ptr
	cdef tuple shape
	cdef bool own_data

	cdef set_data(self, tuple shape, void* data_ptr,
					bool own_data)

cdef class DbArrayWrapper:
	cdef void* data_ptr
	cdef int shape
	cdef bool own_data

	cdef set_data(self, int shape, void* data_ptr,
					bool own_data)

cdef class DbArray2DWrapper:
	cdef void* data_ptr
	cdef tuple shape
	cdef bool own_data

	cdef set_data(self, tuple shape, void* data_ptr,
					bool own_data)

cdef np.ndarray from_uchar_array(shape, uchar *data,
									bool own_data)


cdef np.ndarray from_int_array(shape, int *data,
									bool own_data)

cdef np.ndarray from_double_array(shape, double *data,
									 bool own_data)

cdef class Image8:
	cdef usis.image.Image8 *img
	cdef public np.ndarray arr
	cdef int _it #iterator index

	cdef bool __created_from_img__
	cdef bool __own_image__

	cdef void __free__(self)

	cpdef topixel(self, int p)
	cpdef toindex(self, xy)
	cpdef valid_pixel(self, xy)

	cpdef initialize(self, int ncols, int nrows)

	cpdef from_file(self, filename)
	cpdef from_array(self, np.ndarray[np.uint8_t, ndim=2, mode='c'] array)

	cdef from_image(self, usis.image.Image8 *img, bool own_image)

	cpdef add_frame(self, int sz, unsigned char value)
	cpdef write(self, filename)

cdef class Image32:
	cdef usis.image.Image32 *img
	cdef public np.ndarray arr
	cdef int _it #iterator index

	cdef bool __created_from_img__
	cdef bool __own_image__

	cdef void __free__(self)

	cpdef topixel(self, int p)
	cpdef toindex(self, xy)
	cpdef valid_pixel(self, xy)

	cpdef initialize(self, int ncols, int nrows)

	cpdef from_file(self, filename)
	cpdef from_array(self, np.ndarray[np.int32_t, ndim=2, mode='c'] array)

	cdef from_image(self, usis.image.Image32 *img, bool own_image)

	cpdef add_frame(self, int sz, int value)
	cpdef write(self, filename)

	cpdef to_8bits(self)
	cpdef from_8bits(self, Image8 img8)

cdef class DbImage:
	cdef usis.image.DbImage *img
	cdef np.ndarray arr
	cdef int _it #iterator index

	cdef bool __created_from_img__
	cdef bool __own_image__

	cdef void __free__(self)

	cpdef topixel(self, int p)
	cpdef toindex(self, xy)
	cpdef valid_pixel(self, xy)

	cpdef initialize(self, int ncols, int nrows)

	cpdef from_array(self, np.ndarray[np.float64_t, ndim=2, mode='c'] array)
	cdef from_image(self, usis.image.DbImage *img, bool own_image)


cdef class CImage8:
	cdef usis.image.CImage8 *img

	cdef void __free__(self)

	cpdef topixel(self, int p)
	cpdef toindex(self, xy)
	cpdef valid_pixel(self, xy)

	cpdef initialize(self, int ncols, int nrows)

	cpdef bool is_grayscale(self)

	cpdef from_file(self, filename)
	cpdef from_array(self, np.ndarray[np.uint8_t, ndim = 3, mode = 'c'] arr)
	cdef from_image(self, usis.image.CImage8 *cimg)

	cpdef from_image8(self, Image8 img)
	cpdef add_frame(self, int sz, unsigned char r, unsigned char g,
					unsigned char b)
	cpdef write(self, filename)

cpdef np.ndarray cimage8_file_2_ndarray(filename)
cdef np.ndarray cimage8_2_ndarray(usis.image.CImage8 *cimg)

cdef usis.image.CImage8* ndarray_2_cimage8(np.ndarray[np.uint8_t, ndim = 3, mode = 'c'] arr)

#cpdef CImage8 concat(CImage8 img1, CImage8 img2)
#cpdef concat_inplace(CImage8 img1, CImage8 img2, CImage8 dst)

# MISC

cpdef random_color()
cpdef triplet_py(int a,int b,int c)
