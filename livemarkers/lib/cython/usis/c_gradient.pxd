from libcpp cimport bool

from c_common cimport *
from c_adjacency cimport *
from image cimport *
cimport c_features

cdef extern from "usisgradient.h" namespace "usis":
	cdef c_features.Features *vimage_gradient(Image8 *img, float radius, float *Omax,
									Rectangle bb)
	cdef void vimage_gradient(c_features.Features *grad, Image8 *img, float radius,
								float *Omax, Rectangle bb)

	cdef DbImage *vimage_gradient2(Image8 *img, float radius, double *Omax, Rectangle bb)

	cdef void vimage_gradient2(DbImage *grad, Image8 *img, float radius,
								double *Omax, Rectangle bb)

	cdef void image_gradient_on_mask(DbImage *grad, Image8 *img,
										float radius, double *Omax, Rectangle bb)

	cdef Image8 *image_gradient(Image32 *img, float radius)
	cdef Image8 *image_gradient(Image8 *img, float radius)

	cdef c_features.Features *vfeatures_gradient(c_features.Features *f, float radius, float *Fmax)
	cdef void vfeatures_gradient(c_features.Features *grad, c_features.Features *f,float radius, float *Fmax,
									Rectangle bb)

	cdef Image8 *feat_obj_weight_image(c_features.Features *Gfeat, c_features.Features *Gobj,
										float Wobj, float Fmax, float Omax,
										Rectangle bb)

	cdef void feat_obj_weight_image(c_features.Features *Gfeat, c_features.Features *Gobj, Image8 *img,
										float Wobj, float Fmax, float Omax,
										Rectangle bb)

    # Combining feature, object, and cloud based gradients
	cdef void feat_obj_cloud_weight_image(c_features.Features *Gfeat, DbImage *Gobj,
											Image8 *cloud_grad,
											Image8 *final_grad,
											double Wobj, double Wfeat, double Wcloud,
											float Fmax, float Omax,
											Pixel grad_center, Pixel cloud_position,
											Rectangle bb)

	cdef Image8* weighted_image_gradient(CImage8 *cimg, AdjRel *A, float channel_weight[3])

	cdef c_features.Features* weighted_feature_gradient(c_features.Features *feat, float *Fmax, AdjRel *A,
														float *channel_weight)
