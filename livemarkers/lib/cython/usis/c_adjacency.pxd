from image cimport *

cdef extern from "adjacency.h":
	struct AdjRel:
		int *dx
		int *dy
		int n

	AdjRel *create_adjrel(int n)
	void destroy_adjrel(AdjRel **A)

	inline Pixel adjacent(AdjRel *A, Pixel v, int i)

	Pixel next_adjacent(Image8 *img, AdjRel *A, Pixel v, int &cur_idx)
	Pixel next_adjacent(Image32 *img, AdjRel *A, Pixel v, int &cur_idx)
	Pixel next_adjacent(CImage8 *img, AdjRel *A, Pixel v, int &cur_idx)
	Pixel next_adjacent(CImage32 *img, AdjRel *A, Pixel v, int &cur_idx)
	Pixel next_adjacent(DbImage *img, AdjRel *A, Pixel v, int &cur_idx)

	AdjRel *circular(float r)
	AdjRel *fast_circular(float r)

	int FrameSize(AdjRel *A)
	AdjRel *box(int ncols, int nrows)

	int adjacent_index(AdjRel *A, Pixel px, Pixel px_adj)
