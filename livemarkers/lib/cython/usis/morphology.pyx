cimport c_morphology
from npimage cimport *
from adjacency cimport *

cpdef Image8 dilate(Image8 img, AdjRel A):
	cdef Image8 dil = Image8()
	dil.from_image(c_morphology.dilate(img.img, A.A), True)

	return dil

cpdef Image8 erode(Image8 img, AdjRel A):
	cdef Image8 ero = Image8()
	ero.from_image(c_morphology.erode(img.img, A.A), True)

	return ero


cpdef Image8 object_border(Image8 bin):
	cdef Image8 border = Image8()
	border.from_image(c_morphology.object_border(bin.img), True)

	return border

cpdef Image8 close_holes(Image8 img):
	cdef Image8 closed = Image8()
	closed.from_image(c_morphology.close_holes(img.img), True)

	return closed

cpdef Image32 edt(Image8 I, Image32 root = None, int thrsh = 0):
	cdef Image32 dist = Image32()
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		dist.from_image(c_morphology.edt(I.img, &R, thrsh), True)
		root.from_image(R, True)
	else:
		dist.from_image(c_morphology.edt(I.img, NULL, thrsh), True)

	return dist


cpdef edt_inplace(Image8 I, Image32 edt, Image32 root = None, int thrsh = 0):
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		c_morphology.edt_inplace(I.img, edt.img, &R, thrsh)
		root.from_image(R, True)
	else:
		c_morphology.edt_inplace(I.img, edt.img, NULL, thrsh)


cpdef DbImage signed_edtdb(Image8 B, Image8 I, Image32 root = None, int thrsh = 0):
	cdef DbImage dist = DbImage()
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		dist.from_image(c_morphology.signed_edtdb(B.img, I.img, &R, thrsh), True)
		root.from_image(R, True)
	else:
		dist.from_image(c_morphology.signed_edtdb(B.img, I.img, NULL, thrsh), True)

	return dist


cpdef DbImage edtdb(Image8 I, Image32 root = None, int thrsh = 0):
	cdef DbImage dist = DbImage()
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		dist.from_image(c_morphology.edtdb(I.img, &R, thrsh), True)
		root.from_image(R, True)
	else:
		dist.from_image(c_morphology.edtdb(I.img, NULL, thrsh), True)

	return dist


cpdef edtdb_inplace(Image8 I, DbImage edt, Image32 root = None, int thrsh = 0):
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		c_morphology.edt_inplace(I.img, edt.img, &R, thrsh)
		root.from_image(R, True)
	else:
		c_morphology.edt_inplace(I.img, edt.img, NULL, thrsh)



cpdef Image32 edt32(Image32 I, Image32 root = None, int thrsh = 0):
	cdef Image32 dist = Image32()
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		dist.from_image(c_morphology.edt(I.img, &R, thrsh), True)
		root.from_image(R, True)
	else:
		dist.from_image(c_morphology.edt(I.img, NULL, thrsh), True)


	return dist


cpdef edt_inplace32(Image32 I, Image32 edt, Image32 root = None, int thrsh = 0):
	cdef usis.image.Image32 *R = NULL

	if root is not None:
		c_morphology.edt_inplace(I.img, edt.img, &R, thrsh)
		root.from_image(R, True)
	else:
		c_morphology.edt_inplace(I.img, edt.img, NULL, thrsh)


cpdef Image8 close_rec(Image8 img, AdjRel A):
	cdef Image8 closed = Image8()

	closed.from_image(c_morphology.close_rec(img.img, A.A), True)

	return closed

cpdef Image8 open_rec(Image8 img, AdjRel A):
	cdef Image8 opened = Image8()

	opened.from_image(c_morphology.open_rec(img.img, A.A), True)

	return opened

cpdef Image32 open_rec32(Image32 img, AdjRel A):
	cdef Image32 opened = Image32()

	opened.from_image(c_morphology.open_rec(img.img, A.A), True)

	return opened

cpdef Image32 label_comp(Image32 img, AdjRel A, int thres):
	cdef Image32 labeled = Image32()

	labeled.from_image(c_morphology.label_comp(img.img, A.A, thres), True)

	return labeled
