cdef extern from "realheap.h":

	cdef struct RealHeap:
		float *cost
		char *color
		int *pixel
		int *pos
		int last
		int n
		char removal_policy # 0 is MINVALUE and 1 is MAXVALUE

	cdef void SetRemovalPolicyRealHeap(RealHeap *H, char policy)
	cdef char IsFullRealHeap(RealHeap *H)
	cdef char IsEmptyRealHeap(RealHeap *H)
	cdef RealHeap *CreateRealHeap(int n, float *cost)
	cdef void DestroyRealHeap(RealHeap **H)
	cdef char InsertRealHeap(RealHeap *H, int pixel)
	cdef char RemoveRealHeap(RealHeap *H, int *pixel)
	cdef void UpdateRealHeap(RealHeap *H, int p, float value)
	cdef void GoUpRealHeap(RealHeap *H, int i)
	cdef void GoDownRealHeap(RealHeap *H, int i)
	cdef void ResetRealHeap(RealHeap *H)
