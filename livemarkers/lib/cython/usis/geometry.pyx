from npimage cimport *
cimport c_geometry
cimport image
import numpy as np
from libcpp cimport bool

def vector_direction(np.ndarray vec):
	cdef c_geometry.Vector2D vec2d = c_geometry.Vector2D(<double>vec[0], <double>vec[1])
	cdef c_geometry.Vector2D origin = c_geometry.Vector2D(1.0, 0.0)

	return vec2d.Direction(origin)

def vector_angle(np.ndarray vec, np.ndarray vec2):
	cdef c_geometry.Vector2D vec2d = c_geometry.Vector2D(<double>vec[0], <double>vec[1])
	cdef c_geometry.Vector2D vec2d_2 = c_geometry.Vector2D(<double>vec2[0], <double>vec2[1])

	return vec2d.Angle(vec2d_2)


def mask_centroid(Image8 mask, int label):
	cdef np.ndarray px_nd
	cdef Pixel px

	px = c_geometry.mask_centroid(mask.img, label)

	return np.array((px.x, px.y), np.int32)

def mask_centroid32(Image32 mask, int label):
	cdef np.ndarray px_nd
	cdef Pixel px

	px = c_geometry.mask_centroid(mask.img, label)

	return np.array((px.x, px.y), np.int32)

def scl_rot_trans(float sx, float sy, float th, float dx, float dy,
					int x_in, int y_in, int x_out, int y_out,
					bool direct = False):

	cdef np.ndarray[np.float32_t, ndim=2, mode='c'] T
	cdef float M[4][4]
	T = np.zeros((4,4), dtype=np.float32, order = 'C')

	c_geometry.scl_rot_trans(sx, sy, th, dx, dy, M,
								x_in, y_in, x_out, y_out,
								direct)
	for i in xrange(4):
		for j in xrange(4):
			T[i,j] = M[i][j]

	return T

def transform_image(CImage8 img, np.ndarray[np.float32_t, ndim=2, mode='c'] M,
					int ncols, int nrows, Image8 selected_pixels):
	cdef CImage8 out = CImage8()
	cdef image.Image8 *tmp = NULL
	cdef float T[4][4]

	for i in xrange(4):
		for j in xrange(4):
			T[i][j] = M[i,j]


	if(selected_pixels is None):
		out.from_image(c_geometry.transform_image(img.img, T, ncols, nrows, NULL))
	else:
		out.from_image(c_geometry.transform_image(img.img, T, ncols, nrows, &tmp))
		selected_pixels.from_image(tmp, True)

	return out
