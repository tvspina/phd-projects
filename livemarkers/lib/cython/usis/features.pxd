cimport c_features
from npimage cimport *

cdef class Features:
	cdef c_features.Features *f

	cdef from_feats(self, c_features.Features *feats)

	cpdef ycbcr_feats(self, CImage8 img, weights)
	cpdef ycbcr_feats2(self, Features rgb, weights)

	cpdef lab_feats(self, CImage8 cimg, weights)
	cpdef lab_feats2(self, Features rgb, weights)

	cpdef ms_gauss_cimage8_feats(self, CImage8 cimg, int nscales)
	cpdef ms_gauss_image8_feats(self, Image8 img, int nscales)
	cpdef double max_arcweight(self)
	cpdef normalize(self)
	cpdef weigh_features(self, weights)
