cimport usis.view
from npimage cimport *
from os.path import exists

def triplet(a,b,c):
	return ((a&0xff)<<16)|((b&0xff)<<8)|(c&0xff)

def t0(a):
	return ((a>>16)&0xff)
def t1(a):
	return ((a>>8)&0xff)
def t2(a):
	return (a&0xff)

def triplet2rgb(a):
	return (t0(a),t1(a),t2(a))

cpdef highlight_label(CImage8 npimg, Image8 label, np.ndarray[np.int32_t, ndim=1, mode='c']
						colormap, int highlight_opt,
						float radius, bool fill):
	if highlight_opt == 2:
		usis.view.icfill_lb(npimg.img, label.img, <int*>colormap.data, NIL, 0.7)
	elif highlight_opt == 3:
		usis.view.icwide_highlight_bkg(npimg.img, label.img, radius, <int*>colormap.data, fill, 0.3)
	else:
		usis.view.icwide_highlight_lb(npimg.img, label.img, radius, <int*>colormap.data, fill)

cpdef highlight_label32(CImage8 npimg, Image32 label, np.ndarray[np.int32_t, ndim=1, mode='c']
						colormap, float radius, bool fill):
	usis.view.icwide_highlight_lb(npimg.img, label.img, radius, <int*>colormap.data, fill)


cpdef highlight_gt(CImage8 npimg, Image8 gt, float radius, color, bool fill):
	cdef int c = triplet(color[0], color[1], color[2])

	icwide_highlight_bin_mask(npimg.img, gt.img, radius, c, fill)

cpdef highlight_lb_regions(CImage8 cimg, Image8 label, Image32 regions,
							float radius, np.ndarray[np.int32_t, ndim=1, mode='c']
							colormap, bool fill):
	ihighlight_lb_n_regions(cimg.img, label.img, regions.img,
								radius, <int*>colormap.data, fill)


cpdef highlight_bkg(CImage8 cimg, Image8 label, float radius,
					np.ndarray[np.int32_t, ndim=1, mode='c'] colormap, bool opaque):
	if not opaque:
		usis.view.icwide_highlight_bkg(cimg.img, label.img, 0.0,
										<int*>colormap.data, False, 0.7)
	else:
		usis.view.icbwide_highlight(cimg.img, label.img, 0.0,
									<int>colormap[1], <int>colormap[0], True)

cpdef np.ndarray initialize_colormap(bool load_colormap):
	cdef int ncolors = 256, last_read_color = 0
	cdef int *colormap = AllocIntArray(ncolors)

	if load_colormap:
		if exists('colormap.txt'):

			f_colormap = open('colormap.txt')

			print 'Loading colormap\n'

			for line in f_colormap.readlines():
				values = line.split()
				l = int(values[0])
				r = int(values[1])
				g = int(values[2])
				b = int(values[3])
				colormap[l] = triplet(r,g,b)
				last_read_color = last_read_color + 1

			f_colormap.close()


	for i in xrange(last_read_color, ncolors):
		colormap[i] = randomColor()

	return from_int_array((ncolors), colormap, True)
