from c_common cimport *
cimport image
from c_geometry cimport *
cimport c_features
from c_morphology cimport *
from realheap cimport *
from gqueue cimport *
cimport c_adjacency
from set cimport *

from libcpp.pair cimport *
from libcpp.queue cimport *
from libcpp.stack cimport *
from libcpp.set cimport set as cppset
from libcpp.vector cimport *
from tr1.unordered_map cimport *
from tr1.unordered_set cimport *

cdef extern from "graph.h":
	cppclass Graph:
		pass

cdef extern from "usissegmentation.h":

	cdef:
		enum USIS_LW_ORI:
			USIS_LW_NOORIENTATION
			USIS_LW_CLOCKWISE
			USIS_LW_COUNTERCLOCKWISE

		int USIS_DEFAULT_SEG_BKG_LABEL = 0
		int USIS_DEFAULT_SEG_OBJ_LABEL = 1
		int USIS_MAX_N_SEG_LABELS = 100

cdef extern from "usissegmentation.h" namespace "usis":

	struct MarkerData:
		int mk_id, lb
		# Segmentation data

	cppclass SegData:

		image.Image8  *grad
		image.Image32 *cost
		image.Image8  *label
		image.Image32 *pred
		image.Image32 *root
		image.Image32 *regions
		image.Image8  *objMap

		c_features.Features *feat


		float Wobj
		float Fmax # maximum feature-based arc-weight
		float *fcost
		double power
		int Wmax # maxmimum object map value
		float ift_radius

		unordered_map[int, MarkerData] seeds

		SegData(bool free)

	void SetFreeData(bool free)


	inline float dist_grad(SegData *data, int p, int q)

	inline float fmax(SegData *data, float cst, float weight)

	inline float fsum_power(SegData *data, float cst, float weight)

	cdef cppclass DelineationAlg:
		pass
	cdef cppclass DelineationIFT_SC (DelineationAlg):
		pass

	# Region-based Segmentation
	cdef cppclass DelineationAlg:
		c_adjacency.AdjRel *A
		SegData *data

		DelineationAlg()

		double DelineateAndRecognize()

		void SetBarrier()
		void SetBarrier(int p, uchar label)
		void SetSeed(int p, uchar label, int mk_id)
		void SetUnknownSample(int p)
		void ResetData()
		bool ResetData(Pixel px)


		void SetData(SegData *seg_data)


	#IFT-SC using GQueue pr Realheap
	#
	#Because the uncertainty region is fairly small,
	#this class may be used with either the Fmax function
	#or Fsum raised to a power. The latter provides smoother
	#borders.

	cdef cppclass DelineationIFT_SC (DelineationAlg):
		GQueue *Q
		RealHeap *H
		float *fcost
		double fsum_power

		DelineationIFT_SC(int nbuckets, int n, int *cost,
							   float (*cost_function)(SegData *, float, float))

		DelineationIFT_SC(int n, float (*cost_function)(SegData *,float, float),
							double fsum_power)

		double DelineateAndRecognize()

		void SetBarrier()
		void SetBarrier(int p)
		void SetSeed(int p, uchar label, int mk_id)
		void SetUnknownSample(int p)

		void SetData(SegData *seg_data)
		void ResetData()
		bool ResetData(Pixel px)

	# DIFT
	Set *forest_removal_by_regions(SegData *data, c_adjacency.AdjRel *A, Set *delSet, int defaut_label)
	void differential_ift_by_regions(SegData *data, c_adjacency.AdjRel *A, Set *delSet,
									 float(*weight_function)(SegData*, int, int),
									 float (*cost_function)(SegData*, float, float))
	void dift_by_regions(SegData *data, c_adjacency.AdjRel *A, Set *delSet, GQueue **Q,
						 float (*weight_function)(SegData*, int, int),
						 float (*cost_function)(SegData*, float, float))

	Set *forest_removal(SegData *data, c_adjacency.AdjRel *A, unordered_map[int, MarkerData] delSet)
#
#    # Delineation with Recognition Functional using GQueu
	void differential_ift(SegData *data, c_adjacency.AdjRel *A, unordered_map[int, MarkerData] delSet,
                            float (*weight_function)(SegData*, int, int),
                            float (*cost_function)(SegData*, float, float))

	void dift(SegData *data, c_adjacency.AdjRel *A, unordered_map[int, MarkerData] delSet,
				GQueue **Q, float (*weight_function)(SegData*, int, int),
                float (*cost_function)(SegData*, float, float))

#
	Set *forest_removal_heap(SegData *data, c_adjacency.AdjRel *A, unordered_map[int, MarkerData] delSet)

	double differential_ift_heap(SegData *data, c_adjacency.AdjRel *A, unordered_map[int, MarkerData] delSet,
								float(*weight_function)(SegData*, int, int),
								float (*cost_function)(SegData*, float, float))

	double dift(SegData *data, c_adjacency.AdjRel *A, unordered_map[int, MarkerData] delSet,
				RealHeap **Q, float(*weight_function)(SegData*, int, int),
				float (*cost_function)(SegData*, float, float))

	# IFT-SC
	double ift_sc_and_mean_cut(SegData *data, c_adjacency.AdjRel *A, int nbuckets,
							   float (*weight_function)(SegData*, int, int),
							   float (*cost_function)(SegData*, float, float))
	double ift_sc_and_mean_cut(SegData *data, c_adjacency.AdjRel *A, GQueue **Q,
							   float (*weight_function)(SegData*, int, int),
							   float (*cost_function)(SegData*, float, float))
	# IFT-SC using heap
	double ift_sc_and_mean_cut_heap(SegData *data, c_adjacency.AdjRel *A,
										  float (*weight_function)(SegData*, int, int),
										  float (*cost_function)(SegData*, float, float))
	double ift_sc_and_mean_cut(SegData *data, c_adjacency.AdjRel *A, RealHeap *Q,
							   float (*weight_function)(SegData*, int, int),
							   float (*cost_function)(SegData*, float, float))


	# Misc superpixel-based segmentation
	void watergray(SegData *data, image.Image8 *img, image.Image32 *marker, c_adjacency.AdjRel *A)
	void watergray(SegData *data, image.Image32 *img, image.Image32 *marker, c_adjacency.AdjRel *A)

	vector[vector[image.Image32Ptr] ] gbh(vector[CImage8Ptr] images, float c,
									float c_reg, int min_size, float sigma,
									int hie_num)

	#Max-flow/Min-cut
	void max_flow(SegData *data, AdjRel *A, float mf_lambda, float mf_power,
					float (*weight_function)(SegData*, int, int),
					Graph **graph, void* **nodes)

	# Boundary-based segmentation

	# Live-wire-on-the-fly and Riverbed
	int lwof_cost(image.Image8 *grad, image.Image32 *cost, image.Image32 *pred, image.Image8 *obj,
					int p, int q, int Wmax, int Cmax,
					double power, c_adjacency.AdjRel *L, c_adjacency.AdjRel *R, int i,
					USIS_LW_ORI ori, bool force_ori)

	int riverbed_cost(image.Image8 *grad, image.Image32 *cost, image.Image32 *pred, image.Image8 *obj,
					int p, int q, int Wmax, int Cmax,
					double power, c_adjacency.AdjRel *L, c_adjacency.AdjRel *R, int i,
					USIS_LW_ORI ori, bool force_ori)

	void contour_tracking(image.Image8 *grad, image.Image32 *cost, image.Image32 *pred, image.Image8 *obj,
    				      	GQueue **Q, c_adjacency.AdjRel *A, int Wmax, USIS_LW_ORI ori, int dst,
							double power, bool force_ori,
					        int (*path_cost)(image.Image8 *grad, image.Image32 *cost, image.Image32 *pred, image.Image8 *obj,
											int p, int q, int Wmax, int Cmax,
											double power, c_adjacency.AdjRel *L, c_adjacency.AdjRel *R, int i,
											USIS_LW_ORI ori, bool force_ori))

	int *path2array(image.Image32 *pred, int dst, int src)

	unordered_map[int, MarkerData]* path_seeds(vector[pair[int, double]] &anchors, image.Image32 *pred, image.Image32 *cost,
													int primary_label, int left_pixel_label,
													int right_pixel_label, int mk_id,
													unordered_multimap[int, int] *root_inv_index)

	# Returns @param dst UNION @param src inplace in dst, where the only
	#    pixel values to be copied from src to dst are those whose label is
	#    @param label.
	void label_union(image.Image8 *dst, image.Image8 *src, int label)
	void intersect_labels(image.Image8 *label1, image.Image8 *label2, int value)
	void intersect_labels_bb(image.Image8 *label1, image.Image8 *label2, int value, int xmin,
								int ymin, int xmax, int ymax)

	# Util
	void fast_smooth_ift(SegData *data, int niterations,
						float(*weight_function)(SegData*, int, int))

	image.Image8* threshold(image.Image8 *img, int lower, int higher)
	image.Image32* threshold(image.Image32 *img, int lower, int higher)
	image.Image8* binarize(image.Image8 *img, cppset[uchar] &values)
	image.Image32* binarize(image.Image8 *img, cppset[int] &values)

	image.Image32 *label_bin_comp(image.Image8 *bin, c_adjacency.AdjRel *A)
	image.Image32 *label_bin_comp(image.Image32 *bin, c_adjacency.AdjRel *A)

	void error_components(image.Image8 *label, image.Image8 *gt, image.Image8 *errors)

	void relabel_regions(image.Image32 *regions)

	vector[int*] image_contours(image.Image8 *bin, int thrsh)
