cimport usis.image
cimport c_geometry
from libcpp cimport bool

cdef extern from "usisevaluation.h" namespace "usis":
	double precision(int tp, int fp)

	double recall(int tp, int fn)

	double fmeasure(double precision, double recall)

	double compute_fmeasure(usis.image.Image8 *label, usis.image.Image8 *mask)

	void conf_matrix(usis.image.Image8 *label, usis.image.Image8 *mask,
						int *truepos, int *trueneg,
						int *falsepos, int *falseneg,
						int lb, int lb_mask, c_geometry.Rectangle _bb)

	void sgn_edt_error_measures(usis.image.Image8 *label, usis.image.Image8 *gt, float *max_dist_pos,
								float *max_dist_neg,
								float *fp_dist_sum, float *fn_dist_sum,
								int *fp_size, int *fn_size, int *border_size)


	#	@param changes image which counts the number of label changes for each pixel
	#	@param n_si number of internal markers
	#	@param n_se number of external markers
	#	@param avg_changes average number of label changes (i.e., (total amount of changes)/(image size))
	#	@param avg_changed_pixels average number of pixels which suffered a label change (i.e.,
	#		(number of pixels whose label changed at least once)/(image size))
	#	@param avg_changes_by_mk equals to avg_changes/(n_si + n_se)
	#	@param avg_changed_pixels_by_mk equals to avg_changed_pixels/(n_si + n_se)
	#	@param avg_changes_by_changed_pixels number of changes divided by the number of changed pixels
	#	@param controllability equals to 1 - avg_changes_by_mk
	void controllability_measures(usis.image.Image32 *changes, int niterations,
	                              float *avg_changes, float *avg_changed_pixels,
	                              float *avg_changes_by_changed_pixels,
	                              float *controllability,
	                              int *n_changes, int *n_changed_pixels)

	void num_label_changes(usis.image.Image32 *changes, int *n_changes,
							int *n_changed_pixels)
