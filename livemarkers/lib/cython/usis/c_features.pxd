from image cimport *
from subgraph cimport *

cdef extern from "usisfeatures.h":
	cdef struct FElem:
		float *feat

	cdef struct Features:
		FElem *elem
		int  nfeats
		int  nelems
		int  nrows, ncols
		float Fmax


	cdef Features* CreateFeatures(int ncols, int nrows, int nfeats)
	cdef void DestroyFeatures(Features **f)

	void SetSubgraphFeatures(Subgraph *sg, Features *f)


cdef extern from "usisfeatures.h" namespace "usis":

	cdef float max_arcweight(Features *f)

	# Pthread
	cdef Features *ms_gauss_cimage8_feats_threaded(CImage8 *cimg, int nscales, int nthreads)

	Features* ycbcr_feats(CImage8* rgb)
	Features* ycbcr_feats(Features* rgb)

	# OMP support
	cdef Features *lab_feats(Features *rgb)
	cdef void lab_feats(Features *lab, Features *rgb)
	cdef Features *lab_feats(CImage8 *rgb)

	cdef Features *ms_gauss_image8_feats(Image8 *img, int nscales)
	cdef Features *ms_gauss_cimage8_feats(CImage8 *cimg, int nscales)

