from image cimport *
from c_adjacency cimport *

cdef extern from "ctree.h":
	cdef struct CTNode:
		int  level   # gray level
		int  comp    # representative pixel of this node
		int  dad     # dad node in the maxtree
		int *son     # son nodes in the maxtree
		int  numsons # number of sons in the maxtree
		int  size    # number of pixels of the node

	cdef struct CTree:
		CTNode *node     # nodes of the mtree
		Image32  *cmap     # component map
		int     root     # root node of the mtree
		int     numnodes # number of nodes of the maxtree

	cdef:
		CTree *CreateMaxTree(Image32 *img, AdjRel *A)
		CTree *CreateMinTree(Image32 *img, AdjRel *A)
		void   DestroyCTree(CTree **ctree)

		Image32 *CTAreaClose(Image32 *img, int thres)
		Image32 *CTAreaOpen(Image32 *img, int thres)
		Image32 *CTVolumeOpen(Image32 *img, int thres)
		Image32 *CTVolumeClose(Image32 *img, int thres)
