from c_adjacency cimport *
from image cimport *
from libcpp cimport bool
from libcpp.pair cimport *
from tr1.unordered_map cimport *

cdef extern from "usismorphology.h" namespace "usis":
    # Creates Empty Forest
	cdef struct Forest:
		Image32 *P # predecessor map
		Image32 *R # root map
		Image32 *V # distance (cost or connectivity) map

	cdef:
		Image8* object_border(Image8 *bin)
		Image8 *dilate(Image8 *img, AdjRel *A)
		Image32 *dilate(Image32 *img, AdjRel *A)
		Image8 *erode(Image8 *img, AdjRel *A)
		Image32 *erode(Image32 *img, AdjRel *A)

		Image8 *close(Image8 *img, AdjRel *A)
#		Image8 *morph_open(Image8 *img, AdjRel *A)
#		Image32 *morph_open(Image32 *img, AdjRel *A)

		Image8 *close_holes(Image8 *img)
		Image32 *close_holes(Image32 *img)
		Image8 *sup_rec(Image8 *img, Image32 *marker, AdjRel *A)
		Image32 *sup_rec(Image32 *img, Image32 *marker, AdjRel *A)
		Image8 *inf_rec(Image8 *img, Image32 *marker, AdjRel *A)
		Image32 *inf_rec(Image32 *img, Image32 *marker, AdjRel *A)

		Image8 *close_rec(Image8 *img, AdjRel *A)
		Image8 *open_rec(Image8 *img, AdjRel *A)
		Image32 *open_rec(Image32 *img, AdjRel *A)

		Image32 *label_comp(Image32 *img, AdjRel *A, int thres)
		Image8 *remove_region_by_area(Image8 *label, int area)
		Image32 *remove_region_by_area(Image32 *label, int area)

		Image8 *close_label_holes(Image8 *label, Image32 *root, int deflabel)
		Image8 *label_sup_rec(Image8 *img, Image32 *marker, Image32 *root, int deflabel, AdjRel *A)

		Image8 *image_xor(Image8 *img0, Image8 *img1)

		Image8 *morph_grad(Image8 *img0, AdjRel *A)

		#/ Signed euclidean distance transform (squared)
		#/ For pixels where the label I->val[p] == 0
		#/ the EDT value will be positive
		Image32 *signed_edt2(Image8 *B, Image8 *I, Image32 **root, int thrsh)
		Image32 *signed_edt(Image8 *B, Image8 *I, Image32 **root, int thrsh)

		Image32 *edt(Image8 *I, Image32 **root, int thrsh)
		void edt_inplace(Image8 *I, Image32 *edt, Image32 **root, int thrsh)


		DbImage* signed_edtdb(Image8 *B, Image8 *I, Image32 **root, int thrsh)
		DbImage* edtdb(Image8 *I, Image32 **root, int thrsh)
		void edt_inplace(Image8 *I, DbImage *edt, Image32 **root, int thrsh)

		Image32 *edt(Image32 *I, Image32 **root, int thrsh)
		void edt_inplace(Image32*I, Image32 *edt, Image32 **root, int thrsh)

		Forest *edt2(Image8 *I, int thrsh)
		Forest *edt2_inplace(Image8 *I, Image32 *edt, int thrsh)
		Forest *edt2(Image32 *I, int thrsh)
		Forest *edt2_inplace(Image32 *I, Image32 *edt, int thrsh)

		Forest *create_forest(int ncols, int nrows)
		void destroy_forest(Forest **F)

		Forest *create_forest(int ncols, int nrows)
		void destroy_forest(Forest **F)

		# Multiscale Skeletonization **/
		DbImage *ms_geodesic_skel(Image8 *I)

		double density(int p, Image8 *mask, int lb, AdjRel *A)
		unordered_map[int, pair[double, double] ] avg_density(Image8 *mask, AdjRel *A)
		double avg_density(Image8 *mask, int lb, AdjRel *A)
		Image8* filter_by_density(Image8 *mask, AdjRel *A)
		Image8* filter_by_density(Image8 *mask, int lb, AdjRel *A)
