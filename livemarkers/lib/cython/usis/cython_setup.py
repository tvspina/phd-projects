from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy
import sys
import os
import commands
from os.path import *

BASE_DIR= os.getenv('BASE_DIR')
CODE_C_HDR=join(BASE_DIR, 'lib/code_c/include')
CODE_C_LIB=join(BASE_DIR,'lib/code_c/lib')

CODE_CPP_HDR=join(BASE_DIR,'lib/code_cpp/include')
CODE_CPP_LIB=join(BASE_DIR,'lib/code_cpp/lib')
MF_LIB=join(BASE_DIR,'lib/maxflow-v2.2.src/lib')


CYTHON_HDR=join(BASE_DIR,'lib/cython')
USIS_HDR=join(BASE_DIR,'lib/cython/usis')
TR1_HDR=join(BASE_DIR,'lib/cython/tr1')
MF_HDR=join(BASE_DIR,'lib/maxflow-v2.2.src/adjacency_list')

COMPILE_FLAGS = ['-fopenmp']
LINK_FLAGS = ['-fopenmp']

#COMPILE_FLAGS = ['-g', '-fopenmp']
#LINK_FLAGS = ['-g', '-fopenmp']

#CV_LIBS = commands.getoutput('pkg-config --libs opencv').replace('-l','').split()
INCLUDE_DIRS = [numpy.get_include(),CODE_C_HDR, CODE_CPP_HDR, '.',CYTHON_HDR,TR1_HDR,USIS_HDR,
				MF_HDR]
LIBRARIES = ['code_cpp', 'code_c', 'm', 'pthread', 'boost_system','maxflow']

LIBRARY_DIRS =  [CODE_CPP_LIB, CODE_C_LIB, MF_LIB]

# Testing if the opencv libraries contain their full path.
# This might happen if one installs opencv from source. Hence,
# the libraries should be added to the LINK_FLAGS as opposed to libraries.
#if all(map(lambda x: os.path.dirname(x) != '',CV_LIBS)):
#	LINK_FLAGS = LINK_FLAGS + CV_LIBS
#else:
#	LIBRARIES = LIBRARIES + CV_LIBS

kwargs = {'include_dirs':INCLUDE_DIRS, 'library_dirs':LIBRARY_DIRS,
			'libraries':LIBRARIES, 'extra_compile_args':COMPILE_FLAGS,
			'extra_link_args':LINK_FLAGS, 'language':'c++'}

# Selecting all .pyx file in the current directory for compilation
files = filter(lambda x: x.lower().rfind('.pyx') >= 0, os.listdir('.'))
files = map(lambda x: x.strip().replace('.pyx',''), files)

ext_modules = [Extension(i, [i + '.pyx'], **kwargs) for i in files]

setup(cmdclass = {'build_ext': build_ext}, packages=['usis'], ext_modules = ext_modules)

