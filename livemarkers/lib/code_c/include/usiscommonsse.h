#ifndef _USISCOMMONSSE_H_
#define _USISCOMMONSSE_H_

#include <xmmintrin.h>

#ifdef __cplusplus
extern "C" {
#endif

float* usis_alloc_algn_float_array(const int n, const int align);

int* usis_alloc_algn_int_array(const int n, const int align);

unsigned char* usis_alloc_algn_uchar_array(const int n, const int align);

__m128 usis_mux_2to1(const __m128 x, const __m128 y, const __m128 s);

float usis_min_ps(__m128 A);

float usis_max_ps(__m128 A);

void usis_free(void *data);


 #ifdef __cplusplus
 }
 #endif

#endif // _COMMONSSE_H_
