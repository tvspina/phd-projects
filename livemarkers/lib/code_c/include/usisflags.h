#ifndef _USISFLAGS_H_
#define _USISFLAGS_H_

/// if this define is uncommented then USIS will compile as LiveMarkers
//#define USIS_LIVEMARKERS_GUI

#define USIS_MAX_NCANVAS 1

//#define USIS_VERBOSE

//#define USIS_EXPERIMENT_MODE

//#define USIS_PAPER_MODE

#define USIS_MAXGRAD 255

#define USIS_MAX_OBJMAP_VALUE 255

// 16 byte alignment
#define USIS_SSE_ALIGNMENT 16

#ifdef USIS_VERBOSE
    #define USIS_VERBOSE_PRINTF(...) fprintf(stderr, __VA_ARGS__);
#else
    #define USIS_VERBOSE_PRINTF(...)
#endif

#define USIS_NAME "CAVOS - Computer-Aided Video Object Segmentation"

#define USIS_TMP_DIR "cavos" // created in /tmp

#endif // _USISFLAGS_H
