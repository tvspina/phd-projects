#ifndef _USISSSEMATH_H_
#define _USISSSEMATH_H_

#include <xmmintrin.h>
/* yes I know, the top of this file is quite ugly */

#ifdef _MSC_VER /* visual c++ */
# define ALIGN16_BEG __declspec(align(16))
# define ALIGN16_END
#else /* gcc or icc */
# define ALIGN16_BEG
# define ALIGN16_END __attribute__((aligned(16)))
#endif

/* __m128 is ugly to write */
typedef __m128 v4sf;  // vector of 4 float (sse1)

#ifdef USE_SSE2
#include <emmintrin.h>
typedef __m128i v4si; // vector of 4 int (sse2)
#else
typedef __m64 v2si;   // vector of 2 int (mmx)
#endif

v4sf sin_ps(v4sf x);
v4sf cos_ps(v4sf x);
v4sf exp_ps(v4sf x);
v4sf log_ps(v4sf x);
void sincos_ps(v4sf x, v4sf *s, v4sf *c);

__m128 exp2f4(__m128 x);
__m128 log2f4(__m128 x);
inline __m128 powf4(__m128 x, __m128 y);

#endif //
