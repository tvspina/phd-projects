#include "usiscommonsse.h"

inline float* usis_alloc_algn_float_array(const int n, const int align)
{
    return (float *) _mm_malloc(n*sizeof(float),align);
}

inline int* usis_alloc_algn_int_array(const int n, const int align)
{
    return (int *) _mm_malloc(n*sizeof(int),align);
}

inline unsigned char* usis_alloc_algn_uchar_array(const int n, const int align)
{
    return (unsigned char*) _mm_malloc(n*sizeof(unsigned char),align);
}

inline __m128 usis_mux_2to1(const __m128 x, const __m128 y, const __m128 s)
{
    return _mm_or_ps(_mm_and_ps(x,s), _mm_andnot_ps(s,y));
}


inline float usis_min_ps(__m128 A)
{
	__m128 tmp;

	tmp = _mm_min_ps(A, _mm_shuffle_ps(A,A,_MM_SHUFFLE(2,1,0,3)));
	tmp = _mm_min_ps(tmp, _mm_shuffle_ps(A,A,_MM_SHUFFLE(1,0,3,2)));
	tmp = _mm_min_ps(tmp, _mm_shuffle_ps(A,A,_MM_SHUFFLE(0,3,2,1)));

	float minimum[4] __attribute__((aligned(16)));
	_mm_store_ps(minimum,tmp);

	return minimum[0];
}

inline float usis_max_ps(__m128 A)
{
	__m128 tmp;

	tmp = _mm_max_ps(A, _mm_shuffle_ps(A,A,_MM_SHUFFLE(2,1,0,3)));
	tmp = _mm_max_ps(tmp, _mm_shuffle_ps(A,A,_MM_SHUFFLE(1,0,3,2)));
	tmp = _mm_max_ps(tmp, _mm_shuffle_ps(A,A,_MM_SHUFFLE(0,3,2,1)));

	float maximum[4] __attribute__((aligned(16)));
	_mm_store_ps(maximum,tmp);

	return maximum[0];
}


inline void usis_free(void *data)
{
	if(data != NULL) _mm_free(data);
}
