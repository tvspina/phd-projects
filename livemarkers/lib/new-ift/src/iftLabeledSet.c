#include "iftLabeledSet.h"

void iftInsertLabeledSet(iftLabeledSet **S, int elem, int label)
{
  iftLabeledSet *p=NULL;

  p = (iftLabeledSet *) calloc(1,sizeof(iftLabeledSet));
  if (p == NULL) iftError(MSG1,"iftInsertLabeledSet");
  if (*S == NULL){
    p->elem  = elem;
    p->label = label;
    p->next  = NULL;
  }else{
    p->elem  = elem;
    p->label = label;
    p->next  = *S;
  }
  *S = p;
}

int iftRemoveLabeledSet(iftLabeledSet **S, int *label)
{
  iftLabeledSet *p;
  int elem = NIL;

  if (*S != NULL){
    p    =  *S;
    elem = p->elem;
    (*label) = p->label;
    *S   = p->next;
    free(p);
  }

  return(elem);
}


void iftRemoveLabeledSetElem(iftLabeledSet **S, int elem){
  iftLabeledSet *tmp = NULL, *remove;

  tmp = *S;
  if ( tmp->elem == elem ) {
    *S = tmp->next;
    free( tmp );
  }
  else {
    while( tmp->next->elem != elem ) tmp = tmp->next;
    remove = tmp->next;
    tmp->next = remove->next;
    free( remove );
  }
}


void iftDestroyLabeledSet(iftLabeledSet **S)
{
  iftLabeledSet *p;
  while(*S != NULL){
    p = *S;
    *S = p->next;
    free(p);
  }
}

void iftConcatLabeledSet(iftLabeledSet **S1,iftLabeledSet **S2){
	if(*S2 == NULL)
		return;

	iftLabeledSet *i = *S2;
	while(i != NULL){
		iftInsertLabeledSet(S1,i->elem,i->label);
		i = i->next;
	}
}

/* Warning: S2 must be a subset of S1! */

void iftRemoveSubsetLabeledSet(iftLabeledSet **S1,iftLabeledSet **S2){
	if(*S2 == NULL)
		return;

	iftLabeledSet *i = *S2;
	while(i != NULL){
		iftRemoveLabeledSetElem(S1,i->elem);
		i = i->next;
	}
}

iftLabeledSet* iftCopyLabeledSet(iftLabeledSet *s){
	iftLabeledSet *lset = NULL;

	while(s != NULL){
		iftInsertLabeledSet(&lset,s->elem,s->label);
		s = s->next;
	}

	return lset;
}

iftLabeledSet* iftCopyOrderedLabeledSet(iftLabeledSet *s){
	iftLabeledSet *lset = NULL;
	if(s == NULL)
		return lset;

	iftLabeledSet *i = s;
	int nelem = 0;
	while(i != NULL){
		nelem++;
		i = i->next;
	}

	int elems[nelem];
	int labels[nelem];
	i = s;
	int index = 0;
	while(i != NULL){
		elems[index] = i->elem;
		labels[index] = i->label;
		index++;
		i = i->next;
	}

	for(index = nelem - 1; index >= 0; index--){
		iftInsertLabeledSet(&lset,elems[index],labels[index]);
	}

	return lset;
}

/* By TVS from CAVOS */
int iftLabeledSetSize(iftLabeledSet* S){
	iftLabeledSet *s = S;

	int i = 0;
	while(s != NULL){
		i++;
		s = s->next;
	}

	return i;

}

iftSet* iftLabeledSetToSet(iftLabeledSet *S, int lb)
{
	iftSet *Snew = NULL;
	iftLabeledSet *s = S;

	while(s != NULL){
		if(lb < 0 || lb == s->label)
			iftInsertSet(&Snew, s->elem);
		s = s->next;
	}

	return Snew;
}


iftLabeledSet* iftCopyLabels(iftLabeledSet *S, int lb)
{
	iftLabeledSet *Snew = NULL;
	iftLabeledSet *s = S;

	while(s != NULL){
		if(lb < 0 || lb == s->label)
			iftInsertLabeledSet(&Snew, s->elem, lb);
		s = s->next;
	}

	return Snew;
}


