#include "iftSort.h"

void iftBucketSort(int *value, int *index, int nelems, uchar order)
{
  int i,j,maxval=-INFINITY_INT, *sval=NULL, *sind=NULL;
  iftGQueue *Q=NULL;

  for(i=0; i < nelems; i++) 
    if (value[i] > maxval)
      maxval = value[i];
  
  Q    = iftCreateGQueue(maxval+1,nelems,value);
  sval = iftAllocIntArray(nelems);
  sind = iftAllocIntArray(nelems);

  if (order==DECREASING){
    iftSetRemovalPolicy(Q,MAXVALUE);
  }

  for(i=0; i < nelems; i++)
    iftInsertGQueue(&Q,i);

  j = 0;
  while(!iftEmptyGQueue(Q)) {
    i = iftRemoveGQueue(Q);
    sval[j]  = value[i];
    sind[j]  = index[i];
    j++;
  }

  iftDestroyGQueue(&Q);

  for(i=0; i < nelems; i++){
    value[i]=sval[i];
    index[i]=sind[i];
  }
  free(sval);
  free(sind);
}

void iftFHeapSort(float *value, int *index, int nelems, uchar order)
{
  int i,j, *sind=NULL;
  float *sval=NULL;
  iftFHeap *H=NULL;

  H    = iftCreateFHeap(nelems,value);
  sval = iftAllocFloatArray(nelems);
  sind = iftAllocIntArray(nelems);

  if (order==DECREASING){
    iftSetRemovalPolicyFHeap(H, MAXVALUE);
  }

  for(i=0; i < nelems; i++)
    iftInsertFHeap(H, i);

  j = 0;
  while(!iftEmptyFHeap(H)) {
    i = iftRemoveFHeap(H);
    sval[j]  = value[i];
    sind[j]  = index[i];
    j++;
  }

  iftDestroyFHeap(&H);

  for(i=0; i < nelems; i++){
    value[i]=sval[i];
    index[i]=sind[i];
  }
  free(sval);
  free(sind);
}

void iftQuickSort( int *value, int *index, int i0, int i1, uchar order ) 
{
  int m, d;
  

  if( i0 < i1 ) {
    /* random index to avoid bad pivots.*/
    d = iftRandomInteger( i0, i1 );
    iftSwitchValues( &value[ d ], &value[ i0 ] );
    iftSwitchValues( &index[ d ], &index[ i0 ] );
    m = i0;

    if( order == INCREASING ) {
      for( d = i0 + 1; d <= i1; d++ ) {
	if( value[ d ] < value[ i0 ] ) {
	  m++;
	  iftSwitchValues( &value[ d ], &value[ m ] );
	  iftSwitchValues( &index[ d ], &index[ m ] );
	}
      }
    }
    else {
      for( d = i0 + 1; d <= i1; d++ ) {
	if( value[ d ] > value[ i0 ] ) {
	  m++;
	  iftSwitchValues( &value[ d ], &value[ m ] );
	  iftSwitchValues( &index[ d ], &index[ m ] );
	}
      }
    }
    iftSwitchValues( &value[ m ], &value[ i0 ] );
    iftSwitchValues( &index[ m ], &index[ i0 ] );
    
    

    iftQuickSort( value, index, i0, m - 1, order );
    iftQuickSort( value, index, m + 1, i1, order );
  }
}

void iftSQuickSort( char **value, int *index, int i0, int i1, uchar order, int size ) 
{
  int m, d;
  

  if( i0 < i1 ) {
    /* random index to avoid bad pivots.*/
    d = iftRandomInteger( i0, i1 );
    iftSSwitchValues( value[ d ], value[ i0 ] , size);
    iftSwitchValues( &index[ d ], &index[ i0 ] );
    m = i0;

    if( order == INCREASING ) {
      for( d = i0 + 1; d <= i1; d++ ) {
	if( strcmp( value[ d ], value[ i0 ] ) < 0 ){
	  m++;
	  iftSSwitchValues( value[ d ], value[ m ] , size);
	  iftSwitchValues( &index[ d ], &index[ m ] );
	}
      }
    }
    else {
      for( d = i0 + 1; d <= i1; d++ ) {
	if( strcmp( value[ d ] , value[ i0 ] ) > 0 ) {
	  m++;
	  iftSSwitchValues(  value[ d ],  value[ m ] , size);
	  iftSwitchValues( &index[ d ], &index[ m ] );
	}
      }
    }
    iftSSwitchValues( value[ m ], value[ i0 ] , size);
    iftSwitchValues( &index[ m ], &index[ i0 ] );
    iftSQuickSort( value, index, i0, m - 1, order , size);
    iftSQuickSort( value, index, m + 1, i1, order , size);
  }
}

void iftFQuickSort( float *value, int *index, int i0, int i1, uchar order ) 
{
  int m, d;
  

  if( i0 < i1 ) {
    /* random index to avoid bad pivots.*/
    d = iftRandomInteger( i0, i1 );
    iftFSwitchValues( &value[ d ], &value[ i0 ] );
    iftSwitchValues( &index[ d ], &index[ i0 ] );
    m = i0;

    if( order == INCREASING ) {
      for( d = i0 + 1; d <= i1; d++ ) {
	if( value[ d ] < value[ i0 ] ) {
	  m++;
	  iftFSwitchValues( &value[ d ], &value[ m ] );
	  iftSwitchValues( &index[ d ], &index[ m ] );
	}
      }
    }
    else {
      for( d = i0 + 1; d <= i1; d++ ) {
	if( value[ d ] > value[ i0 ] ) {
	  m++;
	  iftFSwitchValues( &value[ d ], &value[ m ] );
	  iftSwitchValues( &index[ d ], &index[ m ] );
	}
      }
    }
    iftFSwitchValues( &value[ m ], &value[ i0 ] );
    iftSwitchValues( &index[ m ], &index[ i0 ] );
    iftFQuickSort( value, index, i0, m - 1, order );
    iftFQuickSort( value, index, m + 1, i1, order );
  }
}

