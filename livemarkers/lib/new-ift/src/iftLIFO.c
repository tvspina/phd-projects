#include "iftLIFO.h"

iftLIFO *iftCreateLIFO(int n)
{
  iftLIFO *L=(iftLIFO *)calloc(1,sizeof(iftLIFO));

  L->LIFO   = iftAllocIntArray(n);
  L->color  = iftAllocCharArray(n);
  L->n      = n;
  L->last   = NIL;

  return(L);
}

void     iftDestroyLIFO(iftLIFO **L)
{
  iftLIFO *aux=*L;

  if (aux != NULL) {
    free(aux->LIFO);
    free(aux->color);
    free(aux);
    *L = NULL;
  }
}

char iftInsertLIFO(iftLIFO *L, int node)
{  
  if (iftFullLIFO(L)){
    iftWarning("LIFO is full","iftInsertLIFO");
    return 0;
  }
  L->last++; L->LIFO[L->last]=node;  
  L->color[node]=GRAY;

  return 1;
}

int      iftRemoveLIFO(iftLIFO *L)
{
  int node=NIL;

  if (!iftEmptyLIFO(L)){
    node = L->LIFO[L->last];  L->last--;
    L->color[node]=BLACK;
  }else{
    iftWarning("LIFO is empty","iftRemoveLIFO");
  }

 return node;
}

char     iftEmptyLIFO(iftLIFO *L)
{
  int p;

  if (L->last==NIL) {
    for (p=0; p < L->n; p++) 
      L->color[p]=WHITE;
    return(1);
  }else
    return(0);
}

char     iftFullLIFO(iftLIFO *L)
{
  if (L->last==L->n-1) {
    return(1);
  }else
    return(0);
}

void iftResetLIFO(iftLIFO *L)
{
  int p;

  L->last=NIL;
  for (p=0; p < L->n; p++) 
    L->color[p]=WHITE;
}
