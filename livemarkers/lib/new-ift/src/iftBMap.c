#include "iftBMap.h"

iftBMap *iftCreateBMap(int n) {
  iftBMap *b;
  b= (iftBMap *) calloc(1,sizeof(iftBMap));
  b->n        = n;
  b->nbytes   = n/8;
  if (n%8) b->nbytes++;
  b->val = (char *) calloc(b->nbytes,sizeof(char));
  if (b->val==NULL){
    iftError(MSG1,"iftCreateBMap");
  }
  return b;
}

void iftDestroyBMap(iftBMap **b) {
  iftBMap *aux=*b;

  if (aux != NULL) {
    free(aux->val);
    free(aux);
    *b=NULL;
  }

}

void iftFillBMap(iftBMap *b, int value) {
  int p;
  for (p=0; p < b->nbytes; p++) 
    b->val[p] = (value?0xff:0);
}

iftBMap *iftCopyBMap(iftBMap *src) {
  iftBMap *dst=iftCreateBMap(src->n);
  int p;
  for (p=0; p < src->nbytes; p++) 
    dst->val[p] = src->val[p];
  return(dst);
}

