#include "iftAdjacency.h"

/*! \file
 * Author: Alexandre Falcao
 * Date: Aug 22, 2011
 * Last Update: Aug 22, 2011
 */

/* 3D adjacency relations */

iftAdjRel  *iftCreateAdjRel(int n) /*! \brief Allocates memory for a
				      3D adjacency relation */
{
  iftAdjRel *A=(iftAdjRel *)calloc(1,sizeof(iftAdjRel ));

  A->dx = (int *)iftAllocIntArray(n);
  A->dy = (int *)iftAllocIntArray(n);
  A->dz = (int *)iftAllocIntArray(n);
  A->n  = n;

  return(A);
}

void     iftDestroyAdjRel(iftAdjRel **A) /*! \brief Deallocates memory for a
					     3D adjacency relation */
{
  iftAdjRel *aux = *A;

  if (aux != NULL){
    if (aux->dx != NULL) free(aux->dx);
    if (aux->dy != NULL) free(aux->dy);
    if (aux->dz != NULL) free(aux->dz);
    free(aux);
    *A = NULL;
  }
}

iftAdjRel  *iftSpheric(float r) /*! \brief Creates a 3D ball of radius r as
				 adjacency relation */
{
  iftAdjRel *A=NULL;
  int i,j,k,n,r0,d,dx,dy,dz,i0=0;
  float *dr,aux,r2;

  n=0;
  r0  = (int)r;
  r2  = (int)(r*r + 0.5);
  for(dz=-r0;dz<=r0;dz++)
    for(dy=-r0;dy<=r0;dy++)
      for(dx=-r0;dx<=r0;dx++)
      if(((dx*dx)+(dy*dy)+(dz*dz)) <= r2)
	n++;

  A = iftCreateAdjRel(n);
  i=0;
  for(dz=-r0;dz<=r0;dz++)
    for(dy=-r0;dy<=r0;dy++)
      for(dx=-r0;dx<=r0;dx++)
	if(((dx*dx)+(dy*dy)+(dz*dz)) <= r2){
	  A->dx[i]=dx;
	  A->dy[i]=dy;
	  A->dz[i]=dz;
	  if ((dx==0)&&(dy==0)&&(dz==0))
	    i0 = i;
	  i++;
	}

  /* shift to right and place central voxel at first */

  for (i=i0; i > 0; i--) {
    dx = A->dx[i];
    dy = A->dy[i];
    dz = A->dz[i];
    A->dx[i] = A->dx[i-1];
    A->dy[i] = A->dy[i-1];
    A->dz[i] = A->dz[i-1];
    A->dx[i-1] = dx;
    A->dy[i-1] = dy;
    A->dz[i-1] = dz;
  }

  /* sort by radius, so the 6 closest neighbors will come first */

  dr = iftAllocFloatArray(A->n);
  for (i=0; i < A->n; i++) {
    dr[i] = A->dx[i]*A->dx[i] + A->dy[i]*A->dy[i] + A->dz[i]*A->dz[i];
  }

  for (i=1; i < A->n-1; i++){
    k = i;
    for (j=i+1; j < A->n; j++)
      if (dr[j] < dr[k]){
	k = j;
      }
    aux   = dr[i];
    dr[i] = dr[k];
    dr[k] = aux;
    d        = A->dx[i];
    A->dx[i] = A->dx[k];
    A->dx[k] = d;
    d        = A->dy[i];
    A->dy[i] = A->dy[k];
    A->dy[k] = d;
    d        = A->dz[i];
    A->dz[i] = A->dz[k];
    A->dz[k] = d;
  }

  free(dr);


  return(A);
}

iftAdjRel  *iftHemispheric(float r, char axis, int direction) /*! \brief Creates a 3D half-ball of radius r as
                                                            adjacency relation, in the corresponding axis and direction.
                                                            This adjacency is useful for segmenting a volume in a single
                                                            direction, e.g., a video-volume where z is time. */
{
  iftAdjRel *A=NULL;
  int i,j,k,n,r0,d,dx,dy,dz,dx0,dy0,dz0,xr1,yr1,zr1,i0=0;
  float *dr,aux,r2;

  n=0;
  r0 = (int)r;
  r2  = (int)(r*r + 0.5);

  dz0 = dy0 = dx0 = -r0;
  zr1 = yr1 = xr1 = r0;
  if(axis == 'z')
  {
    dz0 = (direction >= 0) ? 0 : -r0;
    zr1 = (direction >= 0) ? r0 : 0;
  }
  else if(axis == 'y')
  {
    dy0 = (direction >= 0) ? 0 : -r0;
    yr1 = (direction >= 0) ? r0 : 0;
  }
  else if(axis == 'x')
  {
    dx0 = (direction >= 0) ? 0 : -r0;
    xr1 = (direction >= 0) ? r0 : 0;
  }

  for(dz=dz0;dz<=zr1;dz++)
    for(dy=dy0;dy<=yr1;dy++)
      for(dx=dx0;dx<=xr1;dx++)
      if(((dx*dx)+(dy*dy)+(dz*dz)) <= r2)
	n++;

  A = iftCreateAdjRel(n);
  i=0;
  for(dz=dz0;dz<=zr1;dz++)
    for(dy=dy0;dy<=yr1;dy++)
      for(dx=dx0;dx<=xr1;dx++)
	if(((dx*dx)+(dy*dy)+(dz*dz)) <= r2){
	  A->dx[i]=dx;
	  A->dy[i]=dy;
	  A->dz[i]=dz;
	  if ((dx==0)&&(dy==0)&&(dz==0))
	    i0 = i;
	  i++;
	}

  /* shift to right and place central voxel at first */

  for (i=i0; i > 0; i--) {
    dx = A->dx[i];
    dy = A->dy[i];
    dz = A->dz[i];
    A->dx[i] = A->dx[i-1];
    A->dy[i] = A->dy[i-1];
    A->dz[i] = A->dz[i-1];
    A->dx[i-1] = dx;
    A->dy[i-1] = dy;
    A->dz[i-1] = dz;
  }

  /* sort by radius, so the 6 closest neighbors will come first */

  dr = iftAllocFloatArray(A->n);
  for (i=0; i < A->n; i++) {
    dr[i] = A->dx[i]*A->dx[i] + A->dy[i]*A->dy[i] + A->dz[i]*A->dz[i];
  }

  for (i=1; i < A->n-1; i++){
    k = i;
    for (j=i+1; j < A->n; j++)
      if (dr[j] < dr[k]){
	k = j;
      }
    aux   = dr[i];
    dr[i] = dr[k];
    dr[k] = aux;
    d        = A->dx[i];
    A->dx[i] = A->dx[k];
    A->dx[k] = d;
    d        = A->dy[i];
    A->dy[i] = A->dy[k];
    A->dy[k] = d;
    d        = A->dz[i];
    A->dz[i] = A->dz[k];
    A->dz[k] = d;
  }

  free(dr);


  return(A);
}

iftAdjRel *iftSphericEdges(float r)
{
	int i,j,n=0;
	iftAdjRel* Ac = iftSpheric(r);

	for(i=0;i<Ac->n;i++)
		if (Ac->dz[i]*(2*r+1)*(2*r+1) + Ac->dy[i]*(2*r+1) + Ac->dx[i] >= 0)
			n++;

	iftAdjRel* A = iftCreateAdjRel(n);
	for(i=0,j=0;i<Ac->n;i++)
		if (Ac->dz[i]*(2*r+1)*(2*r+1) + Ac->dy[i]*(2*r+1) + Ac->dx[i] >= 0)
		{
			A->dx[j] = Ac->dx[i];
			A->dy[j] = Ac->dy[i];
			A->dz[j] = Ac->dz[i];
			j++;
		}

	iftDestroyAdjRel(&Ac);
	return A;
}


iftAdjRel *iftRectangular(int xsize, int ysize)
{
  iftAdjRel *A;
  int i,dx,dy,n,i0=0;

  n = 0;
  for(dy=-ysize/2;dy<=ysize/2;dy++)
    for(dx=-xsize/2;dx<=xsize/2;dx++)
      n++;

  A = iftCreateAdjRel(n);

  i = 0;
  for(dy=-ysize/2;dy<=ysize/2;dy++)
    for(dx=-xsize/2;dx<=xsize/2;dx++){
      A->dx[i] = dx;
      A->dy[i] = dy;
      A->dz[i] =  0;
      if ((dx==0)&&(dy==0))
	i0 = i;
      i++;
    }

  /* shift to right and place central point (origin) at first */

  for (i=i0; i > 0; i--) {
    dx = A->dx[i];
    dy = A->dy[i];
    A->dx[i] = A->dx[i-1];
    A->dy[i] = A->dy[i-1];
    A->dx[i-1] = dx;
    A->dy[i-1] = dy;
  }

  return(A);
}

iftAdjRel *iftCopyAdjacency(iftAdjRel *A)
{
  iftAdjRel *B = iftCreateAdjRel(A->n);
  int i;
  for (i=0; i < A->n; i++) {
    B->dx[i] = A->dx[i];
    B->dy[i] = A->dy[i];
    B->dz[i] = A->dz[i];
  }

  return(B);
}


iftAdjRel *iftCuboid(int xsize, int ysize, int zsize)
{
  iftAdjRel *A;
  int i,dx,dy,dz,n,i0=0;

  n = 0;
  for(dz=-zsize/2;dz<=zsize/2;dz++)
    for(dy=-ysize/2;dy<=ysize/2;dy++)
      for(dx=-xsize/2;dx<=xsize/2;dx++)
      n++;

  A = iftCreateAdjRel(n);

  i = 0;
  for(dz=-zsize/2;dz<=zsize/2;dz++)
    for(dy=-ysize/2;dy<=ysize/2;dy++)
      for(dx=-xsize/2;dx<=xsize/2;dx++){
	A->dx[i] = dx;
	A->dy[i] = dy;
	A->dz[i] = dz;
	if ((dx==0)&&(dy==0)&&(dz==0))
	  i0 = i;
	i++;
      }

  /* shift to right and place central point (origin) at first */

  for (i=i0; i > 0; i--) {
    dx = A->dx[i];
    dy = A->dy[i];
    dz = A->dz[i];
    A->dx[i] = A->dx[i-1];
    A->dy[i] = A->dy[i-1];
    A->dz[i] = A->dz[i-1];
    A->dx[i-1] = dx;
    A->dy[i-1] = dy;
    A->dz[i-1] = dz;
  }

  return(A);
}
iftAdjRel *iftCircular(float r)
{
  iftAdjRel *A=NULL;
  int i,j,k,n,dx,dy,r0,d,i0=0;
  float *dr,aux,r2;

  n=0;

  r0  = (int)r;
  r2  = (int)(r*r + 0.5);
  for(dy=-r0;dy<=r0;dy++)
    for(dx=-r0;dx<=r0;dx++)
      if(((dx*dx)+(dy*dy)) <= r2)
	n++;

  A = iftCreateAdjRel(n);
  i=0;
  for(dy=-r0;dy<=r0;dy++)
    for(dx=-r0;dx<=r0;dx++)
      if(((dx*dx)+(dy*dy)) <= r2){
	A->dx[i]=dx;
	A->dy[i]=dy;
	A->dz[i]=0;
	if ((dx==0)&&(dy==0))
	  i0 = i;
	i++;
      }

  /* shift to right and place central pixel at first */

  for (i=i0; i > 0; i--) {
    dx = A->dx[i];
    dy = A->dy[i];
    A->dx[i] = A->dx[i-1];
    A->dy[i] = A->dy[i-1];
    A->dx[i-1] = dx;
    A->dy[i-1] = dy;
  }

  /* sort by radius, so the 4 closest neighbors will come first */

  dr = iftAllocFloatArray(A->n);
  for (i=0; i < A->n; i++) {
    dr[i] = A->dx[i]*A->dx[i] + A->dy[i]*A->dy[i];
  }

  for (i=1; i < A->n-1; i++){
    k = i;
    for (j=i+1; j < A->n; j++)
      if (dr[j] < dr[k]){
	k = j;
      }
    aux   = dr[i];
    dr[i] = dr[k];
    dr[k] = aux;
    d        = A->dx[i];
    A->dx[i] = A->dx[k];
    A->dx[k] = d;
    d        = A->dy[i];
    A->dy[i] = A->dy[k];
    A->dy[k] = d;
  }

  free(dr);

  return(A);
}

iftAdjRel *iftCircularEdges(float r)
{
	int i,j,n=0;
	iftAdjRel* Ac = iftCircular(r);

	for(i=0;i<Ac->n;i++)
		if (Ac->dy[i]*(2*r+1) + Ac->dx[i] >= 0)
			n++;

	iftAdjRel* A = iftCreateAdjRel(n);
	for(i=0,j=0;i<Ac->n;i++)
		if (Ac->dy[i]*(2*r+1) + Ac->dx[i] >= 0)
		{
			A->dx[j] = Ac->dx[i];
			A->dy[j] = Ac->dy[i];
			A->dz[j] = Ac->dz[i];
			j++;
		}

	iftDestroyAdjRel(&Ac);
	return A;
}

iftAdjRel *iftClockCircular(float r)
{
  iftAdjRel *A=NULL;
  int i,j,k,n,dx,dy,r0,r2,d,i0=0;
  float *da,*dr,aux;

  n=0;

  r0  = (int)r;
  r2  = (int)(r*r + 0.5);
  for(dy=-r0;dy<=r0;dy++)
    for(dx=-r0;dx<=r0;dx++)
      if(((dx*dx)+(dy*dy)) <= r2)
	n++;

  A = iftCreateAdjRel(n);
  i=0;
  for(dy=-r0;dy<=r0;dy++)
    for(dx=-r0;dx<=r0;dx++)
      if(((dx*dx)+(dy*dy)) <= r2){
	A->dx[i]=dx;
	A->dy[i]=dy;
	A->dz[i]=0;
	if ((dx==0)&&(dy==0))
	  i0 = i;
	i++;
      }

  /* Set clockwise */

  da = iftAllocFloatArray(A->n);
  dr = iftAllocFloatArray(A->n);
  for (i=0; i < A->n; i++) {
    dx = A->dx[i];
    dy = A->dy[i];
    dr[i] = (float)sqrtf((dx*dx) + (dy*dy));
    if (i != i0){
      da[i] = atan2(-dy,-dx)*180.0/PI;
      if (da[i] < 0.0)
	da[i] += 360.0;
    }
  }
  da[i0] = 0.0;
  dr[i0] = 0.0;

  /* place central pixel at first */

  aux    = da[i0];
  da[i0] = da[0];
  da[0]  = aux;
  aux    = dr[i0];
  dr[i0] = dr[0];
  dr[0]  = aux;
  d         = A->dx[i0];
  A->dx[i0] = A->dx[0];
  A->dx[0]  = d;
  d         = A->dy[i0];
  A->dy[i0] = A->dy[0];
  A->dy[0]  = d;

  /* sort by angle */

  for (i=1; i < A->n-1; i++){
    k = i;
    for (j=i+1; j < A->n; j++)
      if (da[j] < da[k]){
	k = j;
      }
    aux   = da[i];
    da[i] = da[k];
    da[k] = aux;
    aux   = dr[i];
    dr[i] = dr[k];
    dr[k] = aux;
    d   = A->dx[i];
    A->dx[i] = A->dx[k];
    A->dx[k] = d;
    d        = A->dy[i];
    A->dy[i] = A->dy[k];
    A->dy[k] = d;
  }

  /* sort by radius for each angle */

  for (i=1; i < A->n-1; i++){
    k = i;
    for (j=i+1; j < A->n; j++)
      if ((dr[j] < dr[k])&&(da[j]==da[k])){
	k = j;
      }
    aux   = dr[i];
    dr[i] = dr[k];
    dr[k] = aux;
    d        = A->dx[i];
    A->dx[i] = A->dx[k];
    A->dx[k] = d;
    d        = A->dy[i];
    A->dy[i] = A->dy[k];
    A->dy[k] = d;
  }

  free(dr);
  free(da);

  return(A);
}

iftAdjRel *iftRightSide(iftAdjRel *A, float r){
  iftAdjRel *R=NULL;
  int i;
  float d;

  for (i=0; i < A->n; i++)
    if (A->dz[i]!=0)
      iftError("It must be a 2D adjacency relation","iftRightSide2D");

  /* Let p -> q be an arc represented by the increments dx,dy,dz. Its
     right side at distance r is given by the increments Dx = (-dy/d +
     dx/2)*r and Dy = dx/d + dy/2, where d=sqrt(dx²+dy²). */

  R = iftCreateAdjRel(A->n);
  for (i=0; i < R->n; i++){
    d  = sqrt(A->dx[i]*A->dx[i] + A->dy[i]*A->dy[i]);
    if (d != 0){
      R->dx[i] = ROUND( ((float)A->dx[i]/2.0)-((float)A->dy[i]/d)*r );
      R->dy[i] = ROUND( ((float)A->dx[i]/d)+((float)A->dy[i]/2.0)*r );
      R->dz[i] = 0.0;
    }
  }

  return(R);
}

iftAdjRel *iftLeftSide(iftAdjRel *A, float r)
{
  iftAdjRel *L=NULL;
  int i;
  float d;

  for (i=0; i < A->n; i++)
    if (A->dz[i]!=0)
      iftError("It must be a 2D adjacency relation","iftLeftSide2D");

  /* Let p -> q be an arc represented by the increments dx,dy. Its
     left side is given by the increments Dx = dy/d + dx/2 and Dy =
     -dx/d + dy/2, where d=sqrt(dx²+dy²). */

  L = iftCreateAdjRel(A->n);
  for (i=0; i < L->n; i++){
    d  = sqrt(A->dx[i]*A->dx[i] + A->dy[i]*A->dy[i]);
    if (d != 0){
      L->dx[i] = ROUND( ((float)A->dx[i]/2.0)+((float)A->dy[i]/d)*r );
      L->dy[i] = ROUND( ((float)A->dy[i]/2)-((float)A->dx[i]/d)*r );
      L->dz[i] = 0;
    }
  }

  return(L);
}

void iftMaxAdjShifts(iftAdjRel *A, int *dx, int *dy, int *dz)
{
  int i, d[3];

  *dx = *dy = *dz = 0.0;

  for (i=0; i < A->n; i++) {
    d[0] = abs(A->dx[i]);
    d[1] = abs(A->dy[i]);
    d[2] = abs(A->dz[i]);
    if (*dx < d[0]) *dx = d[0];
    if (*dy < d[1]) *dy = d[1];
    if (*dz < d[2]) *dz = d[2];
  }

}

iftFastAdjRel *iftCreateFastAdjRel(iftAdjRel *A, int *tby, int *tbz)
{
  iftFastAdjRel *F = (iftFastAdjRel *) calloc(1,sizeof(iftFastAdjRel ));
  int i, po, qo;
  iftVoxel v;

  F->n  = A->n;
  F->dq = iftAllocIntArray(F->n);

  /* compute maximum displacements */

  iftMaxAdjShifts(A,&F->bx,&F->by,&F->bz);

  /* compute displacements to adjacent voxels */

  po = F->bx + tby[F->by] + tbz[F->bz];

  for (i=0; i < F->n; i++) {
    v.x = F->bx + A->dx[i];
    v.y = F->by + A->dy[i];
    v.z = F->bz + A->dz[i];
    qo  = v.x + tby[v.y] + tbz[v.z];
    F->dq[i] = qo-po;
  }

  return(F);

}

void iftDestroyFastAdjRel(iftFastAdjRel **F)
{
  iftFastAdjRel *aux=*F;

  if (aux != NULL) {
    free(aux->dq);
    free(aux);
    *F = NULL;
  }
}


void iftWriteAdjRel(iftAdjRel *A, char* filename){
  FILE *fp = fopen(filename, "w");
  if(!fp)
    iftError("Unable to open file", "iftWriteAdjRel");

  fprintf(fp,"%d\n",A->n);
  for (int i=0; i < A->n; i++)
    fprintf(fp,"%d %d %d\n",A->dx[i],A->dy[i],A->dz[i]);

  fclose(fp);
}

iftAdjRel* iftReadAdjRel(char* filename){
  FILE *fp = fopen(filename, "r");
  if(!fp)
    iftError("Unable to open file", "iftReadAdjRel");

  int n;
  if (fscanf(fp,"%d",&n)!=1)
    iftError("Reading error","iftReadAdjRel");

  iftAdjRel *A = iftCreateAdjRel(n);

  for (int i=0; i < A->n; i++)
    if (fscanf(fp,"%d %d %d",&A->dx[i],&A->dy[i],&A->dz[i])!=3)
      iftError("Reading error","iftReadAdjRel");

  fclose(fp);

  return(A);
}



iftVoxel iftGetAdjacentVoxel(iftAdjRel *A, iftVoxel u, int adj)
{
  iftVoxel v;

  v.x = u.x + A->dx[adj];
  v.y = u.y + A->dy[adj];
  v.z = u.z + A->dz[adj];

  return(v);
}

int iftIsAdjRel3D(iftAdjRel *A)
{
  int dx,dy,dz;
  iftMaxAdjShifts(A,&dx,&dy,&dz);
  if (dz > 0)
    return 1;
  else
    return 0;
}
