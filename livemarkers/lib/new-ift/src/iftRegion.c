#include "iftRegion.h"
#include "iftGQueue.h"
#include "iftCommon.h"


/* Given a label image with n labels different from 0, returns an image relabeled from 1 to n and leaves region 0 untouched */
iftImage* iftRelabelRegions(iftImage* labelled, iftAdjRel* adj_rel){
	iftImage *relabelled = iftCreateImage(labelled->xsize,labelled->ysize,labelled->zsize);
	iftCopyVoxelSize(labelled,relabelled);
	iftFIFO *F = iftCreateFIFO(labelled->n);

	int nlabels = 1;

	int i;
	for(i = 0; i < labelled->n; i++){
		if( (labelled->val[i] != 0) && (relabelled->val[i] == 0)){
			relabelled->val[i] = nlabels;
			iftInsertFIFO(F,i);

			while(!iftEmptyFIFO(F)){
				int p = iftRemoveFIFO(F);
				iftVoxel u = iftGetVoxelCoord(labelled,p);

				int j;
				for(j = 1; j < adj_rel->n; j++){
					iftVoxel v = iftGetAdjacentVoxel(adj_rel,u,j);

					if(iftValidVoxel(labelled,v)){
						int q = iftGetVoxelIndex(labelled,v);

						if((relabelled->val[q] == 0) && (labelled->val[p] == labelled->val[q]) ){
							relabelled->val[q] = nlabels;
							iftInsertFIFO(F,q);
						}
					}
				}
			}

			nlabels++;
		}
	}

	iftDestroyFIFO(&F);

	return relabelled;
}

/* Receives an image labelled from 0 to n and returns a labeled set with the n geodesic centers, ordered by decreasing distance to their respective borders.*/
iftLabeledSet* iftGeodesicCenters(iftImage* label_image){
	iftLabeledSet* geo_centers = NULL;

	int nregions = iftMaximumValue(label_image);

	iftImage *dist = iftCreateImage(label_image->xsize,label_image->ysize,label_image->zsize);
	iftImage *root = iftCreateImage(label_image->xsize,label_image->ysize,label_image->zsize);
	iftGQueue *Q = iftCreateGQueue(QSIZE,label_image->n,dist->val);

	iftAdjRel *adj_rel = NULL;
	if(iftIs3DImage(label_image))
		adj_rel = iftSpheric(sqrt(1.0));
	else
		adj_rel = iftCircular(sqrt(1.0));

	//Creates a distance map that is 0 in the (internal) borders and +infinity otherwise
	int p;
	for(p = 0; p < label_image->n; p++){
		dist->val[p] = INFINITY_INT;

		iftVoxel u = iftGetVoxelCoord(label_image, p);

		int i;
		for(i = 1; i < adj_rel->n; i++){
			iftVoxel v = iftGetAdjacentVoxel(adj_rel,u,i);

			if(iftValidVoxel(label_image,v)){
				int q = iftGetVoxelIndex(label_image,v);
				if((label_image->val[p] != label_image->val[q]) && (dist->val[p] > 0)){
					dist->val[p] = 0;
					root->val[p] = p;
					iftInsertGQueue(&Q,p);
				}
			}else if(dist->val[p] > 0){
				dist->val[p] = 0;
				root->val[p] = p;
				iftInsertGQueue(&Q,p);
			}
		}
	}

	iftDestroyAdjRel(&adj_rel);

	if(iftIs3DImage(label_image))
		adj_rel = iftSpheric(sqrtf(3.0));
	else
		adj_rel = iftCircular(sqrtf(2.0));

	while(!iftEmptyGQueue(Q)){
		p = iftRemoveGQueue(Q);

		iftVoxel u = iftGetVoxelCoord(label_image,p);
		iftVoxel r = iftGetVoxelCoord(label_image, root->val[p]);

		int i;
		for(i = 1; i < adj_rel->n; i++){
			iftVoxel v = iftGetAdjacentVoxel(adj_rel,u,i);

			if(iftValidVoxel(label_image,v)){
				int q = iftGetVoxelIndex(label_image,v);

				if ((dist->val[q] > dist->val[p]) && (label_image->val[p] == label_image->val[q])){
					int tmp = (v.x-r.x)*(v.x-r.x) + (v.y-r.y)*(v.y-r.y) + (v.z-r.z)*(v.z-r.z);
					if (tmp < dist->val[q]){
						if(dist->val[q] != INFINITY_INT)
						  iftRemoveGQueueElem(Q,q);

						dist->val[q] = tmp;
						root->val[q] = root->val[p];
						iftInsertGQueue(&Q,q);
					}
				}
			}
		}
	}

	iftDestroyImage(&root);
	iftDestroyGQueue(&Q);
	iftDestroyAdjRel(&adj_rel);

	//Finds, for each region, the most distant pixel from its border
	int centers[nregions];
	int centers_distance[nregions];
	int i;
	for(i = 0; i < nregions; i++)
		centers_distance[i] = -1;

	for(p = 0; p < label_image->n; p++){
		int region = label_image->val[p] - 1;
		if(centers_distance[region] < dist->val[p]){
			centers[region] = p;
			centers_distance[region] = dist->val[p];
		}
	}

	iftDestroyImage(&dist);

	//This array will be scrambled by quicksort
	int centers_index[nregions];
	for(i = 0; i < nregions; i++)
		centers_index[i] = i;

	//Sorts the centers in increasing order, so inserting them in order in the iftLabeledSet geo_centers gives us a decreasing order
	iftQuickSort(centers_distance,centers_index,0,nregions - 1, INCREASING);

	for(i = 0; i < nregions; i++){
		int index = centers_index[i];

		iftInsertLabeledSet(&geo_centers,centers[index],index + 1);
	}

	return geo_centers;
}

iftLabeledSet* iftGeometricCenters(iftImage* label_image){
	iftLabeledSet* centers = NULL;

	int nregions = iftMaximumValue(label_image);

	ullong *xc = iftAllocULLongArray(nregions);
	ullong *yc = iftAllocULLongArray(nregions);
	ullong *zc = iftAllocULLongArray(nregions);

	int *np = iftAllocIntArray(nregions);

	int p;
	for(p = 0; p < label_image->n; p++){
		int region = label_image->val[p] - 1;

		iftVoxel v = iftGetVoxelCoord(label_image,p);

		xc[region] += v.x;
		yc[region] += v.y;
		zc[region] += v.z;

		np[region]++;
	}

	int i;
	for(i = 0; i < nregions; i++){
		iftVoxel v;
		v.x = xc[i]/np[i];
		v.y = yc[i]/np[i];
		v.z = zc[i]/np[i];

		iftInsertLabeledSet(&centers,iftGetVoxelIndex(label_image,v),i+1);
	}

	free(xc);
	free(yc);
	free(zc);

	free(np);

	return centers;
}

