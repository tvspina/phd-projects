#include "iftColor.h"

iftColorTable *iftCreateColorTable(int ncolors)
{
  iftColorTable *ctb = (iftColorTable *) calloc(1,sizeof(iftColorTable));
  int i;

  ctb->color = (iftColor *)calloc(ncolors,sizeof(iftColor));
  if (ctb->color == NULL) 
    iftError(MSG1,"iftCreateColorTable");

  ctb->ncolors = ncolors;

  for (i=0; i < ncolors; i++) {
    ctb->color[i].val[0] = iftRandomInteger(0,255);
    ctb->color[i].val[1] = iftRandomInteger(0,255);
    ctb->color[i].val[2] = iftRandomInteger(0,255);
  }

  return(ctb);
}

void iftDestroyColorTable(iftColorTable **ctb)
{
  iftColorTable *aux=*ctb;

  if (aux != NULL) {
    free(aux->color);
    free(aux);
    ctb = NULL;
  }

}

/* 
 http://www.equasys.de/colorconversion.html#YCbCr-RGBColorFormatConversion
 for 24-bit color images
*/


iftColor iftRGBtoYCbCr(iftColor cin)
{
  iftColor cout;
  


  cout.val[0]=(int)(0.257*(float)cin.val[0]+
		    0.504*(float)cin.val[1]+
		    0.098*(float)cin.val[2]+16.0);
  cout.val[1]=(int)(-0.148*(float)cin.val[0]+
		    -0.291*(float)cin.val[1]+
		    0.439*(float)cin.val[2]+128.0);
  cout.val[2]=(int)(0.439*(float)cin.val[0]+
		    -0.368*(float)cin.val[1]+
		    -0.071*(float)cin.val[2]+128.0);
   
  for(int i=0; i < 3; i++) {
    if (cout.val[i]<0)   cout.val[i]=0; 
    if (cout.val[i]>255) cout.val[i]=255; 
  }

  return(cout);
}

// http://www.equasys.de/colorconversion.html#YCbCr-RGBColorFormatConversion
// http://www.bluebit.gr/matrix-calculator/calculate.aspx

iftColor iftYCbCrtoRGB(iftColor cin)
{
  iftColor cout;
  
  cout.val[0]=(int)(1.164*((float)cin.val[0]-16.0)+
		    1.596*((float)cin.val[2]-128.0));
      
  cout.val[1]=(int)(1.164*((float)cin.val[0]-16.0)+
		    -0.392*((float)cin.val[1]-128.0)+
		    -0.813*((float)cin.val[2]-128.0));

  cout.val[2]=(int)(1.164*((float)cin.val[0]-16.0)+
		    2.017*((float)cin.val[1]-128.0));
  

  for(int i=0; i < 3; i++) {
    if (cout.val[i]<0)   cout.val[i]=0; 
    if (cout.val[i]>255) cout.val[i]=255; 
  }
			
  return(cout);
}

// L in [0, 99.998337]
// a in [-86.182236, 98.258614]
// b in [-107.867744, 94.481682]
iftFColor iftRGBtoLab(iftColor rgb)
{
	//RGB to XYZ
	float R = rgb.val[0]/255.;
	float G = rgb.val[1]/255.;
	float B = rgb.val[2]/255.;

	float X = (0.4123955889674142161*R + 0.3575834307637148171*G + 0.1804926473817015735*B);
	float Y = (0.2125862307855955516*R + 0.7151703037034108499*G + 0.07220049864333622685*B);
	float Z = (0.01929721549174694484*R + 0.1191838645808485318*G + 0.9504971251315797660*B);

	//XYZ to lab
	X /= WHITEPOINT_X;
	Y /= WHITEPOINT_Y;
	Z /= WHITEPOINT_Z;
	X = LABF(X);
	Y = LABF(Y);
	Z = LABF(Z);
	float L = 116*Y - 16;
	float a = 500*(X - Y);
	float b = 200*(Y - Z);

	iftFColor lab;
	lab.val[0] = L;
	lab.val[1] = a;
	lab.val[2] = b;

	return lab;
}

iftColor iftLabtoRGB(iftFColor lab)
{
	//Lab to XYZ
	float L = lab.val[0];
	float a = lab.val[1];
	float b = lab.val[2];

	L = (L + 16)/116;
	a = L + a/500;
	b = L - b/200;

	float X = WHITEPOINT_X*LABINVF(a);
	float Y = WHITEPOINT_Y*LABINVF(L);
	float Z = WHITEPOINT_Z*LABINVF(b);

	//XYZ to RGB
	float R = ( 3.2406*X - 1.5372*Y - 0.4986*Z);
	float G = (-0.9689*X + 1.8758*Y + 0.0415*Z);
	float B = ( 0.0557*X - 0.2040*Y + 1.0570*Z);

	iftColor rgb;
	rgb.val[0] = R*255.;
	rgb.val[1] = G*255.;
	rgb.val[2] = B*255.;

	return rgb;
}

iftColor iftLabtoQLab(iftFColor lab){
	iftColor cout;

	cout.val[0] = (lab.val[0] / 99.998337)*255;
	cout.val[1] = ((lab.val[1] + 86.182236)/(86.182236 + 98.258614) )*255;
	cout.val[2] = ((lab.val[2] + 107.867744)/(107.867744 + 94.481682))*255;

	return cout;
}

iftFColor iftQLabToLab(iftColor qlab){
	iftFColor cout;

	cout.val[0] = ((float)qlab.val[0]/255.0) * 99.998337;
	cout.val[1] = (((float)qlab.val[1]/255.0) * (86.182236 + 98.258614)) -86.182236;
	cout.val[2] = (((float)qlab.val[2]/255.0) * (107.867744 + 94.481682)) -107.867744;

	return cout;
}

