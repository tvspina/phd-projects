#include "iftCommon.h"

/*! \file
 * Author: Alexandre Falcao
 * Date: Aug 22, 2011
 * Last Update: Aug 22, 2011
 */

/*! \brief 
 * Allocates character memory array for n elements 
 */

char *iftAllocCharArray(int n)
{
  char *v=NULL;
  v = (char *) calloc(n,sizeof(char));
  if (v == NULL)
    iftError(MSG1,"iftAllocCharArray");
  return(v);
}

/*! \brief
 * Allocates unsigned character memory array for n elements. 
 */

uchar *iftAllocUCharArray(int n)
{
  uchar *v=NULL;
  v = (uchar *) calloc(n,sizeof(uchar));
  if (v == NULL)
    iftError(MSG1,"iftAllocUCharArray");
  return(v);
}

/*! \brief
 * Allocates short memory array for n elements. 
 */

short *iftAllocShortArray(int n)
{
  short *v=NULL;
  v = (short *) calloc(n,sizeof(short));
  if (v == NULL)
    iftError(MSG1,"iftAllocShortArray");
  return(v);
}

/*! \brief
 * Allocates unsigned short memory array for n elements. 
 */

ushort *iftAllocUShortArray(int n)
{
  ushort *v=NULL;
  v = (ushort *) calloc(n,sizeof(ushort));
  if (v == NULL)
    iftError(MSG1,"iftAllocUShortArray");
  return(v);
}

/*! \brief
 * Allocates integer memory array for n elements. 
 */

int *iftAllocIntArray(int n)
{
  int *v=NULL;
  v = (int *) calloc(n,sizeof(int));
  if (v == NULL)
    iftError(MSG1,"iftAllocIntArray");
  return(v);
}

/*! \brief
 * Allocates unsigned integer memory array for n elements. 
 */

uint *iftAllocUIntArray(int n)
{
  uint *v=NULL;
  v = (uint *) calloc(n,sizeof(uint));
  if (v == NULL)
    iftError(MSG1,"iftAllocUIntArray");
  return(v);
}

/*! \brief
 * Allocates unsigned long long memory array for n elements. 
 */

ullong *iftAllocULLongArray(int n)
{
  ullong *v=NULL;
  v = (ullong *) calloc(n,sizeof(ullong));
  if (v == NULL)
    iftError(MSG1,"iftAllocULLongArray");
  return(v);
}


/*! \brief
 * Allocates float memory array for n elements. 
 */

float *iftAllocFloatArray(int n)
{
  float *v=NULL;
  v = (float *) calloc(n,sizeof(float));
  if (v == NULL)
    iftError(MSG1,"iftAllocFloatArray");
  return(v);
}

void iftPrintFloatArray(float* v, int n)
{
  fprintf(stdout,"\n");
  for(int i=0;i<n;i++)
    fprintf(stdout,"%.4f ",v[i]);
  fprintf(stdout,"\n");
}
/*! \brief
 * Allocates double memory array for n elements. 
 */

double *iftAllocDoubleArray(int n)
{
  double *v=NULL;
  v = (double *) calloc(n,sizeof(double));
  if (v == NULL)
    iftError(MSG1,"iftAllocDoubleArray");
  return(v);
}

/*! \brief
 * Allocates complex memory array for n elements. 
 */

complex *iftAllocComplexArray(int n)
{
  complex *v=NULL;
  v = (complex *) calloc(n,sizeof(complex));
  if (v == NULL)
    iftError(MSG1,"iftAllocComplexArray");
  return(v);
}

/*! \brief
 * Allocates long double memory array for n elements. 
 */

long double *iftAllocLongDoubleArray(int n)
{
  long double *v=NULL;
  v = (long double *) calloc(n,sizeof(long double));
  if (v == NULL)
    iftError(MSG1,"iftAllocLongDoubleArray");
  return(v);
}

/*! \brief 
 * Error message msg is printed in function func and the
 * program exits abnormally.
 */

void iftError(char *msg,char *func){ 
  fprintf(stderr,"Error in %s: \n%s. \n",func,msg);
  exit(-1);
}

/*! \brief 
 * Warning message msg is printed in function func and the
 * program continues.
 */

void iftWarning(char *msg,char *func){ 
  fprintf(stdout,"Warning in %s: \n%s. \n",func,msg);
}

/*! \brief 
 * Switches the contents of integer variables.
 */

void iftSwitchValues(int *a, int *b){ 
  int c;
  c  = *a;
  *a = *b;
  *b = c;
}

/*! \brief 
 * Switches the contents of string variables.
 */

void iftSSwitchValues(char *a, char *b, int size){ 
  char c[size];
  strcpy(c,a);
  strcpy(a,b);
  strcpy(b,c);
}

/*! \brief 
 * Switches the contents of float variables.
 */

void iftFSwitchValues(float *a, float *b){ 
  float c;
  c  = *a;
  *a = *b;
  *b = c;
}

/*! \brief 
 * Switches the contents of float variables.
 */

void iftDSwitchValues(double *a, double *b){ 
  double c;
  c  = *a;
  *a = *b;
  *b = c;
}

/*! \brief 
 * Randomly selects an integer from low to high
 */

int iftRandomInteger (int low, int high){
  int k;
  double d;

  d = (double) rand () / ((double) RAND_MAX + 0.5);
  k =  MIN((int)(d * (double)(high - low + 1.0) ) + low, high);

  return k;
}

/*! \brief 
 * Randomly selects a normal distributed (N(0,1)) float number
 */

float iftRandomNormalFloat(void) {
  float v1,v2,s;

  do {
    v1 = 2.0 * ((float) rand()/RAND_MAX) - 1;
    v2 = 2.0 * ((float) rand()/RAND_MAX) - 1;

    s = v1*v1 + v2*v2;
  } while ( s >= 1.0 );

  if (s == 0.0)
    return 0.0;
  else
    return (v1*sqrt(-2.0 * log(s) / s));
}

/*! \brief 
 * Returns the distance between P0 and the line from P1 to P2,
 * whose size is P1P2.
 */

float iftVoxelLineDist2D(iftVoxel P0, iftVoxel P1, iftVoxel P2, float P1P2)
{
  return(fabs((P2.x - P1.x)*(P1.y - P0.y) - (P1.x - P0.x)*(P2.y - P1.y))/P1P2);
}


/*! \brief 
 * Returns the initial time 
 */

timer *iftTic(){ 
  timer *tic=NULL;
  tic = (timer *)malloc(sizeof(timer));
  gettimeofday(tic,NULL); 
  return(tic);
}

/*! \brief 
 * Returns the final time 
 */

timer *iftToc(){ 
  timer *toc=NULL;
  toc = (timer *)malloc(sizeof(timer));
  gettimeofday(toc,NULL);
  return(toc);
}

/*! \brief 
 * Returns the difference from the initial to the final times
 */

float iftCompTime(timer *tic, timer *toc) 
{ 
  float t=0.0;
  if ((tic!=NULL)&&(toc!=NULL)){
    t = (toc->tv_sec-tic->tv_sec)*1000.0 + 
      (toc->tv_usec-tic->tv_usec)*0.001;
    free(tic);free(toc);
  }
  return(t);
}

/*! \brief 
 * Generates a new seed for rand(), used in iftRandomInteger()
 */

void iftRandomSeed(unsigned int seed) 
{ 
 
  srand(seed);
  
}

/*! \brief 
 * Returns the factorial of a number or NIL in case of overflow 
 */

long double iftFactorial(int n)
{
  long double fact=1;
  int i;


  for (i=1; i <= n; i++) {    
    if (fact > (INFINITY_LDBL/i)){ // overflow
      return(NIL);
    }    
    fact=fact*i;
  }   
  return(fact);
}

/*! \brief 
 * Returns the limit to avoid overflow in the factorial computation
 */

int iftFactorialLimit()
{
  long double fact=1.0;
  int i;

  i = 1;
  while (fact < (INFINITY_LDBL/i)){
    fact=fact*i;
    i++;
  }
  return(i-1);
}

float iftVoxelDistance(iftVoxel u, iftVoxel v)
{
  return(sqrtf((u.x-v.x)*(u.x-v.x)+(u.y-v.y)*(u.y-v.y)+(u.z-v.z)*(u.z-v.z)));
}

float iftPointDistance(iftPoint u, iftPoint v)
{
  return(sqrtf((u.x-v.x)*(u.x-v.x)+(u.y-v.y)*(u.y-v.y)+(u.z-v.z)*(u.z-v.z)));
}

int iftVoxelSquareDistance(iftVoxel u, iftVoxel v)
{
  return((u.x-v.x)*(u.x-v.x)+(u.y-v.y)*(u.y-v.y)+(u.z-v.z)*(u.z-v.z));
}

iftVector iftCrossProduct(iftVector a, iftVector b)
{
  iftVector c;

  c.x = a.y*b.z - a.z*b.y;
  c.y = a.z*b.x - a.x*b.z;
  c.z = a.x*b.y - a.y*b.x;

  return(c);
}

float iftInnerProduct(iftVector a, iftVector b)
{
  return(a.x*b.x + a.y*b.y + a.z*b.z);
}

char iftCollinearPoints(iftPoint P1, iftPoint P2, iftPoint P3)
{
  iftVector a, b, c;

  a.x = P2.x - P1.x; a.y = P2.y - P1.y; a.z = P2.z - P1.z; 
  b.x = P3.x - P1.x; b.y = P3.y - P1.y; b.z = P3.z - P1.z; 
  c = iftCrossProduct(a,b);
  if ((fabs(c.x)<Epsilon)&&(fabs(c.y)<Epsilon)&&(fabs(c.z)<Epsilon))
    return(1);
  else
    return(0);
}

char iftCollinearVoxels(iftVoxel P1, iftVoxel P2, iftVoxel P3)
{
  iftVector a, b, c;

  a.x = P2.x - P1.x; a.y = P2.y - P1.y; a.z = P2.z - P1.z; 
  b.x = P3.x - P1.x; b.y = P3.y - P1.y; b.z = P3.z - P1.z; 
  c = iftCrossProduct(a,b);
  if ((fabs(c.x)<1.0)&&(fabs(c.y)<1.0)&&(fabs(c.z)<1.0))
    return(1);
  else
    return(0);
}

iftVector iftNormalizeVector(iftVector v)
{
  iftVector u;
  float     m = iftVectorMagnitude(v);

  u.x = v.x; u.y = v.y; u.z = v.z;

  if (m!=0.0){
    u.x = v.x/m;
    u.y = v.y/m;
    u.z = v.z/m;
  }

  return(u);
}

float iftVectorMagnitude(iftVector v)
{
  return(sqrtf(v.x*v.x + v.y*v.y + v.z*v.z)); 
}

int iftVoxelLinePosition2D (iftVoxel P0, iftVoxel P1, iftVoxel P2)
{
    return (P2.x - P1.x) * (P0.y - P1.y) - (P2.y - P1.y) * (P0.x - P1.x);   
}

void iftRemoveCarriageReturn(char *token)
{
  int i;

  for (i=0; i < strlen(token); i++){     
    if ((token[i]=='\n')||(token[i]=='\r')){
      token[i]='\0';	
    }
  }
}

void iftSwitchVoxels(iftVoxel *u, iftVoxel *v)
{
  iftVoxel w;

  w  = *v;
  *v = *u;
  *u = w;
}

void iftWriteFloatArray(float *v, int size, char *filename)
{
  int i;
  FILE *fp=fopen(filename,"w");
  
  fprintf(fp,"%d\n",size);
  for (i=0; i < size; i++) 
    fprintf(fp,"%f\n",v[i]);

  fclose(fp);
}

float *iftReadFloatArray(char *filename, int *size)
{
  int i;
  float *v;
  FILE  *fp=fopen(filename,"r");
  
  if (fscanf(fp,"%d\n",size)!=1) iftError("Reading error","iftReadFloatArray");
  v = iftAllocFloatArray(*size);
  for (i=0; i < *size; i++) 
    if (fscanf(fp,"%f\n",&v[i])!=1) iftError("Reading error","iftReadFloatArray");

  fclose(fp);

  return(v);
}

float iftSigmoidalValue(float value, float alfa)
{
   float result;

   result = 1 / (1 + exp(-alfa * value));

   return(result);
}

void iftVerifyToken(FILE *fp, char *token, char *function)
{
  char label[30];

  if ((fscanf(fp,"%s",label)!=1)||(strcmp(label,token)!=0)){
    char msg[100];
    sprintf(msg,"Could not find token %s",token);
    iftError(msg,function);
  }

}

void iftReadIntValue(FILE *fp, int *value, char *token, char *function)
{
  
  iftVerifyToken(fp, token, function);
  if (fscanf(fp,"%d\n",value)!=1){
    char msg[100];
    sprintf(msg,"Could not read %s value",token);
    iftError(msg,function);
  }
}

void iftReadIntValues(FILE *fp, int **value, int nvalues, char *token, char *function)
{
  
  iftVerifyToken(fp, token, function);
  for (int i = 0; i < nvalues; i++) {
    if (fscanf(fp,"%d",&(*value)[i])!=1){
      char msg[100];
      sprintf(msg,"Could not read %s value",token);
      iftError(msg,function);
    }
  }
  if (fscanf(fp,"\n")!=0){
    iftError("Could not read \\n in file","iftReadIntValues");
  }
}

void iftWriteIntValue(FILE *fp, int value, char *token)
{
  fprintf(fp,"%s %d\n",token,value);
}

void iftWriteIntValues(FILE *fp, int *value, int nvalues, char *token)
{
  fprintf(fp,"%s",token);
  for (int i=0; i < nvalues; i++) {
    fprintf(fp," %d",value[i]);
  }
  fprintf(fp,"\n");
}

void iftReadFloatValue(FILE *fp, float *value, char *token, char *function)
{
  iftVerifyToken(fp, token, function);
  if (fscanf(fp,"%f\n",value)!=1){
    char msg[100];
    sprintf(msg,"Could not read %s value",token);
    iftError(msg,function);
  }
}

void iftReadFloatValues(FILE *fp, float **value, int nvalues, char *token, char *function)
{
  
  iftVerifyToken(fp, token, function);
  for (int i = 0; i < nvalues; i++) {
    if (fscanf(fp,"%f",&(*value)[i])!=1){
      char msg[100];
      sprintf(msg,"Could not read %s value",token);
      iftError(msg,function);
    }
  }
  if (fscanf(fp,"\n")!=0){
    iftError("Could not read \\n in file","iftReadFloatValues");
  }
}

void iftWriteFloatValue(FILE *fp, float value, char *token)
{
  fprintf(fp,"%s %f\n",token,value);
}

void iftWriteFloatValues(FILE *fp, float *value, int nvalues, char *token)
{
  fprintf(fp,"%s",token);
  for (int i=0; i < nvalues; i++) {
    fprintf(fp," %f",value[i]);
  }
  fprintf(fp,"\n");
}

void iftReadDoubleValue(FILE *fp, double *value, char *token, char *function)
{
  
  iftVerifyToken(fp, token, function);
  if (fscanf(fp,"%lf\n",value)!=1){
    char msg[100];
    sprintf(msg,"Could not read %s value",token);
    iftError(msg,function);
  }
}

void iftReadDoubleValues(FILE *fp, double **value, int nvalues, char *token, char *function)
{
  
  iftVerifyToken(fp, token, function);
  for (int i = 0; i < nvalues; i++) {
    if (fscanf(fp,"%lf",&(*value)[i])!=1){
      char msg[100];
      sprintf(msg,"Could not read %s value",token);
      iftError(msg,function);
    }
  }
  if (fscanf(fp,"\n")!=0){
    iftError("Could not read \\n in file","iftReadDoubleValues");
  }
}

void iftWriteDoubleValue(FILE *fp, double value, char *token)
{
  fprintf(fp,"%s %lf\n",token,value);
}

void iftWriteDoubleValues(FILE *fp, double *value, int nvalues, char *token)
{
  fprintf(fp,"%s",token);
  for (int i=0; i < nvalues; i++) {
    fprintf(fp," %lf",value[i]);
  }
  fprintf(fp,"\n");
}

void iftSkipComments(FILE *fp)
{
    //skip for comments   
    while (fgetc(fp) == '#') { 
      while (fgetc(fp) != '\n');
    }
    fseek(fp,-1,SEEK_CUR);

}

