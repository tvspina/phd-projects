#include "iftFHeap.h"

iftFHeap *iftCreateFHeap(int n, float *value) {
  iftFHeap *H = NULL;
  int i;

  if (value == NULL) {
    iftError("Cannot create heap without priority value map","iftCreateFHeap");
  }

  H = (iftFHeap *) malloc(sizeof(iftFHeap));
  if (H != NULL) {
    H->n       = n;
    H->value    = value;
    H->color   = (char *) malloc(sizeof(char) * n);
    H->node   = (int *) malloc(sizeof(int) * n);
    H->pos     = (int *) malloc(sizeof(int) * n);
    H->last    = -1;
    H->removal_policy = MINVALUE;
    if (H->color == NULL || H->pos == NULL || H->node == NULL)
      iftError(MSG1,"iftCreateFHeap");
    for (i = 0; i < H->n; i++) {
      H->color[i] = WHITE;
      H->pos[i]   = -1;
      H->node[i] = -1;
    }    
  } 
  else
    iftError(MSG1,"iftCreateFHeap");

  return H;
}

void iftDestroyFHeap(iftFHeap **H) {
  iftFHeap *aux = *H;
  if (aux != NULL) {
    if (aux->node != NULL) free(aux->node);
    if (aux->color != NULL) free(aux->color);
    if (aux->pos != NULL)   free(aux->pos);
    free(aux);
    *H = NULL;
  }
}



void  iftGoUpFHeap(iftFHeap *H, int i) {
  int j = iftDad(i);

  if(H->removal_policy == MINVALUE){

    while ((i > 0) && (H->value[H->node[j]] > H->value[H->node[i]])) {
      iftSwitchValues(&H->node[j], &H->node[i]);
      H->pos[H->node[i]] = i;
      H->pos[H->node[j]] = j;
      i = j;
      j = iftDad(i);
    }
  }
  else{ /* removal_policy == MAXVALUE */

    while ((i > 0) && (H->value[H->node[j]] < H->value[H->node[i]])) {
      iftSwitchValues(&H->node[j], &H->node[i]);
      H->pos[H->node[i]] = i;
      H->pos[H->node[j]] = j;
      i = j;
      j = iftDad(i);
    }
  }
}

void iftGoDownFHeap(iftFHeap *H, int i) {
  int j, left = iftLeftSon(i), right = iftRightSon(i);

  j = i;
  if(H->removal_policy == MINVALUE){

    if ((left <= H->last) && 
	(H->value[H->node[left]] < H->value[H->node[i]]))
      j = left;
    if ((right <= H->last) && 
	(H->value[H->node[right]] < H->value[H->node[j]]))
      j = right;
  }
  else{ /* removal_policy == MAXVALUE */

    if ((left <= H->last) && 
	(H->value[H->node[left]] > H->value[H->node[i]]))
      j = left;
    if ((right <= H->last) && 
	(H->value[H->node[right]] > H->value[H->node[j]]))
      j = right;
  }

  if(j != i) {
    iftSwitchValues(&H->node[j], &H->node[i]);
    H->pos[H->node[i]] = i;
    H->pos[H->node[j]] = j;
    iftGoDownFHeap(H, j);
 }
}

char iftFullFHeap(iftFHeap *H) {
  if (H->last == (H->n - 1))
    return 1;
  else
    return 0;
}

char iftEmptyFHeap(iftFHeap *H) {
  if (H->last == -1){
    return 1;
  }else{
    return 0;
  }
}


char iftInsertFHeap(iftFHeap *H, int node) {

  if (!iftFullFHeap(H)) {
    H->last++;
    H->node[H->last] = node;
    H->color[node]   = GRAY;
    H->pos[node]     = H->last;
    iftGoUpFHeap(H, H->last); 
    return 1;
  } else {
    iftWarning("FHeap is full","iftInsertFHeap");
    return 0;
  }

}

int iftRemoveFHeap(iftFHeap *H) {
  int node=NIL;

  if (!iftEmptyFHeap(H)) {
    node = H->node[0];  
    H->pos[node]   = -1;
    H->color[node] = BLACK;
    H->node[0]      = H->node[H->last];
    H->pos[H->node[0]] = 0;
    H->node[H->last] = -1;
    H->last--;
    iftGoDownFHeap(H, 0);
  }else{
    iftWarning("FHeap is empty","iftRemoveFHeap");
  }
  
  return node;

}

void    iftRemoveFHeapElem(iftFHeap *H, int pixel){
	if(H->pos[pixel] == -1)
		iftError("Element is not in the Heap", "iftRemoveFHeapElem");

	if(H->removal_policy == MINVALUE)
		H->value[pixel] = -INFINITY_FLT;
	else
		H->value[pixel] = INFINITY_FLT;

	iftGoUpFHeap(H, H->pos[pixel]);
	int p = iftRemoveFHeap(H);

	if(p != pixel)
		iftError("FHeap could not remove element","iftRemoveFHeapElem");
}

void iftResetFHeap(iftFHeap *H)
{
  int i;

  H->removal_policy = MINVALUE;
  for (i=0; i < H->n; i++) {
    H->color[i] = WHITE;
    H->pos[i]   = -1;
    H->node[i] = -1;
  }
  H->last = -1;
}



