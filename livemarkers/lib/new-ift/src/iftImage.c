#include "iftImage.h"


/*---------------------------- Public Functions ---------------------------*/

void iftVerifyImageDomains(iftImage *img1, iftImage *img2)
{
    if ((img1->xsize!=img2->xsize)||
            (img1->ysize!=img2->ysize)||
            (img1->zsize!=img2->zsize))
    {
        iftError("Images must have the same domain","iftVerifyImageDomains");
    }
}

char iftIsColorImage(iftImage *img)
{
    if ((img->Cb != NULL)&&(img->Cr != NULL))
        return(1);
    else
        return(0);
}

char iftIs3DImage(iftImage *img)
{
    if (img->zsize > 1)
        return(1);
    else
        return(0);
}

int iftXSize(iftImage *img)
{
    return(img->xsize);
}

int iftYSize(iftImage *img)
{
    return(img->ysize);
}

int iftZSize(iftImage *img)
{
    return(img->zsize);
}


iftImage  *iftCreateImage(int xsize,int ysize,int zsize)
{
    iftImage *img=NULL;
    int     y,z,xysize;

    img = (iftImage *) calloc(1,sizeof(iftImage));
    if (img == NULL)
    {
        iftError(MSG1,"iftCreateImage");
    }

    img->val     = iftAllocIntArray(xsize*ysize*zsize);
    img->Cb=img->Cr=NULL;
    img->xsize   = xsize;
    img->ysize   = ysize;
    img->zsize   = zsize;
    img->dx      = 1.0;
    img->dy      = 1.0;
    img->dz      = 1.0;
    img->tby     = iftAllocIntArray(ysize);
    img->tbz     = iftAllocIntArray(zsize);
    img->maxval  = 0;
    img->minval  = 0;
    img->n       = xsize*ysize*zsize;

    if (img->val==NULL || img->tbz==NULL || img->tby==NULL)
    {
        iftError(MSG1,"iftCreateImage");
    }

    img->tby[0]=0;
    for (y=1; y < ysize; y++)
        img->tby[y]=img->tby[y-1] + xsize;

    img->tbz[0]=0;
    xysize = xsize*ysize;
    for (z=1; z < zsize; z++)
        img->tbz[z]=img->tbz[z-1] + xysize;

    return(img);
}

iftImage  *iftCreateColorImage(int xsize,int ysize,int zsize)
{
    iftImage *img=NULL;
    int     y,z,xysize;

    img = (iftImage *) calloc(1,sizeof(iftImage));
    if (img == NULL)
    {
        iftError(MSG1,"iftCreateImage");
    }

    img->val     = iftAllocIntArray(xsize*ysize*zsize);
    img->Cb      = iftAllocUShortArray(xsize*ysize*zsize);
    img->Cr      = iftAllocUShortArray(xsize*ysize*zsize);
    img->xsize   = xsize;
    img->ysize   = ysize;
    img->zsize   = zsize;
    img->dx      = 1.0;
    img->dy      = 1.0;
    img->dz      = 1.0;
    img->tby     = iftAllocIntArray(ysize);
    img->tbz     = iftAllocIntArray(zsize);
    img->maxval  = 0;
    img->minval  = 0;
    img->n       = xsize*ysize*zsize;

    if (img->val==NULL || img->tbz==NULL || img->tby==NULL)
    {
        iftError(MSG1,"iftCreateImage");
    }

    img->tby[0]=0;
    for (y=1; y < ysize; y++)
        img->tby[y]=img->tby[y-1] + xsize;

    img->tbz[0]=0;
    xysize = xsize*ysize;
    for (z=1; z < zsize; z++)
        img->tbz[z]=img->tbz[z-1] + xysize;

    return(img);
}

void  iftDestroyPyImage(iftImage *img)
{
    iftDestroyImage(&img);
}

void  iftDestroyImage(iftImage **img)
{
    iftImage *aux = *img;

    if(aux != NULL)
    {
        if (aux->val  != NULL)  free(aux->val);
        if (aux->Cb  != NULL)  free(aux->Cb);
        if (aux->Cr  != NULL)  free(aux->Cr);
        if (aux->tby  != NULL)  free(aux->tby);
        if (aux->tbz  != NULL)  free(aux->tbz);
        free(aux);
        *img = NULL;
    }
}


void iftCopyVoxelSize(iftImage *img1, iftImage *img2)
{
    img2->dx = img1->dx;
    img2->dy = img1->dy;
    img2->dz = img1->dz;
}

void iftUpdateMinMax(iftImage *img)
{
    img->minval = iftMinimumValue(img);
    img->maxval = iftMaximumValue(img);
}

char iftValidVoxel(iftImage *img, iftVoxel v)
{
    if ((v.x >= 0)&&(v.x < img->xsize)&&
            (v.y >= 0)&&(v.y < img->ysize)&&
            (v.z >= 0)&&(v.z < img->zsize))
        return(1);
    else
        return(0);
}


void     iftCopyCbCr(iftImage *img1, iftImage *img2)
{
    int p;

    if ((img1->xsize != img2->xsize)||(img1->ysize!=img2->ysize)||(img1->zsize!=img2->zsize))
        iftError("Images must have the same domain","iftCopyCbCr");

    if (img2->Cb == NULL)
    {
        img2->Cb = iftAllocUShortArray(img2->n);
        img2->Cr = iftAllocUShortArray(img2->n);
    }

    for (p=0; p < img2->n; p++)
    {
        img2->Cb[p] = img1->Cb[p];
        img2->Cr[p] = img1->Cr[p];
    }

}


void    iftSetCbCr(iftImage *img, ushort value)
{
    int p;

    if (img->Cb == NULL)
    {
        img->Cb = iftAllocUShortArray(img->n);
        img->Cr = iftAllocUShortArray(img->n);
    }

    for (p=0; p < img->n; p++)
    {
        img->Cb[p] = value;
        img->Cr[p] = value;
    }
}


int iftMaximumValue(iftImage *img)
{
    int p;

    img->maxval = -INFINITY_INT;
    for (p=0; p < img->n; p++)
        if (img->maxval < img->val[p])
            img->maxval = img->val[p];

    return(img->maxval);
}

int iftMinimumValue(iftImage *img)
{
    int p;

    img->minval = INFINITY_INT;
    for (p=0; p < img->n; p++)
        if (img->minval > img->val[p])
            img->minval = img->val[p];

    return(img->minval);
}

int iftMaximumCb(iftImage *img)
{
    int p, max;

    if (img->Cb == NULL)
        iftError("Image is grayscale","iftMaximumCb");

    max = -INFINITY_INT;
    for (p=0; p < img->n; p++)
        if (max < img->Cb[p])
            max = img->Cb[p];

    return(max);
}

int iftMaximumCr(iftImage *img)
{
    int p, max;

    if (img->Cr == NULL)
        iftError("Image is grayscale","iftMaximumCr");

    max = -INFINITY_INT;
    for (p=0; p < img->n; p++)
        if (max < img->Cr[p])
            max = img->Cr[p];

    return(max);
}


iftImage *iftReadImage(char *filename)
{
    iftImage  *img=NULL;
    FILE    *fp=NULL;
    uchar   *data8=NULL;
    ushort  *data16=NULL;
    int     *data32=NULL;
    char    type[10];
    int     p,v,xsize,ysize,zsize;

    fp = fopen(filename,"r");
    if (fp == NULL)
    {
        iftError(MSG2,"iftReadImage");
    }
    if (fscanf(fp,"%s\n",type) != 1)
        iftError("Reading error","iftReadImage");

    if((strcmp(type,"SCN")==0))
    {

        iftSkipComments(fp);

        if (fscanf(fp,"%d %d %d\n",&xsize,&ysize,&zsize)!=3)
            iftError("Reading error","iftReadImage");
        img = iftCreateImage(xsize,ysize,zsize);
        if (fscanf(fp,"%f %f %f\n",&img->dx,&img->dy,&img->dz)!=3)
            iftError("Reading error","iftReadImage");
        if (fscanf(fp,"%d\n",&v)!=1)
            iftError("Reading error","iftReadImage");

        if (v==8)
        {
            data8  = iftAllocUCharArray(img->n);

            if (fread(data8,sizeof(uchar),img->n,fp)!=img->n)
                iftError("Reading error","iftReadImage");
            for (p=0; p < img->n; p++)
                img->val[p] = (int) data8[p];
            free(data8);
        }
        else if (v==16)
        {
            data16 = iftAllocUShortArray(img->n);

            if (fread(data16,sizeof(ushort),img->n,fp)!=img->n)
                iftError("Reading error","iftReadImage");
            for (p=0; p < img->n; p++)
                img->val[p] = (int) data16[p];
            free(data16);
        }
        else if (v==32)
        {
            data32 = iftAllocIntArray(img->n);
            if (fread(data32,sizeof(int),img->n,fp)!=img->n)
                iftError("Reading error","iftReadImage");
            for (p=0; p < img->n; p++)
                img->val[p] = data32[p];
            free(data32);
        }
        else
        {
            iftError("Input scene must be 8, 16, or 32 bit","iftReadImage");
        }
        img->minval = iftMinimumValue(img);
        img->maxval = iftMaximumValue(img);
    }
    else
    {
        iftError("Invalid file type","iftReadImage");
    }

    fclose(fp);
    return(img);
}

void iftWriteImage(iftImage *img, char *filename)
{
    FILE *fp=NULL;
    int   p;
    uchar  *data8 =NULL;
    ushort *data16=NULL;
    int    *data32=NULL;

    img->minval = iftMinimumValue(img);
    img->maxval = iftMaximumValue(img);
    if (img->minval < 0)
    {
        char msg[200];
        sprintf(msg,"Shifting image values from [%d,%d] to [%d,%d]\n",img->minval,img->maxval,0,img->maxval-img->minval);
        iftWarning(msg,"iftWriteImage");
        for (p=0; p < img->n; p++)
            img->val[p] = img->val[p] - img->minval;
        img->maxval = img->maxval - img->minval;
    }

    fp = fopen(filename,"w");
    if (fp == NULL)
        iftError(MSG2,"iftWriteImage");

    fprintf(fp,"SCN\n");
    fprintf(fp,"%d %d %d\n",img->xsize,img->ysize,img->zsize);
    fprintf(fp,"%f %f %f\n",img->dx,img->dy,img->dz);


    if (img->maxval < 256)
    {
        fprintf(fp,"%d\n",8);
        data8 = iftAllocUCharArray(img->n);
        for (p=0; p < img->n; p++)
            data8[p] = (uchar) img->val[p];
        fwrite(data8,sizeof(uchar),img->n,fp);
        free(data8);
    }
    else if (img->maxval < 65536)
    {
        fprintf(fp,"%d\n",16);
        data16 = iftAllocUShortArray(img->n);
        for (p=0; p < img->n; p++)
            data16[p] = (ushort) img->val[p];
        fwrite(data16,sizeof(ushort),img->n,fp);
        free(data16);
    }
    else if (img->maxval < INFINITY_INT)
    {
        fprintf(fp,"%d\n",32);
        data32 = iftAllocIntArray(img->n);
        for (p=0; p < img->n; p++)
            data32[p] =  img->val[p];
        fwrite(data32,sizeof(int),img->n,fp);
        free(data32);
    }

    fclose(fp);
}

iftImage *iftReadImageP5(char *filename)
{
    iftImage  *img=NULL;
    FILE    *fp=NULL;
    uchar   *data8=NULL;
    ushort  *data16=NULL;
    char    type[10];
    int     p,v,xsize,ysize,zsize,hi,lo;

    fp = fopen(filename,"r");
    if (fp == NULL)
    {
        iftError(MSG2,"iftReadImageP5");
    }

    if (fscanf(fp,"%s\n",type) != 1)
        iftError("Reading error","iftReadImageP5");

    if((strcmp(type,"P5")==0))
    {

        iftSkipComments(fp);

        if (fscanf(fp,"%d %d\n",&xsize,&ysize)!=2)
            iftError("Reading error","iftReadImageP5");
        zsize = 1;

        img = iftCreateImage(xsize,ysize,zsize);
        img->dz=0.0;

        if (fscanf(fp,"%d",&v)!=1)
            iftError("Reading error","iftReadImageP5");

        while(fgetc(fp) != '\n');

        if ((v<=255)&&(v>0))
        {
            data8  = iftAllocUCharArray(img->n);

            if (fread(data8,sizeof(uchar),img->n,fp)!=img->n)
                iftError("Reading error","iftReadImageP5");

            for (p=0; p < img->n; p++)
                img->val[p] = (int) data8[p];

            free(data8);

        }
        else if ((v<=65535)&&(v>255))
        {
            data16 = iftAllocUShortArray(img->n);

            for (p=0; p< img->n; p++)
            {
                if ( (hi = fgetc(fp)) == EOF)
                    iftError("Reading error","iftReadImageP5");
                if ( (lo = fgetc(fp)) == EOF)
                    iftError("Reading error","iftReadImageP5");

                data16[p] = (hi << 8) + lo;
            }

            for (p=0; p < img->n; p++)
                img->val[p] = (int) data16[p];

            free(data16);

        }
        else
        {
            iftError("Invalid maximum value","iftReadImageP5");
        }
        img->maxval = iftMaximumValue(img);
        img->minval = iftMinimumValue(img);
    }
    else
    {
        iftError("Invalid image type","iftReadImageP5");
    }

    fclose(fp);
    return(img);
}

void iftWriteImageP5(iftImage *img, char *filename)
{
    FILE *fp=NULL;
    int   p,hi,lo;
    uchar  *data8 =NULL;
    ushort *data16=NULL;

    fp = fopen(filename,"w");
    if (fp == NULL)
        iftError(MSG2,"iftWriteImageP5");

    fprintf(fp,"P5\n");
    fprintf(fp,"%d %d\n",img->xsize,img->ysize);

    img->maxval = iftMaximumValue(img);
    img->minval = iftMinimumValue(img);

    if ((img->maxval < 256)&&(img->minval>=0))
    {
        fprintf(fp,"%d\n",255);
        data8 = iftAllocUCharArray(img->n);
        for (p=0; p < img->n; p++)
            data8[p] = (uchar) img->val[p];
        fwrite(data8,sizeof(uchar),img->n,fp);
        free(data8);
    }
    else if (img->maxval < 65536)
    {
        fprintf(fp,"%d\n",65535);
        data16 = iftAllocUShortArray(img->n);
        for (p=0; p < img->n; p++)
            data16[p] = (ushort) img->val[p];

        {
#define HI(num) (((num) & 0x0000FF00) >> 8)
#define LO(num) ((num) & 0x000000FF)
            for (p = 0; p < img->n; p++)
            {
                hi = HI(data16[p]);
                lo = LO(data16[p]);
                fputc(hi, fp);
                fputc(lo, fp);
            }
        }

        free(data16);
    }
    else
    {
        char msg[200];
        sprintf(msg,"Cannot write image as P5 (%d/%d)",img->maxval,img->minval);
        iftError(msg,"iftWriteImageP5");
    }
    fclose(fp);
}

iftImage *iftReadImageAsP5(char *filename)
{
    char command[400];
    iftImage *img;

    sprintf(command,"convert %s temp.pgm",filename);
    if (system(command)==-1) iftError("Command error","iftReadImageAsP5");
    img = iftReadImageP5("temp.pgm");
    if (system("rm -f temp.pgm")==-1) iftError("Command error","iftReadImageAsP5");
    return(img);
}

void iftWriteImageExtFormat(iftImage *img, char *filename)
{
    char command[400];

    if (iftIs3DImage(img))
        iftError("It is not handling 3D images yet","iftWriteImageExtFormat");

    if (iftIsColorImage(img))
    {
        iftWriteImageP6(img,"temp.ppm");
        sprintf(command,"convert temp.ppm %s",filename);
        if (system(command)==-1) iftError("Command error","iftWriteImageExtFormat");
        if (system("rm -f temp.ppm")==-1) iftError("Command error","iftWriteImageExtFormat");
    }
    else
    {
        iftWriteImageP5(img,"temp.pgm");
        sprintf(command,"convert temp.pgm %s",filename);
        if (system(command)==-1) iftError("Command error","iftWriteImageExtFormat");
        if (system("rm -f temp.pgm")==-1) iftError("Command error","iftWriteImageExtFormat");
    }
}

iftImage *iftReadImageAsP6(char *filename)
{
    char command[400];
    iftImage *img;

    sprintf(command,"convert %s temp.ppm",filename);
    if(system(command)==-1) iftError("Command error","iftReadImageAsP6");
    img = iftReadImageP6("temp.ppm");
    if(system("rm -f temp.ppm")==-1) iftError("Command error","iftReadImageAsP6");;
    return(img);
}

iftImage *iftReadImageP6(char *filename)
{
    iftImage  *img=NULL;
    FILE    *fp=NULL;
    char    type[10];
    int     p,v,xsize,ysize,zsize;
    iftColor RGB,YCbCr;

    fp = fopen(filename,"r");
    if (fp == NULL)
    {
        iftError(MSG2,"iftReadImageP6");
    }

    if(fscanf(fp,"%s\n",type)!=1)
        iftError("Reading error","iftReadImageP6");
    if((strcmp(type,"P6")==0))
    {

        iftSkipComments(fp);

        if(fscanf(fp,"%d %d\n",&xsize,&ysize)!=2)
            iftError("Reading error","iftReadImageP6");

        zsize = 1;
        img = iftCreateImage(xsize,ysize,zsize);
        img->Cb = iftAllocUShortArray(img->n);
        img->Cr = iftAllocUShortArray(img->n);
        img->dz=0.0;
        if (fscanf(fp,"%d",&v)!=1)
            iftError("Reading error","iftReadImageP6");

        while(fgetc(fp) != '\n');

        if (v==255)
        {

            for (p=0; p < img->n; p++)
            {
                RGB.val[0] = fgetc(fp);
                RGB.val[1] = fgetc(fp);
                RGB.val[2] = fgetc(fp);
                YCbCr      = iftRGBtoYCbCr(RGB);
                img->val[p]=YCbCr.val[0];
                img->Cb[p] =(ushort)YCbCr.val[1];
                img->Cr[p] =(ushort)YCbCr.val[2];
            }
        }
        else
        {
            iftError("Invalid maximum value","iftReadImageP6");
        }
        img->maxval = iftMaximumValue(img);
        img->minval = iftMinimumValue(img);
    }
    else
    {
        iftError("Invalid image type","iftReadImageP6");
    }

    fclose(fp);
    return(img);
}

void iftWriteImageP6(iftImage *img, char *filename)
{
    FILE *fp=NULL;
    int   p;
    iftColor YCbCr,RGB;

    fp = fopen(filename,"w");
    if (fp == NULL)
        iftError(MSG2,"iftWriteImageP6");

    fprintf(fp,"P6\n");
    fprintf(fp,"%d %d\n",img->xsize,img->ysize);

    img->maxval = iftMaximumValue(img);
    img->minval = iftMinimumValue(img);

    if ((img->maxval < 256)&&(img->minval>=0))
    {
        fprintf(fp,"%d\n",255);
        for (p=0; p < img->n; p++)
        {
            YCbCr.val[0] = img->val[p];
            YCbCr.val[1] = img->Cb[p];
            YCbCr.val[2] = img->Cr[p];

            RGB = iftYCbCrtoRGB(YCbCr);

            fputc(((uchar)RGB.val[0]),fp);
            fputc(((uchar)RGB.val[1]),fp);
            fputc(((uchar)RGB.val[2]),fp);
        }
    }
    else
    {
        iftError("Cannot write image as P6","iftWriteImageP6");
    }
    fclose(fp);
}

iftImage *iftReadImageP2(char *filename)
{
    iftImage  *img=NULL;
    FILE    *fp=NULL;
    char    type[10];
    int     p,v,xsize,ysize,zsize;

    fp = fopen(filename,"r");

    if (fp == NULL)
    {
        iftError(MSG2,"iftReadImageP2");
    }

    if (fscanf(fp,"%s\n",type)!=1)
        iftError("Reading error","iftReadImageP2");

    if((strcmp(type,"P2")==0))
    {

        iftSkipComments(fp);

        if(fscanf(fp,"%d %d\n",&xsize,&ysize)!=2)
            iftError("Reading error","iftReadImageP2");
        zsize = 1;
        img = iftCreateImage(xsize,ysize,zsize);
        img->dz=0.0;
        if(fscanf(fp,"%d\n",&v)!=1)
            iftError("Reading error","iftReadImageP2");

        for (p=0; p < img->n; p++)
            if(fscanf(fp,"%d",&img->val[p])!=1)
                iftError("Reading error","iftReadImageP2");
        img->maxval = iftMaximumValue(img);
        img->minval = iftMinimumValue(img);

    }
    else
    {
        iftError("Invalid image type","iftReadImageP2");
    }

    fclose(fp);
    return(img);
}

void iftWriteImageP2(iftImage *img, char *filename)
{
    FILE *fp=NULL;
    int   p;

    fp = fopen(filename,"w");
    if (fp == NULL)
        iftError(MSG2,"iftWriteImageP2");

    fprintf(fp,"P2\n");
    fprintf(fp,"%d %d\n",img->xsize,img->ysize);

    img->maxval = iftMaximumValue(img);

    fprintf(fp,"%d\n",img->maxval);
    for (p=0; p < img->n; p++)
        fprintf(fp,"%d ",img->val[p]);

    fclose(fp);
}

iftImage *iftExtractObject(iftImage *label, int obj_code, iftVoxel *pos)
{
    int p,q;
    iftVoxel uo,uf,u;
    iftImage *bin=NULL;


    if ((obj_code<1)||(obj_code>iftMaximumValue(label)))
    {
        iftError("Invalid object code","iftExtractObject");
    }

    uo.x = uo.y = uo.z = INFINITY_INT;
    uf.x = uf.y = uf.z = -INFINITY_INT;
    for (u.z=0; u.z < label->zsize; u.z++)
        for (u.y=0; u.y < label->ysize; u.y++)
            for (u.x=0; u.x < label->xsize; u.x++)
            {
                p = iftGetVoxelIndex(label,u);
                if (label->val[p]==obj_code)
                {
                    if (u.x < uo.x) uo.x = u.x;
                    if (u.y < uo.y) uo.y = u.y;
                    if (u.z < uo.z) uo.z = u.z;
                    if (u.x > uf.x) uf.x = u.x;
                    if (u.y > uf.y) uf.y = u.y;
                    if (u.z > uf.z) uf.z = u.z;
                }
            }

    bin = iftCreateImage(uf.x-uo.x+1,uf.y-uo.y+1,uf.z-uo.z+1);

    q = 0;
    for (u.z=uo.z; u.z <= uf.z; u.z++)
        for (u.y=uo.y; u.y <= uf.y; u.y++)
            for (u.x=uo.x; u.x <= uf.x; u.x++)
            {
                p = iftGetVoxelIndex(label,u);
                if (label->val[p]==obj_code)
                {
                    bin->val[q]=obj_code;
                }
                q++;
            }

    *pos = uo;

    iftCopyVoxelSize(label,bin);

    return(bin);
}

void iftInsertObject(iftImage *bin, iftImage *label, int obj_code, iftVoxel  pos)
{
    int p,q;
    iftVoxel u,v;

    if (!iftValidVoxel(label,pos))
        iftError("Invalid position for insertion","iftInsertObject");

    if ((bin->xsize > label->xsize) ||
            (bin->ysize > label->ysize) ||
            (bin->zsize > label->zsize))
        iftError("Object cannot be inserted","iftInsertObject");

    for (u.z=0; u.z < bin->zsize; u.z++)
        for (u.y=0; u.y < bin->ysize; u.y++)
            for (u.x=0; u.x < bin->xsize; u.x++)
            {
                v.x = u.x + pos.x;
                v.y = u.y + pos.y;
                v.z = u.z + pos.z;
                if (iftValidVoxel(label,v))
                {
                    p = iftGetVoxelIndex(bin,u);
                    q = iftGetVoxelIndex(label,v);
                    if (bin->val[p])
                        label->val[q] = obj_code;
                }
            }
}

iftImage  *iftCopyImage(iftImage *img)
{
    iftImage *imgc=iftCreateImage(img->xsize,img->ysize,img->zsize);
    int p;

    iftCopyVoxelSize(img,imgc);

    for (p=0; p < img->n; p++)
        imgc->val[p]=img->val[p];

    if (img->Cb != NULL)
    {
        imgc->Cb = iftAllocUShortArray(img->n);
        imgc->Cr = iftAllocUShortArray(img->n);
        for (p=0; p < img->n; p++)
        {
            imgc->Cb[p]=img->Cb[p];
            imgc->Cr[p]=img->Cr[p];
        }
    }

    return(imgc);
}


iftImage  *iftCreateCuboid(int xsize, int ysize, int zsize)
{
    iftImage *img=iftCreateImage(xsize,ysize,zsize);
    iftVoxel u,uo,uf;
    int p;

    uo.x = (int)(0.1*xsize);
    uo.y = (int)(0.1*ysize);
    uo.z = (int)(0.1*zsize);
    uf.x = (int)(0.9*xsize);
    uf.y = (int)(0.9*ysize);
    uf.z = (int)(0.9*zsize);

    for (u.z=uo.z; u.z <= uf.z; u.z++)
        for (u.y=uo.y; u.y <= uf.y; u.y++)
            for (u.x=uo.x; u.x <= uf.x; u.x++)
            {
                p = iftGetVoxelIndex(img,u);
                img->val[p]=255;
            }

    return(img);

}


char iftAdjacentVoxels(iftImage *img, iftAdjRel *A, iftVoxel u, iftVoxel v)
{
    int i;

    for (i=0; i < A->n; i++)
    {
        if ((A->dx[i]==(v.x-u.x))&&
                (A->dy[i]==(v.y-u.y))&&
                ((A->dz[i]==(v.z-u.z))))
            return 1;
    }

    return 0;
}

iftImage *iftCSVtoImage(char *filename)
{
    char      basename[100],ext[10],*pos,newfilename[150];
    iftImage  *img=NULL;
    int       len,p;
    iftVoxel  um,u;
    FILE     *fp=NULL;

    pos = strrchr(filename,'.') + 1;
    sscanf(pos,"%s",ext);

    if (strcmp(ext,"csv")==0)
    {
        len = strlen(filename);
        strncpy(basename,filename,len-4);
        basename[len-4]='\0';
        sprintf(newfilename,"%s.scn",basename);
        fp = fopen(filename,"r");
        um.x=um.y=um.z=-INFINITY_INT;
        while (!feof(fp))
        {
            if (fscanf(fp,"%d,%d,%d",&u.x,&u.y,&u.z)!=3) iftError("Reading error","iftCSVtoImage");
            if (u.x > um.x) um.x = u.x;
            if (u.y > um.y) um.y = u.y;
            if (u.z > um.z) um.z = u.z;
        }
        fclose(fp);
        img = iftCreateImage(um.x+10,um.y+10,um.z+10);
        fp = fopen(filename,"r");
        while (!feof(fp))
        {
            if(fscanf(fp,"%d,%d,%d",&u.x,&u.y,&u.z)!=3) iftError("Reading error","iftCSVtoImage");;
            u.x=u.x+5;
            u.y=u.y+5;
            u.z=u.z+5;
            p = iftGetVoxelIndex(img,u);
            img->val[p]=255;
        }
        fclose(fp);
        iftWriteImage(img,newfilename);
    }
    else
        iftError(MSG2,"iftCSVtoImage");

    return(img);
}

iftImage *iftAddFrame(iftImage *img, int sz, int value)
{
    iftImage *fimg;
    int p, q;
    iftVoxel u;


    if (iftIs3DImage(img))
    {
        fimg = iftCreateImage(img->xsize+(2*sz),img->ysize+(2*sz), img->zsize+(2*sz));
        iftCopyVoxelSize(img,fimg);
        iftSetImage(fimg,value);

        p = 0;
        for (u.z=sz; u.z < fimg->zsize-sz; u.z++)
            for (u.y=sz; u.y < fimg->ysize-sz; u.y++)
                for (u.x=sz; u.x < fimg->xsize-sz; u.x++)
                {
                    q = iftGetVoxelIndex(fimg,u);
                    fimg->val[q] = img->val[p];
                    p++;
                }
    }
    else
    {
        fimg = iftCreateImage(img->xsize+(2*sz),img->ysize+(2*sz), 1);
        iftSetImage(fimg,value);

        if (iftIsColorImage(img))
        {
            iftSetCbCr(fimg,128);
            p = 0;
            u.z = 0;
            for (u.y=sz; u.y < fimg->ysize-sz; u.y++)
                for (u.x=sz; u.x < fimg->xsize-sz; u.x++)
                {
                    q = iftGetVoxelIndex(fimg,u);
                    fimg->val[q] = img->val[p];
                    fimg->val[q] = img->Cb[p];
                    fimg->val[q] = img->Cr[p];
                    p++;
                }
        }
        else
        {
            p = 0;
            u.z = 0;
            for (u.y=sz; u.y < fimg->ysize-sz; u.y++)
                for (u.x=sz; u.x < fimg->xsize-sz; u.x++)
                {
                    q = iftGetVoxelIndex(fimg,u);
                    fimg->val[q] = img->val[p];
                    p++;
                }
        }
    }

    return(fimg);
}

iftImage *iftRemFrame(iftImage *fimg, int sz)
{
    iftImage *img;
    int p, q;
    iftVoxel u;

    if (iftIs3DImage(fimg))
    {
        img = iftCreateImage(fimg->xsize-(2*sz),fimg->ysize-(2*sz),fimg->zsize-(2*sz));
        iftCopyVoxelSize(fimg,img);

        p = 0;
        for (u.z=sz; u.z < fimg->zsize-sz; u.z++)
            for (u.y=sz; u.y < fimg->ysize-sz; u.y++)
                for (u.x=sz; u.x < fimg->xsize-sz; u.x++)
                {
                    q = iftGetVoxelIndex(fimg,u);
                    img->val[p] = fimg->val[q];
                    p++;
                }
    }
    else
    {
        img = iftCreateImage(fimg->xsize-(2*sz),fimg->ysize-(2*sz),1);

        if(iftIsColorImage(fimg))
        {
            iftSetCbCr(img,128);
            p = 0;
            u.z = 0;
            for (u.y=sz; u.y < fimg->ysize-sz; u.y++)
                for (u.x=sz; u.x < fimg->xsize-sz; u.x++)
                {
                    q = iftGetVoxelIndex(fimg,u);
                    img->val[p] = fimg->val[q];
                    img->Cb[p]  = fimg->Cb[q];
                    img->Cr[p]  = fimg->Cr[q];
                    p++;
                }
        }
        else
        {
            p = 0;
            u.z = 0;
            for (u.y=sz; u.y < fimg->ysize-sz; u.y++)
                for (u.x=sz; u.x < fimg->xsize-sz; u.x++)
                {
                    q = iftGetVoxelIndex(fimg,u);
                    img->val[p] = fimg->val[q];
                    p++;
                }
        }
    }


    return(img);
}

void iftSetImage(iftImage *img, int value)
{
    int p;

    for (p=0; p < img->n; p++)
        img->val[p]=value;

}

iftImage *iftGetXYSlice(iftImage *img, int zcoord)
{
    iftImage *slice;
    iftVoxel  u;
    int       p,q;

    if ( (zcoord < 0) || (zcoord >= img->zsize))
        iftError("Invalid z coordinate","iftGetXYSlice");

    if(iftIsColorImage(img))
        slice = iftCreateColorImage(img->xsize,img->ysize,1);
    else
        slice = iftCreateImage(img->xsize,img->ysize,1);

    u.z   = zcoord;
    q     = 0;
    for (u.y = 0; u.y < img->ysize; u.y++)
        for (u.x = 0; u.x < img->xsize; u.x++)
        {
            p = iftGetVoxelIndex(img,u);
            slice->val[q] = img->val[p];
            if(iftIsColorImage(img))
            {
                slice->Cb[q] = img->Cb[p];
                slice->Cr[q] = img->Cr[p];
            }
            q++;
        }
    iftCopyVoxelSize(img,slice);

    return(slice);
}

void iftPutXYSlice(iftImage *img, iftImage *slice, int zcoord)
{
    iftVoxel  u;
    int       p,q;

    if ( (zcoord < 0) || (zcoord >= img->zsize))
        iftError("Invalid z coordinate","iftPutXYSlice");

    if ( (img->ysize!=slice->ysize)||(img->xsize!=slice->xsize) )
        iftError("Image and slice are incompatibles","iftPutXYSlice");

    u.z   = zcoord;
    p     = 0;
    for (u.y = 0; u.y < img->ysize; u.y++)
        for (u.x = 0; u.x < img->xsize; u.x++)
        {
            q = iftGetVoxelIndex(img,u);
            img->val[q] = slice->val[p];
            if(iftIsColorImage(img))
            {
                img->Cb[q] = slice->Cb[p];
                img->Cr[q] = slice->Cr[p];
            }
            p++;
        }
}

iftImage *iftGetZXSlice(iftImage *img, int ycoord)
{
    iftImage *slice;
    iftVoxel  u;
    int       p,q;

    if ( (ycoord < 0) || (ycoord >= img->ysize))
        iftError("Invalid y coordinate","iftGetZXSlice");

    if(iftIsColorImage(img))
        slice = iftCreateColorImage(img->xsize,img->ysize,1);
    else
        slice = iftCreateImage(img->xsize,img->ysize,1);

    u.y   = ycoord;
    q = 0;
    for (u.x = 0; u.x < img->xsize; u.x++)
        for (u.z = 0; u.z < img->zsize; u.z++)
        {
            p = iftGetVoxelIndex(img,u);
            slice->val[q] = img->val[p];
            if(iftIsColorImage(img))
            {
                slice->Cb[q] = img->Cb[p];
                slice->Cr[q] = img->Cr[p];
            }
            q++;
        }
    slice->dx = img->dz;
    slice->dy = img->dx;
    slice->dz = img->dy;

    return(slice);
}

void iftPutZXSlice(iftImage *img, iftImage *slice, int ycoord)
{
    iftVoxel  u;
    int       p,q;

    if ( (ycoord < 0) || (ycoord >= img->ysize))
        iftError("Invalid y coordinate","iftPutZXSlice");

    if ( (img->xsize!=slice->ysize)||(img->zsize!=slice->xsize) )
        iftError("Image and slice are incompatibles","iftPutZXSlice");

    u.y   = ycoord;
    p     = 0;
    for (u.x = 0; u.x < img->xsize; u.x++)
        for (u.z = 0; u.z < img->zsize; u.z++)
        {
            q = iftGetVoxelIndex(img,u);
            img->val[q] = slice->val[p];
            if(iftIsColorImage(img))
            {
                img->Cb[q] = slice->Cb[p];
                img->Cr[q] = slice->Cr[p];
            }
            p++;
        }
}

iftImage *iftGetYZSlice(iftImage *img, int xcoord)
{
    iftImage *slice;
    iftVoxel  u;
    int       p,q;

    if ( (xcoord < 0) || (xcoord >= img->xsize))
        iftError("Invalid x coordinate","iftGetYZSlice");

    if(iftIsColorImage(img))
        slice = iftCreateColorImage(img->xsize,img->ysize,1);
    else
        slice = iftCreateImage(img->xsize,img->ysize,1);

    u.x   = xcoord;
    q     = 0;
    for (u.z = 0; u.z < img->zsize; u.z++)
        for (u.y = 0; u.y < img->ysize; u.y++)
        {
            p = iftGetVoxelIndex(img,u);
            slice->val[q] = img->val[p];
            if(iftIsColorImage(img))
            {
                slice->Cb[q] = img->Cb[p];
                slice->Cr[q] = img->Cr[p];
            }
            q++;
        }
    slice->dx = img->dy;
    slice->dy = img->dz;
    slice->dz = img->dx;

    return(slice);
}

void iftPutYZSlice(iftImage *img, iftImage *slice, int xcoord)
{
    iftVoxel  u;
    int       p,q;

    if ( (xcoord < 0) || (xcoord >= img->xsize))
        iftError("Invalid x coordinate","iftPutYZSlice");

    if ( (img->zsize!=slice->ysize)||(img->ysize!=slice->xsize) )
        iftError("Image and slice are incompatibles","iftPutYZSlice");

    u.x   = xcoord;
    p     = 0;
    for (u.z = 0; u.z < img->zsize; u.z++)
        for (u.y = 0; u.y < img->ysize; u.y++)
        {
            q = iftGetVoxelIndex(img,u);
            img->val[q] = slice->val[p];
            if(iftIsColorImage(img))
            {
                img->Cb[q] = slice->Cb[p];
                img->Cr[q] = slice->Cr[p];
            }
            p++;
        }
}


iftVoxel iftGeometricCenterVoxel(iftImage *obj)
{
    iftVoxel u;
    iftPoint c;
    int p,n=0;

    c.x = c.y = c.z = 0.0;
    for (u.z=0; u.z < obj->zsize; u.z++)
        for (u.y=0; u.y < obj->ysize; u.y++)
            for (u.x=0; u.x < obj->xsize; u.x++)
            {
                p = iftGetVoxelIndex(obj,u);
                if (obj->val[p]!=0)
                {
                    c.x += u.x;
                    c.y += u.y;
                    c.z += u.z;
                    n++;
                }
            }
    if (n==0)
        iftError("Empty image","iftGeometricCenterVoxel");

    u.x = ROUND(c.x / n);
    u.y = ROUND(c.y / n);
    u.z = ROUND(c.z / n);

    return(u);
}

int iftObjectDiagonal(iftImage *obj)
{
    int p,xsize,ysize,zsize,diag;
    iftVoxel u,min,max;

    min.x = min.y = min.z =  INFINITY_INT;
    max.x = max.y = max.z = -INFINITY_INT;
    for (u.z=0; u.z < obj->zsize; u.z++)
        for (u.y=0; u.y < obj->ysize; u.y++)
            for (u.x=0; u.x < obj->xsize; u.x++)
            {
                p = iftGetVoxelIndex(obj,u);
                if (obj->val[p]!=0)
                {
                    if (u.x < min.x) min.x = u.x;
                    if (u.y < min.y) min.y = u.y;
                    if (u.z < min.z) min.z = u.z;
                    if (u.x > max.x) max.x = u.x;
                    if (u.y > max.y) max.y = u.y;
                    if (u.z > max.z) max.z = u.z;
                }
            }
    xsize = max.x - min.x + 1;
    ysize = max.y - min.y + 1;
    zsize = max.z - min.z + 1;
    diag  = (int)(sqrtf(xsize*xsize + ysize*ysize + zsize*zsize)+0.5);

    return(diag);
}


iftPoint iftGeometricCenter(iftImage *obj)
{
    iftVoxel u;
    iftPoint c;
    int p;
    unsigned long n=0;

    c.x = c.y = c.z = 0.0;
    for (u.z=0; u.z < obj->zsize; u.z++)
        for (u.y=0; u.y < obj->ysize; u.y++)
            for (u.x=0; u.x < obj->xsize; u.x++)
            {
                p = iftGetVoxelIndex(obj,u);
                if (obj->val[p]!=0)
                {
                    c.x += u.x;
                    c.y += u.y;
                    c.z += u.z;
                    n++;
                }
            }
    if (n==0)
        iftError("Empty image","iftGeometricCenter");

    c.x /= n;
    c.y /= n;
    c.z /= n;

    return(c);
}


iftImage *iftCropImage(iftImage *img, iftVoxel uo, iftVoxel uf)
{
    iftVoxel u,v;
    int p, q, xsize, ysize, zsize;
    iftImage *crop;

    xsize = uf.x - uo.x + 1;
    ysize = uf.y - uo.y + 1;
    zsize = uf.z - uo.z + 1;
    if ((xsize > img->xsize)||(ysize > img->ysize)||(zsize > img->zsize))
        iftError("Invalid crop region","iftCropImage");

    crop = iftCreateImage(xsize,ysize,zsize);
    if (img->Cb != NULL)
        iftSetCbCr(crop,128);

    for (u.z=uo.z, v.z=0; u.z <= uf.z; u.z++, v.z++)
        for (u.y=uo.y, v.y=0; u.y <= uf.y; u.y++, v.y++)
            for (u.x=uo.x, v.x=0; u.x <= uf.x; u.x++, v.x++)
            {
                p   = iftGetVoxelIndex(img,u);
                q   = iftGetVoxelIndex(crop,v);
                crop->val[q]=img->val[p];
                if (img->Cb != NULL)
                {
                    crop->Cb[q] = img->Cb[p];
                    crop->Cr[q] = img->Cr[p];
                }
            }

    iftCopyVoxelSize(img,crop);

    return(crop);
}

iftImage *iftImageGradientMagnitude(iftImage *img, iftAdjRel *A)
{
    float   dist,gx,gy,gz, g, gmax;
    float   gxCb , gyCb , gzCb, gxCr , gyCr , gzCr;
    int     i,p,q;
    iftVoxel   u,v;
    float   *mag =iftAllocFloatArray(A->n);
    iftImage  *grad=iftCreateImage(img->xsize,img->ysize,img->zsize);

    iftCopyVoxelSize(img,grad);

    for (i=0; i < A->n; i++)
        mag[i]=sqrtf(A->dx[i]*A->dx[i]+A->dy[i]*A->dy[i]+A->dz[i]*A->dz[i]);

    if ( (img->Cb == NULL) && (img->Cr == NULL) )
    {
        for (u.z=0; u.z < img->zsize; u.z++)
            for (u.y=0; u.y < img->ysize; u.y++)
                for (u.x=0; u.x < img->xsize; u.x++)
                {
                    p = iftGetVoxelIndex(img,u);
                    gx = gy = gz = 0.0;
                    for (i=1; i < A->n; i++)
                    {
                        v.x = u.x + A->dx[i];
                        v.y = u.y + A->dy[i];
                        v.z = u.z + A->dz[i];
                        if (iftValidVoxel(img,v))
                        {
                            q = iftGetVoxelIndex(img,v);
                            dist = img->val[q]-img->val[p];
                            gx  += dist*A->dx[i]/mag[i];
                            gy  += dist*A->dy[i]/mag[i];
                            gz  += dist*A->dz[i]/mag[i];
                        }
                    }
                    grad->val[p]=(int)sqrtf(gx*gx + gy*gy + gz*gz);
                }
    }
    else   // colored image
    {
        for (u.z=0; u.z < img->zsize; u.z++)
            for (u.y=0; u.y < img->ysize; u.y++)
                for (u.x=0; u.x < img->xsize; u.x++)
                {
                    p = iftGetVoxelIndex(img,u);
                    gx = gy = gz = 0.0;
                    gxCb = gyCb = gzCb = 0.0;
                    gxCr = gyCr = gzCr = 0.0;
                    for (i=1; i < A->n; i++)
                    {
                        v.x = u.x + A->dx[i];
                        v.y = u.y + A->dy[i];
                        v.z = u.z + A->dz[i];
                        if (iftValidVoxel(img,v))
                        {
                            q = iftGetVoxelIndex(img,v);
                            dist = img->val[q]-img->val[p];
                            gx  += dist*A->dx[i]/mag[i];
                            gy  += dist*A->dy[i]/mag[i];
                            gz  += dist*A->dz[i]/mag[i];
                            dist = img->Cb[q]-img->Cb[p];
                            gxCb  += dist*A->dx[i]/mag[i];
                            gyCb  += dist*A->dy[i]/mag[i];
                            gzCb  += dist*A->dz[i]/mag[i];
                            dist = img->Cr[q]-img->Cr[p];
                            gxCr  += dist*A->dx[i]/mag[i];
                            gyCr  += dist*A->dy[i]/mag[i];
                            gzCr  += dist*A->dz[i]/mag[i];
                        }
                    }
                    gmax = sqrtf(gx*gx + gy*gy + gz*gz);
                    g    = sqrtf(gxCb*gxCb + gyCb*gyCb + gzCb*gzCb);
                    if (g > gmax)
                        gmax = g;
                    g    = sqrtf(gxCr*gxCr + gyCr*gyCr + gzCr*gzCr);
                    if (g > gmax)
                        gmax = g;
                    grad->val[p] = (int)gmax;
                }
    }

    free(mag);

    return(grad);
}


void iftGetDisplayRange(iftImage *img, int *lower, int *higher)
{
    int p, i, n;
    double *hist;

    iftMinimumValue(img);
    iftMaximumValue(img);
    n=img->maxval-img->minval+1;

    hist = iftAllocDoubleArray(n);

    /* Compute histogram */
    for (p=0; p < img->n; p++)
        hist[img->val[p]+img->minval]++;

    /* Compute normalized and accumulated histogram */

    hist[0] /= img->n;
    for (i=1; i < n; i++)
    {
        hist[i] = hist[i]/img->n + hist[i-1];
    }

    /* Compute lower value */

    for (i=0; i < n; i++)
        if (hist[i] > 0.020)
        {
            *lower = (i-img->minval);
            break;
        }

    for (i=n-1; i >= 0; i--)
        if (hist[i] < 0.998)
        {
            *higher = (i-img->minval);
            break;
        }

    free(hist);

}

iftVoxel iftGetVoxelCoord(iftImage *img, int p)
{
    iftVoxel u;

    u.x = iftGetXCoord(img,p);
    u.y = iftGetYCoord(img,p);
    u.z = iftGetZCoord(img,p);

    return(u);
}


iftImage *iftImageCb(iftImage *img)
{
    iftImage *Cb=iftCreateImage(img->xsize,img->ysize,img->zsize);
    int p;

    if (img->Cb == NULL)
        iftError("There is no color component","iftImageCb");

    for (p=0; p < img->n; p++)
        Cb->val[p] = img->Cb[p];


    iftCopyVoxelSize(img,Cb);

    return(Cb);
}

iftImage *iftImageCr(iftImage *img)
{
    iftImage *Cr=iftCreateImage(img->xsize,img->ysize,img->zsize);
    int p;

    if (img->Cr == NULL)
        iftError("There is no color component","iftImageCb");

    for (p=0; p < img->n; p++)
        Cr->val[p] = img->Cr[p];

    iftCopyVoxelSize(img,Cr);

    return(Cr);
}

iftImage *iftImageRed(iftImage *img)
{
    iftImage *red=iftCreateImage(img->xsize,img->ysize,img->zsize);
    iftColor RGB,YCbCr;
    int p;

    if ((img->Cr == NULL)||(img->Cb == NULL))
        iftError("There are no color components","iftImageRed");

    for (p=0; p < img->n; p++)
    {
        YCbCr.val[0] = img->val[p];
        YCbCr.val[1] = img->Cb[p];
        YCbCr.val[2] = img->Cr[p];
        RGB = iftYCbCrtoRGB(YCbCr);
        red->val[p] = RGB.val[0];
    }

    iftCopyVoxelSize(img,red);

    return(red);
}

iftImage *iftImageGreen(iftImage *img)
{
    iftImage *green=iftCreateImage(img->xsize,img->ysize,img->zsize);
    iftColor RGB,YCbCr;
    int p;

    if ((img->Cr == NULL)||(img->Cb == NULL))
        iftError("There are no color components","iftImageGreen");

    for (p=0; p < img->n; p++)
    {
        YCbCr.val[0] = img->val[p];
        YCbCr.val[1] = img->Cb[p];
        YCbCr.val[2] = img->Cr[p];
        RGB = iftYCbCrtoRGB(YCbCr);
        green->val[p] = RGB.val[1];
    }

    iftCopyVoxelSize(img,green);

    return(green);
}

iftImage *iftImageGray(iftImage *img)
{
    iftImage *gray=iftCreateImage(img->xsize,img->ysize,img->zsize);
    int p;

    if ((img->Cr == NULL)||(img->Cb == NULL))
        iftError("There are no color components","iftImageGray");

    for (p=0; p < img->n; p++)
    {
        gray->val[p] = img->val[p];
    }

    iftCopyVoxelSize(img,gray);

    return(gray);
}

iftImage *iftImageBlue(iftImage *img)
{
    iftImage *blue=iftCreateImage(img->xsize,img->ysize,img->zsize);
    iftColor RGB,YCbCr;
    int p;

    if ((img->Cr == NULL)||(img->Cb == NULL))
        iftError("There are no color components","iftImageBlue");

    for (p=0; p < img->n; p++)
    {
        YCbCr.val[0] = img->val[p];
        YCbCr.val[1] = img->Cb[p];
        YCbCr.val[2] = img->Cr[p];
        RGB = iftYCbCrtoRGB(YCbCr);
        blue->val[p] = RGB.val[2];
    }

    iftCopyVoxelSize(img,blue);

    return(blue);
}

iftImage *iftCreateGaussian(int xsize, int ysize, int zsize, iftVoxel mean, float stdev, int maxval)
{
    iftImage *img   = iftCreateImage(xsize,ysize,zsize);
    iftVoxel u;
    int p;
    float K, variance=stdev*stdev;

    if (variance <= 0.0)
        iftError("Invalid variance","iftCreateGaussian");

    K=2.0*variance;

    for (u.z=0; u.z < img->zsize; u.z++)
        for (u.y=0; u.y < img->ysize; u.y++)
            for (u.x=0; u.x < img->xsize; u.x++)
            {
                p = iftGetVoxelIndex(img,u);
                img->val[p]=(int)(maxval*expf(-(float)iftVoxelSquareDistance(u,mean)/K));
            }

    return(img);
}

iftImage *iftRegionBorders(iftImage *label, int value)
{
    iftImage *borders=iftCreateImage(label->xsize,label->ysize,label->zsize);
    int i,p,q;
    iftVoxel u,v;
    iftAdjRel *A;

    if (label->zsize==1)
        A = iftCircular(1.0);
    else
        A = iftSpheric(1.0);

    for (u.z=0; u.z < label->zsize; u.z++)
        for (u.y=0; u.y < label->ysize; u.y++)
            for (u.x=0; u.x < label->xsize; u.x++)
            {
                p = iftGetVoxelIndex(label,u);
                for (i=1; i < A->n; i++)
                {
                    v.x = u.x + A->dx[i];
                    v.y = u.y + A->dy[i];
                    v.z = u.z + A->dz[i];
                    if (iftValidVoxel(label,v))
                    {
                        q = iftGetVoxelIndex(label,v);
                        if (label->val[q]<label->val[p])
                        {
                            borders->val[p] = value;
                            break;
                        }
                    }
                }
            }
    iftCopyVoxelSize(label,borders);
    iftDestroyAdjRel(&A);

    return(borders);
}

iftImage *iftExtractROI(iftImage *img, iftVoxel uo, iftVoxel uf)
{
    iftVoxel u;
    int p,q;
    iftImage *roi=NULL;

    if (!iftValidVoxel(img,uo))
        iftError("uo is not a valid voxel","iftExtractROI");

    if (!iftValidVoxel(img,uf))
        iftError("uf is not a valid voxel","iftExtractROI");

    roi = iftCreateImage(uf.x-uo.x+1,uf.y-uo.y+1,uf.z-uo.z+1);

    if (img->Cb != 0)
    {
        iftSetCbCr(roi,128);
        q = 0;
        u.z=0;
        for (u.y=uo.y; u.y <= uf.y; u.y++)
            for (u.x=uo.x; u.x <= uf.x; u.x++)
            {
                p = iftGetVoxelIndex(img,u);
                roi->val[q]=img->val[p];
                roi->Cb[q] =img->Cb[p];
                roi->Cr[q] =img->Cr[p];
                q++;
            }
    }
    else
    {
        q=0;
        for (u.z=uo.z; u.z <= uf.z; u.z++)
            for (u.y=uo.y; u.y <= uf.y; u.y++)
                for (u.x=uo.x; u.x <= uf.x; u.x++)
                {
                    p = iftGetVoxelIndex(img,u);
                    roi->val[q]=img->val[p];
                    q++;
                }
    }

    iftCopyVoxelSize(img,roi);

    return(roi);
}

void iftInsertROI(iftImage *roi, iftImage *img, iftVoxel pos)
{
    iftVoxel u, uo = pos, uf;
    int p,q;

    if (!iftValidVoxel(img,pos))
        iftError("pos is not a valid voxel","iftInsertROI");

    uf.x = uo.x + roi->xsize - 1;
    uf.y = uo.y + roi->ysize - 1;
    uf.z = uo.z + roi->zsize - 1;


    if (roi->Cb != 0)
    {
        q = 0;
        u.z=0;
        for (u.y=uo.y; u.y <= uf.y; u.y++)
            for (u.x=uo.x; u.x <= uf.x; u.x++)
            {
                p = iftGetVoxelIndex(img,u);
                img->val[p] = roi->val[q];
                img->Cb[p]  = roi->Cb[q];
                img->Cr[p]  = roi->Cr[q];
                q++;
            }
    }
    else
    {
        q=0;
        for (u.z=uo.z; u.z <= uf.z; u.z++)
            for (u.y=uo.y; u.y <= uf.y; u.y++)
                for (u.x=uo.x; u.x <= uf.x; u.x++)
                {
                    p = iftGetVoxelIndex(img,u);
                    img->val[p] = roi->val[q];
                    q++;
                }
    }

}

float iftObjectVolume(iftImage *label, int obj_code)
{
    int p;
    float volume=0.0, dv = label->dx*label->dy*label->dz;

    for (p=0; p < label->n; p++)
    {
        if (label->val[p]==obj_code)
            volume += dv;
    }

    return(volume);
}

iftImage *iftCreateImageWithGaussianHistogram(int xsize, int ysize, int zsize, float mean, float stdev, int maxval)
{
    iftImage *img=iftCreateImage(xsize,ysize,zsize);
    int       p, i, j;
    float    *gauss=iftAllocFloatArray(maxval+1), area, variance=stdev*stdev;


    if ((mean > maxval)||(mean < 0))
        iftError("The mean value must be positive and lower than the maximum value","iftCreateImageWithGaussianHistogram");

    /* Create a Gaussian histogram with area = number of voxels */

    for (i=0, area=0.0; i <= maxval; i++)
    {
        gauss[i] = exp(-(i-mean)*(i-mean)/(2*variance));
        area    += gauss[i];
    }
    for (i=0; i <= maxval; i++)
    {
        gauss[i] = img->n*gauss[i]/area;
    }

    /* Create image with Gaussian histogram */

    p = 0;
    for (i=0; (i <= maxval)&&(p < img->n); i++)
    {
        for (j=0; (j < gauss[i])&&(p < img->n); j++)
        {
            img->val[p]=i;
            p++;
        }
    }


    free(gauss);
    return(img);
}

iftImage *iftCreateImageWithTwoGaussiansHistogram(int xsize, int ysize, int zsize, float mean1, float stdev1, float mean2, float stdev2, int maxval)
{
    iftImage *img=iftCreateImage(xsize,ysize,zsize);
    int       p, i, j;
    float    *gauss=iftAllocFloatArray(maxval+1), area;
    float     variance1=stdev1*stdev1,variance2=stdev2*stdev2;


    if ((mean1 > maxval)||(mean1 < 0)||(mean2 > maxval)||(mean2 < 0))
        iftError("The mean values must be positive and lower than the maximum value","iftCreateImageWithTwoGaussiansHistogram");

    /* Create a two-Gaussians histogram with area = number of voxels */

    for (i=0, area=0.0; i <= maxval; i++)
    {
        gauss[i] = exp(-(i-mean1)*(i-mean1)/(2*variance1)) + exp(-(i-mean2)*(i-mean2)/(2*variance2));
        area    += gauss[i];
    }
    for (i=0; i <= maxval; i++)
    {
        gauss[i] = img->n*gauss[i]/area;
    }

    /* Create image with Gaussian histogram */

    p = 0;
    for (i=0; (i <= maxval)&&(p < img->n); i++)
    {
        for (j=0; (j < gauss[i])&&(p < img->n); j++)
        {
            img->val[p]=i;
            p++;
        }
    }


    free(gauss);
    return(img);
}



iftImage *iftReadRawSlices(char *basename, int first, int last, int xsize, int ysize, int bits_per_voxel)
{
    FILE     *fp;
    int       p, offset, n = xsize*ysize, i, zsize = last - first + 1;
    iftImage *img;
    char      filename[200];
    uchar    *data8;
    ushort   *data16;
    int    *data32;

    img = iftCreateImage(xsize,ysize,zsize);

    switch(bits_per_voxel)
    {

    case 8:
        data8 = iftAllocUCharArray(n);
        for (i=first; i <= last; i++)
        {
            offset = n*(i-first);
            sprintf(filename,"%s%03d.raw",basename,i);
            fp  = fopen(filename,"rb");
            if(fread(data8,sizeof(uchar),n,fp)!=n)
                iftError("Reading error","iftReadRawSlices");
            for (p=0; p < n; p++)
            {
                img->val[p+offset] = data8[p];
            }
            fclose(fp);
        }
        free(data8);
        break;

    case 16:
        data16 = iftAllocUShortArray(n);
        for (i=first; i <= last; i++)
        {
            offset = n*(i-first);
            sprintf(filename,"%s%03d.raw",basename,i);
            fp  = fopen(filename,"rb");
            if(fread(data16,sizeof(ushort),n,fp)!=n)
                iftError("Reading error","iftReadRawSlices");

            for (p=0; p < n; p++)
            {
                img->val[p+offset] = data16[p];

            }

            fclose(fp);
        }
        free(data16);
        break;

    case 32:
        data32 = iftAllocIntArray(n);
        for (i=first; i <= last; i++)
        {
            offset = n*(i-first);
            sprintf(filename,"%s%03d.raw",basename,i);
            fp  = fopen(filename,"rb");
            if(fread(data32,sizeof(int),n,fp)!=n)
                iftError("Reading error","iftReadRawSlices");
            for (p=0; p < n; p++)
            {
                img->val[p+offset] = data32[p];
            }
            fclose(fp);
        }
        free(data32);
        break;

    default:
        iftError("Invalid number of bits per voxel","iftReadRawSlices");
    }


    return(img);
}

void iftWriteRawSlices(iftImage *img, char *basename)
{
    FILE      *fp;
    int        p, offset, n = img->xsize*img->ysize, i;
    char       filename[200];

    iftMaximumValue(img);

    if (img->maxval <= 255)
    {
        uchar *data8 = iftAllocUCharArray(n);
        for (i=0; i < img->zsize; i++)
        {
            offset = n*i;
            for (p=0; p < n; p++)
                data8[p] = (uchar) img->val[p+offset];
            sprintf(filename,"%s%03d.raw",basename,i);
            fp  = fopen(filename,"wb");
            fwrite(data8,n,sizeof(uchar),fp);
            fclose(fp);
        }
        free(data8);
    }
    else
    {
        if (img->maxval <= 4095)
        {
            ushort    *data16=iftAllocUShortArray(n);
            for (i=0; i < img->zsize; i++)
            {
                offset = n*i;
                for (p=0; p < n; p++)
                    data16[p] = (ushort) img->val[p+offset];
                sprintf(filename,"%s%03d.raw",basename,i);
                fp  = fopen(filename,"wb");
                fwrite(data16,n,sizeof(ushort),fp);
                fclose(fp);
            }
            free(data16);
        }
        else
        {
            int    *data32=iftAllocIntArray(n);
            for (i=0; i < img->zsize; i++)
            {
                offset = n*i;
                for (p=0; p < n; p++)
                    data32[p] = (int) img->val[p+offset];
                sprintf(filename,"%s%03d.raw",basename,i);
                fp  = fopen(filename,"wb");
                fwrite(data32,n,sizeof(int),fp);
                fclose(fp);
            }
            free(data32);
        }
    }
}

iftImage *iftExtractGreyObject(iftImage *image)
{
    int p,q;
    iftVoxel uo,uf,u;
    iftImage *result=NULL;

    uo.x = uo.y = uo.z = INFINITY_INT;
    uf.x = uf.y = uf.z = -INFINITY_INT;
    for (u.z=0; u.z < image->zsize; u.z++)
        for (u.y=0; u.y < image->ysize; u.y++)
            for (u.x=0; u.x < image->xsize; u.x++)
            {
                p = iftGetVoxelIndex(image,u);
                if (image->val[p] > 0)
                {
                    if (u.x < uo.x) uo.x = u.x;
                    if (u.y < uo.y) uo.y = u.y;
                    if (u.z < uo.z) uo.z = u.z;
                    if (u.x > uf.x) uf.x = u.x;
                    if (u.y > uf.y) uf.y = u.y;
                    if (u.z > uf.z) uf.z = u.z;
                }
            }

    result = iftCreateImage(uf.x-uo.x+1,uf.y-uo.y+1,uf.z-uo.z+1);

    q = 0;
    for (u.z=uo.z; u.z <= uf.z; u.z++)
        for (u.y=uo.y; u.y <= uf.y; u.y++)
            for (u.x=uo.x; u.x <= uf.x; u.x++)
            {
                p = iftGetVoxelIndex(image,u);
                result->val[q]=image->val[p];
                q++;
            }

    iftCopyVoxelSize(image,result);

    return(result);
}

iftImage *iftExtractGreyObjectPos(iftImage *image, iftVoxel *pos)
{
    int p,q;
    iftVoxel uo,uf,u;
    iftImage *result=NULL;

    uo.x = uo.y = uo.z = INFINITY_INT;
    uf.x = uf.y = uf.z = -INFINITY_INT;
    for (u.z=0; u.z < image->zsize; u.z++)
        for (u.y=0; u.y < image->ysize; u.y++)
            for (u.x=0; u.x < image->xsize; u.x++)
            {
                p = iftGetVoxelIndex(image,u);
                if (image->val[p] > 0)
                {
                    if (u.x < uo.x) uo.x = u.x;
                    if (u.y < uo.y) uo.y = u.y;
                    if (u.z < uo.z) uo.z = u.z;
                    if (u.x > uf.x) uf.x = u.x;
                    if (u.y > uf.y) uf.y = u.y;
                    if (u.z > uf.z) uf.z = u.z;
                }
            }

    result = iftCreateImage(uf.x-uo.x+1,uf.y-uo.y+1,uf.z-uo.z+1);

    q = 0;
    for (u.z=uo.z; u.z <= uf.z; u.z++)
        for (u.y=uo.y; u.y <= uf.y; u.y++)
            for (u.x=uo.x; u.x <= uf.x; u.x++)
            {
                p = iftGetVoxelIndex(image,u);
                result->val[q]=image->val[p];
                q++;
            }

    iftCopyVoxelSize(image,result);
    *pos = uo;

    return(result);
}

iftImage  *iftImageDomes(iftImage *img, iftAdjRel *A)
{
    iftImage   *domes=iftCreateImage(img->xsize,img->ysize,img->zsize);
    int         p,q,i,Imax;
    iftVoxel    u,v;

    /* Compute domes image in the spatial domain */

    Imax = -INFINITY_INT;

    if (img->Cb != NULL)   // Color images
    {

        float       dist;
        for (p=0; p < img->n; p++)
        {
            u   = iftGetVoxelCoord(img,p);
            for (i=1; i < A->n; i++)
            {
                v = iftGetAdjacentVoxel(A,u,i);
                if (iftValidVoxel(img,v))
                {
                    q = iftGetVoxelIndex(img,v);
                    dist =  0.2*(img->val[q]-img->val[p])*(img->val[q]-img->val[p])
                            + 1.0*(img->Cb[q]-img->Cb[p])*(img->Cb[q]-img->Cb[p])
                            + 1.0*(img->Cr[q]-img->Cr[p])*(img->Cr[q]-img->Cr[p]);
                    dist = sqrtf(dist);
                    domes->val[p] += ROUND(dist);
                }
            }
            if (domes->val[p] > Imax)
                Imax = domes->val[p];
        }

    }
    else   // Gray images
    {

        for (p=0; p < img->n; p++)
        {
            u   = iftGetVoxelCoord(img,p);
            for (i=1; i < A->n; i++)
            {
                v = iftGetAdjacentVoxel(A,u,i);
                if (iftValidVoxel(img,v))
                {
                    q = iftGetVoxelIndex(img,v);
                    domes->val[p] += abs(img->val[q]-img->val[p]);
                }
            }
            if (domes->val[p] > Imax)
                Imax = domes->val[p];
        }

    }

    // Normalize image

    if (Imax > 0)
    {
        for (p=0; p < img->n; p++)
        {
            domes->val[p] = ROUND(((MAXWEIGHT-1.0)/Imax * (Imax-domes->val[p])) + 1.0);
        }
    }

    iftCopyVoxelSize(img,domes);

    return(domes);
}

iftImage  *iftImageBasins(iftImage *img, iftAdjRel *A)
{
    iftImage   *basins=iftCreateImage(img->xsize,img->ysize,img->zsize);
    int         Imax;

    /* Compute basins image in the spatial domain */

    Imax = -INFINITY_INT;

    if (iftIsColorImage(img))   // Color images
    {

#pragma omp parallel for shared(img,basins,Imax,A)
        for (int p=0; p < img->n; p++)
        {
            iftVoxel u   = iftGetVoxelCoord(img,p);
            for (int i=1; i < A->n; i++)
            {
                iftVoxel v = iftGetAdjacentVoxel(A,u,i);
                if (iftValidVoxel(img,v))
                {
                    int      q = iftGetVoxelIndex(img,v);
                    float dist =  0.2*(img->val[q]-img->val[p])*(img->val[q]-img->val[p])
                                  + 1.0*(img->Cb[q]-img->Cb[p])*(img->Cb[q]-img->Cb[p])
                                  + 1.0*(img->Cr[q]-img->Cr[p])*(img->Cr[q]-img->Cr[p]);
                    dist = sqrtf(dist);
                    basins->val[p] += ROUND(dist);
                }
            }
#pragma omp critical
            if (basins->val[p] > Imax)
                Imax = basins->val[p];
        }

    }
    else   // Gray images
    {

#pragma omp parallel for shared(img,basins,Imax,A)
        for (int p=0; p < img->n; p++)
        {
            iftVoxel u   = iftGetVoxelCoord(img,p);
            for (int i=1; i < A->n; i++)
            {
                iftVoxel v = iftGetAdjacentVoxel(A,u,i);
                if (iftValidVoxel(img,v))
                {
                    int q = iftGetVoxelIndex(img,v);
                    basins->val[p] += abs(img->val[q]-img->val[p]);
                }
            }
#pragma omp critical
            if (basins->val[p] > Imax)
                Imax = basins->val[p];
        }

    }

    // Normalize image

    if (Imax > 0)
    {
        for (int p=0; p < img->n; p++)
        {
            basins->val[p] = ROUND(((MAXWEIGHT-1.0)/Imax * basins->val[p]) + 1.0);
        }
    }

    iftCopyVoxelSize(img,basins);

    return(basins);
}

iftImage  *iftCrop2DImageByBorder(iftImage *img, int border)
{
    iftImage *imgc=iftCreateImage(img->xsize - 2*border,img->ysize - 2*border,img->zsize);
    int pcrop;
    int x1,y1,z1,x2,y2,z2;

    x1 = border;
    y1 = border;
    z1 = 0;
    x2 = img->xsize - border;
    y2 = img->ysize - border;
    z2 = 1;

    iftCopyVoxelSize(img,imgc);
    pcrop = 0;
    for (int y = y1; y < y2; y++)
        for (int x = x1; x < x2; x++)
            for (int z = z1; z < z2 ; z++)
            {
                int p = x + img->tby[y] + img->tbz[z];
                imgc->val[pcrop]=img->val[p];
                pcrop++;
            }

    if (img->Cb != NULL)
    {
        imgc->Cb = iftAllocUShortArray(imgc->n);
        imgc->Cr = iftAllocUShortArray(imgc->n);
        pcrop = 0;
        for (int y = y1; y < y2; y++)
            for (int x = x1; x < x2; x++)
                for (int z = z1; z < z2 ; z++)
                {
                    int p = x + img->tby[y] + img->tbz[z];
                    imgc->Cb[pcrop]=img->Cb[p];
                    imgc->Cr[pcrop]=img->Cr[p];
                    pcrop++;
                }
    }
    return(imgc);
}


/** By TVS from CAVOS **/
int iftMaximumValueOnMask(iftImage *img, iftImage *mask)
{
    int p;

    img->maxval = -INFINITY_INT;
    for (p=0; p < img->n; p++)
        if (img->maxval < img->val[p] && mask->val[p] > 0)
            img->maxval = img->val[p];

    return(img->maxval);
}

int iftMinimumValueOnMask(iftImage *img, iftImage *mask)
{
    int p;

    img->minval = INFINITY_INT;
    for (p=0; p < img->n; p++)
        if (img->minval > img->val[p] && mask->val[p] > 0)
            img->minval = img->val[p];

    return(img->minval);
}


