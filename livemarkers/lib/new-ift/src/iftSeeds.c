#include "iftSeeds.h"

iftSet *iftObjectBorderSet(iftImage *bin, iftAdjRel *A)
{
  iftSet *border=NULL;
  int i,p,q;
  iftVoxel u,v;

  for (u.z=0; u.z < bin->zsize; u.z++)
    for (u.y=0; u.y < bin->ysize; u.y++)
      for (u.x=0; u.x < bin->xsize; u.x++){
	p = iftGetVoxelIndex(bin,u);
	if (bin->val[p]!=0){
	  for (i=1; i < A->n; i++) {
	    v.x = u.x + A->dx[i];
	    v.y = u.y + A->dy[i];
	    v.z = u.z + A->dz[i];
	    if (iftValidVoxel(bin,v)){
	      q = iftGetVoxelIndex(bin,v);
	      if (bin->val[q]==0){
		iftInsertSet(&border,p);
		break;
	      }
	    }else{
	      iftInsertSet(&border,p);
	      break;
	    }
	  }
	}
      }

  return(border);
}


iftImage *iftObjectBorders(iftImage *bin, iftAdjRel *A)
{
  iftImage *border=iftCreateImage(bin->xsize,bin->ysize,bin->zsize);
  int i,p,q;
  iftVoxel u,v;

  for (u.z=0; u.z < bin->zsize; u.z++)
    for (u.y=0; u.y < bin->ysize; u.y++)
      for (u.x=0; u.x < bin->xsize; u.x++){
	p = iftGetVoxelIndex(bin,u);
	if (bin->val[p]!=0){
	  for (i=1; i < A->n; i++) {
	    v.x = u.x + A->dx[i];
	    v.y = u.y + A->dy[i];
	    v.z = u.z + A->dz[i];
	    if (iftValidVoxel(bin,v)){
	      q = iftGetVoxelIndex(bin,v);
	      if (bin->val[q]==0){
		border->val[p] = 255;
		break;
	      }
	    }else{
	      border->val[p] = 255;
	      break;
	    }
	  }
	}
      }
  iftCopyVoxelSize(bin,border);

  return(border);
}

iftLabeledSet *iftLabelObjBorderSet(iftImage *bin, iftAdjRel *A)
{
  iftLabeledSet *seed=NULL;
  iftImage *label;
  int p;
   
  label = iftLabelObjBorders(bin,A);
  for (p=0; p < label->n; p++) {
    if (label->val[p]!=0){
      iftInsertLabeledSet(&seed,p,label->val[p]);
    }
  }
  iftDestroyImage(&label);

  return(seed);
}

iftImage  *iftLabelObjBorders(iftImage *bin, iftAdjRel *A)
{
  iftImage *borders,*label;
  iftAdjRel *B;

  if (bin->zsize == 1) // 2D
    B = iftCircular(1.0);
  else // 3D
    B = iftSpheric(1.0);
  
  borders = iftObjectBorders(bin,B);
  label   = iftFastLabelComp(borders,A);
  iftDestroyImage(&borders);
  iftDestroyAdjRel(&B);

  iftCopyVoxelSize(bin,label);

  return(label);
}


int iftRootVoxel(iftImage *pred, int p)
{
  if (pred->val[p]==p)
    return(p);
  else
    return(pred->val[p] = iftRootVoxel(pred,pred->val[p])); /* path
							       compression */
}

iftImage  *iftFastLabelComp(iftImage *bin, iftAdjRel *A)
{
  iftImage *label=NULL, *pred=NULL;
  int l = 1;

  pred  = iftCreateImage(bin->xsize,bin->ysize,bin->zsize);

#pragma omp parallel for shared(pred)
  for (int p=0;  p < pred->n; p++)
    pred->val[p]=p;


  for (int p=0;  p < bin->n; p++) {
    if (bin->val[p]!=0){
      int rp = iftRootVoxel(pred,p);
      iftVoxel u = iftGetVoxelCoord(bin,p);
      for (int i=1; i < A->n; i++){
	iftVoxel v = iftGetAdjacentVoxel(A,u,i);
	if (iftValidVoxel(bin,v)){
	  int q = iftGetVoxelIndex(bin,v);
	  if (bin->val[q]!=0){
	    int rq = iftRootVoxel(pred,q);
	    if (rq != rp){
	      if (rp < rq)
		pred->val[rq] = rp;
	      else
		pred->val[rp] = rq;
	    }
	  }
	}
      }
    }
  }

  label = iftCreateImage(bin->xsize,bin->ysize,bin->zsize);

  for (int p=0;  p < pred->n; p++) {
    if (bin->val[p]!=0){
      if (pred->val[p]==p){
	label->val[p]=l; l++;
      }else{
	if (pred->val[pred->val[p]]!=pred->val[p])
	  pred->val[p]=iftRootVoxel(pred,p);
      }
    }
  }

#pragma omp parallel for shared(pred,bin,label)
  for (int p=0;  p < pred->n; p++)
    if ((bin->val[p]!=0)&&(pred->val[p]!=p)){
      label->val[p]=label->val[pred->val[p]];
    }
  
  iftCopyVoxelSize(bin,label);
  iftDestroyImage(&pred);

  return(label);
}


/* Gt image needs to be: 0, 1, 2, ..., N */
iftLabeledSet *iftBorderMarkersForPixelSegmentation(iftImage *grad_image, iftImage *gt_image, float border_distance)
{
  iftAdjRel *A1,*A2;
  iftSet    *B=NULL;
  iftImage  *dist=NULL,*root=NULL,*bin=NULL;
  iftGQueue *Q=NULL;
  int        i,p,q,tmp;
  iftVoxel   u,v,r;
  float      dist_thres=border_distance*border_distance;
  iftLabeledSet *S=NULL;

  /* Initialization */
  
  if (iftIs3DImage(grad_image)){
    A1 = iftSpheric(1.0);
    A2 = iftSpheric(sqrtf(3.0));
  }else
    {
      A1 = iftCircular(1.0);
      A2 = iftCircular(sqrtf(2.0));
    }
  
  B = iftObjectBorderSet(gt_image, A1);

  dist  = iftCreateImage(gt_image->xsize,gt_image->ysize,gt_image->zsize);
  iftSetImage(dist,INFINITY_INT);
  root  = iftCreateImage(gt_image->xsize,gt_image->ysize,gt_image->zsize);
  bin   = iftCreateImage(gt_image->xsize,gt_image->ysize,gt_image->zsize);
  Q     = iftCreateGQueue(QSIZE,gt_image->n,dist->val);

  while (B != NULL) {
    p = iftRemoveSet(&B);
    dist->val[p]=0;
    root->val[p]=p;
    iftInsertGQueue(&Q,p);
  }

  /* Image Foresting Transform: Dilation of the border upto dist_thres */
  
  while(!iftEmptyGQueue(Q)) {
    p=iftRemoveGQueue(Q);

    if (dist->val[p] > dist_thres) 
      break;
    else{
      bin->val[p]=1;
    }
    
    u = iftGetVoxelCoord(bin,p);
    r = iftGetVoxelCoord(bin,root->val[p]);

    for (i=1; i < A2->n; i++){
      v = iftGetAdjacentVoxel(A2,u,i);
      if (iftValidVoxel(bin,v)){
	q = iftGetVoxelIndex(bin,v);
	if (dist->val[q] > dist->val[p]){
	  tmp = (v.x-r.x)*(v.x-r.x) + (v.y-r.y)*(v.y-r.y) + (v.z-r.z)*(v.z-r.z);
	  if (tmp < dist->val[q]){
	    if (dist->val[q]!=INFINITY_INT)
	      iftRemoveGQueueElem(Q, q);
	    dist->val[q]  = tmp;
	    root->val[q]  = root->val[p];
	    iftInsertGQueue(&Q, q);
	  }
	}
      }
    }
  }
  
  iftDestroyGQueue(&Q);
  iftDestroyImage(&dist);
  iftDestroyAdjRel(&A2);

  /* Compute markers in the decreasing order of gradient values (due to
     the LIFO nature of iftLabeledSet) */

  Q     = iftCreateGQueue(QSIZE,gt_image->n,grad_image->val);
  iftSetRemovalPolicy(Q,MAXVALUE);

  B = iftObjectBorderSet(bin, A1);
  iftDestroyImage(&bin);
  iftDestroyAdjRel(&A1);
  
  while (B != NULL) {
    p = iftRemoveSet(&B);
    grad_image->val[p]=grad_image->val[root->val[p]];
    iftInsertGQueue(&Q,p);
  }

  iftDestroyImage(&root);

  while(!iftEmptyGQueue(Q)) {
    p=iftRemoveGQueue(Q);
    //XXX
    //if (gt_image->val[p]==0)
      //iftInsertLabeledSet(&S,p,1);
    //else
      //iftInsertLabeledSet(&S,p,2);
    iftInsertLabeledSet(&S,p,gt_image->val[p]);
  }
  
  iftDestroyGQueue(&Q);

  return(S);
}

/* Gt image needs to be: 0, 1, 2, ..., N */
iftLabeledSet *iftGeodesicMarkersForSegmentation(iftImage *gt_image, iftImage *classification_image){
	iftImage* error_image = iftCreateImage(gt_image->xsize,gt_image->ysize,gt_image->zsize);
  iftAdjRel *adj_relabeling = NULL;
	int p;
	if(classification_image == NULL){
		for(p = 0; p < gt_image->n; p++){
      //XXX
			//if(gt_image->val[p] == 0)
				//error_image->val[p] = 1;
			//else
				//error_image->val[p] = 2;
      error_image->val[p] = gt_image->val[p]+1;
      /* 
      The function iftGeodesicCenters does not computes the geodesic center
      of label 0. The error image is created with the labels from the gt+1. 
      And this label is subtracted before returing the labeledSet.
      c->label = error_image->val[c->elem]-1;
      */
		}
	}else{
		for(p = 0; p < gt_image->n; p++){
      //XXX
			//if((gt_image->val[p] == 0) && (classification_image->val[p] == 2))
				//error_image->val[p] = 1;
			//else if ( (gt_image->val[p] != 0) && (classification_image->val[p] == 1))
				//error_image->val[p] = 2;
      if (gt_image->val[p] != classification_image->val[p])
        error_image->val[p] = gt_image->val[p]+1;
		}
	}

  if (iftIs3DImage(gt_image))
    adj_relabeling = iftSpheric(1.0);
  else
    adj_relabeling = iftCircular(1.0);

	iftImage *relabelled = iftRelabelRegions(error_image, adj_relabeling);
	iftDestroyAdjRel(&adj_relabeling);

	iftLabeledSet *geo_centers = iftGeodesicCenters(relabelled);
	iftLabeledSet *c = geo_centers;
	//Redefines the labels, since the image was rellabeled
	while(c != NULL){
    //Subtracts one, since the labels from the gt were added by one.
    //error_image->val[p] = gt_image->val[p]+1;
		c->label = error_image->val[c->elem]-1;
		c = c->next;
	}

	iftDestroyImage(&relabelled);
	iftDestroyImage(&error_image);

	return geo_centers;
}


iftLabeledSet* iftGetSeeds(iftLabeledSet* S, int nelem, int label){
	iftLabeledSet* seeds = NULL;

	while(S != NULL && nelem > 0){
		if(S->label == label){
			iftInsertLabeledSet(&seeds,S->elem,S->label);
			nelem--;
		}

		S = S->next;
	}

	return seeds;
}

/* Gt image needs to be: 0, 1, 2, ..., N */
iftLabeledSet* iftGetMisclassifiedSeeds(iftLabeledSet* S, int nelem, int label, iftImage* gt_image, iftImage* cl_image){
	iftLabeledSet* seeds = NULL;

	while(S != NULL && nelem > 0){
		if(S->label == label){
			int p = S->elem;
			//If it is misclassified
      //XXX
			//if( (cl_image == NULL) || ((gt_image->val[p] == 0) && (cl_image->val[p] == 2) )
			//		|| ( (gt_image->val[p] != 0) && (cl_image->val[p] == 1) )){
      if( (cl_image == NULL) || (gt_image->val[p] != cl_image->val[p]) ){
				iftInsertLabeledSet(&seeds,S->elem,S->label);
				nelem--;
			}
		}

		S = S->next;
	}

	return seeds;
}

int iftCheckNewSeeds(int *nelem, int length)
{
  for (int i=0; i<length; i++)
    if (nelem[i] > 0) 
      return 1;
  return 0;
}
// Binary Segmentation only. As of now, the iftBMap is redundant, but kept for future works.
int iftMarkersFromMisclassifiedSeeds(iftImage* seed_image, iftLabeledSet* all_seeds, iftBMap* used_seeds, int nseeds, int number_of_objects, iftImage* gt_image, iftImage* cl_image, int dist_border, int max_marker_radius, int min_marker_radius){
	iftLabeledSet *S = all_seeds;

	//int nelem_1 = nseeds/2;
	//int nelem_2 = nelem_1;
  int i=0;
  int nseeds_per_object = (int) nseeds/number_of_objects;
  int *nelem = iftAllocIntArray(number_of_objects);
  for(i=0; i< number_of_objects; i++)
    nelem[i] = nseeds_per_object;

	int total_seeds = 0;

  iftAdjRel *distance_border;
  if(iftIs3DImage(gt_image))
    distance_border = iftSpheric((float)(dist_border + max_marker_radius));
  else
	  distance_border = iftCircular((float)(dist_border + max_marker_radius));

	//while((S != NULL) && ( (nelem_1 > 0) || (nelem_2 > 0) ) ){
  while((S != NULL) && iftCheckNewSeeds(nelem, number_of_objects) ){
		int p = S->elem;


    //XXX
		//int misclassified = ( (cl_image == NULL) || ((gt_image->val[p] == 0) && (cl_image->val[p] == 2) )
							//|| ( (gt_image->val[p] != 0) && (cl_image->val[p] == 1) ));
    int misclassified = ( (cl_image == NULL) || (gt_image->val[p] != cl_image->val[p]));


    //XXX
		//int needs_seed = ( (( S->label == 1 ) && (nelem_1 > 0 )) || (( S->label == 2 ) && ( nelem_2 > 0)));
    int needs_seed = ( nelem[S->label] > 0 );  //If this object still need seed

		if ( (seed_image->val[p] == -1 ) && misclassified && needs_seed){
    //if ( (iftBMapValue(used_seeds, p)) && misclassified && needs_seed){
			int closest_distance = INFINITY_INT;

			int i;
			iftVoxel u = iftGetVoxelCoord(gt_image,p);
			for(i = 1; i < distance_border->n; i++){
				iftVoxel v = iftGetAdjacentVoxel(distance_border,u,i);
				if(iftValidVoxel(gt_image,v)){
					int q = iftGetVoxelIndex(gt_image,v);

					if((gt_image->val[p] != gt_image->val[q]) && (closest_distance > iftVoxelDistance(u,v))){
						closest_distance = floorf(iftVoxelDistance(u,v));
					}
				}
			}

			closest_distance = MIN(closest_distance - dist_border, max_marker_radius) ;

			if(closest_distance >= min_marker_radius){
        iftAdjRel *marker_size;
        if(iftIs3DImage(gt_image))
          marker_size = iftSpheric(closest_distance);
        else
				  marker_size = iftCircular(closest_distance);

				for(i = 0; i < marker_size->n; i++){
					iftVoxel v = iftGetAdjacentVoxel(marker_size,u,i);

					if(iftValidVoxel(gt_image,v)){
						int q = iftGetVoxelIndex(gt_image,v);

            //XXX
						//if(gt_image->val[q] == 0)
							//seed_image->val[q] = 1;
						//else
							//seed_image->val[q] = 2;
            seed_image->val[q] = gt_image->val[q];
					}
				}

				iftDestroyAdjRel(&marker_size);

        //XXX
				//if((S->label == 1))
					//nelem_1--;
				//else
					//nelem_2--;
        nelem[S->label]--;
				total_seeds++;

				iftBMapSet1(used_seeds,p);
			}
		}

		S = S->next;
	}

	iftDestroyAdjRel(&distance_border);
  free(nelem);
	return total_seeds;

}

