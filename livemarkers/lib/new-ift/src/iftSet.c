#include "iftSet.h"

void iftInsertSet(iftSet **S, int elem)
{
  iftSet *p=NULL;

  p = (iftSet *) calloc(1,sizeof(iftSet));
  if (p == NULL) iftError(MSG1,"iftInsertSet");
  if (*S == NULL){
    p->elem  = elem;
    p->next  = NULL;
  }else{
    p->elem  = elem;
    p->next  = *S;
  }
  *S = p;
}

int iftRemoveSet(iftSet **S)
{
  iftSet *p;
  int elem = NIL;

  if (*S != NULL){
    p    =  *S;
    elem = p->elem;
    *S   = p->next;
    free(p);
  }

  return(elem);
}


void iftRemoveSetElem(iftSet **S, int elem){
  iftSet *tmp = NULL, *remove;

  tmp = *S;
  if ( tmp->elem == elem ) {
    *S = tmp->next;
    free( tmp );
  }
  else {
    while( tmp->next->elem != elem ) tmp = tmp->next;
    remove = tmp->next;
    tmp->next = remove->next;
    free( remove );
  }
}


void iftDestroySet(iftSet **S)
{
  iftSet *p;
  while(*S != NULL){
    p = *S;
    *S = p->next;
    free(p);
  }
}

// Could be made efficient by sorting both sets and merging (Theta(n + mlg(m))).
// This implementation is Theta(mn)
iftSet* 	iftSetUnion(iftSet *S1,iftSet *S2){
	iftSet *S = 0;

	iftSet *s = S1;
	while(s){
		iftInsertSet(&S,s->elem);
		s = s->next;
	}

	s = S2;
	while(s){
		iftUnionSetElem(&S,s->elem);
		s = s->next;
	}

	return S;
}

iftSet* iftSetCopy(iftSet* S){
	return iftSetUnion(S,0);
}

//Doesn't check for duplicates (faster than iftSetUnion)
iftSet* 	iftSetConcat(iftSet *S1,iftSet *S2){
	iftSet *S = 0;

	iftSet *s = S1;
	while(s){
		iftInsertSet(&S,s->elem);
		s = s->next;
	}

	s = S2;
	while(s){
		iftInsertSet(&S,s->elem);
		s = s->next;
	}

	return S;
}


char iftUnionSetElem(iftSet **S, int elem)
{
  iftSet *aux=*S;

  while (aux != NULL) {
    if (aux->elem == elem)
      return(0);
    aux = aux->next;
 }
  iftInsertSet(S,elem);
  return(1);
}

void iftInvertSet(iftSet **S)
{
  iftSet *I=NULL;

  while (*S != NULL)
    iftInsertSet(&I,iftRemoveSet(S));
  *S = I;
}

int iftSetHasElement(iftSet *S, int elem){
	iftSet *s = S;
	while(s){
		if(s->elem == elem)
			return 1;

		s = s->next;
	}

	return 0;
}

int 	iftSetSize(iftSet* S){
	iftSet *s = S;

	int i = 0;
	while(s != NULL){
		i++;
		s = s->next;
	}

	return i;

}



/** By TVS, from CAVOS **/

// Uses recursion to copy the set in order
iftSet* iftCopyOrderedSet(iftSet *S)
{
	iftSet *newS = NULL, *part_cpy = NULL;

	// For the empty set, return NULL
	if(S == NULL)
		return NULL;

	// Recrusively copy the n-1 elements of the set
	// excluding the current one
	part_cpy = iftCopyOrderedSet(S->next);

	// Copy the current element to the new set in the
	// appropriate position, given that newS
	// for now is NULL and S->elem will be inserted
	// at the beginning of the new set. Since
	// iftInsertSet always inserts at the beginning
	// we are ensuring that each element will be copied
	// backwards
	iftInsertSet(&newS, S->elem);

	// Attached the copied set after the newly added
	// element of newS and return it
	newS->next = part_cpy;

	return newS;
}
