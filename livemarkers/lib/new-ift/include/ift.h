#ifndef IFT_H_
#define IFT_H_

#include "iftCommon.h"
#include "iftImage.h"
#include "iftAdjacency.h"
#include "iftSet.h"
#include "iftLabeledSet.h"
#include "iftGQueue.h"
#include "iftLIFO.h"
#include "iftFIFO.h"
#include "iftBMap.h"
#include "iftRegion.h"
#include "iftSeeds.h"

#endif
