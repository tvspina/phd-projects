#ifndef IFTREGIONGRAPH_H_
#define IFTREGIONGRAPH_H_

#include "iftImage.h"
#include "iftCommon.h"
#include "iftImage.h"
#include "iftAdjacency.h"
#include "iftLabeledSet.h"
#include "iftSet.h"
#include "iftSort.h"

iftImage* iftRelabelRegions(iftImage* labelled, iftAdjRel* adj_rel);
iftLabeledSet* iftGeodesicCenters(iftImage* label_image);
iftLabeledSet* iftGeometricCenters(iftImage* label_image);


#endif /* iftregiongraph_h_ */
