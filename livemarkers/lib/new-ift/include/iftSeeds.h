#ifndef IFT_SEEDS_H_
#define IFT_SEEDS_H_

#include "iftCommon.h"
#include "iftImage.h"
#include "iftAdjacency.h"
#include "iftSet.h"
#include "iftLabeledSet.h"
#include "iftGQueue.h"
#include "iftLIFO.h"
#include "iftFIFO.h"
#include "iftBMap.h"
#include "iftRegion.h"

iftLabeledSet *iftLabelObjBorderSet(iftImage *bin, iftAdjRel *A);

iftImage  *iftFastLabelComp(iftImage *bin, iftAdjRel *A);

iftImage      *iftObjectBorders(iftImage *bin, iftAdjRel *A);
iftImage      *iftLabelObjBorders(iftImage *bin, iftAdjRel *A);

iftLabeledSet *iftBorderMarkersForPixelSegmentation(iftImage *grad_image, iftImage *gt_image, float border_distance);
iftLabeledSet *iftGeodesicMarkersForSegmentation(iftImage *gt_image, iftImage *classification_image);


int iftMarkersFromMisclassifiedSeeds(iftImage* seed_image, iftLabeledSet* all_seeds, iftBMap* used_seeds, int nseeds, int number_of_objects, iftImage* gt_image, iftImage* cl_image, int dist_border, int max_marker_radius, int min_marker_radius);

#endif
