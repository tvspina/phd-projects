//#include "usisediting.h"
//
//namespace usis
//{
//	FOPFAMattingData::FOPFAMattingData()
//	{
//		Si = NULL;
//		Se = NULL;
//		pixels = NULL;
//	}
//
//	FOPFAMattingData::~FOPFAMattingData()
//	{
//		DestroySet(&pixels);
//
//		DestroySet(&Si);
//
//		DestroySet(&Se);
//	}
//
//	void destroy_fopf_amatting_data(void **data)
//	{
//		if(data == NULL || *data == NULL) return;
//
//		delete (FOPFAMattingData*)(*data);
//
//		*data = NULL;
//	}
//
////GList* usis_border_sets(Image* label, Features* feat, int length, float inadj, float outadj)
////{
////    int p,q;
////    int ncols = label->ncols;
////    int nrows = label->nrows;
////
////    Image* visited = CreateImage(ncols, nrows);
////
////    if(inadj <= 1) inadj = 2.0;
////    if(outadj <= 1) outadj = 1.0;
////
////    AdjRel* Ain = circular(1.0);
////    AdjRel* Ain2 = circular(inadj);
////    AdjRel* Aout = circular(outadj);
////    AdjRel* Amax = NULL;
////
////    if(inadj > outadj)
////        Amax = Ain2;
////    else
////        Amax = Aout;
////
////    Image* eroMin = Erode(label,Ain);
////    Image* eroMax = Erode(label,Ain2);
////    Image* dilMax = Dilate(label, Aout);
////
////    Image* eroded = XOr(eroMin, eroMax);
////    Image* dilated = Diff(dilMax, label);
////    Image* border = Diff(label, eroMin);
////
////    WriteImage(eroded,"s_beroded.pgm");
////    WriteImage(dilated,"s_dilated.pgm");
////    WriteImage(border,"s_border.pgm");
////
////    Image* t = Sum(eroded, dilated);
////    Image* t1 = Sum(border, t);
////
////    WriteImage(t1, "s_t1.pgm");
////
////    DestroyImage(&t);
////    DestroyImage(&t1);
////
////    GList* sglist = NULL;
////    Set* Si = NULL;
////    Set* Se = NULL;
////    Set* pixels = NULL;
////    Set* queue = NULL;
////
////    sglist = CreateGList(usis_destroy_fopf_amatting_data);
////
////    int counter = length;
////
////    int totlength = 0;
////    for(p = 0; p < ncols*nrows; p++)
////    {
////        if(border->val[p] != 0 && visited->val[p] == 0)
////        {
////            InsertSet(&queue, p);
////
////            while(queue != NULL)
////            {
////                int t = RemoveSet(&queue);
////
////                int xt = t%ncols;
////                int yt = t/ncols;
////
////                for(q = 1; q < Amax->n; q++)
////                {
////                    int xq = xt + Amax->dx[q];
////                    int yq = yt + Amax->dy[q];
////
////                    if(ValidPixel(label, xq, yq))
////                    {
////                        /// if it is a border pixel, hasn't been visited, and
////                        /// the counter is > 0
////
////                        int v = xq + label->tbrow[yq];
////
////                        if(border->val[v] != 0 && visited->val[v] == 0)
////                        {
////                            InsertSet(&queue, v);
////                        }else if(dilated->val[v] != 0)
////                        {
////                            if(!IsInSet(Se, v)) InsertSet(&Se, v);
////                        }else if(eroded->val[v] != 0){
////                            if(!IsInSet(Si, v)) InsertSet(&Si, v);
////                        }
////                    }
////                }
////
////                if(counter > 0)
////                {
////                    counter--;
////                    fprintf(stderr,"counter %d\n",counter);
////                    InsertSet(&pixels, t);
////                    totlength++;
////
////                }else{
////                    /// create subgraph here
////                    fprintf(stderr,"totlength  %d set %d\n",totlength, GetSetSize(pixels));
////
////                    FOPFAMattingData* r = usis_create_fopf_amatting_data();
////
////                    r->pixels = pixels;
////                    r->Si = Si;
////                    r->Se = Se;
////
////                    GListInsert(sglist, (void*)r);
////
////                    pixels = NULL;
////                    Si = NULL;
////                    Se = NULL;
////
////                    counter = length;
////
////                }
////
////                visited->val[t] = 1;
////            }
////
////            fprintf(stderr,"counter after while %d\n",counter);
////
////            if(counter > 0 && counter < length)
////            {
////                /// create last subgraph
////                fprintf(stderr,"totlength next %d counter %d\n",totlength, counter);
////
////                FOPFAMattingData* r = usis_create_fopf_amatting_data();
////
////                r->pixels = pixels;
////                r->Si = Si;
////                r->Se = Se;
////
////                GListInsert(sglist, (void*)r);
////
////                pixels = NULL;
////                Si = NULL;
////                Se = NULL;
////
////                counter = length;
////            }
////        }
////    }
////
////    DestroyImage(&visited);
////    DestroyImage(&eroMin);
////    DestroyImage(&eroMax);
////    DestroyImage(&dilMax);
////    DestroyImage(&eroded);
////    DestroyImage(&dilated);
////    DestroyImage(&border);
////    destroy_adjrel(&Ain);
////    destroy_adjrel(&Ain2);
////    destroy_adjrel(&Aout);
////
////    return sglist;
////}
//
//
//	GList *border_sets(Image8 *label, int seglength, AdjRel *A, bool wide_border)
//	{
//		int p,q;
//		int ncols = label->ncols;
//		int nrows = label->nrows;
//
//		Image8 *visited = create_image8(ncols, nrows);
//		Image8 *border = object_border(label);
//
//		GList *sglist = NULL;
//		Set *Si = NULL;
//		Set *Se = NULL;
//		Set *pixels = NULL;
//		Set *queue = NULL;
//
//		sglist = CreateGList(destroy_fopf_amatting_data);
//
//		int counter = seglength;
//
//		int totlength = 0;
//
//		for(p = 0; p < ncols*nrows; p++)
//		{
//			if(border->val[p] > 0 && visited->val[p] == 0)
//			{
//				InsertSet(&queue, p);
//
//				while(queue != NULL)
//				{
//					int t = RemoveSet(&queue);
//
//					int xt = t%ncols;
//					int yt = t/ncols;
//
//					for(q = 1; q < A->n; q++)
//					{
//						int xq = xt + A->dx[q];
//						int yq = yt + A->dy[q];
//
//						if(valid_pixel(label,xq, yq))
//						{
//							/// if it is a border pixel, hasn't been visited, and
//							/// the counter is > 0
//
//							int v = to_index(label,xq,yq);
//
//							if(border->val[v] > 0 && visited->val[v] == 0)
//							{
//								InsertSet(&queue, v);
//							}
//							else if(label->val[v] == 0)
//							{
//								if(!IsInSet(Se, v)) InsertSet(&Se, v);
//							}
//							else if(label->val[v] > 0)
//							{
//								if(!IsInSet(Si, v)) InsertSet(&Si, v);
//							}
//						}
//					}
//
//					if(counter > 0)
//					{
//						counter--;
//						InsertSet(&pixels, t);
//						totlength++;
//
//					}
//					else
//					{
//						/// create subgraph here
//						FOPFAMattingData *r = new FOPFAMattingData();
//
//						if(wide_border)
//						{
//							Set *auxSi = CloneSet(Si);
//							Set *auxSe = CloneSet(Se);
//							MergeSets(&pixels,&auxSi);
//							MergeSets(&pixels,&auxSe);
//						}
//
//						r->pixels = pixels;
//
//						r->Si = Si;
//						r->Se = Se;
//
//
//						GListInsert(sglist, (void*)r);
//
//						pixels = NULL;
//						Si = NULL;
//						Se = NULL;
//
//						counter = seglength;
//
//					}
//
//					visited->val[t] = 1;
//				}
//
//				if(counter > 0 && counter < seglength)
//				{
//					/// create last subgraph
//					FOPFAMattingData *r = new FOPFAMattingData();
//
//					r->pixels = pixels;
//					r->Si = Si;
//					r->Se = Se;
//
//					GListInsert(sglist, (void*)r);
//
//					pixels = NULL;
//					Si = NULL;
//					Se = NULL;
//
//					counter = seglength;
//				}
//			}
//		}
//
//		destroy_image(&visited);
//
//		destroy_image(&border);
//
//		return sglist;
//	}
//
//	Image8 *fopf_alpha_border_aux(GList *list, usis::Image8 *label, Features *feat, double max_arc_weight)
//	{
//		Image8 *alpha = create_image8(label->ncols, label->nrows);
//		float fdo,fdb;
//		int p,n;
//		n = alpha->ncols*alpha->nrows;
//
//		DbImage *obj = create_dbimage(feat->ncols, feat->nrows);
//		DbImage *bkg = create_dbimage(feat->ncols, feat->nrows);
//
//		for(p = 0; p < n; p++)
//			bkg->val[p] = (label->val[p] == 1)? USIS_MAXGRAD : 0;
//
//		GListNode *aux = list->first;
//
//		for(; aux != NULL; aux = aux->next)
//		{
//			Subgraph *sgtrainobj = NULL;
//			Subgraph *sgtrainbkg = NULL;
//
//			FOPFAMattingData *tmp = (FOPFAMattingData*)aux->data;
//
//			/// Since the size of Si + Se is small, we use INT_MAX as
//			/// nsamples so that 50% of the pixels be used for training
//			fopf_on_markers(feat, tmp->Si, tmp->Se, &sgtrainobj, &sgtrainbkg, INT_MAX, max_arc_weight);
//
//			if(sgtrainbkg != NULL && sgtrainobj != NULL)
//			{
//				fopf_pathval_map_set(sgtrainobj, feat, obj, tmp->pixels, USIS_OPFOBJLABEL);
//				fopf_pathval_map_set(sgtrainbkg, feat, bkg, tmp->pixels, USIS_OPFBKGLABEL);
//			}
//
//			DestroySubgraph(&sgtrainobj);
//
//			DestroySubgraph(&sgtrainbkg);
//		}
//
//		for(p=0; p<n; p++)
//		{
//			fdo = (float)obj->val[p];
//			fdb = (float)bkg->val[p];
//
//			if(label->val[p] == 0)
//			{
//				alpha->val[p] = 0.0;
//			} else {
//				alpha->val[p] = ROUND(USIS_MAXALPHA*fdb/(fdb+fdo));
//			}
//		}
//
//		destroy_dbimage(&obj);
//
//		destroy_dbimage(&bkg);
//
//		return alpha;
//	}
//
//// Computes alpha values for pixels on (and around) the border
//// of the segmented object(s)
//	Image8 *fopf_alpha_border(Image8 *label, Features *feat, int seglength, float radius, bool wide_border, double max_arc_weight)
//	{
//		AdjRel *A = circular(radius);
//		// if wide_boder == true then pixels surrounding the
//		// border will also have their alpha values computed
//
//		fprintf(stderr,"Starting to compute border sets\n");
//		GList *list = border_sets(label, seglength, A, wide_border);
//		fprintf(stderr,"Starting to compute alpha border values\n");
//
//		Image8 *alpha = fopf_alpha_border_aux(list, label, feat, max_arc_weight);
//
//		DestroyGList(&list);
//		destroy_adjrel(&A);
//
//		return alpha;
//	}
//
//
//	Image *soft_segmentation(Image *label, Image *mask, Set *seeds, int threshold, int objlabel, int bkglabel)
//	{
//		Set *aux = NULL;
//
//		Image *result = ift_CopyImage(label);
//
//		for(aux = seeds; aux != NULL; aux = aux->next)
//		{
//			const int p = aux->elem;
//			result->val[p] = (mask->val[p] >= threshold)? objlabel:bkglabel;
//		}
//
//		return result;
//	}
//
//	Image *alpha_from_objmap(Image *label, Image *objMap, Set *seeds, int objmaxval, int threshold)
//	{
//		int p;
//		Set *aux = NULL;
//
//		Image *result = CreateImage(label->ncols, label->nrows);
//
//		for(p = 0; p < label->ncols*label->nrows; p++)
//			result->val[p] = label->val[p]*USIS_MAXALPHA;
//
//		for(aux = seeds; aux != NULL; aux = aux->next)
//		{
//			p = aux->elem;
//			result->val[p] = (objMap->val[p] < threshold) ? 0 : objMap->val[p]*USIS_MAXALPHA/objmaxval;
//		}
//
//		return result;
//	}
//
//}
