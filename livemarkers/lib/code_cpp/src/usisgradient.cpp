#include "usisgradient.h"
#include <omp.h>

namespace usis
{
Features *vimage_gradient(Image8 *img, float radius, float *Omax, Rectangle bb)
{
    Features *grad = CreateFeatures(img->ncols, img->nrows, 1);
    vimage_gradient(grad, img, radius, Omax, bb);

    return(grad);
}

void vimage_gradient(Features *grad, Image8 *img, float radius,
                     float *Omax, Rectangle bb)
{
    double    dist, gx, gy;
    int     i, p, x, y, q;
    Pixel   u, v;
    AdjRel *A = circular(radius);
    double   *md = AllocDoubleArray(A->n);

    Rectangle bb1;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, img->ncols - 1, img->nrows - 1);

    grad->Fmax = maximum_value(img) + 1;

    for(i = 1; i < A->n; i++)
        md[i] = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);

    *Omax = -USIS_FINFTY;

    for(y = bb1.beg.y; y <= bb1.end.y; y++)
    {
        for(x = bb1.beg.x; x <= bb1.end.x; x++)
        {
            u.x = x;
            u.y = y;
            p = to_index(img, u);

            gx = gy = 0.0;

            for(i = 1; i < A->n; i++)
            {
                v.x = u.x + A->dx[i];
                v.y = u.y + A->dy[i];

                if(valid_pixel(img, v))
                {
                    q    = to_index(img, v);
                    dist = ((float)img->val[q] - (float)img->val[p]) / grad->Fmax;

                    gx  += dist * A->dx[i] / md[i];
                    gy  += dist * A->dy[i] / md[i];
                }
            }

            grad->elem[p].feat[0] = sqrt(gx * gx + gy * gy);
            *Omax = MAX(*Omax, grad->elem[p].feat[0]);
        }
    }

	grad->Fmax = *Omax;

    free(md);
    destroy_adjrel(&A);
}

DbImage *vimage_gradient2(Image8 *img, float radius, double *Omax, Rectangle bb)
{
    DbImage *grad = create_dbimage(img->ncols, img->nrows);
    vimage_gradient2(grad, img, radius, Omax, bb);

    return(grad);
}

void vimage_gradient2(DbImage *grad, Image8 *img, float radius,
                      double *Omax, Rectangle bb)
{
    double    dist, gx, gy;
    int     i, p, x, y, q;
    Pixel   u, v;
    AdjRel *A = circular(radius);
    double   *md = AllocDoubleArray(A->n);

    Rectangle bb1;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, img->ncols - 1, img->nrows - 1);

    double Imax = maximum_value(img) + 1;

    for(i = 1; i < A->n; i++)
        md[i] = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);

    *Omax = -USIS_DINFTY;

    for(y = bb1.beg.y; y <= bb1.end.y; y++)
    {
        for(x = bb1.beg.x; x <= bb1.end.x; x++)
        {
            u.x = x;
            u.y = y;
            p = to_index(img, u);

            gx = gy = 0.0;

            for(i = 1; i < A->n; i++)
            {
                v.x = u.x + A->dx[i];
                v.y = u.y + A->dy[i];

                if(valid_pixel(img, v))
                {
                    q    = to_index(img, v);
                    dist = ((double)img->val[q] - (double)img->val[p]) / Imax;

                    gx  += dist * A->dx[i] / md[i];
                    gy  += dist * A->dy[i] / md[i];
                }
            }

            grad->val[p] = sqrt(gx * gx + gy * gy);
            *Omax = MAX(*Omax, grad->val[p]);
        }
    }

    free(md);
    destroy_adjrel(&A);
}

void image_gradient_on_mask(DbImage *grad, Image8 *img,
                            float radius, double *Omax, Rectangle bb)
{
    double    dist, gx, gy;
    int     i, p, x, y, q;
    Pixel   u, v;
    AdjRel *A = circular(radius);
    double   *md = AllocDoubleArray(A->n);

    Rectangle bb1;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, img->ncols - 1, img->nrows - 1);

    double Imax = maximum_value(img) + 1;

    for(i = 1; i < A->n; i++)
        md[i] = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);

    *Omax = -USIS_DINFTY;

    for(y = bb1.beg.y; y <= bb1.end.y; y++)
    {
        for(x = bb1.beg.x; x <= bb1.end.x; x++)
        {
            u.x = x;
            u.y = y;

            p = to_index(img, u);

            if(grad->val[p] > 0)
            {
                gx = gy = 0.0;

                for(i = 1; i < A->n; i++)
                {
                    v.x = u.x + A->dx[i];
                    v.y = u.y + A->dy[i];

                    if(valid_pixel(img, v))
                    {
                        q    = to_index(img, v);
                        dist = ((double)img->val[q] - (double)img->val[p]) / Imax;

                        gx  += dist * A->dx[i] / md[i];
                        gy  += dist * A->dy[i] / md[i];
                    }
                }

                grad->val[p] = sqrt(gx * gx + gy * gy);
                *Omax = MAX(*Omax, grad->val[p]);
            }
        }
    }

    free(md);
    destroy_adjrel(&A);
}

Image8 *image_gradient(Image32 *img, float radius)
{
    double    dist, gx, gy;
    double max_grad = -USIS_FINFTY;
    int     i, p, q, n = img->ncols * img->nrows;
    int Imax;
    Pixel   u, v;
    AdjRel *A = circular(radius);
    double   *md = AllocDoubleArray(A->n);

    DbImage *grad = create_dbimage(img->ncols, img->nrows);
    Imax = maximum_value(img) + 1;

    for(i = 1; i < A->n; i++)
        md[i] = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);

    for(p = 0; p < n; p++)
    {
        u.x = p % img->ncols;
        u.y = p / img->ncols;

        gx = gy = 0.0;

        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(img, v))
            {
                q    = to_index(img, v);
                dist = ((float)img->val[q] - (float)img->val[p]) / (float)Imax;

                gx  += dist * A->dx[i] / md[i];
                gy  += dist * A->dy[i] / md[i];
            }
        }

        grad->val[p] = sqrt(gx * gx + gy * gy);
        max_grad = MAX(max_grad, grad->val[p]);
    }

    free(md);
    destroy_adjrel(&A);

    Image8 *final_grad = create_image8(img->ncols, img->nrows);
    for(p = 0; p < n; p++)
    {
        final_grad->val[p] = (uchar)(grad->val[p] * USIS_MAXGRAD / max_grad);
    }

    destroy_dbimage(&grad);

    return(final_grad);
}

Image8 *image_gradient(Image8 *img, float radius)
{
    double    dist, gx, gy;
    double max_grad = -USIS_FINFTY;
    int     i, p, q, n = img->ncols * img->nrows;
    int Imax;
    Pixel   u, v;
    AdjRel *A = circular(radius);
    double   *md = AllocDoubleArray(A->n);

    DbImage *grad = create_dbimage(img->ncols, img->nrows);
    Imax = maximum_value(img) + 1;

    for(i = 1; i < A->n; i++)
        md[i] = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);

    for(p = 0; p < n; p++)
    {
        u.x = p % img->ncols;
        u.y = p / img->ncols;

        gx = gy = 0.0;

        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(img, v))
            {
                q    = to_index(img, v);
                dist = ((float)img->val[q] - (float)img->val[p]) / (float)Imax;

                gx  += dist * A->dx[i] / md[i];
                gy  += dist * A->dy[i] / md[i];
            }
        }

        grad->val[p] = sqrt(gx * gx + gy * gy);
        max_grad = MAX(max_grad, grad->val[p]);
    }

    free(md);
    destroy_adjrel(&A);

    Image8 *final_grad = create_image8(img->ncols, img->nrows);
    for(p = 0; p < n; p++)
    {
        final_grad->val[p] = (uchar)(grad->val[p] * USIS_MAXGRAD / max_grad);
    }

    destroy_dbimage(&grad);

    return(final_grad);
}

Features *vfeatures_gradient(Features *f, float radius, float *Fmax)
{
    Features *grad = CreateFeatures(f->ncols, f->nrows, 3);

    vfeatures_gradient(grad, f, radius, Fmax);

    return grad;
}

void vfeatures_gradient(Features *grad, Features *f, float radius,
                        float *Fmax, Rectangle bb)
{
    double    dist, gx, gy, mag;
    int     j, i, x, y, p, q;
    Pixel   u, v;
    AdjRel *A = circular(radius);
    AdjRel *A5 = box(5,5);
    double   *md = AllocDoubleArray(A->n);

    grad->Fmax = f->Fmax;

    for(i = 1; i < A->n; i++)
        md[i] = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);

    *Fmax = -USIS_FINFTY;

    Rectangle bb1;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, f->ncols - 1, f->nrows - 1);

    for(y = bb1.beg.y; y <= bb1.end.y; y++)
    {
        for(x = bb1.beg.x; x <= bb1.end.x; x++)
        {
            u.x = x;
            u.y = y;
            p = x + grad->ncols * y;

            float max_mag = -USIS_FINFTY;

            for(j = 0; j < f->nfeats; j++)
            {
                gx = gy = 0.0;

                for(i = 1; i < A->n; i++)
                {
                    v.x = u.x + A->dx[i];
                    v.y = u.y + A->dy[i];

                    if((v.x >= 0 && v.x < f->ncols) && (v.y >= 0 && v.y < f->nrows))
                    {
                        q    = v.x + v.y * f->ncols;
                        dist = (f->elem[q].feat[j] - f->elem[p].feat[j]);
                        gx  += dist * A->dx[i] / md[i];
                        gy  += dist * A->dy[i] / md[i];
                    }
                }

                mag = sqrt(gx * gx + gy * gy);

                if(mag > max_mag)
                {
                    grad->elem[p].feat[0] = mag;
                    grad->elem[p].feat[1] = gx;
                    grad->elem[p].feat[2] = gy;

                    max_mag = mag;
                    *Fmax = MAX(*Fmax, mag);
                }
            }
        }
    }

	grad->Fmax = *Fmax;

    free(md);
    destroy_adjrel(&A);
    destroy_adjrel(&A5);
}

Image8 *feat_obj_weight_image(Features *Gfeat, Features *Gobj, float Wobj,
                              float Fmax, float Omax,
                              Rectangle bb)
{
    Image8 *img = create_image8(Gfeat->ncols, Gfeat->nrows);

    feat_obj_weight_image(Gfeat, Gobj, img, Wobj, Fmax, Omax, bb);

    return img;
}

void feat_obj_weight_image(Features *Gfeat, Features *Gobj, Image8 *img,
                           float Wobj, float Fmax, float Omax,
                           Rectangle bb)
{
    int ncols, nrows;
    int x, y, p;
    Rectangle bb1;

    uchar weight;

    ncols     = Gfeat->ncols;
    nrows     = Gfeat->nrows;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, ncols - 1, nrows - 1);


    if(Gobj != NULL && Wobj > 0.0)
    {
        for(y = bb1.beg.y; y <= bb1.end.y; y++)
        {
            for(x = bb1.beg.x; x <= bb1.end.x; x++)
            {
                p = to_index(img, x, y);
                const float mag_go = Gobj->elem[p].feat[0] / Omax;
                const float mag_gf = Gfeat->elem[p].feat[0] / Fmax;

                weight = (uchar)(USIS_MAXGRAD * (Wobj * mag_go + (1.0 - Wobj) * mag_gf));
                img->val[p] = weight;
            }
        }
    }
    else
    {
        for(y = bb1.beg.y; y <= bb1.end.y; y++)
        {
            for(x = bb1.beg.x; x <= bb1.end.x; x++)
            {
                p = to_index(img, x, y);

                weight = (uchar)(USIS_MAXGRAD * Gfeat->elem[p].feat[0] / Fmax);
                img->val[p] = weight;
            }
        }
    }
}

void feat_obj_cloud_weight_image(Features *Gfeat, DbImage *Gobj,
                                 Image8 *cloud_grad,
                                 Image8 *final_grad,
                                 double Wobj, double Wfeat, double Wcloud,
                                 float Fmax, float Omax,
                                 Pixel grad_center, Pixel cloud_position,
                                 Rectangle bb)
{
    int x, y, p;
    Rectangle bb1;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, final_grad->ncols - 1, final_grad->nrows - 1);
    /** The values of all the pixels in m_grad are going to be copied
        to final_grad. The pixels which are superimposed by the cloud
        gradient take into account the cloud gradient values, whereas the
        values of the remaining ones are adjusted to consider the cloud gradient
        weight factor because these pixels are not under the cloud.
     **/
    for(y = bb1.beg.y; y <= bb1.end.y; y++)
    {
        for(x = bb1.beg.x; x <= bb1.end.x; x++)
        {
            p = to_index(final_grad, x, y);
            Pixel px = (Pixel)
            {
                x, y
            };
            /** Converting from final_grad coordinates to
                cloud_grad coordinates
             **/
            px.x = px.x - cloud_position.x + grad_center.x;
            px.y = px.y - cloud_position.y + grad_center.y;

            double mag_go = (Gobj != NULL && Omax != 0.0) ? Gobj->val[p] / Omax : 0.0;
            double mag_gf = Gfeat->elem[p].feat[0] / Fmax;

            if(valid_pixel(cloud_grad, px))
            {
                int q = to_index(cloud_grad, px);

                double mag_cloud = ((double)cloud_grad->val[q]) / USIS_MAXGRAD;

                final_grad->val[p] = (uchar)(USIS_MAXGRAD * (Wobj * mag_go
                                             + Wfeat * mag_gf + Wcloud * mag_cloud));
            }
            else
            {
                final_grad->val[p] = (uchar)(USIS_MAXGRAD * (Wobj * mag_go + Wfeat * mag_gf));
            }
        }
    }
}

void local_feat_obj_weight_image(Features *Gfeat, DbImage *Gobj,
                                 DbImage *acc_grad,
                                 DbImage *norm_factor,
                                 double Wfeat, double Wobj, double Wcloud,
                                 AdjRel *Awindow,
                                 float Fmax, float Omax,
                                 Pixel window_position,
                                 Pixel cloud_position,
                                 Image8 *cloud_grad,
                                 Pixel cloud_grad_center)
{
    int i;
    double epsilon = 0.00001;

    for(i = 0; i < Awindow->n; i++)
    {
        /** Obtaining the pixel coordinates in final_grad from
            the sample's vector. The window_position should be
            provided in final_grad coordinates.
        **/
        Pixel px_vec, px_grad;
        px_vec.x = window_position.x + Awindow->dx[i];
        px_vec.y = window_position.y + Awindow->dy[i];

        if(valid_pixel(acc_grad, px_vec.x, px_vec.y))
        {
            int p = px_vec.x + px_vec.y * acc_grad->ncols;
            double spatial_eucl_dist = 1.0;
            double mag_go = Gobj->val[p] / Omax;
            double mag_gf = Gfeat->elem[p].feat[0] / Fmax;

            /** Converting from acc_grad coordinates to
                the cloud_grad coordinates
             **/
            px_grad.x = px_vec.x - cloud_position.x + cloud_grad_center.x;
            px_grad.y = px_vec.y - cloud_position.y + cloud_grad_center.y;


            /** The final gradient for pixel p takes into account all windows
                which cover it, by performing a sum weighted by the inverse
                spatial distance to each window's center.
             **/
            spatial_eucl_dist = sqrt((double)eucl_dist2(px_vec,
                                     window_position))
                                + epsilon;

            if(valid_pixel(cloud_grad, px_grad))
            {
                int q = to_index(cloud_grad, px_grad);

                double cloud_grad_val = Wcloud * ((double)cloud_grad->val[q]) / USIS_MAXGRAD;


                acc_grad->val[p] += (Wobj * mag_go + Wfeat * mag_gf + cloud_grad_val) / spatial_eucl_dist;
            }
            else
            {
                acc_grad->val[p] += (Wobj * mag_go + Wfeat * mag_gf) / spatial_eucl_dist;

            }

            norm_factor->val[p] += 1.0 / spatial_eucl_dist;
        }
    }
}



void local_combine_cloud_grad(Image8 *final_grad,
                              Image8 *cloud_grad,
                              DbImage *acc_grad,
                              DbImage *norm_factor,
                              double Wcloud,
                              Pixel cloud_grad_center,
                              Pixel cloud_position,
                              Rectangle bb)
{
    int x, y, p;
    Rectangle bb1;

    if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
        bb1 = bb;
    else
        bb1 = Rectangle(0, 0, final_grad->ncols - 1, final_grad->nrows - 1);

    for(y = bb1.beg.y; y <= bb1.end.y; y++)
    {
        for(x = bb1.beg.x; x <= bb1.end.x; x++)
        {
            p = to_index(final_grad, x, y);
            Pixel px = to_pixel(final_grad, p);
            /// converting from m_grad coordinates to
            /// the m_cloud_grad coordinates
            px.x = px.x - cloud_position.x + cloud_grad_center.x;
            px.y = px.y - cloud_position.y + cloud_grad_center.y;

            double reg_grad_val = acc_grad->val[p] / norm_factor->val[p];

            if(valid_pixel(cloud_grad, px))
            {
                int q = to_index(cloud_grad, px);

                double cloud_grad_val = Wcloud * ((double)cloud_grad->val[q]) / USIS_MAXGRAD;
                /// dividing by 1.0 + m_WCloud guarantees that m_WCloud + Wobj + (1-Wobj) == 1.0
                final_grad->val[p] = (uchar)((reg_grad_val + cloud_grad_val) * USIS_MAXGRAD);
            }
            else
            {

                final_grad->val[p] = (uchar)(reg_grad_val * USIS_MAXGRAD);
            }
        }
    }
}

Image8 *weighted_image_gradient(CImage8 *cimg, AdjRel *A,
                                float channel_weight[3])
{
	int Imax = INT_MIN;
    Image32 *grad = create_image32(cimg->ncols, cimg->nrows);
    Image8 *grad8 = create_image8(cimg->ncols, cimg->nrows);
    Pixel u, v;
    for(int s = 0; s < grad->n; s++)
    {
        int i;
        double s_c[3];
        double dist_sum = 0.0;

        u   = to_pixel(cimg, s);

        s_c[0] = (double)cimg->C[0]->val[s];
        s_c[1] = (double)cimg->C[1]->val[s];
        s_c[2] = (double)cimg->C[2]->val[s];

        for(i = 1; i < A->n; i++)
        {
            v = adjacent(A, u, i);
            if(valid_pixel(grad, v))
            {
                int c;

                int t = to_index(cimg, v);
                double t_c[3], dist = 0.0;

                t_c[0] = (double)cimg->C[0]->val[t];
                t_c[1] = (double)cimg->C[1]->val[t];
                t_c[2] = (double)cimg->C[2]->val[t];

                for(c = 0; c < 3; c++)
                    dist += (t_c[c] - s_c[c])*(t_c[c] - s_c[c])*channel_weight[c];

                dist_sum += sqrt(dist);
            }
        }

        grad->val[s] = (Image32::value_type)dist_sum;
        Imax = MAX(Imax, grad->val[s]);
    }

	for(int s = 0; s < grad->n; s++)
		grad8->val[s] = ROUND(((USIS_MAXGRAD-1.0)/Imax * grad->val[s]) + 1.0);

	destroy_image(&grad);

    return grad8;
}

Features* weighted_feature_gradient(Features *feat, float *Fmax,
                                    AdjRel *A, float *feat_weight)
{
    Features *Gfeat = CreateFeatures(feat->ncols, feat->nrows, 1);

    weighted_feature_gradient(feat, Gfeat, Fmax, A, feat_weight);

    return Gfeat;
}

void weighted_feature_gradient(Features *feat, Features *Gfeat, float *Fmax,
                               AdjRel *A, float *feat_weight)
{
    int         s;
    double       dist;

    Pixel u, v;

    *Fmax = -USIS_FINFTY;
    for(s = 0; s < Gfeat->ncols*Gfeat->nrows; s++)
    {
        int i;
        float *val_s = feat->elem[s].feat;

        u = to_pixel(Gfeat, s);

		Gfeat->elem[s].feat[0] = 0.0;

        for(i = 1; i < A->n; i++)
        {
            v = adjacent(A, u, i);
            // This was wrongly here for ICASSP robot paper
            dist = 0.0;
            if(valid_pixel(Gfeat, v))
            {
                int c;

                int t = to_index(Gfeat, v);
                float *val_t = feat->elem[t].feat;

                dist = 0.0;

                for(c = 0; c < feat->nfeats; c++)
                    dist += (val_t[c] - val_s[c])*(val_t[c] - val_s[c])*feat_weight[c];

                Gfeat->elem[s].feat[0] += sqrt(dist);
            }
        }

        *Fmax = MAX(*Fmax, Gfeat->elem[s].feat[0]);

    }

    // Normalize weights
    if(*Fmax > 0.0)
    {
        for(s = 0; s < Gfeat->ncols*Gfeat->nrows; s++)
        {
            Gfeat->elem[s].feat[0] /= *Fmax;
        }
    }

    Gfeat->Fmax = *Fmax;
}

Features* normalized_gradient(Features *feat, double eta, int Awidth)
{
	Features *normalized = NULL;
	AdjRel *A = NULL;
	float Fmax = -USIS_FINFTY;
	float Fmin = USIS_FINFTY;

	assert(Awidth > 3);

	normalized = CreateFeatures(feat->ncols, feat->nrows, 3);
	A = box(Awidth,Awidth);


	for(int f = 0; f < normalized->nfeats; f++)
	{
		for(int p = 0; p < feat->nelems; p++)
		{
			double x_sum = 0.0, x2_sum = 0.0, variance = 0.0, nelems = 0.0;
			double norm_factor = 0.0;
			Pixel px_0;
			Pixel px = usis::to_pixel(feat, p);
			float sobel_x[3][3] = {{-1,0,1}, {-2,0,2},{-1,0,1}};
			float sobel_y[3][3] = {{1,2,1}, {0,0,0},{-1,-2,-1}};
			float dx = 0.0, dy = 0.0, mag = 0.0;
			int i = 0;

			// Sobel operator
			for(int x = 0; x < 3; x++)
				for(int y = 0; y < 3; y++)
				{
					if(usis::valid_pixel(feat, px.x-x-1,px.y-y-1))
					{
						dx += feat->elem[to_index(feat,px.x-x-1,px.y-y-1)].feat[f]*sobel_x[x][y];
						dy += feat->elem[to_index(feat,px.x-x-1,px.y-y-1)].feat[f]*sobel_y[x][y];
					}
				}

			// Computing normalization factor inside window
			px_0 = px;
			px = next_adjacent(feat, A, px_0, i);

			while(px.x != NIL && px.y != NIL)
			{
				int q = usis::to_index(feat, px);
				x_sum += feat->elem[q].feat[f];
				x2_sum += feat->elem[q].feat[f]*feat->elem[q].feat[f];

				px = next_adjacent(feat, A, px_0, i);
				nelems++;
			}

			variance = x2_sum/nelems - (x_sum/nelems)*(x_sum/nelems);

			norm_factor = sqrt(variance + eta*eta);

			mag = sqrt(dx*dx+dy*dy)/norm_factor;

			if(mag > normalized->elem[p].feat[0])
			{
				normalized->elem[p].feat[0] = mag;
				normalized->elem[p].feat[1] = dx/norm_factor;
				normalized->elem[p].feat[2] = dy/norm_factor;
				Fmax = MAX(Fmax,mag);
				Fmin = MIN(Fmin,mag);
			}
		}
	}

	normalized->Fmax = Fmax;
	normalized->Fmin = Fmin;

	destroy_adjrel(&A);

	return normalized;
}

}
