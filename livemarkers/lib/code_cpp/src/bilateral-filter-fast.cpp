/* This file is an image processing operation for GEGL
 *
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2012 Victor Oliveira <victormatheus@gmail.com>
 */

 /* This is an implementation of a fast approximated bilateral filter
  * algorithm descripted in:
  *
  *  A Fast Approximation of the Bilateral Filter using a Signal Processing Approach
  *  Sylvain Paris and Frédo Durand
  *  European Conference on Computer Vision (ECCV'06)
  */

#include "bilateral-filter-fast.h"

extern "C"
{
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#if __GNUC_MINOR__ > 6
#include <stdalign.h>
#endif
}

#include <omp.h>

inline static float lerp(float a, float b, float v)
{
  return (1.0f - v) * a + v * b;
}

inline static int clamp(int x, int low, int high)
{
  return (x < low) ? low : ((x > high) ? high : x);
}

void
bilateral_filter (const float * __restrict__ _input,
                  float       * __restrict__ _output,
                  int   width,
                  int   height,
                  int 	channels,
                  int   s_sigma,
                  float r_sigma)
{
  const int padding_xy = 2;
  const int padding_z  = 2;

  const int sw = (width -1) / s_sigma + 1 + (2 * padding_xy);
  const int sh = (height-1) / 	 + 1 + (2 * padding_xy);
  const int depth = (int)(1.0f / r_sigma) + 1 + (2 * padding_z);

  /* down-sampling */

#if __GNUC_MINOR__ > 6
	// enabling AVX (SSE evolution)
  float * __restrict__ input  = (float * __restrict__) __builtin_assume_aligned(_input,  32);
  float * __restrict__ output = (float * __restrict__) __builtin_assume_aligned(_output, 32);
#else
  float * __restrict__ input  = (float * __restrict__) _input;
  float * __restrict__ output = (float * __restrict__) _output;
#endif

  float * __restrict__ grid ;
  float * __restrict__ blurx;
  float * __restrict__ blury;
  float * __restrict__ blurz;

  posix_memalign((void **) &grid , 32, sw * sh * depth * channels * 2 * sizeof(float));
  posix_memalign((void **) &blurx, 32, sw * sh * depth * channels * 2 * sizeof(float));
  posix_memalign((void **) &blury, 32, sw * sh * depth * channels * 2 * sizeof(float));
  posix_memalign((void **) &blurz, 32, sw * sh * depth * channels * 2 * sizeof(float));

  #define INPUT(x,y,c) input[(c)+channels*((x) + width * (y))]

  #define  GRID(x,y,z,c,i) grid [(i)+2*((c)+channels*((x)+sw*((y)+(z)*sh)))]
  #define BLURX(x,y,z,c,i) blurx[(i)+2*((c)+channels*((x)+sw*((y)+(z)*sh)))]
  #define BLURY(x,y,z,c,i) blury[(i)+2*((c)+channels*((x)+sw*((y)+(z)*sh)))]
  #define BLURZ(x,y,z,c,i) blurz[(i)+2*((c)+channels*((x)+sw*((y)+(z)*sh)))]

  for (int k=0; k < (sw * sh * depth * channels * 2); k++)
    {
      grid [k] = 0.0f;
      blurx[k] = 0.0f;
      blury[k] = 0.0f;
      blurz[k] = 0.0f;
    }

  /* downsampling */

  for(int y = 0; y < height; y++)
    for(int x = 0; x < width; x++)
      for(int c = 0; c < channels; c++)
        {
          const float z = INPUT(x,y,c);

          const int small_x = (int)((float)(x) / s_sigma + 0.5f) + padding_xy;
          const int small_y = (int)((float)(y) / s_sigma + 0.5f) + padding_xy;
          const int small_z = (int)((float)(z) / r_sigma + 0.5f) + padding_z;

          assert(small_x >= 0 && small_x < sw);
          assert(small_y >= 0 && small_y < sh);
          assert(small_z >= 0 && small_z < depth);

          GRID(small_x, small_y, small_z, c, 0) += INPUT(x, y, c);
          GRID(small_x, small_y, small_z, c, 1) += 1.0f;
        }

  /* blur in x, y and z */
  /* XXX: we could use less memory, but at expense of code readability */

  #pragma omp parallel for
  for (int z = 1; z < depth-1; z++)
    for (int y = 1; y < sh-1; y++)
      for (int x = 1; x < sw-1; x++)
        for(int c = 0; c < channels; c++)
          for (int i=0; i<2; i++)
            BLURX(x, y, z, c, i) = (GRID (x-1, y, z, c, i) + 2.0f * GRID (x, y, z, c, i) + GRID (x+1, y, z, c, i)) / 4.0f;

  #pragma omp parallel for
  for (int z = 1; z < depth-1; z++)
    for (int y = 1; y < sh-1; y++)
      for (int x = 1; x < sw-1; x++)
        for(int c = 0; c < channels; c++)
          for (int i=0; i<2; i++)
            BLURY(x, y, z, c, i) = (BLURX (x, y-1, z, c, i) + 2.0f * BLURX (x, y, z, c, i) + BLURX (x, y+1, z, c, i)) / 4.0f;

  #pragma omp parallel for
  for (int z = 1; z < depth-1; z++)
    for (int y = 1; y < sh-1; y++)
      for (int x = 1; x < sw-1; x++)
        for(int c = 0; c < channels; c++)
          for (int i=0; i<2; i++)
            BLURZ(x, y, z, c, i) = (BLURY (x, y, z-1, c, i) + 2.0f * BLURY (x, y, z, c, i) + BLURY (x, y, z+1, c, i)) / 4.0f;

  /* trilinear filtering */

  #pragma omp parallel for
  for (int y=0; y < height; y++)
    for (int x=0; x < width; x++)
      for(int c = 0; c < channels; c++)
        {
          float xf = (float)(x) / s_sigma + padding_xy;
          float yf = (float)(y) / s_sigma + padding_xy;
          float zf = INPUT(x,y,c) / r_sigma + padding_z;

          int x1 = (int)xf;
          int y1 = (int)yf;
          int z1 = (int)zf;

          int x2 = x1+1;
          int y2 = y1+1;
          int z2 = z1+1;

          float x_alpha = xf - x1;
          float y_alpha = yf - y1;
          float z_alpha = zf - z1;

          float interpolated[2];

          assert(xf >= 0 && xf < sw);
          assert(yf >= 0 && yf < sh);
          assert(zf >= 0 && zf < depth);

          for (int i=0; i<2; i++)
              interpolated[i] =
              lerp(lerp(lerp(BLURZ(x1, y1, z1, c, i), BLURZ(x2, y1, z1, c, i), x_alpha),
                        lerp(BLURZ(x1, y2, z1, c, i), BLURZ(x2, y2, z1, c, i), x_alpha), y_alpha),
                   lerp(lerp(BLURZ(x1, y1, z2, c, i), BLURZ(x2, y1, z2, c, i), x_alpha),
                        lerp(BLURZ(x1, y2, z2, c, i), BLURZ(x2, y2, z2, c, i), x_alpha), y_alpha), z_alpha);

          output[channels*(y*width+x)+c] = interpolated[0] / interpolated[1];
        }

  free (grid);
  free (blurx);
  free (blury);
  free (blurz);
}
