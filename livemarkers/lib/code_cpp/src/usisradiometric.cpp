#include "usisradiometric.h"

namespace usis
{
    Image8 *linear_stretch(Image8 *img, Image8::value_type f1, Image8::value_type f2,
							Image8::value_type g1, Image8::value_type g2)
	{
		Image8 *simg=NULL;
		simg = copy_image8(img);
		linear_stretch_inplace(simg, f1, f2, g1, g2);

		return simg;
	}

    void linear_stretch_inplace(Image8 *simg, Image8::value_type f1, Image8::value_type f2,
								Image8::value_type g1, Image8::value_type g2)
    {

        int p,n;
        float a;

        n    = simg->ncols*simg->nrows;
        if (f1 != f2)
            a = (float)(g2-g1)/(float)(f2-f1);
        else
            a = FLT_MAX;

        for (p=0; p < n; p++)
        {
            if (simg->val[p] < f1)
                simg->val[p] = g1;
            else
                if (simg->val[p] > f2)
                simg->val[p] = g2;
            else
            {
                if (a != FLT_MAX)
                    simg->val[p] = (int)(a*(simg->val[p]-f1)+g1);
                else
                {
                    simg->val[p] = g2;
                }
            }
        }
    }

    Image32 *linear_stretch(Image32 *img, Image32::value_type f1, Image32::value_type f2,
                          Image32::value_type g1, Image32::value_type g2)
	{
        Image32 *simg=NULL;
        simg = copy_image32(img);
		linear_stretch_inplace(simg, f1, f2, g1, g2);

		return simg;
	}

    void linear_stretch_inplace(Image32 *simg, Image32::value_type f1,
								Image32::value_type f2, Image32::value_type g1,
								Image32::value_type g2)
    {

        int p,n;
        float a;

        n    = simg->ncols*simg->nrows;
        if (f1 != f2)
            a = (float)(g2-g1)/(float)(f2-f1);
        else
            a = FLT_MAX;

        for (p=0; p < n; p++)
        {
            if (simg->val[p] < f1)
                simg->val[p] = g1;
            else
                if (simg->val[p] > f2)
                simg->val[p] = g2;
            else
            {
                if (a != FLT_MAX)
                    simg->val[p] = (int)(a*(simg->val[p]-f1)+g1);
                else
                {
                    simg->val[p] = g2;
                }
            }
        }
    }




	void log_stretch(Image32 *img, int fmin, int fmax, int gmin, int gmax)
	{
		int p, n = img->ncols*img->nrows;
		int fdiff = fmax - fmin;
		int gdiff = gmax - gmin;

		for (p = 0; p < n; p++)
		{
			double loga = log(1+img->val[p]);
			double logb = log(1 + fdiff);
			img->val[p] = (int)(loga*gdiff/logb + gmin);
		}
	}

	void log_stretch(Image8 *img, int fmin, int fmax, int gmin, int gmax)
	{
		int p, n = img->ncols*img->nrows;
		int fdiff = fmax - fmin;
		int gdiff = gmax - gmin;

		for (p = 0; p < n; p++)
		{
			double loga = log(1+img->val[p]);
			double logb = log(1 + fdiff);
			img->val[p] = (int)(loga*gdiff/logb + gmin);

		}
	}

    /// Sigmoidal rescaling of values.
    /// Since negative values are considered to be inside the signed tde of the mask
    /// then sigma and beta must be negative
    Image8 *sigmoidal_stretch(Image32 *img, int neg_thresh, int pos_thresh,
								double beta, double sigma, Image8::value_type maxval)
    {
        int p;
        Image8 *result = create_image8(img->ncols, img->nrows);

        for(p = 0; p < img->ncols * img->nrows; p++)
        {
            result->val[p] = negative_sigmoid((double)img->val[p],
											 0.0, 1.0, neg_thresh,
											 pos_thresh,
											 beta, sigma)*maxval;
        }
        return result;
    }


	int update_brightness_contrast(int value, int B, int C)
	{
		float v,width,level,l1,l2;

		v = (float) value;
		level = (1.0 - (float)B/100.0) * 255.0;
		width = (1.0 - (float)C/100.0) * 255.0;

		l1 = level - width/2.0;
		l2 = level + width/2.0;

		if (l1<0)   l1 = 0.0;

		if (l2>255) l2 = 255.0;

		if (value < l1)
			v = 0.0;
		else if (value >= l2)
			v = 255.0;
		else
		{
			v = (value - l1)/(l2-l1);
			v *= 255.0;
		}

		return (int)v;
	}

	void update_image_brightness_contrast(Image8 *img, int B, int C)
	{
		int p;

		for (p = 0; p < img->ncols*img->nrows; p++)
		{
			/// updating in place
			img->val[p] = update_brightness_contrast(img->val[p],B,C);
		}
	}

	void update_image_brightness_contrast(Image32 *img, int B, int C)
	{
		int p;

		for (p = 0; p < img->ncols*img->nrows; p++)
		{
			/// updating in place
			img->val[p] = update_brightness_contrast(img->val[p],B,C);
		}
	}

	void update_cimage_brightness_contrast(CImage8 *cimg, int B, int C)
	{
		int p;

		for (p = 0; p < cimg->ncols*cimg->nrows; p++)
		{
			/// converting from RGB to YCbCr
			const int ycbcr = RGB2YCbCr(triplet(cimg->C[0]->val[p],cimg->C[1]->val[p],cimg->C[2]->val[p]));
			/// updating brightness and contrast of Y
			const int y1 = update_brightness_contrast(t0(ycbcr),B,C);
			/// back to RGB
			const int rgb = YCbCr2RGB(triplet(y1,t1(ycbcr),t2(ycbcr)));

			/// updating in place
			cimg->C[0]->val[p] = (uchar)t0(rgb);
			cimg->C[1]->val[p] = (uchar)t1(rgb);
			cimg->C[2]->val[p] = (uchar)t2(rgb);
		}
	}

    Curve *histogram(Image8 *img)
    {
        int i,p,n,nbins;
        Curve *hist=NULL;

        nbins = maximum_value(img)+1;
        hist  = create_curve(nbins);
        n     = img->ncols*img->nrows;

        for (p=0; p < n; p++)
            hist->Y[img->val[p]]++;
        for (i=0; i < nbins; i++)
            hist->X[i] = i;

      return(hist);
    }

    Curve *histogram(Image32 *img)
    {
        int i,p,n,nbins;
        Curve *hist=NULL;

        nbins = maximum_value(img)+1;
        hist  = create_curve(nbins);
        n     = img->ncols*img->nrows;

        for (p=0; p < n; p++)
            hist->Y[img->val[p]]++;
        for (i=0; i < nbins; i++)
            hist->X[i] = i;

      return(hist);
    }

}
