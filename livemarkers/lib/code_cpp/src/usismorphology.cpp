#include "usismorphology.h"

namespace usis
{
	AdjPxl *adj_pixels(int ncols, AdjRel *A)
	{
		AdjPxl *N;
		int i;

		N = (AdjPxl *) calloc(1, sizeof(AdjPxl));

		if(N != NULL)
		{
			N->dp = AllocIntArray(A->n);
			N->n  = A->n;

			for(i = 0; i < N->n; i++)
				N->dp[i] = A->dx[i] + ncols * A->dy[i];
		}
		else
		{
			Error((char*)MSG1, (char*)"usis::adj_pixels");
		}

		return(N);
	}

	Image8 *object_border(Image8 *bin)
	{
		Image8 *border = create_image8(bin->ncols, bin->nrows);
		int p, q, i;
		Pixel u, v;
		AdjRel *A = circular(1.0);

		for(u.y = 0; u.y < bin->nrows; u.y++)
			for(u.x = 0; u.x < bin->ncols; u.x++)
			{
				p = to_index(bin, u);

				if(bin->val[p] > 0)
				{
					for(i = 1; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];

						if(valid_pixel(bin, v))
						{
							q = to_index(bin, v);

							if(bin->val[q] == 0)
							{
								border->val[p] = 1;
								break;
							}
						}
					}
				}
			}

		destroy_adjrel(&A);
		return(border);
	}


	set<int> object_border2(Image8 *bin)
	{
		set<int> border;
		int p, q, i;
		Pixel u, v;
		AdjRel *A = circular(1.0);

		for(u.y = 0; u.y < bin->nrows; u.y++)
			for(u.x = 0; u.x < bin->ncols; u.x++)
			{
				p = to_index(bin, u);

				if(bin->val[p] > 0)
				{
					for(i = 1; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];

						if(valid_pixel(bin, v))
						{
							q = to_index(bin, v);

							if(bin->val[q] == 0)
							{
								border.insert(p);
								break;
							}
						}
					}
				}
			}

		destroy_adjrel(&A);
		return(border);
	}

	Image8 *dilate(Image8 *img, AdjRel *A)
	{
		Image32 *img32 = cast_2_image32(img);
		Image32 *fimg = NULL, *fdil = NULL, *dil = NULL;
		AdjPxl *N = NULL;
		int sz, p, q, max, i, x, y, po;

		sz  = FrameSize(A);
		fimg = add_frame(img32, sz, INT_MIN);
		fdil = create_image32(fimg->ncols, fimg->nrows);
		N  = adj_pixels(fimg->ncols, A);

		for(y = 0, po = to_index(fimg, sz, sz); y < img->nrows; y++, po = po + fimg->ncols)
			for(x = 0, p = po; x < img->ncols; x++, p++)
			{
				max = INT_MIN;

				for(i = 0; i < N->n; i++)
				{
					q = p + N->dp[i];

					if(fimg->val[q] > max)
						max = fimg->val[q];
				}

				fdil->val[p] = max;
			}

		dil = rem_frame(fdil, sz);
		Image8 *dil8 = cast_2_image8(dil);

		destroy_image(&fimg);
		destroy_image(&fdil);
		destroy_image(&dil);
		destroy_image(&img32);
		DestroyAdjPxl(&N);

		return dil8;
	}

	Image32 *dilate(Image32 *img, AdjRel *A)
	{
		Image32 *fimg = NULL, *fdil = NULL, *dil = NULL;
		AdjPxl *N = NULL;
		int sz, p, q, max, i, x, y, po;

		sz  = FrameSize(A);
		fimg = add_frame(img, sz, INT_MIN);
		fdil = create_image32(fimg->ncols, fimg->nrows);
		N  = adj_pixels(fimg->ncols, A);

		for(y = 0, po = to_index(fimg, sz, sz); y < img->nrows; y++, po = po + fimg->ncols)
			for(x = 0, p = po; x < img->ncols; x++, p++)
			{
				max = INT_MIN;

				for(i = 0; i < N->n; i++)
				{
					q = p + N->dp[i];

					if(fimg->val[q] > max)
						max = fimg->val[q];
				}

				fdil->val[p] = max;
			}

		dil = rem_frame(fdil, sz);

		destroy_image(&fimg);
		destroy_image(&fdil);
		DestroyAdjPxl(&N);

		return dil;
	}

	Image8 *erode(Image8 *img, AdjRel *A)
	{
		Image32 *img32 = cast_2_image32(img);
		Image32 *fimg = NULL, *fero = NULL, *ero = NULL;
		AdjPxl *N = NULL;
		int sz, p, q, min, i, x, y, po;

		sz  = FrameSize(A);
		fimg = add_frame(img32, sz, INT_MAX);
		fero = create_image32(fimg->ncols, fimg->nrows);
		N  = adj_pixels(fimg->ncols, A);

		for(y = 0, po = to_index(fimg, sz, sz); y < img->nrows; y++, po = po + fimg->ncols)
			for(x = 0, p = po; x < img->ncols; x++, p++)
			{
				min = INT_MAX;

				for(i = 0; i < N->n; i++)
				{
					q = p + N->dp[i];

					if(fimg->val[q] < min)
						min = fimg->val[q];
				}

				fero->val[p] = min;
			}

		ero = rem_frame(fero, sz);
		Image8 *ero8 = cast_2_image8(ero);
		destroy_image(&fimg);
		destroy_image(&fero);
		destroy_image(&ero);
		destroy_image(&img32);
		DestroyAdjPxl(&N);

		return ero8;
	}


	Image32 *erode(Image32 *img, AdjRel *A)
	{
		Image32 *fimg = NULL, *fero = NULL, *ero = NULL;
		AdjPxl *N = NULL;
		int sz, p, q, min, i, x, y, po;

		sz  = FrameSize(A);
		fimg = add_frame(img, sz, INT_MAX);
		fero = create_image32(fimg->ncols, fimg->nrows);
		N  = adj_pixels(fimg->ncols, A);

		for(y = 0, po = to_index(fimg, sz, sz); y < img->nrows; y++, po = po + fimg->ncols)
			for(x = 0, p = po; x < img->ncols; x++, p++)
			{
				min = INT_MAX;

				for(i = 0; i < N->n; i++)
				{
					q = p + N->dp[i];

					if(fimg->val[q] < min)
						min = fimg->val[q];
				}

				fero->val[p] = min;
			}

		ero = rem_frame(fero, sz);

		destroy_image(&fimg);
		destroy_image(&fero);
		DestroyAdjPxl(&N);

		return ero;
	}

	Image8 *close(Image8 *img, AdjRel *A)
	{
		Image8 *clse = NULL, *dil = NULL;

		dil   = dilate(img, A);
		clse = erode(dil, A);
		destroy_image(&dil);

		return(clse);
	}

	Image8 *open(Image8 *img, AdjRel *A)
	{
		Image8 *op = NULL, *ero = NULL;

		ero = erode(img, A);
		op   = dilate(ero, A);
		destroy_image(&ero);

		return(op);
	}

	Image32 *open(Image32 *img, AdjRel *A)
	{
		Image32 *op = NULL, *ero = NULL;

		ero = erode(img, A);
		op   = dilate(ero, A);
		destroy_image(&ero);

		return(op);
	}

	Image8 *close_rec(Image8 *img, AdjRel *A)
	{
		Image8 *cls = NULL, *crec = NULL;
		Image32 *cls32 = NULL;
		AdjRel *A4;

		cls = close(img, A);
		A4    = circular(1.0);
		cls32 = cast_2_image32(cls);
		crec  = sup_rec(img, cls32, A4);
		destroy_image(&cls);
		destroy_image(&cls32);
		destroy_adjrel(&A4);

		return(crec);
	}

	Image8 *open_rec(Image8 *img, AdjRel *A)
	{
		Image8 *op = NULL, *orec = NULL;
		Image32 *open32 = NULL;
		AdjRel *A4;

		op = open(img, A);
		A4   = circular(1.0);
		open32 = cast_2_image32(op);
		orec = inf_rec(img, open32, A4);
		destroy_image(&op);
		destroy_image(&open32);
		destroy_adjrel(&A4);

		return(orec);
	}


	Image32 *open_rec(Image32 *img, AdjRel *A)
	{
		Image32 *op = NULL, *orec = NULL;

		AdjRel *A4;

		op = open(img, A);
		A4   = circular(1.0);
		orec = inf_rec(img, op, A4);
		destroy_image(&op);
		destroy_adjrel(&A4);

		return(orec);
	}

	Image32 *label_comp(Image32 *img, AdjRel *A, int thres)
	{
		Image32 *label = NULL, *flabel = NULL, *fimg = NULL;
		int i, j, n, sz, p, q, l = 1;
		AdjPxl *N = NULL;
		int *FIFO = NULL;
		int first = 0, last = 0;

		sz     = FrameSize(A);
		fimg   = add_frame(img, sz, INT_MIN);
		flabel = create_image32(fimg->ncols, fimg->nrows);
		N      = adj_pixels(fimg->ncols, A);
		n      = fimg->ncols * fimg->nrows;
		FIFO   = AllocIntArray(n);
		for(j = 0; j < n; j++)
		{
			if((flabel->val[j] == 0) && (fimg->val[j] != INT_MIN))
			{
				flabel->val[j] = l;
				FIFO[last] = j;
				last++;
				while(first != last)
				{
					p = FIFO[first];
					first++;
					for(i = 1; i < N->n; i++)
					{
						q = p + N->dp[i];
						if((fimg->val[q] != INT_MIN) && (fabs(fimg->val[q] - fimg->val[p]) <= thres) && (flabel->val[q] == 0))
						{
							flabel->val[q] = flabel->val[p];
							FIFO[last] = q;
							last++;
						}
					}
				}
				l++;
				first = last = 0;
			}
		}

		label = rem_frame(flabel, sz);
		DestroyAdjPxl(&N);
		destroy_image(&fimg);
		destroy_image(&flabel);
		free(FIFO);

		return(label);
	}

	Image8 *remove_region_by_area(Image8 *label, int area)
	{
		Curve *label_area;
		GQueue *Q;
		int p, q, i, n, Lmax;
		Pixel u, v;
		AdjRel *A;
		int   *root;
		Image8 *rlabel8;
		Image32 *rlabel;
		Image32 *label32 = usis::cast_2_image32(label);
		char need_removal;

		A      = circular(1.0);
		rlabel = label_comp(label32, A, 0);
		label_area = histogram(rlabel);
		n    = label->ncols * label->nrows;

		for(p = 0; p < n; p++)
			rlabel->val[p] = (int)label_area->Y[rlabel->val[p]];

		destroy_curve(&label_area);

		Lmax = maximum_value(rlabel);
		root        = AllocIntArray(n);
		Q = CreateGQueue(Lmax + 1, n, rlabel->val);
		SetRemovalPolicy(Q, MAXVALUE);

		need_removal = 0;

		for(p = 0; p < n; p++)
		{
			if(rlabel->val[p] >= area)
			{
				root[p] = p;
				InsertGQueue(&Q, p);
			}
			else
			{
				need_removal = 1;
				root[p] = NIL;
				rlabel->val[p] = 0;
			}
		}

		if(need_removal == 0) // no need to eliminate regions
		{
			DestroyGQueue(&Q);
			destroy_adjrel(&A);
			free(root);
			destroy_image(&rlabel);
			destroy_image(&label32);

			return usis::copy_image8(label);
		}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);
			u.x = p % label->ncols;
			u.y = p / label->ncols;

			for(i = 1; i < A->n; i++)
			{
				v.x = u.x + A->dx[i];
				v.y = u.y + A->dy[i];

				if(valid_pixel(label, v))
				{
					q = to_index(label, v);

					if(rlabel->val[q] == 0)
					{
						root[q]       = root[p];
						rlabel->val[q] = rlabel->val[p];
						InsertGQueue(&Q, q);
					}
				}
			}
		}

		DestroyGQueue(&Q);
		destroy_adjrel(&A);

		// Copy label
		for(p = 0; p < n; p++)
			if(root[p] != p)
			{
				rlabel->val[p] = label->val[root[p]];
			}
			else
			{
				rlabel->val[p] = label->val[p];
			}

		free(root);

		rlabel8 = usis::cast_2_image8(rlabel);

		destroy_image(&rlabel);
		destroy_image(&label32);

		return rlabel8;
	}


	Image32 *remove_region_by_area(Image32 *label, int area)
	{
		Curve *label_area;
		GQueue *Q;
		int p, q, i, n, Lmax;
		Pixel u, v;
		AdjRel *A;
		int   *root;
		Image32 *rlabel;
		char need_removal;

		A      = circular(1.0);
		rlabel = label_comp(label, A, 0);
		label_area = histogram(rlabel);
		n    = label->ncols * label->nrows;

		for(p = 0; p < n; p++)
			rlabel->val[p] = (int)label_area->Y[rlabel->val[p]];

		destroy_curve(&label_area);

		Lmax = maximum_value(rlabel);
		root        = AllocIntArray(n);
		Q = CreateGQueue(Lmax + 1, n, rlabel->val);
		SetRemovalPolicy(Q, MAXVALUE);

		need_removal = 0;

		for(p = 0; p < n; p++)
		{
			if(rlabel->val[p] >= area)
			{
				root[p] = p;
				InsertGQueue(&Q, p);
			}
			else
			{
				need_removal = 1;
				root[p] = NIL;
				rlabel->val[p] = 0;
			}
		}

		if(need_removal == 0) // no need to eliminate regions
		{
			DestroyGQueue(&Q);
			destroy_adjrel(&A);
			free(root);
			destroy_image(&rlabel);

			return usis::copy_image32(label);
		}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);
			u.x = p % label->ncols;
			u.y = p / label->ncols;

			for(i = 1; i < A->n; i++)
			{
				v.x = u.x + A->dx[i];
				v.y = u.y + A->dy[i];

				if(valid_pixel(label, v))
				{
					q = to_index(label, v);

					if(rlabel->val[q] == 0)
					{
						root[q]       = root[p];
						rlabel->val[q] = rlabel->val[p];
						InsertGQueue(&Q, q);
					}
				}
			}
		}

		DestroyGQueue(&Q);
		destroy_adjrel(&A);

		// Copy label
		for(p = 0; p < n; p++)
			if(root[p] != p)
			{
				rlabel->val[p] = label->val[root[p]];
			}
			else
			{
				rlabel->val[p] = label->val[p];
			}

		free(root);

		return rlabel;
	}

	Image8 *close_holes(Image8 *img)
	{
		AdjRel *A = NULL;
		Image32 *marker = NULL;
		Image8 *cimg = NULL;
		int x, y, i, j, Imax;

		Imax   = maximum_value(img);
		marker   = create_image32(img->ncols, img->nrows);
		set_image(marker, Imax + 1);

		for(y = 0; y < marker->nrows; y++)
		{
			i = usis::to_index(marker, 0, y);
			j = usis::to_index(marker, marker->ncols - 1, y);
			marker->val[i] = img->val[i];
			marker->val[j] = img->val[j];
		}

		for(x = 0; x < marker->ncols; x++)
		{
			i = usis::to_index(marker, x, 0);
			j = usis::to_index(marker, x, marker->nrows - 1);
			marker->val[i] = img->val[i];
			marker->val[j] = img->val[j];
		}

		A      = circular(1.0);
		cimg   = sup_rec(img, marker, A);
		destroy_image(&marker);
		destroy_adjrel(&A);

		return(cimg);
	}

	Image32 *close_holes(Image32 *img)
	{
		AdjRel *A = NULL;
		Image32 *marker = NULL;
		Image32 *cimg = NULL;
		int x, y, i, j, Imax;

		Imax   = maximum_value(img);
		marker   = create_image32(img->ncols, img->nrows);
		set_image(marker, Imax + 1);

		for(y = 0; y < marker->nrows; y++)
		{
			i = usis::to_index(marker, 0, y);
			j = usis::to_index(marker, marker->ncols - 1, y);
			marker->val[i] = img->val[i];
			marker->val[j] = img->val[j];
		}

		for(x = 0; x < marker->ncols; x++)
		{
			i = usis::to_index(marker, x, 0);
			j = usis::to_index(marker, x, marker->nrows - 1);
			marker->val[i] = img->val[i];
			marker->val[j] = img->val[j];
		}

		A      = circular(1.0);
		cimg   = sup_rec(img, marker, A);
		destroy_image(&marker);
		destroy_adjrel(&A);

		return(cimg);
	}

	Image8 *sup_rec(Image8 *img, Image32 *marker, AdjRel *A)
	{
		Image32 *fcost = NULL, *cost = NULL;
		Image8 *fimg = NULL;
		GQueue *Q = NULL;
		int i, sz, p, q, tmp, n;
		AdjPxl *N = NULL;

		sz  = FrameSize(A);
		fimg = add_frame(img, sz, 0);
		fcost = add_frame(marker, sz, INT_MIN);
		N  = adj_pixels(fcost->ncols, A);
		n = fcost->ncols * fcost->nrows;
		Q = CreateGQueue(maximum_value(marker) + 1, n, fcost->val);

		for(p = 0; p < n; p++)
			if(fcost->val[p] != INT_MIN)
			{
				InsertGQueue(&Q, p);
			}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);

			for(i = 1; i < N->n; i++)
			{
				q = p + N->dp[i];

				if(fcost->val[p] < fcost->val[q])
				{
					tmp = MAX(fcost->val[p], fimg->val[q]);

					if(tmp < fcost->val[q])
					{
						if(Q->L.elem[q].color == GRAY)
							RemoveGQueueElem(Q, q);

						fcost->val[q] = tmp;
						InsertGQueue(&Q, q);
					}
				}
			}
		}

		cost = rem_frame(fcost, sz);
		Image8 *mask = cast_2_image8(cost);

		DestroyGQueue(&Q);
		destroy_image(&fimg);
		destroy_image(&fcost);
		destroy_image(&cost);
		DestroyAdjPxl(&N);

		return mask;
	}


	Image32 *sup_rec(Image32 *img, Image32 *marker, AdjRel *A)
	{
		Image32 *fcost = NULL, *cost = NULL;
		Image32 *fimg = NULL;
		GQueue *Q = NULL;
		int i, sz, p, q, tmp, n;
		AdjPxl *N = NULL;

		sz  = FrameSize(A);
		fimg = add_frame(img, sz, 0);
		fcost = add_frame(marker, sz, INT_MIN);
		N  = adj_pixels(fcost->ncols, A);
		n = fcost->ncols * fcost->nrows;
		Q = CreateGQueue(maximum_value(marker) + 1, n, fcost->val);

		for(p = 0; p < n; p++)
			if(fcost->val[p] != INT_MIN)
			{
				InsertGQueue(&Q, p);
			}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);

			for(i = 1; i < N->n; i++)
			{
				q = p + N->dp[i];

				if(fcost->val[p] < fcost->val[q])
				{
					tmp = MAX(fcost->val[p], fimg->val[q]);

					if(tmp < fcost->val[q])
					{
						if(Q->L.elem[q].color == GRAY)
							RemoveGQueueElem(Q, q);

						fcost->val[q] = tmp;
						InsertGQueue(&Q, q);
					}
				}
			}
		}

		cost = rem_frame(fcost, sz);

		DestroyGQueue(&Q);
		destroy_image(&fimg);
		destroy_image(&fcost);
		DestroyAdjPxl(&N);

		return cost;
	}

	Image8 *inf_rec(Image8 *img, Image32 *marker, AdjRel *A)
	{
		Image32 *fcost = NULL, *cost = NULL;
		Image8 *fimg = NULL;
		GQueue *Q = NULL;
		int i, sz, p, q, tmp, n, Imax;
		AdjPxl *N = NULL;

		sz    = FrameSize(A);
		fimg  = add_frame(img, sz, UCHAR_MAX);
		fcost = add_frame(marker, sz, INT_MAX);
		N     = adj_pixels(fcost->ncols, A);
		n     = fcost->ncols * fcost->nrows;
		Imax  = maximum_value(img);
		Q     = CreateGQueue(Imax + 1, n, fcost->val);

		SetRemovalPolicy(Q, MAXVALUE);

		for(p = 0; p < n; p++)
			if(fcost->val[p] != INT_MAX)
			{
				InsertGQueue(&Q, p);
			}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);
			for(i = 1; i < N->n; i++)
			{
				q = p + N->dp[i];
				if(fcost->val[p] > fcost->val[q])
				{
					tmp = MIN(fcost->val[p], fimg->val[q]);
					if(tmp > fcost->val[q])
					{
						UpdateGQueue(&Q, q, tmp);
						fcost->val[q] = tmp;
					}
				}
			}
		}

		cost  = rem_frame(fcost, sz);

		Image8 *mask = cast_2_image8(cost);

		DestroyGQueue(&Q);
		destroy_image(&fimg);
		destroy_image(&fcost);
		destroy_image(&cost);
		DestroyAdjPxl(&N);

		return(mask);
	}


	Image32 *inf_rec(Image32 *img, Image32 *marker, AdjRel *A)
	{
		Image32 *fcost = NULL, *cost = NULL;
		Image32 *fimg = NULL;
		GQueue *Q = NULL;
		int i, sz, p, q, tmp, n, Imax;
		AdjPxl *N = NULL;

		sz    = FrameSize(A);
		fimg  = add_frame(img, sz, INT_MAX);
		fcost = add_frame(marker, sz, INT_MAX);
		N     = adj_pixels(fcost->ncols, A);
		n     = fcost->ncols * fcost->nrows;
		Imax  = maximum_value(img);
		Q     = CreateGQueue(Imax + 1, n, fcost->val);

		SetRemovalPolicy(Q, MAXVALUE);

		for(p = 0; p < n; p++)
			if(fcost->val[p] != INT_MAX)
			{
				InsertGQueue(&Q, p);
			}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);
			for(i = 1; i < N->n; i++)
			{
				q = p + N->dp[i];
				if(fcost->val[p] > fcost->val[q])
				{
					tmp = MIN(fcost->val[p], fimg->val[q]);
					if(tmp > fcost->val[q])
					{
						UpdateGQueue(&Q, q, tmp);
						fcost->val[q] = tmp;
					}
				}
			}
		}

		cost  = rem_frame(fcost, sz);

		DestroyGQueue(&Q);
		destroy_image(&fimg);
		destroy_image(&fcost);

		DestroyAdjPxl(&N);

		return cost;
	}


	Image8 *close_label_holes(Image8 *label, Image32 *root, int deflabel)
	{
		AdjRel *A = NULL;
		Image32 *marker = NULL;
		Image8 *cimg = NULL;
		int x, y, i, j, Imax;

		Imax   = maximum_value(label);
		marker   = create_image32(label->ncols, label->nrows);
		set_image(marker, Imax + 1);

		for(y = 0; y < marker->nrows; y++)
		{
			i = to_index(marker, 0, y);;
			j = to_index(marker, marker->ncols - 1, y);
			marker->val[i] = label->val[i];
			marker->val[j] = label->val[j];
		}

		for(x = 0; x < marker->ncols; x++)
		{
			i = to_index(marker, x, 0);
			j = to_index(marker, x, marker->nrows - 1);
			marker->val[i] = label->val[i];
			marker->val[j] = label->val[j];
		}

		A      = circular(1.0);
		cimg   = label_sup_rec(label, marker, root, deflabel, A);
		destroy_image(&marker);
		destroy_adjrel(&A);

		return(cimg);
	}

	Image8 *label_sup_rec(Image8 *label, Image32 *marker, Image32 *root, int deflabel, AdjRel *A)
	{
		Image32 *fcost = NULL, *cost = NULL, *froot = NULL;
		Image8 *flabel = NULL;

		GQueue *Q = NULL;
		int i, sz, p, q, tmp, n;
		AdjPxl *N = NULL;

		sz  = FrameSize(A);
		flabel = add_frame(label, sz, 0);
		fcost = add_frame(marker, sz, INT_MIN);
		froot = add_frame(root, sz, NIL);
		N  = adj_pixels(fcost->ncols, A);
		n = fcost->ncols * fcost->nrows;
		Q = CreateGQueue(maximum_value(marker) + 1, n, fcost->val);

		for(p = 0; p < n; p++)
			if(fcost->val[p] != INT_MIN)
			{
				InsertGQueue(&Q, p);
			}

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);

			/// if the hole is taken by deflabel
			/// sets its cost (final label) as deflabel
			/// or it will keep Imax+1 as its value
			if(froot->val[p] != NIL && label->val[froot->val[p]] == deflabel)
				fcost->val[p] = deflabel;

			for(i = 1; i < N->n; i++)
			{
				q = p + N->dp[i];

				int proceed = 0;

				/// if pixel p label is an object label
				if(flabel->val[p] != deflabel)
				{
					/// and if q's root is NIL OR is not deflabel pixel
					/// proceed to conquer
					if(froot->val[q] == NIL || label->val[froot->val[q]] != deflabel)
						proceed = 1;
				}
				else
				{
					proceed = 1;
				}

				if(fcost->val[p] < fcost->val[q] && proceed)
				{
					tmp = MAX(fcost->val[p], flabel->val[q]);

					if(tmp < fcost->val[q])
					{
						UpdateGQueue(&Q, q, tmp);
						fcost->val[q] = tmp;
					}
				}
			}
		}

		cost = rem_frame(fcost, sz);
		Image8 *final_label = cast_2_image8(cost);

		DestroyGQueue(&Q);
		destroy_image(&flabel);
		destroy_image(&fcost);
		destroy_image(&froot);
		DestroyAdjPxl(&N);

		return final_label;
	}

	Image8 *image_xor(Image8 *img0, Image8 *img1)
	{
		int p;
		Image8 *result = create_image8(img0->ncols, img0->nrows);
		for(p = 0; p < img0->ncols * img0->nrows; p++)
		{
			if(img0->val[p] == 0 && img1->val[p] > 0)
				result->val[p] = img1->val[p];
			else if(img0->val[p] > 0 && img1->val[p] == 0)
				result->val[p] = img0->val[p];
		}

		return result;
	}

/// Signed euclidean distance transform
/// For pixels where the label I->val[p] == 0
/// the EDT value will be positive
	Image32 *signed_edt2(Image8 *B, Image8 *I, Image32 **root, int thrsh)
	{
		int i;
		Forest *F = edt2(B, thrsh);

		usis::Image32 *edt2 = F->V;

		for(i = 0; i < I->ncols * I->nrows; i++)
			edt2->val[i] *= (I->val[i] > 0) ? -1 : 1;

		F->V = NULL;

		if(root != NULL)
		{
			*root = F->R;

			F->R = NULL;
		}


		destroy_forest(&F);

		return edt2;
	}

	Image32* edt(Image8 *I, Image32 **root, int thrsh)
	{
		Image32 *edt = create_image32(I->ncols, I->nrows);

		edt_inplace(I,edt, root, thrsh);

		return edt;
	}

	void edt_inplace(Image8 *I, Image32 *edt, Image32 **root, int thrsh)
	{
	    int i;
		Forest *F = edt2_inplace(I, edt, thrsh);

		for(i = 0; i < edt->ncols * edt->nrows; i++)
			edt->val[i] = (int)sqrt((double)edt->val[i]);

		F->V = NULL;

		if(root != NULL)
		{
			*root = F->R;

			F->R = NULL;
		}

		destroy_forest(&F);
	}

	DbImage* signed_edtdb(Image8 *B, Image8 *I, Image32 **root, int thrsh)
	{
		DbImage *sgn_edt2 = create_dbimage(I->ncols, I->nrows);
		Image32 *sgn_edt2_int = signed_edt2(B, I, root, thrsh);

		for(int i = 0; i < sgn_edt2->n; i++)
		{
			int val = sgn_edt2_int->val[i];
			sgn_edt2->val[i] = (val >= 0) ? sqrt((double)val) :
											-sqrt((double) -val);
		}

		destroy_image(&sgn_edt2_int);

		return sgn_edt2;
	}


	DbImage* edtdb(Image8 *I, Image32 **root, int thrsh)
	{
		DbImage *edt = create_dbimage(I->ncols, I->nrows);

		edt_inplace(I,edt, root, thrsh);

		return edt;
	}

	void edt_inplace(Image8 *I, DbImage *edt, Image32 **root, int thrsh)
	{
	    int i;
		Forest *F = edt2(I, thrsh);

		for(i = 0; i < edt->n; i++)
			edt->val[i] = sqrt((double)F->V->val[i]);

		if(root != NULL)
		{
			*root = F->R;

			F->R = NULL;
		}


		destroy_forest(&F);
	}

	Image32 *edt(Image32 *I, Image32 **root, int thrsh)
	{
		Image32 *edt = create_image32(I->ncols, I->nrows);

		edt_inplace(I,edt, root, thrsh);

		return edt;
	}

	void edt_inplace(Image32 *I, Image32 *edt, Image32 **root, int thrsh)
	{
	    int i;
		Forest *F = edt2_inplace(I, edt, thrsh);

		for(i = 0; i < edt->n; i++)
			edt->val[i] = (int)sqrt((double)edt->val[i]);

		F->V = NULL;

		if(root != NULL)
		{
			*root = F->R;

			F->R = NULL;
		}

		destroy_forest(&F);
	}

	Image32 *signed_edt(Image8 *B, Image8 *I, Image32 **root, int thrsh)
    {
        int i;
		Image32 *sgn_edt2 = signed_edt2(B, I, root, thrsh);

		for(i = 0; i < sgn_edt2->n; i++)
			sgn_edt2->val[i] = (sgn_edt2->val[i] >= 0) ? (int)sqrt((double)sgn_edt2->val[i]) :
                                                        -(int)sqrt((double) - sgn_edt2->val[i]);
		return sgn_edt2;
	}


// Euclidean distance transform

	Forest *create_forest(int ncols, int nrows)
	{
		Forest *F = (Forest *)calloc(1, sizeof(Forest));

		F->P = create_image32(ncols, nrows);
		F->R = create_image32(ncols, nrows);
		F->V = create_image32(ncols, nrows);

		return(F);
	}

// Destroys Forest

	void destroy_forest(Forest **F)
	{
		Forest *tmp = *F;

		if(tmp != NULL)
		{
			destroy_image(&(tmp->P));
			destroy_image(&(tmp->R));
			destroy_image(&(tmp->V));
			free(tmp);
			*F = NULL;
		}
	}


	Forest *edt2(Image8 *I, int thrsh)
	{
		Image32 *edt_img = create_image32(I->ncols, I->nrows);

		return edt2_inplace(I, edt_img, thrsh);
	}

	Forest *edt2_inplace(Image8 *I, Image32 *edt, int thrsh)
	{
		int p, q, n = I->ncols * I->nrows, i, tmp;
		Pixel u, v, w;
		AdjRel *A = circular(1.5), *A4 = circular(1.0);
		Forest *F = create_forest(I->ncols, I->nrows);

		destroy_image(&F->V);
		F->V = edt;

		GQueue *Q = CreateGQueue(10240, n, F->V->val);

		// Trivial path initialization

		for(p = 0; p < n; p++)
		{
			u.x = p % I->ncols;
			u.y = p / I->ncols;
			F->V->val[p] = INT_MAX;
			F->R->val[p] = p;
			F->P->val[p] = NIL;
			if(I->val[p] > thrsh) // p belongs to an object's border
			{
				F->V->val[p] = 0;
				InsertGQueue(&Q, p);
			}
		}

		// Path propagation

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);
			u.x = p % I->ncols;
			u.y = p / I->ncols;
			w.x = F->R->val[p] % I->ncols;
			w.y = F->R->val[p] / I->ncols;
			for(i = 1; i < A->n; i++)
			{
				v.x = u.x + A->dx[i];
				v.y = u.y + A->dy[i];
				if(valid_pixel(I, v))
				{
					q   = to_index(I, v);
					if(F->V->val[q] > F->V->val[p])
					{
						tmp = (v.x - w.x) * (v.x - w.x) + (v.y - w.y) * (v.y - w.y);
						if(tmp < F->V->val[q])
						{
							if(F->V->val[q] != INT_MAX) RemoveGQueueElem(Q, q);
							F->V->val[q] = tmp;
							F->R->val[q] = F->R->val[p];
							F->P->val[q] = p;
							InsertGQueue(&Q, q);
						}
					}
				}
			}
		}

		DestroyGQueue(&Q);
		destroy_adjrel(&A);
		destroy_adjrel(&A4);

		return(F);
	}

	Forest *edt2(Image32 *I, int thrsh)
	{
		Image32 *edt = create_image32(I->ncols, I->nrows);

		return edt2_inplace(I, edt, thrsh);
	}

	Forest *edt2_inplace(Image32 *I, Image32 *edt, int thrsh)
	{
		int p, q, n = I->ncols * I->nrows, i, tmp;
		Pixel u, v, w;
		AdjRel *A = circular(1.5), *A4 = circular(1.0);
		Forest *F = create_forest(I->ncols, I->nrows);

		destroy_image(&F->V);
		F->V = edt;

		GQueue *Q = CreateGQueue(10240, n, F->V->val);

		// Trivial path initialization

		for(p = 0; p < n; p++)
		{
			u.x = p % I->ncols;
			u.y = p / I->ncols;
			F->V->val[p] = INT_MAX;
			F->R->val[p] = p;
			F->P->val[p] = NIL;
			if(I->val[p] > thrsh) // p belongs to an object's border
			{
				F->V->val[p] = 0;
				InsertGQueue(&Q, p);
			}
		}

		// Path propagation

		while(!EmptyGQueue(Q))
		{
			p = RemoveGQueue(Q);
			u.x = p % I->ncols;
			u.y = p / I->ncols;
			w.x = F->R->val[p] % I->ncols;
			w.y = F->R->val[p] / I->ncols;
			for(i = 1; i < A->n; i++)
			{
				v.x = u.x + A->dx[i];
				v.y = u.y + A->dy[i];
				if(valid_pixel(I, v))
				{
					q   = to_index(I, v);
					if(F->V->val[q] > F->V->val[p])
					{
						tmp = (v.x - w.x) * (v.x - w.x) + (v.y - w.y) * (v.y - w.y);
						if(tmp < F->V->val[q])
						{
							if(F->V->val[q] != INT_MAX) RemoveGQueueElem(Q, q);
							F->V->val[q] = tmp;
							F->R->val[q] = F->R->val[p];
							F->P->val[q] = p;
							InsertGQueue(&Q, q);
						}
					}
				}
			}
		}

		DestroyGQueue(&Q);
		destroy_adjrel(&A);
		destroy_adjrel(&A4);

		return(F);
	}


bool valid_cont_point(Image8 *bin, AdjRel *L, AdjRel *R, int p, int thrsh)
{
    int i,q,n,left,right;
    Pixel u,v,l,r;
    bool found=false;

    u.x = p%bin->ncols;
    u.y = p/bin->ncols;
    n   = L->n;

    for (i=0; i < n; i++)
    {
        v.x = u.x + L->dx[i];
        v.y = u.y + L->dy[i];
        if (valid_pixel(bin,v.x,v.y))
        {
            q = to_index(bin, v);
            if ((bin->val[q] > thrsh)&&(p!=q))
            {
                l.x = u.x + L->dx[i];
                l.y = u.y + L->dy[i];
                r.x = u.x + R->dx[i];
                r.y = u.y + R->dy[i];
                if (valid_pixel(bin,l.x,l.y))
                    left = to_index(bin,l);
                else
                    left = -1;
                if (valid_pixel(bin,r.x,r.y))
                    right = to_index(bin,r);
                else
                    right = -1;
                if (((left!=-1)&&(right!=-1)&&(bin->val[left] != bin->val[right]))||
                        ((left==-1)&&(right!=-1)&&(bin->val[right] > thrsh)) ||
                        ((right==-1)&&(left!=-1)&&(bin->val[left] > thrsh)))
                {
                    found = true;
                    break;
                }
            }
        }
    }

    return(found);
}



	Image32 *lambda_contour(Image8 *bin)
	{
		Image8 *bndr = NULL, *color = NULL;
		Image32 *pred = NULL, *lambda = NULL;
		int p = 0, q, r, i, j, left = 0, right = 0, n, *LIFO, last, l = 1;
		AdjRel *A, *L, *R;
		Pixel u, v, w;

		A     = circular(1.0);
		n     = bin->ncols * bin->nrows;
		bndr  = create_image8(bin->ncols, bin->nrows);
		for(p = 0; p < n; p++)
		{
			if(bin->val[p] == 1)
			{
				u.x = p % bin->ncols;
				u.y = p / bin->ncols;
				for(i = 1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];
					if(valid_pixel(bin, v))
					{
						q = to_index(bin, v);
						if(bin->val[q] == 0)
						{
							bndr->val[p] = 1;
							break;
						}
					}
					else
					{
						bndr->val[p] = 1;
						break;
					}
				}
			}
		}
		destroy_adjrel(&A);

		A      = circular(1.5);
		L      = left_side(A);
		R      = right_side(A);
		lambda = create_image32(bndr->ncols, bndr->nrows);
		color  = create_image8(bndr->ncols, bndr->nrows);
		pred   = create_image32(bndr->ncols, bndr->nrows);
		LIFO   = AllocIntArray(n);
		last   = NIL;
		for(j = 0; j < n; j++)
		{
			if((bndr->val[j] == 1) &&
			        (color->val[j] != BLACK) &&
			        valid_cont_point(bin, L, R, j, 0))
			{
				last++;
				LIFO[last]    = j;
				color->val[j] = GRAY;
				pred->val[j] = j;
				while(last != NIL)
				{
					p = LIFO[last];
					last--;
					color->val[p] = BLACK;
					u.x = p % bndr->ncols;
					u.y = p / bndr->ncols;
					for(i = 1; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];
						if(valid_pixel(bndr, v))
						{
							q = to_index(bndr, v);
							if((q == j) && (pred->val[p] != j))
							{
								last = NIL;
								break;
							}
							w.x = u.x + L->dx[i];
							w.y = u.y + L->dy[i];
							if(valid_pixel(bndr, w))
								left = to_index(bndr, w);
							else
								left = -1;
							w.x = u.x + R->dx[i];
							w.y = u.y + R->dy[i];
							if(valid_pixel(bndr, w))
								right = to_index(bndr, w);
							else
								right = -1;

							if((bndr->val[q] == 1) &&
							        (color->val[q] != BLACK) &&
							        (((left != -1) && (right != -1) && (bin->val[left] != bin->val[right])) ||
							         ((left == -1) && (right != -1) && (bin->val[right] == 1)) ||
							         ((right == -1) && (left != -1) && (bin->val[left] == 1))))
							{
								pred->val[q] = p;
								if(color->val[q] == WHITE)
								{
									last++;
									LIFO[last] = q;
									color->val[q] = GRAY;
								}
							}
						}
					}
				}
				r = p;
				while(pred->val[p] != p)
				{
					lambda->val[p] = l;
					p = pred->val[p];
				}
				if(r != p)
				{
					lambda->val[p] = l;
					l++;
				}
			}
		}

		destroy_adjrel(&A);
		destroy_adjrel(&L);
		destroy_adjrel(&R);
		destroy_image(&bndr);
		destroy_image(&color);
		destroy_image(&pred);
		free(LIFO);


		return(lambda);
	}

// Assigs the geodesic length with respect to an arbitrary reference point on the contour

	DbImage *lambda_geodesic_length(Image8 *bin)
	{
		Image8 *bndr = NULL, *color = NULL;
		Image32 *pred = NULL;
		DbImage *lambda = NULL;
		int p = 0, q, r, i, j, n, left = 0, right = 0, *LIFO, last;
		double l;
		AdjRel *A, *L, *R;
		Pixel u, v, w;

		A     = circular(1.0);
		n     = bin->ncols * bin->nrows;
		bndr  = create_image8(bin->ncols, bin->nrows);
		for(p = 0; p < n; p++)
		{
			if(bin->val[p] == 1)
			{
				u.x = p % bin->ncols;
				u.y = p / bin->ncols;
				for(i = 1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];
					if(valid_pixel(bin, v))
					{
						q = to_index(bin, v);
						if(bin->val[q] == 0)
						{
							bndr->val[p] = 1;
							break;
						}
					}
					else
					{
						bndr->val[p] = 1;
						break;
					}
				}
			}
		}
		destroy_adjrel(&A);

		A      = circular(1.5);
		L      = left_side(A);
		R      = right_side(A);
		lambda = create_dbimage(bndr->ncols, bndr->nrows);
		color  = create_image8(bndr->ncols, bndr->nrows);
		pred   = create_image32(bndr->ncols, bndr->nrows);
		n      = bndr->ncols * bndr->nrows;
		LIFO   = AllocIntArray(n);
		last   = NIL;
		for(j = 0; j < n; j++)
		{
			if((bndr->val[j] == 1)
			        && (color->val[j] != BLACK)
			        && valid_cont_point(bin, L, R, j, 0))
			{
				last++;
				LIFO[last]    = j;
				color->val[j] = GRAY;
				pred->val[j] = j;
				while(last != NIL)
				{
					p = LIFO[last];
					last--;
					color->val[p] = BLACK;
					u.x = p % bndr->ncols;
					u.y = p / bndr->ncols;
					for(i = 1; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];
						if(valid_pixel(bndr, v))
						{
							q = to_index(bndr, v);
							if((q == j) && (pred->val[p] != j))
							{
								last = NIL;
								break;
							}

							w.x = u.x + L->dx[i];
							w.y = u.y + L->dy[i];
							if(valid_pixel(bndr, w))
								left = to_index(bndr, w);
							else
								left = -1;
							w.x = u.x + R->dx[i];
							w.y = u.y + R->dy[i];
							if(valid_pixel(bndr, w))
								right = to_index(bndr, w);
							else
								right = -1;

							if((bndr->val[q] == 1) &&
							        (color->val[q] != BLACK) &&
							        (((left != -1) && (right != -1) && (bin->val[left] != bin->val[right])) ||
							         ((left == -1) && (right != -1) && (bin->val[right] == 1)) ||
							         ((right == -1) && (left != -1) && (bin->val[left] == 1))))
							{
								pred->val[q] = p;
								if(color->val[q] == WHITE)
								{
									last++;
									LIFO[last] = q;
									color->val[q] = GRAY;
								}
							}
						}
					}
				}
				r = p;
				l = 1.0;
				while(pred->val[p] != p)
				{
					lambda->val[p] = l;
					u.x = p % bndr->ncols;
					u.y = p / bndr->ncols;
					v.x = pred->val[p] % bndr->ncols;
					v.y = pred->val[p] / bndr->ncols;
					l  += sqrt((u.x - v.x) * (u.x - v.x) + (u.y - v.y) * (u.y - v.y));
					p   = pred->val[p];
				}

				if(r != p)
				{
					lambda->val[p] = l;
				}

			}
		}


		destroy_adjrel(&A);
		destroy_adjrel(&L);
		destroy_adjrel(&R);
		destroy_image(&bndr);
		destroy_image(&color);
		destroy_image(&pred);
		free(LIFO);
		return(lambda);
	}


// Computes the geodesic perimeter of each contour

	DbImage *geodesic_perimeter(Image8 *bin)
	{
		Image8 *bndr = NULL, *color = NULL;
		Image32 *pred = NULL;
		DbImage *perim = NULL;
		int p = 0, q, r, i, j, n, left = 0, right = 0, *LIFO, last;
		double l;
		AdjRel *A, *L, *R;
		Pixel u, v, w;

		A     = circular(1.0);
		n     = bin->ncols * bin->nrows;
		bndr  = create_image8(bin->ncols, bin->nrows);
		for(p = 0; p < n; p++)
		{
			if(bin->val[p] == 1)
			{
				u.x = p % bin->ncols;
				u.y = p / bin->ncols;
				for(i = 1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];
					if(valid_pixel(bin, v))
					{
						q = to_index(bin, v);
						if(bin->val[q] == 0)
						{
							bndr->val[p] = 1;
							break;
						}
					}
					else
					{
						bndr->val[p] = 1;
						break;
					}
				}
			}
		}
		destroy_adjrel(&A);

		A      = circular(1.5);
		L      = left_side(A);
		R      = right_side(A);
		perim  = create_dbimage(bndr->ncols, bndr->nrows);
		color  = create_image8(bndr->ncols, bndr->nrows);
		pred   = create_image32(bndr->ncols, bndr->nrows);
		n      = bndr->ncols * bndr->nrows;
		LIFO   = AllocIntArray(n);
		last   = NIL;
		for(j = 0; j < n; j++)
		{
			if((bndr->val[j] == 1)
			        && (color->val[j] != BLACK)
			        && valid_cont_point(bin, L, R, j, 0))
			{
				last++;
				LIFO[last]    = j;
				color->val[j] = GRAY;
				pred->val[j] = j;
				while(last != NIL)
				{
					p = LIFO[last];
					last--;
					color->val[p] = BLACK;
					u.x = p % bndr->ncols;
					u.y = p / bndr->ncols;
					for(i = 1; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];
						if(valid_pixel(bndr, v))
						{
							q = to_index(bndr, v);
							if((q == j) && (pred->val[p] != j))
							{
								last = NIL;
								break;
							}

							w.x = u.x + L->dx[i];
							w.y = u.y + L->dy[i];
							if(valid_pixel(bndr, w))
								left = to_index(bndr, w);
							else
								left = -1;
							w.x = u.x + R->dx[i];
							w.y = u.y + R->dy[i];
							if(valid_pixel(bndr, w))
								right = to_index(bndr, w);
							else
								right = -1;

							if((bndr->val[q] == 1) &&
							        (color->val[q] != BLACK) &&
							        (((left != -1) && (right != -1) && (bin->val[left] != bin->val[right])) ||
							         ((left == -1) && (right != -1) && (bin->val[right] == 1)) ||
							         ((right == -1) && (left != -1) && (bin->val[left] == 1))))
							{
								pred->val[q] = p;
								if(color->val[q] == WHITE)
								{
									last++;
									LIFO[last] = q;
									color->val[q] = GRAY;
								}
							}
						}
					}
				}
				r = p;
				l = 1.0;
				while(pred->val[p] != p)
				{
					u.x = p % bndr->ncols;
					u.y = p / bndr->ncols;
					v.x = pred->val[p] % bndr->ncols;
					v.y = pred->val[p] / bndr->ncols;
					l  += sqrt((u.x - v.x) * (u.x - v.x) + (u.y - v.y) * (u.y - v.y));
					p   = pred->val[p];
				}
				p = r;
				r = p;
				while(pred->val[p] != p)
				{
					perim->val[p] = l;
					p   = pred->val[p];
				}
				if(r != p)
				{
					perim->val[p] = l;
				}
			}
		}


		destroy_adjrel(&A);
		destroy_adjrel(&L);
		destroy_adjrel(&R);
		destroy_image(&bndr);
		destroy_image(&color);
		destroy_image(&pred);
		free(LIFO);
		return(perim);
	}


// Computes multiscale geodesic skeleton

	DbImage *ms_geodesic_skel(Image8 *I, Image32 **distances)
	{
		int i, p, q, n, maxd1, d1;
		float maxd2, d2, MaxD;
		Pixel u, v;
		Image32  *cont = NULL;
		DbImage *label = NULL, *perim = NULL, *msskel = NULL;
		AdjRel *A;
		Forest *F = NULL;

		/* Compute MS Skeletons */

		cont   = lambda_contour(I);
		F      = edt2(cont); // Euclidean distance transform of the contours
		label  = lambda_geodesic_length(I);
		n      = I->ncols * I->nrows;
		perim  = geodesic_perimeter(I);

		A      = circular(1.0);
		msskel = create_dbimage(I->ncols, I->nrows);


		MaxD = INT_MIN;
		for(p = 0; p < n; p++)
		{
			if(F->R->val[p] != p)    // avoid computation on the contours
			{
				u.x = p % I->ncols;
				u.y = p / I->ncols;
				maxd1 = INT_MIN;
				maxd2 = INT_MIN;
				for(i = 1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];
					if(valid_pixel(I, v))
					{
						q = to_index(I, v);
						if(cont->val[F->R->val[p]] == cont->val[F->R->val[q]])
						{
							d2   = label->val[F->R->val[q]] - label->val[F->R->val[p]];
							if(d2 > (perim->val[F->R->val[p]] - d2))
							{
								d2 = (perim->val[F->R->val[p]] - d2);
							}
							if(d2 > maxd2)
							{
								maxd2 = d2;
							}
						}
						else
						{
							d1 = cont->val[F->R->val[q]] - cont->val[F->R->val[p]];
							if(d1 > maxd1)
								maxd1 = d1;
						}
					}
				}
				if(maxd1 > 0)
				{
					msskel->val[p] = INT_MAX;
				}
				else
				{
					msskel->val[p] = MAX(maxd2, 0);
					if(msskel->val[p] > MaxD)
					{
						MaxD = msskel->val[p];
					}
				}
			}
		}

		for(p = 0; p < n; p++)  /* Set the SKIZ */
		{
			if(msskel->val[p] == INT_MAX)
				msskel->val[p] = MaxD + 1;
		}

		destroy_image(&cont);
		destroy_dbimage(&perim);
		destroy_dbimage(&label);
		destroy_adjrel(&A);

		if(distances != NULL)
		{
			*distances = F->V;
			F->V = NULL;
		}

		destroy_forest(&F);

		return(msskel);
	}


	MedialAxis medial_axis_from_skiz(DbImage *skiz, Image32 *eucl_distance2,
									Image8 *label, int lb, double thresh)
	{
		MedialAxis axis;
		double maxval = maximum_value(skiz);

		for(int p = 0; p < skiz->n; p++)
		{
			if((*skiz)[p] >= thresh*maxval)
			{
				if(lb < 0 || (label != NULL && (*label)[p] == lb))
				{
					MedialAxisData data;
					data.lb = (lb <= 0) ? 1 : lb;
					data.dist = sqrt((double)(*eucl_distance2)[p]);
					axis[p] = data;
				}
			}
		}

		return axis;
	}

	void dilate_medial_axis(MedialAxis axis, Image8 *dil_label,	double dist_perc)
	{
		MedialAxis::iterator it;

		for(it = axis.begin(); it != axis.end(); it++)
		{
			AdjRel *A = fast_circular(it->second.dist*dist_perc);
			Pixel px = to_pixel(dil_label, it->first);
			Pixel px_0 = px;
			int i = 0;

			px = next_adjacent(dil_label, A, px_0, i);

			while(px.x != NIL && px.y != NIL)
			{
				int p = to_index(dil_label, px);

				if((*dil_label)[p] <= 0)
					(*dil_label)[p] = it->second.lb;

				px = next_adjacent(dil_label, A, px_0, i);
			}

			destroy_adjrel(&A);
		}
	}

//	void fast_dilate_medial_axis(MedialAxis axis, Image8 *dil_label, double dist_perc)
//	{
//		for(int p = 0; p < dil_label->n; p++)
//		{
//			int root_p = (*root)[p];
//
//			if(axis.find(root_p) != axis.end())
//			{
//				double dist = sqrt((*distances)[p]);
//
//				if(dist <= dist_perc*axis[root_p].dist)
//				{
//					(*dil_label)[p] = axis[root_p].lb;
//				}
//			}
//		}
//	}


	double density(int p, usis::Image8 *mask, int lb, AdjRel *A)
	{
		int i;
		double dens = 0.0, nvalid_pixels = 0.0;
		Pixel px = usis::to_pixel(mask, p);

		for(i = 0; i < A->n; i++)
		{
			Pixel px_adj = (Pixel)
			{
				px.x + A->dx[i], px.y + A->dy[i]
			};

			if(usis::valid_pixel(mask, px_adj))
			{
				if(mask->val[usis::to_index(mask, px_adj)] == lb)
					dens++;

				nvalid_pixels++;
			}
		}

		return dens / MAX(nvalid_pixels, 1.0);
	}

	std::tr1::unordered_map<int, std::pair<double, double> > avg_density(usis::Image8 *mask, AdjRel *A)
	{
		int p;

		std::tr1::unordered_map<int, std::pair<double, double> > avg_dens;
		std::tr1::unordered_map<int, std::pair<double, double> >::iterator it;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			int lb = mask->val[p];

			if(lb > 0)
			{

				if(avg_dens.find(lb) == avg_dens.end())
					avg_dens[lb] = std::make_pair(0.0, 0.0);

				avg_dens[lb].first += density(p, mask, lb, A);
				avg_dens[lb].second++;
			}
		}

		for(it = avg_dens.begin(); it != avg_dens.end(); it++)
			it->second.first = it->second.first / MAX(it->second.second, 1.0);

		return avg_dens;
	}

	double avg_density(usis::Image8 *mask, int lb, AdjRel *A)
	{
		int p;

		double avg_dens = 0.0, npixels = 0.0;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			if(mask->val[p] == lb)
			{
				avg_dens += density(p, mask, lb, A);

				npixels++;
			}
		}

		return avg_dens / MAX(npixels, 1.0);
	}

	usis::Image8 *filter_by_density(usis::Image8 *mask, AdjRel *A)
	{
		int p;
		std::tr1::unordered_map<int, std::pair<double, double> > avg_dens = avg_density(mask, A);

		usis::Image8 *filtered = usis::create_image8(mask->ncols, mask->nrows);

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			int lb = mask->val[p];
			if(density(p, mask, lb, A) >= avg_dens[lb].first)
				filtered->val[p] = lb;
		}

		return filtered;
	}

	usis::Image8 *filter_by_density(usis::Image8 *mask, int lb, AdjRel *A)
	{
		int p;
		double avg_dens = avg_density(mask, lb, A);

		usis::Image8 *filtered = usis::create_image8(mask->ncols, mask->nrows);

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			if(density(p, mask, lb, A) >= avg_dens)
				filtered->val[p] = lb;
		}

		return filtered;
	}


}

