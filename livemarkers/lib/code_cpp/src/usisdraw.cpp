#include "usisdraw.h"

namespace usis
{
void blend_draw_image(Image8 *img, Image8 *draw, Image8 *alpha)
{
    int p, n;

    n = img->ncols * img->nrows;

    for (p = 0; p < n; p++)
    {
        float a, bkg, fg;
        a = ((float)alpha->val[p]) / UCHAR_MAX;

        bkg = (float)img->val[p];
        fg = (float)draw->val[p];

        img->val[p] = (uchar)(fg * a + bkg * (1.0 - a));
    }
}


void blend_draw_cimage(CImage8 *bkg, CImage8 *fg, Image8 *alpha)
{
    blend_draw_image(bkg->C[0], fg->C[0], alpha);
    blend_draw_image(bkg->C[1], fg->C[1], alpha);
    blend_draw_image(bkg->C[2], fg->C[2], alpha);
}


void blend_draw_sub_image(Image8 *bkg, Image8 *fg, Image8 *alpha, int x, int y)
{
    int i, j, p, q;

    q = 0;

    for (i = 0; i < fg->nrows; i++)
    {
        for (j = 0; j < fg->ncols; j++)
        {
            if (valid_pixel(bkg, x + j, i + y))
            {
                p = x+j + (i+y)*bkg->ncols;
                float a = ((float)alpha->val[x+y*alpha->ncols]) / UCHAR_MAX;
                bkg->val[p] = (uchar)(bkg->val[p] * (1.0 - a) + fg->val[q] * a);
            }

            q++;
        }
    }
}

void blend_draw_sub_cimage(CImage8 *bkg, CImage8 *fg, Image8 *alpha, int x, int y)
{
    blend_draw_sub_image(bkg->C[0], fg->C[0], alpha, x, y);
    blend_draw_sub_image(bkg->C[1], fg->C[1], alpha, x, y);
    blend_draw_sub_image(bkg->C[2], fg->C[2], alpha, x, y);
}

void draw_circumference(CImage8 *draw, int c, float r, CImage8::triplet_type color)
{
    draw_circumference(draw->C[0], c, r, t0(color));
    draw_circumference(draw->C[1], c, r, t1(color));
    draw_circumference(draw->C[2], c, r, t2(color));
}

void draw_circumference(Image8 *draw, int c, float r, Image8::value_type color)
{
    int dx, dy, ncols, ri;
    float d;
    Pixel C;

    ri = ROUND(r);
    ncols = draw->ncols;

    C.x = c % ncols;
    C.y = c / ncols;

    for (dy = -ri; dy <= ri; dy++)
    {
        for (dx = -ri; dx <= ri; dx++)
        {
            d = dx * dx + dy * dy;

            if (d >= (r - 0.5)*(r - 0.5) && d < (r + 0.5)*(r + 0.5))
            {
                if (valid_pixel(draw,dx + C.x, dy + C.y))
                {
                    draw->val[to_index(draw,dx+C.x,dy+C.y)] = color;
                }
            }
        }
    }
}


void draw_cimage_circle(CImage8 *draw, int c, float r, CImage8::triplet_type color)
{
    draw_image_circle(draw->C[0], c, r, t0(color));
    draw_image_circle(draw->C[1], c, r, t1(color));
    draw_image_circle(draw->C[2], c, r, t2(color));
}

void draw_image_circle(Image8 *draw, int c, float r, Image8::value_type color)
{
    int dx, dy, ncols, ri;
    float d;
    Pixel C;

    ri = ROUND(r);
    ncols = draw->ncols;

    C.x = c % ncols;
    C.y = c / ncols;

    for (dy = -ri; dy <= ri; dy++)
    {
        for (dx = -ri; dx <= ri; dx++)
        {
            d = dx * dx + dy * dy;

            if (d < (r + 0.5)*(r + 0.5))
            {
                if (valid_pixel(draw,dx + C.x, dy + C.y))
                {
                    draw->val[to_index(draw,dx+C.x,dy+C.y)] = color;
                }
            }
        }
    }
}


void draw_line(Image8 *img, int x1, int y1, int xn, int yn, Image8::value_type val, float thickness)
{
    int vx, vy;
    float Dx, Dy;
    int amostras; /* numero de pontos a serem pintados */
    float m; /* coeficiente angular da reta */
    int i, j, p;
    float xk, yk;
    AdjRel *A = fast_circular(thickness);

    vx = xn - x1;
    vy = yn - y1;

    if (vx == 0)
    {
        Dx = 0.0;
        Dy = (float) SIGN(vy);
        amostras = abs(vy)+1;
    }
    else
    {
        m = ((float)vy )/((float)vx);
        if ( abs(vx) > abs(vy))
        {
            Dx = (float) SIGN(vx);
            Dy = m * Dx;
            amostras = abs(vx)+1;
        }
        else
        {
            Dy = (float) SIGN(vy);
            Dx = Dy / m;
            amostras = abs(vy)+1;
        }
    }

    xk = (float) x1;
    yk = (float) y1;
    for (i = 0; i < amostras; i++)
    {
        for(j = 0; j < A->n; j++)
        {
            float adj_xk = xk + A->dx[j], adj_yk = yk + A->dy[j];

            if (valid_pixel(img, ROUND(adj_xk), ROUND(adj_yk)) )
            {
                p = to_index(img, ROUND(adj_xk),ROUND(adj_yk));

                img->val[p] = val;
            }
        }

        xk += Dx;
        yk += Dy;
    }

    destroy_adjrel(&A);
}
void draw_line(CImage8 *img, int x1, int y1, double r, double theta, CImage8::triplet_type color, float thickness)
{
    int xn, yn;
    xn = x1 + (int)(r*cos(theta));
    yn = y1 - (int)(r*sin(theta));

    draw_line(img, x1, y1, xn, yn, color, thickness);
}

void draw_line(CImage8 *draw, int x1, int y1, int xn, int yn, CImage8::triplet_type color, float thickness)
{
    draw_line(draw->C[0], x1, y1, xn, yn, t0(color), thickness);
    draw_line(draw->C[1], x1, y1, xn, yn, t1(color), thickness);
    draw_line(draw->C[2], x1, y1, xn, yn, t2(color), thickness);
}

void draw_line(Image8 *img, int x1, int y1, double r, double theta,
				Image8::value_type val,	float thickness)
{
    int xn, yn;
    xn = x1 + (int)(r*cos(theta));
    yn = y1 - (int)(r*sin(theta));

    draw_line(img, x1, y1, xn, yn, val, thickness);
}

void draw_box(Image8 *img, int x1, int y1, int xn, int yn,
              Image8::value_type val, float thickness)
{
    draw_line(img, x1, y1, x1, yn, val, thickness);
    draw_line(img, x1, y1, xn, y1, val, thickness);
    draw_line(img, xn, y1, xn, yn, val, thickness);
    draw_line(img, x1, yn, xn, yn, val, thickness);
}

void draw_box(CImage8 *img, int x1, int y1, int xn, int yn,
              CImage8::triplet_type val, float thickness)
{
    draw_box(img->C[0], x1, y1, xn, yn, t0(val), thickness);
    draw_box(img->C[1], x1, y1, xn, yn, t1(val), thickness);
    draw_box(img->C[2], x1, y1, xn, yn, t2(val), thickness);
}

void draw_image_arrow(CImage8 *draw,
                      int p, int q,
                      float r, float w, float h,
                      CImage8::triplet_type val)
{
    draw_image_arrow(draw->C[0], p, q, r, w, h, t0(val));
    draw_image_arrow(draw->C[1], p, q, r, w, h, t1(val));
    draw_image_arrow(draw->C[2], p, q, r, w, h, t2(val));
}

void draw_image_arrow(Image8 *draw,
                      int p, int q,
                      float r, float w, float h,
                      int val)
{
    float dlm,dlx,dly,dtx,dty;
    int xc,yc,xb,yb,xq,yq,xp,yp;

    xq = q%(draw->ncols);
    yq = q/(draw->ncols);
    xp = p%(draw->ncols);
    yp = p/(draw->ncols);

    // A vector along the arc:
    dlm = hypot(xq - xp, yq - yp);
    dlx = (xq - xp)/dlm;
    dly = (yq - yp)/dlm;
    // A vector orthogonal to the arc:
    dtx = -dly;
    dty = dlx;
    // Arrowhead corners:
    xc = xq - ROUND(h*dlx + w*dtx);
    yc = yq - ROUND(h*dly + w*dty);
    xb = xq - ROUND(h*dlx - w*dtx);
    yb = yq - ROUND(h*dly - w*dty);
    // Draw arc from p to q:
    draw_line(draw, xp, yp, xq, yq, val);

    draw_line(draw, xc, xb, yc, yb, val, r);
    draw_line(draw, xb, yb, xq, yq, val, r);
    draw_line(draw, xc, yc, xq, yq, val, r);
}

}
