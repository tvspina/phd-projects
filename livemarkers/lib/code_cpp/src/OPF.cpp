/*
  Copyright (C) <2009> <Alexandre Xavier Falcão and João Paulo Papa>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  please see full copyright in COPYING file.
  -------------------------------------------------------------------------
  written by A.X. Falcão <afalcao@ic.unicamp.br> and by J.P. Papa
  <papa.joaopaulo@gmail.com>, Oct 20th 2008

  This program is a collection of functions to manage the Optimum-Path Forest (OPF)
  classifier.*/

#include "OPF.h"
#include <set>

using namespace std;

char	opf_PrecomputedDistance;
float  **opf_DistanceValue;
float *opf_FeatWeight = NULL;

opf_ArcWeightFun opf_ArcWeight = opf_EuclDist;

/*--------- Supervised OPF -------------------------------------*/
//Training function -----
void opf_OPFTraining(Subgraph *sg)
{
    int p,q, i;
    float tmp,weight;
    RealHeap *Q = NULL;
    float *pathval = NULL;

    // compute optimum prototypes
    opf_MSTPrototypes(sg);

    // initialization
    pathval = AllocFloatArray(sg->nnodes);

    Q=CreateRealHeap(sg->nnodes, pathval);

    for (p = 0; p < sg->nnodes; p++)
    {
        if (sg->node[p].status==opf_PROTOTYPE)
        {
            sg->node[p].pred   = NIL;
            pathval[p]         = 0;
            sg->node[p].label  = sg->node[p].truelabel;
            InsertRealHeap(Q, p);
        }
        else   // non-prototypes
        {
            pathval[p]  = FLT_MAX;
        }
    }

    // IFT with fmax
    i=0;
    while ( !IsEmptyRealHeap(Q) )
    {
        RemoveRealHeap(Q,&p);

        sg->ordered_list_of_nodes[i]=p;
        i++;
        sg->node[p].pathval = pathval[p];

        for (q=0; q < sg->nnodes; q++)
        {
            if (p!=q)
            {
                if (pathval[p] < pathval[q])
                {
                    if(!opf_PrecomputedDistance)
                        weight = opf_ArcWeight(sg->node[p].feat,sg->node[q].feat,sg->nfeats);
                    else
                        weight = opf_DistanceValue[sg->node[p].position][sg->node[q].position];
                    tmp  = MAX(pathval[p],weight);
                    if ( tmp < pathval[ q ] )
                    {
                        sg->node[q].pred  = p;
                        sg->node[q].label = sg->node[p].label;
                        UpdateRealHeap(Q, q, tmp);
                    }
                }
            }
        }
    }

    DestroyRealHeap( &Q );
    free( pathval );
}

//Classification function: it simply classifies samples from sg -----
void opf_OPFClassifying(Subgraph *sgtrain, Subgraph *sg)
{
    int i, j, k, l, label = -1;
    float tmp, weight, minCost;

    for (i = 0; i < sg->nnodes; i++)
    {
        j       = 0;
        k       = sgtrain->ordered_list_of_nodes[j];
        if(!opf_PrecomputedDistance)
            weight = opf_ArcWeight(sgtrain->node[k].feat,sg->node[i].feat,sg->nfeats);
        else
            weight = opf_DistanceValue[sgtrain->node[k].position][sg->node[i].position];

        minCost = MAX(sgtrain->node[k].pathval, weight);
        label   = sgtrain->node[k].label;

        while((j < sgtrain->nnodes-1)&&
                (minCost > sgtrain->node[sgtrain->ordered_list_of_nodes[j+1]].pathval))
        {

            l  = sgtrain->ordered_list_of_nodes[j+1];

            if(!opf_PrecomputedDistance)
                weight = opf_ArcWeight(sgtrain->node[l].feat,sg->node[i].feat,sg->nfeats);
            else
                weight = opf_DistanceValue[sgtrain->node[l].position][sg->node[i].position];
            tmp = MAX(sgtrain->node[l].pathval, weight);
            if(tmp < minCost)
            {
                minCost = tmp;
                label = sgtrain->node[l].label;
            }
            j++;
            k  = l;
        }
        sg->node[i].label = label;
    }
}

/*--------- UnSupervised OPF -------------------------------------*/
//Training function: it computes unsupervised training for the
//pre-computed best k.

void opf_OPFClustering(Subgraph *sg)
{
    Set *adj_i,*adj_j;
    char insert_i;
    int i,j;
    int p, q, l;
    float tmp,*pathval=NULL;
    RealHeap *Q=NULL;
    Set *Saux=NULL;

    //   Add arcs to guarantee symmetry on plateaus
    for (i=0; i < sg->nnodes; i++)
    {
        adj_i = sg->node[i].adj;
        while (adj_i != NULL)
        {
            j  = adj_i->elem;
            if (sg->node[i].dens==sg->node[j].dens)
            {
                // insert i in the adjacency of j if it is not there.
                adj_j    = sg->node[j].adj;
                insert_i = 1;
                while (adj_j != NULL)
                {
                    if (i == adj_j->elem)
                    {
                        insert_i=0;
                        break;
                    }
                    adj_j=adj_j->next;
                }
                if (insert_i)
                    InsertSet(&(sg->node[j].adj),i);
            }
            adj_i=adj_i->next;
        }
    }

    // Compute clustering

    pathval = AllocFloatArray(sg->nnodes);
    Q = CreateRealHeap(sg->nnodes, pathval);
    SetRemovalPolicyRealHeap(Q, MAXVALUE);

    for (p = 0; p < sg->nnodes; p++)
    {
        pathval[ p ] = sg->node[ p ].pathval;
        sg->node[ p ].pred  = NIL;
        sg->node[ p ].root  = p;

        InsertRealHeap(Q, p);
    }

    l = 0;
    i = 0;
    while (!IsEmptyRealHeap(Q))
    {
        RemoveRealHeap(Q,&p);
        sg->ordered_list_of_nodes[i]=p;
        i++;

        if ( sg->node[ p ].pred == NIL )
        {
            pathval[ p ] = sg->node[ p ].dens;
            sg->node[p].label=l;
            l++;
        }

        sg->node[ p ].pathval = pathval[ p ];
        for ( Saux = sg->node[ p ].adj; Saux != NULL; Saux = Saux->next )
        {
            q = Saux->elem;
            if ( Q->color[q] != BLACK )
            {
                tmp = MIN( pathval[ p ], sg->node[ q ].dens);
                if ( tmp > pathval[ q ] )
                {
                    UpdateRealHeap(Q,q,tmp);
                    sg->node[ q ].pred  = p;
                    sg->node[ q ].root  = sg->node[ p ].root;
                    sg->node[ q ].label = sg->node[ p ].label;
                }
            }
        }
    }

    sg->nlabels = l;

    DestroyRealHeap( &Q );
    free( pathval );
}

/*------------ Auxiliary functions ------------------------------ */

//Replace errors from evaluating set by non prototypes from training set
void opf_SwapErrorsbyNonPrototypes(Subgraph **sgtrain, Subgraph **sgeval,
									void *random_generator)
{
    int i, j, counter, nonprototypes = 0, nerrors = 0;

    for (i = 0; i < (*sgtrain)->nnodes; i++)
    {
        if((*sgtrain)->node[i].pred != NIL)  // non prototype
        {
            nonprototypes++;
        }
    }

    for (i = 0; i < (*sgeval)->nnodes; i++)
        if((*sgeval)->node[i].label != (*sgeval)->node[i].truelabel) nerrors++;

    for (i = 0; i < (*sgeval)->nnodes && nonprototypes >0 && nerrors > 0; i++)
    {
        if((*sgeval)->node[i].label != (*sgeval)->node[i].truelabel)
        {
            counter = nonprototypes;
            while(counter > 0)
            {
                j = random_integer(0,(*sgtrain)->nnodes-1, random_generator);
                if ((*sgtrain)->node[j].pred!=NIL)
                {
                    SwapSNode(&((*sgtrain)->node[j]), &((*sgeval)->node[i]));
                    (*sgtrain)->node[j].pred = NIL;
                    nonprototypes--;
                    nerrors--;
                    counter = 0;
                }
                else counter--;
            }
        }
    }
}

// Find prototypes by the MST approach
void opf_MSTPrototypes(Subgraph *sg)
{
    int p,q;
    float weight;
    RealHeap *Q=NULL;
    float *pathval = NULL;
    int  pred;
    float nproto;

    // initialization
    pathval = AllocFloatArray(sg->nnodes);
    Q = CreateRealHeap(sg->nnodes, pathval);

    for (p = 0; p < sg->nnodes; p++)
    {
        pathval[ p ] = FLT_MAX;
        sg->node[p].status=0;
    }

    pathval[0]  = 0;
    sg->node[0].pred = NIL;
    InsertRealHeap(Q, 0);

    nproto=0.0;

    // Prim's algorithm for Minimum Spanning Tree
    while ( !IsEmptyRealHeap(Q) )
    {
        RemoveRealHeap(Q,&p);
        sg->node[p].pathval = pathval[p];

        pred=sg->node[p].pred;
        if (pred!=NIL)
            if (sg->node[p].truelabel != sg->node[pred].truelabel)
            {
                if (sg->node[p].status!=opf_PROTOTYPE)
                {
                    sg->node[p].status=opf_PROTOTYPE;
                    nproto++;
                }
                if (sg->node[pred].status!=opf_PROTOTYPE)
                {
                    sg->node[pred].status=opf_PROTOTYPE;
                    nproto++;
                }
            }

        for (q=0; q < sg->nnodes; q++)
        {
            if (Q->color[q]!=BLACK)
            {
                if (p!=q)
                {
                    if(!opf_PrecomputedDistance)
                        weight = opf_ArcWeight(sg->node[p].feat,sg->node[q].feat,sg->nfeats);
                    else
                        weight = opf_DistanceValue[sg->node[p].position][sg->node[q].position];
                    if ( weight < pathval[ q ] )
                    {
                        sg->node[q].pred = p;
                        UpdateRealHeap(Q, q, weight);
                    }
                }
            }
        }
    }
    DestroyRealHeap(&Q);
    free( pathval );

}

// Split subgraph into two parts such that the size of the first part
// is given by a percentual of samples.
void opf_SplitSubgraph(Subgraph *sg, Subgraph **sg1, Subgraph **sg2, float perc1,
					void *random_generator)
{
    int *label=AllocIntArray(sg->nlabels+1),i,j,i1,i2;
    int *nelems=AllocIntArray(sg->nlabels+1),totelems;

    for (i=0; i < sg->nnodes; i++)
    {
        sg->node[i].status = 0;
        label[sg->node[i].truelabel]++;
    }

    for (i=0; i < sg->nnodes; i++)
    {
        nelems[sg->node[i].truelabel]=MAX((int)(perc1*label[sg->node[i].truelabel]),1);
    }

    free(label);

    totelems=0;
    for (j=1; j <= sg->nlabels; j++)
        totelems += nelems[j];

    *sg1 = CreateSubgraph(totelems);
    *sg2 = CreateSubgraph(sg->nnodes-totelems);
    (*sg1)->nfeats = sg->nfeats;
    (*sg2)->nfeats = sg->nfeats;

    for (i1=0; i1 < (*sg1)->nnodes; i1++)
        (*sg1)->node[i1].feat = AllocFloatArray((*sg1)->nfeats);
    for (i2=0; i2 < (*sg2)->nnodes; i2++)
        (*sg2)->node[i2].feat = AllocFloatArray((*sg2)->nfeats);

    (*sg1)->nlabels = sg->nlabels;
    (*sg2)->nlabels = sg->nlabels;

    i1=0;
    while(totelems > 0)
    {
        i = random_integer(0,sg->nnodes-1, random_generator);
        if (sg->node[i].status!=NIL)
        {
            if (nelems[sg->node[i].truelabel]>0) // copy node to sg1
            {
                (*sg1)->node[i1].position = sg->node[i].position;
                for (j=0; j < (*sg1)->nfeats; j++)
                    (*sg1)->node[i1].feat[j]=sg->node[i].feat[j];
                (*sg1)->node[i1].truelabel = sg->node[i].truelabel;
                i1++;
                nelems[sg->node[i].truelabel] = nelems[sg->node[i].truelabel] - 1;
                sg->node[i].status = NIL;
                totelems--;
            }
        }
    }

    i2=0;
    for (i=0; i < sg->nnodes; i++)
    {
        if (sg->node[i].status!=NIL)
        {
            (*sg2)->node[i2].position = sg->node[i].position;
            for (j=0; j < (*sg2)->nfeats; j++)
                (*sg2)->node[i2].feat[j]=sg->node[i].feat[j];
            (*sg2)->node[i2].truelabel = sg->node[i].truelabel;
            i2++;
        }
    }

    free(nelems);
}

//Merge two subgraphs
Subgraph *opf_MergeSubgraph(Subgraph *sg1, Subgraph *sg2)
{
    if(sg1->nfeats != sg2->nfeats) Error("Invalid number of feats!","MergeSubgraph");

    Subgraph *out = CreateSubgraph(sg1->nnodes+sg2->nnodes);
    int i = 0, j;
	set<int> unique_labels;

    for (i = 0; i < sg1->nnodes; i++)
		unique_labels.insert(sg1->node[i].truelabel);

    for (i = 0; i < sg2->nnodes; i++)
		unique_labels.insert(sg2->node[i].truelabel);

	out->nlabels = unique_labels.size();
    out->nfeats = sg1->nfeats;

    for (i = 0; i < sg1->nnodes; i++)
        CopySNode(&out->node[i], &sg1->node[i], out->nfeats);
    for (j = 0; j < sg2->nnodes; j++)
    {
        CopySNode(&out->node[i], &sg2->node[j], out->nfeats);
        i++;
    }

    return out;
}

// Compute accuracy
float opf_Accuracy(Subgraph *sg)
{
    float Acc = 0.0f, **error_matrix = NULL, error = 0.0f;
    int i, *nclass = NULL, nlabels=0;

    error_matrix = (float **)calloc(sg->nlabels+1, sizeof(float *));
    for(i=0; i<= sg->nlabels; i++)
        error_matrix[i] = (float *)calloc(2, sizeof(float));

    nclass = AllocIntArray(sg->nlabels+1);

    for (i = 0; i < sg->nnodes; i++)
    {
        nclass[sg->node[i].truelabel]++;
    }

    for (i = 0; i < sg->nnodes; i++)
    {
        if(sg->node[i].truelabel != sg->node[i].label)
        {
            error_matrix[sg->node[i].truelabel][1]++;
            error_matrix[sg->node[i].label][0]++;
        }
    }

    for(i=1; i <= sg->nlabels; i++)
    {
        if (nclass[i]!=0)
        {
            error_matrix[i][1] /= (float)nclass[i];
            error_matrix[i][0] /= (float)(sg->nnodes - nclass[i]);
            nlabels++;
        }
    }

    for(i=1; i <= sg->nlabels; i++)
    {
        if (nclass[i]!=0)
            error += (error_matrix[i][0]+error_matrix[i][1]);
    }

    Acc = 1.0-(error/(2.0*nlabels));

    for(i=0; i <= sg->nlabels; i++)
        free(error_matrix[i]);
    free(error_matrix);
    free(nclass);

    return(Acc);
}


// Normalized cut
float opf_NormalizedCut( Subgraph *sg )
{
    int l, p, q;
    Set *Saux;
    float ncut, dist;
    float *acumIC; //acumulate weights inside each class
    float *acumEC; //acumulate weights between the class and a distinct one

    ncut = 0.0;
    acumIC = AllocFloatArray( sg->nlabels );
    acumEC = AllocFloatArray( sg->nlabels );

    for ( p = 0; p < sg->nnodes; p++ )
    {
        for ( Saux = sg->node[ p ].adj; Saux != NULL; Saux = Saux->next )
        {
            q = Saux->elem;
            if (!opf_PrecomputedDistance)
                dist = opf_ArcWeight(sg->node[p].feat,sg->node[q].feat,sg->nfeats);
            else
                dist = opf_DistanceValue[sg->node[p].position][sg->node[q].position];
            if ( dist > 0.0 )
            {
                if ( sg->node[ p ].label == sg->node[ q ].label )
                {
                    acumIC[ sg->node[ p ].label ] += 1.0 / dist; // intra-class weight
                }
                else   // inter - class weight
                {
                    acumEC[ sg->node[ p ].label ] += 1.0 / dist; // inter-class weight
                }
            }
        }
    }

    for ( l = 0; l < sg->nlabels; l++ )
    {
        if ( acumIC[ l ] + acumEC[ l ]  > 0.0 ) ncut += (float) acumEC[ l ] / ( acumIC[ l ] + acumEC[ l ] );
    }
    free( acumEC );
    free( acumIC );
    return( ncut );
}

// Estimate the best k by minimum cut
void opf_BestkMinCut(Subgraph *sg, int kmin, int kmax)
{
    int k, bestk = kmax;
    float mincut=FLT_MAX,nc;

    float* maxdists = opf_CreateArcs2(sg,kmax); // stores the maximum distances for every k=1,2,...,kmax
    // Find the best k
    for (k = kmin; (k <= kmax)&&(mincut != 0.0); k++)
    {
        sg->df = maxdists[k-1];
        sg->bestk = k;

        opf_PDFtoKmax(sg);
        opf_OPFClusteringToKmax(sg);

        nc = opf_NormalizedCutToKmax(sg);

        if (nc < mincut)
        {
            mincut=nc;
            bestk = k;
        }

        opf_RemovePlateauNeighbors(sg);
    }

    free(maxdists);
    opf_DestroyArcs(sg);
    sg->bestk = bestk;

    opf_CreateArcs(sg,sg->bestk);
    opf_PDF(sg);

}


// Create adjacent list in subgraph: a knn graph
void opf_CreateArcs(Subgraph *sg, int knn)
{
    int    i,j,l,k;
    float  dist;
    int   *nn=AllocIntArray(knn+1);
    float *d=AllocFloatArray(knn+1);

    /* Create graph with the knn-nearest neighbors */

    sg->df=0.0;
    for (i=0; i < sg->nnodes; i++)
    {
        for (l=0; l < knn; l++)
            d[l]=FLT_MAX;
        for (j=0; j < sg->nnodes; j++)
        {
            if (j!=i)
            {
                if (!opf_PrecomputedDistance)
                    d[knn] = opf_ArcWeight(sg->node[i].feat,sg->node[j].feat,sg->nfeats);
                else
                    d[knn] = opf_DistanceValue[sg->node[i].position][sg->node[j].position];
                nn[knn]= j;
                k      = knn;
                while ((k > 0)&&(d[k]<d[k-1]))
                {
                    dist    = d[k];
                    l       = nn[k];
                    d[k]    = d[k-1];
                    nn[k]   = nn[k-1];
                    d[k-1]  = dist;
                    nn[k-1] = l;
                    k--;
                }
            }
        }

        for (l=0; l < knn; l++)
        {
            if (d[l]!=INT_MAX)
            {
                if (d[l] > sg->df)
                    sg->df = d[l];
                //if (d[l] > sg->node[i].radius)
                sg->node[i].radius = d[l];
                InsertSet(&(sg->node[i].adj),nn[l]);
            }
        }
    }
    free(d);
    free(nn);

    if (sg->df<0.00001)
        sg->df = 1.0;
}

// Destroy Arcs
void opf_DestroyArcs(Subgraph *sg)
{
    int i;

    for (i=0; i < sg->nnodes; i++)
    {
        sg->node[i].nplatadj = 0;
        DestroySet(&(sg->node[i].adj));
    }
}

// opf_PDF computation
void opf_PDF(Subgraph *sg)
{
    int     i,nelems;
    double  dist;
    float  *value=AllocFloatArray(sg->nnodes);
    Set    *adj=NULL;

    sg->K    = (2.0*(float)sg->df/9.0);
    sg->mindens = FLT_MAX;
    sg->maxdens = -FLT_MAX;
    for (i=0; i < sg->nnodes; i++)
    {
        adj=sg->node[i].adj;
        value[i]=0.0;
        nelems=1;
        while (adj != NULL)
        {
            if (!opf_PrecomputedDistance)
                dist  = opf_ArcWeight(sg->node[i].feat,sg->node[adj->elem].feat,sg->nfeats);
            else
                dist  = opf_DistanceValue[sg->node[i].position][sg->node[adj->elem].position];
            value[i] += exp(-dist/sg->K);
            adj = adj->next;
            nelems++;
        }

        value[i] = (value[i]/(float)nelems);

        if (value[i] < sg->mindens)
            sg->mindens = value[i];
        if (value[i] > sg->maxdens)
            sg->maxdens = value[i];
    }

    //  printf("df=%f,K1=%f,K2=%f,mindens=%f, maxdens=%f\n",sg->df,sg->K1,sg->K2,sg->mindens,sg->maxdens);

    if (sg->mindens==sg->maxdens)
    {
        for (i=0; i < sg->nnodes; i++)
        {
            sg->node[i].dens = opf_MAXDENS;
            sg->node[i].pathval= opf_MAXDENS-1;
        }
    }
    else
    {
        for (i=0; i < sg->nnodes; i++)
        {
            sg->node[i].dens = ((float)(opf_MAXDENS-1)*(value[i]-sg->mindens)/(float)(sg->maxdens-sg->mindens))+1.0;
            sg->node[i].pathval=sg->node[i].dens-1;
        }
    }
    free(value);
}


/*------------ Distance functions ------------------------------ */

// Compute Euclidean distance between feature vectors
float opf_EuclDist(float *f1, float *f2, int n)
{
    int i;
    float dist=0.0f;

	if(opf_FeatWeight == NULL)
	{
		for (i=0; i < n; i++)
			dist += (f1[i]-f2[i])*(f1[i]-f2[i]);
	}
	else
	{
		for (i=0; i < n; i++)
			dist += (f1[i]-f2[i])*(f1[i]-f2[i])*opf_FeatWeight[i];
	}
    return(dist);
}


// Discretizes original distance
float opf_EuclDistLog(float *f1, float *f2,int n)
{
    return(((float)opf_MAXARCW*log(opf_EuclDist(f1,f2,n)+1)));
}

/* -------- Auxiliary functions to optimize BestkMinCut -------- */

// Create adjacent list in subgraph: a knn graph.
// Returns an array with the maximum distances
// for each k=1,2,...,kmax
float* opf_CreateArcs2(Subgraph *sg, int kmax)
{
    int    i,j,l,k;
    float  dist;

    int   *nn=AllocIntArray(kmax+1);

    float *d=AllocFloatArray(kmax+1);
    float *maxdists=AllocFloatArray(kmax);
    /* Create graph with the knn-nearest neighbors */

    sg->df=0.0;

    for (i=0; i < sg->nnodes; i++)
    {
        for (l=0; l < kmax; l++)
            d[l]=FLT_MAX;
        for (j=0; j < sg->nnodes; j++)
        {
            if (j!=i)
            {
                if(!opf_PrecomputedDistance)
                    d[kmax] = opf_ArcWeight(sg->node[i].feat,sg->node[j].feat,sg->nfeats);
                else
                    d[kmax] = opf_DistanceValue[sg->node[i].position][sg->node[j].position];
                nn[kmax]= j;
                k      = kmax;
                while ((k > 0)&&(d[k]<d[k-1]))
                {
                    dist    = d[k];
                    l       = nn[k];
                    d[k]    = d[k-1];
                    nn[k]   = nn[k-1];
                    d[k-1]  = dist;
                    nn[k-1] = l;
                    k--;
                }
            }
        }
        sg->node[i].radius = 0.0;
        sg->node[i].nplatadj = 0; //zeroing amount of nodes on plateaus
        //making sure that the adjacent nodes be sorted in non-decreasing order
        for (l=kmax-1; l >= 0; l--)
        {
            if (d[l]!=FLT_MAX)
            {
                if (d[l] > sg->df)
                    sg->df = d[l];
                if (d[l] > sg->node[i].radius)
                    sg->node[i].radius = d[l];
                if(d[l] > maxdists[l])
                    maxdists[l] = d[l];
                //adding the current neighbor at the beginnig of the list
                InsertSet(&(sg->node[i].adj),nn[l]);
            }
        }
    }
    free(d);
    free(nn);

    if (sg->df<0.00001)
        sg->df = 1.0;

    return maxdists;
}

// OPFClustering computation only for sg->bestk neighbors
void opf_OPFClusteringToKmax(Subgraph *sg)
{
    Set *adj_i,*adj_j;
    char insert_i;
    int i,j;
    int p, q, l, ki,kj;
    const int kmax = sg->bestk;
    float tmp,*pathval=NULL;
    RealHeap *Q=NULL;
    Set *Saux=NULL;

    //   Add arcs to guarantee symmetry on plateaus
    for (i=0; i < sg->nnodes; i++)
    {
        adj_i = sg->node[i].adj;
        ki = 1;
        while (ki <= kmax)
        {
            j  = adj_i->elem;
            if (sg->node[i].dens==sg->node[j].dens)
            {
                // insert i in the adjacency of j if it is not there.
                adj_j    = sg->node[j].adj;
                insert_i = 1;
                kj = 1;
                while (kj <= kmax)
                {
                    if (i == adj_j->elem)
                    {
                        insert_i=0;
                        break;
                    }
                    adj_j=adj_j->next;
                    kj++;
                }
                if (insert_i)
                {
                    InsertSet(&(sg->node[j].adj),i);
                    sg->node[j].nplatadj++; //number of adjacent nodes on
                    //plateaus (includes adjacent plateau
                    //nodes computed for previous kmax's)
                }
            }
            adj_i=adj_i->next;
            ki++;
        }
    }

    // Compute clustering

    pathval = AllocFloatArray(sg->nnodes);
    Q = CreateRealHeap(sg->nnodes, pathval);
    SetRemovalPolicyRealHeap(Q, MAXVALUE);

    for (p = 0; p < sg->nnodes; p++)
    {
        pathval[ p ] = sg->node[ p ].pathval;
        sg->node[ p ].pred  = NIL;
        sg->node[ p ].root  = p;
        InsertRealHeap(Q, p);
    }

    l = 0;
    i = 0;
    while (!IsEmptyRealHeap(Q))
    {
        RemoveRealHeap(Q,&p);
        sg->ordered_list_of_nodes[i]=p;
        i++;

        if ( sg->node[ p ].pred == NIL )
        {
            pathval[ p ] = sg->node[ p ].dens;
            sg->node[p].label=l;
            l++;
        }

        sg->node[ p ].pathval = pathval[ p ];
        const int nadj = sg->node[p].nplatadj + kmax; // total amount of neighbors
        for ( Saux = sg->node[ p ].adj, ki = 1; ki <= nadj; Saux = Saux->next, ki++ )
        {
            q = Saux->elem;
            if ( Q->color[q] != BLACK )
            {
                tmp = MIN( pathval[ p ], sg->node[ q ].dens);
                if ( tmp > pathval[ q ] )
                {
                    UpdateRealHeap(Q,q,tmp);
                    sg->node[ q ].pred  = p;
                    sg->node[ q ].root  = sg->node[ p ].root;
                    sg->node[ q ].label = sg->node[ p ].label;
                }
            }
        }
    }

    sg->nlabels = l;

    DestroyRealHeap( &Q );
    free( pathval );
}

void opf_RemovePlateauNeighbors(Subgraph *sg)
{
    int i,j;
    Set *next_adj=NULL;

    for(i = 0; i < sg->nnodes; i++)
    {
        //Eliminating all neighbors that were added
        //for belonging to the plateau of node i.
        //Plateau neighbors are always added to the beginning
        //of the list, since InsertSet works that way, so they
        //must be removed in the same fashion.
        for(j = 0; j < sg->node[i].nplatadj; j++)
        {
            next_adj = sg->node[i].adj->next;
            free(sg->node[i].adj);
            sg->node[i].adj = next_adj;
        }
        sg->node[i].nplatadj = 0;
    }
}

// PDF computation only for sg->bestk neighbors
void opf_PDFtoKmax(Subgraph *sg)
{
    int     i,nelems;
    const int kmax = sg->bestk;
    double  dist;
    float  *value=AllocFloatArray(sg->nnodes);
    Set    *adj=NULL;

    sg->K    = (2.0*(float)sg->df/9.0);

    sg->mindens = FLT_MAX;
    sg->maxdens = -FLT_MAX;
    for (i=0; i < sg->nnodes; i++)
    {
        adj=sg->node[i].adj;
        value[i]=0.0;
        nelems=1;
        int k;

        //The PDF is computed only for the kmax adjacents
        //because it is assumed that there will be no plateau
        //neighbors yet, i.e. nplatadj = 0 for every node in sg.
        //PATCH: this assumption is now maintained by removing
        //the plateau neighbors after the computation of normalized cut
        //in BestkMinCut. Previously this was not true. (Thiago Spina, 26/08/2011)
        for (k = 1; k <= kmax; k++)
        {
            if(!opf_PrecomputedDistance)
                dist  = opf_ArcWeight(sg->node[i].feat,sg->node[adj->elem].feat,sg->nfeats);
            else
                dist = opf_DistanceValue[sg->node[i].position][sg->node[adj->elem].position];
            value[i] += exp(-dist/sg->K);
            adj = adj->next;
            nelems++;
        }

        value[i] = (value[i]/(float)nelems);

        if (value[i] < sg->mindens)
            sg->mindens = value[i];
        if (value[i] > sg->maxdens)
            sg->maxdens = value[i];
    }

    if (sg->mindens==sg->maxdens)
    {
        for (i=0; i < sg->nnodes; i++)
        {
            sg->node[i].dens = opf_MAXDENS;
            sg->node[i].pathval= opf_MAXDENS-1;
        }
    }
    else
    {
        for (i=0; i < sg->nnodes; i++)
        {
            sg->node[i].dens = ((float)(opf_MAXDENS-1)*(value[i]-sg->mindens)/(float)(sg->maxdens-sg->mindens))+1.0;
            sg->node[i].pathval=sg->node[i].dens-1;
        }
    }
    free(value);
}


// Normalized cut computed only for sg->bestk neighbors
float opf_NormalizedCutToKmax( Subgraph *sg )
{
    int l, p, q, k;
    const int kmax = sg->bestk;
    Set *Saux;
    float ncut, dist;
    float *acumIC; //acumulate weights inside each class
    float *acumEC; //acumulate weights between the class and a distinct one

    ncut = 0.0;
    acumIC = AllocFloatArray( sg->nlabels );
    acumEC = AllocFloatArray( sg->nlabels );

    for ( p = 0; p < sg->nnodes; p++ )
    {
        const int nadj = sg->node[p].nplatadj + kmax; //for plateaus the number of adjacent
        //nodes will be greater than the current
        //kmax, but they should be considered
        for ( Saux = sg->node[ p ].adj, k = 1; k <= nadj; Saux = Saux->next, k++ )
        {
            q = Saux->elem;
            if(!opf_PrecomputedDistance)
                dist = opf_ArcWeight(sg->node[p].feat,sg->node[q].feat,sg->nfeats);
            else
                dist = opf_DistanceValue[sg->node[p].position][sg->node[q].position];
            if ( dist > 0.0 )
            {
                if ( sg->node[ p ].label == sg->node[ q ].label )
                {
                    acumIC[ sg->node[ p ].label ] += 1.0 / dist; // intra-class weight
                }
                else   // inter - class weight
                {
                    acumEC[ sg->node[ p ].label ] += 1.0 / dist; // inter-class weight
                }
            }
        }
    }

    for ( l = 0; l < sg->nlabels; l++ )
    {
        if ( acumIC[ l ] + acumEC[ l ]  > 0.0 ) ncut += (float) acumEC[ l ] / ( acumIC[ l ] + acumEC[ l ] );
    }
    free( acumEC );
    free( acumIC );
    return( ncut );
}


