#include "usisview.h"

namespace usis
{
	CImage8 *cwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, bool fill)
	{
		CImage8 *hcimg = NULL;
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		hcimg = copy_cimage8(cimg);
		A    = fast_circular(radius);

		for(u.y=0; u.y < hcimg->nrows; u.y++)
		{
			for(u.x=0; u.x < hcimg->ncols; u.x++)
			{
				p = to_index(hcimg,u);

				if(fill)
				{
					if(label->val[p] > 0)
					{
						hcimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*hcimg->C[0]->val[p]);
						hcimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*hcimg->C[1]->val[p]);
						hcimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*hcimg->C[2]->val[p]);
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(hcimg,v))
					{
						q = to_index(hcimg,v);

						if(label->val[p] != label->val[q])
						{
							hcimg->C[0]->val[p] = t0(color);
							hcimg->C[1]->val[p] = t1(color);
							hcimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);

		return(hcimg);
	}

	CImage8 *cwide_highlight(CImage8 *cimg, Image32 *regions, float radius, int color, bool fill)
	{
		CImage8 *hcimg = NULL;
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		hcimg = copy_cimage8(cimg);
		A    = fast_circular(radius);

		for(u.y=0; u.y < hcimg->nrows; u.y++)
		{
			for(u.x=0; u.x < hcimg->ncols; u.x++)
			{
				p = to_index(hcimg,u);

				if(fill)
				{
					if(regions->val[p] > 0)
					{
						hcimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*hcimg->C[0]->val[p]);
						hcimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*hcimg->C[1]->val[p]);
						hcimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*hcimg->C[2]->val[p]);
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(hcimg,v))
					{
						q = to_index(hcimg,u);

						if(regions->val[p] != regions->val[q])
						{
							hcimg->C[0]->val[p] = t0(color);
							hcimg->C[1]->val[p] = t1(color);
							hcimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);

		return(hcimg);
	}

	void icwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, bool fill)
	{
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < cimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < cimg->C[0]->ncols; u.x++)
			{
				p = u.x + to_index(cimg,u);

				if(fill &&label->val[p] > 0)
				{
					cimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*cimg->C[2]->val[p]);
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						q = to_index(cimg,v);

						if(label->val[p] > 0 && label->val[p] != label->val[q])
						{
							cimg->C[0]->val[p] = t0(color);
							cimg->C[1]->val[p] = t1(color);
							cimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}


	void icwide_highlight(CImage8 *cimg, Image32 *label, float radius, int color, bool fill)
	{
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < cimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < cimg->C[0]->ncols; u.x++)
			{
				p = u.x + to_index(cimg,u);

				if(fill &&label->val[p] > 0)
				{
					cimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*cimg->C[2]->val[p]);
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						q = to_index(cimg,v);

						if(label->val[p] > 0 && label->val[p] != label->val[q])
						{
							cimg->C[0]->val[p] = t0(color);
							cimg->C[1]->val[p] = t1(color);
							cimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}


	void icwide_highlight_bin_mask(CImage8 *cimg, Image8 *gt, float radius, int color, bool fill)
	{
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < cimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < cimg->C[0]->ncols; u.x++)
			{
				p = to_index(cimg,u);

				if(fill &&gt->val[p] > 0)
				{
					cimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*cimg->C[2]->val[p]);
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						q = to_index(cimg,v);

						if(gt->val[p] > 0 && gt->val[q] == 0)
						{
							cimg->C[0]->val[p] = t0(color);
							cimg->C[1]->val[p] = t1(color);
							cimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}


	CImage8 *cb_wide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, int bkgcolor, bool fill)
	{
		CImage8 *hcimg = NULL;
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		hcimg = copy_cimage8(cimg);
		A    = fast_circular(radius);

		for(u.y=0; u.y < hcimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < hcimg->C[0]->ncols; u.x++)
			{
				p = to_index(hcimg,u);

				if(fill)
				{
					if(label->val[p] > 0)
					{
						hcimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*hcimg->C[0]->val[p]);
						hcimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*hcimg->C[1]->val[p]);
						hcimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*hcimg->C[2]->val[p]);
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(hcimg,v))
					{
						q = to_index(hcimg,v);

						if(label->val[p] > label->val[q])
						{
							hcimg->C[0]->val[p] = t0(color);
							hcimg->C[1]->val[p] = t1(color);
							hcimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}

				if(label->val[p] == 0)
				{
					hcimg->C[0]->val[p] = t0(bkgcolor);
					hcimg->C[1]->val[p] = t1(bkgcolor);
					hcimg->C[2]->val[p] = t2(bkgcolor);
				}
			}
		}

		destroy_adjrel(&A);

		return(hcimg);
	}

	CImage8 *cwide_highlight_lb(CImage8 *cimg, Image8 *label, float radius, int *colormap, bool fill)
	{
		CImage8 *hcimg;
		hcimg = copy_cimage8(cimg);

		icwide_highlight_lb(hcimg, label, radius, colormap, fill);

		return hcimg;
	}



	CImage8 *highlight_obj_bkg_lb(CImage8 *cimg, Image32 *objlabel, Image32 *bkglabel, float radius, int objcolor, int bkgcolor, bool fill)
	{
		CImage8 *hcimg;
		int p,q,i,lb,ld,color;
		AdjRel *A=NULL;
		Pixel u,v;

		hcimg = copy_cimage8(cimg);
		A    = fast_circular(radius);

		for(u.y=0; u.y < hcimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < hcimg->C[0]->ncols; u.x++)
			{
				p = to_index(hcimg,u);
				color = (objlabel->val[p] == 1 ? objcolor : bkgcolor);
				lb = (objlabel->val[p] == 1 ? objlabel->val[p]: bkglabel->val[p]);

				if(fill)
				{
					if(lb> 0)
					{
						hcimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*hcimg->C[0]->val[p]);
						hcimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*hcimg->C[1]->val[p]);
						hcimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*hcimg->C[2]->val[p]);
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(hcimg,v))
					{
						q = to_index(hcimg,v);
						ld = (objlabel->val[q] == 1 ? objlabel->val[q]: bkglabel->val[q]);

						if(lb != ld)
						{
							hcimg->C[0]->val[p] = t0(color);
							hcimg->C[1]->val[p] = t1(color);
							hcimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);

		return(hcimg);
	}

	CImage8 *highlight_lb_n_bkg_regions(CImage8 *cimg, Image8 *label, Image32 *regions, float radius, int objcolor, int bkgcolor, bool fill)
	{
		CImage8 *hcimg;
		int p,q,i,lb,ld,rlb,rld,color;
		AdjRel *A=NULL;
		Pixel u,v;

		hcimg = copy_cimage8(cimg);
		A    = fast_circular(radius);

		for(u.y=0; u.y < hcimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < hcimg->C[0]->ncols; u.x++)
			{
				p = to_index(hcimg,u);
				color = (label->val[p] == 1 ? objcolor : bkgcolor);
				lb = label->val[p];

				if(fill)
				{
					if(lb> 0)
					{
						hcimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*hcimg->C[0]->val[p]);
						hcimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*hcimg->C[1]->val[p]);
						hcimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*hcimg->C[2]->val[p]);
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(hcimg,v))
					{
						q = to_index(hcimg,v);
						ld = label->val[q];
						rlb = regions->val[p];
						rld = regions->val[q];

						if(lb != ld || (rlb != rld && lb == 0 && ld == 0))
						{
							hcimg->C[0]->val[p] = t0(color);
							hcimg->C[1]->val[p] = t1(color);
							hcimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);

		return(hcimg);
	}

	CImage8 *wb_highlight_lb_n_regions(CImage8 *cimg, Image8 *label, Image32 *regions, float radius, int objcolor, int bkgcolor, bool fill)
	{
		CImage8 *hcimg;
		int p,q,i,lb,ld,rlb,rld,color;
		AdjRel *A=NULL;
		Pixel u,v;

		hcimg = copy_cimage8(cimg);

		A    = fast_circular(radius);

		for(u.y=0; u.y < hcimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < hcimg->C[0]->ncols; u.x++)
			{
				p = to_index(hcimg,u);
				color = (label->val[p] == 1 ? objcolor : bkgcolor);
				lb = label->val[p];

				if(lb == 0)
				{
					hcimg->C[0]->val[p] = 255;
					hcimg->C[1]->val[p] = 255;
					hcimg->C[2]->val[p] = 255;
				} else {
					if(fill)
					{
						if(lb> 0)
						{
							hcimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*hcimg->C[0]->val[p]);
							hcimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*hcimg->C[1]->val[p]);
							hcimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*hcimg->C[2]->val[p]);
						}
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(hcimg,v))
					{
						q = to_index(hcimg,v);
						ld = label->val[q];
						rlb = regions->val[p];
						rld = regions->val[q];

						if(lb != ld || (rlb != rld))
						{
							hcimg->C[0]->val[p] = t0(color);
							hcimg->C[1]->val[p] = t1(color);
							hcimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);

		return(hcimg);
	}



	void icwide_highlight_lb(CImage8 *cimg, Image8 *label,
							float radius, int *colormap, bool fill)
	{
		int p,q,i,lb;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < label->nrows; u.y++)
		{
			for(u.x=0; u.x < label->ncols; u.x++)
			{
				p = to_index(cimg,u);
				lb = label->val[p];

				if(fill && lb > 0)
				{
					cimg->C[0]->val[p] = ROUND(0.3*t0(colormap[lb]) + 0.7*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(0.3*t1(colormap[lb]) + 0.7*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(0.3*t2(colormap[lb]) + 0.7*cimg->C[2]->val[p]);
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						q = to_index(cimg,v);

						if(lb > 0 && lb != label->val[q])
						{
							cimg->C[0]->val[p] = t0(colormap[lb]);
							cimg->C[1]->val[p] = t1(colormap[lb]);
							cimg->C[2]->val[p] = t2(colormap[lb]);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}



	void icwide_highlight_lb(CImage8 *cimg, Image32 *label,
							float radius, int *colormap, bool fill)
	{
		int p,q,i,lb;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < label->nrows; u.y++)
		{
			for(u.x=0; u.x < label->ncols; u.x++)
			{
				p = to_index(cimg,u);
				lb = label->val[p];

				if(fill && lb > 0)
				{
					cimg->C[0]->val[p] = ROUND(0.3*t0(colormap[lb]) + 0.7*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(0.3*t1(colormap[lb]) + 0.7*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(0.3*t2(colormap[lb]) + 0.7*cimg->C[2]->val[p]);
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						q = to_index(cimg,v);

						if(lb > 0 && lb != label->val[q])
						{
							cimg->C[0]->val[p] = t0(colormap[lb]);
							cimg->C[1]->val[p] = t1(colormap[lb]);
							cimg->C[2]->val[p] = t2(colormap[lb]);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}

	void icwide_highlight_bkg(CImage8 *cimg, Image8 *label, float radius, int *colormap,
								bool border, float alpha)
	{
		int p,q,i,lb;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < label->nrows; u.y++)
		{
			for(u.x=0; u.x < label->ncols; u.x++)
			{
				p = to_index(cimg,u);
				lb = label->val[p];

				if(lb == 0)
				{
					cimg->C[0]->val[p] = ROUND(alpha*t0(colormap[lb]) + (1.0-alpha)*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(alpha*t1(colormap[lb]) + (1.0-alpha)*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(alpha*t2(colormap[lb]) + (1.0-alpha)*cimg->C[2]->val[p]);
				}

                if(border)
                {
                    for(i=1; i < A->n; i++)
                    {
                        v.x = u.x + A->dx[i];
                        v.y = u.y + A->dy[i];

                        if(valid_pixel(cimg,v))
                        {
                            q = to_index(cimg,v);

                            if(lb == 0 && lb != label->val[q])
                            {
                                cimg->C[0]->val[p] = t0(colormap[lb]);
                                cimg->C[1]->val[p] = t1(colormap[lb]);
                                cimg->C[2]->val[p] = t2(colormap[lb]);
                                break;
                            }
                        }
                    }
                }
			}
		}

		destroy_adjrel(&A);
	}

	void icfill_lb(CImage8 *cimg, Image8 *label, int *colormap, int sel_label, float alpha)
	{
		int p,lb;

		for(p=0; p < label->nrows*label->ncols; p++)
		{
			lb = label->val[p];

			if((sel_label >= 0 && lb == sel_label)
                || (sel_label < 0 && lb > 0))
			{
				cimg->C[0]->val[p] = ROUND((1-alpha)*t0(colormap[lb]) + alpha*cimg->C[0]->val[p]);
				cimg->C[1]->val[p] = ROUND((1-alpha)*t1(colormap[lb]) + alpha*cimg->C[1]->val[p]);
				cimg->C[2]->val[p] = ROUND((1-alpha)*t2(colormap[lb]) + alpha*cimg->C[2]->val[p]);
			}
		}
	}


	void icbwide_highlight(CImage8 *cimg, Image8 *label, float radius,
							int color, int bkgcolor, bool fill)
	{
		int p,q,i;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < cimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < cimg->C[0]->ncols; u.x++)
			{
				p = to_index(cimg,u);

				if(fill)
				{
					if(label->val[p] == USIS_DEFAULT_SEG_BKG_LABEL)
					{
						cimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*cimg->C[0]->val[p]);
						cimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*cimg->C[1]->val[p]);
						cimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*cimg->C[2]->val[p]);
					}
				}

				if(label->val[p] == 0)
				{
					cimg->C[0]->val[p] = t0(bkgcolor);
					cimg->C[1]->val[p] = t1(bkgcolor);
					cimg->C[2]->val[p] = t2(bkgcolor);
				}
				else
				{
					for(i=1; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];

						if(valid_pixel(cimg,v))
						{
							q = to_index(cimg,v);

							if(label->val[p] != label->val[q])
							{
								cimg->C[0]->val[p] = t0(color);
								cimg->C[1]->val[p] = t1(color);
								cimg->C[2]->val[p] = t2(color);
								break;
							}
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}


	void ihighlight_lb_n_bkg_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
									float radius, int objcolor, int bkgcolor, bool fill)
	{
		int i;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < cimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < cimg->C[0]->ncols; u.x++)
			{
				const int p = to_index(cimg,u);
				const int lb = label->val[p];
				const int color = (lb == 1 ? objcolor : bkgcolor);

				if(fill)
				{
					if(lb> 0)
					{
						cimg->C[0]->val[p] = ROUND(0.3*t0(color) + 0.7*cimg->C[0]->val[p]);
						cimg->C[1]->val[p] = ROUND(0.3*t1(color) + 0.7*cimg->C[1]->val[p]);
						cimg->C[2]->val[p] = ROUND(0.3*t2(color) + 0.7*cimg->C[2]->val[p]);
					}
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						const int q = to_index(cimg,v);
						const int ld = label->val[q];
						const int rlb = regions->val[p];
						const int rld = regions->val[q];

						if(lb != ld || (rlb != rld && lb == 0 && ld == 0))
						{
							cimg->C[0]->val[p] = t0(color);
							cimg->C[1]->val[p] = t1(color);
							cimg->C[2]->val[p] = t2(color);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}

	void ihighlight_lb_n_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
								float radius, int *colormap, bool fill)
	{
		int i;
		AdjRel *A=NULL;
		Pixel u,v;

		A    = fast_circular(radius);

		for(u.y=0; u.y < cimg->C[0]->nrows; u.y++)
		{
			for(u.x=0; u.x < cimg->C[0]->ncols; u.x++)
			{
				const int p = to_index(cimg,u);
				const int lb = label->val[p];

				if(fill && lb> 0)
				{
					cimg->C[0]->val[p] = ROUND(0.3*t0(colormap[lb]) + 0.7*cimg->C[0]->val[p]);
					cimg->C[1]->val[p] = ROUND(0.3*t1(colormap[lb]) + 0.7*cimg->C[1]->val[p]);
					cimg->C[2]->val[p] = ROUND(0.3*t2(colormap[lb]) + 0.7*cimg->C[2]->val[p]);
				}

				for(i=1; i < A->n; i++)
				{
					v.x = u.x + A->dx[i];
					v.y = u.y + A->dy[i];

					if(valid_pixel(cimg,v))
					{
						const int q = to_index(cimg,v);
						const int ld = label->val[q];
						const int rlb = regions->val[p];
						const int rld = regions->val[q];

						if(lb != ld || rlb != rld)
						{
							cimg->C[0]->val[p] = t0(colormap[lb]);
							cimg->C[1]->val[p] = t1(colormap[lb]);
							cimg->C[2]->val[p] = t2(colormap[lb]);
							break;
						}
					}
				}
			}
		}

		destroy_adjrel(&A);
	}

	void draw_leak_points(CImage8 *cimg, Set *leak, float radius, int color)
	{
		Set *aux = leak;

		while(aux != NULL)
		{
			draw_circumference(cimg, aux->elem, radius, color);
			cimg->C[0]->val[aux->elem] = t0(color);
			cimg->C[1]->val[aux->elem] = t1(color);
			cimg->C[2]->val[aux->elem] = t2(color);
			aux = aux->next;
		}
	}
}
