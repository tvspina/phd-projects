#include "usismemory.h"

#include "usiscommon.h"

namespace usis
{
    int* alloc_int_array(const int n, int align)
	{
		return alloc_array<int>(n,align);
	}

    float* alloc_float_array(const int n, int align)
	{
		return alloc_array<float>(n,align);
	}

    double* alloc_double_array(const int n, int align)
	{
		return alloc_array<double>(n,align);
	}


    uchar* alloc_uchar_array(const int n, int align)
	{
		return alloc_array<uchar>(n,align);
	}

	void free_array_data(void *data)
	{
		_mm_free(data);
	}

	void safe_free_array(void **data)
	{
		if(data == NULL || *data == NULL) return;

		free_array_data(*data);

		*data = NULL;
	}
}

