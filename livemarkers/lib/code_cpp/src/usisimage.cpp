#include "usisimage.h"

#include "adjacency.h"


Pixel usis_right_pixel(Pixel p, Pixel q)
{
	Pixel r = (Pixel)
	{
		-1, -1
	};
	int dx = q.x - p.x;
	int dy = q.y - p.y;

	float d  = sqrt(dx * dx + dy * dy);
	if(d != 0)
	{
		r.x = p.x + ROUND(((float)dx / 2.0) - ((float)dy / d));
		r.y = p.y + ROUND(((float)dx / d) + ((float)dy / 2.0));
	}

	return r;
}


Pixel usis_left_pixel(Pixel p, Pixel q)
{
	Pixel l = (Pixel)
	{
		-1, -1
	};
	int dx = q.x - p.x;
	int dy = q.y - p.y;

	float d  = sqrt(dx * dx + dy * dy);
	if(d != 0)
	{
		l.x = p.x + ROUND(((float)dx / 2.0) + ((float)dy / d));
		l.y = p.y + ROUND(((float)dy / 2) - ((float)dx / d));
	}

	return l;
}

namespace usis
{
	/******************************************************************/
	/**						Grey image template 					 **/
	/******************************************************************/

	Image8::value_type& Image8 :: operator [] (int p)
	{
		return this->val[p];
	}

/// Basic image constructor
	Image8 *create_image8(const int w, const int h)
	{
		return create_image<Image8, Image8::value_type>(w,h);
	}

/// Copy constructor
	Image8 *copy_image8(Image8 *old_img)
	{
		Image8 *img = create_image8(old_img->ncols, old_img->nrows);
		copy_inplace(img, old_img);
		return img;
	}


/// Constructor that reads from an PGM image file
	Image8 *read_image8(const char *filename)
	{
		FILE *fp = NULL;
		char type[10];
		int  i, ncols, nrows, n;
		char z[256];

		Image8 *img = NULL;
		fp = fopen(filename, "rb");

		if(fp == NULL)
		{
			fprintf(stderr, "Cannot open %s\n", filename);
			exit(-1);
		}

		fscanf(fp, "%s\n", type);

		if((strcmp(type, "P5") == 0))
		{
			ncf_gets(z, 255, fp);
			sscanf(z, "%d %d\n", &ncols, &nrows);
			n = (ncols) * (nrows);
			ncf_gets(z, 255, fp);
			sscanf(z, "%d\n", &i);
			fgetc(fp);

			img = create_image8(ncols, nrows);

			if(img->val != NULL)
			{
				fread(img->val, sizeof(unsigned char), n, fp);
			}
			else
			{
				fprintf(stderr, "Insufficient memory when reading %s in read_image8\n",
							filename);
				exit(-1);
			}

			fclose(fp);
		}
		else
		{
			if((strcmp(type, "P2") == 0))
			{
				ncf_gets(z, 255, fp);
				sscanf(z, "%d %d\n", &(ncols), &(nrows));
				n = ncols * nrows;
				ncf_gets(z, 255, fp);
				sscanf(z, "%d\n", &i);

				img = create_image8(ncols, nrows);

				for(i = 0; i < n; i++)
				{
					int pix_val;
					fscanf(fp, "%d", &pix_val);
					img->val[i] = (uchar)pix_val;
				}

				fclose(fp);
			}
			else
			{
				fprintf(stderr, "Input image must be P2 or P5\n");
				exit(-1);
			}
		}

		return img;
	}

	Image32::value_type& Image32 :: operator [] (int p)
	{
		return this->val[p];
	}

	Image32 *create_image32(const int w, const int h)
	{
		return create_image<Image32, Image32::value_type>(w,h);
	}

	DbImage::value_type& DbImage :: operator [] (int p)
	{
		return this->val[p];
	}

	DbImage *create_dbimage(const int w, const int h)
	{
		return create_image<DbImage, DbImage::value_type>(w,h);
	}

	Image32 *copy_image32(Image32 *old_img)
	{
		Image32 *img = create_image32(old_img->ncols, old_img->nrows);
		copy_inplace(img, old_img);
		return img;
	}

/// Constructor that reads from an PGM image file
	Image32 *read_image32(const char *filename)
	{
		FILE *fp = NULL;
		uchar *value = NULL;
		char type[10];
		int  i, ncols, nrows, n;
		char z[256];

		Image32 *img = NULL;

		fp = fopen(filename, "rb");

		if(fp == NULL)
		{
			fprintf(stderr, "Cannot open %s\n", filename);
			exit(-1);
		}

		fscanf(fp, "%s\n", type);

		if((strcmp(type, "P5") == 0))
		{
			ncf_gets(z, 255, fp);
			sscanf(z, "%d %d\n", &(ncols), &(nrows));
			n = (ncols) * (nrows);
			ncf_gets(z, 255, fp);
			sscanf(z, "%d\n", &i);
			fgetc(fp);
			value = AllocUCharArray(n);

			if(value != NULL)
			{
				fread(value, sizeof(unsigned char), n, fp);
			}
			else
			{
				fprintf(stderr, "Insufficient memory in ImageT<T>(filename)\n");
				exit(-1);
			}

			fclose(fp);
			img = create_image32(ncols, nrows);

			for(i = 0; i < n; i++)
				img->val[i] = (int)value[i];

			free(value);
		}
		else
		{
			if((strcmp(type, "P2") == 0))
			{
				ncf_gets(z, 255, fp);
				sscanf(z, "%d %d\n", &(ncols), &(nrows));
				n = ncols * nrows;
				ncf_gets(z, 255, fp);
				sscanf(z, "%d\n", &i);

				img = create_image32(ncols, nrows);

				for(i = 0; i < n; i++)
				{
					int pix_val;
					fscanf(fp, "%d", &pix_val);
					img->val[i] = (int)pix_val;
				}

				fclose(fp);
			}
			else
			{
				fprintf(stderr, "Input image must be P2 or P5\n");
				exit(-1);
			}
		}

		return img;
	}

	DbImage *copy_dbimage(DbImage *old_img)
	{
		DbImage *img = create_dbimage(old_img->ncols, old_img->nrows);
		copy_inplace(img, old_img);
		return img;
	}


/// Basic constructor
	CImage8 *create_cimage8(const int w, const int h)
	{

		int n = w*h*3;
		int l = USIS_SSE_ALIGNMENT / sizeof(Image8::value_type);

		n += l - n % l; /// Rounding up to a number divisible by 16 for SSE

		Image8::value_type *data = alloc_uchar_array(n);

		return create_cimage8(w,h, data, n);
	}


	CImage8 *create_cimage8(const int w, const int h,
							Image8::value_type *data,
							const int n_alloced)
	{
		int i;
		CImage8 *cimg = (CImage8*)calloc(1, sizeof(CImage8));

		cimg->data = data;

		cimg->n_alloced = n_alloced;

		for(i = 0; i < 3; i++)
		{
			cimg->C[i] = (Image8*)calloc(1,sizeof(Image8));
			cimg->C[i]->ncols = w;
			cimg->C[i]->nrows = h;
			cimg->C[i]->n = w*h;
			// Setting the pointer to the right position
			cimg->C[i]->val = cimg->data + i*w*h;
		}

		cimg->ncols = w;
		cimg->nrows = h;
		cimg->n = w*h;

		cimg->bb = Rectangle(0,0,w-1,h-1);

		return cimg;
	}

/// Copy constructor
	CImage8 *copy_cimage8(CImage8 *old_img)
	{
		CImage8 *cimg = create_cimage8(old_img->ncols, old_img->nrows);

		copy_inplace(cimg, old_img);

		return cimg;
	}


/// Constructur that reads from a PPM file
	CImage8 *read_cimage8(const char *filename)
	{
		FILE *fp = NULL;
		char type[10];
		int  i, ncols, nrows, n;
		char z[256];
		CImage8 *cimg = NULL;

		fp = fopen(filename, "rb");

		if(fp == NULL)
		{
			fprintf(stderr, "Cannot open %s\n", filename);
			exit(-1);
		}

		fscanf(fp, "%s\n", type);

		if((strcmp(type, "P6") == 0))
		{

			ncf_gets(z, 255, fp);
			sscanf(z, "%d %d\n", &(ncols), &(nrows));
			n = (ncols) * (nrows);
			ncf_gets(z, 255, fp);
			sscanf(z, "%d\n", &i);

			cimg = create_cimage8(ncols, nrows);

			for(i = 0; i < n; i++)
			{
				cimg->C[0]->val[i] = fgetc(fp);
				cimg->C[1]->val[i] = fgetc(fp);
				cimg->C[2]->val[i] = fgetc(fp);
			}

			fclose(fp);
		}
		else
		{
			fprintf(stderr, "Input image must be P6\n");
			exit(-1);
		}

		return cimg;
	}

/// Basic constructor
	CImage32 *create_cimage32(const int w, const int h)
	{
		int n = w*h*3;
		int l = 16 / sizeof(Image32::value_type);

		n += l - n % l; /// Rounding up to a number divisible by 16 for SSE

		Image32::value_type *data = alloc_int_array(n);

		return create_cimage32(w,h, data, n);
	}


	CImage32 *create_cimage32(const int w, const int h,
							Image32::value_type *data,
							const int n_alloced)
	{
		int i;
		CImage32 *cimg = (CImage32*)calloc(1, sizeof(CImage32));

		cimg->data = data;

		cimg->n_alloced = n_alloced;

		for(i = 0; i < 3; i++)
		{
			cimg->C[i] = (Image32*)calloc(1,sizeof(Image32));
			cimg->C[i]->ncols = w;
			cimg->C[i]->nrows = h;
			cimg->C[i]->n = w*h;
			// Setting the pointer to the right position
			cimg->C[i]->val = cimg->data + i*w*h;
		}

		cimg->ncols = w;
		cimg->nrows = h;
		cimg->n = w*h;
		cimg->bb = Rectangle(0,0,w-1,h-1);

		return cimg;
	}


/// Copy constructor
	CImage32 *copy_cimage32(CImage32 *old_img)
	{
		CImage32 *cimg = create_cimage32(old_img->ncols, old_img->nrows);
		copy_inplace(cimg, old_img);
		return cimg;
	}

	CImage32 *convert_2_cimage32(Image32 *img)
	{
		CImage32 *cimg = create_cimage32(img->ncols, img->nrows);
		int p;
		for(p = 0; p < cimg->n; p++)
		{
			cimg->C[0]->val[p] = img->val[p];
			cimg->C[1]->val[p] = img->val[p];
			cimg->C[2]->val[p] = img->val[p];
		}
		return cimg;
	}



/// Constructur that reads from a PPM file
	CImage32 *read_cimage32(const char *filename)
	{
		FILE *fp = NULL;
		char type[10];
		int  i, ncols, nrows, n;
		char z[256];

		CImage32 *cimg = NULL;

		fp = fopen(filename, "rb");

		if(fp == NULL)
		{
			fprintf(stderr, "Cannot open %s\n", filename);
			exit(-1);
		}

		fscanf(fp, "%s\n", type);

		if((strcmp(type, "P6") == 0))
		{
			ncf_gets(z, 255, fp);
			sscanf(z, "%d %d\n", &(ncols), &(nrows));
			n = (ncols) * (nrows);
			ncf_gets(z, 255, fp);
			sscanf(z, "%d\n", &i);

			cimg = create_cimage32(ncols, nrows);

			for(i = 0; i < n; i++)
			{
				cimg->C[0]->val[i] = fgetc(fp);
				cimg->C[1]->val[i] = fgetc(fp);
				cimg->C[2]->val[i] = fgetc(fp);
			}

			fclose(fp);
		}
		else
		{
			fprintf(stderr, "Input image must be P6\n");
			exit(-1);
		}

		return cimg;
	}

	Image8::value_type maximum_value(Image8 *img)
	{
		return maximum_value<Image8, Image8::value_type>(img);
	}

	Image32::value_type maximum_value(Image32 *img)
	{
		return maximum_value<Image32, Image32::value_type>(img);
	}

	DbImage::value_type maximum_value(DbImage *img)
	{
		return maximum_value<DbImage, DbImage::value_type>(img);
	}

	Image8::value_type minimum_value(Image8 *img)
	{
		return minimum_value<Image8, Image8::value_type>(img);
	}

	Image32::value_type minimum_value(Image32 *img)
	{
		return minimum_value<Image32, Image32::value_type>(img);
	}

	DbImage::value_type minimum_value(DbImage *img)
	{
		return minimum_value<DbImage, DbImage::value_type>(img);
	}


	void destroy_image(Image8 **img)
	{
		if(img == NULL || *img == NULL) return;

		safe_free_array((void**)(&((*img)->val)));
		free(*img);

		*img = NULL;
	}

	void destroy_image(Image32 **img)
	{
		if(img == NULL || *img == NULL) return;

		safe_free_array((void**)(&((*img)->val)));
		free(*img);

		*img = NULL;
	}

	void destroy_dbimage(DbImage **img)
	{
		if(img == NULL || *img == NULL) return;

		safe_free_array((void**)(&((*img)->val)));
		free(*img);

		*img = NULL;
	}

	bool write_image(Image8 *img, const char *format, ...)
	{
		FILE *fp;
		int i, n, Imax;

		va_list args;
		char filename[BUFSIZ];

		va_start(args, format);
		vsprintf(filename, format, args);
		va_end(args);

		fp = fopen(filename, "wb");

		if(fp == NULL)
		{
			fprintf(stderr, "Cannot open %s\n", filename);
			return false;
		}

		n = img->ncols * img->nrows;

		Imax = maximum_value(img);

		fprintf(fp, "P2\n");
		fprintf(fp, "%d %d\n", img->ncols, img->nrows);

		if(Imax == 0) Imax++;

		fprintf(fp, "%d\n", Imax);

		for(i = 0; i < n; i++)
		{
			fprintf(fp, "%d ", img->val[i]);

			if(((i + 1) % 17) == 0)
				fprintf(fp, "\n");
		}

		fclose(fp);

		return true;
	}


	bool write_image(Image32 *img, const char *format, ...)
	{
		FILE *fp;
		int i, n, Imax;

		va_list args;
		char filename[BUFSIZ];

		va_start(args, format);
		vsprintf(filename, format, args);
		va_end(args);

		fp = fopen(filename, "wb");

		if(fp == NULL)
		{
			fprintf(stderr, "Cannot open %s\n", filename);
			return false;
		}

		n    = img->ncols * img->nrows;

		if((Imax = maximum_value(img)) == INT_MAX)
		{
			Warning((char*)"Image with infinity values", (char*)"write_image");
			Imax = INT_MIN;

			for(i = 0; i < n; i++)
				if((img->val[i] > Imax) && (img->val[i] != INT_MAX))
					Imax = img->val[i];

			fprintf(fp, "P2\n");
			fprintf(fp, "%d %d\n", img->ncols, img->nrows);
			fprintf(fp, "%d\n", Imax + 1);
		}
		else
		{
			fprintf(fp, "P2\n");
			fprintf(fp, "%d %d\n", img->ncols, img->nrows);

			if(Imax == 0) Imax++;

			fprintf(fp, "%d\n", Imax);
		}

		for(i = 0; i < n; i++)
		{
			if(img->val[i] == INT_MAX)
				fprintf(fp, "%d ", Imax + 1);
			else
				fprintf(fp, "%d ", img->val[i]);

			if(((i + 1) % 17) == 0)
				fprintf(fp, "\n");
		}

		fclose(fp);

		return true;
	}

	Image8 *add_frame(Image8 *img, int sz, uchar value)
	{
		return add_frame<Image8, Image8::value_type>(img, sz, sz, value);
	}

	Image8 *rem_frame(Image8 *fimg, int sz)
	{
		return rem_frame<Image8, Image8::value_type>(fimg, sz, sz);
	}

	Image32 *add_frame(Image32 *img, int sz, int value)
	{
		return add_frame<Image32, Image32::value_type>(img, sz, sz, value);
	}

	Image32 *rem_frame(Image32 *fimg, int sz)
	{
		return rem_frame<Image32, Image32::value_type>(fimg, sz, sz);
	}

	void paste_image(Image8 *bkg, Image8 *fg, int x, int y)
	{
		int i, j, p, q;

		q = 0;

		for (i = 0; i < fg->nrows; i++)
		{
			for (j = 0; j < fg->ncols; j++)
			{
				if (valid_pixel(bkg, x + j, i + y))
				{
					p = x+j + (i+y)*bkg->ncols;

					bkg->val[p] = fg->val[q];
				}

				q++;
			}
		}
	}

	Image8 *cast_2_image8(Image32 *img32)
	{
		Image8 *img8 = create_image8(img32->ncols, img32->nrows);

		int p;

		for(p = 0; p < img32->n; p++)
			img8->val[p] = (uchar)img32->val[p];

		return img8;
	}

	Image32 *cast_2_image32(Image8 *img8)
	{
		Image32 *img32 = create_image32(img8->ncols, img8->nrows);

		int p;

		for(p = 0; p < img32->n; p++)
			img32->val[p] = (uchar)img8->val[p];

		return img32;
	}

	Image8 *convert_2_8bits(Image8 *img)
	{
		Image8 *imgN;
		int N = 8;
		int min, max, i, n, Imax;

		imgN = create_image8(img->ncols, img->nrows);
		n    = img->ncols * img->nrows;
		Imax = (int)(pow(2, N) - 1);

		min = INT_MAX;
		max = INT_MIN;

		for(i = 0; i < n; i++)
		{
			if((img->val[i] != INT_MIN) && (img->val[i] != INT_MAX))
			{
				if(img->val[i] > max)
					max = img->val[i];

				if(img->val[i] < min)
					min = img->val[i];
			}
		}

		if(min != max)
			for(i = 0; i < n; i++)
			{
				if((img->val[i] != INT_MIN) && (img->val[i] != INT_MAX))
				{
					imgN->val[i] = (uchar)(((float)Imax * (float)(img->val[i] - min)) /
					                       (float)(max - min));
				}
				else
				{
					if(img->val[i] == INT_MIN)
						imgN->val[i] = 0;
					else
						imgN->val[i] = Imax;
				}
			}

		return(imgN);
	}

	Image8 *convert_2_8bits(Image32 *img)
	{
		Image8 *imgN;
		int N = 8;
		int min, max, i, n, Imax;

		imgN = create_image8(img->ncols, img->nrows);
		n    = img->ncols * img->nrows;
		Imax = (int)(pow(2, N) - 1);

		min = INT_MAX;
		max = INT_MIN;

		for(i = 0; i < n; i++)
		{
			if((img->val[i] != INT_MIN) && (img->val[i] != INT_MAX))
			{
				if(img->val[i] > max)
					max = img->val[i];

				if(img->val[i] < min)
					min = img->val[i];
			}
		}

		if(min != max)
			for(i = 0; i < n; i++)
			{
				if((img->val[i] != INT_MIN) && (img->val[i] != INT_MAX))
				{
					imgN->val[i] = (uchar)(((float)Imax * (float)(img->val[i] - min)) /
					                       (float)(max - min));
				}
				else
				{
					if(img->val[i] == INT_MIN)
						imgN->val[i] = 0;
					else
						imgN->val[i] = Imax;
				}
			}

		return(imgN);
	}

	Image32 *convert_2_Nbits(Image32 *img, int N)
	{
		Image32 *imgN;
		int min, max, i, n, Imax;

		imgN = create_image32(img->ncols, img->nrows);
		n    = img->n;
		Imax = (int)(pow(2, N) - 1);

		min = INT_MAX;
		max = INT_MIN;

		for(i = 0; i < n; i++)
		{
			if((img->val[i] != INT_MIN) && (img->val[i] != INT_MAX))
			{
				if(img->val[i] > max)
					max = img->val[i];

				if(img->val[i] < min)
					min = img->val[i];
			}
		}

		if(min != max)
			for(i = 0; i < n; i++)
			{
				if((img->val[i] != INT_MIN) && (img->val[i] != INT_MAX))
				{
					imgN->val[i] = (uchar)(((float)Imax * (float)(img->val[i] - min)) /
					                       (float)(max - min));
				}
				else
				{
					if(img->val[i] == INT_MIN)
						imgN->val[i] = 0;
					else
						imgN->val[i] = Imax;
				}
			}

		return(imgN);
	}


	Image8 *convert_2_image8(DbImage *dimg)

    {
		Image8 *img = create_image8(dimg->ncols, dimg->nrows);

		convert_2_image8_inplace(img, dimg);

		return img;
    }

    void convert_2_image8_inplace(Image8 *img, DbImage *dimg)
	{

		DbImage::value_type max, min;
		int p, n = dimg->n;

		max = maximum_value(dimg);
		min = minimum_value(dimg);

		if(max - min <= DBL_EPSILON)
		{
			for(p = 0; p < n; p++)
				img->val[p] = max;
		}
		else
		{
			for(p = 0; p < n; p++)
			{
				img->val[p] = UCHAR_MAX * (dimg->val[p] - min) / (max - min);
			}
		}
	}

	Image32 *convert_2_image32(DbImage *dimg)
	{
		Image32 *img = create_image32(dimg->ncols, dimg->nrows);

		convert_2_image32_inplace(img, dimg);

		return img;
	}

    void convert_2_image32_inplace(Image32 *img, DbImage *dimg)
	{
		DbImage::value_type max, min;
		int p, n = dimg->n;

		max = maximum_value(dimg);
		min = minimum_value(dimg);
		if(max - min <= DBL_EPSILON)
		{
			for(p = 0; p < n; p++)
				img->val[p] = max;
		}
		else
		{
			for(p = 0; p < n; p++)
			{
				img->val[p] = UCHAR_MAX * (dimg->val[p] - min) / (max - min);
			}
		}
	}


/// copies each pixel value from src to dest
	void copy_inplace(Image8 *dest, Image8 *src)
	{
		memcpy(dest->val, src->val, sizeof(Image8::value_type)*dest->n_alloced);
	}

/// copies each pixel value from src to dest
	void copy_inplace(Image32 *dest, Image32 *src)
	{
		memcpy(dest->val, src->val, sizeof(Image32::value_type)*dest->n_alloced);
	}

/// copies each pixel value from src to dest
	void copy_inplace(DbImage *dest, DbImage *src)
	{
		memcpy(dest->val, src->val, sizeof(DbImage::value_type)*dest->n_alloced);
	}


	Image8 *scale(Image8 *img, float Sx, float Sy)
	{
		Image8 *scl;

		scl = create_image8((int)(img->ncols * fabs(Sx) + 0.5),
					(int)(img->nrows * fabs(Sy) + 0.5));

		scale(img, scl, Sx, Sy);

		return scl;
	}

	Image8 *scale(Image8 *img, Image8 *scl, float Sx, float Sy)
	{
		float S[2][2], x, y, d1, d2, d3, d4, Ix1, Ix2, If;

		Pixel u, v, prev, next;

		if(Sx == 0.0) Sx = 1.0;
		if(Sy == 0.0) Sy = 1.0;

		S[0][0] = 1.0 / Sx;
		S[0][1] = 0;
		S[1][0] = 0;
		S[1][1] = 1.0 / Sy;


		for(v.y = 0; v.y < scl->nrows; v.y++)
			for(v.x = 0; v.x < scl->ncols; v.x++)
			{
				x = ((v.x - scl->ncols / 2.) * S[0][0] + (v.y - scl->nrows / 2.) * S[0][1])
				    + img->ncols / 2.;
				y = ((v.x - scl->ncols / 2.) * S[1][0] + (v.y - scl->nrows / 2.) * S[1][1])
				    + img->nrows / 2.;
				u.x = (int)(x + 0.5);
				u.y = (int)(y + 0.5);
				if(valid_pixel(img, u.x, u.y))
				{
					if(x < u.x)
					{
						next.x = u.x;
						prev.x = u.x - 1;
					}
					else
					{
						next.x = u.x + 1;
						prev.x = u.x;
					}
					d1 = next.x - x;
					d2 = x - prev.x;
					if(y < u.y)
					{
						next.y = u.y;
						prev.y = u.y - 1;
					}
					else
					{
						next.y = u.y + 1;
						prev.y = u.y;
					}
					d3 = next.y - y;
					d4 = y - prev.y;

					if(valid_pixel(img, prev.x, prev.y) && valid_pixel(img, next.x, prev.y))
						Ix1 = d1 * img->val[to_index(img, prev)] +
						      d2 * img->val[to_index(img, next.x, prev.y)];
					else
						Ix1 = img->val[to_index(img, u)];

					if(valid_pixel(img, prev.x, next.y) && valid_pixel(img, next.x, next.y))
						Ix2 = d1 * img->val[to_index(img, prev.x, next.y)] +
						      d2 * img->val[to_index(img, next)];
					else
						Ix2 = img->val[to_index(img, u)];

					If = d3 * Ix1 + d4 * Ix2;

					scl->val[to_index(scl, v)] = (int)If;
				}
			}

		return(scl);
	}



	Image32 *scale(Image32 *img, float Sx, float Sy)
	{
		Image32 *scl;

		scl = create_image32((int)(img->ncols * fabs(Sx) + 0.5),
						(int)(img->nrows * fabs(Sy) + 0.5));
		scale(img, scl, Sx, Sy);

		return scl;
	}

	Image32 *scale(Image32 *img, Image32 *scl, float Sx, float Sy)
	{
		float S[2][2], x, y, d1, d2, d3, d4, Ix1, Ix2, If;

		Pixel u, v, prev, next;

		if(Sx == 0.0) Sx = 1.0;
		if(Sy == 0.0) Sy = 1.0;

		S[0][0] = 1.0 / Sx;
		S[0][1] = 0;
		S[1][0] = 0;
		S[1][1] = 1.0 / Sy;



		for(v.y = 0; v.y < scl->nrows; v.y++)
			for(v.x = 0; v.x < scl->ncols; v.x++)
			{
				x = ((v.x - scl->ncols / 2.) * S[0][0] + (v.y - scl->nrows / 2.) * S[0][1])
				    + img->ncols / 2.;
				y = ((v.x - scl->ncols / 2.) * S[1][0] + (v.y - scl->nrows / 2.) * S[1][1])
				    + img->nrows / 2.;
				u.x = (int)(x + 0.5);
				u.y = (int)(y + 0.5);
				if(valid_pixel(img, u.x, u.y))
				{
					if(x < u.x)
					{
						next.x = u.x;
						prev.x = u.x - 1;
					}
					else
					{
						next.x = u.x + 1;
						prev.x = u.x;
					}
					d1 = next.x - x;
					d2 = x - prev.x;
					if(y < u.y)
					{
						next.y = u.y;
						prev.y = u.y - 1;
					}
					else
					{
						next.y = u.y + 1;
						prev.y = u.y;
					}
					d3 = next.y - y;
					d4 = y - prev.y;

					if(valid_pixel(img, prev.x, prev.y) && valid_pixel(img, next.x, prev.y))
						Ix1 = d1 * img->val[to_index(img, prev)] +
						      d2 * img->val[to_index(img, next.x, prev.y)];
					else
						Ix1 = img->val[to_index(img, u)];

					if(valid_pixel(img, prev.x, next.y) && valid_pixel(img, next.x, next.y))
						Ix2 = d1 * img->val[to_index(img, prev.x, next.y)] +
						      d2 * img->val[to_index(img, next)];
					else
						Ix2 = img->val[to_index(img, u)];

					If = d3 * Ix1 + d4 * Ix2;

					scl->val[to_index(scl, v)] = (int)If;
				}
			}

		return(scl);
	}

	double eucl_dist2(Image32 *img, int p, int q)
	{
		Pixel u = to_pixel(img, p);
		Pixel v = to_pixel(img, q);

		return eucl_dist2(u, v);
	}

	double eucl_dist2(Image8 *img, int p, int q)
	{
		Pixel u = to_pixel(img, p);
		Pixel v = to_pixel(img, q);

		return eucl_dist2(u, v);
	}


	/******************************************************************/
	/**						Color image template 					 **/
	/******************************************************************/

	void destroy_cimage(CImage8 **cimg)
	{
		if(cimg == NULL || *cimg == NULL) return;

		free_array_data((*cimg)->data);

		free((*cimg)->C[0]);
		free((*cimg)->C[1]);
		free((*cimg)->C[2]);

		free(*cimg);

		*cimg = NULL;
	}

	void destroy_cimage(CImage32 **cimg)
	{
		if(cimg == NULL || *cimg == NULL) return;

		free_array_data((*cimg)->data);

		free((*cimg)->C[0]);
		free((*cimg)->C[1]);
		free((*cimg)->C[2]);

		free(*cimg);

		*cimg = NULL;
	}

	bool is_cimage_gray(CImage8 *cimg)
	{
		int i, n;
		n = cimg->ncols * cimg->nrows;

		for(i = 0; i < n; i++)
		{
			if(cimg->C[0]->val[i] != cimg->C[1]->val[i]
			        || cimg->C[1]->val[i] != cimg->C[2]->val[i]) return false;
		}

		return true;
	}

	bool write_cimage(CImage8 *cimg, const char *format, ...)
	{
		FILE *fp;
		int i, n;

		va_list args;
		char filename[BUFSIZ];

		va_start(args, format);
		vsprintf(filename, format, args);
		va_end(args);

		fp = fopen(filename, "w");

		if(fp == NULL)
		{
			fprintf(stderr, "Could not open %s!\n", filename);
			return false;
		}

		fprintf(fp, "P6\n");
		fprintf(fp, "%d %d\n", cimg->ncols, cimg->nrows);
		fprintf(fp, "255\n");
		n = cimg->ncols * cimg->nrows;

		for(i = 0; i < n; i++)
		{
			fputc(cimg->C[0]->val[i], fp);
			fputc(cimg->C[1]->val[i], fp);
			fputc(cimg->C[2]->val[i], fp);
		}

		fclose(fp);

		return true;
	}

	bool write_cimage(CImage32 *cimg, const char *format, ...)
	{
		FILE *fp;
		int i, n;

		va_list args;
		char filename[BUFSIZ];

		va_start(args, format);
		vsprintf(filename, format, args);
		va_end(args);

		fp = fopen(filename, "w");

		if(fp == NULL)
		{
			fprintf(stderr, "Could not open %s!\n", filename);
			return false;
		}

		fprintf(fp, "P6\n");
		fprintf(fp, "%d %d\n", cimg->ncols, cimg->nrows);
		fprintf(fp, "255\n");
		n = cimg->ncols * cimg->nrows;

		for(i = 0; i < n; i++)
		{
			fputc(cimg->C[0]->val[i], fp);
			fputc(cimg->C[1]->val[i], fp);
			fputc(cimg->C[2]->val[i], fp);
		}

		fclose(fp);

		return true;
	}

    CImage8* concatenate(CImage8 *img1, CImage8 *img2)

    {
		CImage8 *cat = create_cimage8(img1->ncols + img2->ncols, MAX(img1->nrows, img2->nrows));
		return cat;
    }


    void concatenate(CImage8 *img1, CImage8 *img2, CImage8 *cat)
    {
		int x,y;


		for(y = 0; y < cat->nrows; y++)
			for(x = 0; x < cat->ncols; x++)
			{
				if(x < img1->ncols)
				{
					int p = to_index(cat, x, y);

					if(valid_pixel(img1, x, y))
					{
						int p1 = to_index(img1, x, y);
						set_rgb(cat, p, get_rgb(img1,p1));
					}
				}
				else
				{
					int p = to_index(cat, x, y);
					int x2 = x - img1->ncols;

					if(valid_pixel(img2, x2, y))
					{
						int p2 = to_index(img2, x2, y);
						set_rgb(cat, p,  get_rgb(img2, p2));
					}
				}
			}
    }


	CImage8 *add_frame(CImage8 *img, int sz, uchar r, uchar g, uchar b)
	{
		CImage8 *fimg;
		int c, x, y;

		fimg = create_cimage8(img->ncols + (2 * sz), img->nrows + (2 * sz));
		set_cimage(fimg, triplet(r,g,b));


		for(c = 0; c < 3; c++)
			for(y = 0; y < img->nrows; y++)
			{
				for(x = 0; x < img->ncols; x++)
					fimg->C[c]->val[to_index(fimg, x+sz, y+sz)] = img->C[c]->val[to_index(img, x, y)];
			}

		return(fimg);
	}

	void paste_image(CImage8 *bkg, CImage8 *fg, int x, int y)
	{
		paste_image(bkg->C[0], fg->C[0], x, y);
		paste_image(bkg->C[1], fg->C[1], x, y);
		paste_image(bkg->C[2], fg->C[2], x, y);
	}


	void set_image(Image8 *img, Image8::value_type val)
	{
//		int p;
		memset(img->val, val, img->n);
//		/// Image8 is always aligned and padded to a multiple of 16
//		for(p = 0; p < img->n - 4; p += 4)
//		{
//			img->val[p] = val;
//			img->val[p+1] = val;
//			img->val[p+2] = val;
//			img->val[p+3] = val;
//		}
//
//		/// doing this to avoid setting the padding to something != 0
//		for(; p < img->n; p++)
//		{
//			img->val[p] = val;
//		}
	}

	void set_image(Image32 *img, Image32::value_type val)
	{
		if(val == 0)
			memset(img->val, 0, img->n*sizeof(Image32::value_type));
		else
		{
			int p;
			/// Image8 is always aligned and padded to a multiple of 16
			for(p = 0; p < img->n - 4; p += 4)
			{
				img->val[p] = val;
				img->val[p+1] = val;
				img->val[p+2] = val;
				img->val[p+3] = val;
			}

			/// doing this to avoid setting the padding to something != 0
			for(; p < img->n; p++)
			{
				img->val[p] = val;
			}
		}

	}

	void set_image(DbImage *img, DbImage::value_type val)
	{
		if(val == 0.0)
			memset(img->val, 0.0, img->n*sizeof(DbImage::value_type));
		else
		{
			int p;
			/// Image8 is always aligned and padded to a multiple of 16
			for(p = 0; p < img->n - 4; p += 4)
			{
				img->val[p] = val;
				img->val[p+1] = val;
				img->val[p+2] = val;
				img->val[p+3] = val;
			}

			/// doing this to avoid setting the padding to something != 0
			for(; p < img->n; p++)
			{
				img->val[p] = val;
			}
		}
	}


	CImage8 *cimage_rgb_to_ycbcr(CImage8 *cimg)
	{
		CImage8 *ncimg = NULL;
		int p, n, i;


		ncimg = create_cimage8(cimg->ncols, cimg->nrows);
		n    = ncimg->ncols * ncimg->nrows;

		for(p = 0; p < n; p++)
		{

			i = triplet(cimg->C[0]->val[p], cimg->C[1]->val[p], cimg->C[2]->val[p]);
			i = RGB2YCbCr(i);
			ncimg->C[0]->val[p] = t0(i);
			ncimg->C[1]->val[p] = t1(i);
			ncimg->C[2]->val[p] = t2(i);
		}

		return(ncimg);
	}

	CImage8 *cimage_ycbcr_to_rgb(CImage8 *cimg)
	{
		CImage8 *ncimg = NULL;
		int p, n, i;


		ncimg = create_cimage8(cimg->ncols, cimg->nrows);
		n    = ncimg->ncols * ncimg->nrows;

		for(p = 0; p < n; p++)
		{

			i = triplet(cimg->C[0]->val[p], cimg->C[1]->val[p], cimg->C[2]->val[p]);
			i = YCbCr2RGB(i);
			ncimg->C[0]->val[p] = t0(i);
			ncimg->C[1]->val[p] = t1(i);
			ncimg->C[2]->val[p] = t2(i);
		}

		return(ncimg);
	}


	CImage8 *cimage_rgb_to_lab(CImage8 *cimg)
	{
		int i, n;
		CImage8 *lab;

		if(cimg == NULL) return NULL;

		n = cimg->ncols * cimg->nrows;

		lab = create_cimage8(cimg->ncols, cimg->nrows);

		for(i = 0; i < n; i++)
		{
			float R, G, B;

			R = (float)cimg->C[0]->val[i];
			G = (float)cimg->C[1]->val[i];
			B = (float)cimg->C[2]->val[i];

			RGB2Lab(&R, &G, &B);

			ScaleLab(&R, &G, &B);

			lab->C[0]->val[i] = (CImage8::triplet_type)(R*UCHAR_MAX);
			lab->C[1]->val[i] = (CImage8::triplet_type)(G*UCHAR_MAX);
			lab->C[2]->val[i] = (CImage8::triplet_type)(B*UCHAR_MAX);
		}

		return lab;
	}

	CImage8 *cimage_lab_to_rgb(CImage8 *cimg)
	{
		int n, i;
		CImage8 *rgb;

		if(cimg == NULL) return NULL;

		n = cimg->ncols * cimg->nrows;

		rgb = create_cimage8(cimg->ncols, cimg->nrows);

		for(i = 0; i < n; i++)
		{
			float L, a, b;

			L = cimg->C[0]->val[i]/(float)UCHAR_MAX;
			a = cimg->C[1]->val[i]/(float)UCHAR_MAX;
			b = cimg->C[2]->val[i]/(float)UCHAR_MAX;

			RescaleLab(&L, &a, &b);

			Lab2RGB(&L, &a, &b);

			rgb->C[0]->val[i] = (CImage8::triplet_type)L;
			rgb->C[1]->val[i] = (CImage8::triplet_type)a;
			rgb->C[2]->val[i] = (CImage8::triplet_type)b;
		}

		return rgb;
	}

	void set_cimage(CImage8 *img, CImage8::triplet_type val)
	{
		set_image(img->C[0], (CImage8::triplet_type)t0(val));
		set_image(img->C[1], (CImage8::triplet_type)t1(val));
		set_image(img->C[2], (CImage8::triplet_type)t2(val));
	}

	void set_cimage(CImage32 *img, int val)
	{
		set_image(img->C[0], t0(val));
		set_image(img->C[1], t1(val));
		set_image(img->C[2], t2(val));
	}

	void set_cimage(CImage32 *img, int c0, int c1, int c2)
	{
		set_image(img->C[0], c0);
		set_image(img->C[1], c1);
		set_image(img->C[2], c2);
	}


/// Only copies the content from a 32-bit image
/// (with non-checked max value of 255) to a
/// CImage8
	CImage8 *cast_2_cimage8(Image32 *img32)
	{
		CImage8 *cimg = create_cimage8(img32->ncols, img32->nrows);

		int p;

		for(p = 0; p < img32->n; p++)
		{
			cimg->C[0]->val[p] = (uchar) img32->val[p];
			cimg->C[1]->val[p] = (uchar) img32->val[p];
			cimg->C[2]->val[p] = (uchar) img32->val[p];
		}

		return cimg;
	}

/// Only copies the content from a 32-bit image
/// (with non-checked max value of 255) to a
/// CImage8
	CImage8 *cast_2_cimage8(Image8 *img)
	{
		CImage8 *cimg = create_cimage8(img->ncols, img->nrows);

		int p;
		for(p = 0; p < img->n; p++)
		{
			cimg->C[0]->val[p] = img->val[p];
			cimg->C[1]->val[p] = img->val[p];
			cimg->C[2]->val[p] = img->val[p];
		}

		return cimg;
	}


	CImage8 *convert_2_cimage8(Image32 *img)
	{
		Image8 *img8 = convert_2_8bits(img);
		CImage8 *cimg = cast_2_cimage8(img8);

		destroy_image(&img8);

		return cimg;
	}

	CImage8 *convert_2_grayscale(CImage8 *color)
	{
		CImage8 *ycbcr = cimage_rgb_to_ycbcr(color);
		CImage8 *gray = cast_2_cimage8(ycbcr->C[0]);

		destroy_cimage(&ycbcr);

		return gray;
	}

/// copies each pixel value from src to dest
	void copy_inplace(CImage8 *dest, CImage8 *src)
	{
		memcpy(dest->data, src->data,
				sizeof(Image8::value_type)*dest->n_alloced);
	}

/// copies each pixel value from src to dest
	void copy_inplace(CImage32 *dest, CImage32 *src)
	{
		memcpy(dest->data, src->data,
				sizeof(Image32::value_type)*dest->n_alloced);
	}

	CImage8 *scale(CImage8 *img, float Sx, float Sy)
	{
		CImage8 *cimg = create_cimage8((int)(img->ncols * fabs(Sx) + 0.5),
										(int)(img->nrows * fabs(Sy) + 0.5));

		scale(img->C[0], cimg->C[0], Sx, Sy);
		scale(img->C[1], cimg->C[1], Sx, Sy);
		scale(img->C[2], cimg->C[2], Sx, Sy);

		return cimg;
	}

	CImage32 *scale(CImage32 *img, float Sx, float Sy)
	{
		CImage32 *cimg = create_cimage32((int)(img->ncols * fabs(Sx) + 0.5),
										(int)(img->nrows * fabs(Sy) + 0.5));

		scale(img->C[0], cimg->C[0], Sx, Sy);
		scale(img->C[1], cimg->C[1], Sx, Sy);
		scale(img->C[2], cimg->C[2], Sx, Sy);

		return cimg;
	}

	void add_image(Image32 *bkg, Image32 *fg, int x, int y)
	{
		int i, j, p, q;

		q = 0;

		for(i = 0; i < fg->nrows; i++)
		{
			for(j = 0; j < fg->ncols; j++)
			{
				if(valid_pixel(bkg, x + j, i + y))
				{
					int val;
					p = to_index(bkg, x + j, i + y);

					val = bkg->val[p] + fg->val[q];

					bkg->val[p] = val;
				}

				q++;
			}
		}
	}


	void add_image(Image8 *bkg, Image8 *fg, int x, int y)
	{
		int i, j, p, q;

		q = 0;

		for(i = 0; i < fg->nrows; i++)
		{
			for(j = 0; j < fg->ncols; j++)
			{
				if(valid_pixel(bkg, x + j, i + y))
				{
					int val;
					p = to_index(bkg, x + j, i + y);

					val = ROUND(bkg->val[p] + fg->val[q]);
					val = (val > UCHAR_MAX) ? UCHAR_MAX : val;

					bkg->val[p] = val;
				}

				q++;
			}
		}
	}

	void add_image(CImage8 *bkg, CImage8 *fg, int x, int y)
	{
		add_image(bkg->C[0], fg->C[0], x, y);
		add_image(bkg->C[1], fg->C[1], x, y);
		add_image(bkg->C[2], fg->C[2], x, y);
	}

	void and_image(Image8 *mask1, Image8 *mask2, uchar val)
	{
		int p, n;

		n = mask1->ncols * mask1->nrows;
		for(p = 0; p < n; p++)
			mask1->val[p] = (mask1->val[p] * mask2->val[p] > 0) ? val : 0;
	}


	void xor_image(Image8 *mask1, Image8 *mask2, uchar val)
	{
		int p, n;

		n = mask1->ncols * mask1->nrows;
		for(p = 0; p < n; p++)
		{
			uchar m1 = mask1->val[p];
			uchar m2 = mask2->val[p];
			mask1->val[p] = (m1 > 0 && m2 <= 0) ||(m2 > 0 && m1 <= 0) ? val : 0;
		}

	}

	CImage8 *blur(CImage8 *cimg, double radius, Rectangle bb)
	{
		CImage8 *f = copy_cimage8(cimg);

		blur_inplace(f, radius, bb);

		return f;
	}

	void blur_inplace(CImage8 *f, double radius, Rectangle bb)
	{
		int       i, j, p, q;
		Pixel     u, v;
		float    *w=NULL, d, K, sigma2, val;
		float    norm_factor = 0.0;
		AdjRel   *A = NULL;

		A  = circular(radius);
		w  = AllocFloatArray(A->n);

		sigma2 = (radius/3.0)*(radius/3.0);
		K      =  2.0*sigma2;

		for(i = 0; i < A->n; i++)
		{
			d    = A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i];
			w[i] = 1.0 / sqrt(2.0 * M_PI * sigma2) * exp(-d / K); // Gaussian
			norm_factor += w[i];
		}

		for(j=0; j < 3; j=j+1)
		{
			for(u.y = bb.beg.y; u.y <= bb.end.y; u.y++)
				for(u.x = bb.beg.x; u.x <= bb.end.x; u.x++)
				{
					p = to_index(f, u);

					val = 0.0;

					for(i = 0; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];

						if(valid_pixel(f,v))
						{
							q   = to_index(f,v);
							val += (float)f->C[j]->val[q] * w[i];
						}
					}

					f->C[j]->val[p] = (char)MIN(val/norm_factor, 255.0);
				}
		}

		free(w);

		destroy_adjrel(&A);
	}


}
