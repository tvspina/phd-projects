#include "usisgeometry.h"
#include "usiscommon.h"
#include <omp.h>

extern "C"
{
	#include "stackandfifo.h"
}

namespace usis
{

	Vector2D::Vector2D()
	{
		this->x = 0.0;
		this->y = 0.0;

	}

	Vector2D::Vector2D(double x, double y)
	{
		this->x = x;
		this->y = y;
	}

	Vector2D::Vector2D(Pixel px, Pixel center)
	{
		x = (int)(px.x - center.x);
		y = (int)(px.y - center.y);
	}

	bool Vector2D :: operator == (Vector2D &cmp)
	{
		return fabs(this->x - cmp.x) <= DBL_EPSILON
		       && fabs(this->y - cmp.y) <= DBL_EPSILON;
	}

	bool Vector2D :: operator != (Vector2D &cmp)
	{
		return !(*this == cmp);
	}

	void Vector2D :: operator *= (double val)
	{
		this->x *= val;
		this->y *= val;
	}

	void Vector2D :: operator /= (double val)
	{
		this->x /= val;
		this->y /= val;
	}

	Vector2D Vector2D :: operator *(double val)
	{
		return Vector2D(this->x * val, this->y * val);
	}

	Vector2D Vector2D :: operator / (double val)
	{
		return Vector2D((((double)this->x) / val), (((double)this->y) / val));
	}

	Vector2D Vector2D :: operator + (const Vector2D &vec1)
	{
		return Vector2D(this->x + vec1.x, this->y + vec1.y);
	}

	Vector2D Vector2D :: operator + (double val)
	{
		return Vector2D(this->x + val, this->y + val);
	}

	Vector2D Vector2D :: operator - (const Vector2D &vec1)
	{
		return Vector2D(this->x - vec1.x, this->y - vec1.y);
	}

	Vector2D Vector2D :: operator - (double val)
	{
		return Vector2D(this->x - val, this->y - val);
	}

	double Vector2D :: operator *(const Vector2D &vec1)
	{
		return (double)(this->x * vec1.x + this->y * vec1.y);
	}

	double Vector2D :: Norm()
	{
		return sqrt(this->x * this->x + this->y * this->y);
	}

	Vector2D Vector2D :: Normalize()
	{
		double norm = Norm();
		if(fabs(norm) >= DBL_EPSILON)
			return *this / Norm();
		else
			return *this;
	}

	double Vector2D :: Angle(Vector2D &vec)
	{
		int concurrent_vectors = Concurrent(vec);

		/** First evaluate if  both vectors are concurrent
		    to avoid rounding mistakes (that may cause a value
		    greater than 1.0 when computing the cosine of the
		    angle.
		 **/
		if(!concurrent_vectors)
		{
			double norm_factor = this->Norm() * vec.Norm();

			if(norm_factor >= DBL_EPSILON)
				return acos(((*this) * vec) / (this->Norm() * vec.Norm()));
			else
				return 0.0;
		}
		else
		{
			return (concurrent_vectors > 0) ? 0.0 : M_PI;
		}
	}
	double Vector2D :: Direction(Vector2D origin)
	{
		double angle = Angle(origin);
		double det = origin.x*this->y - origin.y*this->x; // cross product's determinant between
														// this vector and the origin
		// if the determinant is negative, than the angle is [pi,2*pi]
		// (cross product rule)
		if(det < 0.0)
			angle = 2 * M_PI - angle;

		return angle;
	}

	int Vector2D :: Concurrent(Vector2D &vec)
	{
		int direction = 0; /// By default we assume that
							/// the vectors are not concurrent
		Vector2D normalized = this->Normalize();
		Vector2D normalized_vec = vec.Normalize();
		Vector2D opposite_vec =  normalized_vec * (-1.0);

		/** The direction of vector @param vec w.r.t this one
		    is returned if they are concurrent
		 **/
		if(normalized == normalized_vec)
		{
			direction = 1;
		}
		else if(normalized == opposite_vec)
		{
			direction = -1;
		}

		return direction;
	}

	Pixel mask_centroid(Image8 *mask, int label)
	{
		int p, area = 0;

		Pixel centroid;
		centroid.x = 0;
		centroid.y = 0;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			if(mask->val[p] > 0 && (mask->val[p] == label || label < 0))
			{
				Pixel px = to_pixel(mask, p);
				centroid.x += px.x;
				centroid.y += px.y;
				area++;
			}
		}

		if(area > 0)
		{
			centroid.x = centroid.x / area;
			centroid.y = centroid.y / area;
		}
		else
		{
			/// if no mask element was found
			/// then the centroid should be
			/// the (NIL, NIL) should be returned
			centroid.x = NIL;
			centroid.y = NIL;
		}

		return centroid;
	}

	Pixel mask_centroid(Image32 *mask, int label)
	{
		int p, area = 0;

		Pixel centroid;
		centroid.x = 0;
		centroid.y = 0;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			if(mask->val[p] > 0 && (mask->val[p] == label || label < 0))
			{
				Pixel px = to_pixel(mask, p);
				centroid.x += px.x;
				centroid.y += px.y;
				area++;
			}
		}

		if(area > 0)
		{
			centroid.x = centroid.x / area;
			centroid.y = centroid.y / area;
		}
		else
		{
			/// if no mask element was found
			/// then the centroid should be
			/// the (NIL, NIL) should be returned
			centroid.x = NIL;
			centroid.y = NIL;
		}

		return centroid;
	}

	Vector2D transform_vector(float M[4][4], Vector2D p, bool round)
	{
		Vector2D np;

		/** TODO: Change -> Obtained from IFT where a Point had coordinates x,y,z
		    so 0 is used instead of z.
		 **/
		np.x = M[0][0] * p.x + M[0][1] * p.y + M[0][2] * 0 + M[0][3];
		np.y = M[1][0] * p.x + M[1][1] * p.y + M[1][2] * 0 + M[1][3];
//        np.z = M[2][0]*p.x + M[2][1]*p.y + M[2][2]*0 + M[2][3];

		if(round)
		{
			np.x = ROUND(np.x);
			np.y = ROUND(np.y);
		}
		return(np);
	}

	Pixel transform_point(float M[4][4], Pixel px)
	{
		Pixel px_transf;
		/** TODO: Change -> Obtained from IFT where a Point had coordinates x,y,z
		    so 0 is used instead of z.
		 **/
		px_transf.x = ROUND(M[0][0] * px.x + M[0][1] * px.y + M[0][2] * 0 + M[0][3]);
		px_transf.y = ROUND(M[1][0] * px.x + M[1][1] * px.y + M[1][2] * 0 + M[1][3]);

		return(px_transf);
	}

	Point3D transform_point(float M[4][4], Point3D p)
	{
		Point3D np;

		np.x = M[0][0] * p.x + M[0][1] * p.y + M[0][2] * p.z + M[0][3];
		np.y = M[1][0] * p.x + M[1][1] * p.y + M[1][2] * p.z + M[1][3];
		np.z = M[2][0] * p.x + M[2][1] * p.y + M[2][2] * p.z + M[2][3];

		return(np);
	}


	Image8 *transform_image(Image8 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels)
	{
		Image8 *transf = create_image8(ncols, nrows);

		if(selected_pixels != NULL)
			*selected_pixels = create_image8(ncols, nrows);

		transform_image(img, transf, M, selected_pixels);

		return transf;
	}

	void transform_image(Image8 *src, Image8 *dst, float M[4][4],
						Image8 **selected_pixels)
	{
		for(int p = 0; p < dst->n; p++)
		{
			float x, y, d1, d2, d3, d4, Ix1, Ix2, If;
			Pixel u, v, prev, next;
			Point3D orig, dest;

			v = to_pixel(dst, p);
			orig.x = v.x;
			orig.y = v.y;
			orig.z = 0;

			dest = transform_point(M, orig);

			x =  dest.x;
			y =  dest.y;
			u.x = (int)(x + 0.5);
			u.y = (int)(y + 0.5);

			if(valid_pixel(src, u.x, u.y))
			{
				if(x < u.x)
				{
					next.x = u.x;
					prev.x = u.x - 1;
				}
				else
				{
					next.x = u.x + 1;
					prev.x = u.x;
				}

				d1 = next.x - x;
				d2 = x - prev.x;

				if(y < u.y)
				{
					next.y = u.y;
					prev.y = u.y - 1;
				}
				else
				{
					next.y = u.y + 1;
					prev.y = u.y;
				}

				d3 = next.y - y;
				d4 = y - prev.y;

				if(valid_pixel(src, prev.x, prev.y) && valid_pixel(src, next.x, prev.y))
					Ix1 = d1 * src->val[to_index(src, prev)] +
						  d2 * src->val[to_index(src, next.x, prev.y)];
				else
					Ix1 = src->val[to_index(src, u)];

				if(valid_pixel(src, prev.x, next.y) && valid_pixel(src, next.x, next.y))
					Ix2 = d1 * src->val[to_index(src, prev.x, next.y)] +
						  d2 * src->val[to_index(src, next)];
				else
					Ix2 = src->val[to_index(src, u)];

				If = d3 * Ix1 + d4 * Ix2;

				dst->val[to_index(dst, v)] = ROUND(If);

				if(selected_pixels != NULL)
					(**selected_pixels)[to_index(*selected_pixels, v)] = 1;
			}
		}
	}


	Image32 *transform_image(Image32 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels)
	{
		Image32 *transf = create_image32(ncols, nrows);

		if(selected_pixels != NULL)
			*selected_pixels = create_image8(ncols, nrows);

		transform_image(img, transf, M, selected_pixels);

		return transf;
	}

	void transform_image(Image32 *src, Image32 *dst, float M[4][4],
						Image8 **selected_pixels)
	{
		for(int p = 0; p < dst->n; p++)
		{
			float x, y, d1, d2, d3, d4, Ix1, Ix2, If;
			Pixel u, v, prev, next;
			Point3D orig, dest;

			v = to_pixel(dst, p);
			orig.x = v.x;
			orig.y = v.y;
			orig.z = 0;
			dest = transform_point(M, orig);

			x =  dest.x;
			y =  dest.y;
			u.x = (int)(x + 0.5);
			u.y = (int)(y + 0.5);

			if(valid_pixel(src, u.x, u.y))
			{
				if(x < u.x)
				{
					next.x = u.x;
					prev.x = u.x - 1;
				}
				else
				{
					next.x = u.x + 1;
					prev.x = u.x;
				}

				d1 = next.x - x;
				d2 = x - prev.x;

				if(y < u.y)
				{
					next.y = u.y;
					prev.y = u.y - 1;
				}
				else
				{
					next.y = u.y + 1;
					prev.y = u.y;
				}

				d3 = next.y - y;
				d4 = y - prev.y;

				if(valid_pixel(src, prev.x, prev.y) && valid_pixel(src, next.x, prev.y))
					Ix1 = d1 * src->val[to_index(src, prev)] +
						  d2 * src->val[to_index(src, next.x, prev.y)];
				else
					Ix1 = src->val[to_index(src, u)];

				if(valid_pixel(src, prev.x, next.y) && valid_pixel(src, next.x, next.y))
					Ix2 = d1 * src->val[to_index(src, prev.x, next.y)] +
						  d2 * src->val[to_index(src, next)];
				else
					Ix2 = src->val[to_index(src, u)];

				If = d3 * Ix1 + d4 * Ix2;

				dst->val[to_index(dst, v)] = ROUND(If);

				if(selected_pixels != NULL)
					(**selected_pixels)[to_index(*selected_pixels, v)] = 1;

			}
		}
	}

	Image32 *transform_image_direct(Image32 *img, float M[4][4], int ncols, int nrows,
									Image8 **selected_pixels)
	{
		Image32 *transf = create_image32(ncols, nrows);

		if(selected_pixels != NULL)
			*selected_pixels = create_image8(ncols, nrows);

		transform_image_direct(img, transf, M, selected_pixels);

		return transf;
	}


	void transform_image_direct(Image32 *src, Image32 *dst, float M[4][4],
								Image8 **selected_pixels)
	{
		for(int p = 0; p < src->n; p++)
		{
			Pixel u, v;
			Point3D orig, dest;

			v = to_pixel(src, p);
			orig.x = v.x;
			orig.y = v.y;
			orig.z = 0;
			dest = transform_point(M, orig);

			u.x = (int)(dest.x + 0.5);
			u.y = (int)(dest.y + 0.5);

			if(valid_pixel(dst, u.x, u.y))
			{
				dst->val[to_index(dst, u)] = src->val[p];

				if(selected_pixels != NULL)
					(**selected_pixels)[to_index(*selected_pixels, v)] = 1;
			}
		}
	}

    CImage8 *transform_image(CImage8 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels)
    {
        CImage8 *transf = create_cimage8(ncols, nrows);

		if(selected_pixels != NULL)
			*selected_pixels = create_image8(ncols, nrows);

		transform_image(img, transf, M, selected_pixels);

		return transf;
    }

	void transform_image(CImage8 *src, CImage8 *out, float M[4][4],
						Image8 **selected_pixels)
	{
	    transform_image(src->C[0], out->C[0], M, selected_pixels);
	    transform_image(src->C[1], out->C[1], M);
        transform_image(src->C[2], out->C[2], M);
	}

	Features* transform_image(Features *src,float M[4][4], int ncols, int nrows,
								Image8 **selected_pixels)
	{
		Features *dst = CreateFeatures(ncols, nrows, src->nfeats);

		if(selected_pixels != NULL)
			*selected_pixels = create_image8(ncols, nrows);

	     transform_image(src, dst, M, selected_pixels);

	     return dst;
	}


	void transform_image(Features *src, Features *dst, float M[4][4],
						Image8 **selected_pixels)
	{
		for(int p = 0; p < dst->nelems; p++)
		{
			float x, y, d1, d2, d3, d4;
			Pixel u, v, prev, next;
			Point3D orig, dest;

			v = to_pixel(dst, p);
			orig.x = v.x;
			orig.y = v.y;
			orig.z = 0;

			dest = transform_point(M, orig);

			x =  dest.x;
			y =  dest.y;
			u.x = (int)(x + 0.5);
			u.y = (int)(y + 0.5);

			if(valid_pixel(src, u.x, u.y))
			{
				if(x < u.x)
				{
					next.x = u.x;
					prev.x = u.x - 1;
				}
				else
				{
					next.x = u.x + 1;
					prev.x = u.x;
				}

				d1 = next.x - x;
				d2 = x - prev.x;

				if(y < u.y)
				{
					next.y = u.y;
					prev.y = u.y - 1;
				}
				else
				{
					next.y = u.y + 1;
					prev.y = u.y;
				}

				d3 = next.y - y;
				d4 = y - prev.y;

				for(int i = 0; i < src->nfeats; i++)
				{
					float Ix1, Ix2, If;

					if(valid_pixel(src, prev.x, prev.y) && valid_pixel(src, next.x, prev.y))
						Ix1 = d1 * src->elem[to_index(src, prev)].feat[i] +
							  d2 * src->elem[to_index(src, next.x, prev.y)].feat[i];
					else
						Ix1 = src->elem[to_index(src, u)].feat[i];

					if(valid_pixel(src, prev.x, next.y) && valid_pixel(src, next.x, next.y))
						Ix2 = d1 * src->elem[to_index(src, prev.x, next.y)].feat[i] +
							  d2 * src->elem[to_index(src, next)].feat[i];
					else
						Ix2 = src->elem[to_index(src, u)].feat[i];

					If = d3 * Ix1 + d4 * Ix2;

					dst->elem[to_index(dst, v)].feat[i] = If;
				}
				if(selected_pixels != NULL)
					(**selected_pixels)[to_index(*selected_pixels, v)] = 1;
			}
		}

		dst->Fmax = src->Fmax;
		dst->Fmin = src->Fmin;
	}


	//Rotacao e translacao em imagens
	void inverse(float M[4][4], float IM[4][4])
	{

		float detM;
		float aux[4][4], M_cofatores[4][4], Trans_cofat[4][4];
		int i, j;

		for(i = 0; i < 4; i++)
		{
			for(j = 0; j < 4; j++)
			{
				aux[i][j] = M[i][j];
				//printf("[%f] ",M[i][j]);
			}
			//printf("\n");
		}
		//calcula o determinante de M (a ultima linha de M é 0 0 0 1 - por laplace faco 1*Cofator M3x3)
		detM = aux[0][0] * aux[1][1] * aux[2][2] + aux[0][1] * aux[1][2] * aux[2][0] + aux[0][2] * aux[1][0] * aux[2][1]
		       - aux[0][0] * aux[1][2] * aux[2][1] - aux[0][1] * aux[1][0] * aux[2][2] - aux[0][2] * aux[1][1] * aux[2][0];

		if(fabs(detM) < FLT_EPSILON)
			throw string("Determinant 0 when computing the inverse matrix. Function: inverse");

		// calculo da matriz dos cofatores
		for(i = 0; i < 4; i++)
		{
			for(j = 0; j < 4; j++)
			{
				M_cofatores[i][j] = cofactor(aux, i, j);
			}
		}

		//Transposta da Matriz dos cofatores
		trans_matrix(M_cofatores, Trans_cofat);

		//dividir a Matriz Transposta pelo determinante de M
		for(i = 0; i < 4; i++)
		{
			for(j = 0; j < 4; j++)
			{
				IM[i][j] = Trans_cofat[i][j] / detM;
			}

		}

	}

	float cofactor(float M[4][4], int l, int c)
	{

		float aux[3][3];
		int linhas[3] = {0, 0, 0}, colunas[3] = {0, 0, 0};
		int i, j;
		float cof = 0;
		int aux_sinal;

		if(l == 0)
		{
			linhas[0] = 1;
			linhas[1] = 2;
			linhas[2] = 3;
		}
		else if(l == 1)
		{
			linhas[0] = 0;
			linhas[1] = 2;
			linhas[2] = 3;
		}
		else if(l == 2)
		{
			linhas[0] = 0;
			linhas[1] = 1;
			linhas[2] = 3;
		}
		else if(l == 3)
		{
			linhas[0] = 0;
			linhas[1] = 1;
			linhas[2] = 2;
		}
		if(c == 0)
		{
			colunas[0] = 1;
			colunas[1] = 2;
			colunas[2] = 3;
		}
		else if(c == 1)
		{
			colunas[0] = 0;
			colunas[1] = 2;
			colunas[2] = 3;
		}
		else if(c == 2)
		{
			colunas[0] = 0;
			colunas[1] = 1;
			colunas[2] = 3;
		}
		else if(c == 3)
		{
			colunas[0] = 0;
			colunas[1] = 1;
			colunas[2] = 2;
		}

		for(i = 0; i < 3; i++)
		{
			for(j = 0; j < 3; j++)
			{
				aux[i][j] = M[linhas[i]][colunas[j]];
			}
		}

		cof = aux[0][0] * aux[1][1] * aux[2][2] + aux[0][1] * aux[1][2] * aux[2][0] + aux[0][2] * aux[1][0] * aux[2][1]
		      - aux[0][0] * aux[1][2] * aux[2][1] - aux[0][1] * aux[1][0] * aux[2][2] - aux[0][2] * aux[1][1] * aux[2][0];

		aux_sinal = l + c;
		if((aux_sinal % 2) == 1)
			cof = -1 * cof;
		return(cof);
	}

	void trans_matrix(float M1[4][4], float M2[4][4])
	{
		int i, j;

		for(j = 0; j < 4; j++)
			for(i = 0; i < 4; i++)
				M2[i][j] = M1[j][i];
	}

	void translation(float T[4][4], float dx, float dy, float dz)
	{
		T[0][0] = 1;
		T[1][0] = 0;
		T[2][0] = 0;
		T[3][0] = 0;

		T[0][1] = 0;
		T[1][1] = 1;
		T[2][1] = 0;
		T[3][1] = 0;

		T[0][2] = 0;
		T[1][2] = 0;
		T[2][2] = 1;
		T[3][2] = 0;

		T[0][3] = dx;
		T[1][3] = dy;
		T[2][3] = dz;
		T[3][3] = 1;
	}

	void rot_x(float Rx[4][4], float thx)
	{
		if(thx < 0)
			thx = 2*M_PI + thx;

		Rx[0][0] = 1;
		Rx[1][0] = 0;
		Rx[2][0] = 0;
		Rx[3][0] = 0;

		Rx[0][1] = 0;
		Rx[1][1] = cos(thx);
		Rx[2][1] = sin(thx);
		Rx[3][1] = 0;

		Rx[0][2] = 0;
		Rx[1][2] = -sin(thx);
		Rx[2][2] = cos(thx);
		Rx[3][2] = 0;

		Rx[0][3] = 0;
		Rx[1][3] = 0;
		Rx[2][3] = 0;
		Rx[3][3] = 1;
	}

	void rot_y(float Ry[4][4], float thy)
	{
		if(thy < 0.0)
			thy = 2*M_PI + thy;

		Ry[0][0] = cos(thy);
		Ry[1][0] = 0;
		Ry[2][0] = -sin(thy);
		Ry[3][0] = 0;

		Ry[0][1] = 0;
		Ry[1][1] = 1;
		Ry[2][1] = 0;
		Ry[3][1] = 0;

		Ry[0][2] = sin(thy);
		Ry[1][2] = 0;
		Ry[2][2] = cos(thy);
		Ry[3][2] = 0;

		Ry[0][3] = 0;
		Ry[1][3] = 0;
		Ry[2][3] = 0;
		Ry[3][3] = 1;
	}

	void rot_z(float Rz[4][4], float thz)
	{
		if(thz < 0.0)
			thz = 2*M_PI + thz;

		Rz[0][0] = cos(thz);
		Rz[1][0] = sin(thz);
		Rz[2][0] = 0;
		Rz[3][0] = 0;

		Rz[0][1] = -sin(thz);
		Rz[1][1] = cos(thz);
		Rz[2][1] = 0;
		Rz[3][1] = 0;

		Rz[0][2] = 0;
		Rz[1][2] = 0;
		Rz[2][2] = 1;
		Rz[3][2] = 0;

		Rz[0][3] = 0;
		Rz[1][3] = 0;
		Rz[2][3] = 0;
		Rz[3][3] = 1;
	}

	void scl_matrix(float S[4][4], float sx, float sy, float sz)
	{
		S[0][0] = sx;
		S[1][0] = 0;
		S[2][0] = 0;
		S[3][0] = 0;

		S[0][1] = 0;
		S[1][1] = sy;
		S[2][1] = 0;
		S[3][1] = 0;

		S[0][2] = 0;
		S[1][2] = 0;
		S[2][2] = sz;
		S[3][2] = 0;

		S[0][3] = 0;
		S[1][3] = 0;
		S[2][3] = 0;
		S[3][3] = 1;
	}

	void mult_matrices(float M1[4][4], float M2[4][4], float M3[4][4])
	{
		int i, j, k;


		for(k = 0; k < 4; k++)
			for(j = 0; j < 4; j++)
				M3[k][j] = 0.0;

		for(k = 0; k < 4; k++)
			for(j = 0; j < 4; j++)
				for(i = 0; i < 4; i++)
					M3[k][j] += M1[k][i] * M2[i][j];

	}

	void scl_rot_trans(float sx, float sy, float th, float dx, float dy,
	                   float T[4][4], int x_in, int y_in, int x_out, int y_out,
	                   bool direct)
	{
		float Tc[4][4], To[4][4], S[4][4], R[4][4], Trans[4][4];
		float M1[4][4], M2[4][4], M3[4][4], M4[4][4];

		//transladar o centro do objeto para a origem
		translation(Tc, -x_in, -y_in, 0);

		// Escala
		scl_matrix(S, sx, sy, 1);

		//Rotacao
		rot_z(R, th);

		//Translacao em dx dy
		translation(Trans, dx, dy, 0);

		//tranladar para o centro da imagem de saida
		translation(To, x_out, y_out, 0);

		//multiplicar as matrizes
		mult_matrices(To, Trans, M1);
		mult_matrices(M1, R, M2);
		mult_matrices(M2, S, M3);
		mult_matrices(M3, Tc, M4);

		if(!direct)
		{
			inverse(M4, T);
		}
		else
		{
			/** If the transformation is supposed to be direct,
			     then simply copy M4 to T.
			  **/
			int i, j;
			for(i = 0; i < 4; i++)
			{
				for(j = 0; j < 4; j++)
					T[i][j] = M4[i][j];
			}
		}
	}

	void scl_rot_axis(float sx, float sy, float th, float th_axis,
	                  float T[4][4], int x_in, int y_in, int x_out, int y_out,
	                  bool direct)
	{
		float Tc[4][4], To[4][4], S[4][4], R[4][4], Raxis[4][4];
		float M1[4][4], M2[4][4], M3[4][4], M4[4][4];

		//Translating the object's center to the image's center
		translation(Tc, -x_in, -y_in, 0);

		//Rotation matrix equivalent to rotating by th (rot_z(th))
		//and then by -th_axis (rot_z(-th_axis))
		rot_z(R, th - th_axis);

		// Scaling
		scl_matrix(S, sx, sy, 1);

		//Rotating to align the object's axis with the image's axis
		rot_z(Raxis, th_axis);

		//Translating to the output image coordinates
		translation(To, x_out, y_out, 0);

		//Multiplying matrices in order To*R(-th_axis)*R(th)*S*R(th_axis)*Tc
		mult_matrices(To, R, M1);
		mult_matrices(M1, S, M2);
		mult_matrices(M2, Raxis, M3);
		mult_matrices(M3, Tc, M4);

		if(!direct)
		{
			inverse(M4, T);
		}
		else
		{
			/** If the transformation is supposed to be direct,
			     then simply copy M4 to T.
			  **/
			int i, j;
			for(i = 0; i < 4; i++)
			{
				for(j = 0; j < 4; j++)
					T[i][j] = M4[i][j];
			}
		}
	}

	void transformed_dimensions(Rectangle bb, Pixel px_center, float T[4][4],
								int *final_w, int *final_h, int padding)
	{
		Pixel corners[4], beg, end;
		double dx, dy;

		corners[0].x = bb.beg.x;
		corners[0].y = bb.beg.y;

		corners[1].x = bb.end.x;
		corners[1].y = bb.beg.y;

		corners[2].x = bb.beg.x;
		corners[2].y = bb.end.y;

		corners[3].x = bb.end.x;
		corners[3].y = bb.end.y;

		beg.x = INT_MAX;
		beg.y = INT_MAX;
		end.x = INT_MIN;
		end.y = INT_MIN;

		for(int i = 0; i < 4; i++)
		{
			Vector2D vec = Vector2D(corners[i], px_center);

			vec = transform_vector(T, vec, true);

			beg.x = MIN(beg.x, vec.x);
			beg.y = MIN(beg.y, vec.y);

			end.x = MAX(end.x, vec.x);
			end.y = MAX(end.y, vec.y);
		}

		// Guaranteeing that the original center will be
		// transported to the transformed image's center.
		// This is true because we are taking the maximum
		// absolute transformed (x,y) corner values
		// w.r.t. the origin and using them (plus padding)
		// as half the width/height of the new image.
		dx = MAX(fabs(end.x), fabs(beg.x));
		dy = MAX(fabs(end.y), fabs(beg.y));

		*final_w = 2*(dx + padding) + 1;
		*final_h = 2*(dy + padding) + 1;
	}

	void mask_bounding_box(Image8 *mask, Pixel *beg, Pixel *end, int val)
	{
		int pmin, pmax;
		set<int> values;
		values.insert(val);
		if(val > 0)
			mask_bounding_box(mask, &pmin, &pmax, &values);
		else
			mask_bounding_box(mask, &pmin, &pmax, (set<int>*)NULL);
		*beg = to_pixel(mask, pmin);
		*end = to_pixel(mask, pmax);
	}

	void mask_bounding_box(Image32 *mask, Pixel *beg, Pixel *end, int val)
	{
		int pmin, pmax;
		set<int> values;
		values.insert(val);
		if(val > 0)
			mask_bounding_box(mask, &pmin, &pmax, &values);
		else
			mask_bounding_box(mask, &pmin, &pmax, (set<int>*)NULL);

		*beg = to_pixel(mask, pmin);
		*end = to_pixel(mask, pmax);
	}

	void mask_bounding_box(Image8 *mask, Pixel *beg, Pixel *end, set<int> values)
	{
		int pmin, pmax;

		mask_bounding_box(mask, &pmin, &pmax, &values);

		*beg = to_pixel(mask, pmin);
		*end = to_pixel(mask, pmax);
	}

	void mask_bounding_box(Image32 *mask, Pixel *beg, Pixel *end, set<int> values)
	{
		int pmin, pmax;
		mask_bounding_box(mask, &pmin, &pmax, &values);

		*beg = to_pixel(mask, pmin);
		*end = to_pixel(mask, pmax);
	}

	void mask_bounding_box(Image8 *mask, int *pmin, int *pmax, int val)
	{
		set<int> values;
		values.insert(val);
		if(val > 0)
			mask_bounding_box(mask, pmin, pmax, &values);
		else
			mask_bounding_box(mask, pmin, pmax, (set<int>*)NULL);
	}

	void mask_bounding_box(Image32 *mask, int *pmin, int *pmax, int val)
	{
		set<int> values;
		values.insert(val);
		if(val > 0)
			mask_bounding_box(mask, pmin, pmax, &values);
		else
			mask_bounding_box(mask, pmin, pmax, (set<int>*)NULL);
	}

	void mask_bounding_box(Image8 *mask, int *pmin, int *pmax, set<int> *values)
	{
		int xmin = INT_MAX, xmax = INT_MIN;
		int ymin = INT_MAX, ymax = INT_MIN;
		int p;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			Pixel px = to_pixel(mask, p);

			int v = mask->val[p];

			if((values != NULL && (values->find(v) != values->end()))||
				(values == NULL && v > 0))
			{
				if(px.x < xmin) xmin = px.x;
				else if(px.x > xmax) xmax = px.x;

				if(px.y < ymin) ymin = px.y;
				else if(px.y > ymax) ymax = px.y;
			}
		}

		if(xmin == INT_MAX && ymin == INT_MAX && xmax ==  INT_MIN && ymax == INT_MIN)
		{
			*pmin = 0;
			*pmax = mask->ncols * mask->nrows - 1;
		}
		else
		{
			*pmin = to_index(mask, xmin, ymin);
			*pmax = to_index(mask, xmax, ymax);
		}
	}

	void mask_bounding_box(Image32 *mask, int *pmin, int *pmax, set<int> *values)
	{
		int xmin = INT_MAX, xmax = INT_MIN;
		int ymin = INT_MAX, ymax = INT_MIN;
		int p;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			Pixel px = to_pixel(mask, p);

			int v = mask->val[p];

			if((values != NULL && (values->find(v) != values->end()))||
				(values == NULL && v > 0))
			{
				if(px.x < xmin) xmin = px.x;
				else if(px.x > xmax) xmax = px.x;

				if(px.y < ymin) ymin = px.y;
				else if(px.y > ymax) ymax = px.y;
			}
		}

		if(xmin == INT_MAX && ymin == INT_MAX && xmax ==  INT_MIN && ymax == INT_MIN)
		{
			*pmin = 0;
			*pmax = mask->ncols * mask->nrows - 1;
		}
		else
		{
			*pmin = to_index(mask, xmin, ymin);
			*pmax = to_index(mask, xmax, ymax);
		}
	}

	Image8 *mask_bounding_box(Image8 *mask, int val)
	{
		int pmin, pmax;

		set<int> values;
		values.insert(val);
		if(val > 0)
			mask_bounding_box(mask, &pmin, &pmax, &values);
		else
			mask_bounding_box(mask, &pmin, &pmax, (set<int>*)NULL);

		int xl, yl, xr, yr;

		to_pixel(mask, pmin, &xl, &yl);
		to_pixel(mask, pmax, &xr, &yr);

		return ROI(mask, xl, yl, xr, yr);
	}

	Image8 *mask_bounding_box(Image8 *mask, set<int> values)
	{
		int pmin, pmax;

		mask_bounding_box(mask, &pmin, &pmax, &values);

		int xl, yl, xr, yr;

		to_pixel(mask, pmin, &xl, &yl);
		to_pixel(mask, pmax, &xr, &yr);

		return ROI(mask, xl, yl, xr, yr);
	}

	Image32 *mask_bounding_box(Image32 *mask, int val)
	{
		int pmin, pmax;

		mask_bounding_box(mask, &pmin, &pmax, val);

		int xl, yl, xr, yr;

		to_pixel(mask, pmin, &xl, &yl);
		to_pixel(mask, pmax, &xr, &yr);

		return ROI(mask, xl, yl, xr, yr);

	}


	Image32 *mask_bounding_box(Image32 *mask, set<int> values)
	{
		int pmin, pmax;

		mask_bounding_box(mask, &pmin, &pmax, &values);

		int xl, yl, xr, yr;

		to_pixel(mask, pmin, &xl, &yl);
		to_pixel(mask, pmax, &xr, &yr);

		return ROI(mask, xl, yl, xr, yr);

	}


	Image8 *ROI(Image8 *img, int xl, int yl, int xr, int yr)
	{
		return ROI<Image8, Image8::value_type>(img, Rectangle(xl, yl, xr, yr));
	}


	CImage8 *ROI(CImage8 *img, int xl, int yl, int xr, int yr)
	{
		Rectangle bb = Rectangle(xl, yl, xr, yr);

		CImage8* cropped = create_cimage8(bb.end.x-bb.beg.x+1, bb.end.y-bb.beg.y+1);

		for(int y = bb.beg.y; y <= bb.end.y; y++)
			for(int x = bb.beg.x; x <= bb.end.x; x++)
			{
				int p0 = to_index(cropped, x-bb.beg.x, y-bb.beg.y);
				int p = to_index(img, x,y);

				for(int i = 0; i < 3; i++)
					cropped->C[i]->val[p0] = img->C[i]->val[p];
			}

		return cropped;
	}

	Image32 *ROI(Image32 *img, int xl, int yl, int xr, int yr)
	{
		return ROI<Image32, Image32::value_type>(img, Rectangle(xl, yl, xr, yr));
	}

	Features *ROI(Features *f, Rectangle _bb)
	{
		Rectangle bb = Rectangle(MAX(_bb.beg.x, 0), MAX(_bb.beg.y, 0),
								MIN((_bb.end.x <= 0) ? f->ncols-1 : _bb.end.x, f->ncols-1),
								MIN((_bb.end.y <= 0) ? f->nrows-1 : _bb.end.y, f->nrows-1));

		Features* cropped = CreateFeatures(bb.end.x-bb.beg.x+1, bb.end.y-bb.beg.y+1, f->nfeats);

		for(int y = bb.beg.y; y <= bb.end.y; y++)
			for(int x = bb.beg.x; x <= bb.end.x; x++)
			{
				int p0 = to_index(cropped, x-bb.beg.x, y-bb.beg.y);
				int p = to_index(f, x,y);

				for(int i = 0; i < f->nfeats; i++)
					cropped->elem[p0].feat[i] = f->elem[p].feat[i];
			}

		cropped->Fmax = f->Fmax;
		cropped->Fmin = f->Fmin;

		return cropped;
	}

	int mask_area(Image8 *mask)
	{
		int p, area = 0;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			if(mask->val[p] > 0)
				area++;
		}

		return area;
	}

	int mask_area(Image32 *mask)
	{
		int p, area = 0;

		for(p = 0; p < mask->ncols * mask->nrows; p++)
		{
			if(mask->val[p] > 0)
				area++;
		}

		return area;
	}


	int label_area(Image8 *mask, int label)
	{
		int p, area = 0;

		for(p = 0; p < mask->n; p++)
		{
			if(mask->val[p] == label)
				area++;
		}

		return area;
	}

	int label_area(Image32 *mask, int label)
	{
		int p, area = 0;

		for(p = 0; p < mask->n; p++)
		{
			if(mask->val[p] == label)
				area++;
		}

		return area;
	}
	map<int, int> label_areas(Image32 *label)
	{
		int p;
		map<int, int> areas;

		for(p = 0; p < label->n; p++)
		{
			if(areas.find(label->val[p]) == areas.end())
				areas[label->val[p]] = 1;
			else
				areas[label->val[p]]++;
		}

		return areas;
	}

	list<int> component_connected_to_pixel(Image32 *mask, Pixel px, int val,
											AdjRel *A)
	{
		int p = to_index(mask, px);
		bool *visited = (bool*)calloc(mask->n,sizeof(bool));
		list<int> component;
		FIFOQ *Q = FIFOQNew(mask->n);

		FIFOQPush(Q, p);
		component.push_back(p);

		while(!FIFOQEmpty(Q))
		{
			Pixel u;
			p = FIFOQPop(Q);

			u = to_pixel(mask, p);

			for(int i = 1; i < A->n; i++)
			{
				Pixel v;

				v.x = u.x + A->dx[i];
				v.y = u.y + A->dy[i];

				if(valid_pixel(mask, v))
				{
					int q = to_index(mask, v);

					if(mask->val[q] == val && !visited[q])
					{
						component.push_back(q);
						visited[q] = true;
						FIFOQPush(Q,q);
					}
				}
			}
		}

		free(visited);
		FIFOQDestroy(&Q);

		return component;
	}


	void line_intersection(Pixel p1, double a1, Pixel p2, double a2,
	                       double *x_prime, double *y_prime)
	{
		double b1, b2;

		b1 = p1.y - a1 * p1.x;
		b2 = p2.y - a2 * p2.x;

		*x_prime = (b2 - b1) / (a1 - a2);
		*y_prime = a2 * (*x_prime) + b2;
	}

	double point_dist_2_line(Pixel center, double angle, Pixel px,
	                         double degree_epsilon)
	{
		double value;
		/** Vertical line treatment **/
		if(fabs(angle - M_PI / 2) <= degree_epsilon
		        || fabs(angle - (-M_PI / 2)) <= degree_epsilon
		        || fabs(angle - (3 * M_PI / 2)) <= degree_epsilon)
		{
			value = fabs((double)(px.x - center.x));
		}
		else
		{
			double a = tan(angle);
			value = a * (double)(px.x - center.x) + (double)(center.y - px.y);
		}

		return fabs(value);
	}

	double point_set_diameter(std::tr1::unordered_set<int> &point_set, Image8 *mask,
	                          Pixel *px1, Pixel *px2)
	{
		double diam = -USIS_DINFTY;
		std::tr1::unordered_set<int>::iterator it;
		int p1 = 0, p2 = 0;

		for(it = point_set.begin(); it != point_set.end(); it++)
		{
			std::tr1::unordered_set<int>::iterator it2;
			for(it2 = point_set.begin(); it2 != point_set.end(); it2++)
			{
				double dist;
				dist = eucl_dist2(mask, *it, *it2);
				if(dist > diam)
				{
					diam = dist;
					p1 = *it;
					p2 = *it2;
				}
			}
		}

		*px1 = to_pixel(mask, p1);
		*px2 = to_pixel(mask, p2);

		return sqrt(diam);
	}

	Pixel accumulate_pixel(Pixel px0, Pixel px1)
	{
		return (Pixel)
		{
			px0.x + px1.x, px0.y + px1.y
		};
	}

	Vector2D accumulate_vector(Vector2D vec0, Vector2D vec1)
	{
		return vec0 + vec1;
	}

	std::pair<double, double> point_set_variance(std::list<Pixel> &point_set)
	{
		size_t npts = point_set.size();
		Pixel sum = std::accumulate(point_set.begin(), point_set.end(),
		                            (Pixel)
		{
			0, 0
		}, accumulate_pixel);

		double x_avg = ((double)sum.x) / MAX(npts, 1);
		double y_avg = ((double)sum.y) / MAX(npts, 1);
		double x_var = 0.0;
		double y_var = 0.0;

		std::list<Pixel>::iterator it;

		for(it = point_set.begin(); it != point_set.end(); it++)
		{
			Pixel px = *it;

			x_var += ((double)px.x - x_avg) * ((double)px.x - x_avg);
			y_var += ((double)px.y - y_avg) * ((double)px.y - y_avg);
		}

		x_var = x_var / MAX(npts - 1, 1);
		y_var = y_var / MAX(npts - 1, 1);

		return std::make_pair(x_var, y_var);
	}


	std::pair<double, double> point_set_variance(std::list<Vector2D> &point_set)
	{
		size_t npts = point_set.size();
		Vector2D sum = std::accumulate(point_set.begin(), point_set.end(),
		                               Vector2D(0, 0), accumulate_vector);
		double x_avg = ((double)sum.x) / MAX(npts, 1);
		double y_avg = ((double)sum.y) / MAX(npts, 1);
		double x_var = 0.0;
		double y_var = 0.0;

		std::list<Vector2D>::iterator it;

		for(it = point_set.begin(); it != point_set.end(); it++)
		{
			Vector2D vec = *it;

			x_var += ((double)vec.x - x_avg) * ((double)vec.x - x_avg);
			y_var += ((double)vec.y - y_avg) * ((double)vec.y - y_avg);
		}

		x_var = x_var / MAX(npts - 1, 1);
		y_var = y_var / MAX(npts - 1, 1);

		return std::make_pair(x_var, y_var);
	}

}
