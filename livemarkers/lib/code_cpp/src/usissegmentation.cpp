/*
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
*/
#include "usissegmentation.h"

/** SegData auxliary functions **/
namespace usis
{

SegData :: SegData(bool free)
{
    this->grad = NULL;
    this->cost = NULL;
    this->label = NULL;
    this->pred = NULL;
    this->root = NULL;
    this->regions = NULL;
    this->objMap = NULL;
    this->fcost = NULL;
    this->ift_radius = 1.5;

    this->feat = NULL;
    this->seeds = MarkerSet();

    SetFreeData(free);
}

SegData :: ~SegData()
{
    if(m_free_data)
    {
        destroy_image(&this->grad);
        destroy_image(&this->cost);
        destroy_image(&this->label);
        destroy_image(&this->pred);
        destroy_image(&this->root);
        destroy_image(&this->regions);
        destroy_image(&this->objMap);
        DestroyFeatures(&this->feat);
        this->seeds.clear();
        USIS_UDESTROY(&this->fcost);
    }
}

void SegData :: SetFreeData(bool free)
{
    this->m_free_data = free;
}


/** Interface for the cloud's delineation algorithm **/

DelineationAlg :: DelineationAlg()
{
	this->A = NULL;
    this->data = NULL;
}

DelineationAlg :: ~DelineationAlg()
{
	destroy_adjrel(&this->A);
}

void DelineationAlg :: SetData(SegData *seg_data)
{
    this->data = seg_data;
}

/** IFT-SC using GQueue **/

DelineationIFT_SC :: DelineationIFT_SC(int nbuckets, int n, int *cost,
                                       float(*cost_function)(SegData *, float, float))
{
    this->H = NULL;
    this->fcost = NULL;
    this->Q = CreateGQueue(nbuckets, n, cost);
    this->m_cost_function = cost_function;
}

DelineationIFT_SC :: DelineationIFT_SC(int n, float(*cost_function)(SegData *, float, float),
										double fsum_power)
{
    this->Q = NULL;
    this->fcost = alloc_float_array(n);
    this->H = CreateRealHeap(n, this->fcost);
    this->m_cost_function = cost_function;
    this->fsum_power = fsum_power;
}

DelineationIFT_SC :: ~DelineationIFT_SC()
{
    if(this->Q != NULL) DestroyGQueue(&(this->Q));
    if(this->H != NULL) DestroyRealHeap(&(this->H));
    if(this->fcost != NULL) free_array_data(fcost);
}

double DelineationIFT_SC :: DelineateAndRecognize()
{
    double score;

	if(this->Q != NULL)
		score = ift_sc_and_mean_cut(this->data, A, &(this->Q),
									dist_grad, m_cost_function);
	else
		score = ift_sc_and_mean_cut(this->data, A, this->H,
									dist_grad, m_cost_function);

    return score/USIS_MAXGRAD;

}

void DelineationIFT_SC :: SetBarrier()
{
    if(this->fcost != NULL)
    {
        for(int i = 0; i < this->data->label->ncols * this->data->label->nrows; i++)
            this->fcost[i] = -USIS_FINFTY;
    }
    else
    {
        set_image(data->cost, INT_MIN);
    }
}

void DelineationIFT_SC :: SetBarrier(int p, uchar label)
{
    if(this->fcost != NULL)
    {
        this->fcost[p] = -USIS_FINFTY;
    }
    else
    {
        this->data->cost->val[p] = INT_MIN;
    }

    this->data->label->val[p] = label;
}

void DelineationIFT_SC :: SetSeed(int p, uchar label, int mk_id)
{
    if(this->fcost != NULL)
    {
        this->fcost[p] = 0;
    }
    else
    {
        this->data->cost->val[p] = 0;
    }

	MarkerData marker_data;

	marker_data.lb = label;
	marker_data.mk_id = mk_id;

    this->data->label->val[p] = label;
	this->data->seeds[p] = marker_data;
}

void DelineationIFT_SC :: SetUnknownSample(int p)
{
    if(this->fcost != NULL)
    {
        this->fcost[p] = FLT_MAX;
    }
    else
    {
        this->data->cost->val[p] = INT_MAX;
    }
}

void DelineationIFT_SC :: SetData(SegData *seg_data)
{
    DelineationAlg::SetData(seg_data);

    if(this->data != NULL && this->fcost != NULL)
    {
        this->data->fcost = this->fcost;
    }

	destroy_adjrel(&this->A);
    this->A = circular(seg_data->ift_radius);
	this->data->power = fsum_power;
}

void DelineationIFT_SC :: ResetData()
{
    set_image(this->data->label, 0);
    set_image(this->data->pred, NIL);
    set_image(this->data->root, NIL);

    if(this->fcost != NULL)
    {
        this->data->fcost = this->fcost;

        for(int i = 0; i < this->data->label->n; i++)
            this->data->fcost[i] = FLT_MAX;

        ResetRealHeap(this->H);
    }
    else
    {
        set_image(this->data->cost, INT_MAX);

        ResetGQueue(this->Q);
    }
}

bool DelineationIFT_SC :: ResetData(Pixel px)
{
    bool val_pixel = valid_pixel(this->data->label, px);

    if(val_pixel)
    {
        int p = to_index(this->data->label, px);

        if(this->fcost != NULL)
        {
            this->fcost[p] = FLT_MAX;
        }
        else
        {
            this->data->cost->val[p] = INT_MAX;
        }

        this->data->label->val[p] = 0;
        this->data->pred->val[p] = NIL;
        this->data->root->val[p] = NIL;
    }

    return val_pixel;
}

bool DelineationIFT_SC :: IsSeed(int p)
{
    if(this->fcost != NULL)
        return this->fcost[p] == 0.0;
    else
        return this->data->cost->val[p] == 0;

}

Set *forest_removal(SegData *data, AdjRel *A, MarkerSet delSet)
{
    Set *Frontier = NULL; /// frontier pixels
    Image32 *cost = data->cost;
    Image32 *pred = data->pred;
    Image32 *root = data->root;

    int ncols = cost->ncols;

    FIFOQ *T = FIFOQNew(cost->ncols * cost->nrows);

    int i, p;

    MarkerSet::iterator aux = delSet.begin();

    for(; aux != delSet.end(); aux++)
    {
        p = aux->first;
        FIFOQPush(T, p);
        cost->val[p] = INT_MAX;
        pred->val[p] = NIL;
    }

    while(!FIFOQEmpty(T))
    {
        p = FIFOQPop(T);

        Pixel u;
        u.x = p % ncols;
        u.y = p / ncols;

        for(i = 1; i < A->n; i++)
        {
            Pixel v;

            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(cost, v))
            {
                int q = to_index(cost, v);

                if(pred->val[q] == p)
                {
                    cost->val[q] = INT_MAX;
                    pred->val[q] = NIL;
                    FIFOQPush(T, q);
                }
                else if(delSet.find(root->val[q]) == delSet.end())
                {
                    InsertSet(&Frontier, q);
                }
            }
        }
    }

    FIFOQDestroy(&T);

    T = NULL;

    return Frontier;
}

void differential_ift(SegData *data, AdjRel *A, int nbuckets, MarkerSet delSet,
                      float(*weight_function)(SegData*, int, int),
                      float(*cost_function)(SegData*, float, float))
{
    GQueue *Q = NULL;
    int n    = data->grad->n;
    Q    = CreateGQueue(nbuckets, n, data->cost->val);

    dift(data, A, delSet, &Q, weight_function, cost_function);

    DestroyGQueue(&Q);
}

void dift(SegData *data, AdjRel *A, MarkerSet delSet, GQueue **Q,
          float(*weight_function)(SegData*, int, int),
          float(*cost_function)(SegData*, float, float))
{
    int i, p, q, cst, weight;

    Image8 *label = data->label;
    Image32 *cost = data->cost;
    Image32 *pred = data->pred;
    Image32 *root = data->root;

    MarkerSet::iterator seed_aux;
    Pixel u, v;
    Set *Saux = NULL;

    Set *F = forest_removal(data, A, delSet);

    for(seed_aux = data->seeds.begin(); seed_aux != data->seeds.end(); seed_aux++)
    {
        int seed_pixel = USIS_SEED_INDEX(seed_aux);
        cost->val[seed_pixel]  = 0; //Marker imposition.
        pred->val[seed_pixel] = NIL;
        root->val[seed_pixel] = seed_pixel;
        (*label)[seed_pixel] = USIS_SEED_LABEL(seed_aux);
        InsertGQueue(Q, seed_pixel);
    }

    for(Saux = F; Saux != NULL; Saux = Saux->next)
    {
        /// Frontier pixels should either have already left
        /// the Q in a previous iteration or are not part
        /// of the seed set, hence, they should be BLACK or WHITE
        if((*Q)->L.elem[Saux->elem].color != GRAY)
            InsertGQueue(Q, Saux->elem);

    }

    while(!EmptyGQueue(*Q))
    {
        p = RemoveGQueue(*Q);

        u.x = p % label->ncols;
        u.y = p / label->ncols;
        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];
            if(valid_pixel(label, v))
            {
                q = to_index(label, v);

                weight = (int)weight_function(data, p, q);

                cst = cost_function(data, cost->val[p], weight);

                if((cst < cost->val[q]) || (pred->val[q] == p))
                {
                    if((*Q)->L.elem[q].color == GRAY)
                        RemoveGQueueElem(*Q, q);

                    cost->val[q]  = cst;
                    pred->val[q]  = p;
                    label->val[q] = label->val[p];
                    root->val[q] = root->val[p];
                    InsertGQueue(Q, q);
                }
            }
        }
    }

    DestroySet(&F);
}

Set *forest_removal_heap(SegData *data, AdjRel *A, MarkerSet delSet)
{
    Set *Frontier = NULL; /// frontier pixels
    float *fcost = data->fcost;
    Image32 *pred = data->pred;
    Image32 *root = data->root;

    int ncols = data->pred->ncols;

    FIFOQ *T = FIFOQNew(data->pred->ncols * data->pred->nrows);

    int i, p;

    MarkerSet::iterator aux = delSet.begin();

    for(; aux != delSet.end(); aux++)
    {
        p = aux->first;
        FIFOQPush(T, p);
        fcost[p] = INT_MAX;
        pred->val[p] = NIL;
    }

    while(!FIFOQEmpty(T))
    {
        p = FIFOQPop(T);

        Pixel u;
        u.x = p % ncols;
        u.y = p / ncols;

        for(i = 1; i < A->n; i++)
        {
            Pixel v;

            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(pred, v))
            {
                int q = to_index(pred, v);

                if(pred->val[q] == p)
                {
                    fcost[q] = INT_MAX;
                    pred->val[q] = NIL;
                    FIFOQPush(T, q);
                }
                else if(delSet.find(root->val[q]) == delSet.end())
                {
                    InsertSet(&Frontier, q);
                }
            }
        }
    }

    FIFOQDestroy(&T);

    T = NULL;

    return Frontier;
}

double differential_ift_heap(SegData *data, AdjRel *A, MarkerSet delSet,
                             float(*weight_function)(SegData*, int, int),
                             float(*cost_function)(SegData*, float, float))
{
    RealHeap *Q = NULL;
    int n    = data->grad->ncols * data->grad->nrows;
    Q    = CreateRealHeap(n, data->fcost);

    double mean_cut = dift(data, A, delSet, &Q, weight_function, cost_function);

    DestroyRealHeap(&Q);

    return mean_cut;
}

double dift(SegData *data, AdjRel *A, MarkerSet delSet, RealHeap **Q,
            float(*weight_function)(SegData*, int, int),
            float(*cost_function)(SegData*, float, float))
{
    int i, p, q;
    float weight, cst;
    double mean_cut = 0, size = 0;

    Image8 *label = data->label;
    float *fcost = data->fcost;
    Image32 *pred = data->pred;
    Image32 *root = data->root;
    MarkerSet::iterator seed_aux;

    Pixel u, v;
    Set *Saux = NULL;
    Set *F = forest_removal_heap(data, A, delSet);

    for(seed_aux = data->seeds.begin(); seed_aux != data->seeds.end(); seed_aux++)
    {
        int seed_pixel = USIS_SEED_INDEX(seed_aux);
        fcost[seed_pixel]  = 0; //Marker imposition.
        pred->val[seed_pixel] = NIL;
        root->val[seed_pixel] = seed_pixel;
        (*label)[seed_pixel] = USIS_SEED_LABEL(seed_aux);
        InsertRealHeap(*Q, seed_pixel);
    }

    for(Saux = F; Saux != NULL; Saux = Saux->next)
    {
        /// Frontier pixels should either have already left
        /// the Q in a previous iteration or are not part
        /// of the seed set, hence, they should be BLACK or WHITE
        if((*Q)->color[Saux->elem] != GRAY)
            InsertRealHeap(*Q, Saux->elem);

    }

    while(!IsEmptyRealHeap(*Q))
    {
        RemoveRealHeap(*Q, &p);

        u.x = p % label->ncols;
        u.y = p / label->ncols;
        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];
            if(valid_pixel(label, v))
            {
                q = to_index(label, v);

                weight = weight_function(data, p, q);

                cst = cost_function(data, fcost[p], weight);
                if((cst < fcost[q]) || (pred->val[q] == p))
                {
                    pred->val[q]  = p;
                    label->val[q] = label->val[p];
                    root->val[q] = root->val[p];
                    UpdateRealHeap(*Q, q, cst);
                }
                else
                {
                    if((*Q)->color[q] == BLACK && label->val[q] != label->val[p]
                            && fcost[q] != INT_MIN)
                    {
                        mean_cut += weight_function(data, p, q);
                        size++;
                    }
                }
            }
        }
    }

    DestroySet(&F);

    if(size == 0.0)
        mean_cut = 0;
    else
        mean_cut = mean_cut / size;

    return mean_cut;
}


/** Delineation and Recognition Functional **/
double ift_sc_and_mean_cut(SegData *data, AdjRel *A, int nbuckets,
                           float(*weight_function)(SegData*, int, int),
                           float(*cost_function)(SegData*, float, float))
{
    GQueue *Q = NULL;
    int n    = data->grad->n;
    Q    = CreateGQueue(nbuckets, n, data->cost->val);

    double mean_cut = ift_sc_and_mean_cut(data, A, &Q, weight_function,
                                          cost_function);

    DestroyGQueue(&Q);

    return mean_cut;
}

double ift_sc_and_mean_cut(SegData *data, AdjRel *A, GQueue **Q,
                           float(*weight_function)(SegData*, int, int),
                           float(*cost_function)(SegData*, float, float))
{
    int i, p, q, cst, weight;
    double mean_cut = 0, size = 0;

    Image8 *label = data->label;
    Image32 *cost = data->cost;
    Image32 *pred = data->pred;
    Image32 *root = data->root;
    MarkerSet::iterator seed_aux;

    Pixel u, v;

    for(seed_aux = data->seeds.begin(); seed_aux != data->seeds.end(); seed_aux++)
    {
        int seed_pixel = USIS_SEED_INDEX(seed_aux);
        cost->val[seed_pixel] = 0; //Marker imposition.
        pred->val[seed_pixel] = NIL;
        root->val[seed_pixel] = seed_pixel;
        (*label)[seed_pixel] = USIS_SEED_LABEL(seed_aux);
        InsertGQueue(Q, seed_pixel);
    }

    while(!EmptyGQueue(*Q))
    {
        p = RemoveGQueue(*Q);

        u = to_pixel(label, p);

        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(label, v))
            {
                q = to_index(label, v);

                if(cost->val[q] > cost->val[p])
                {
                    weight = (int)weight_function(data, p, q);

                    cst = cost_function(data, cost->val[p], weight);

                    if((cst < cost->val[q]))
                    {
                        if((*Q)->L.elem[q].color == GRAY)
                            RemoveGQueueElem(*Q, q);

                        cost->val[q]  = cst;
                        pred->val[q]  = p;
                        label->val[q] = label->val[p];
                        root->val[q] = root->val[p];
                        InsertGQueue(Q, q);
                    }

                }
                else
                {
                    if((*Q)->L.elem[q].color == BLACK && label->val[q] != label->val[p]
                            && cost->val[q] != INT_MIN)
                    {
                        mean_cut += weight_function(data, p, q);
                        size++;
                    }
                }
            }
        }
    }

    if(size == 0.0)
        mean_cut = 0;
    else
        mean_cut = mean_cut / size;

    return mean_cut;
}


/** IFT-SC using path_cost  sum + weight^power **/
double ift_sc_and_mean_cut_heap(SegData *data, AdjRel *A,
                                float(*weight_function)(SegData*, int, int),
                                float(*cost_function)(SegData*, float, float))
{
    RealHeap *Q = NULL;
    int n    = data->grad->ncols * data->grad->nrows;
    Q    = CreateRealHeap(n, data->fcost);

    double mean_cut = ift_sc_and_mean_cut(data, A, Q, weight_function,
                                          cost_function);

    DestroyRealHeap(&Q);

    return mean_cut;
}

double ift_sc_and_mean_cut(SegData *data, AdjRel *A, RealHeap *Q,
                           float(*weight_function)(SegData*, int, int),
                           float(*cost_function)(SegData*, float, float))
{
    int i, p, q;
    float weight, cst;
    double mean_cut = 0, size = 0;

    Image8 *label = data->label;
    float *cost = data->fcost;
    Image32 *pred = data->pred;
    Image32 *root = data->root;
    MarkerSet::iterator seed_aux;

    Pixel u, v;

    for(seed_aux = data->seeds.begin(); seed_aux != data->seeds.end(); seed_aux++)
    {
        int seed_pixel = USIS_SEED_INDEX(seed_aux);
        cost[seed_pixel]  = 0.0; //Marker imposition.
        pred->val[seed_pixel] = NIL;
        root->val[seed_pixel] = seed_pixel;
        (*label)[seed_pixel] = USIS_SEED_LABEL(seed_aux);
        InsertRealHeap(Q, seed_pixel);
    }

    while(!IsEmptyRealHeap(Q))
    {
        RemoveRealHeap(Q, &p);

        u = to_pixel(label, p);

        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(label, v))
            {
                q = to_index(label, v);

                if(cost[q] > cost[p])
                {
                    weight = weight_function(data, p, q);

                    cst = cost_function(data, cost[p], weight);

                    if((cst < cost[q]))
                    {
                        pred->val[q]  = p;
                        label->val[q] = label->val[p];
                        root->val[q] = root->val[p];
                        UpdateRealHeap(Q, q, cst);
                    }

                }
                else
                {
                    if(Q->color[q] == BLACK && label->val[q] != label->val[p]
                            && cost[q] != INT_MIN)
                    {
                        mean_cut += weight_function(data, p, q);
                        size++;
                    }
                }
            }
        }
    }

    if(size == 0.0)
        mean_cut = 0;
    else
        mean_cut = mean_cut / size;

    return mean_cut;
}

double mean_cut(Image8 *grad, Image8 *label,
				Image8::value_type lb, double radius)
{
	AdjRel *A = fast_circular(radius);
	double mean_grad = 0.0, npixels = 0.0;

	for(int p = 0; p < label->n; p++)
	{
		int i;
		Pixel v, u;

		u = to_pixel(label, p);
		v = next_adjacent(label, A, u, i);

		while(v.x != NIL && v.y != NIL)
		{
			int q = to_index(label, v);

			if(label->val[p] == lb && label->val[q] != label->val[p])
			{
				mean_grad += (grad->val[p]+grad->val[q])/2;
				npixels++;
				break;
			}

			v = next_adjacent(label, A, u, i);
		}
	}

	destroy_adjrel(&A);

	return mean_grad / MAX(npixels, 1.0);
}

/** Max-Flow/Min-Cut **/

// Calls the algorithm developed by Yuri Boykov
// and Vladimir Kolmogorov.
void max_flow(SegData *data, AdjRel *A,
              float lambda, float power,
              float (*weight_function)(SegData*, int, int),
              Graph **g, Graph::node_id **nodes)
{
    int i,p,q,n,ncols,nrows;
    int K,ws,wt,max_obj,min_obj;
    const int max_weight = 10000000; // a really large weight to accommodate
    float w;
    Pixel u,v;
    MarkerSet::iterator it;

    ncols = data->label->ncols;
    nrows = data->label->nrows;
    n     = ncols*nrows;
    K     = Graph::CAP_MAX-1;
    max_obj	= maximum_value(data->objMap);
    min_obj	= minimum_value(data->objMap);

    Graph *internal_graph = NULL;
    Graph::node_id *internal_nodes = NULL;

    // If g == NULL && nodes == NULL
    // then Graph g and its nodes must be
    // allocated and deallocated internally
    // to this function (i.e., they will not
    // be returned through the corresponding pointers.
    // We do force pointers g and node to point
    // to these internally allocated structures
    // for the sake of convenience.
    if(g == NULL && nodes == NULL)
    {
        g = &internal_graph;
        nodes = &internal_nodes;
    }

    // If *g == NULL && *nodes == NULL
    // then Graph g and its nodes must be
    // allocated and populated with the
    // detault wieghts. Otherwise, those
    // structures have been previously allocated
    // and only seed weights should be set.
    if(*g == NULL && *nodes == NULL)
    {
        *nodes = (Graph::node_id *)malloc(n*sizeof(Graph::node_id));
        *g = new Graph();
        for(p=0; p<n; p++)
        {
            (*nodes)[p] = (*g)->add_node();

            ws = (*(data->objMap))[p]; //(rcap->t_link_S)->val[p];
            wt = USIS_MAX_OBJMAP_VALUE - (*(data->objMap))[p]; // (rcap->t_link_T)->val[p];

            // Normalizing the terminal weights to be at most max_weight, in order
            // to accomodate the values in case the n-links should be raised to a power
            if (power > 1.0)
            {
                ws = (ws/(float)max_obj)*max_weight;
                wt = (wt/(float)(USIS_MAX_OBJMAP_VALUE-min_obj))*max_weight;
            }

            ws = ROUND(lambda*ws);
            wt = ROUND(lambda*wt);

            (*g)->set_tweights((*nodes)[p], (Graph::captype)ws, (Graph::captype)wt);
        }

        // Setting n-link weights
        for(p=0; p<n; p++)
        {
            u = to_pixel(data->label, p);

            for(i=1; i<A->n; i++)
            {
                v = adjacent(A, u, i);
                if(valid_pixel(data->label, v.x, v.y))
                {
                    q = to_index(data->label, v);

                    // We test q > p because only one node needs to add
                    // the edge incident to them. Hence, when it is q's turn
                    // the current edge (q,p) need not be added.
                    if(q>p)
                    {
                        // Considering that the gradient has higher values on the boundaries.
                        // Hence, the edge's capacity is given by the complement gradient
                        // weight.
                        w = (USIS_MAXGRAD - weight_function(data, p,q));
                        // Raising the weight to a given power and normalizing it
                        // to be at most max_weight
                        if(power > 1.0)
                        {
                            w = powf(w/USIS_MAXGRAD, power);
                            w = ROUND(w*(max_weight-1))+1;
                        }
                        // Considering symmetrical weights for both arcs between p and q
                        (*g)->add_edge((*nodes)[p], (*nodes)[q], (Graph::captype)w, (Graph::captype)w);
                    }
                }
            }
        }
    }

    // Setting seed terminal edge weights
    for(it = data->seeds.begin(); it != data->seeds.end(); it++)
    {
        p = USIS_SEED_INDEX(it);

        if(USIS_SEED_LABEL(it) == USIS_DEFAULT_SEG_BKG_LABEL)
            (*g)->set_tweights((*nodes)[p], 0, K);
        else
            (*g)->set_tweights((*nodes)[p], K, 0);
    }

    (*g)->maxflow();

    set_image(data->label, 0);
    for(p=0; p<n; p++)
        if((*g)->what_segment((*nodes)[p]) == Graph::SOURCE)
            (*(data->label))[p] = USIS_DEFAULT_SEG_OBJ_LABEL;

    if(internal_nodes != NULL)
        free(internal_nodes);

    if(internal_graph != NULL)
        delete internal_graph;
}

void watergray(SegData *data, Image8 *img, Image8 *marker, AdjRel *A)
{
    Image32 *cost = NULL, *pred = NULL, *regions = NULL;
    GQueue *Q = NULL;
    int i, p, q, tmp, n, lambda = 1;
    Pixel u, v;

    n     = img->ncols * img->nrows;
    set_image(data->cost, 0);
    set_image(data->pred, 0);
    set_image(data->regions, 0);
    cost = data->cost;
    pred = data->pred;
    regions = data->regions;
    Q     = CreateGQueue(maximum_value(marker) + 2, n, cost->val);

    // Trivial path initialization

    for(p = 0; p < n; p++)
    {
        cost->val[p] = marker->val[p] + 1;
        pred->val[p] = NIL;
        InsertGQueue(&Q, p);
    }

    // Path propagation

    while(!EmptyGQueue(Q))
    {
        p = RemoveGQueue(Q);
        if(pred->val[p] == NIL)  // on-the-fly root detection
        {
            cost->val[p] = img->val[p];
            regions->val[p] = lambda;
            lambda++;
        }

        u = to_pixel(img, p);
        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];
            if(valid_pixel(img, v.x, v.y))
            {
                q = to_index(img,v);
                if(cost->val[q] > cost->val[p])
                {
                    tmp = MAX(cost->val[p], img->val[q]);
                    if(tmp < cost->val[q])
                    {
                        RemoveGQueueElem(Q, q);
                        pred->val[q]  = p;
                        regions->val[q] = regions->val[p];
                        cost->val[q]  = tmp;
                        InsertGQueue(&Q, q);
                    }
                }
            }
        }
    }
    DestroyGQueue(&Q);
}




void watergray(SegData *data, Image32 *img, Image32 *marker, AdjRel *A)
{
    Image32 *cost = NULL, *pred = NULL, *regions = NULL;
    GQueue *Q = NULL;
    int i, p, q, tmp, n, lambda = 1;
    Pixel u, v;

    n     = img->ncols * img->nrows;
    set_image(data->cost, 0);
    set_image(data->pred, 0);
    set_image(data->regions, 0);
    cost = data->cost;
    pred = data->pred;
    regions = data->regions;
    Q     = CreateGQueue(maximum_value(marker) + 2, n, cost->val);

    // Trivial path initialization

    for(p = 0; p < n; p++)
    {
        cost->val[p] = marker->val[p] + 1;
        pred->val[p] = NIL;
        InsertGQueue(&Q, p);
    }

    // Path propagation

    while(!EmptyGQueue(Q))
    {
        p = RemoveGQueue(Q);
        if(pred->val[p] == NIL)  // on-the-fly root detection
        {
            cost->val[p] = img->val[p];
            regions->val[p] = lambda;
            lambda++;
        }

        u = to_pixel(img, p);
        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];
            if(valid_pixel(img, v.x, v.y))
            {
                q = to_index(img,v);
                if(cost->val[q] > cost->val[p])
                {
                    tmp = MAX(cost->val[p], img->val[q]);
                    if(tmp < cost->val[q])
                    {
                        RemoveGQueueElem(Q, q);
                        pred->val[q]  = p;
                        regions->val[q] = regions->val[p];
                        cost->val[q]  = tmp;
                        InsertGQueue(&Q, q);
                    }
                }
            }
        }
    }
    DestroyGQueue(&Q);
}

void fast_smooth_ift(SegData *data, int niterations,
						float(*WeightFunction)(SegData*, int, int))
{
    Image8 *label = data->label;
    BoundaryRegion region;
    int p,q,i;
    Pixel u,v;
    AdjRel *A = circular(1.5);

    for (u.y=0; u.y < label->nrows; u.y++)
        for (u.x=0; u.x < label->ncols; u.x++)
        {
            p = to_index(label, u);
            if (label->val[p]>0)
            {
                for (i=1; i < A->n; i++)
                {
                    v.x = u.x + A->dx[i];
                    v.y = u.y + A->dy[i];
                    if (valid_pixel(label,v))
                    {
                        q = to_index(label, v);
                        if (label->val[q]!= label->val[p])
                        {
                            region.insert(p);
                            break;
                        }
                    }
                }
            }
        }

    fast_smooth_ift(data, region, niterations, WeightFunction);
    destroy_adjrel(&A);
}

void fast_smooth_ift(SegData *data, BoundaryRegion &region, int niterations,
                          float(*WeightFunction)(SegData*, int, int))
{
    AdjRel *A;
    Image8  *label1 = data->label, *label2 = NULL;
    DbImage *flabel1,*flabel2;
    double *sum, norm_factor,max_membership;
    int l,i,p,q,max_label, iter;
    int maxlabel;
    Pixel u,v;
    Set  *Fprev=NULL, *Fnext=NULL;
    set<int> inFrontier;
    BoundaryRegion::iterator it;

    A = circular(1.5);

    maxlabel = maximum_value(data->label);

    sum        = AllocDoubleArray(maxlabel+1);
    label2     = copy_image8(data->label);
    flabel1    = create_dbimage(label1->ncols, label1->nrows);
    flabel2    = create_dbimage(label1->ncols, label1->nrows);

    for (p=0; p < label1->ncols*label1->nrows; p++)
        flabel1->val[p]=flabel2->val[p]=1.0;

    /* Find initial frontier set: this implementation is more
       efficient when the background is large. */

    for(it = region.begin(); it != region.end(); it++)
    {
        p = *it;
        InsertSet(&Fprev,p);
        inFrontier.insert(p);
    }

    /* Smooth objects */

    for (iter=0; iter < niterations; iter++)
    {
        while(Fprev != NULL)
        {
            p   = RemoveSet(&Fprev);

            InsertSet(&Fnext,p);

            u.x = p%label1->ncols;
            u.y = p/label1->ncols;

            for (l=0; l <= maxlabel; l++)
                sum[l]= 0.0;

            norm_factor = 0.0;

            for (i=1; i < A->n; i++)
            {
                v.x = u.x + A->dx[i];
                v.y = u.y + A->dy[i];

                if (valid_pixel(label1,v))
                {
                    double weight;
                    q = v.x + label1->ncols*v.y;
                    weight = WeightFunction(data, p, q);
                    weight = USIS_MAXGRAD - weight;
                    sum[label1->val[q]] +=  flabel1->val[q]*weight;
                    norm_factor         +=  weight;
                    if (inFrontier.find(q)==inFrontier.end())  // expand frontier set
                    {
                        InsertSet(&Fnext,q);
                        inFrontier.insert(q);
                    }
                }
            }

            for (l=0; l <= maxlabel; l++)
            {
                sum[l]  = sum[l] / norm_factor;
            }

            max_membership = -DBL_MAX;
            max_label=NIL;
            for (l=0; l <= maxlabel; l++)
                if (sum[l] > max_membership)
                {
                    max_membership = sum[l];
                    max_label      = l;
                }

            label2->val[p]  = max_label;
            flabel2->val[p] = sum[max_label];

        }

        Fprev = Fnext;
        Fnext = NULL;

        memcpy(flabel1->val,flabel2->val,flabel1->ncols*flabel1->nrows*sizeof(DbImage::value_type));
        memcpy(label1->val,label2->val,label1->ncols*label1->nrows*sizeof(Image8::value_type));
    }

    memcpy(data->label->val,label2->val,label1->ncols*label1->nrows*sizeof(Image8::value_type));

    free(sum);
    destroy_dbimage(&flabel1);
    destroy_dbimage(&flabel2);
    destroy_adjrel(&A);
    DestroySet(&Fprev);
    destroy_image(&label2);
}
/** Boundary-based segmentation **/

/* Live-wire-on-the-fly */
int lwof_cost(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
              int p, int q, int Wmax, int Cmax,
              double power, AdjRel *L, AdjRel *R, int i,
              USIS_LW_ORI ori, bool force_ori)
{
    int left, right, tmp;
    bool test_ori = false;

    Pixel u,v;

    u = to_pixel(grad, p);

    // treat orientation
    v.x = u.x + L->dx[i];
    v.y = u.y + L->dy[i];

    if(valid_pixel(grad, v))
        left = to_index(grad, v);
    else
        left = q;

    v.x = u.x + R->dx[i];
    v.y = u.y + R->dy[i];

    if(valid_pixel(grad, v))
        right = to_index(grad, v);
    else
        right = q;


    if(obj != NULL && ori == USIS_LW_CLOCKWISE)
    {
        /// if for some reason the object membership map is not good at some point,
        /// the user may force the opposite orientation (COUNTERCLOCKWISE in this case)
        /// to do the segmentation (this is useful for the placement of markers during live markers)
        test_ori = (!force_ori) ? obj->val[left] <= obj->val[right] : obj->val[left] >= obj->val[right];
    }
    else if(obj != NULL && ori == USIS_LW_COUNTERCLOCKWISE)
    {
        /// if for some reason the object membership map is not good at some point,
        /// the user may force the opposite orientation (CLOCKWISE in this case)
        /// to do the segmentation (this is useful for the placement of markers during live markers)
        test_ori = (!force_ori) ? obj->val[left] >= obj->val[right] : obj->val[left] <= obj->val[right];
    }

    /// In case obj is NULL, no orientation should be considered
    if(obj == NULL || ori == USIS_LW_NOORIENTATION || test_ori)
    {
        tmp = cost->val[p] + (int)pow(Wmax - grad->val[q], power);
    }
    else
    {
        tmp = cost->val[p] + Cmax;
    }

    if(left == pred->val[right] || right == pred->val[left]) // avoid self-crossing
        tmp = INT_MAX;

    return tmp;
}


int riverbed_cost(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
                  int p, int q, int Wmax, int Cmax,
                  double power, AdjRel *L, AdjRel *R, int i,
                  USIS_LW_ORI ori, bool force_ori)
{
    int left, right, tmp;
    bool test_ori = false;
    Pixel u,v;

    u = to_pixel(grad, p);

    // treat orientation
    v.x = u.x + L->dx[i];
    v.y = u.y + L->dy[i];

    if(valid_pixel(grad, v))
        left = to_index(grad, v);
    else
        left = q;

    v.x = u.x + R->dx[i];
    v.y = u.y + R->dy[i];

    if(valid_pixel(grad, v))
        right = to_index(grad, v);
    else
        right = q;


    if(obj != NULL && ori == USIS_LW_CLOCKWISE)
    {
        /// if for some reason the object membership map is not good at some point,
        /// the user may force the opposite orientation (COUNTERCLOCKWISE in this case)
        /// to do the segmentation (this is useful for the placement of markers during live markers)
        test_ori = (!force_ori) ? obj->val[left] <= obj->val[right] : obj->val[left] >= obj->val[right];
    }
    else if(obj != NULL && ori == USIS_LW_COUNTERCLOCKWISE)
    {
        /// if for some reason the object membership map is not good at some point,
        /// the user may force the opposite orientation (CLOCKWISE in this case)
        /// to do the segmentation (this is useful for the placement of markers during live markers)
        test_ori = (!force_ori) ? obj->val[left] >= obj->val[right] : obj->val[left] <= obj->val[right];
    }

    /// In case obj is NULL, no orientation should be considered
    if(obj == NULL || ori == USIS_LW_NOORIENTATION || test_ori)
    {
        tmp = Wmax - grad->val[q];
    }
    else
    {
        tmp = Cmax;
    }

    if(left == pred->val[right] || right == pred->val[left]) // avoid self-crossing
        tmp = INT_MAX;

    return tmp;
}

void contour_tracking(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
                      GQueue **Q, AdjRel *A, int Wmax, USIS_LW_ORI ori, int dst,
                      double power, bool force_ori,
                      int (*path_cost)(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
                                       int p, int q, int Wmax, int Cmax,
                                       double power, AdjRel *L, AdjRel *R, int i,
                                       USIS_LW_ORI ori, bool force_ori))
{
    int p, q, i, tmp;
    Pixel u, v;

    AdjRel *L = left_side(A);
    AdjRel *R = right_side(A);

    int Cmax = (int)pow(Wmax, power);

    if((*Q)->L.elem[dst].color != BLACK)
    {
        p = NIL;

        while(!EmptyGQueue(*Q) && p != dst)
        {
            p   = RemoveGQueue(*Q);

            u = to_pixel(grad, p);

            for(i = 1; i < A->n; i++)
            {
                v.x = u.x + A->dx[i];
                v.y = u.y + A->dy[i];


                if(valid_pixel(grad, v))
                {
                    q   = to_index(grad, v);

                    // Since Riverbed has a non-smooth path cost function,
                    // we test the adjacent's color instead of its cost
                    // to determine whether q has been processed or not.
                    if((*Q)->L.elem[q].color != BLACK)
                    {
                        tmp = path_cost(grad, cost, pred, obj, p, q,
                                        Wmax, Cmax, power, L, R, i,
                                        ori, force_ori);

                        if(tmp < cost->val[q])
                        {
                            if(cost->val[q] != INT_MAX)
                                RemoveGQueueElem(*Q, q);

                            cost->val[q] = tmp;
                            pred->val[q] = p;

                            InsertGQueue(Q, q);
                        }
                    }
                }
            }
        }
    }

    destroy_adjrel(&L);
    destroy_adjrel(&R);
}

int *path2array(Image32 *pred, int dst, int src)
{
    int *path = NULL, i, n, p, pred_src = NIL;

    p = dst;
    n = 0;

    if(src >= 0)
        pred_src = pred->val[src];

    while(p != NIL && p != pred_src)
    {
        n++;
        p = pred->val[p];

    }

    if(n > 0)
    {
        path = AllocIntArray(n + 1);
        path[0] = n;
        p = dst;
        i = 0;

        while(p != NIL && p != pred_src)
        {
            i++;
            path[i] = p;
            p = pred->val[p];
        }
    }

    return(path);
}

MarkerSet* path_seeds(LMAnchorsRadii &anchors, Image32 *pred, Image32 *cost,
                      int primary_label, int left_pixel_label,
                      int right_pixel_label, int mk_id,
                      LWInvRootMap *root_inv_index)
{
    int i, q, adj, first_node, cur_anchor_idx, path_dst;
    int right, left;
    // represents the brush radius that was used for the segment
    double radius, dist_cur_anchor, dist_next_anchor;

    Pixel px_nil = (Pixel)
    {
        NIL, NIL
    };
    Pixel px_q, px_adj, px_dst;
    Pixel px_next_anchor = px_nil, px_cur_anchor = px_nil;
    AdjRel *A = NULL, *A4 = NULL, *A8 = NULL, *B = NULL, *L = NULL, *R = NULL;


    std::tr1::unordered_set<int> segment_nodes;

    // <int,pair<char,int> > == first int is the pixel index, char represents
    // the pixel status in Q and the second int is the propagated label.
    MarkerSet *seed_map = new MarkerSet();

    std::tr1::unordered_set<int> visited;
    std::queue<int> Q = std::queue<int>();

    cur_anchor_idx = anchors.size()-1;
    first_node = anchors[0].first;
    path_dst = anchors[cur_anchor_idx].first;
    radius = anchors[cur_anchor_idx].second;

    px_dst = to_pixel(pred, path_dst);

    px_cur_anchor = px_dst;
    px_next_anchor = to_pixel(pred, anchors[cur_anchor_idx-1].first);

    if(radius < 1.5)
        radius = 1.5;

    A4 = circular(1.0);
    A8 = circular(1.5);
    L = left_side(A8);
    R = right_side(A8);
    B = circular(radius);

    A = B;
    q = path_dst;

    while(q != NIL)
    {
        segment_nodes.insert(q);

        q = pred->val[q];
    }

    //  Setting variable iter to the beginning of root_inv_index to allow fast insertion of nodes
    LWInvRootMap::iterator iter;

    if(root_inv_index != NULL)
        iter = root_inv_index->begin();

    q = path_dst;

    // We add seeds only for the current segment.
    // This function should ideally be recalled every time
    // a new segment is added, while all the seeds should be removed.
    while((q != NIL && first_node == NIL)
            || (first_node != NIL && q != pred->val[first_node]))
    {
        bool is_candidate = true;
        // Adding "q" as an object seed with label "m_lseed_label" Marking pixel q as visited.
        (*seed_map)[q].mk_id = mk_id;
        (*seed_map)[q].lb = primary_label; 	// can be either left or right depending

        // on the orientation/application
        visited.insert(q);

        px_q = to_pixel(pred,q);

        if(cur_anchor_idx > 1)
        {
            if(q == anchors[cur_anchor_idx-1].first)
            {
                px_cur_anchor = to_pixel(pred, anchors[--cur_anchor_idx].first);
                px_next_anchor = to_pixel(pred, anchors[cur_anchor_idx-1].first);
                radius = anchors[cur_anchor_idx].second;

                destroy_adjrel(&B);
                B = circular(radius);
            }
        }

        dist_cur_anchor = sqrt(eucl_dist2(px_q, px_cur_anchor));
        dist_next_anchor = sqrt(eucl_dist2(px_q, px_next_anchor));

        // Adapting the width of the brush when
        // near the extremities of the current segment.
        if(dist_cur_anchor < 2 * radius || dist_next_anchor < 2 * radius)
        {
            A = A8; // 8-adj
        }
        else
        {
            A = B;
        }

        // Adding all pixels adjacent to q in
        // adjacency B as future seeds. This
        // helps to constrain the search space
        // for seed propagation. That is, only the
        // pixels within "radius" of the current segment
        // are added as markers.
        for(i = 1; i < A->n; i++)
        {
            double dist;

            px_adj = adjacent(A, px_q, i);
            adj = to_index(pred, px_adj);

            // Adding future seed to seed_map if it nis not part
            // of the current segment.
            // Note that we cannot avoid selecting seeds over previous selected segments, since the
            // only current way of verifying that is to check if the cost is != INT_MIN. However,
            // this would be troublesome for Riverbed since the neighboring pixels of the segment
            // are set to INT_MIN.
            if(valid_pixel(pred, px_adj) && seed_map->find(adj) == seed_map->end()
                    && segment_nodes.find(adj) == segment_nodes.end())
            {

                (*seed_map)[adj] = (MarkerData)
                {
                    mk_id, NIL
                };

                if(root_inv_index != NULL)
                {
                    // Propagating the "root" q to the other seeds generated from it
                    iter = root_inv_index->insert(iter, make_pair(q, adj));
                    // I need to insert the pair (adj,q) in the inverted index to find
                    // q when left leaves Q
                    iter = root_inv_index->insert(iter, make_pair(adj, q));
                }
            }

            // If the adjacent pixel is 8-neighbor of q, part
            // of the contour, but is neither q's predecessor nor
            // q is adj's predecessor then q is not an ideal candidate
            // for labeling q's left/right nodes as being inside/outside the
            // contour.
            dist = sqrt(eucl_dist2(px_adj, px_q));

            if(valid_pixel(pred, px_adj) && dist <= 1.5
                    && segment_nodes.find(adj) != segment_nodes.end()
                    && pred->val[adj] != q && pred->val[q] != adj)
            {
                is_candidate = false;
            }
        }

        // The predecessor must be valid in order
        // to determine the left/right orientation.
        // Furthermore, a candidate pixel q
        // can only be valid if the contour does not touch
        // itself at q's 8-neighborhood.
        if(is_candidate && pred->val[q] > 0)
        {
            Pixel px_pred = to_pixel(pred, pred->val[q]);
            int pred_idx = adjacent_index(A8, px_q, px_pred);

            if(pred_idx > 0)
            {
                right = to_index(pred, adjacent(R, px_q, pred_idx));
                left = to_index(pred, adjacent(L, px_q, pred_idx));

                // Both right and left pixels must be seeds and not
                // part of the contour to have their labels properly
                // determined.
                if(seed_map->find(right) != seed_map->end()
                        && seed_map->find(left) != seed_map->end()
                        && segment_nodes.find(right) == segment_nodes.end()
                        && segment_nodes.find(left) == segment_nodes.end())
                {
                    (*seed_map)[right].mk_id = mk_id;
                    (*seed_map)[right].lb = right_pixel_label;

                    Q.push(right);
                    visited.insert(right);

                    (*seed_map)[left].mk_id = mk_id;
                    (*seed_map)[left].lb = left_pixel_label;

                    Q.push(left);
                    visited.insert(left);
                }
            }
        }

        if(q == NIL && q != pred->val[path_dst])
        {
            fprintf(stderr, "Self crossing segment in Live Wire!!\n");

            destroy_adjrel(&A4);
            destroy_adjrel(&A8);
            destroy_adjrel(&B);
            destroy_adjrel(&L);
            destroy_adjrel(&R);
            delete seed_map;

            throw(true);
        }

        q = pred->val[q];
    }

    propagate_seed_labels(pred, seed_map, segment_nodes, visited,
                          Q, A4, root_inv_index, iter);

    destroy_adjrel(&A4);
    destroy_adjrel(&A8);
    destroy_adjrel(&B);
    destroy_adjrel(&L);
    destroy_adjrel(&R);

    return seed_map;
}

void propagate_seed_labels(Image32 *pred, MarkerSet *seed_map,
                           std::tr1::unordered_set<int> &segment_nodes,
                           std::tr1::unordered_set<int> &visited,
                           std::queue<int> &Q, AdjRel *A4,
                           LWInvRootMap *root_inv_index,
                           LWInvRootMap::iterator &iter)
{
    int q,i,adj;

    Pixel px_q, px_adj;
    // BFS to propagate the labels to the right and left pixels of the current segment
    while(!Q.empty())
    {
        int q_root;

        q = Q.front();
        Q.pop();

        px_q = to_pixel(pred, q);
        visited.insert(q); // signing that pixel q is out of Q

        // Obtaining q's root
        if(root_inv_index != NULL)
        {
            q_root = root_inv_index->find(q)->second;
        }

        // Using 4-adjacency to avoid some weird cases that might happen
        // when propagating the labels due to edge crossings
        for(i = 1; i < A4->n; i++)
        {
            px_adj = adjacent(A4, px_q, i);
            adj = to_index(pred, px_adj);

            // Inserting seeds which have not yet been inserted before in any iteration
            if(valid_pixel(pred, px_adj) && seed_map->find(adj) != seed_map->end()
                    && visited.find(adj) == visited.end()
                    && segment_nodes.find(adj) == segment_nodes.end())
            {

                if(root_inv_index != NULL)
                {
                    // Propagating the "root" q to the other seeds generated from it, using an inverted index
                    iter = root_inv_index->insert(iter, make_pair(q_root, adj));
                    // I need to insert the pair (adj,q_root) in the inverted index to find q_root when adj leaves Q
                    iter = root_inv_index->insert(iter, make_pair(adj, q_root));
                }

                // Propagating label and marking as visited
                (*seed_map)[adj].mk_id;
                (*seed_map)[adj].lb = (*seed_map)[q].lb;

                visited.insert(adj);
                Q.push(adj);
            }
        }
    }
}

void label_union(Image8 *dst, Image8 *src, int label)
{
    int p;

    if(label > 0)
    {
        for(p = 0; p < src->ncols * src->nrows; p++)
            dst->val[p] = (src->val[p]  == label) ? label : dst->val[p];
    }
    else
    {
        for(p = 0; p < src->ncols * src->nrows; p++)
            dst->val[p] = (src->val[p]  > 0) ? src->val[p] : dst->val[p];
    }
}

void intersect_labels(Image8 *label1, Image8 *label2, int value)
{
    int p;

    for(p = 0; p < label1->ncols * label1->nrows; p++)
        label1->val[p] = (label2->val[p] > 0) ? value : label1->val[p];
}

void intersect_labels_bb(Image8 *label1, Image8 *label2, int value, int xmin, int ymin, int xmax, int ymax)
{
    int p, x, y;

    for(y = ymin; y < ymax; y++)
        for(x = xmin; x < xmax; x++)
        {
            p = to_index(label1, x, y);
            label1->val[p] = (label2->val[p] > 0) ? value : label1->val[p];
        }
}

Image8 *threshold(Image8 *img, Image8::value_type lower,
					Image8::value_type higher,
					Image8::value_type value)
{
    Image8 *bin = NULL;
    int p, n;

    bin = create_image8(img->ncols, img->nrows);
    n = img->ncols * img->nrows;

    for(p = 0; p < n; p++)
        if((img->val[p] >= lower) && (img->val[p] <= higher))
            bin->val[p] = value;

    return(bin);
}

Image32 *threshold(Image32 *img, int lower, int higher,
                  Image32::value_type value)
{
    Image32 *bin = NULL;
    int p, n;

    bin = create_image32(img->ncols, img->nrows);
    n = img->ncols * img->nrows;

    for(p = 0; p < n; p++)
        if((img->val[p] >= lower) && (img->val[p] <= higher))
            bin->val[p] = value;

    return(bin);
}


Image8 *threshold(DbImage *img, DbImage::value_type lower,
                  DbImage::value_type higher,
                  Image8::value_type value)
{
    Image8 *bin = NULL;
    int p, n;

    bin = create_image8(img->ncols, img->nrows);
    n = img->ncols * img->nrows;
    for(p = 0; p < n; p++)
        if((img->val[p] >= lower) && (img->val[p] <= higher))
            bin->val[p] = value;
    return(bin);
}


Image8 *binarize(Image8 *img, set<uchar> &values)
{
    Image8 *bin = NULL;
    int p, n;

    bin = create_image8(img->ncols, img->nrows);
    n = img->ncols * img->nrows;

    for(p = 0; p < n; p++)
        if(values.find(img->val[p]) != values.end())
            bin->val[p] = 1;

    return(bin);
}

Image32 *binarize(Image8 *img, set<int> &values)
{
    Image32 *bin = NULL;
    int p, n;

    bin = create_image32(img->ncols, img->nrows);
    n = img->ncols * img->nrows;

    for(p = 0; p < n; p++)
        if(values.find(img->val[p]) != values.end())
            bin->val[p] = 1;

    return(bin);
}


Image32 *label_bin_comp(Image8 *bin, AdjRel *A)
{
    Image32 *bin32;
    Image32 *label = NULL, *flabel = NULL, *fbin = NULL;
    int i, j, n, sz, p, q, l = 1;
    AdjPxl *N = NULL;
    int *FIFO = NULL;
    int first = 0, last = 0;

    sz  = FrameSize(A);
    bin32 = cast_2_image32(bin);
    fbin = add_frame(bin32, sz, INT_MIN);
    destroy_image(&bin32);
    flabel = create_image32(fbin->ncols, fbin->nrows);
    N  = adj_pixels(fbin->ncols, A);
    n  = fbin->ncols * fbin->nrows;
    FIFO  = AllocIntArray(n);
    for(j = 0; j < n; j++)
    {
        if((fbin->val[j] > 0) && (flabel->val[j] == 0))
        {
            flabel->val[j] = l;
            FIFO[last] = j;
            last++;
            while(first != last)
            {
                p = FIFO[first];
                first++;
                for(i = 1; i < N->n; i++)
                {
                    q = p + N->dp[i];
                    if((fbin->val[q] > 0) && (flabel->val[q] == 0))
                    {
                        flabel->val[q] = flabel->val[p];
                        FIFO[last] = q;
                        last++;
                    }
                }
            }
            l++;
            first = last = 0;
        }
    }

    label = rem_frame(flabel, sz);
    DestroyAdjPxl(&N);
    destroy_image(&fbin);
    destroy_image(&flabel);
    free(FIFO);

    return(label);
}

Image32 *label_bin_comp(Image32 *bin, AdjRel *A)
{
    Image32 *label = NULL, *flabel = NULL, *fbin = NULL;
    int i, j, n, sz, p, q, l = 1;
    AdjPxl *N = NULL;
    int *FIFO = NULL;
    int first = 0, last = 0;

    sz  = FrameSize(A);
    fbin = add_frame(bin, sz, INT_MIN);
    flabel = create_image32(fbin->ncols, fbin->nrows);
    N  = adj_pixels(fbin->ncols, A);
    n  = fbin->ncols * fbin->nrows;
    FIFO  = AllocIntArray(n);
    for(j = 0; j < n; j++)
    {
        if((fbin->val[j] > 0) && (flabel->val[j] == 0))
        {
            flabel->val[j] = l;
            FIFO[last] = j;
            last++;
            while(first != last)
            {
                p = FIFO[first];
                first++;
                for(i = 1; i < N->n; i++)
                {
                    q = p + N->dp[i];
                    if((fbin->val[q] > 0) && (flabel->val[q] == 0))
                    {
                        flabel->val[q] = flabel->val[p];
                        FIFO[last] = q;
                        last++;
                    }
                }
            }
            l++;
            first = last = 0;
        }
    }

    label = rem_frame(flabel, sz);
    DestroyAdjPxl(&N);
    destroy_image(&fbin);
    destroy_image(&flabel);
    free(FIFO);

    return(label);
}

void error_components(Image8 *label, Image8 *gt, Image8 *errors)
{
    int i;

    for(i = 0; i < label->n; i++)
    {
        // If an error happened (either false positive or negative)
        // errors[i] will be assigned 1
        if(((*label)[i] > 0 && (*gt)[i] == 0) || ((*label)[i] == 0 && (*gt)[i] > 0))
            (*errors)[i] = 1;
        else
            (*errors)[i] = 0;
    }
}

void relabel_regions(Image32 *regions)
{
    int i, l = 1, nregions = maximum_value(regions)+1;
    int *region_labels = AllocIntArray(nregions);

    for(i = 0; i < nregions; i++)
        region_labels[i] = NIL;

    for(i = 0; i < regions->ncols*regions->nrows; i++)
    {
        int r = regions->val[i];
        if(region_labels[r] <= 0)
        {
            region_labels[r] = l;
            l++;
        }

        regions->val[i] = region_labels[r]; // updating label on-the-fly
    }

    free(region_labels);
}

vector<int*> image_contours(Image8 *bin, int thrsh)
{
    Image8 *bndr=NULL, *color=NULL;
    Image32 *pred=NULL;
    int p=0,q,i,j,left=0,right=0,n,*LIFO,last;
    AdjRel *A,*L,*R;
    Pixel u,v,w;
    vector<int*> img_contours;

    A     = circular(1.0);
    n     = bin->ncols*bin->nrows;
    bndr  = create_image8(bin->ncols,bin->nrows);
    for (p=0; p < n; p++)
    {
        if (bin->val[p]>thrsh)
        {
            u.x = p%bin->ncols;
            u.y = p/bin->ncols;
            for (i=1; i < A->n; i++)
            {
                v.x = u.x + A->dx[i];
                v.y = u.y + A->dy[i];
                if (valid_pixel(bin,v.x,v.y))
                {
                    q = to_index(bin,v);
                    if (bin->val[q]<=thrsh)
                    {
                        bndr->val[p]=1;
                        break;
                    }
                }
                else
                {
                    bndr->val[p]=1;
                    break;
                }
            }
        }
    }

    destroy_adjrel(&A);

    A      = circular(1.5);
    L      = left_side(A);
    R      = right_side(A);
    color  = create_image8(bndr->ncols,bndr->nrows);
    pred   = create_image32(bndr->ncols,bndr->nrows);
    LIFO   = AllocIntArray(n);
    last   = NIL;
    for (j=0; j < n; j++)
    {
        if ((bndr->val[j]==1)&&
                (color->val[j]!=BLACK)&&
                valid_cont_point(bin,L,R,j, thrsh))
        {
            last++;
            LIFO[last]    = j;
            color->val[j] = GRAY;
            pred->val[j] = j;
            while(last != NIL)
            {
                p = LIFO[last];
                last--;
                color->val[p]=BLACK;
                u.x = p%bndr->ncols;
                u.y = p/bndr->ncols;
                for (i=1; i < A->n; i++)
                {
                    v.x = u.x + A->dx[i];
                    v.y = u.y + A->dy[i];
                    if (valid_pixel(bndr,v.x,v.y))
                    {
                        q = to_index(bndr,v);
                        if ((q==j)&&(pred->val[p]!=j))
                        {
                            last = NIL;
                            break;
                        }
                        w.x = u.x + L->dx[i];
                        w.y = u.y + L->dy[i];
                        if (valid_pixel(bndr,w.x,w.y))
                            left = to_index(bndr,w);
                        else
                            left = -1;
                        w.x = u.x + R->dx[i];
                        w.y = u.y + R->dy[i];
                        if (valid_pixel(bndr,w.x,w.y))
                            right = to_index(bndr, w);
                        else
                            right = -1;

                        if ((bndr->val[q]==1)&&
                                (color->val[q] != BLACK)&&
                                (((left!=-1)&&(right!=-1)&&(bin->val[left] != bin->val[right]))||
                                 ((left==-1)&&(right!=-1)&&(bin->val[right] > thrsh)) ||
                                 ((right==-1)&&(left!=-1)&&(bin->val[left] > thrsh))) )
                        {
                            pred->val[q] = p;
                            if (color->val[q] == WHITE)
                            {
                                last++;
                                LIFO[last] = q;
                                color->val[q]=GRAY;
                            }
                        }
                    }
                }
            }

            // Spurious contours might still be caught by the algorithm above.
            // Hence, we remove them.
            if(p != j)
            {
                // Setting the predecessor of j to NIL
                // because path2array adds all nodes between
                // [p, pred->val[j]), hence, it would stop
                // when q == pred->val[j], which in the current
                // case would mean that it would stop when
                // q == j.
                pred->val[j] = NIL;
                img_contours.push_back(path2array(pred, p,j));
            }
        }
    }

    destroy_adjrel(&A);
    destroy_adjrel(&L);
    destroy_adjrel(&R);
    destroy_image(&bndr);
    destroy_image(&color);
    destroy_image(&pred);
    free(LIFO);

    return img_contours;
}

Image32* label_contours(Image8 *bin, int thresh)
{
    size_t i,j,n, l=1; // The pixels from all contours are assigned unique sequential ids
    vector<int*> contours = image_contours(bin, thresh);
    Image32 *label = create_image32(bin->ncols, bin->nrows);

    for(i = 0; i < contours.size(); i++)
    {
        n = contours[i][0]; // contour length;
        for(j = 1; j <= n; j++)
        {
            size_t p = contours[i][j];
            label->val[p] = l; // labeling the contour
        }
        l++;

        free(contours[i]);
    }

    return label;
}

MarkerSet seeds_from_label(Image8 *label, Image8 * mask, int mk_id)
{
	MarkerSet seeds;
	// Getting inner seeds
	for(int p = 0; p < label->n; p++)
	{
		if((*mask)[p] > 0)
		{
			seeds[p] = MarkerData();
			seeds[p].lb = (*label)[p];
			seeds[p].mk_id = mk_id;
		}
	}

	return seeds;
}

MarkerSet seeds_from_medial_axis_transform(Image8 *label, int lb, double skiz_perc,
											double axis_dilation_dist_perc, int mk_id)
{
	MarkerSet inner_seeds;
	MedialAxis axis;
	DbImage *msskel = NULL;
	Image32 *distances = NULL;
	// If lb < 0, binarize all labels
	Image8 *bin_label = threshold(label, MAX(lb,0), MIN(lb, UCHAR_MAX));
	Image8 *dil_axis = create_image8(label->ncols, label->nrows);

	msskel = ms_geodesic_skel(bin_label, &distances);

	axis = medial_axis_from_skiz(msskel, distances, label, lb, skiz_perc);

	dilate_medial_axis(axis, dil_axis, axis_dilation_dist_perc);

	inner_seeds = seeds_from_label(dil_axis, dil_axis, mk_id);

	destroy_dbimage(&msskel);
	destroy_image(&distances);
	destroy_image(&bin_label);
	destroy_image(&dil_axis);

	return inner_seeds;
}


MarkerSet seeds_from_label_erosion_or_skel(Image8 *label, bool from_skeleton, float inner_radius,
										   float outer_radius, double skeleton_scale,
										   int lb, int mk_id, int bkg_lb, int bkg_mk_id)
{
    Image8 *cropped = NULL, *dil = NULL, *bin_label = NULL;
    Image8 *outer = NULL;
    AdjRel *Adil = NULL;
    MarkerSet seeds, outer_seeds, tmp_seeds;
    MarkerSet::iterator it;
    Rectangle bb = mask_bb(label, lb);

    bb.beg.x = MAX(bb.beg.x-ROUND(outer_radius)-1, 0);
    bb.beg.y = MAX(bb.beg.y-ROUND(outer_radius)-1, 0);

    bb.end.x = MIN(bb.end.x+ROUND(outer_radius)+1, label->ncols-1);
    bb.end.y = MIN(bb.end.y+ROUND(outer_radius)+1, label->nrows-1);

    cropped = ROI<Image8, Image8::value_type>(label, bb);

    if(lb > 0)
    {
        bin_label = threshold(cropped, lb, lb);
    }
    else
    {
        bin_label = cropped;
    }

    // Obtaining interior seeds from the dilation of the skeleton
    if(from_skeleton)
    {
		tmp_seeds = seeds_from_medial_axis_transform(cropped, lb, skeleton_scale,
													inner_radius, mk_id);
    }
    else
    {
		Image8 *ero = NULL, *inner = NULL;
		AdjRel *Aero = NULL;

        // Eroding the skeleton using the "erosion" radius if necessary
        if(inner_radius > FLT_EPSILON)
        {
            Aero = circular(inner_radius);
            ero = erode(bin_label, Aero);
            inner = object_border(ero);
            destroy_image(&ero);
        }
        else
        {
            inner = object_border(bin_label);
        }

		// Forcing the inner seeds to have the given label
		for(int p = 0; p < inner->n; p++)
			(*inner)[p] = ((*inner)[p] > 0) ? lb : 0;

		tmp_seeds = seeds_from_label(inner, inner, mk_id);

		destroy_image(&inner);
		destroy_adjrel(&Aero);

    }

	/** Computing outer seeds **/
    Adil = circular(outer_radius);
    dil = dilate(bin_label, Adil);
    outer = object_border(dil);

	// Forcing the outer seeds to have the default  background
	// segmentation label (which probably is 0)
	set_image(bin_label, USIS_DEFAULT_SEG_BKG_LABEL);
	outer_seeds = seeds_from_label(bin_label, outer, bkg_mk_id);

	/** Updating the seeds' coordinates to the larger image **/
	for(it = tmp_seeds.begin(); it != tmp_seeds.end(); it++)
	{
		Pixel px = to_pixel(bin_label, it->first);
        int p1 = to_index(label, bb.beg.x + px.x, bb.beg.y + px.y);

		seeds[p1] = it->second;
	}

	for(it = outer_seeds.begin(); it != outer_seeds.end(); it++)
	{
		Pixel px = to_pixel(bin_label, it->first);
        int p1 = to_index(label, bb.beg.x + px.x, bb.beg.y + px.y);

		seeds[p1] = it->second;
	}

	if(bin_label != cropped)
		destroy_image(&bin_label);
	destroy_image(&cropped);
    destroy_image(&outer);
    destroy_image(&dil);
    destroy_adjrel(&Adil);

    return seeds;
}


}
