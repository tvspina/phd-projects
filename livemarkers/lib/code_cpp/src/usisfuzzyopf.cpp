/*
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
*/
#include "usisfuzzyopf.h"

extern "C"
{
#include "usisssemath.h"
#include "usiscommonsse.h"
}

namespace usis
{

    int count_nodes_by_truelabel(Subgraph* sg, int label)
    {
        int i;
        int count = 0;

        for(i = 0; i < sg->nnodes; i++)
            if(sg->node[i].truelabel == label) count++;

        return count;
    }

    Subgraph* induced_subgraph(Subgraph* sg, set<int> nodes)
    {
        int nnodes = nodes.size();
        set<int>::iterator it;

		if(nnodes <= 0)
			return NULL; // No nodes selected

        Subgraph* sgresult = CreateSubgraph(nnodes);

        int i, i0 = 0;

        sgresult->nfeats = sg->nfeats;
        sgresult->df = sg->df;

        for(i = 0; i < sg->nnodes; i++)
        {
            if(nodes.find(i) != nodes.end())
            {
                CopySNode(&sgresult->node[i0++], &sg->node[i], sg->nfeats);

				Set *aux = sg->node[i].adj;
				Set **aux_prev = &sg->node[i].adj;

                while(aux != NULL)
                {
                	int adj = aux->elem;

                	if(nodes.find(adj) == nodes.end())
                	{
						Set *tmp = aux;

						*aux_prev = aux->next;

						aux = aux->next;
						free(tmp);
                	}
                	else
                	{
                		aux_prev = &aux->next;
						aux = aux->next;
                	}
                }
            }
        }

        return sgresult;
    }

    Subgraph* induced_subgraph_by_truelabel(Subgraph* sg, set<int> labels)
    {
        int nnodes = 0;
        set<int>::iterator it;

        for(it = labels.begin(); it != labels.end(); it++)
			nnodes += count_nodes_by_truelabel(sg, *it);

        Subgraph* sgresult = CreateSubgraph(nnodes);

        int i, i0 = 0;

        sgresult->nfeats = sg->nfeats;
        sgresult->df = sg->df;

        for(i = 0; i < sg->nnodes; i++)
        {
        	int truelabel = sg->node[i].truelabel;

            if(labels.find(truelabel) != labels.end())
            {
                CopySNode(&sgresult->node[i0++], &sg->node[i], sg->nfeats);

				Set *aux = sg->node[i].adj;
				Set **aux_prev = &sg->node[i].adj;

                while(aux != NULL)
                {
                	if(labels.find(truelabel) == labels.end())
                	{
						Set *tmp = aux;

						*aux_prev = aux->next;

						aux = aux->next;
						free(tmp);
                	}
                	else
                	{
                		aux_prev = &aux->next;
						aux = aux->next;
                	}

                }
            }
        }

        return sgresult;
    }

    void set_subgraph_features(Subgraph *sg, Features *f)
    {
        int i,j;

        sg->nfeats = f->nfeats;
        for (i=0; i < sg->nnodes; i++)
        {
            sg->node[i].feat = AllocFloatArray(sg->nfeats);

            for (j=0; j < sg->nfeats; j++)
            {
//                sg->node[i].feat[j] = f->elem[sg->node[i].pixel].feat[j];
                sg->node[i].feat[j] = f->elem[sg->node[i].position].feat[j];
            }
        }

    }



	/** Creates subgraph from samples selected in a uniform grid.
		@param mask if not NULL the samples are going to be selected only from pixels where mask->val[p] != 0.
		@param bb bounding box used to further contrain the region from where pixels are sampled, by default
		the whole image is considered.
	 */
	Subgraph *uniform_sampling(Image32 *img, Image32 *mask, int dx, int dy, Rectangle bb)
	{
		Subgraph *sg = NULL;
		int x, y, nnodes, i, p;

		Rectangle bb1;

		if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0 )
			bb1 = bb;
		else
			bb1 = Rectangle(0, 0, img->ncols - 1, img->nrows - 1);

		if(mask != NULL)
		{
			nnodes = 0;

			for(y = bb1.beg.y; y <= bb1.end.y; y = y + dy)
				for(x = bb1.beg.x; x <= bb1.end.x; x = x + dx)
				{
					p = to_index(img, x, y);

					if(mask->val[p])
					{
						nnodes++;
					}
				}

			sg = CreateSubgraph(nnodes);
			i = 0;

			for(y = bb1.beg.y; y <= bb1.end.y; y = y + dy)
				for(x = bb1.beg.x; x <= bb1.end.x; x = x + dx)
				{
					p = to_index(img, x, y);

					if(mask->val[p])
					{
//						sg->node[i].pixel = p;
						sg->node[i].position = p;
						i++;
					}
				}
		}
		else
		{
			nnodes = 0;

			for(y = bb1.beg.y; y <= bb1.end.y; y = y + dy)
				for(x = bb1.beg.x; x <= bb1.end.x; x = x + dx)
				{
					p = to_index(img, x, y);
					nnodes++;
				}

			sg = CreateSubgraph(nnodes);
			i = 0;

			for(y = bb1.beg.y; y <= bb1.end.y; y = y + dy)
				for(x = bb1.beg.x; x <= bb1.end.x; x = x + dx)
				{
					p = to_index(img, x, y);
//					sg->node[i].pixel = p;
					sg->node[i].position = p;
					i++;
				}
		}

		return(sg);
	}

	Image8* membership_map(DbImage *d1, DbImage *d2, double max_arc_weight,
                           Rectangle bb)
	{
		Image8 *map = create_image8(d1->ncols, d1->nrows);

        membership_map(map, d1, d2, max_arc_weight, bb);

		return map;
	}

	void membership_map(Image8 *map, DbImage *d1, DbImage *d2,
                        double max_arc_weight, Rectangle bb)
	{
		int x, y;
		double d = USIS_MAX_ARC_WEIGHT_PERC * max_arc_weight;
		Rectangle bb1;

   		if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0 )
			bb1 = bb;
		else
			bb1 = Rectangle(0, 0, d1->ncols - 1, d1->nrows - 1);

		for(y = bb1.beg.y; y <= bb1.end.y; y++)
			for(x = bb1.beg.x; x <= bb1.end.x; x++)
			{
				int p = x + y * d1->ncols;

				double fd1, fd2;

				fd1 = d1->val[p];
				fd2 = d2->val[p];
				double val = USIS_MAX_OBJMAP_VALUE * (fd2 + d) / (fd2 + fd1 + 2 * d);

                map->val[p] = (uchar)val;

			}
	}

	void membership_map(Image8 *map, DbImage *d1, DbImage *d2, Rectangle bb)
	{
		int x, y;
		Rectangle bb1;

   		if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0 )
			bb1 = bb;
		else
			bb1 = Rectangle(0, 0, d1->ncols - 1, d1->nrows - 1);

		for(y = bb1.beg.y; y <= bb1.end.y; y++)
			for(x = bb1.beg.x; x <= bb1.end.x; x++)
			{
				int p = x + y * d1->ncols;

				double fd1, fd2, val;

				fd1 = d1->val[p];
				fd2 = d2->val[p];

                val = USIS_MAX_OBJMAP_VALUE * fd1/fd2;

                map->val[p] = (uchar)val;
			}
	}

    /// Computes the membership map constrained to the pixels from the selected regions
	void membership_map_by_regions(Image8 *map, Image8 *regions, int *sel_regions, DbImage *d1, DbImage *d2, double max_arc_weight)
	{
		register int p, n;
		double d = USIS_MAX_ARC_WEIGHT_PERC * max_arc_weight;

		n = d1->ncols * d1->nrows;

		for(p = 0; p < n; p++)
		{
			if(sel_regions[regions->val[p]])
			{
				register float fd1, fd2;

				fd1 = (float)d1->val[p];
				fd2 = (float)d2->val[p];
				map->val[p] = USIS_MAX_OBJMAP_VALUE * (fd2 + d) / (fd2 + fd1 + 2 * d);
			}
		}
	}


	/** Fuzzy OPF**/

	void fopf_on_markers(Features *f, Set *Si, Set *Se,
						Subgraph **sgtrainobj, Subgraph **sgtrainbkg,
						int nsamples, double max_arc_weight,
						void *random_generator)
	{
		Subgraph *sg = NULL;
		float perc;

		if(f == NULL || Si == NULL || Se == NULL) return;

		sg = subgraph_from_markers(f, Si, Se);

		if(sg == NULL) return;

		if(sg->nnodes > nsamples)
			perc = (float)nsamples / (float)sg->nnodes;
		else
			perc = 0.5;

		sg->df = max_arc_weight;
		fopf_contrast_learning(sg, sgtrainobj, sgtrainbkg, perc,
								random_generator);

		DestroySubgraph(&sg);
	}

    /// Classify nodes of evaluation/test set for CompGraph
	double fopf_contrast_classify(Subgraph *sgtrain, Subgraph *sgeval)
	{
		int i, j;
		int nbkg_eval_samples = 0;
		int nobj_eval_samples = 0;
		double avg_obj_mmap = 0.0;
		double avg_bkg_mmap = 0.0;

		/// This value is used as a term to smooth the
		/// object membership value when the minimum cost
		/// to the object and to the background are close to zero
        ///	float d = sgtrain->node[sgtrain->ordered_list_of_nodes[sgtrain->nnodes -1]].pathval;
		double d = sgtrain->df * USIS_MAX_ARC_WEIGHT_PERC;

		for(i = 0; i < sgeval->nnodes; i++)
		{
			float weight, cost;
			float min_cost_obj = FLT_MAX;
			float min_cost_bkg = FLT_MAX;

			for(j = 0; j < sgtrain->nnodes; j++)
			{
				weight = opf_ArcWeight(sgtrain->node[j].feat, sgeval->node[i].feat, sgeval->nfeats);
				cost = MAX(sgtrain->node[j].pathval, weight);

				if(sgtrain->node[j].truelabel == USIS_OPFBKGLABEL)
					min_cost_bkg = MIN(min_cost_bkg, cost);
				else
					min_cost_obj = MIN(min_cost_obj, cost);
			}

			if(sgeval->node[i].truelabel == USIS_OPFBKGLABEL)
			{
				avg_bkg_mmap += (min_cost_bkg + d) / (min_cost_bkg + min_cost_obj + 2 * d);
				nbkg_eval_samples++;
			}
			else
			{
				avg_obj_mmap += (min_cost_bkg + d) / (min_cost_bkg + min_cost_obj + 2 * d);
				nobj_eval_samples++;
			}

			if(min_cost_obj < min_cost_bkg)
			{
				sgeval->node[i].label = USIS_OPFOBJLABEL;
			}
			else
			{
				sgeval->node[i].label = USIS_OPFBKGLABEL;
			}
		}

        /// Special case when there are not enough object/background
        /// samples to evaluate the contrast;
        if(nbkg_eval_samples == 0 || nobj_eval_samples == 0)
        {
            avg_bkg_mmap = 0;
            avg_obj_mmap = 0;
        }
        else
        {
            avg_bkg_mmap /= nbkg_eval_samples;
            avg_obj_mmap /= nobj_eval_samples;
        }

		/// We expect the cost to the background be higher
		/// than to the object
		return avg_obj_mmap - avg_bkg_mmap;
	}


    // Executes the learning procedure for CompGraph replacing the
    // missclassified samples in the evaluation set by non prototypes from
    // training set
	void fopf_contrast_learning(Subgraph *sg, Subgraph **sgtrainobj, Subgraph **sgtrainbkg,
								float perc, void *random_generator)
	{
		int i = 1, iterations = 5;
		double contrast = -USIS_DINFTY, max_contrast = -USIS_DINFTY;
		Subgraph *sgtrain_best = NULL;
		Subgraph *sgtrain = NULL, *sgeval = NULL;

		opf_SplitSubgraph(sg, &sgtrain, &sgeval, perc, random_generator);

		sgtrain->df = sg->df;
		opf_PrecomputedDistance = 0;

		do
		{
			USIS_VERBOSE_PRINTF("\nRunning iteration ... %d/%d\n", i, iterations);

			opf_OPFTraining(sgtrain);
			contrast = fopf_contrast_classify(sgtrain, sgeval);

			if(contrast > max_contrast)
			{
				max_contrast = contrast;

				if(sgtrain_best != NULL) DestroySubgraph(&sgtrain_best);

				sgtrain_best = CopySubgraph(sgtrain);
			}

			opf_SwapErrorsbyNonPrototypes(&sgtrain, &sgeval, random_generator);

			USIS_VERBOSE_PRINTF("contrast: %f\n", contrast);

			i++;
		}
		while(i <= iterations);

		set<int> obj_labels, bkg_labels;
		obj_labels.insert(USIS_OPFOBJLABEL);
		bkg_labels.insert(USIS_OPFBKGLABEL);

		*sgtrainobj = induced_subgraph_by_truelabel(sgtrain_best, obj_labels);
		*sgtrainbkg = induced_subgraph_by_truelabel(sgtrain_best, bkg_labels);

		(*sgtrainobj)->df = sgtrain_best->df;
		(*sgtrainbkg)->df = sgtrain_best->df;

		USIS_VERBOSE_PRINTF("Best accuracy %f\n", max_contrast);

		DestroySubgraph(&sgeval);
		DestroySubgraph(&sgtrain);
		DestroySubgraph(&sgtrain_best);
	}


	/** Clusters **/

	/** Performs clustering on markers then filters them according to a purity criterion **/
	float filter_sg_by_clustering_markers(Features *f, Set **Si, Set **Se, int lenSi, int lenSe,
										Subgraph **sgfiltered, int nsamples, void *random_generator)
	{
		Subgraph *sg = NULL;
		*sgfiltered = NULL;
		float sample_perc;

		int i, acc = 0;

		for(i = 0; i < lenSi; i++)
			acc += GetSetSize(Si[i]);

		for(i = 0; i < lenSe; i++)
			acc += GetSetSize(Se[i]);

		if(acc <= 4 * nsamples)
			sample_perc = 0.75;
		else if(acc <= 6 * nsamples)
			sample_perc = 0.5;
		else
			sample_perc = 0.2;

		USIS_VERBOSE_PRINTF("lenSi %d lenSe %d acc %d, nsamples %d, sample_perc  %f\n", lenSi, lenSe, acc, nsamples, sample_perc);

		sg = subgraph_from_markers(f, Si, Se, lenSi, lenSe, sample_perc, random_generator);

		float runtime = filter_sg_by_clustering(sg, sgfiltered);

		DestroySubgraph(&sg);

		return runtime;
	}


	float filter_sg_by_clustering(Subgraph *sg, Subgraph **sgfiltered)
	{
		int i,kmin, kmax;
		int nbkg = 0, nobj = 0, nsmallest;

		opf_ArcWeightFun tmp_fun = opf_ArcWeight;
		opf_ArcWeight = opf_EuclDist;

		for(i = 0; i < sg->nnodes; i++)
			if(sg->node[i].truelabel == USIS_OPFOBJLABEL) nobj++;
			else nbkg++;

		nsmallest = MIN(nbkg, nobj);

		USIS_VERBOSE_PRINTF("nsmallest %d\n", nsmallest);

		kmin = (int)MAX(MIN(5.0, ((float)nsmallest) * 0.04), 1);
		kmax = (int)MAX(MIN(50.0, ((float)nsmallest) * 0.08), 1);

        USIS_VERBOSE_PRINTF("\nSelecting enhancement pixels by marker clustering...\n");

        // Clustering
		timer *tic1 = Tic();
		opf_BestkMinCut(sg, kmin, kmax);
		opf_OPFClustering(sg);
		timer *toc1 = Toc();

		float runtime = CTime(&tic1, &toc1);

        USIS_VERBOSE_PRINTF("Marker selection by clustering runtime: %fms\n", runtime);

		timer *tic2 = Tic();
		*sgfiltered = filter_clusters_by_truelabel(sg, 2, USIS_OPFOBJLABEL, 0.96);
		timer *toc2 = Toc();

		float runtime_filter = CTime(&tic2, &toc2);
		runtime += runtime_filter;

        USIS_VERBOSE_PRINTF( "Filter clusters run time: %fms\n", runtime_filter);

		if(*sgfiltered != NULL)
			(*sgfiltered)->nlabels = 2;

		opf_ArcWeight = tmp_fun;

		return runtime;
	}

	Subgraph *clustering_outliers(Subgraph *sgtrain, Subgraph *sgtest)
	{
		int i;
		set<int> outliers;
		Subgraph *sg_outliers = NULL;

		opf_ArcWeightFun tmp_fun = opf_ArcWeight;
		opf_ArcWeight = opf_EuclDist;

		for(i = 0; i < sgtest->nnodes; i++)
		{
			int j;
			bool is_outlier = true;
			for(j = 0; j < sgtrain->nnodes && is_outlier; j++)
			{
				float dist = opf_ArcWeight(sgtrain->node[j].feat,
											sgtest->node[i].feat,
											sgtrain->nfeats);

				is_outlier = dist > sgtrain->node[j].radius;
			}

			if(is_outlier)
				outliers.insert(i);
		}

		if(outliers.size() > 0)
			sg_outliers = induced_subgraph(sgtest, outliers);

		opf_ArcWeight = tmp_fun;

		return sg_outliers;
	}

	Subgraph *filter_sg_by_clust_outliers(Subgraph *sgtrain, Subgraph *sgtest)
	{
		int kmin, kmax;

		opf_ArcWeightFun tmp_fun = opf_ArcWeight;
		opf_ArcWeight = opf_EuclDist;

		kmin = (int)MAX(MIN(5.0, ((float)sgtrain->nnodes) * 0.04), 1);
		kmax = (int)MAX(MIN(50.0, ((float)sgtrain->nnodes) * 0.08), 1);

        USIS_VERBOSE_PRINTF("\nSelecting pixels as outliers of clustering...\n");

        // Clustering
		opf_BestkMinCut(sgtrain, kmin, kmax);
		opf_OPFClustering(sgtrain);

		opf_ArcWeight = tmp_fun;

		return clustering_outliers(sgtrain, sgtest);
	}

	/** Computes a filtered subgraph from marker clustering then trains a Fuzzy OPF classifier **/
	float fopf_on_clustered_markers(Features *f, Set **Si, Set **Se, int lenSi,
									int lenSe, Subgraph **sgtrainobj, Subgraph **sgtrainbkg,
									int nsamples, double max_arc_weight,
									void *random_generator)
	{
		Subgraph *sgfiltered = NULL;

		float runtime = filter_sg_by_clustering_markers(f, Si, Se, lenSi, lenSe,
														&sgfiltered, nsamples,
														random_generator);

		if(sgfiltered != NULL)
		{
			float split_perc;

			if(sgfiltered->nnodes < nsamples)
				split_perc = 0.5;
			else
				split_perc = (float)nsamples / (float)sgfiltered->nnodes;

			sgfiltered->df = max_arc_weight;

			timer *tic = Tic();
			fopf_contrast_learning(sgfiltered, sgtrainobj, sgtrainbkg, split_perc,
									random_generator);
			timer *toc = Toc();

			runtime += CTime(&tic, &toc);

			DestroySubgraph(&sgfiltered);
		}

		return runtime;
	}


	/** Eliminates unnecessary samples by filtering markers according to a clustered Image32*,
		then computes a new graph and executes the supervised OPF learning
		@return run time of the marker selection operation
	 */
	float fopf_by_clustered_regions(Features *f, Image32 *clusters, Set **Si, Set **Se, int lenSi, int lenSe,
									Subgraph **sgtrainobj, Subgraph **sgtrainbkg, int nsamples, double max_arc_weight,
									void *random_generator)
	{
		Subgraph *sg = NULL, *sgfiltered = NULL;
		float split_perc;

		int i, acc = 0;

		for(i = 0; i < lenSi; i++)
			acc += GetSetSize(Si[i]);

		for(i = 0; i < lenSe; i++)
			acc += GetSetSize(Se[i]);

		sg = subgraph_from_markers(f, Si, Se, lenSi, lenSe);

		if(sg->nnodes > 3 * nsamples)
		{
			split_perc = (float)nsamples / (float)sg->nnodes;
		}
		else if(sg->nnodes > nsamples)
		{
			split_perc = (float)nsamples / (float)sg->nnodes;
		}
		else
		{
			split_perc = 0.5;
		}

		USIS_VERBOSE_PRINTF("\nSelecting enhancement pixels by analyzing image clusters...\n");

		/// Obtaining labels from cluster labels

		timer *tic = Tic();

		int nclusters = INT_MIN;

		for(i = 0; i < sg->nnodes; i++)
		{
//			sg->node[i].label = clusters->val[sg->node[i].pixel];
			sg->node[i].label = clusters->val[sg->node[i].position];
			nclusters = MAX(nclusters, sg->node[i].label);
		}

		sg->nlabels = nclusters;

		sgfiltered = filter_clusters_by_truelabel(sg, 2, USIS_OPFOBJLABEL, 0.96);
		timer *toc = Toc();

		float runtime = CTime(&tic, &toc);

		USIS_VERBOSE_PRINTF("Filter clusters run time: %fms\n", runtime);

		if(sgfiltered == NULL)
		{
			DestroySubgraph(&sg);
			*sgtrainobj = NULL;
			*sgtrainbkg = NULL;
			return runtime;
		}

		USIS_VERBOSE_PRINTF("Training OPF...\n");

		sgfiltered->nlabels = 2;
		sgfiltered->df = max_arc_weight;

		fopf_contrast_learning(sgfiltered, sgtrainobj, sgtrainbkg, split_perc,
								random_generator);

		DestroySubgraph(&sg);
		DestroySubgraph(&sgfiltered);

		return runtime;

	}


	Subgraph *filter_clusters_by_truelabel(Subgraph *sg, int ntruelabels,
											int preferred_label, float threshold)
	{
		register int i, l, itrain, clusternnodes;
		const int nclusters = sg->nlabels + 1;
		const int nlabels = ntruelabels + 1;
		int trainnodes = 0, cluster; /// number of resulting nodes
		int *truelabel_rep = AllocIntArray(nlabels);

		int **usableclusters = (int**)calloc(nclusters, sizeof(int*));
		int **clusters = (int**)calloc(nclusters, sizeof(int*));

		for(i = 0; i < nclusters; i++)
		{
			clusters[i] = AllocIntArray(nlabels);
			usableclusters[i] = AllocIntArray(2);
		}

		/// counting number of nodes per label in each cluster
		for(i = 0; i < sg->nnodes; i++)
			(clusters[sg->node[i].label][sg->node[i].truelabel])++;

		for(i = 0; i < nclusters; i++)
		{
			clusternnodes = 0;
			usableclusters[i][0] = INT_MIN;

			/// couting number of nodes in cluster i
			for(l = 1; l < nlabels; l++)
				clusternnodes += clusters[i][l];

			for(l = 1; l < nlabels; l++)
			{
				float tlabelsperc = (float)clusters[i][l] / (float)clusternnodes;

				if(tlabelsperc >= threshold || l == preferred_label)
				{
					usableclusters[i][0] = clusters[i][l]; /// number of nodes from truelabel l
					usableclusters[i][1] = l; /// truelabel l
					trainnodes += usableclusters[i][0];
					break;
				}
			}

		}

		Subgraph *result = CreateSubgraph(trainnodes);

		result->nfeats = sg->nfeats;
		result->df = sg->df;

		itrain = 0;

		/// copying usable nodes
		for(i = 0; i < sg->nnodes; i++)
		{
			cluster = sg->node[i].label;

			if(usableclusters[cluster][0] >= 0)
			{
				if(usableclusters[cluster][0] > 0 &&
						usableclusters[cluster][1] == sg->node[i].truelabel)
				{
					CopySNode(&(result->node[itrain]), &(sg->node[i]), sg->nfeats);
					usableclusters[cluster][0]--;
					/// Counting the number of representative nodes for each true label
					truelabel_rep[result->node[itrain].truelabel]++;
					itrain++;
				}
			}
		}

		for(i = 0; i < nclusters; i++)
		{
			free(clusters[i]);
			free(usableclusters[i]);
		}

		free(clusters);
		free(usableclusters);

		for(i = 1; i < nlabels; i++)
		{
		    /// At least two nodes for each label are required for intelligent learning
			if(truelabel_rep[i] <= 1)
			{
				DestroySubgraph(&result);

				USIS_VERBOSE_PRINTF("\nNot enough samples for truelabel %d!! Only %d were selected.\n", i,truelabel_rep[i]);

				free(truelabel_rep);

				return NULL;
			}
		}

		/// updating amount of labels
		result->nlabels = ntruelabels;

        free(truelabel_rep);

		return result;
	}

	Subgraph* train_prot_classifier(Subgraph *sg, float kmin_perc,
										float kmax_perc)
	{
		Subgraph *sg_prot = NULL;

        opf_ArcWeightFun tmpFun = opf_ArcWeight;
        opf_ArcWeight = opf_EuclDist;

        if(sg != NULL && sg->nnodes >= 2)
        {
			int i, i0, kmin, kmax;

			kmin = MAX(kmin_perc*sg->nnodes,1);
			kmax = MAX(kmax_perc*sg->nnodes,1);

			for(i = 0; i < sg->nnodes; i++)
			{
				sg->node[i].status = 0; // training node
			}

			opf_BestkMinCut(sg, kmin,kmax);

			opf_OPFClustering(sg);

			int nprototypes = sg->nlabels; /// one prototype per label/cluster

			for(i = 0; i < sg->nnodes; i++)
				if(sg->node[i].pred == NIL)
				{
					sg->node[i].radius = -USIS_DINFTY;
				}

			for(i = 0; i < sg->nnodes; i++)
			{
				int root = sg->node[i].root;
				double dist = opf_ArcWeight(sg->node[i].feat,
											sg->node[root].feat,
											sg->nfeats);
				double radius = sg->node[root].radius;

				sg->node[root].radius = MAX(dist, radius);
			}

			sg_prot = CreateSubgraph(nprototypes);
			sg_prot->nfeats = sg->nfeats;
			sg_prot->bestk = sg->bestk;
			sg_prot->df = sg->df;
			sg_prot->nlabels = sg->nlabels;
			sg_prot->nfeats = sg->nfeats;
			sg_prot->mindens = sg->mindens;
			sg_prot->maxdens = sg->maxdens;
			sg_prot->K = sg->K;



			for(i = 0, i0 = 0; i < sg->nnodes; i++)
			{
				if(sg->node[i].pred == NIL)
				{
					CopySNode(&(sg_prot->node[i0]),
							  &(sg->node[i]), sg_prot->nfeats);
					i0++;
				}
			}
        }

		opf_ArcWeight = tmpFun;

		return sg_prot;
	}

	bool train_prot_fuzzy_classifier(Subgraph *sg_train_internal,
										Subgraph *sg_train_external,
										float kmin_perc,
										float kmax_perc,
										Subgraph **sg_internal_prot,
										Subgraph **sg_external_prot)
    {
        /** Clearing previous prototypes **/
        if(sg_internal_prot != NULL && *sg_internal_prot != NULL)
        	DestroySubgraph(sg_internal_prot);
        if(sg_external_prot != NULL && *sg_external_prot != NULL)
        	DestroySubgraph(sg_external_prot);

		opf_DestroyArcs(sg_train_internal);
		opf_DestroyArcs(sg_train_external);

		*sg_internal_prot = train_prot_classifier(sg_train_internal,kmin_perc, kmax_perc);
		*sg_external_prot = train_prot_classifier(sg_train_external,kmin_perc, kmax_perc);
        /** If both internal and external classifiers exist (i.e., at least
            two internal and external samples for this classification window were
            sucessfully selected) then the prototypes may be chosen to
            perform fuzzy classification.
         **/
        return *sg_internal_prot != NULL && *sg_external_prot;

    }


	double prot_fuzzy_classify_sample(float *feat,
										Subgraph *int_prot,
										Subgraph *ext_prot)
	{
        double bkg_dens, bkg_radius, bkg_sigma2, bkg_membership;
        double obj_dens, obj_radius, obj_sigma2, obj_membership;
        double membership = NIL;
		double epsilon = 0.000001;

        opf_ArcWeightFun tmpFun = opf_ArcWeight;
        opf_ArcWeight = opf_EuclDist;

        if(int_prot != NULL && ext_prot != NULL)
        {
            int j, j_obj = 0, j_bkg = 0;

            double min_dist_2_obj = DBL_MAX;
            double min_dist_2_bkg = DBL_MAX;

            for(j = 0; j < int_prot->nnodes; j++)
            {
                double dist = opf_ArcWeight(int_prot->node[j].feat,
                                            feat,
                                            int_prot->nfeats);
                if(dist < min_dist_2_obj)
                {
                    min_dist_2_obj = dist;
                    j_obj = j;
                }
            }

            for(j = 0; j < ext_prot->nnodes; j++)
            {
                double dist = opf_ArcWeight(ext_prot->node[j].feat,
                                            feat, ext_prot->nfeats);

                if(dist < min_dist_2_bkg)
                {
                    min_dist_2_bkg = dist;
                    j_bkg = j;
                }
            }

            bkg_dens = ext_prot->node[j_bkg].dens;
            bkg_radius = ext_prot->node[j_bkg].radius;

            if(bkg_radius > DBL_EPSILON)
            {
                bkg_sigma2 = 2.0*bkg_radius/9.0;
                bkg_membership = bkg_dens*exp(-min_dist_2_bkg/bkg_sigma2)/opf_MAXDENS;
            }
            else
            {
                bkg_membership = bkg_dens/opf_MAXDENS;
            }

            obj_dens = int_prot->node[j_obj].dens;
            obj_radius = int_prot->node[j_obj].radius;

            if(obj_radius > DBL_EPSILON)
            {
                obj_sigma2 = 2.0*obj_radius/9.0;
                obj_membership = obj_dens*exp(-min_dist_2_obj/obj_sigma2)/opf_MAXDENS;
            }
            else
            {
                obj_membership = obj_dens/opf_MAXDENS;
            }

            membership = obj_membership/(bkg_membership+obj_membership+epsilon);
        }

        opf_ArcWeight = tmpFun;

        return membership;
	}


    /// Classify nodes of evaluation/test set for CompGraph
	double prot_contrast_classify(Subgraph *sg_int_prot, Subgraph *sg_ext_prot,
										Subgraph *sgeval)
	{
		int i;
		int nbkg_eval_samples = 0;
		int nobj_eval_samples = 0;
		double avg_obj_mmap = 0.0;
		double avg_bkg_mmap = 0.0;

		for(i = 0; i < sgeval->nnodes; i++)
		{
			double membership = prot_fuzzy_classify_sample(sgeval->node[i].feat,
															sg_int_prot,
															sg_ext_prot);

			if(sgeval->node[i].truelabel == USIS_OPFBKGLABEL)
			{
				avg_bkg_mmap += membership;
				nbkg_eval_samples++;
			}
			else
			{
				avg_obj_mmap += membership;
				nobj_eval_samples++;
			}

			if(membership >= 0.5)
			{
				sgeval->node[i].label = USIS_OPFOBJLABEL;
			}
			else
			{
				sgeval->node[i].label = USIS_OPFBKGLABEL;
			}
		}

        /// Special case when there are not enough object/background
        /// samples to evaluate the contrast;
        if(nbkg_eval_samples == 0 || nobj_eval_samples == 0)
        {
            avg_bkg_mmap = 0.0;
            avg_obj_mmap = 0.0;
        }
        else
        {
            avg_bkg_mmap /= nbkg_eval_samples;
            avg_obj_mmap /= nobj_eval_samples;
        }

		/// We expect the cost to the background be higher
		/// than to the object
		return avg_obj_mmap - avg_bkg_mmap;
	}

    // Executes the learning procedure for CompGraph replacing the
    // missclassified samples in the evaluation set by non prototypes from
    // training set
	void prot_contrast_learning(Subgraph *sg, Subgraph **sgtrainobj,
								Subgraph **sgtrainbkg, float split_perc,
								float kmin_perc, float kmax_perc,
								void *random_generator)
	{
		int i = 1, iterations = 5;
		double contrast = -USIS_DINFTY, max_contrast = -USIS_DINFTY;
		Subgraph *sgtrain_int_best = NULL, *sgtrain_ext_best = NULL;
		Subgraph *sgtrain = NULL, *sgeval = NULL;
		Subgraph *sgtrain_int = NULL, *sgtrain_ext = NULL;
		Subgraph *sgtrain_int_prot = NULL, *sgtrain_ext_prot = NULL;

		opf_SplitSubgraph(sg, &sgtrain, &sgeval, split_perc, random_generator);

		sgtrain->df = sg->df;

		set<int> obj_labels, bkg_labels;
		obj_labels.insert(USIS_OPFOBJLABEL);
		bkg_labels.insert(USIS_OPFBKGLABEL);

		sgtrain_int = induced_subgraph_by_truelabel(sgtrain, obj_labels);
		sgtrain_ext = induced_subgraph_by_truelabel(sgtrain, bkg_labels);

		opf_PrecomputedDistance = 0;

		do
		{
			USIS_VERBOSE_PRINTF("\nRunning iteration ... %d/%d\n", i, iterations);

			train_prot_fuzzy_classifier(sgtrain_int, sgtrain_ext, kmin_perc,
										kmax_perc, &sgtrain_int_prot,
										&sgtrain_ext_prot);
			contrast = prot_contrast_classify(sgtrain_int_prot, sgtrain_ext_prot,
												sgeval);

			contrast = opf_Accuracy(sgeval);

			if(contrast > max_contrast)
			{
				max_contrast = contrast;

				if(sgtrain_int_best != NULL) DestroySubgraph(&sgtrain_int_best);
				if(sgtrain_ext_best != NULL) DestroySubgraph(&sgtrain_ext_best);

				sgtrain_int_best = CopySubgraph(sgtrain_int_prot);
				sgtrain_ext_best = CopySubgraph(sgtrain_ext_prot);
			}



//			fprintf(stderr,"Contrast: %f Accuracy %f\n", contrast, );
//			USIS_VERBOSE_PRINTF("Contrast: %f\n", contrast);

//			int foo;
//			move_misclassified_labeled_nodes(&sgeval, &sgtrain_int, &foo);
//			fprintf(stderr,"misclassified int %d\n",foo);
//			move_misclassified_labeled_nodes(&sgeval, &sgtrain_ext, &foo);
//			fprintf(stderr,"misclassified int %d\n",foo);

			opf_DestroyArcs(sgtrain_int);
			opf_DestroyArcs(sgtrain_ext);
			opf_DestroyArcs(sgeval);

			swap_errors_by_labeled_non_prot(&sgtrain_int, &sgeval, random_generator);
			swap_errors_by_labeled_non_prot(&sgtrain_ext, &sgeval, random_generator);

			DestroySubgraph(&sgtrain_int_prot);
			DestroySubgraph(&sgtrain_ext_prot);
			i++;
		}
		while(i <= iterations);

		*sgtrainobj = sgtrain_int_best;
		*sgtrainbkg = sgtrain_ext_best;

		USIS_VERBOSE_PRINTF("Best accuracy %f\n", max_contrast);

		DestroySubgraph(&sgtrain_int);
		DestroySubgraph(&sgtrain_ext);
		DestroySubgraph(&sgeval);
		DestroySubgraph(&sgtrain);
	}

    /// Returns true if at least one foreground or background marker has a percentage
    /// of its pixels selected on pixels which were misclassified (i.e. if the membership value
    /// is less than USIS_MAX_OBJMAP_VALUE/2 for an object seed or greater than USIS_MAX_OBJMAP_VALUE/2
    /// for a background seed). Parameters objClass and bkgClass return which class needs
    /// to have its pixels re-classified
	void assess_relearning(Image32 *objMap, Set **Si, Set **Se, int lenSi,
                            int lenSe, float mksize_threshold, int n_int_seeds,
                            int n_ext_seeds, float nseeds_threshold, bool *objClass,
                            bool *bkgClass)
	{
		int i;

		const int class_thresh = USIS_MAX_OBJMAP_VALUE / 2;

		*objClass = *bkgClass = false;

		for(i = 0; i < lenSi; i++)
		{
			int counter = 0; // counts the amount of misclassfied pixels
			int mksize = 0;  // counts the amount of pixels in the marker
			Set *aux = Si[i];

			for(mksize = 0; aux != NULL; aux = aux->next)
			{
				mksize++;

				if(objMap->val[aux->elem] < class_thresh) counter++;

			}

			if((float)counter / (float)mksize >= mksize_threshold && (float)counter / (float) n_ext_seeds >= nseeds_threshold)
			{
				*bkgClass = true;//if an object marker was selected on pixels classified as background
				//then the object membership map should be recomputed for background pixels
				break;
			}
		}

		for(i = 0; i < lenSe; i++)
		{
			int counter = 0; //counts the amount of misclassfied pixels
			int mksize = 0;
			Set *aux = Se[i];

			for(mksize = 0; aux != NULL; aux = aux->next)
			{
				mksize++;

				if(objMap->val[aux->elem] > class_thresh) counter++;
			}

			if((float)counter / (float)mksize >= mksize_threshold && (float)counter / (float) n_int_seeds >= nseeds_threshold)
			{
				*objClass = true;//if a background marker was selected on pixels classified as object
				//then the object membership map should be recomputed for object pixels
				break;
			}
		}
	}

	/** Threaded OPF Classification **/

	/** Subgraph **/
	Set *subsample_markers(Set **S, int len, int *ntotmarkers, float perc,
							void *random_generator)
	{
		Set *aux = NULL;
		Set *R = NULL; // Chosen pixels
		int nmarkers, nchosenmarkers, i, j, p;
		int *marker_pixels;

		for(i = 0; i < len; i++)
		{
			nmarkers = GetSetSize(S[i]);
			nchosenmarkers = MAX((int)(((float)nmarkers) * perc), 1);
			(*ntotmarkers) += nchosenmarkers;
			marker_pixels = AllocIntArray(nmarkers);
			j = 0;

			for(j = 0, aux = S[i]; aux != NULL; j++, aux = aux->next)
				marker_pixels[j] = aux->elem;

			// Knuth-Shuffle:
            // When a new sample p is
            // selected it is removed from future selections
            // by "swapping" it with the sample pointed by nmarkers-1
            // and forcing the pointer to move back one index (nmarkers--).
			do
			{
				p = random_integer(0, nmarkers - 1, random_generator);
                InsertSet(&R, marker_pixels[p]);

                // replacing selected pixel with the last one from the array
                marker_pixels[p] = marker_pixels[nmarkers-1];
                nmarkers--;

                nchosenmarkers--;

			}
			while(nchosenmarkers > 0);

			free(marker_pixels);
		}

		return R;
	}

	Subgraph *subgraph_from_regions(Image32 *regions)
	{
		AdjRel *A = fast_circular(1.5);
		int nregions;

		int max_lb = maximum_value(regions);
		int min_lb = minimum_value(regions);

		nregions = max_lb - min_lb +1;

		Subgraph *sg = CreateSubgraph(nregions);

		for(int p = 0; p < regions->n; p++)
		{
			int lb = regions->val[p];
			int lb_id = lb-min_lb;
			Pixel px = to_pixel(regions,p);

			if(sg->node[lb_id].truelabel != lb)
			{
				sg->node[lb_id].truelabel = lb;
				sg->node[lb_id].position = p;
			}

			for(int i = 1; i < A->n; i++)
			{
				Pixel px_adj = adjacent(A, px, i);

				if(valid_pixel(regions, px_adj))
				{
					int p_adj = to_index(regions, px_adj);
					int lb_adj = regions->val[p_adj];
					int lb_adj_id = lb_adj - min_lb;

					if(lb_adj != lb)
					{
						if(!IsInSet(sg->node[lb_id].adj, lb_adj_id))
							InsertSet(&sg->node[lb_id].adj, lb_adj_id);
					}
				}
			}
		}

		destroy_adjrel(&A);

		return sg;
	}

	void set_region_graph_feats(Subgraph *sg, Image32 *regions, Features *f)
	{
		int min_lb = minimum_value(regions);
		int nfeats = 3;
		int *region_size = AllocIntArray(sg->nnodes);

		sg->nfeats = nfeats;

		for(int i = 0; i < sg->nnodes; i++)
		{
			if(sg->node[i].feat != NULL)
				free(sg->node[i].feat);

			sg->node[i].feat = AllocFloatArray(sg->nfeats);
		}

		for(int p = 0; p < regions->n; p++)
		{
			int lb = regions->val[p];
			int lb_id = lb-min_lb;

			region_size[lb_id]++;

			for(int i = 0; i < sg->nfeats; i++)
				sg->node[lb_id].feat[i] += f->elem[p].feat[i];
		}

		for(int i = 0; i < sg->nnodes; i++)
		{
			for(int j = 0; j < sg->nfeats; j++)
				sg->node[i].feat[j] /= MAX(region_size[i],1);
		}

		free(region_size);
	}

	void prot_fuzzy_classify_regions(Subgraph *sg, Subgraph *int_prot, Subgraph *ext_prot,
									Image32 *regions, Features *f, DbImage *membership_map)
	{
		int min_lb = minimum_value(regions);
		double *region_membership = AllocDoubleArray(sg->nnodes);
		float **avg_region_feat = (float**)calloc(sg->nnodes, sizeof(float*));

		double *avg_region_dist = AllocDoubleArray(sg->nnodes);
		double *var_region_dist = AllocDoubleArray(sg->nnodes); // distance variance
		double *region_size = AllocDoubleArray(sg->nnodes);

		for(int i = 0; i < sg->nnodes; i++)
		{
			int lb = sg->node[i].truelabel;
			int lb_id = lb - min_lb;
			double membership;

			membership = prot_fuzzy_classify_sample(sg->node[i].feat,
													int_prot, ext_prot);
			region_membership[lb_id] = membership;

			avg_region_feat[lb_id] =  AllocFloatArray(sg->nfeats);
		}

		for(int p = 0; p < regions->n; p++)
		{
			int lb = regions->val[p];
			int lb_id = lb - min_lb;

			for(int i = 0; i < f->nfeats; i++)
				avg_region_feat[lb_id][i] += f->elem[p].feat[i];

			region_size[lb_id]++;
		}

		for(int i = 0; i < sg->nnodes; i++)
			for(int j = 0; j < f->nfeats; j++)
				avg_region_feat[i][j] /= region_size[i];

		for(int p = 0; p < regions->n; p++)
		{
			int lb = regions->val[p];
			int lb_id = lb - min_lb;

			double dist = sqrt(opf_EuclDist(f->elem[p].feat,
										avg_region_feat[lb_id],
										f->nfeats));

			avg_region_dist[lb_id] += dist;
			var_region_dist[lb_id] += dist*dist;
		}

		for(int i = 0; i < sg->nnodes; i++)
		{
			double avg_dist2;
			avg_region_dist[i] /= region_size[i];

			avg_dist2 = pow(avg_region_dist[i],2.0);

			var_region_dist[i] = var_region_dist[i]/region_size[i] - avg_dist2;
		}

		for(int p = 0; p < regions->n; p++)
		{
			int lb = regions->val[p];
			int lb_id = lb - min_lb;
			double membership;

			membership = region_membership[lb_id];
			membership_map->val[p] = membership;
		}

		for(int i = 0; i < sg->nnodes; i++)
			free(avg_region_feat[i]);

		free(avg_region_feat);
		free(avg_region_dist);
		free(var_region_dist);
		free(region_membership);
	}

	Subgraph *subgraph_from_set(Features *f, Set *S, int truelabel)
	{

		Subgraph *sg = NULL;
		Set *aux;
		int n, i;

		n = GetSetSize(S);

		if(n == 0) return NULL;

		//--------CreateSubGraph-----------------

		sg = CreateSubgraph(n);
		sg->nlabels = 1;

		if(sg->node == NULL) Error((char*)"Cannot allocate nodes", (char*)"subgraph_from_markers");

		i = 0;
		aux = S;

		while(aux != NULL)
		{
//			sg->node[i].pixel  = aux->elem;
			sg->node[i].position  = aux->elem;
			sg->node[i].truelabel = truelabel;
			sg->node[i].status = 0; // training node
			aux = aux->next;
			i++;
		}

		set_subgraph_features(sg, f);

		return sg;
	}


	Subgraph *subgraph_from_markers(Features *f, Set *Si, Set *Se)
	{

		Subgraph *sg = NULL;
		Set *aux;
		int ni, ne, i;

		ni = GetSetSize(Si);
		ne = GetSetSize(Se);

		if(ni == 0 || ne == 0) return NULL;

		//--------CreateSubGraph-----------------

		sg = CreateSubgraph(ni + ne);
		sg->nlabels = 2;

		if(sg->node == NULL) Error((char*)"Cannot allocate nodes", (char*)"subgraph_from_markers");

		i = 0;
		aux = Si;

		while(aux != NULL)
		{
//			sg->node[i].pixel  = aux->elem;
			sg->node[i].position  = aux->elem;
			sg->node[i].truelabel = USIS_OPFOBJLABEL;
			sg->node[i].status = 0; // training node
			aux = aux->next;
			i++;
		}

		aux = Se;

		while(aux != NULL)
		{
//			sg->node[i].pixel  = aux->elem;
			sg->node[i].position  = aux->elem;
			sg->node[i].truelabel = USIS_OPFBKGLABEL;
			sg->node[i].status = 0; // training node
			aux = aux->next;
			i++;
		}

		set_subgraph_features(sg, f);

		return sg;
	}

	Subgraph *subgraph_from_markers(Features *f, Set **Si, Set **Se, int lenSi, int lenSe)
	{
		Subgraph *sg = NULL;
		Set *aux;
		int ni = 0, ne = 0, i, k;

		for(k = 0; k < lenSi; k++)
			ni += GetSetSize(Si[k]);

		for(k = 0; k < lenSe; k++)
			ne += GetSetSize(Se[k]);

		if(ni == 0 || ne == 0) return NULL;

		//--------CreateSubGraph-----------------

		sg = CreateSubgraph(ni + ne);
		sg->nlabels = 2;

		if(sg->node == NULL) Error((char*)"Cannot allocate nodes", (char*)"subgraph_from_markers");

		i = 0;

		for(k = 0; k < lenSi; k++)
		{
			aux = Si[k];

			while(aux != NULL)
			{
//				sg->node[i].pixel  = aux->elem;
				sg->node[i].position  = aux->elem;
				sg->node[i].truelabel = USIS_OPFOBJLABEL;
				sg->node[i].status = 0; // training node
				aux = aux->next;
				i++;
			}
		}

		for(k = 0; k < lenSe; k++)
		{
			aux = Se[k];

			while(aux != NULL)
			{
//				sg->node[i].pixel  = aux->elem;
				sg->node[i].position  = aux->elem;
				sg->node[i].truelabel = USIS_OPFBKGLABEL;
				sg->node[i].status = 0; // training node
				aux = aux->next;
				i++;
			}
		}

		set_subgraph_features(sg, f);

		return sg;
	}

	Subgraph *subgraph_from_markers(Features *f, Set **Si, Set **Se, int lenSi, int lenSe,
									float perc, void *random_generator)
	{
		Subgraph *sg = NULL;
		Set *auxI = NULL, *auxE = NULL;
		Set *Mi, *Me;
		int ni = 0, ne = 0, i = 0;

		Mi = subsample_markers(Si, lenSi, &ni, perc, random_generator);
		Me = subsample_markers(Se, lenSe, &ne, perc, random_generator);

		//--------CreateSubGraph-----------------
		sg = CreateSubgraph(ni + ne);
		sg->nlabels = 2;

		auxI = Mi;
		auxE = Me;

//	Image32 *samples = CreateImage(f->ncols, f->nrows);

		while(auxI != NULL)
		{
//			sg->node[i].pixel  = auxI->elem;
			sg->node[i].position  = auxI->elem;
			sg->node[i].truelabel = USIS_OPFOBJLABEL;
			sg->node[i].status = 0; // training node
			auxI = auxI->next;
			i++;
		}

		while(auxE != NULL)
		{
//			sg->node[i].pixel  = auxE->elem;
			sg->node[i].position  = auxE->elem;
			sg->node[i].truelabel = USIS_OPFBKGLABEL;
			sg->node[i].status = 0; // training node
			auxE = auxE->next;
			i++;
		}

//    WriteImage(samples, "samples.pgm");
//	DestroyImage(&samples);

		set_subgraph_features(sg, f);

		DestroySet(&Mi);
		DestroySet(&Me);

		return sg;
	}


// Replace errors from evaluating set by non prototypes from training set
// Notice that all nodes in this training set have the same label
	void swap_errors_by_labeled_non_prot(Subgraph **sgtrain, Subgraph **sgeval,
										void *random_generator)
	{
		int i, nnon_prototypes = 0, nerrors = 0, last_node;
		const int truelabel = (*sgtrain)->node[0].truelabel;

		for(i = 0; i < (*sgtrain)->nnodes; i++)
			if((*sgtrain)->node[i].pred != NIL) nnon_prototypes++;

		for(i = 0; i < (*sgeval)->nnodes; i++)
			if((*sgeval)->node[i].label != (*sgeval)->node[i].truelabel) nerrors++;

		last_node = (*sgtrain)->nnodes-1;
		for(i = 0; i < (*sgeval)->nnodes && nnon_prototypes > 0 && nerrors > 0; i++)
		{
			// only nodes labeled incorrectly whose truelabel is the same of
			// the nodes in sgtrain will be replaced, therefore errors
			// from other labels will remain
			if((*sgeval)->node[i].truelabel == truelabel &&
					(*sgeval)->node[i].label != (*sgeval)->node[i].truelabel)
			{
				int j = random_integer(0, last_node, random_generator);

				// Swapping selected training node with the last one to eliminate it
				// from reassessment, since it will either be a prototype and will
				// not be replaced by the erroneously classified node anyway, or
				// it is a non-prototype which is available for selection (Knuth-shuffle)
				SwapSNode(&((*sgtrain)->node[j]), &((*sgtrain)->node[last_node]));

				if(((*sgtrain)->node[last_node].pred != NIL))
				{
					SwapSNode(&((*sgtrain)->node[last_node]), &((*sgeval)->node[i]));

					nnon_prototypes--;
					nerrors--;

					if(last_node == 0)
						break;	// no more nodes available in sgtrain for swapping,
								// so break the loop
				}

				last_node--; 	// at this point, the last node will be the current
								// node j selected at random from sgtrain and will
								// never be reassessed
			}
		}
	}

	//Move misclassified nodes from source graph (src) to destiny graph (dst)
	void move_misclassified_labeled_nodes(Subgraph **src, Subgraph **dst,  int *p)
	{
		int i, j, k, num_of_misclassified=0;
		const int truelabel = (*dst)->node[0].truelabel;
		Subgraph *newsrc = NULL, *newdst = NULL;

		for (i=0; i < (*src)->nnodes; i++)
		{
			if ((*src)->node[i].truelabel != (*src)->node[i].label)
				num_of_misclassified++;
		}
		*p = num_of_misclassified;

		if (num_of_misclassified>0)
		{
			newsrc = CreateSubgraph((*src)->nnodes-num_of_misclassified);
			newdst = CreateSubgraph((*dst)->nnodes+num_of_misclassified);

			newsrc->nfeats = (*src)->nfeats;
			newdst->nfeats = (*dst)->nfeats;
			newsrc->nlabels = (*src)->nlabels;
			newdst->nlabels = (*dst)->nlabels;

			for (i = 0; i < (*dst)->nnodes; i++)
				CopySNode(&(newdst->node[i]), &((*dst)->node[i]), newdst->nfeats);
			j=i;

			k = 0;
			for (i=0; i < (*src)->nnodes; i++)
			{
				if((*src)->node[i].truelabel == truelabel)
				{
					if ((*src)->node[i].truelabel == (*src)->node[i].label)// misclassified node
						CopySNode(&(newsrc->node[k++]), &((*src)->node[i]), newsrc->nfeats);
					else
						CopySNode(&(newdst->node[j++]), &((*src)->node[i]), newdst->nfeats);
				}
			}
			DestroySubgraph(&(*src));
			DestroySubgraph(&(*dst));
			*src = newsrc;
			*dst = newdst;
		}
	}

//Replace errors from 'ating set by least important labeled non prototypes from training set
//Note that all nodes in this training set have the same label
	void swap_errors_by_least_import_lb_non_prot(Subgraph **sgtrain, Subgraph **sgeval)
	{
		register int i, j, k;
		int nprototypes = 0, nerrors = 0;

		for(i = 0; i < (*sgtrain)->nnodes; i++)
			if((*sgtrain)->node[i].pred != NIL) nprototypes++;

		for(i = 0; i < (*sgeval)->nnodes; i++)
			if((*sgeval)->node[i].label != (*sgeval)->node[i].truelabel) nerrors++;


		k = (*sgtrain)->nnodes - 1; // works its way up from the node with the greatest
		// cost to the one with the least cost
		const int truelabel = (*sgtrain)->node[0].truelabel;

		for(i = 0; i < (*sgeval)->nnodes && nprototypes > 0 && nerrors > 0; i++)
		{
			// only nodes labeled incorrectly whose truelabel is the same of
			// the nodes in sgtrain will be replaced, therefore errors
			// from other labels will remain
			if((*sgeval)->node[i].truelabel == truelabel &&
					(*sgeval)->node[i].label != (*sgeval)->node[i].truelabel)
			{
				for(; k >= 0; k--)
				{
					j = (*sgtrain)->ordered_list_of_nodes[k];

					if(((*sgtrain)->node[j].status != NIL) && ((*sgtrain)->node[j].pred != NIL))
					{
						SwapSNode(&((*sgtrain)->node[j]), &((*sgeval)->node[i]));
						(*sgtrain)->node[j].status = NIL;
						nprototypes--;
						nerrors--;
						break; // if the current node fits the requirements, leaves the loop
					}
				}
			}
		}
	}

	/** Util **/
//
//	Image32 *remove_region_by_area(Image32 *label, int area)
//	{
//		Curve *label_area;
//		GQueue *Q;
//		int p, q, i, n, Lmax;
//		Pixel u, v;
//		AdjRel *A;
//		int   *root;
//		Image32 *rlabel;
//		char need_removal;
//
//		A      = circular(1.0);
//		rlabel = LabelComp(label, A, 0);
//		label_area = Histogram(rlabel);
//		n    = label->ncols * label->nrows;
//
//		for(p = 0; p < n; p++)
//			rlabel->val[p] = (int)label_area->Y[rlabel->val[p]];
//
//		destroy_curve(&label_area);
//
//		Lmax = maximum_value(rlabel);
//		root        = AllocIntArray(n);
//		Q = CreateGQueue(Lmax + 1, n, rlabel->val);
//		SetRemovalPolicy(Q, MAXVALUE);
//
//		need_removal = 0;
//
//		for(p = 0; p < n; p++)
//		{
//			if(rlabel->val[p] >= area)
//			{
//				root[p] = p;
//				InsertGQueue(&Q, p);
//			}
//			else
//			{
//				need_removal = 1;
//				root[p] = NIL;
//				rlabel->val[p] = 0;
//			}
//		}
//
//		if(need_removal == 0) // no need to eliminate regions
//		{
//			DestroyGQueue(&Q);
//			destroy_adjrel(&A);
//			free(root);
//			destroy_image(&rlabel);
//#ifdef USIS_VERBOSE
//			fprintf(stderr, "No need to eliminate regions\n");
//#endif
//			return(create_image32(*label));
//		}
//
//		while(!EmptyGQueue(Q))
//		{
//			p = RemoveGQueue(Q);
//			u.x = p % label->ncols;
//			u.y = p / label->ncols;
//
//			for(i = 1; i < A->n; i++)
//			{
//				v.x = u.x + A->dx[i];
//				v.y = u.y + A->dy[i];
//
//				if(label->ValidPixel(v))
//				{
//					q = label->Pixel2Index(v);
//
//					if(rlabel->val[q] == 0)
//					{
//						root[q]       = root[p];
//						rlabel->val[q] = rlabel->val[p];
//						InsertGQueue(&Q, q);
//					}
//				}
//			}
//		}
//
//		DestroyGQueue(&Q);
//		destroy_adjrel(&A);
//
//		// Copy label
//
//		for(p = 0; p < n; p++)
//			if(root[p] != p)
//			{
//				rlabel->val[p] = label->val[root[p]];
//			}
//			else
//			{
//				rlabel->val[p] = label->val[p];
//			}
//
//		free(root);
//
//		return(rlabel);
//	}


	Image32 *image_class_knn_graph(Subgraph *sg, Features *f)
	{
		int     i, p, k;
		Image32  *label = create_image32(f->ncols, f->nrows);
		float dist;

		for(p = 0; p < f->nelems; p++)
		{
			for(i = 0; i < sg->nnodes; i++)
			{
				k = sg->ordered_list_of_nodes[i];
				dist = opf_ArcWeight(sg->node[k].feat, f->elem[p].feat, sg->nfeats);

				if(dist <= sg->node[k].radius)
				{
					label->val[p] = sg->node[k].label + 1;

					break;
				}
			}
		}

		return(label);
	}

	void image_class_knn_graph(Subgraph *sg, Features *f, Image32 *clusters, Rectangle bb)
	{
		int     i, x, y, k;
		float dist;
		Rectangle bb1;

		if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0 )
			bb1 = bb;
		else
			bb1 = Rectangle(0, 0, clusters->ncols - 1, clusters->nrows - 1);

		for(y = bb1.beg.y; y <= bb1.end.y; y++)
		{
			for(x = bb1.beg.x; x <= bb1.end.x; x++)
			{
				int p = to_index(clusters, x, y);

				for(i = 0; i < sg->nnodes; i++)
				{
					k = sg->ordered_list_of_nodes[i];
					dist = opf_ArcWeight(sg->node[k].feat, f->elem[p].feat, sg->nfeats);

					if(dist <= sg->node[k].radius)
					{
						clusters->val[p] = sg->node[k].label + 1;
						break;
					}
				}
			}
		}
	}

	Image32 *image_opf_clustering(Features *f, Subgraph *sg, int kmin, int kmax, int area)
	{
		opf_ArcWeightFun tmpFun = opf_ArcWeight;
		opf_ArcWeight = opf_EuclDist;

#ifdef USIS_VERBOSE
		timer *tic = Tic();
#endif
		opf_BestkMinCut(sg, kmin, kmax);
		opf_OPFClustering(sg);

#ifdef USIS_VERBOSE
		timer *toc = Toc();

		USIS_VERBOSE_PRINTF("Clustering subgraph run time %fms\n", CTime(&tic, &toc));
#endif

#ifdef USIS_VERBOSE
		tic = Tic();
#endif
		Image32 *clusters = image_class_knn_graph(sg, f);

#ifdef USIS_VERBOSE
		toc = Toc();
		USIS_VERBOSE_PRINTF("Clustering image run time %fms\n", CTime(&tic, &toc));
#endif
		//returning the original arcweight function
		opf_ArcWeight = tmpFun;

		return clusters;
	}

	DbImage *propagate_pdf(Subgraph *sg, Features *f, Rectangle bb)
	{
		int     i, x, y, p, k;
		DbImage  *pdf = create_dbimage(f->ncols, f->nrows);
		float   dist;

		Rectangle bb1;
		if(bb.beg.x >= 0 && bb.beg.y >= 0 && bb.end.x >= 0 && bb.end.y >= 0)
			bb1 = bb;
		else
			bb1 = Rectangle(0, 0, f->ncols - 1, f->nrows - 1);

		for(y = bb1.beg.y; y <= bb1.end.y; y++)
			for(x = bb1.beg.x; x <= bb1.end.x; x++)
			{
				p = x + y * f->ncols;
				for(i = 0; i < sg->nnodes; i++)
				{
					k = sg->ordered_list_of_nodes[i];
					dist = opf_ArcWeight(sg->node[k].feat, f->elem[p].feat, sg->nfeats);

					if(dist <= sg->node[k].radius)
					{
						// normalizing by the inverted arc weight
                        pdf->val[p]=(double)(sg->node[k].pathval*exp(-dist/sg->K));
						break;
					}
				}
			}

		return(pdf);
	}

	Set *detect_outliers_in_set(Subgraph *sg, Set *S, Features *f)
	{
		int     i, k;
		bool is_outlier;
		float   dist;
		Set    *aux = S, *Outliers = NULL;

		while(aux != NULL)
		{
			is_outlier = true;

			for(i = 0; i < sg->nnodes; i++)
			{
				k = sg->ordered_list_of_nodes[i];
				dist = opf_ArcWeight(sg->node[k].feat, f->elem[aux->elem].feat, f->nfeats);

				if(dist <= sg->node[k].radius)
				{
					is_outlier = false;
					break;
				}
			}

			if(is_outlier)
			{
				InsertSet(&Outliers, aux->elem);
			}

			aux = aux->next;
		}

		return Outliers;
	}

	// Returns true if at least one foreground or background marker has a percentage
	// of its pixels selected on pixels which were misclassified (i.e. if the membership value
	// is less than USIS_MAX_OBJMAP_VALUE/2 for an object seed or greater than USIS_MAX_OBJMAP_VALUE/2
	// for a background seed). Parameters objClass and bkgClass return which class needs
	// to have its pixels re-classified
	void assess_relearning(Image8 *objMap, Set **Si, Set **Se, int lenSi, int lenSe, float mksize_threshold,
												 int n_int_seeds, int n_ext_seeds, float nseeds_threshold, bool *objClass, bool *bkgClass)
	{
		int i;

		const int class_thresh = USIS_MAX_OBJMAP_VALUE / 2;

		*objClass = *bkgClass = false;

		for(i = 0; i < lenSi; i++)
		{
			int counter = 0; // counts the amount of misclassfied pixels
			int mksize = 0;  // counts the amount of pixels in the marker
			Set *aux = Si[i];

			for(mksize = 0; aux != NULL; aux = aux->next)
			{
				mksize++;

				if(objMap->val[aux->elem] < class_thresh) counter++;

			}

			if((float)counter / (float)mksize >= mksize_threshold && (float)counter / (float) n_ext_seeds >= nseeds_threshold)
			{
				*bkgClass = true;//if an object marker was selected on pixels classified as background
				//then the object membership map should be recomputed for background pixels
				break;
			}
		}

		for(i = 0; i < lenSe; i++)
		{
			int counter = 0; //counts the amount of misclassfied pixels
			int mksize = 0;
			Set *aux = Se[i];

			for(mksize = 0; aux != NULL; aux = aux->next)
			{
				mksize++;

				if(objMap->val[aux->elem] > class_thresh) counter++;
			}

			if((float)counter / (float)mksize >= mksize_threshold && (float)counter / (float) n_int_seeds >= nseeds_threshold)
			{
				*objClass = true;//if a background marker was selected on pixels classified as object
				//then the object membership map should be recomputed for object pixels
				break;
			}
		}
	}

	/** SSE **/
	inline __m128 arc_weight_ps(__m128 A)
	{
		return _mm_mul_ps(
						 log_ps(_mm_add_ps(A, _mm_set_ps(1.0, 1.0, 1.0, 1.0))),
						 _mm_set_ps(opf_MAXARCW, opf_MAXARCW, opf_MAXARCW, opf_MAXARCW));
	}

	struct TFeats
	{
		Features *f;
		Features *out;
		float radius;
		int beg;
		int end;
		int Imax;
	};

	void algn_sg_feats_npval(Subgraph *sg, float *** features, float **pathval)
	{
		const int nnodes = sg->nnodes;
		const int n = nnodes + 4 - nnodes % 4;
		const int nfeats = sg->nfeats;

		*features = (float**)_mm_malloc(nfeats * sizeof(float*), USIS_SSE_ALIGNMENT);
		*pathval = usis_alloc_algn_float_array(n, USIS_SSE_ALIGNMENT);

		int i, j;

		for(i = 0; i < nfeats; i++)
			(*features)[i] = usis_alloc_algn_float_array(n, 16);

		for(j = nnodes; j--;)
		{
			(*pathval)[j] = sg->node[j].pathval;

			for(i = nfeats; i--;)
				(*features)[i][j] = sg->node[j].feat[i];
		}

		for(j = nnodes; j < n; j++)
		{
			(*pathval)[j] = FLT_MAX;

			for(i = nfeats; i--;)
				(*features)[i][j] = 0.0;
		}
	}


	void *tfopf_pathval_map_sse(void *data)
	{
		TOPFData *opfdata = (TOPFData*)data;

		Subgraph *sg = opfdata->sg;
		Features *feat = opfdata->f;
		DbImage *pvalmap = opfdata->pvalmap;
		const int beg = opfdata->beg;
		const int end = opfdata->end;

		const int nnodes = sg->nnodes;// total number of nodes + extra paddingconst int nloop = n/4;
		const int n = nnodes + 4 - nnodes % 4;
		const int nloop = n / 4;
		const int nfeats = feat->nfeats;

		register int p, i, j;
		register int counter;

		float **features = opfdata->features;
		float *pathval = opfdata->pathval;

		float *feat_weight = NULL;

		if(opf_FeatWeight == NULL)
		{
			feat_weight = AllocFloatArray(nfeats);
			for(i = 0; i < nfeats; i++)
				feat_weight[i] = 1.0;
		}
		else
		{
			feat_weight = opf_FeatWeight;
		}




		// used to store the feature vector of a given pixel p
		// using the format F0,F0,F0,F0,F1,F1,F1,F1,...,Fn,Fn,Fn,Fn
		float f[4*nfeats] __attribute__((aligned(16)));


		for(counter = end - beg, p = beg; counter--; p++)
		{
			for(i = 4 * nfeats; i--;)
				f[i] = feat->elem[p].feat[i/4];

			__m128 minCost = _mm_set_ps(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);

			__m128 *pval = (__m128*)pathval;

			for(j = 0; j < nloop; j++)
			{
				__m128 acc = _mm_setzero_ps();
				__m128 *p1 = (__m128*)f;
				__m128 p2;
				const int index = j * 4;

				for(i = 0; i < nfeats; i++)
				{
					__m128 w = _mm_set_ps(feat_weight[i], feat_weight[i],
											feat_weight[i], feat_weight[i]);

					p2 = _mm_load_ps(features[i] + index);

					__m128 sub = _mm_sub_ps(p2, *p1);

					acc = _mm_add_ps(acc, _mm_mul_ps(_mm_mul_ps(sub, sub), w));
					p1++;
				}

				minCost = _mm_min_ps(_mm_max_ps(arc_weight_ps(acc), *pval), minCost);

				pval++;
			}

			pvalmap->val[p] = (double)usis_min_ps(minCost);
		}

		if(feat_weight != opf_FeatWeight)
			free(feat_weight);

		pthread_exit((void*)opfdata);
	}

	void *tfopf_pathval_map_by_regions_sse(void *data)
	{
		TOPFData *opfdata = (TOPFData*)data;

		Subgraph *sg = opfdata->sg;
		Features *feat = opfdata->f;
		DbImage *pvalmap = opfdata->pvalmap;
		Image8 *regions = opfdata->regions;
		const int beg = opfdata->beg;
		const int end = opfdata->end;

		const int nnodes = sg->nnodes;// total number of nodes + extra paddingconst int nloop = n/4;
		const int n = nnodes + 4 - nnodes % 4;
		const int nloop = n / 4;
		const int nfeats = feat->nfeats;

		float **features = opfdata->features;
		float *pathval = opfdata->pathval;

		int *sel_regions = opfdata->sel_regions;

		// used to store the feature vector of a given pixel p
		// using the format F0,F0,F0,F0,F1,F1,F1,F1,...,Fn,Fn,Fn,Fn
		float f[4*nfeats] __attribute__((aligned(16)));

		register int p, i, j;
		register int counter;

		for(counter = end - beg, p = beg; counter--; p++)
		{
			if(sel_regions[regions->val[p]])
			{
				for(i = 4 * nfeats; i--;)
					f[i] = feat->elem[p].feat[i/4];

				__m128 minCost = _mm_set_ps(FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX);

				__m128 *pval = (__m128*)pathval;

				for(j = 0; j < nloop; j++)
				{
					__m128 acc = _mm_setzero_ps();
					__m128 *p1 = (__m128*)f;
					__m128 p2;
					const int index = j * 4;

					for(i = 0; i < nfeats; i++)
					{
						p2 = _mm_load_ps(features[i] + index);

						__m128 sub = _mm_sub_ps(p2, *p1);

						acc = _mm_add_ps(acc, _mm_mul_ps(sub, sub));
						p1++;
					}

					minCost = _mm_min_ps(_mm_max_ps(arc_weight_ps(acc), *pval), minCost);

					pval++;
				}

				pvalmap->val[p] = (double)usis_min_ps(minCost);
			}
		}

		pthread_exit((void*)opfdata);
	}

	void tfopf_membership_map_by_regions_sse(Subgraph *sgtrainobj, Subgraph *sgtrainbkg,
			Features *f, Image8 *objMap, Image8 *regions, int *sel_regions, int nthreads)
	{
		register int i, j, chunksize, tmp;
		const int n = f->nelems, nlabels = 2;

		pthread_t  thread_id[nthreads][nlabels];
		pthread_attr_t attr;

		DbImage *obj = create_dbimage(f->ncols, f->nrows);
		DbImage *bkg = create_dbimage(f->ncols, f->nrows);

		chunksize = (int)ceilf((float)n / nthreads);

		TOPFData *opfdata[nthreads][nlabels];

		float **featuresObj = NULL;
		float **featuresBkg = NULL;
		float *pathvalObj = NULL;
		float *pathvalBkg = NULL;

		algn_sg_feats_npval(sgtrainobj, &featuresObj, &pathvalObj);
		algn_sg_feats_npval(sgtrainbkg, &featuresBkg, &pathvalBkg);

		USIS_VERBOSE_PRINTF("Using %d threads to compute the object membership map\n", nthreads);

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

		USIS_VERBOSE_PRINTF("Computing object and background membership map... Please wait... \n");

		for(i = 0; i < nthreads; i++)
		{
			//common info
			for(j = 0; j < nlabels; j++)
			{
				opfdata[i][j] = (TOPFData*)calloc(1, sizeof(TOPFData));
				opfdata[i][j]->f = f;
				opfdata[i][j]->beg = i * chunksize;
				tmp = (i + 1) * chunksize;
				opfdata[i][j]->end = (tmp <= n) ? tmp : n;
			}

			//background threads info
			opfdata[i][0]->sg = sgtrainbkg;
			opfdata[i][0]->pvalmap = bkg;
			opfdata[i][0]->regions = regions;
			opfdata[i][0]->sel_regions = sel_regions;
			opfdata[i][0]->label = USIS_OPFBKGLABEL;
			opfdata[i][0]->pathval = pathvalBkg;
			opfdata[i][0]->features = featuresBkg;

			//object threads info
			opfdata[i][1]->sg = sgtrainobj;
			opfdata[i][1]->label = USIS_OPFOBJLABEL;
			opfdata[i][1]->pvalmap = obj;
			opfdata[i][1]->regions = regions;
			opfdata[i][1]->sel_regions = sel_regions;
			opfdata[i][1]->pathval = pathvalObj;
			opfdata[i][1]->features = featuresObj;

			// running threads
			pthread_create(&(thread_id[i][0]), &attr, tfopf_pathval_map_by_regions_sse, (void*)opfdata[i][0]);
			pthread_create(&(thread_id[i][1]), &attr, tfopf_pathval_map_by_regions_sse, (void*)opfdata[i][1]);
		}

		for(i = 0; i < nthreads; i++)
		{
			for(j = 0; j < nlabels; j++)
			{
				void *status;
				int rc = pthread_join(thread_id[i][j], &status);

				if(rc)
				{
					fprintf(stderr, "ERROR; return code from pthread_join() is %d\n", rc);
				}
			}
		}

		membership_map_by_regions(objMap, regions, sel_regions, obj, bkg, sgtrainobj->df);

		USIS_VERBOSE_PRINTF("Object membership map computed... \n");

		pthread_attr_destroy(&attr);

		for(i = 0; i < nthreads; i++)
			for(j = 0; j < nlabels; j++)
				free(opfdata[i][j]);

		for(i = 0; i < sgtrainobj->nfeats; i++)
		{
			_mm_free(featuresObj[i]);
			_mm_free(featuresBkg[i]);
		}


		_mm_free(featuresObj);
		_mm_free(featuresBkg);
		_mm_free(pathvalObj);
		_mm_free(pathvalBkg);

		destroy_dbimage(&obj);
		destroy_dbimage(&bkg);

	}


	Image8 *tfopf_membership_map_sse(Subgraph *sgtrainobj, Subgraph *sgtrainbkg,
									Features *f, int p_nthreads)
	{
		register int i, j, chunksize, tmp;
		const int n = f->nelems, nlabels = 2;
		const int nthreads = MAX(p_nthreads / nlabels, 1);
		Image8 *objMap = NULL;
		pthread_t  thread_id[nthreads][nlabels];
		pthread_attr_t attr;

		DbImage *obj = create_dbimage(f->ncols, f->nrows);
		DbImage *bkg = create_dbimage(f->ncols, f->nrows);

		chunksize = (int)ceilf((float)n / nthreads);

		TOPFData *opfdata[nthreads][nlabels];

		float **featuresObj = NULL;
		float **featuresBkg = NULL;
		float *pathvalObj = NULL;
		float *pathvalBkg = NULL;

		algn_sg_feats_npval(sgtrainobj, &featuresObj, &pathvalObj);
		algn_sg_feats_npval(sgtrainbkg, &featuresBkg, &pathvalBkg);


		USIS_VERBOSE_PRINTF("Using %d threads to compute the object membership map\n", p_nthreads);

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

		USIS_VERBOSE_PRINTF("Computing object and background membership map... Please wait... \n");

		for(i = 0; i < nthreads; i++)
		{
			//common info
			for(j = 0; j < nlabels; j++)
			{
				opfdata[i][j] = (TOPFData*)calloc(1, sizeof(TOPFData));
				opfdata[i][j]->f = f;
				opfdata[i][j]->beg = i * chunksize;
				tmp = (i + 1) * chunksize;
				opfdata[i][j]->end = (tmp <= n) ? tmp : n;
			}

			//background threads info
			opfdata[i][0]->sg = sgtrainbkg;
			opfdata[i][0]->pvalmap = bkg;
			opfdata[i][0]->label = USIS_OPFBKGLABEL;
			opfdata[i][0]->pathval = pathvalBkg;
			opfdata[i][0]->features = featuresBkg;

			//object threads info
			opfdata[i][1]->sg = sgtrainobj;
			opfdata[i][1]->label = USIS_OPFOBJLABEL;
			opfdata[i][1]->pvalmap = obj;
			opfdata[i][1]->pathval = pathvalObj;
			opfdata[i][1]->features = featuresObj;

			// running threads
			pthread_create(&(thread_id[i][0]), &attr, tfopf_pathval_map_sse, (void*)opfdata[i][0]);
			pthread_create(&(thread_id[i][1]), &attr, tfopf_pathval_map_sse, (void*)opfdata[i][1]);
		}

		for(i = 0; i < nthreads; i++)
		{
			for(j = 0; j < nlabels; j++)
			{
				void *status;
				int rc = pthread_join(thread_id[i][j], &status);

				if(rc)
				{
					fprintf(stderr, "ERROR; return code from pthread_join() is %d\n", rc);
				}
			}
		}

		objMap = membership_map(obj, bkg, sgtrainobj->df);

		USIS_VERBOSE_PRINTF("Object membership map computed... \n");

		pthread_attr_destroy(&attr);

		for(i = 0; i < nthreads; i++)
			for(j = 0; j < nlabels; j++)
				free(opfdata[i][j]);

		for(i = 0; i < sgtrainobj->nfeats; i++)
		{
			_mm_free(featuresObj[i]);
			_mm_free(featuresBkg[i]);
		}


		_mm_free(featuresObj);
		_mm_free(featuresBkg);
		_mm_free(pathvalObj);
		_mm_free(pathvalBkg);

		destroy_dbimage(&obj);
		destroy_dbimage(&bkg);

		return objMap;
	}

}


