#include "usisfeatures.h"
#include "adjacency.h"
#include <omp.h>
#include <pthread.h>

Features* CreateFeatures(int ncols, int nrows, int nfeats)
{
    int i;

    Features *f=(Features *)calloc(1,sizeof(Features));

    f->ncols  = ncols;
    f->nrows  = nrows;
    f->nelems = ncols*nrows;
    f->elem   = (FElem *)calloc(f->nelems,sizeof(FElem));
    f->nfeats = nfeats;
    f->Fmax = 0.0;
    f->Fmin = 0.0;

	f->data = usis::alloc_float_array(nfeats*f->nelems);

	/** Setting pointers to f->data **/
    for (i=0; i < f->nelems; i++)
        f->elem[i].feat = f->data + i*f->nfeats;

    return f;
}

void DestroyFeatures(Features **f)
{
    if ((*f)!=NULL)
    {
		usis::free_array_data((*f)->data);

        free((*f)->elem);
        free(*f);
        (*f)=NULL;
    }
}

Features* CopyFeatures(Features* feat)
{
    int size;
	Features* result;
    if(feat == NULL)
    {
        Error("Features* feat NULL ","CopyFeatures");
    }

    result = CreateFeatures(feat->ncols, feat->nrows, feat->nfeats);

    result->Fmax = feat->Fmax;

    size = feat->nfeats*sizeof(float);

	memcpy(result->data, feat->data, feat->nelems*size);

    return result;
}

usis::DbImage* GetFeature(Features* feats, int index)
{
    int i;
    usis::DbImage* imgfeats;
    double value;

    if (feats == NULL) return NULL;
    if (index >= feats->nfeats) return NULL;

    imgfeats = usis::create_dbimage(feats->ncols, feats->nrows);

    for (i = 0; i < feats->nelems; i++)
    {
        if (feats->elem == NULL || feats->elem[i].feat == NULL)
        {
            usis::destroy_dbimage(&imgfeats);
            perror("Error! Pixels or feature with NULL values in GetFeature!");
            return NULL;
        }

        value = (double)feats->elem[i].feat[index];
        imgfeats->val[i] = value;
    }

    return imgfeats;
}


int SetFeature(Features* feats, int index, usis::DbImage* imagefeats)
{

    int i;

    if (feats == NULL || imagefeats == NULL) return 0;;
    if (index >= feats->nfeats) return 0;

    for (i = 0; i < feats->nelems; i++)
    {
        feats->elem[i].feat[index] = (float)imagefeats->val[i];
    }
    return 1;
}



/*normalize features*/
void MeanStdNormalizeFeatures(Features *f)
{
    float *mean = (float *)calloc(f->nfeats,sizeof(float)), *std = (float *)calloc(f->nfeats, sizeof(int));
    int i,j;

    for (i = 0; i < f->nfeats; i++)
    {
        for (j = 0; j < f->nelems; j++)
            mean[i]+=f->elem[j].feat[i]/f->nelems;
        for (j = 0; j < f->nelems; j++)
            std[i]+=pow(f->elem[j].feat[i]-mean[i],2)/f->nelems;
        std[i]=sqrt(std[i]);
    }

    for (i = 0; i < f->nfeats; i++)
    {
        for (j = 0; j < f->nelems; j++)
        {
            float v =(f->elem[j].feat[i]-mean[i]);
            /// Preventing division by zero
			f->elem[j].feat[i] = v/(std[i]+FLT_EPSILON);
        }
    }

    free(mean);
    free(std);
}

void SetSubgraphFeatures(Subgraph *sg, Features *f)
{
    int i,j;

    sg->nfeats = f->nfeats;

    for (i=0; i < sg->nnodes; i++)
    {
        sg->node[i].feat = AllocFloatArray(sg->nfeats);

        for (j=0; j < sg->nfeats; j++)
        {
            sg->node[i].feat[j] = f->elem[sg->node[i].position].feat[j];
        }
    }
}


namespace usis
{
    struct TFeats
    {
        Features *f;
        Features *out;
        CImage8 *cimg;
        float radius;
        int beg;
        int end;
        int Fmax;
        int nscales;
    };


	float max_arcweight(Features *f)
	{
		float  dist;
		float *fv1, *fv2;
		int p, i;

		fv1 = AllocFloatArray(f->nfeats);
		fv2 = AllocFloatArray(f->nfeats);

		for(i = 0; i < f->nfeats; i++)
		{
			fv1[i] = FLT_MAX;
			fv2[i] = -USIS_FINFTY;
		}

		for(p = 0; p < f->nelems; p++)
		{
			for(i = 0; i < f->nfeats; i++)
			{
				fv1[i] = MIN(fv1[i], f->elem[p].feat[i]);
				fv2[i] = MAX(fv2[i], f->elem[p].feat[i]);
			}
		}

		dist = opf_ArcWeight(fv1, fv2, f->nfeats);

		free(fv1);
		free(fv2);

		return dist;
	}


	void *ms_gauss_cimage8_feats_threaded_aux(void *data)
	{
		int       i, j, p, q, s;
		Pixel     u, v;
		float    *w=NULL, d, K, sigma2, val;
		AdjRel   *A = NULL;

		CImage8 *cimg = ((TFeats*)data)->cimg;
		Features *f = ((TFeats*)data)->out;
		const int beg = ((TFeats*)data)->beg;
		const int end = ((TFeats*)data)->end;
		const int nscales = ((TFeats*)data)->nscales;

		for(j=0; j < 3; j=j+1)
		{
			for(s=1; s <= nscales; s=s+1)
			{
				A  = circular(s);
				w  = AllocFloatArray(A->n);
				sigma2 = (s/3.0)*(s/3.0);
				K      =  2.0*sigma2;

				for(i = 0; i < A->n; i++)
				{
					d    = A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i];
					w[i] = 1.0 / sqrt(2.0 * M_PI * sigma2) * exp(-d / K); // Gaussian
				}

				for(p = beg; p < end; p++)
				{
					u.x = p % f->ncols;
					u.y = p / f->ncols;
					val = 0.0;

					for(i = 0; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];

						if(valid_pixel(cimg,v))
						{
							q   = to_index(cimg,v);
							val += (float)cimg->C[j]->val[q] * w[i];
						}
					}

					f->elem[p].feat[j *nscales+s-1] = val;
				}

			}

			free(w);

			destroy_adjrel(&A);
		}

		pthread_exit((void*)data);
	}


	Features *ms_gauss_cimage8_feats_threaded(CImage8 *cimg, int nscales, int nthreads)
	{
		register int i, chunksize, tmp;
		const int n = cimg->ncols*cimg->nrows;

		Features *f = CreateFeatures(cimg->ncols, cimg->nrows, 3*nscales);

		// The summation of the gaussian kernel will be 1.0,
		// therefore the maximum filtered value is the maximum value of the image.
		f->Fmax = MAX(MAX(maximum_value(cimg->C[0]), maximum_value(cimg->C[1])), maximum_value(cimg->C[2]));

		pthread_t  thread_id[nthreads];
		pthread_attr_t attr;

		chunksize = (int)ceilf((float)n / nthreads);

		TFeats *data[nthreads];

        USIS_VERBOSE_PRINTF("Using %d threads to compute multiscale gaussian features\n", nthreads);

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

        USIS_VERBOSE_PRINTF("Computing multiscale gaussian features using thread... Please wait... \n");

		for(i = 0; i < nthreads; i++)
		{
			data[i] = new TFeats();

			//common info
			data[i]->cimg = cimg;
			data[i]->out = f;
			data[i]->nscales = nscales;
			data[i]->beg = i * chunksize;
			tmp = (i + 1) * chunksize;
			data[i]->end = (tmp <= n) ? tmp : n;

			// start running threads
			pthread_create(&(thread_id[i]), &attr, ms_gauss_cimage8_feats_threaded_aux, (void*)data[i]);
		}

		for(i = 0; i < nthreads; i++)
		{
			void *status;
			int rc = pthread_join(thread_id[i], &status);

			if(rc)
			{
				fprintf(stderr, "ERROR; return code from pthread_join() is %d\n", rc);
			}
		}

        USIS_VERBOSE_PRINTF("Multiscale Gaussian features computed... \n");

		pthread_attr_destroy(&attr);

		for(i = 0; i < nthreads; i++)
		{
			delete data[i];
		}

		return f;
	}


	Features *ms_gauss_image8_feats(Image8 *img, int nscales)
	{
		int       i, p, q, s;
		Pixel     u, v;
		float    *w=NULL, d, K, sigma2, val;
		AdjRel   *A = NULL;
		const int n = img->ncols*img->nrows;

		Features *f = CreateFeatures(img->ncols, img->nrows, nscales);

		f->Fmax = maximum_value(img);

        for(s=1; s <= nscales; s=s+1)
        {
            A  = circular(s);
            w  = AllocFloatArray(A->n);
            sigma2 = (s/3.0)*(s/3.0);
            K      =  2.0*sigma2;

            for(i = 0; i < A->n; i++)
            {
                d    = A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i];
                w[i] = 1.0 / sqrt(2.0 * M_PI * sigma2) * exp(-d / K); // Gaussian
            }

            for(p = 0; p < n ; p++)
            {
                u.x = p % f->ncols;
                u.y = p / f->ncols;
                val = 0.0;

                for(i = 0; i < A->n; i++)
                {
                    v.x = u.x + A->dx[i];
                    v.y = u.y + A->dy[i];

                    if(valid_pixel(img,v))
                    {
                        q   = to_index(img,v);
                        val += (float)img->val[q] * w[i];
                    }
                }

                f->elem[p].feat[s-1] = val;
            }


			free(w);

			destroy_adjrel(&A);
		}

		return f;
	}

	Features *ms_gauss_cimage8_feats(CImage8 *cimg, int nscales)
	{
		int       i, j, p, q, s;
		Pixel     u, v;
		float    *w=NULL, d, K, sigma2, val;
		AdjRel   *A = NULL;
		const int n = cimg->ncols*cimg->nrows;

		Features *f = CreateFeatures(cimg->ncols, cimg->nrows, 3*nscales);

		f->Fmax = MAX(MAX(maximum_value(cimg->C[0]), maximum_value(cimg->C[1])), maximum_value(cimg->C[2]));

		for(j=0; j < 3; j=j+1)
		{
			for(s=1; s <= nscales; s=s+1)
			{
				A  = circular(s);
				w  = AllocFloatArray(A->n);
				sigma2 = (s/3.0)*(s/3.0);
				K      =  2.0*sigma2;

				for(i = 0; i < A->n; i++)
				{
					d    = A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i];
					w[i] = 1.0 / sqrt(2.0 * M_PI * sigma2) * exp(-d / K); // Gaussian
				}

				for(p = 0; p < n ; p++)
				{
					u.x = p % f->ncols;
					u.y = p / f->ncols;
					val = 0.0;

					for(i = 0; i < A->n; i++)
					{
						v.x = u.x + A->dx[i];
						v.y = u.y + A->dy[i];

						if(valid_pixel(cimg,v))
						{
							q   = to_index(cimg,v);
							val += (float)cimg->C[j]->val[q] * w[i];
						}
					}

					f->elem[p].feat[j *nscales+s-1] = val;
					f->Fmax = MAX(f->Fmax, val);
				}

			}

			free(w);

			destroy_adjrel(&A);
		}

		return f;
	}

    Features* lab_feats(Features* rgb)
    {
        Features *f = CreateFeatures(rgb->ncols, rgb->nrows, rgb->nfeats);

        lab_feats(f, rgb);

        return f;
    }

    void lab_feats(Features* lab, Features *rgb)
    {
        int z,i;

        float Fmax = -USIS_FINFTY;
        float Fmin = USIS_FINFTY;
        const int nscales = rgb->nfeats/3;

        /** Generalization of Lab conversion to multiple scales:
            Our assumption is that the feature vectors
            in *rgb are assigned as follows:
            R_1,R_2,...,R_nscales-1,G_1,G_2,...,G_nscales-1,B_1,B_2,...,B_nscales-1
         **/
        for(i = 0; i < nscales; i++)
        {
            #pragma omp parallel for shared(Fmax, Fmin)
            for(z = 0; z < rgb->nelems; z++)
            {
                /******* Original RGB *******/
                float R = rgb->elem[z].feat[i];
                float G = rgb->elem[z].feat[nscales+i];
                float B = rgb->elem[z].feat[2*nscales+i];

                RGB2Lab(&R,&G,&B);

                lab->elem[z].feat[i] = R;
                lab->elem[z].feat[nscales+i] = G;
                lab->elem[z].feat[2*nscales+i] = B;

				#pragma omp critical
				{
					Fmax = MAX(Fmax,MAX(R,MAX(G,B)));
					Fmin = MIN(Fmin,MIN(R,MIN(G,B)));
				}
            }
        }

        lab->Fmax = Fmax;
        lab->Fmin = Fmin;
    }

    Features* lab_feats(CImage8* rgb)
    {
        int z;

        float Fmax = -USIS_FINFTY;
        float Fmin = USIS_FINFTY;

        Features *f = CreateFeatures(rgb->ncols, rgb->nrows, 3);

        /** Generalization of Lab conversion to multiple scales:
            Our assumption is that the feature vectors
            in *rgb are assigned as follows:
            R_1,R_2,...,R_nscales-1,G_1,G_2,...,G_nscales-1,B_1,B_2,...,B_nscales-1
         **/
        #pragma omp parallel for shared(Fmax, Fmin)
        for(z = 0; z < rgb->ncols*rgb->nrows; z++)
        {
            /******* Original RGB *******/
            float R = (float)rgb->C[0]->val[z];
            float G = (float)rgb->C[1]->val[z];
            float B = (float)rgb->C[2]->val[z];

            RGB2Lab(&R,&G,&B);

            f->elem[z].feat[0] = R;
            f->elem[z].feat[1] = G;
            f->elem[z].feat[2] = B;

			#pragma omp critical
			{
				Fmax = MAX(Fmax,MAX(R,MAX(G,B)));
				Fmin = MIN(Fmin,MIN(R,MIN(G,B)));
			}
        }

		f->Fmax = Fmax;
		f->Fmin = Fmin;

        return f;
    }

	void scale_lab_feats(Features *lab)
	{
		float Fmax = -USIS_FINFTY, Fmin = USIS_FINFTY;
		const int nscales = lab->nfeats/3;

        /** Generalization of Lab conversion to multiple scales:
            Our assumption is that the feature vectors
            in *rgb are assigned as follows:
            L_1,L_2,...,L_nscales-1,a_1,a_2,...,a_nscales-1,b_1,b_2,...,b_nscales-1
         **/
        for(int i = 0; i < nscales; i++)
        {
            #pragma omp parallel for shared(Fmax, Fmin)
            for(int z = 0; z < lab->nelems; z++)
            {
                /******* Original RGB *******/
                float l = lab->elem[z].feat[i];
                float a = lab->elem[z].feat[nscales+i];
                float b = lab->elem[z].feat[2*nscales+i];

				ScaleLab(&l, &a, &b);

                lab->elem[z].feat[i] = l;
                lab->elem[z].feat[nscales+i] = a;
                lab->elem[z].feat[2*nscales+i] = b;

				#pragma omp critical
				{
					Fmax = MAX(Fmax,MAX(l,MAX(a,b)));
					Fmin = MIN(Fmin,MIN(l,MIN(a,b)));
				}
            }
        }

        lab->Fmax = Fmax;
        lab->Fmin = Fmin;

	}

    Features* ycbcr_feats(CImage8* rgb)
    {
		Features *f = CreateFeatures(rgb->ncols, rgb->nrows, 3);

		ycbcr_feats(rgb, f);

        return f;
    }

	void ycbcr_feats(CImage8* rgb, Features *f)
	{
        int z;

        float Fmax = -USIS_FINFTY;
        float Fmin = USIS_FINFTY;


        /** Generalization of Lab conversion to multiple scales:
            Our assumption is that the feature vectors
            in *rgb are assigned as follows:
            R_1,R_2,...,R_nscales-1,G_1,G_2,...,G_nscales-1,B_1,B_2,...,B_nscales-1
         **/
        #pragma omp parallel for shared(Fmax, Fmin)
        for(z = 0; z < rgb->ncols*rgb->nrows; z++)
        {
        	int ycbcr, R, G, B;
            /******* Original RGB *******/
            R = rgb->C[0]->val[z];
            G = rgb->C[1]->val[z];
            B = rgb->C[2]->val[z];

            ycbcr = RGB2YCbCr(triplet(R,G,B));

            f->elem[z].feat[0] = t0(ycbcr);
            f->elem[z].feat[1] = t1(ycbcr);
            f->elem[z].feat[2] = t2(ycbcr);
			#pragma omp critical
			{
				Fmax = MAX(Fmax,MAX(t0(ycbcr),MAX(t1(ycbcr),t2(ycbcr))));
				Fmin = MIN(Fmin,MIN(t0(ycbcr),MIN(t1(ycbcr),t2(ycbcr))));
			}
        }
        f->Fmax = Fmax;
        f->Fmin = Fmin;
	}

    Features* ycbcr_feats(Features* rgb)
    {
		Features *f = CreateFeatures(rgb->ncols, rgb->nrows, 3);

		ycbcr_feats(rgb, f);

        return f;
    }


	void ycbcr_feats(Features* rgb, Features *f)
	{
        int z;

        float Fmax = INT_MIN;
		float Fmin = USIS_FINFTY;

        /** Generalization of Lab conversion to multiple scales:
            Our assumption is that the feature vectors
            in *rgb are assigned as follows:
            R_1,R_2,...,R_nscales-1,G_1,G_2,...,G_nscales-1,B_1,B_2,...,B_nscales-1
         **/
        #pragma omp parallel for shared(Fmax, Fmin)
        for(z = 0; z < rgb->ncols*rgb->nrows; z++)
        {
        	int ycbcr, R, G, B;
            /******* Original RGB *******/
            R = rgb->elem[z].feat[0];
            G = rgb->elem[z].feat[1];
            B = rgb->elem[z].feat[2];

            ycbcr = RGB2YCbCr(triplet(R,G,B));

            f->elem[z].feat[0] = t0(ycbcr);
            f->elem[z].feat[1] = t1(ycbcr);
            f->elem[z].feat[2] = t2(ycbcr);

			#pragma omp critical
			{
				Fmax = MAX(Fmax,MAX(t0(ycbcr),MAX(t1(ycbcr),t2(ycbcr))));
				Fmin = MIN(Fmin,MIN(t0(ycbcr),MIN(t1(ycbcr),t2(ycbcr))));
			}
        }
        f->Fmax = Fmax;
        f->Fmin = Fmin;
	}

	Features *bilateral_filter_feats(CImage8 *rgb, int   s_sigma, float r_sigma)
	{
		int nchannels = 3;
		Features *feats = CreateFeatures(rgb->ncols, rgb->nrows, nchannels);

		float * __restrict__ input;
		float * __restrict__ output;

		input = alloc_float_array(rgb->ncols * rgb->nrows * nchannels, 32);
		output = alloc_float_array(rgb->ncols * rgb->nrows * nchannels, 32);

		for(int y = 0; y < rgb->nrows; y++)
			for(int x = 0; x < rgb->ncols; x++)
				for(int c = 0; c < nchannels; c++)
					input[c+nchannels*(y*rgb->ncols+x)] = (float)rgb->C[c]->val[to_index(rgb, x,y)]/(float)UCHAR_MAX;

		float max_val = -1.0;
		for(int p = 0; p < rgb->n*nchannels; p++)
			max_val = MAX(max_val, input[p]);

		assert(max_val != 0.0);

		bilateral_filter(input, output, rgb->ncols, rgb->nrows, nchannels, s_sigma, r_sigma);

		max_val = -1.0;
		for(int p = 0; p < rgb->n*nchannels; p++)
			max_val = MAX(max_val, input[p]);

		assert(max_val != 0.0);

		max_val = -1.0;
		for(int p = 0; p < rgb->n*nchannels; p++)
			max_val = MAX(max_val, output[p]);

		assert(max_val != 0.0);

		for(int y = 0; y < rgb->nrows; y++)
			for(int x = 0; x < rgb->ncols; x++)
				for(int c = 0; c < nchannels; c++)
				{
					feats->elem[to_index(rgb, x,y)].feat[c] = output[c+nchannels*(y*rgb->ncols+x)]*UCHAR_MAX;
					feats->Fmax = MAX(feats->Fmax, feats->elem[to_index(rgb, x,y)].feat[c]);
					feats->Fmin = MIN(feats->Fmin, feats->elem[to_index(rgb, x,y)].feat[c]);
				}

		_mm_free(input);
		_mm_free(output);

		return feats;
	}

}
