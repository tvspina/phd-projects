#include "adjacency.h"
#include "usisgeometry.h"
#include <tr1/unordered_map>
#include <tr1/unordered_set>

AdjRel *create_adjrel(int n)
{
	AdjRel *A = NULL;

	A = (AdjRel *) calloc(1, sizeof(AdjRel));
	if(A != NULL)
	{
		A->dx = AllocIntArray(n);
		A->dy = AllocIntArray(n);
		A->n  = n;
	}
	else
	{
		Error(MSG1, "create_adjrel");
	}

	return(A);
}

void destroy_adjrel(AdjRel **A)
{
	AdjRel *aux;

	aux = *A;
	if(aux != NULL)
	{
		if(aux->dx != NULL) free(aux->dx);
		if(aux->dy != NULL) free(aux->dy);
		free(aux);
		*A = NULL;
	}
}


AdjRel *clone_adjrel(AdjRel *A)
{
	AdjRel *C;
	int i;

	C = create_adjrel(A->n);
	for(i = 0; i < A->n; i++)
	{
		C->dx[i] = A->dx[i];
		C->dy[i] = A->dy[i];
	}

	return C;
}

Pixel next_adjacent(usis::Image8 *img, AdjRel *A, Pixel v, int &cur_idx)
{
	return next_adjacent<usis::Image8*>(img, A, v, cur_idx);
}

Pixel next_adjacent(usis::Image32 *img, AdjRel *A, Pixel v, int &cur_idx)
{
	return next_adjacent<usis::Image32*>(img, A, v, cur_idx);
}


Pixel next_adjacent(usis::CImage8 *img, AdjRel *A, Pixel v, int &cur_idx)
{
	return next_adjacent<usis::CImage8*>(img, A, v, cur_idx);
}

Pixel next_adjacent(usis::CImage32 *img, AdjRel *A, Pixel v, int &cur_idx)
{
	return next_adjacent<usis::CImage32*>(img, A, v, cur_idx);
}

Pixel next_adjacent(usis::DbImage *img, AdjRel *A, Pixel v, int &cur_idx)
{
	return next_adjacent<usis::DbImage*>(img, A, v, cur_idx);
}

Pixel next_adjacent(Features *img, AdjRel *A, Pixel v, int &cur_idx)
{
	return next_adjacent<Features*>(img, A, v, cur_idx);
}

AdjRel *right_side(AdjRel *A)
{
	AdjRel *R = NULL;
	int i;
	float d;

	/* Let p -> q be an arc represented by the increments dx,dy. Its
	   right side is given by the increments Dx = -dy/d + dx/2 and Dy =
	   dx/d + dy/2, where d=sqrt(dx�+dy�). */

	R = create_adjrel(A->n);
	for(i = 0; i < R->n; i++)
	{
		d  = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);
		if(d != 0)
		{
			R->dx[i] = ROUND(((float)A->dx[i] / 2.0) - ((float)A->dy[i] / d));
			R->dy[i] = ROUND(((float)A->dx[i] / d) + ((float)A->dy[i] / 2.0));
		}
	}

	return(R);
}

AdjRel *left_side(AdjRel *A)
{
	AdjRel *L = NULL;
	int i;
	float d;

	/* Let p -> q be an arc represented by the increments dx,dy. Its
	   left side is given by the increments Dx = dy/d + dx/2 and Dy =
	   -dx/d + dy/2, where d=sqrt(dx�+dy�). */

	L = create_adjrel(A->n);
	for(i = 0; i < L->n; i++)
	{
		d  = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);
		if(d != 0)
		{
			L->dx[i] = ROUND(((float)A->dx[i] / 2.0) + ((float)A->dy[i] / d));
			L->dy[i] = ROUND(((float)A->dy[i] / 2.0) - ((float)A->dx[i] / d));
		}
	}

	return(L);
}

AdjRel *right_side2(AdjRel *A, float r)
{
	AdjRel *R = NULL;
	int i;
	float d;

	/* Let p -> q be an arc represented by the increments dx,dy. Its
	   right side is given by the increments Dx = -dy/d + dx/2 and Dy =
	   dx/d + dy/2, where d=sqrt(dx�+dy�). */

	R = create_adjrel(A->n);
	for(i = 0; i < R->n; i++)
	{
		d  = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);
		if(d != 0)
		{
			R->dx[i] = ROUND(((float)A->dx[i] / 2.0) - ((float)A->dy[i] / d) * r);
			R->dy[i] = ROUND(((float)A->dx[i] / d) + ((float)A->dy[i] / 2.0) * r);
		}
	}

	return(R);
}

AdjRel *left_side2(AdjRel *A, float r)
{
	AdjRel *L = NULL;
	int i;
	float d;

	/* Let p -> q be an arc represented by the increments dx,dy. Its
	   left side is given by the increments Dx = dy/d + dx/2 and Dy =
	   -dx/d + dy/2, where d=sqrt(dx�+dy�). */

	L = create_adjrel(A->n);
	for(i = 0; i < L->n; i++)
	{
		d  = sqrt(A->dx[i] * A->dx[i] + A->dy[i] * A->dy[i]);
		if(d != 0)
		{
			L->dx[i] = ROUND(((float)A->dx[i] / 2.0) + ((float)A->dy[i] / d) * r);
			L->dy[i] = ROUND(((float)A->dy[i] / 2) - ((float)A->dx[i] / d) * r);
		}
	}

	return(L);
}


AdjRel *circular(float r)
{
	AdjRel *A = NULL;
	int i, j, k, n, dx, dy, r0, d, i0 = 0;
	float *da, *dr, aux, r2;

	n = 0;

	r0 = (int)r;
	r2  = (int)(r * r + 0.5);
	for(dy = -r0; dy <= r0; dy++)
		for(dx = -r0; dx <= r0; dx++)
			if(((dx * dx) + (dy * dy)) <= r2)
				n++;

	A = create_adjrel(n);
	i = 0;
	for(dy = -r0; dy <= r0; dy++)
		for(dx = -r0; dx <= r0; dx++)
			if(((dx * dx) + (dy * dy)) <= r2)
			{
				A->dx[i] = dx;
				A->dy[i] = dy;
				if((dx == 0) && (dy == 0))
					i0 = i;
				i++;
			}

	/* Set clockwise */

	da = AllocFloatArray(A->n);
	dr = AllocFloatArray(A->n);
	for(i = 0; i < A->n; i++)
	{
		dx = A->dx[i];
		dy = A->dy[i];
		dr[i] = (float)sqrt((dx * dx) + (dy * dy));
		if(i != i0)
		{
			da[i] = atan2(-dy, -dx) * 180.0 / M_PI;
			if(da[i] < 0.0)
				da[i] += 360.0;
		}
	}
	da[i0] = 0.0;
	dr[i0] = 0.0;

	/* place central pixel at first */

	aux    = da[i0];
	da[i0] = da[0];
	da[0]  = aux;
	aux    = dr[i0];
	dr[i0] = dr[0];
	dr[0]  = aux;
	d         = A->dx[i0];
	A->dx[i0] = A->dx[0];
	A->dx[0]  = d;
	d         = A->dy[i0];
	A->dy[i0] = A->dy[0];
	A->dy[0]  = d;

	/* sort by angle */

	for(i = 1; i < A->n - 1; i++)
	{
		k = i;
		for(j = i + 1; j < A->n; j++)
			if(da[j] < da[k])
			{
				k = j;
			}
		aux   = da[i];
		da[i] = da[k];
		da[k] = aux;
		aux   = dr[i];
		dr[i] = dr[k];
		dr[k] = aux;
		d   = A->dx[i];
		A->dx[i] = A->dx[k];
		A->dx[k] = d;
		d        = A->dy[i];
		A->dy[i] = A->dy[k];
		A->dy[k] = d;
	}

	/* sort by radius for each angle */

	for(i = 1; i < A->n - 1; i++)
	{
		k = i;
		for(j = i + 1; j < A->n; j++)
			if((dr[j] < dr[k]) && (da[j] == da[k]))
			{
				k = j;
			}
		aux   = dr[i];
		dr[i] = dr[k];
		dr[k] = aux;
		d        = A->dx[i];
		A->dx[i] = A->dx[k];
		A->dx[k] = d;
		d        = A->dy[i];
		A->dy[i] = A->dy[k];
		A->dy[k] = d;
	}

	free(dr);
	free(da);

	return(A);
}


AdjRel *fast_circular(float r)
{
	AdjRel *A = NULL;
	int i, n, dx, dy, r0, d, i0 = 0;
	float r2;

	n = 0;
	r0 = (int)r;
	r2  = (int)(r * r + 0.5);
	for(dy = -r0; dy <= r0; dy++)
		for(dx = -r0; dx <= r0; dx++)
			if(((dx * dx) + (dy * dy)) <= r2)
				n++;

	A = create_adjrel(n);
	i = 0;
	for(dy = -r0; dy <= r0; dy++)
		for(dx = -r0; dx <= r0; dx++)
			if(((dx * dx) + (dy * dy)) <= r2)
			{
				A->dx[i] = dx;
				A->dy[i] = dy;
				if((dx == 0) && (dy == 0))
					i0 = i;
				i++;
			}


	/* place central pixel at first */
	d         = A->dx[i0];
	A->dx[i0] = A->dx[0];
	A->dx[0]  = d;
	d         = A->dy[i0];
	A->dy[i0] = A->dy[0];
	A->dy[0]  = d;

	return(A);
}

AdjRel *fast_ring_sector(double radius1, double radius2, double theta1, double theta2)
{
	AdjRel *A = NULL;
	int i, n, dx, dy, r2, i0 = NIL;
	double r1_2, r2_2;

	assert(radius1 < radius2);

	n = 0;
	r2 = (int)radius2;
	r1_2  = (int)(radius1 * radius1 + 0.5);
	r2_2  = (int)(radius2 * radius2 + 0.5);

	for(dy = -r2; dy <= r2; dy++)
		for(dx = -r2; dx <= r2; dx++)
		{
			double dist = ((dx * dx) + (dy * dy));
			usis::Vector2D vec(dx, dy);
			double angle = vec.Direction();


			if(theta1 <= theta2)
			{
				if(dist >= r1_2 && dist <= r2_2
					&& angle >= theta1 && angle <= theta2)
					n++;
			}
			else
			{
				if(dist >= r1_2 && dist <= r2_2
					&& ((angle <= 2*M_PI && angle >= theta1)
					|| (angle >= 0.0  && angle <= theta2)))
					n++;
			}
		}
	A = create_adjrel(n);
	i = 0;
	for(dy = -r2; dy <= r2; dy++)
		for(dx = -r2; dx <= r2; dx++)
		{
			double dist = ((dx * dx) + (dy * dy));
			usis::Vector2D vec(dx, dy);
			double angle = vec.Direction();

			if(theta1 <= theta2)
			{
				if(dist >= r1_2 && dist <= r2_2
					&& angle >= theta1 && angle <= theta2)
				{
					A->dx[i] = dx;
					A->dy[i] = dy;
					if((dx == 0) && (dy == 0))
						i0 = i;
					i++;
				}
			}
			else
			{
				if(dist >= r1_2 && dist <= r2_2
					&& ((angle <= 2*M_PI && angle >= theta1)
					|| (angle >= 0.0  && angle <= theta2)))
				{
					A->dx[i] = dx;
					A->dy[i] = dy;
					if((dx == 0) && (dy == 0))
						i0 = i;
					i++;
				}
			}
		}

	/* place central pixel at first if found */
	if(i0 >= 0)
	{
		int d;

		d         = A->dx[i0];
		A->dx[i0] = A->dx[0];
		A->dx[0]  = d;
		d         = A->dy[i0];
		A->dy[i0] = A->dy[0];
		A->dy[0]  = d;
	}

	return(A);
}

AdjRel *transform_adjacency(AdjRel *A, float direct_transform[4][4])
{
	int i, n_new = 0;
	unordered_map<int, unordered_set<int> > unique_displacements;
	unordered_map<int, unordered_set<int> >::iterator it;
	AdjRel *Anew = NULL;

	for(i = 0; i < A->n; i++)
	{
		usis::Vector2D vec;

		vec.x = A->dx[i];
		vec.y = A->dy[i];

		vec = usis::transform_vector(direct_transform, vec, true);

		unique_displacements[(int)vec.x].insert((int)vec.y);
	}

	it = unique_displacements.begin();
	for(;it != unique_displacements.end(); it++)
	{
		n_new += it->second.size();
	}

	Anew = create_adjrel(n_new);

	it = unique_displacements.begin();
	for(i = 0; it != unique_displacements.end(); it++)
	{
		unordered_set<int>::iterator y_it;

		for(y_it = it->second.begin(); y_it != it->second.end(); y_it++)
		{
			Anew->dx[i] = (int)it->first;
			Anew->dy[i] = (int)(*y_it);
			i++;
		}
	}

	return Anew;
}

int adjacent_index(AdjRel *A, Pixel px, Pixel px_adj)
{
	int i, index = NIL;

	for(i = 0; i < A->n && index == NIL; i++)
	{
		if(A->dx[i] == px_adj.x - px.x
			&& A->dy[i] == px_adj.y - px.y)
		{
			index = i;
		}
	}

	return index;
}

usis::Image8  *adjrel2image(AdjRel *A)
{
	int p, i, dx, dy, dxmax, dymax;
	usis::Image8 *mask;
	Pixel v, u;

	dxmax = dymax = 0;
	for(i = 0; i < A->n; i++)
	{
		dx = abs(A->dx[i]);
		dy = abs(A->dy[i]);
		if(dx > dxmax)
			dxmax = dx;
		if(dy > dymax)
			dymax = dy;
	}

	mask = usis::create_image8(dxmax * 2 + 1,
	                           dymax * 2 + 1);
	u.x = dxmax;
	u.y = dymax;
	for(i = 0; i < A->n; i++)
	{
		v.x = u.x + A->dx[i];
		v.y = u.y + A->dy[i];

		if(usis::valid_pixel(mask, v.x, v.y))
		{
			p = usis::to_index(mask, v);
			mask->val[p] = 1;
		}
	}
	return mask;
}


AdjRel *box(int ncols, int nrows)
{
	AdjRel *A = NULL;
	int i, dx, dy;

	if(ncols % 2 == 0) ncols++;
	if(nrows % 2 == 0) nrows++;

	A = create_adjrel(ncols * nrows);
	i = 1;
	for(dy = -nrows / 2; dy <= nrows / 2; dy++)
	{
		for(dx = -ncols / 2; dx <= ncols / 2; dx++)
		{
			if((dx != 0) || (dy != 0))
			{
				A->dx[i] = dx;
				A->dy[i] = dy;
				i++;
			}
		}
	}
	/* place the central pixel at first */
	A->dx[0] = 0;
	A->dy[0] = 0;

	return(A);
}


void DestroyAdjPxl(AdjPxl **N)
{
	AdjPxl *aux;

	aux = *N;
	if(aux != NULL)
	{
		if(aux->dp != NULL) free(aux->dp);
		free(aux);
		*N = NULL;
	}
}


int FrameSize(AdjRel *A)
{
	int sz = INT_MIN, i = 0;

	for(i = 0; i < A->n; i++)
	{
		if(fabs(A->dx[i]) > sz)
			sz = fabs(A->dx[i]);
		if(fabs(A->dy[i]) > sz)
			sz = fabs(A->dy[i]);
	}
	return(sz);
}


