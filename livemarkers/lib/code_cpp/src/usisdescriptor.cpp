#include "usisdescriptor.h"
#include "usissegmentation.h"

#include <algorithm>

#define MAT3D(m, x, y, z, xs, ys, zs) (m[(x)+(y)*(xs)+(z)*(ys)*(xs)])

namespace usis
{
Curve *create_curve(int n)
{
    Curve *curve = NULL;

    curve = (Curve *) calloc(1, sizeof(Curve));
    if(curve != NULL)
    {
        curve->X = AllocDoubleArray(n);
        curve->Y = AllocDoubleArray(n);
        curve->n = n;
    }
    else
    {
        Error(MSG1, (char*)"create_curve");
    }
    return(curve);
}

void destroy_curve(Curve **curve)
{
    Curve *aux;

    aux = *curve;
    if(aux != NULL)
    {
        if(aux->X != NULL) free(aux->X);
        if(aux->Y != NULL) free(aux->Y);
        free(aux);
        *curve = NULL;
    }
}

FeatureVector1D  *curve_to_1D_feature_vector(Curve *curve)
{
    FeatureVector1D *fv;

    fv = create_feature_vector1D(curve->n);
    memcpy(fv->X, curve->Y, curve->n * sizeof(double));

    return fv;
}

FeatureVector1D :: FeatureVector1D(int n)
{
    this->X = AllocDoubleArray(n);
    this->n = n;
}

FeatureVector1D ::~FeatureVector1D()
{
    if(this->X != NULL)
    {
        free(this->X);
        this->X = NULL;
    }
}

FeatureVector1D *create_feature_vector1D(int n)
{
    FeatureVector1D *desc = new FeatureVector1D(n);

    return desc;
}

void destroy_feature_vector1D(FeatureVector1D **desc)
{
    if(desc != NULL && *desc != NULL)
    {
        delete *desc;
        *desc = NULL;
    }
}

Image32 *quantize_colors(CImage8 *img, int color_dim)
{
    int i;
    Image32 *color;

    color = create_image32(img->ncols, img->nrows);
    if(color == NULL)
    {
        printf("\nOut of memory \n");
        exit(-1);
    }

    for(i = 0; i < img->n; i++)
    {
    	int r,g,b;

        r = img->C[0]->val[i];
        g = img->C[1]->val[i];
        b = img->C[2]->val[i];

        color->val[i] = quantize_rgb(r,g,b, color_dim);
    }
    return color;
}

void quantize_colors(CImage8 *img, unordered_map<int, Property> &pixels, int color_dim)
{
    int r, g, b;

    unordered_map<int, Property>::iterator it;

    for(it = pixels.begin(); it != pixels.end(); it++)
    {
        int p = it->first;

        r = img->C[0]->val[p];
        g = img->C[1]->val[p];
        b = img->C[2]->val[p];

        it->second.color = quantize_rgb(r,g,b,color_dim);
    }
}

/***************************************************************************/
unsigned char compute_log(float value)
{
    unsigned char result;

    value = 255. * value;
    if(value == 0.)       result = 0;
    else if(value < 1.)   result = 1;
    else if(value < 2.)   result = 2;
    else if(value < 4.)   result = 3;
    else if(value < 8.)   result = 4;
    else if(value < 16.)  result = 5;
    else if(value < 32.)  result = 6;
    else if(value < 64.)  result = 7;
    else if(value < 128.) result = 8;
    else                result = 9;

    return result;
}

/**************************************************************************/
void compress_histogram(unsigned char *ch, unsigned long *h,
                        unsigned long max, int size)
{
    int i;
    unsigned char v;

    for(i = 0; i < size; i++)
    {
        v = compute_log((float) h[i] / (float) max);
        ch[i] = (unsigned char)(48 + v);
    }
}


/****************************************************************************/
void compute_frequency_property(Image8 *img, Property *ppt)
{

    int x, y, p, q;
    int i;
    bool border;
    AdjRel *A;
    Pixel v;

    A = circular(1.0);

    for(y = 0L; y < img->nrows; y++)
    {
        for(x = 0L; x < img->ncols; x++)
        {
            p = usis::to_index(img, x, y);
            border = false;
            for(i = 1; i < A->n; i++)
            {
                v.x = x + A->dx[i];
                v.y = y + A->dy[i];
                if(valid_pixel(img, v.x, v.y))
                {
                    q = to_index(img, v);
                    if(ppt[p].color != ppt[q].color)
                    {
                        border = true;
                        break;
                    }
                }
            }
            if(border == false)
                ppt[p].frequency = LOW;
            else ppt[p].frequency = HIGH;
        }
    }
    destroy_adjrel(&A);
}

/****************************************************************************/
void compute_frequency_property(CImage8 *img, unordered_map<int, Property> &pixels)
{

    unsigned long p, q;
    int i;
    bool border;
    AdjRel *A;
    Pixel v, u;
    unordered_map<int, Property>::iterator it;

    A = circular(1.0);

    for(it = pixels.begin(); it != pixels.end(); it++)
    {
        p = it->first;
        u = usis::to_pixel(img, p);
        border = false;
        for(i = 1; i < A->n; i++)
        {
            v.x = u.x + A->dx[i];
            v.y = u.y + A->dy[i];

            if(valid_pixel(img, v))
            {
                q = usis::to_index(img, v);

                if(pixels.find(q) != pixels.end() &&
                        pixels[p].color != pixels[q].color)
                {
                    border = true;
                    break;
                }
            }
        }

        if(border == false)
            pixels[p].frequency = LOW;
        else
            pixels[p].frequency = HIGH;
    }

    destroy_adjrel(&A);
}

/****************************************************************************/
Property *compute_pixels_properties(CImage8 *img)
{
    Property *p;
    int i, n;
    Image32 *color = NULL;

    n = img->nrows * img->ncols;

    p = (Property *) calloc(n, sizeof(Property));
    if(p == NULL)
    {
        printf("\nOut of memory \n");
        exit(-1);
    }

    color = quantize_colors(img, 4);
    for(i = 0; i < n; i++)
        p[i].color = color->val[i];
    compute_frequency_property(img->C[0], p);

    destroy_image(&color);
    return p;
}


FeatureVector1D *histogram_descriptor(Features *feat, unordered_map<int, Property> &pixels,
                                      int bins_per_channel)
{
    int i;
    int desc_size = bins_per_channel*bins_per_channel*bins_per_channel;
    Curve *curve = create_curve(desc_size);
    FeatureVector1D *vec;
    unordered_map<int, Property>::iterator it;
	float factor = feat->Fmax - feat->Fmin;

	factor = factor*1.01; // Increasing the factor by 1% to prevent
						  // overflows

    for(i = 0; i < desc_size; i++)
    {
        curve->X[i] = i;
    }

    for(it = pixels.begin(); it != pixels.end(); it++)
    {
        /*GCH*/
		int p = it->first;
		int quant_feat = 0;

		for(int i = 0; i < feat->nfeats; i++)
		{
			int f_quant;

			f_quant = (int)floorf(feat->elem[p].feat[i]*bins_per_channel/factor);

			quant_feat += f_quant*pow(bins_per_channel,i);
		}

		curve->Y[quant_feat]++;
    }

    vec = curve_to_1D_feature_vector(curve);

    // Normalizing histogram
    normalize_histogram(vec);

    destroy_curve(&curve);

    return vec;
}


FeatureVector1D *histogram_descriptor(CImage8 *img, unordered_map<int, Property> &pixels,
                                      int bins_per_channel)
{
    int i;
    int desc_size = bins_per_channel*bins_per_channel*bins_per_channel;
    Curve *curve = create_curve(desc_size);
    FeatureVector1D *vec;
    unordered_map<int, Property>::iterator it;

    quantize_colors(img, pixels, bins_per_channel);

    for(i = 0; i < desc_size; i++)
    {
        curve->X[i] = i;
    }

    for(it = pixels.begin(); it != pixels.end(); it++)
    {
        /*GCH*/
        curve->Y[it->second.color]++;
    }

    vec = curve_to_1D_feature_vector(curve);

    // Normalizing histogram
    normalize_histogram(vec);

    destroy_curve(&curve);

    return vec;
}

void normalize_histogram(FeatureVector1D *hist)
{
    int i;
    double sum = 0.0;

    for(i = 0; i < hist->n; i++)
        sum += hist->X[i];

    if(sum > DBL_EPSILON)
        for(i = 0; i < hist->n; i++)
            hist->X[i] = hist->X[i]/sum;
}

double chi_2_distance(FeatureVector1D *hist1, FeatureVector1D *hist2)
{
    int i;
    double dist = 0.0f;

    for(i = 0; i < hist1->n; i++)
    {
        double sum = hist1->X[i] + hist2->X[i];

        if(sum <= DBL_EPSILON)
            continue;

        dist += pow(hist1->X[i] - hist2->X[i], 2)/sum;
    }

    return dist/2.0;
}

double euclidean_distance(FeatureVector1D *hist1, FeatureVector1D *hist2)
{
    int i;
    double dist = 0.0f;

    for(i = 0; i < hist1->n; i++)
    {
        dist += pow(hist1->X[i] - hist2->X[i], 2);
    }

    return sqrt(dist);
}



void write_feature_vector1d(FeatureVector1D *vec, const char *format, ...)
{
    FILE *fp = NULL;
    int i;

    va_list args;
    char filename[BUFSIZ];

    va_start(args, format);
    vsprintf(filename, format, args);
    va_end(args);

    fp = fopen(filename,"w");

    if(fp == NULL)
    {
        fprintf(stderr, "Cannot open %s\n", filename);
        return;
    }

    for(i = 0; i < vec->n; i++)
    {
        fprintf(fp,"%f\n",vec->X[i]);
    }

    fclose(fp);
}

}
