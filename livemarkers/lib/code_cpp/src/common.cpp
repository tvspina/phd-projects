/*
  Copyright (C) <2009> <Alexandre Xavier Falcão and João Paulo Papa>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  please see full copyright in COPYING file.
  -------------------------------------------------------------------------
  written by A.X. Falcão <afalcao@ic.unicamp.br> and by J.P. Papa
  <papa.joaopaulo@gmail.com>, Oct 20th 2008

  This program is a collection of functions to manage the Optimum-Path Forest (OPF)
  classifier.*/

#include "common.h"

#define EXPERIMENT_RANDOM_SEED 12333 // fixed seed for reproducible experiments
//#define EXPERIMENT_RANDOM_SEED (int)time(NULL) // "random seed"


Rectangle::Rectangle() : beg((Pixel){NIL,NIL}), end((Pixel){NIL,NIL})
			//x(NIL), y(NIL), width(NIL), height(NIL)
{

}

Rectangle::Rectangle(int x0, int y0, int xf, int yf) : beg((Pixel){x0,y0}),
			end((Pixel){xf,yf})
			//x(x0), y(y0), width(xf-x0+1), height(yf-y0+1)
{

}

Rectangle::Rectangle(Pixel px0, Pixel pxf) : beg(px0), end(pxf)
			//x(px0.x), y(px0.y), width(pxf.x-px0.x+1), height(pxf.y-px0.y+1)
{
}


int *AllocIntArray(int n)
{
    int *v=NULL;
    v = (int *) calloc(n,sizeof(int));
    if (v == NULL)
        Error(MSG1,"AllocIntArray");
    return(v);
}

float *AllocFloatArray(int n)
{
    float *v=NULL;
    v = (float *) calloc(n,sizeof(float));
    if (v == NULL)
        Error(MSG1,"AllocFloatArray");
    return(v);
}


char *AllocCharArray(int n)
{
    char *v = NULL;
    v = (char *) calloc(n, sizeof(char));
    if(v == NULL)
        Error(MSG1, "AllocCharArray");
    return(v);
}

uchar *AllocUCharArray(int n)
{
    uchar *v = NULL;
    v = (uchar *) calloc(n, sizeof(uchar));
    if(v == NULL)
        Error(MSG1, "AllocUCharArray");
    return(v);
}

ushort *AllocUShortArray(int n)
{
    ushort *v = NULL;
    v = (ushort *) calloc(n, sizeof(ushort));
    if(v == NULL)
        Error(MSG1, "AllocUShortArray");
    return(v);
}

uint *AllocUIntArray(int n)
{
    uint *v = NULL;
    v = (uint *) calloc(n, sizeof(uint));
    if(v == NULL)
        Error(MSG1, "AllocUIntArray");
    return(v);
}

double *AllocDoubleArray(int n)
{
    double *v = NULL;
    v = (double *) calloc(n, sizeof(double));
    if(v == NULL)
        Error(MSG1, "AllocDoubleArray");
    return(v);
}




void Error(const char *msg, const char *func)
{
    /* It prints error message and exits
                                        the program. */
    fprintf(stderr,"Error:%s in %s\n",msg,func);
    exit(-1);
}

void Warning(const char *msg, const char *func)
{
    /* It prints warning message and
                                           leaves the routine. */
    fprintf(stdout,"Warning:%s in %s\n",msg,func);

}


int random_integer(int low, int high, void *random_generator)
{
	int k;
	// Checking boost minor version to determine which header to include
	#if ((BOOST_VERSION / 100) % 1000) >= 50
		static boost::random::mt19937 internal_gen;

		boost::mt19937 *gen = NULL;
		boost::random::uniform_int_distribution<> dist(low,high);

		//Using default random number generator
		if(random_generator == NULL)
			gen = &internal_gen;
		else
			gen = (boost::mt19937*)random_generator;
	#else
		static boost::mt19937 internal_gen;

		boost::mt19937 *gen = NULL;
		boost::uniform_int<> dist(low,high);

		//Using default random number generator
		if(random_generator == NULL)
			gen = &internal_gen;
		else
			gen = (boost::mt19937*)random_generator;
	#endif

	k = dist(*gen);

	return k;
}


void* random_seed()
{
    boost::mt19937 *gen = new boost::mt19937(EXPERIMENT_RANDOM_SEED);

    return gen;
}

void destroy_random_generator(void **generator)
{
	if(generator != NULL)
	{
		if(*generator != NULL)
		{
			boost::mt19937 *gen = NULL;
			gen = (boost::mt19937 *)*generator;
			delete gen;
			*generator = NULL;
		}
	}
}

int ncf_gets(char *s, int m, FILE *f)
{
    while(fgets(s, m, f) != NULL)
        if(s[0] != '#') return 1;
    return 0;
}

