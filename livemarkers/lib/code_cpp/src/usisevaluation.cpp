#include "usisevaluation.h"

namespace usis
{

double dice_similarity(Image8 *mask1, Image8 *mask2)
{
	double nelems_intersec, nelems_union;
	int  p, n;

	/* compute similarity between shapes */
	n = mask1->ncols * mask1->nrows;
	nelems_intersec = nelems_union = 0.0;

	for(p = 0; p < n; p++)
	{
		if(mask1->val[p] > 0)
		{
			nelems_union = nelems_union + 1;

			if(mask2->val[p] > 0)
				nelems_intersec++;
		}
		else
		{
			if(mask2->val[p] > 0)
				nelems_union++;
		}
	}

	return ((2.0 * nelems_intersec) / (nelems_union + nelems_intersec));
}


double precision(int tp, int fp)
{
	if(tp + fp == 0) return 0;

	return (double)tp / (double)(tp + fp);
}

double recall(int tp, int fn)
{
	if(tp + fn == 0) return 0;

	return (double)tp / (double)(tp + fn);
}

double fmeasure(double precision, double recall)
{
	if(precision + recall == 0) return 0;

	return 2 * (precision * recall) / (precision + recall);
}

void bin_conf_matrix(Image8 *label, Image8 *mask, int *tp, int *tn,
					int *fp, int *fn, Rectangle _bb)
{
	*tp = 0; *tn = 0; *fp = 0; *fn = 0;

	int lbmax = maximum_value(label), maskmax = maximum_value(mask);

	conf_matrix(label, mask, tp, tn, fp, fn, lbmax, maskmax, _bb);
}

double compute_bin_fmeasure(Image8 *label, Image8 *mask)
{
	if(label == NULL || mask == NULL) return -1;

	int tp = 0, tn = 0, fp = 0, fn = 0;

	bin_conf_matrix(label, mask, &tp, &tn, &fp, &fn);

	double f = fmeasure(precision(tp, fp), recall(tp, fn));

	return f;
}

double compute_fuzzy_gt(Image8 *label, Image8 *mask)
{
	if(label == NULL || mask == NULL) return -1;

	int i;
	int Imax = maximum_value(mask);
	double RImax = (double)Imax;

	double measure = 0;
	double total = 0;

	for(i = 0; i < label->ncols * label->nrows; i++)
	{
		if(label->val[i] == 1)
			if(mask->val[i] == 0)
				measure += -RImax;
			else
				measure += (double)mask->val[i];
		else if(mask->val[i] == Imax)
			measure += -RImax;
		else
			measure += (double) - mask->val[i];

		total += ((double)mask->val[i]);
	}

	return measure / total;
}

double grabcut_error_rate(Image8 *label, Image8 *mask)
{
	int mmax = maximum_value(mask);
	int mmin = minimum_value(mask);

	int p;
	int nmisclassified = 0;
	int nunknown = 0;

	for(p = 0; p < mask->ncols * mask->nrows; p++)
	{
		if(mask->val[p] > mmin && mask->val[p] < mmax)
		{
			nunknown++;
		}
		else
		{
			if((int)labs(label->val[p]*mmax - mask->val[p]) == mmax)
				nmisclassified++;
		}
	}

	return ((double)nmisclassified) / (double)nunknown;

}

	void conf_matrix(Image8 *label, Image8 *mask, int *truepos, int *trueneg,
					int *falsepos, int *falseneg, int lb, int lb_mask,
					Rectangle _bb)
	{
		int p;

		int tp = 0, tn = 0, fp = 0, fn = 0;

		Rectangle bb =  Rectangle(MAX(_bb.beg.x,0), MAX(_bb.beg.y,0),
							MIN((_bb.end.x <= 0) ? mask->ncols-1 : _bb.end.x,mask->ncols-1),
							MIN((_bb.end.y <= 0) ? mask->nrows-1 : _bb.end.y,mask->nrows-1));

		for(int y = bb.beg.y; y <= bb.end.y; y++)
			for(int x = bb.beg.x; x <= bb.end.x; x++)
			{
				p = to_index(mask, x, y);
				if((label->val[p] > 0 && lb < 0)
					|| (label->val[p] == lb && lb > 0))
				{
					if((mask->val[p] > 0 && lb_mask < 0)
							|| (mask->val[p] == lb_mask && lb_mask > 0))
						tp++;
					else
						fp++;
				}
				else
				{
					if((mask->val[p] > 0 && lb_mask < 0)
							|| (mask->val[p] == lb_mask && lb_mask > 0))
						fn++;
					else
						tn++;
				}
			}

		if(truepos != NULL) *truepos = tp;

		if(trueneg != NULL) *trueneg = tn;

		if(falsepos != NULL) *falsepos = fp;

		if(falseneg != NULL) *falseneg = fn;
	}

	double compute_fmeasure(Image8 *label, Image8 *mask)
	{
		if(label == NULL || mask == NULL) return -1;

		int tp = 0, tn = 0, fp = 0, fn = 0;

		conf_matrix(label, mask, &tp, &tn, &fp, &fn);

		double f = fmeasure(precision(tp, fp), recall(tp, fn));

		return f;
	}


/// Given a label and a groundtruth, computes the mean squared distance
/// between the groundtruth border and the object's segmentation border,
/// thus representing the mean squared error (positive and negative errors
/// depending on the EDT sign).
/// It also computes the maximum positive and negative distances.
	void sgn_edt2_error_measures(Image8 *label, Image8 *gt, float *max_dist_pos, float *max_dist_neg,
	                                 float *fp_dist_sum, float *fn_dist_sum, int *fp_size, int *fn_size, int *border_size)
	{
		int p;
		Image8 *gt_border = object_border(gt);
		Image8 *lb_border = object_border(label);
		Image32 *sgn_tde = signed_edt2(gt_border, gt);

		*max_dist_neg = -USIS_DINFTY;
		*max_dist_pos = -USIS_DINFTY;
		*fn_dist_sum = 0.0;
		*fp_dist_sum = 0.0;
		*fp_size = 0;
		*fn_size = 0;
		*border_size = 0;

		for(p = 0; p < label->ncols*label->nrows; p++)
		{
			if(lb_border->val[p] > 0)
			{
				if(sgn_tde->val[p] > 0)
				{
					*fp_dist_sum += sgn_tde->val[p];
					*max_dist_pos = MAX(*max_dist_pos, sgn_tde->val[p]);
					(*fp_size)++;
				}
				else if(sgn_tde->val[p] < 0)
				{
					*fn_dist_sum += -sgn_tde->val[p];
					*max_dist_neg = MAX(*max_dist_neg, -sgn_tde->val[p]);
					(*fn_size)++;
				}

				(*border_size)++;
			}
		}

		destroy_image(&gt_border);
		destroy_image(&lb_border);
		destroy_image(&sgn_tde);
	}

/// Given a label and a groundtruth, computes the mean squared distance
/// between the groundtruth border and the object's segmentation border,
/// thus representing the mean squared error (positive and negative errors
/// depending on the EDT sign).
/// It also computes the maximum positive and negative distances.
	void sgn_edt_error_measures(Image8 *label, Image8 *gt, float *max_dist_pos, float *max_dist_neg,
	                                 float *fp_dist_sum, float *fn_dist_sum, int *fp_size, int *fn_size, int *border_size)
	{
		int p;
		Image8 *gt_border = object_border(gt);
		Image8 *lb_border = object_border(label);
		DbImage *sgn_tde = signed_edtdb(gt_border, gt);

		*max_dist_neg = -USIS_DINFTY;
		*max_dist_pos = -USIS_DINFTY;
		*fn_dist_sum = 0.0;
		*fp_dist_sum = 0.0;
		*fp_size = 0;
		*fn_size = 0;
		*border_size = 0;

		for(p = 0; p < label->ncols*label->nrows; p++)
		{
			if(lb_border->val[p] > 0)
			{
				if(sgn_tde->val[p] > 0)
				{
					*fp_dist_sum += sgn_tde->val[p];
					*max_dist_pos = MAX(*max_dist_pos, sgn_tde->val[p]);
					(*fp_size)++;
				}
				else if(sgn_tde->val[p] < 0)
				{
					*fn_dist_sum += -sgn_tde->val[p];
					*max_dist_neg = MAX(*max_dist_neg, -sgn_tde->val[p]);
					(*fn_size)++;
				}

				(*border_size)++;
			}
		}

		destroy_image(&gt_border);
		destroy_image(&lb_border);
		destroy_dbimage(&sgn_tde);
	}

	void controllability_measures(Image32 *changes, int niterations,
	                              float *avg_changes, float *avg_changed_pixels,
	                              float *avg_changes_by_changed_pixels,
	                              float *controllability, int *n_changes,
	                              int *n_changed_pixels)
	{
		int n = changes->ncols*changes->nrows;

		float max_nchanges = 0;

		max_nchanges = floor((niterations - 1)/2);
		max_nchanges = n*max_nchanges;

		num_label_changes(changes, n_changes, n_changed_pixels);

		*avg_changes = *n_changes/(float)n;
		*avg_changed_pixels = *n_changed_pixels/(float)n;
		// dividing by 1.0 if no changes occurred
		*avg_changes_by_changed_pixels = *n_changes/MAX(1.0,(float)*n_changed_pixels);
		*controllability = 1.0 - *n_changes/MAX(max_nchanges,1.0);
	}


	void num_label_changes(Image32 *changes, int *n_changes, int *n_changed_pixels)
	{
		int p;
		int n = changes->ncols*changes->nrows;
		*n_changes = 0;
		*n_changed_pixels = 0;

		for(p = 0; p < n; p++)
		{
			if(changes->val[p] > 0)
			{
				(*n_changed_pixels)++;
				*n_changes += changes->val[p];
			}
		}
	}


}
