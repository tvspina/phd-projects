//#ifndef _USISEDITING_HPP_
//#define _USISEDITING_HPP_
//
//extern "C"
//{
//#include "glist.h"
//#include "code_c.h"
//}
//
//#include "usisimage.h"
//#include "usisfuzzyopf.h"
//#include "usismorphology.h"
//#include "usisgeometry.h"
//
//#ifndef USIS_MAXALPHA
//#define USIS_MAXALPHA 255
//#endif
//
//namespace usis
//{
//	class FOPFAMattingData
//	{
//		public:
//			FOPFAMattingData();
//			~FOPFAMattingData();
//
//			Set *Si;
//			Set *Se;
//			Set *pixels; /// pixels to which an alpha blending value
//			/// should be associated
//	};
//
//	void destroy_fopf_amatting_data(void **data);
//
//	//GList* usis_border_sets(Image8* label, Features* feat, int length, float inadj, float outadj);
//
//	GList *border_sets(Image8 *label, int seglength, AdjRel *A, bool wide_border);
//
//// Computes alpha values for pixels on (and around) the border
//// of the segmented object(s)
//	Image8 *fopf_alpha_border(Image8 *label, Features *feat, int length, float radius, bool wide_border, double max_arc_weight);
//	Image8 *fopf_alpha_border_aux(GList *list, Image8 *label, Features *feat, double max_arc_weight);
//
//	Image *soft_segmentation(Image *label, Image8 *mask, Set *seeds, int threshold, int objlabel, int bkglabel);
//	Image *alpha_from_objmap(Image *label, Image8 *objMap, Set *seeds, int objmaxval, int threshold);
//}
//
//#endif // _EDITING_H_
