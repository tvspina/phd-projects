#ifndef _CTIME_H_
#define _CTIME_H_

extern "C"
{
#include <stdlib.h>
#include <float.h>
#include <time.h>
}

/* Common data types */
typedef struct timeval timer;

// to measure time

#ifndef _MSC_VER

#include <sys/time.h>
#define TIMING_START(M, ftime) timer TM_start, TM_now, TM_now1;\
  gettimeofday(&TM_start,NULL);\
  TM_now = TM_start;\
  fprintf(ftime,"================================================\nStarting to measure %s (in milliseconds)\n\n",M);
#define TIMING_SECTION(M, ftime) gettimeofday(&TM_now1,NULL);\
  fprintf(ftime,"%f:\tSECTION %s\n",(TM_now1.tv_sec-TM_now.tv_sec)*1000.0 + (TM_now1.tv_usec-TM_now.tv_usec)*0.001,M);\
  TM_now=TM_now1;
#define TIMING_END(M, ftime) gettimeofday(&TM_now1,NULL);\
  fprintf(ftime,"%f:\tSECTION %s\n",(TM_now1.tv_sec-TM_now.tv_sec)*1000.0 + (TM_now1.tv_usec-TM_now.tv_usec)*0.001,M);\
  fprintf(ftime,"\nTotal time: %f\n================================================\n",\
      	 (TM_now1.tv_sec-TM_start.tv_sec)*1000.0 + (TM_now1.tv_usec-TM_start.tv_usec)*0.001);

timer *Tic(); /* It marks the initial time */
timer *Toc(); /* It marks the final time */
float CTime(timer **tic, timer **toc); /* It computes the time difference */

#endif

#endif
