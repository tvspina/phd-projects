#ifndef _USISEVALUATION_H_
#define _USISEVALUATION_H_


#include "usismorphology.h"

//void eval_grad_over_gt_iftsc(Image *grad, Image *gt, const char *imgname, const int nscales, FILE *file);


namespace usis
{
	double dice_similarity(Image8 *mask1, Image8 *mask2);

	double precision(int tp, int fp);

	double recall(int tp, int fn);

	double fmeasure(double precision, double recall);

	double compute_bin_fmeasure(Image8 *label, Image8 *mask);

	void bin_conf_matrix(Image8 *label, Image8 *mask, int *tp, int *tn, int *fp, int *fn,
						Rectangle _bb = Rectangle());

	double compute_fuzzy_gt(Image8 *label, Image8 *mask);

	double grabcut_error_rate(Image8 *label, Image8 *mask);


	double compute_fmeasure(Image8 *label, Image8 *mask);

	void conf_matrix(Image8 *label, Image8 *mask, int *truepos, int *trueneg,
					int *falsepos, int *falseneg, int lb = NIL,
					int lb_mask = NIL, Rectangle _bb = Rectangle());

/// Given a label and a groundtruth, computes the mean squared distance
/// between the groundtruth border and the object's segmentation border,
/// thus representing the mean squared error (positive and negative errors
/// depending on the EDT sign).
/// It also computes the maximum positive and negative distances.
	void sgn_edt2_error_measures(Image8 *label, Image8 *gt, float *max_dist_pos, float *max_dist_neg,
	                                 float *fp_dist_sum, float *fn_dist_sum, int *fp_size, int *fn_size, int *border_size);

	void sgn_edt_error_measures(Image8 *label, Image8 *gt, float *max_dist_pos, float *max_dist_neg,
	                                 float *fp_dist_sum, float *fn_dist_sum, int *fp_size, int *fn_size, int *border_size);

/**
    @param changes image which counts the number of label changes for each pixel
    @param n_si number of internal markers
    @param n_se number of external markers
    @param avg_changes average number of label changes (i.e., (total amount of changes)/(image size))
    @param avg_changed_pixels average number of pixels which suffered a label change (i.e.,
        (number of pixels whose label changed at least once)/(image size))
    @param avg_changes_by_changed_pixels number of changes divided by the number of changed pixels
    @param controllability equals to 1 - avg_changes_by_mk
 **/
	void controllability_measures(Image32 *changes, int niterations,
									float *avg_changes,
									float *avg_changed_pixels,
	                              	float *avg_changes_by_changed_pixels,
	                              	float *controllability,
	                              	int *n_changes, int *n_changed_pixels);

	void num_label_changes(Image32 *changes, int *n_changes, int *n_changed_pixels);

}
#endif

