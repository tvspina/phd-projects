#ifndef _USISMEMORY_H_
#define _USISMEMORY_H_

#include "usiscommon.h"

extern "C"
{
	#include <mm_malloc.h>
	#include "usiscommonsse.h"
}

#define USIS_UDESTROY(data) if(data != NULL && *data != NULL) {free(*data); *data = NULL;}
#define USIS_DELETE(data) if(data != NULL && *data != NULL) {delete *data; *data = NULL;}

namespace usis
{
	template <typename T> T* alloc_array(const int n, int align = USIS_SSE_ALIGNMENT)
	{
		T *mem = (T*)_mm_malloc(n*sizeof(T),align);
		memset(mem, 0,n*sizeof(T));

		return mem;
	}

    int* alloc_int_array(const int n, int align = USIS_SSE_ALIGNMENT);

	float* alloc_float_array(const int n, int align = USIS_SSE_ALIGNMENT);

    double* alloc_double_array(const int n, int align = USIS_SSE_ALIGNMENT);

	uchar* alloc_uchar_array(const int n, int align = USIS_SSE_ALIGNMENT);

	/// frees aligned data
	void free_array_data(void *data);

	/// frees aligned data if not NULL and nullifies pointer *data
	void safe_free_array(void **data);
}


#endif // _USISMEMORY_H_
