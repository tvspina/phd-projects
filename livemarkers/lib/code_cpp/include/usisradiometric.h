#ifndef _USISRADIOMETRIC_H_
#define _USISRADIOMETRIC_H_

extern "C"
{
	#include "usisflags.h"
}

#include "usisdescriptor.h"

#include "usisimage.h"

namespace usis
{
    Image8 *linear_stretch(Image8 *img, Image8::value_type f1, Image8::value_type f2,
                           Image8::value_type g1, Image8::value_type g2);

    void linear_stretch_inplace(Image8 *img, Image8::value_type f1, Image8::value_type f2,
								Image8::value_type g1, Image8::value_type g2);

    Image32 *linear_stretch(Image32 *img, Image32::value_type f1,
                            Image32::value_type f2, Image32::value_type g1,
                            Image32::value_type g2);

    void linear_stretch_inplace(Image32 *img, Image32::value_type f1,
								Image32::value_type f2, Image32::value_type g1,
								Image32::value_type g2);

	void log_stretch(Image32 *img, int fmin, int fmax, int gmin, int gmax);
	void log_stretch(Image8 *img, int fmin, int fmax, int gmin, int gmax);

    /// Sigmoidal rescaling of values.
    /// Since negative values are considered to be inside the signed tde of the mask
    /// then sigma and beta must be negative
    Image8 *sigmoidal_stretch(Image32 *img, int neg_thresh,
                              int pos_thresh, double beta, double sigma,
                              Image8::value_type maxval = UCHAR_MAX);

	/** Setting brighness and contrast **/
	int update_brightness_contrast(int value, int B, int C);

	/// updates brighness and contrast in place
	void update_image_brightness_contrast(Image8 *img, int B, int C);
	void update_image_brightness_contrast(Image32 *img, int B, int C);
	void update_cimage_brightness_contrast(CImage8 *cimg, int B, int C);

    Curve *histogram(Image8 *img);
    Curve *histogram(Image32 *img);

	/// Considers higher values should be assigned to negative x's
    inline double negative_sigmoid(double x, double min, double max,
								  double neg_thresh, double pos_thresh,
								  double beta, double sigma)
    {
        if(x <= neg_thresh)
            return max;
        else if(x >= pos_thresh)
            return min;
        else
            return (max - min) / (1.0 + exp((x - beta) / sigma)) + min;
    }

	/// Considers higher values should be assigned to x values >= high_thresh
    inline double sigmoid(double x, double min, double max,
                          double low_thresh, double high_thresh,
                          double beta, double sigma)
    {
        if(x <= low_thresh)
            return min;
        else if(x >= high_thresh)
            return max;
        else
            return (max - min) / (1.0 + exp(-(x - beta) / sigma)) + min;
    }
}

#endif // _USISRADIOMETRIC_H_
