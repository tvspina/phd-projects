#ifndef _BILATERAL_FILTER_FAST_H_
#define _BILATERAL_FILTER_FAST_H_

#include <iostream>

using namespace std;

void
bilateral_filter (const float * __restrict__ _input,
                  float       * __restrict__ _output,
                  int   width,
                  int   height,
                  int 	channels,
                  int   s_sigma,
                  float r_sigma);

#endif // _BILATERAL-FILTER-FAST_H_
