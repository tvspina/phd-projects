#ifndef _USISSEGMENTATION_H_
#define _USISSEGMENTATION_H_

extern "C"
{
#include "stackandfifo.h"
}

#include <queue>
#include <list>
#include <stack>
#include <bitset>
#include <map>
#include <set>
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <iostream>

#include "usiscommon.h"
#include "realheap.h"
#include "gqueue.h"

#include "usisimage.h"
#include "usisfeatures.h"
#include "usismorphology.h"
#include "usisgeometry.h"

//Max-Flow
#include "graph.h"

#define USIS_DEFAULT_SEG_BKG_LABEL 0
#define USIS_DEFAULT_SEG_OBJ_LABEL 1
#define USIS_MAX_N_SEG_LABELS 100

#define USIS_DESC_SMOOTH_MAXVAL 255

#define USIS_SEED_PAIR_MKID(seed_pair) seed_pair.mk_id
#define USIS_SEED_PAIR_LABEL(seed_pair) seed_pair.lb

#define USIS_SEED_INDEX(iter) iter->first
#define USIS_SEED_MKID(iter) USIS_SEED_PAIR_MKID(iter->second)
#define USIS_SEED_LABEL(iter) USIS_SEED_PAIR_LABEL(iter->second)
#define USIS_SEED_DATA(iter) iter->second

using namespace std;

enum USIS_LW_ORI
{
    USIS_LW_NOORIENTATION=0,
    USIS_LW_CLOCKWISE,
    USIS_LW_COUNTERCLOCKWISE
};


/** Region-based segmentation **/

/** Distance Functions **/

namespace usis
{

	struct MarkerData
	{
		int mk_id, lb;
	};

	typedef tr1::unordered_map<int, MarkerData> MarkerSet;
	typedef vector<pair<int, double> > LMAnchorsRadii;

	/// Segmentation data
	class SegData
	{
		public:
			SegData(bool free = false);
			~SegData();

			Image8  *grad;
			Image32 *cost;
			Image8  *label;
			Image32 *pred;
			Image32 *root;
			Image32 *regions;
			Image8  *objMap;

			Features *feat;
			MarkerSet seeds;
            float *fcost;
            double power;
			int Wmax; // maxmimum object map value
            float ift_radius;

            void SetFreeData(bool free);

		protected:

			bool m_free_data;
	};




	float dist_grad(SegData *data, int p, int q);

	inline float dist_grad(SegData *data, int p, int q)
	{
		return (data->grad->val[p] + data->grad->val[q])/2;
	}

	inline float fmax(SegData *data, float cst, float weight)
	{
        return MAX(cst, weight);
	}

	inline float fsum_power(SegData *data, float cst, float weight)
	{
        return cst + pow(weight, data->power);
	}


    /** Interface for the cloud's delineation algorithm **/
    struct DelineationAlg
    {
    	AdjRel *A;
        SegData *data;

        DelineationAlg();
        virtual ~DelineationAlg();

        virtual double DelineateAndRecognize() = 0;

        virtual void SetBarrier() = 0;
        virtual void SetBarrier(int p, uchar label) = 0;
        virtual void SetSeed(int p, uchar label, int mk_id) = 0;
        virtual void SetUnknownSample(int p) = 0;
        virtual void ResetData() = 0;
        virtual bool ResetData(Pixel px) = 0;
        virtual bool IsSeed(int p) = 0;

        virtual void SetData(SegData *seg_data);
    };

    /** IFT-SC using GQueue pr Realheap

        Because the uncertainty region is fairly small,
        this class may be used with either the Fmax function
        or Fsum raised to a power. The latter provides smoother
        borders.
     **/
    struct DelineationIFT_SC : public DelineationAlg
    {
        GQueue *Q;
        RealHeap *H;
        float *fcost;
        double fsum_power;

        DelineationIFT_SC(int nbuckets, int n, int *cost,
                               float (*cost_function)(SegData*, float, float));

        DelineationIFT_SC(int n, float (*cost_function)(SegData *,float, float),
							double fsum_power);

        virtual ~DelineationIFT_SC();

        virtual double DelineateAndRecognize();

        virtual void SetBarrier();
        virtual void SetBarrier(int p, uchar label);
        virtual void SetSeed(int p, uchar label, int mk_id);
        virtual void SetUnknownSample(int p);

        virtual void SetData(SegData *seg_data);
        virtual void ResetData();
        virtual bool ResetData(Pixel px);

        virtual bool IsSeed(int p);

        protected:
			float (*m_cost_function)(SegData *, float, float);
    };

	/** Region-based Segmentation **/

	/** DIFT **/
	Set *forest_removal(SegData *data, AdjRel *A, MarkerSet delSet);

    /** Delineation with Recognition Functional using GQueue **/
	void differential_ift(SegData *data, AdjRel *A, int nbuckets, MarkerSet delSet,
                            float (*weight_function)(SegData*, int, int),
                            float (*cost_function)(SegData*, float, float));
	void dift(SegData *data, AdjRel *A, MarkerSet delSet, GQueue **Q,
                float (*weight_function)(SegData*, int, int),
                float (*cost_function)(SegData*, float, float));


	Set *forest_removal_heap(SegData *data, AdjRel *A, MarkerSet delSet);
	double differential_ift_heap(SegData *data, AdjRel *A, MarkerSet delSet,
                            float(*weight_function)(SegData*, int, int),
                            float (*cost_function)(SegData*, float, float));
	double dift(SegData *data, AdjRel *A, MarkerSet delSet, RealHeap **Q,
                    float(*weight_function)(SegData*, int, int),
                    float (*cost_function)(SegData*, float, float));

    /** IFT-SC **/
	double ift_sc_and_mean_cut(usis::SegData *data, AdjRel *A,
                               float (*weight_function)(usis::SegData*, int, int),
                               float (*cost_function)(usis::SegData*, float, float));
	double ift_sc_and_mean_cut(usis::SegData *data, AdjRel *A, GQueue **Q,
                               float (*weight_function)(usis::SegData*, int, int),
                               float (*cost_function)(usis::SegData*, float, float));


	/** IFT-SC using heap **/
	double ift_sc_and_mean_cut_heap(usis::SegData *data, AdjRel *A,
									float (*weight_function)(usis::SegData*, int, int),
                                    float (*cost_function)(usis::SegData*, float, float));
	double ift_sc_and_mean_cut(usis::SegData *data, AdjRel *A, RealHeap *Q,
                               float (*weight_function)(usis::SegData*, int, int),
                               float (*cost_function)(usis::SegData*, float, float));


	double mean_cut(Image8 *grad, Image8 *label, Image8::value_type lb, double radius);

	/** Max-Flow/Min-Cut **/
	// Calls the algorithm developed by Yuri Boykov
	// and Vladimir Kolmogorov.
	void max_flow(SegData *data, AdjRel *A, float lambda, float power,
					float (*weight_function)(usis::SegData*, int, int),
					Graph **g = NULL, Graph::node_id **nodes = NULL);


	/** Misc superpixel-based segmentation **/

	void watergray(SegData *data, Image8 *img, Image32 *marker, AdjRel *A);
	void watergray(SegData *data, Image32 *img, Image32 *marker, AdjRel *A);

	typedef set<int> BoundaryRegion;

	void fast_smooth_ift(SegData *data, int niterations,
						float(*WeightFunction)(SegData*, int, int));

	void fast_smooth_ift(SegData *data, BoundaryRegion &region, int niterations,
                          float(*WeightFunction)(SegData*, int, int));

	/** Boundary-based segmentation **/

	/** Live-wire-on-the-fly and Riverbed **/
	typedef tr1::unordered_multimap<int,int> LWInvRootMap;

	int lwof_cost(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
					int p, int q, int Wmax, int Cmax,
					double power, AdjRel *L, AdjRel *R, int i,
					USIS_LW_ORI ori, bool force_ori);

	int riverbed_cost(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
					int p, int q, int Wmax, int Cmax,
					double power, AdjRel *L, AdjRel *R, int i,
					USIS_LW_ORI ori, bool force_ori);


	void contour_tracking(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
    				      	GQueue **Q, AdjRel *A, int Wmax, USIS_LW_ORI ori, int dst,
							double power, bool force_ori,
					        int (*path_cost)(Image8 *grad, Image32 *cost, Image32 *pred, Image8 *obj,
											int p, int q, int Wmax, int Cmax,
											double power, AdjRel *L, AdjRel *R, int i,
											USIS_LW_ORI ori, bool force_ori));

	int *path2array(Image32 *pred, int dst, int src);

	// @param anchors should contain a list of pairs (anchor, radius) where
	// the radius of element i denotes the segment width for the path between
	// i and i-1
	MarkerSet* path_seeds(LMAnchorsRadii &anchors, Image32 *pred, Image32 *cost,
								int primary_label, int left_pixel_label,
								int right_pixel_label, int mk_id,
								LWInvRootMap *root_inv_index);

	void propagate_opened_contour_seeds(Image32* pred, Pixel px_first_node_right,
										Pixel px_first_node_left, Pixel px_dst_right,
										Pixel px_dst_left, MarkerSet *seed_map,
										std::tr1::unordered_set<int> &segment_nodes,
										std::tr1::unordered_set<int> &visited,
										AdjRel *A4, AdjRel *A8,
										LWInvRootMap *root_inv_index,
										LWInvRootMap::iterator &iter,
										int mk_id,
										int left_pixel_label,
										int right_pixel_label);

	// This function first labels one of the connected components
	// corresponding to either the interior or exterior path seeds
	// using a temporary label. Then, it looks for a path node with
	// an 8-neighbor inside and another one outside the contour.
	// By analyzing the angle between the vectors from the selected pixel
	// to its predecessor, next node in the path, and to the neighbor
	// inside the contour we are able to determine the neighbors'
	// correct labels.
	void closed_contour_seeds(int path_dst, int first_node,
								Image32 *pred, MarkerSet *seed_map,
								std::tr1::unordered_set<int> &segment_nodes,
								std::tr1::unordered_set<int> &visited,
								AdjRel *A4, AdjRel *A8,
								LWInvRootMap *root_inv_index,
								LWInvRootMap::iterator &iter,
								int left_pixel_label,
								int right_pixel_label);

	void propagate_seed_labels(Image32 *pred, MarkerSet *seed_map,
								std::tr1::unordered_set<int> &segment_nodes,
								std::tr1::unordered_set<int> &visited,
								std::queue<int> &Q, AdjRel *A4,
								LWInvRootMap *root_inv_index,
								LWInvRootMap::iterator &iter);

    /** Returns @param dst UNION @param src inplace in dst, where the only
        pixel values to be copied from src to dst are those whose label is
        @param label.
     **/
    void label_union(Image8 *dst, Image8 *src, int label);
	void intersect_labels(Image8 *label1, Image8 *label2, int value);
	void intersect_labels_bb(Image8 *label1, Image8 *label2, int value, int xmin,
                             int ymin, int xmax, int ymax);

	/** Util **/

	Image8* threshold(Image8 *img, Image8::value_type lower, Image8::value_type higher,
						Image8::value_type value = 1);
	Image32* threshold(Image32 *img, int lower, int higher, Image32::value_type value = 1);
    Image8 *threshold(DbImage *img, DbImage::value_type lower,
                              DbImage::value_type higher,
                              Image8::value_type value = 1);

	Image8* binarize(Image8 *img, set<uchar> &values);
	Image32* binarize(Image8 *img, set<int> &values);

	Image32 *label_bin_comp(Image8 *bin, AdjRel *A);
	Image32 *label_bin_comp(Image32 *bin, AdjRel *A);

	void error_components(Image8 *label, Image8 *gt, Image8 *errors);

	void relabel_regions(Image32 *regions);

	vector<int*> image_contours(Image8 *bin, int thrsh);
	Image32* label_contours(Image8 *bin, int thresh);

	// This function will add marker seeds with label as in the label image,
	// restrained to the positive pixels in mask.
	MarkerSet seeds_from_label(Image8 *label, Image8 * mask, int mk_id);
	MarkerSet seeds_from_medial_axis_transform(Image8 *label, int lb, double skiz_perc,
											double axis_dilation_dist_perc, int mk_id);
	MarkerSet seeds_from_label_erosion_or_skel(Image8 *label, bool from_skeleton, float inner_radius,
												float outer_radius, double skeleton_scale,
												int lb, int mk_id, int bkg_lb, int bkg_mk_id);
}

#endif // _CPPSEGMENTATION_H_
