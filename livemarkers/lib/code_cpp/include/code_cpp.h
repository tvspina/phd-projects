
#ifndef _CODE_CPP_H_
#define _CODE_CPP_H_

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <memory>
#include <cctype>
#include <string>

#include "adjacency.h"
#include "color.h"
#include "comptime.h"
#include "OPF.h"
#include "set.h"
#include "gqueue.h"
#include "realheap.h"
#include "subgraph.h"

#include "usiscommon.h"
#include "appdata.h"
#include "appvideodata.h"
#include "filehandling.h"
#include "softwareversion.h"
#include "usissegmentation.h"
#include "framebuffer.h"
#include "usismemory.h"
#include "usisimage.h"
#include "usisfeatures.h"
#include "usisgradient.h"
#include "usisradiometric.h"
#include "usisediting.h"
#include "usisdraw.h"
#include "usisview.h"
#include "usisfuzzyopf.h"
#include "usismorphology.h"
#include "usisevaluation.h"
#include "usisgeometry.h"
#include "usisclouds.h"
#include "usisbody.h"
#include "usismath.h"
#include "usisdescriptor.h"
#include "usisoptimization.h"
#include "usisprivacyprotection.h"
#include "usisnewiftinterface.h"
#include "cloudtrace.h"


#ifdef USIS_EXPERIMENT_MODE
    extern string USIS_EXP_CUR_MOD_NAME;
#endif


#endif

