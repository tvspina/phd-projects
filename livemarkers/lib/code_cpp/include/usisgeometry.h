#ifndef _USISGEOMETRY_H_
#define _USISGEOMETRY_H_

#include "common.h"
#include "usisimage.h"
#include "adjacency.h"
#include "usisfeatures.h"
#include <tr1/unordered_set>

#include <iostream>
#include <functional>
#include <numeric>
#include <list>
#include <set>
#include <map>
#include <queue>

using namespace std;
using namespace std::tr1;

namespace usis
{
	struct Vector2D
	{
		double x;
		double y;

        Vector2D();

        Vector2D(double x, double y);
        Vector2D(Pixel px, Pixel center);

        bool operator == (Vector2D &cmp);
        bool operator != (Vector2D &cmp);

        void operator *= (double val);
        void operator /= (double val);
        Vector2D operator * (double val);
        Vector2D operator / (double val);
        Vector2D operator + (const Vector2D &vec1);
        Vector2D operator + (double val);
        Vector2D operator - (const Vector2D &vec1);
        Vector2D operator - (double val);

        /** Operator overload for the dot product **/
        double operator * (const Vector2D &vec1);

        double Norm();
        Vector2D Normalize();

        /** @return the angle [0, M_PI] between this vector and @param vec. **/
        double Angle(Vector2D &vec);
        /** @return the global direction [0, 2*M_PI] of this vector w.r.t.,
			the origin vector (default (1,0)).
		 **/
        double Direction(Vector2D origin = Vector2D(1.0,0.0));
        /** @return the direction {1,-1} of vector @param vec w.r.t this one
            if they are concurrent, {0} otherwise.
         **/
        int Concurrent(Vector2D &vec);


        Pixel ToPixel(Pixel center);
        int ToIndex(Pixel center, usis::Image8 *img);
        int ToIndex(Pixel center, usis::Image32 *img);
	};

    struct Point3D{
        double x;
        double y;
        double z;
    };

	Pixel mask_centroid(Image8 *mask, int label = NIL);
	Pixel mask_centroid(Image32 *mask, int label = NIL);

    /** Affine transforms **/
    Vector2D transform_vector(float M[4][4], Vector2D p, bool round = false);
    Pixel transform_point(float M[4][4], Pixel px);
	Point3D transform_point(float M[4][4], Point3D p);

	Image8 *transform_image(Image8 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels = NULL);
	void transform_image(Image8 *src, Image8 *out, float M[4][4],
							Image8 **selected_pixels = NULL);

	Image32 *transform_image(Image32 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels = NULL);
	void transform_image(Image32 *src, Image32 *dst, float M[4][4],
							Image8 **selected_pixels = NULL);

	Image32 *transform_image_direct(Image32 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels = NULL);
	void transform_image_direct(Image32 *src, Image32 *dst, float M[4][4],
							Image8 **selected_pixels = NULL);

	CImage8 *transform_image(CImage8 *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels = NULL);
	void transform_image(CImage8 *src, CImage8 *out, float M[4][4],
							Image8 **selected_pixels = NULL);

	Features *transform_image(Features *img, float M[4][4], int ncols, int nrows,
							Image8 **selected_pixels = NULL);
	void transform_image(Features *src, Features *out, float M[4][4],
							Image8 **selected_pixels = NULL);


    /** Matrix operations **/
    void inverse(float M[4][4], float IM[4][4]);
    float cofactor(float M[4][4], int l, int c);
    void trans_matrix(float M1[4][4],float M2[4][4]);
    void translation(float T[4][4], float dx, float dy, float dz);
    void rot_x(float Rx[4][4], float thx);
    void rot_y(float Ry[4][4], float thy);
    void rot_z(float Rz[4][4], float thz);
    void scl_matrix(float S[4][4], float sx, float sy, float sz);
    void mult_matrices(float M1[4][4],float M2[4][4], float M3[4][4]);

	void scl_rot_trans(float sx, float sy, float th, float dx,float dy,float T[4][4],
                       int x_in, int y_in, int x_out, int y_out, bool direct = false);

	void scl_rot_axis(float sx, float sy, float th, float th_axis,
                      float T[4][4], int x_in, int y_in, int x_out, int y_out,
                      bool direct);

	/** IMPORTANT NOTE: This function guarantees that
		the original center will be transported to the
		transformed image's center. This is true because
		we take the maximum absolute transformed (x,y) corner
		values	w.r.t. the original center and using them (plus padding)
		as half the width/height of the new image.
	**/
	void transformed_dimensions(Rectangle bb, Pixel px_center, float T[4][4],
									int *final_w, int *final_h, int padding);

	void mask_bounding_box(Image8 *mask, Pixel *beg, Pixel *end, int val = NIL);
	void mask_bounding_box(Image32 *mask, Pixel *beg, Pixel *end, int val = NIL);

	void mask_bounding_box(Image8 *mask, Pixel *beg, Pixel *end, set<int> val);
	void mask_bounding_box(Image32 *mask, Pixel *beg, Pixel *end, set<int> val);

	void mask_bounding_box(Image8 *mask, int *pmin, int *pmax, int val = NIL);
	void mask_bounding_box(Image32 *mask, int *pmin, int *pmax, int val = NIL);

	void mask_bounding_box(Image8 *mask, int *pmin, int *pmax, set<int> *values);
	void mask_bounding_box(Image32 *mask, int *pmin, int *pmax, set<int> *values);

	Image8* mask_bounding_box(Image8 *mask, int val = NIL);
	Image32* mask_bounding_box(Image32 *mask, int val = NIL);

	inline Rectangle mask_bb(Image8 *mask, int val = NIL)
	{
		Rectangle bb;
		mask_bounding_box(mask, &(bb.beg), &(bb.end), val);

		return bb;
	}

	inline Rectangle mask_bb(Image32 *mask, int val = NIL)
	{
		Rectangle bb;
		mask_bounding_box(mask, &(bb.beg), &(bb.end), val);

		return bb;
	}

	inline Rectangle mask_bb(Image8 *mask, set<int> val)
	{
		Rectangle bb;
		mask_bounding_box(mask, &(bb.beg), &(bb.end), val);

		return bb;
	}

	inline Rectangle mask_bb(Image32 *mask, set<int> val)
	{
		Rectangle bb;
		mask_bounding_box(mask, &(bb.beg), &(bb.end), val);

		return bb;
	}


	template <typename T, typename U> T* ROI(T *img, Rectangle _bb)
	{
		Rectangle bb = Rectangle(MAX(_bb.beg.x, 0), MAX(_bb.beg.y, 0),
								MIN((_bb.end.x <= 0) ? img->ncols-1 : _bb.end.x, img->ncols-1),
								MIN((_bb.end.y <= 0) ? img->nrows-1 : _bb.end.y, img->nrows-1));

		T* cropped = create_image<T,U>(bb.end.x-bb.beg.x+1, bb.end.y-bb.beg.y+1);

		for(int y = bb.beg.y; y <= bb.end.y; y++)
			for(int x = bb.beg.x; x <= bb.end.x; x++)
			{
				int p0 = to_index(cropped, x-bb.beg.x, y-bb.beg.y);
				int p = to_index(img, x,y);

				(*cropped)[p0] = (*img)[p];
			}

		return cropped;
	}

	Image8 *ROI(Image8 *img, int xl, int yl, int xr, int yr);
	CImage8 *ROI(CImage8 *img, int xl, int yl, int xr, int yr);

	Image32 *ROI(Image32 *img, int xl, int yl, int xr, int yr);

	Features *ROI(Features *f, Rectangle _bb);

	int mask_area(Image8 *mask);
	int mask_area(Image32 *mask);
	int label_area(Image8 *mask, int label);
	int label_area(Image32 *mask, int label);

	map<int, int> label_areas(Image32 *mask);

	list<int> component_connected_to_pixel(Image32 *mask, Pixel px, int val,
											AdjRel *A);

    /** The following function DOES NOT VERIFY IF
        TWO LINES ARE PARALLEL
        @param p1,p2 are points on the lines
        @param a1,a2 are the lines' tangents.
        @return the intersection point (x_prime, y_prime)
     **/
    void line_intersection(Pixel p1, double a1, Pixel p2, double a2,
                           double *x_prime, double *y_prime);

    /** @param  degree_epsilon is the epsilon used to determine if angle
        is close to M_PI/2 or -M_PI/2
     **/
    double point_dist_2_line(Pixel center, double angle, Pixel px,
                     double degree_epsilon = 0.5);

    double point_set_diameter(std::tr1::unordered_set<int> &point_set,
                              usis::Image8 *mask, Pixel *px1, Pixel *px2);

    std::pair<double, double> point_set_variance(std::list<Pixel> &point_set);
    std::pair<double, double> point_set_variance(std::list<Vector2D> &point_set);


    inline double degrees(double rad)
    {
        return rad*180.0/M_PI;
    }

    inline double radians(double deg)
    {
        return deg*M_PI/180.0;
    }

    inline double angle_2_upper_quadrants(double angle, double sin_angle)
    {
        if(sin_angle < 0.0)
            return (angle >= 0.0) ? angle - M_PI : M_PI + angle;

        return angle;
    }

    inline double angle_2_lower_quadrants(double angle, double sin_angle)
    {
        if(sin_angle > 0.0)
            return M_PI + angle;

        return angle;
    }

    /** Function inlining **/

    inline Pixel Vector2D::ToPixel(Pixel center)
	{
	    Pixel px;
	    px.x = center.x + this->x;
	    px.y = center.y + this->y;

	    return px;
	}

	inline int Vector2D::ToIndex(Pixel center, Image8 *img)
	{
	    return usis::to_index(img,center.x + this->x,center.y + this->y);
	}

	inline int Vector2D::ToIndex(Pixel center, Image32 *img)
	{
	    return usis::to_index(img,center.x + this->x,center.y + this->y);
	}
}

#endif // _USISGEOMETRY_H_
