#ifndef _CPPCOMMON_H_
#define _CPPCOMMON_H_

extern "C"
{
	#include <stdlib.h>
	#include <stdio.h>
	#include <math.h>
	#include <string.h>
	#include <limits.h>
	#include <float.h>
}

#include <boost/random/mersenne_twister.hpp>

// Checking boost minor version to determine which header to include
#if ((BOOST_VERSION / 100) % 1000) >= 50
	#include <boost/random/uniform_int_distribution.hpp>
#else
	#include <boost/random/uniform_int.hpp>
#endif


#include <ctime>

/* Error messages */

#define MSG1  "Cannot allocate memory space"
#define MSG2  "Cannot open file"
#define MSG3  "Invalid option"

/* Common definitions */

#define INTERIOR    0
#define EXTERIOR    1
#define BOTH        2
#define WHITE       0
#define GRAY        1
#define BLACK       2
#define NIL        -1
#define INCREASING  1
#define DECREASING  0
#define Epsilon     1E-05

/* Common operations */

#ifndef MAX
#define MAX(x,y) (((x) > (y))?(x):(y))
#endif

#ifndef MIN
#define MIN(x,y) (((x) < (y))?(x):(y))
#endif

#ifndef SIGN
#define SIGN(x) (((x) >= 0)?1:-1)
#endif

#ifndef ROUND
#define ROUND(x) (((x) < 0)?(int)((x)-0.5):(int)((x)+0.5))
#endif

struct Pixel
{
	int x, y;
};

struct Rectangle
{
	Pixel beg;
	Pixel end;

	Rectangle();
	Rectangle(int x0, int y0, int xf, int yf);
	Rectangle(Pixel px0, Pixel pxf);

	bool InRectangle(Pixel px);
};

/* Common data types to all programs */

#ifndef ushort
typedef unsigned short ushort;
#endif
#ifndef uint
typedef unsigned int uint;
#endif

#ifndef uchar
typedef unsigned char uchar;
#endif

int    *AllocIntArray(int n);   /* It allocates 1D array of n integers */
float  *AllocFloatArray(int n); /* It allocates 1D array of n floats */
char   *AllocCharArray(int n);  /* It allocates 1D array of n characters */
uchar  *AllocUCharArray(int n);  /* It allocates 1D array of n characters */
ushort *AllocUShortArray(int n);  /* It allocates 1D array of n ushorts */
uint   *AllocUIntArray(int n); /* It allocates 1D array of n uints */
double *AllocDoubleArray(int n);/* It allocates 1D array of n doubles */

void Error(const char *msg, const char *func); /* It prints error message and exits
                                     the program. */
void Warning(const char *msg, const char *func); /* It prints warning message and
                                       leaves the routine. */

inline void change(int *a, int *b)
{ /* It changes content between a and b */
  const int c = *a;
  *a = *b;
  *b = c;
}

int random_integer(int low, int high, void *random_generator);

void* random_seed();

void destroy_random_generator(void **generator);

int ncf_gets(char *s, int m, FILE *f); /* It skips # comments */



inline bool Rectangle :: InRectangle(Pixel px)
{
	return px.x >= this->beg.x && px.x <= this->end.x
			&& px.y >= this->beg.y && px.y <= this->end.y;
}


template <typename T> void shift_array(int R, T *arr, int n)
{
    if(R < 0)
        R = n+R;

    if (R == n)
        return;

    int cnt = 0;
    int idx = 0;
    T prv = arr[idx];

    for (int i = 0; i < n; i++)
    {
        T tmp = arr[(idx + R) % n];
        arr[(idx + R) % n] = prv;

        prv = tmp;


        idx = (idx + R) % n;
        cnt += R;
        if ((cnt % n) == 0)
        {
            cnt = 0;
            idx = (idx + 1) % n;
            prv = arr[idx];
        }

    }
}

#endif
