#ifndef _SET_H_
#define _SET_H_

struct Set {
  int elem;
  Set *next;
};

void InsertSet(Set **S, int elem);
int  RemoveSet(Set **S);
void  RemoveSetElem(Set **S, int elem);

int  GetSetSize(Set *S);
Set *CloneSet(Set *S);
void DestroySet(Set **S);

void MergeSets(Set **S, Set **T);
bool IsInSet(Set *S, int elem);

#endif
