#ifndef _USISVIEW_HPP_
#define _USISVIEW_HPP_

#include "usisimage.h"
#include "usisdraw.h"
#include "usissegmentation.h"

namespace usis
{
	CImage8 *cwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, bool fill);
	CImage8 *cwide_highlight(CImage8 *cimg, Image32 *regions, float radius, int color, bool fill);
	CImage8 *cb_wide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, int bkgcolor, bool fill);

	CImage8 *cwide_highlight_lb(CImage8 *cimg, Image8 *label, float radius, int *colormap, bool fill);
	CImage8 *highlight_obj_bkg_lb(CImage8 *cimg, Image8 *objlabel, Image8 *bkglabel,
								float radius, int objcolor, int bkgcolor, bool fill);
	CImage8 *highlight_lb_n_bkg_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
										float radius, int objcolor, int bkgcolor, bool fill);
	CImage8 *wb_highlight_lb_n_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
										float radius, int objcolor, int bkgcolor, bool fill);


	/** In-place functions **/
	void icwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, bool fill);
	void icwide_highlight(CImage8 *cimg, Image32 *label, float radius, int color, bool fill);

	void icwide_highlight_bin_mask(CImage8 *cimg, Image8 *gt, float radius, int color, bool fill);

	void icwide_highlight_lb(CImage8 *cimg, Image8 *label,
							float radius, int *colormap, bool fill);
	void icwide_highlight_lb(CImage8 *cimg, Image32 *label,
							float radius, int *colormap, bool fill);

	void icwide_highlight_bkg(CImage8 *cimg, Image8 *label,
							float radius, int *colormap, bool border,
							float alpha = 0.3);
	void icwide_highlight_lb_border_desc(CImage8 *cimg, Image8 *label, Image32* desc,
										float radius, int *colormap,  bool fill);
	void icwide_highlight_lb_border_desc_gray(CImage8 *cimg, Image8 *label, Image32* desc,
											float radius, int *colormap, bool fill);

	void icbwide_highlight(CImage8 *cimg, Image8 *label, float radius, int color, int bkgcolor, bool fill);
	/// combines cimg with the label colors using an alpha value
	void icfill_lb(CImage8 *cimg, Image8 *label, int *colormap, int sel_label = NIL, float alpha = 0.7);

	void ihighlight_lb_n_bkg_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
									float radius, int objcolor, int bkgcolor, bool fill);
	void ihighlight_lb_n_regions(CImage8 *cimg, Image8 *label, Image32 *regions,
								float radius, int* colormap, bool fill);

	/** Tree-pruning related functions for visualization **/
	void draw_leak_points(CImage8 *cimg, Set *leak, float radius, int color);

}

#endif

