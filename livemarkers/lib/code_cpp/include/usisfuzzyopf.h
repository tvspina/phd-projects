#ifndef _USISFUZZYOPF_H_
#define _USISFUZZYOPF_H_

extern "C"
{
#include <pthread.h>
}

#include "usiscommon.h"
#include "OPF.h"

#include "usisimage.h"
#include "usisgeometry.h"
#include "usisdescriptor.h"
#include "usisfeatures.h"

#include <queue>

#define USIS_OPFOBJLABEL 2
#define USIS_OPFBKGLABEL 1

#define USIS_MAX_ARC_WEIGHT_PERC 0.002

namespace usis
{
	struct TOPFData
	{
		Subgraph *sg;
		Features *f;
		DbImage *pvalmap;
		Image8 *regions;
		Image32 *mask;
		float **features;    // aligned data for SSE intrinsics
		float *pathval;      // aligned data for SSE intrinsics
		int *sel_regions;    // regions where the map should be computed (sel_regions[i] > 0)
		int *sel_mask;
		int label;
		int beg;
		int end;
	};

    int count_nodes_by_truelabel(Subgraph* sg, int label);

    Subgraph* induced_subgraph(Subgraph* sg, set<int> nodes);
    Subgraph* induced_subgraph_by_truelabel(Subgraph* sg, set<int> label);
    void set_subgraph_features(Subgraph *sg, Features *f);

	/** Creates subgraph from samples selected in a uniform grid.
		@param mask if not NULL the samples are going to be selected only from pixels where mask->val[p] != 0.
		@param bb bounding box used to further contrain the region from where pixels are sampled, by default
		the whole image is considered.
	 */
	Subgraph *uniform_sampling(Image32 *img, Image32 *mask, int dx, int dy, Rectangle bb = Rectangle());

	/** Computes a new membership map using MAXVAL*(d2+epsilon)/(d2+d1+2*epsilon)*/
	Image8 *membership_map(DbImage *d1, DbImage *d2, double max_arc_weight,
	                       Rectangle bb = Rectangle());

	/** Computes a new membership map in-place. */
	void membership_map(Image8 *map, DbImage *d1, DbImage *d2, double max_arc_weight,
	                    Rectangle bb = Rectangle());

	/** Computes a new membership map using MAXVAL*d1/d2 **/
	void membership_map(Image8 *map, DbImage *d1, DbImage *d2, Rectangle bb = Rectangle());

	/** Computes the membership map constrained to the pixels from the selected regions.
		For regions which were not selected nothing is done.
	 */
	void membership_map_by_regions(Image8 *map, Image8 *regions, int *sel_regions,
	                               DbImage *d1, DbImage *d2, double max_arc_weight);

	/** Fuzzy OPF **/

	/** Computes a graph from sets Si and Se, then executes the supervised OPF learning */
	void fopf_on_markers(Features *f, Set *Si, Set *Se, Subgraph **sgtrainobj,
						Subgraph **sgtrainbkg, int nsamples, double max_arc_weight,
						void *random_generator);

	/** Functions to perform Fuzzy OPF learning by evaluating
		the contrast between the membership map values of
		object and background seeds.
	 */

	//double fopf_contrast_classify(Subgraph *sgtrainobj, Subgraph *sgtrainbkg, Subgraph *sg);
	double fopf_contrast_classify(Subgraph *sgtrain, Subgraph *sg);

	void fopf_contrast_learning(Subgraph *sg, Subgraph **sgtrainobj, Subgraph **sgtrainbkg,
								float perc, void *random_generator);

	/** Object Membership Map computation **/

	/** Using clusters **/

	/** Performs clustering on markers then filters them according to a purity criterion
		@return run time of the marker selection operation
	 */
	float filter_sg_by_clustering_markers(Features *f, Set **Si, Set **Se,
											int lenSi, int lenSe,
											Subgraph **sgfiltered, int nsamples,
											void *random_generator);

	float filter_sg_by_clustering(Subgraph *sg, Subgraph **sgfiltered);

	Subgraph *clustering_outliers(Subgraph *sgtrain, Subgraph *sgtest);
	Subgraph *filter_sg_by_clust_outliers(Subgraph *sgtrain, Subgraph *sgtest);

	/** Computes a filtered subgraph from marker clustering then trains a Fuzzy OPF classifier
		@return run time of the marker selection operation
	 */
	float fopf_on_clustered_markers(Features *f, Set **Si, Set **Se, int lenSi,
									int lenSe, Subgraph **sgtrainobj,
									Subgraph **sgtrainbkg, int nsamples,
									double max_arc_weight,
									void *random_generator);
	/** Eliminates unnecessary samples by filtering markers according to a clustered Image32*,
		then computes a new graph and executes the supervised OPF learning
		@return run time of the marker selection operation
	 */
	float fopf_by_clustered_regions(Features *f, Image32 *clusters, Set **Si, Set **Se, int lenSi, int lenSe,
	                                Subgraph **sgtrainobj, Subgraph **sgtrainbkg, int nsamples,
	                                double max_arc_weight, void *random_generator);


	/** Filter cluster according to a purity threshold (i.e. only clusters with more then size(cluster)*threshold
	 	nodes will be kept)
	 */
	Subgraph *filter_clusters_by_truelabel(Subgraph *sg, int ntruelabels, int preferred_label, float threshold);

	Subgraph* train_prot_classifier(Subgraph *sg, float kmin_perc,
										float kmax_perc);
	bool train_prot_fuzzy_classifier(Subgraph *sg_train_internal,
										Subgraph *sg_train_external,
										float kmin_perc,
										float kmax_perc,
										Subgraph **sg_internal_prot,
										Subgraph **sg_external_prot);

	double prot_fuzzy_classify_sample(float *feat,
										Subgraph *int_prot,
										Subgraph *ext_prot);

    // Executes the learning procedure for CompGraph replacing the
    // missclassified samples in the evaluation set by non prototypes from
    // training set
	void prot_contrast_learning(Subgraph *sg, Subgraph **sgtrainobj,
								Subgraph **sgtrainbkg, float split_perc,
								float kmin_perc, float kmax_perc,
								void *random_generator);


	/** Returns true if at least one foreground or background marker has a percentage
		of its pixels selected on pixels which were misclassified (i.e. if the membership value
		is less than USIS_MAX_OBJMAP_VALUE/2 for an object seed or greater than USIS_MAX_OBJMAP_VALUE/2
		for a background seed)
	 */
	void assess_relearning(Image32 *objMap, Set **Si, Set **Se, int lenSi, int lenSe, float mksize_threshold,
	                       int n_int_seeds, int n_ext_seeds, float nseeds_threshold, bool *objClass, bool *bkgClass);

	/** Subgraph **/
	Set *subsample_markers(Set **S, int len, int *ntotmarkers, float perc,
							void *random_generator);

	/** Region Graph **/
	Subgraph *subgraph_from_regions(Image32 *regions);
	void set_region_graph_feats(Subgraph *sg, Image32 *regions,  Features *f);
	void prot_fuzzy_classify_regions(Subgraph *sg, Subgraph *int_prot, Subgraph *ext_prot,
									Image32 *regions, Features *f, DbImage *membership_map);

	Subgraph *subgraph_from_set(Features *f, Set *S, int truelabel);
	Subgraph *subgraph_from_markers(Features *f, Set *Si, Set *Se);
	Subgraph *subgraph_from_markers(Features *f, Set **Si, Set **Se, int lenSi, int lenSe);
	Subgraph *subgraph_from_markers(Features *f, Set **Si, Set **Se, int lenSi, int lenSe, float perc,
									void *random_generator);

	//Move misclassified nodes from source graph (src) to destiny graph (dst)
	void move_misclassified_labeled_nodes(Subgraph **src, Subgraph **dst,  int *p);

	/** Replace errors from the evaluation set by non prototypes from the training set
		Note that all nodes in this training set have the same label
	 */
	void swap_errors_by_labeled_non_prot(Subgraph **sgtrain, Subgraph **sgeval,
										void *random_generator);
	/** Replace errors from the evaluation set by the least important labeled
		non prototypes from the training set
		Note that all nodes in this training set have the same label
	 */
	void swap_errors_by_least_import_lb_non_prot(Subgraph **sgtrain, Subgraph **sgeval);

	/** OPF Clustering **/

	Image32 *image_class_knn_graph(Subgraph *sg, Features *f);
	void image_class_knn_graph(Subgraph *sg, Features *f, Image32 *clusters, Rectangle bb = Rectangle());
	Image32 *image_opf_clustering(Features *f, Subgraph *sg, int kmin, int kmax, int area);

	DbImage *propagate_pdf(Subgraph *sg, Features *f, Rectangle bb = Rectangle());

	/** Every pixel in S is verified to determine if it
		could be classified by any sample in sg considering
		the node's radius (i.e., clustering). If a given pixel
		is not within any radii then it is an outlier.
	 */
	Set *detect_outliers_in_set(Subgraph *sg, Set *S, Features *f);


	/** Returns true if at least one foreground or background marker has a percentage
		of its pixels selected on pixels which were misclassified (i.e. if the membership value
		is less than USIS_MAX_OBJMAP_VALUE/2 for an object seed or greater than USIS_MAX_OBJMAP_VALUE/2
		for a background seed)
	 */
	void assess_relearning(usis::Image8 *objMap, Set **Si, Set **Se, int lenSi, int lenSe, float mksize_threshold,
	                       int n_int_seeds, int n_ext_seeds, float nseeds_threshold, bool *objClass, bool *bkgClass);


	/** SSE **/

	/**	Creates an aligned float matrix with dimensions of NFEATSx[(NNODES+(4-NNODES%4)]
		and assigns the value of @param padding for the extra cells.
		The new number of columns is returned in *n
	 */
	float **align_sg_feats(Subgraph *sg, float padding, int *n);

	void destroy_aligned_feats(float *** algn, int nfeats);

	/** Threaded SSE **/

	void *tfopf_pathval_map_sse(void *data);
	//void* tfopf_pathval_map_sse2(void* data);
	Image8 *tfopf_membership_map_sse(Subgraph *sgtrainobj, Subgraph *sgtrainbkg, Features *f, int nthreads);
	void *tfopf_pathval_map_by_regions_sse(void *data);

	void tfopf_membership_map_by_regions_sse(Subgraph *sgtrainobj, Subgraph *sgtrainbkg,
	        Features *f, Image8 *objMap, Image8 *regions, int *sel_regions, int nthreads);


}


#endif


