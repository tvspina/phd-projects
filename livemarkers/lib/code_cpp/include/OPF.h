#ifndef _OPF_H_
#define _OPF_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <time.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#include <sys/time.h>
#include <time.h>

#include "common.h"
#include "set.h"
#include "gqueue.h"
#include "subgraph.h"
#include "realheap.h"

/*--------- Common definitions --------- */
#define opf_MAXARCW			100000.0
#define opf_MAXDENS			1000.0
#define opf_PROTOTYPE		1

typedef float (*opf_ArcWeightFun)(float *f1, float *f2, int n);

extern float *opf_FeatWeight;

extern opf_ArcWeightFun opf_ArcWeight;

extern char	opf_PrecomputedDistance;
extern float  **opf_DistanceValue;

/*--------- Supervised OPF with complete graph -----------------------*/
void opf_OPFTraining(Subgraph *sg); //Training function
void opf_OPFClassifying(Subgraph *sgtrain, Subgraph *sg); //Classification function: it simply classifies samples from sg

/*--------- Unsupervised OPF -------------------------------------*/
void  opf_OPFClustering(Subgraph *sg); //Training function: it computes unsupervised training for the pre-computed best k.

/*------------ Auxiliary functions ------------------------------ */
void opf_SwapErrorsbyNonPrototypes(Subgraph **sgtrain, Subgraph **sgeval,
									void *random_generator); //Replace errors from evaluating set by non prototypes from training set

void opf_MSTPrototypes(Subgraph *sg); //Find prototypes by the MST approach
void opf_SplitSubgraph(Subgraph *sg, Subgraph **sg1, Subgraph **sg2, float perc1,
					void *random_generator); //Split subgraph into two parts such that the size of the first part  is given by a percentual of samples.
Subgraph *opf_MergeSubgraph(Subgraph *sg1, Subgraph *sg2); //Merge two subgraphs
float opf_Accuracy(Subgraph *g); //Compute accuracy
float opf_NormalizedCut( Subgraph *sg );
void  opf_BestkMinCut(Subgraph *sg, int kmin, int kmax);
void  opf_CreateArcs(Subgraph *sg, int knn); //it creates arcs for each node (adjacency relation)
void  opf_DestroyArcs(Subgraph *sg); //it destroys the adjacency relation
void  opf_PDF(Subgraph *sg); //it computes the PDf for each node

/*------------ Distance functions ------------------------------ */
float opf_EuclDist(float *f1, float *f2, int n); //Computes Euclidean distance between feature vectors
float opf_EuclDistLog(float *f1, float *f2, int n); //Computes Euclidean distance between feature vectors and applies
                                                    //logarithmic function
/* -------- Auxiliary functions used to optimize BestkMinCut -------- */
float* opf_CreateArcs2(Subgraph *sg, int kmax); //Creates arcs for each node (adjacency relation) and returns
                                               //the maximum distances for each k=1,2,...,kmax
void opf_OPFClusteringToKmax(Subgraph *sg); //OPFClustering computation only for sg->bestk neighbors
void opf_RemovePlateauNeighbors(Subgraph *sg);//Removes plateau neighbors that were added by OPFClusteringToKmax (this is a PATCH to fix a plateau issue during BestkMinCut, Thiago V. Spina, 26/08/2011)
void opf_PDFtoKmax(Subgraph *sg); //PDF computation only for sg->bestk neighbors
float opf_NormalizedCutToKmax( Subgraph *sg ); //Normalized cut computed only for sg->bestk neighbors
#endif
