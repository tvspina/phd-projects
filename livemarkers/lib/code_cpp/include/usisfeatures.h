#ifndef _USISFEATURES_H_
#define _USISFEATURES_H_

#include "usiscommon.h"

#include "OPF.h"

#include "usisimage.h"
#include "subgraph.h"

#include "bilateral-filter-fast.h"

/** Features **/


struct FElem
{
	float *feat;
};

struct Features
{
	FElem *elem;
	int  nfeats;
	int  nelems;
	int  nrows, ncols;
	float Fmax;
	float Fmin;

	float *data;
};


Features* CreateFeatures(int ncols, int nrows, int nfeats);
void DestroyFeatures(Features **f);

Features* CopyFeatures(Features* feat);
usis::DbImage* GetFeature(Features* feats, int index);
int SetFeature(Features* feats, int index, usis::DbImage* imagefeats);

/*normalize features*/
void MeanStdNormalizeFeatures(Features *f);

void SetSubgraphFeatures(Subgraph *sg, Features *f);


namespace usis
{
	float max_arcweight(Features *f);

	/// Pthread
	Features *ms_gauss_cimage8_feats_threaded(CImage8 *cimg, int nscales, int nthreads);

	/// OMP support
	Features *lab_feats(Features *rgb);
	void lab_feats(Features *lab, Features *rgb);
	Features *lab_feats(CImage8 *rgb);

	void scale_lab_feats(Features *lab);

    Features* ycbcr_feats(CImage8* rgb);
    void ycbcr_feats(Features* rgb, Features *ycbcr);

	Features* ycbcr_feats(Features* rgb);
	void ycbcr_feats(CImage8* rgb, Features *ycbcr);

	Features *ms_gauss_image8_feats(Image8 *img, int nscales);
	Features *ms_gauss_cimage8_feats(CImage8 *cimg, int nscales);

	Features *bilateral_filter_feats(CImage8 *rgb, int   s_sigma, float r_sigma);


	inline bool valid_pixel(Features* feat, Pixel px)
	{
		return px.x >=0 && px.x < feat->ncols && px.y >=0 && px.y < feat->nrows;
	}

	inline bool valid_pixel(Features* feat, int x, int y)
	{
		return x >=0 && x < feat->ncols && y >=0 && y < feat->nrows;
	}

	inline int to_index(Features* feat, int x, int y)
	{
		return x + y*feat->ncols;
	}

	inline int to_index(Features* feat, Pixel px)
	{
		return px.x + px.y*feat->ncols;
	}

	inline Pixel to_pixel(Features* feat, int p)
	{
		return (Pixel){p % feat->ncols, p / feat->ncols};
	}

}


#endif


