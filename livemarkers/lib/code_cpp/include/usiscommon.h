#ifndef _USISCOMMON_H_
#define _USISCOMMON_H_

extern "C"
{
	#include <stdlib.h>
	#include <stdio.h>
	#include <math.h>
	#include <string.h>
	#include <limits.h>
	#include <float.h>

	#include "usisflags.h"
}

#include "common.h"
#include "comptime.h"

#define USIS_EPSILON 1E-9
#define USIS_FINFTY FLT_MAX
#define USIS_DINFTY DBL_MAX

#define USIS_GREY_IMAGE    0
#define USIS_COLOR_IMAGE   1

extern int USIS_CUR_STATE_ID;

#endif // _USISCOMMON_H_
