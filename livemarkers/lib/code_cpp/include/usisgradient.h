#ifndef _USISGRADIENT_H_
#define _USISGRADIENT_H_

#include "usiscommon.h"
#include "usisimage.h"
#include "usisgeometry.h"
#include "usisfeatures.h"

namespace usis
{
	Features *vimage_gradient(Image8 *img, float radius, float *Omax,
                              Rectangle bb = Rectangle());
    void vimage_gradient(Features *grad, Image8 *img, float radius,
                         float *Omax, Rectangle bb = Rectangle());

    DbImage *vimage_gradient2(Image8 *img, float radius, double *Omax, Rectangle bb);

	void vimage_gradient2(DbImage *grad, Image8 *img, float radius,
                         double *Omax, Rectangle bb);

	void image_gradient_on_mask(DbImage *grad, Image8 *img,
                               float radius, double *Omax, Rectangle bb);

	Image8 *image_gradient(Image32 *img, float radius);
	Image8 *image_gradient(Image8 *img, float radius);

	Features *vfeatures_gradient(Features *f, float radius, float *Fmax);
    void vfeatures_gradient(Features *grad, Features *f,float radius, float *Fmax,
                            Rectangle bb = Rectangle());

	Image8 *feat_obj_weight_image(Features *Gfeat, Features *Gobj,
                                        float Wobj, float Fmax, float Omax,
                                        Rectangle bb = Rectangle());

	void feat_obj_weight_image(Features *Gfeat, Features *Gobj, Image8 *img,
                               float Wobj, float Fmax, float Omax,
                               Rectangle bb = Rectangle());

    /** Combining feature, object, and cloud based gradients **/
	void feat_obj_cloud_weight_image(Features *Gfeat, DbImage *Gobj,
                                     Image8 *cloud_grad,
                                     Image8 *final_grad,
                                     double Wobj, double Wfeat, double Wcloud,
                                     float Fmax, float Omax,
                                     Pixel grad_center, Pixel cloud_position,
                                     Rectangle bb = Rectangle());


	Image8 *weighted_image_gradient(CImage8 *cimg, AdjRel *A,
									float channel_weight[3]);

	Features* weighted_feature_gradient(Features *feat, float *Fmax, AdjRel *A,
										float *feat_weight);

	void weighted_feature_gradient(Features *feat, Features *Gfeat, float *Fmax,
								 AdjRel *A, float *feat_weight);

	// ** Normalized gradient as proposed by Stolfi.
	// Divides the Sobel gradient operator magnitude/direction by
	// sqrt(V^2+eta^2), where V is the local image intensities standard deviation
	// in a 5x5 window.
	Features* normalized_gradient(Features *feat, double eta, int Awidth = 5);
}

#endif
