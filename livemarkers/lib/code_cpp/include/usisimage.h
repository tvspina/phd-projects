#ifndef _USISIMAGE_H_
#define _USISIMAGE_H_

#include "common.h"
#include "usiscommon.h"
#include "color.h"

#include <iostream>
#include <cstdarg>

#include "usismemory.h"

Pixel usis_right_pixel(Pixel p, Pixel q);

Pixel usis_left_pixel(Pixel p, Pixel q);

using namespace std;

namespace usis
{
	/**********************************************************/
	/**						Grey image  					 **/
	/**********************************************************/

	template <typename T> struct ImageT
	{
	    typedef T value_type;

		T *val;
		int ncols, nrows, n;
		/// total alloced size with padding
		int n_alloced;

		T& operator [](int p);

		Rectangle bb;
	};

	struct Image8
	{
	    typedef uchar value_type;

		Image8::value_type *val;
		int ncols, nrows, n;
		/// total alloced size with padding
		int n_alloced;

		Image8::value_type& operator [](int p);

		Rectangle bb;
	};

	struct Image32
	{
	    typedef int value_type;

		Image32::value_type *val;
		int ncols, nrows, n;
		/// total alloced size with padding
		int n_alloced;

		Image32::value_type& operator [](int p);

		Rectangle bb;
	};

    struct DbImage
    {
	    typedef double value_type;

		DbImage::value_type *val;
		int ncols, nrows, n;
		/// total alloced size with padding
		int n_alloced;

		DbImage::value_type& operator [](int p);

		Rectangle bb;
    };

	typedef Image8* Image8Ptr;
	typedef Image32* Image32Ptr;
	typedef DbImage* DbImagePtr;

	template <typename T, typename U> T* create_image(int w, int h)
	{
		T *img = (T*)calloc(1, sizeof(T));

		int n = w * h;
		int l = USIS_SSE_ALIGNMENT/ sizeof(U);
		n += l - n % l; /// Rounding up to a number divisible by 16 for SSE

		img->val = alloc_array<U>(n);  ///aligning row in 16 bytes
		img->n_alloced = n;
		img->ncols = w;
		img->nrows = h;
		img->n = w*h;
		img->bb = Rectangle(0,0,w-1,h-1);

		return img;
	}

	Image8* create_image8(const int w, const int h);
	/// Copy constructor
	Image8* copy_image8(Image8 *old_img);
		/// Constructor that reads from an PGM image file
	Image8* read_image8(const char *filename);

	Image32* create_image32(const int w, const int h);

    DbImage *create_dbimage(const int w, const int h);

	/// Copy constructor
	Image32* copy_image32(Image32 *old_img);
	/// Constructor that reads from an PGM image file
	Image32* read_image32(const char *filename);

	DbImage* copy_dbimage(DbImage *old_img);

	void destroy_image(Image8 **img);
	void destroy_image(Image32 **img);
	void destroy_dbimage(DbImage **img);

	template <typename T, typename U> U maximum_value(T *img)
	{
		U max_val = img->val[0];

		int i;

		for(i = 1; i < img->n; i++)
		{
			max_val = MAX(max_val, img->val[i]);
		}

		return max_val;
	}

	template <typename T, typename U> U minimum_value(T *img)
	{
		U min_val = img->val[0];

		int i;

		for(i = 1; i < img->n; i++)
		{
			min_val = MIN(min_val, img->val[i]);
		}

		return min_val;
	}

	Image8::value_type maximum_value(Image8 *img);
	Image32::value_type maximum_value(Image32 *img);
	DbImage::value_type maximum_value(DbImage *img);

	Image8::value_type minimum_value(Image8 *img);
	Image32::value_type minimum_value(Image32 *img);
	DbImage::value_type minimum_value(DbImage *img);

	/// writes an 8-bit image to a PGM file
	bool write_image(Image8 *img, const char *format, ...);
	/// writes an 32-bit image to a PGM file
	bool write_image(Image32 *img, const char *format, ...);

	void set_image(Image8 *img, uchar val);
	void set_image(Image32 *img, Image32::value_type val);
	void set_image(DbImage *img, DbImage::value_type val);

	template <typename T, typename U> T* add_frame(T *img, int sx, int sy, U value)
	{
		T *fimg;
		int x, y; //,nbytes,offset;

		fimg = create_image<T, U>(img->ncols+2*sx,img->nrows+2*sy);

		set_image(fimg, value);

		for(y = 0; y < img->nrows; y++)
		{
			for(x = 0; x < img->ncols; x++)
				fimg->val[to_index(fimg, x+sx, y+sy)] = img->val[to_index(img, x, y)];
		}

		return(fimg);
	}

	template <typename T, typename U> T* rem_frame(T *fimg, int sx, int sy)
	{
		T *img;
		int x, y; //,nbytes,offset;

		img = create_image<T, U>(fimg->ncols-2*sx,fimg->nrows-2*sy);

		for(y = 0; y < img->nrows; y++)
		{
			for(x = 0; x < img->ncols; x++)
				img->val[to_index(img, x, y)] = fimg->val[to_index(fimg, x+sx, y+sy)];
		}

		return(img);
	}


	Image8 *add_frame(Image8 *img, int sz, uchar value);
	Image8 *rem_frame(Image8 *fimg, int sz);
	Image32 *add_frame(Image32 *img, int sz, int value);
	Image32 *rem_frame(Image32 *fimg, int sz);

    void paste_image(Image8 *dst, Image8 *src, int x, int y);

	/// Only copies the content from a 32-bit image
	/// (with non-checked max value of 255) to an
	/// Image8
	Image8 *cast_2_image8(Image32 *img32);
	/// Only copies the content from an 8-bit image
	/// (with non-checked max value of 255) to an
	/// Image32
	Image32 *cast_2_image32(Image8 *img8);

	/// Converts the content from an 8-bit image
	/// to an 8-bit image (stretching the values)
	Image8 *convert_2_8bits(Image8 *img);
    /// Converts the content from a 32-bit image
	/// to an 8-bit image (stretching the values)
	Image8 *convert_2_8bits(Image32 *img);
	/// Converts the content from a 32-bit image
	/// to an N-bit image (stretching the values)
	Image32 *convert_2_Nbits(Image32 *img, int N);

    Image8 *convert_2_image8(DbImage *dimg);
    void convert_2_image8_inplace(Image8 *img, DbImage *dimg);

    Image32 *convert_2_image32(DbImage *dimg);
    void convert_2_image32_inplace(Image32 *img, DbImage *dimg);

	/// copies each pixel value from src to dest
	void copy_inplace(Image8 *dest, Image8 *src);
	/// copies each pixel value from src to dest
	void copy_inplace(Image32 *dest, Image32 *src);
	/// copies each pixel value from src to dest
	void copy_inplace(DbImage *dest, DbImage *src);

    Image8 *scale(Image8 *img, float Sx, float Sy);
	Image8 *scale(Image8 *img, Image8 *scl, float Sx, float Sy);

    Image32 *scale(Image32 *img, float Sx, float Sy);
	Image32 *scale(Image32 *img, Image32 *scl, float Sx, float Sy);

    inline double eucl_dist2(Pixel u, Pixel v)
    {
        return (v.x-u.x)*(v.x-u.x)+(v.y-u.y)*(v.y-u.y);
    }

    inline double eucl_dist2(double x0,  double y0, double x1, double y1)
    {
        return (x1-x0)*(x1-x0)+(y0-y1)*(y0-y1);
    }


	template <typename T> double eucl_dist2(T *img, int p, int q)
	{
		Pixel u = to_pixel(img, p);
		Pixel v = to_pixel(img, q);

		return eucl_dist2(u, v);
	}

    double eucl_dist2(Image32 *img, int p, int q);
    double eucl_dist2(Image8 *img, int p, int q);

	/**********************************************************/
	/**						Color image 					 **/
	/**********************************************************/

	struct CImage8
	{
	    typedef uchar value_type;
	    typedef int triplet_type;

		Image8 *C[3];

		int ncols, nrows, n;

		Image8::value_type *data;

		int n_alloced;

		Rectangle bb;
	};

	struct CImage32
	{
	    typedef int value_type;
	    typedef int triplet_type;

		Image32 *C[3];
		int ncols, nrows, n;

		Image32::value_type *data;

		int n_alloced;

		Rectangle bb;
	};

	typedef CImage8* CImage8Ptr;
	typedef CImage32* CImage32Ptr;

	/// Basic constructor
	CImage8 *create_cimage8(const int w, const int h,
							Image8::value_type *data,
							const int n_alloced);

	CImage8* create_cimage8(const int w, const int h);
	/// Copy constructor
	CImage8* copy_cimage8(CImage8 *old_img);
		/// copies each pixel value from src to dest
	void copy_inplace(CImage8 *dest, CImage8 *src);
	/// Constructur that reads from a PPM file
	CImage8* read_cimage8(const char *filename);

	/// Basic constructor
	CImage32 *create_cimage32(const int w, const int h,
							Image32::value_type *data,
							const int n_alloced);
	CImage32* create_cimage32(const int w, const int h);

	/// Copy constructor
	CImage32* copy_cimage32(CImage32 *old_img);

	CImage32* convert_2_cimage32(Image32 *img);
	/// Constructur that reads from a PPM file
	CImage32* read_cimage32(const char *filename);

	/// deletes the cimage object and frees the pointer
	void destroy_cimage(CImage8 **cimg);
	void destroy_cimage(CImage32 **cimg);

	bool is_cimage_gray(CImage8 *cimg);

	/// writes an 8-bit cimage to a PPM file
	bool write_cimage(CImage8 *cimg, const char *filename, ...);
	bool write_cimage(CImage32 *img, const char *filename, ...);

    CImage8* concatenate(CImage8 *img1, CImage8 *img2);
    void concatenate(CImage8 *img1, CImage8 *img2, CImage8 *cat);

	CImage8 *add_frame(CImage8 *img, int sz, uchar r, uchar g, uchar b);

    void paste_image(CImage8 *bkg, CImage8 *fg, int x, int y);

	CImage8 *cimage_ycbcr_to_rgb(CImage8 *cimg);
	CImage8 *cimage_rgb_to_ycbcr(CImage8 *cimg);
	CImage8 *cimage_rgb_to_lab(CImage8 *cimg);
	CImage8 *cimage_lab_to_rgb(CImage8 *cimg);

	/// val is represented by a triplet
	void set_cimage(CImage8 *img, CImage8::triplet_type val);
	/// val is represented by a triplet
	void set_cimage(CImage32 *img, int val);
	/// each channel value is provided separately
	void set_cimage(CImage32 *img, int c0, int c1, int c2);


	/// Only copies the content from a 32-bit image
	/// (with non-checked max value of 255) to a
	/// CImage8
	CImage8 *cast_2_cimage8(Image32 *img32);
	CImage8 *cast_2_cimage8(Image8 *img);
	CImage8 *convert_2_cimage8(Image32 *img);

	CImage8 *convert_2_grayscale(CImage8 *color);

		/// copies each pixel value from src to dest
	void copy_inplace(CImage8 *dest, CImage8 *src);
	void copy_inplace(CImage32 *dest, CImage32 *src);

    CImage8 *scale(CImage8 *img, float Sx, float Sy);
    CImage32 *scale(CImage32 *img, float Sx, float Sy);

	inline bool valid_pixel(Image8 *img, Pixel px)
	{
		return px.x >= 0 && px.x < img->ncols && px.y >= 0 && px.y < img->nrows;
	}

	inline bool valid_pixel(Image8 *img, const int x, const int y)
	{
		return x >= 0 && x < img->ncols && y >= 0 && y < img->nrows;
	}

	inline bool valid_pixel(Image32 *img, Pixel px)
	{
		return px.x >= 0 && px.x < img->ncols && px.y >= 0 && px.y < img->nrows;
	}

	inline bool valid_pixel(Image32 *img, const int x, const int y)
	{
		return x >= 0 && x < img->ncols && y >= 0 && y < img->nrows;
	}

	inline bool valid_pixel(DbImage *img, Pixel px)
	{
		return px.x >= 0 && px.x < img->ncols && px.y >= 0 && px.y < img->nrows;
	}

	inline bool valid_pixel(DbImage *img, const int x, const int y)
	{
		return x >= 0 && x < img->ncols && y >= 0 && y < img->nrows;
	}


	inline bool valid_pixel(CImage8 *img, Pixel px)
	{
		return px.x >= 0 && px.x < img->ncols && px.y >= 0 && px.y < img->nrows;
	}

	inline bool valid_pixel(CImage8 *img, const int x, const int y)
	{
		return x >= 0 && x < img->ncols && y >= 0 && y < img->nrows;
	}

	inline bool valid_pixel(CImage32 *img, Pixel px)
	{
		return px.x >= 0 && px.x < img->ncols && px.y >= 0 && px.y < img->nrows;
	}

	inline bool valid_pixel(CImage32 *img, const int x, const int y)
	{
		return x >= 0 && x < img->ncols && y >= 0 && y < img->nrows;
	}

	inline int to_index(Image8 *img, int x, int y)
	{
		return x + y*img->ncols;
	}

	inline int to_index(Image8 *img, Pixel px)
	{
		return px.x + px.y*img->ncols;
	}

	inline Pixel to_pixel(Image8 *img, int p)
	{
		return (Pixel) {
			p%img->ncols,p/img->ncols
		};
	}

	inline void to_pixel(Image8 *img, int p, int *x, int *y)
	{
		*x = p%img->ncols;
		*y = p/img->ncols;
	}

	inline int to_index(Image32 *img, int x, int y)
	{
		return x + y*img->ncols;
	}

	inline int to_index(Image32 *img, Pixel px)
	{
		return px.x + px.y*img->ncols;
	}

	inline Pixel to_pixel(Image32 *img, int p)
	{
		return (Pixel) {p%img->ncols,p/img->ncols};
	}

	inline void to_pixel(Image32 *img, int p, int *x, int *y)
	{
		*x = p%img->ncols;
		*y = p/img->ncols;
	}

	inline int to_index(DbImage *img, int x, int y)
	{
		return x + y*img->ncols;
	}

	inline int to_index(DbImage *img, Pixel px)
	{
		return px.x + px.y*img->ncols;
	}

	inline Pixel to_pixel(DbImage *img, int p)
	{
		return (Pixel) {
			p%img->ncols,p/img->ncols
		};
	}

	inline void to_pixel(DbImage *img, int p, int *x, int *y)
	{
		*x = p%img->ncols;
		*y = p/img->ncols;
	}


	inline int to_index(CImage8 *img, int x, int y)
	{
		return x + y*img->ncols;
	}

	inline int to_index(CImage8 *img, Pixel px)
	{
		return px.x + px.y*img->ncols;
	}

	inline Pixel to_pixel(CImage8 *img, int p)
	{
		return (Pixel) {p%img->ncols,p/img->ncols};
	}

	inline void to_pixel(CImage8 *img, int p, int *x, int *y)
	{
		*x = p%img->ncols;
		*y = p/img->ncols;
	}

	inline int to_index(CImage32 *img, int x, int y)
	{
		return x + y*img->ncols;
	}

	inline int to_index(CImage32 *img, Pixel px)
	{
		return px.x + px.y*img->ncols;
	}

	inline Pixel to_pixel(CImage32 *img, int p)
	{
		return (Pixel) {
			p%img->ncols,p/img->ncols
		};
	}

	inline void to_pixel(CImage32 *img, int p, int *x, int *y)
	{
		*x = p%img->ncols;
		*y = p/img->ncols;
	}

    inline void set_rgb(CImage8 *img, int p, uchar r, uchar g, uchar b)
    {
        img->C[0]->val[p] = r;
        img->C[1]->val[p] = g;
        img->C[2]->val[p] = b;
    }

    inline void set_rgb(CImage8 *img, int p, int rgb_triplet)
    {
        set_rgb(img, p, t0(rgb_triplet), t1(rgb_triplet), t2(rgb_triplet));
    }

    inline void set_rgb(CImage8 *img, Pixel px, int rgb_triplet)
    {
        set_rgb(img, to_index(img, px), rgb_triplet);
    }

    inline void set_rgb(CImage8 *img, Pixel px,  uchar r, uchar g, uchar b)
    {
        set_rgb(img, to_index(img, px), r,g,b);
    }

    inline void set_rgb(CImage8 *img, int x, int y,  uchar r, uchar g, uchar b)
    {
        set_rgb(img, to_index(img, x, y), r,g,b);
    }

    inline void set_rgb(CImage8 *img, int x, int y,  int rgb_triplet)
    {
        set_rgb(img, to_index(img, x, y), rgb_triplet);
    }

	inline int get_rgb(CImage8 *img, int p)
	{
		return triplet(img->C[0]->val[p],
						img->C[1]->val[p],
						img->C[2]->val[p]);
	}

    /** Misc **/

    void and_image(Image8 *mask1, Image8 *mask2, uchar val = 1);
    void xor_image(Image8 *mask1, Image8 *mask2, uchar val = 1);

    void add_image(Image32 *bkg, Image32 *fg, int x, int y);
    void add_image(Image8 *bkg, Image8 *fg, int x, int y);
    void add_image(CImage8 *bkg, CImage8 *fg, int x, int y);

	CImage8 *blur(CImage8 *cimg, double radius, Rectangle bb);
	void blur_inplace(CImage8 *cimg, double radius, Rectangle bb);

}


#endif // _USISIMAGE_H_
