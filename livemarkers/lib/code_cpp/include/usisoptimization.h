#ifndef _USISOPTIMIZATION_H_
#define _USISOPTIMIZATION_H_

extern "C"
{
    #include <assert.h>
}

#include "usiscommon.h"

/*Whether the method is for minimization or maximization. */
/* > is maximization, < is minimization */
#define USIS_MSPS_MIN_MAX_OP >
#define USIS_MSPS_WORST_SCORE -USIS_DINFTY
#define USIS_MSPS_BEST_SCORE USIS_DINFTY

/* Type for a fitness function. That is, an optimization problem. */
typedef double (*problem) ( double *x, void *context);

struct optContext
{
  problem f;                 /* The fitness function to be optimized. */
  void *fContext;            /* The problem's context. */
  size_t fDim;               /* Dimensionality of problem. */
  double *theta;             /* Theta to be initially considered and to be return
                                as the best one in the end of the optimization. */
  double * lowerInit;   /* Lower initialization boundary. */
  double * upperInit;   /* Upper initialization boundary. */
  double * lowerBound;  /* Lower search-space boundary. */
  double * upperBound;  /* Upper search-space boundary. */
  size_t numIterations;      /* Number of fitness-evaluations. */
  char verbose;              /* Whether it prints information from the optmiation or not. */
};

namespace usis
{
    double MSPS_integer( double *param, void* context);
}


#endif // _USISOPTIMIZATION_H_
