#ifndef _USISDESCRIPTOR_H_
#define _USISDESCRIPTOR_H_

#include "usisimage.h"
#include <tr1/unordered_map>

#include "usiscommon.h"
#include "adjacency.h"
#include "usisfeatures.h"
#include "comptime.h"

#define LOW 0
#define HIGH 1

#define NBINS_PER_CHANNEL 16 // orig 16, 4 for making paper images
#define NBINS_OPTICAL_FLOW 60

using namespace std;
using namespace std::tr1;

namespace usis
{
    struct Curve
    { /* Curve */
        double *X;
        double *Y;
        int n;
    };
    /*data structure used to compute distances */
    struct FeatureVector1D
    {
        FeatureVector1D(int n);
        virtual ~FeatureVector1D();

        double *X;
        int n;
    };
    /* data structure used to extract BIC descriptor */
    struct Property
    {
        int color;
        int frequency;
    };

	Curve *create_curve(int n);
	void destroy_curve(Curve **curve);

	FeatureVector1D  *curve_to_1D_feature_vector(Curve *curve);
    FeatureVector1D *create_feature_vector1D(int n);
    void destroy_feature_vector1D(FeatureVector1D **desc);


	inline int quantize_rgb(int r, int g, int b, int color_dim)
	{
		int fator_g = color_dim;
		int fator_b = fator_g * color_dim;

		r = color_dim * r / 256;
		g = color_dim * g / 256;
		b = color_dim * b / 256;

		return (r + fator_g * g + fator_b * b);
	}

	Image32 *quantize_colors(CImage8 *img, int color_dim);

	FeatureVector1D *histogram_descriptor(Features *feat, unordered_map<int, Property> &pixels,
                                      int bins_per_channel);

    FeatureVector1D *histogram_descriptor(CImage8 *img, unordered_map<int, Property> &pixels,
											int bins_per_channel);

	void normalize_histogram(FeatureVector1D *hist);

	double chi_2_distance(FeatureVector1D *hist1, FeatureVector1D *hist2);
	double euclidean_distance(FeatureVector1D *hist1, FeatureVector1D *hist2);

	void write_feature_vector1d(FeatureVector1D *vec, const char *format, ...);

}

#endif // _USISDESCRIPTOR_H_
