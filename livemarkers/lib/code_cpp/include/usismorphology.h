#ifndef _USISMORPHOLOGY_H_
#define _USISMORPHOLOGY_H_

#include <tr1/unordered_map>
#include <map>
#include <set>

#include "gqueue.h"
#include "usiscommon.h"
#include "adjacency.h"
#include "usisimage.h"
#include "usisradiometric.h"

using namespace std;

namespace usis
{

    // Creates Empty Forest
    struct Forest
    {
        Image32 *P; // predecessor map
        Image32 *R; // root map
        Image32 *V; // distance (cost or connectivity) map
    };

	AdjPxl *adj_pixels(int ncols, AdjRel *A);

	Image8* object_border(Image8 *bin);
	set<int> object_border2(Image8 *bin);

	Image8 *dilate(Image8 *img, AdjRel *A);
	Image32 *dilate(Image32 *img, AdjRel *A);
	Image8 *erode(Image8 *img, AdjRel *A);
	Image32 *erode(Image32 *img, AdjRel *A);

    Image8 *close(Image8 *img, AdjRel *A);
    Image8 *open(Image8 *img, AdjRel *A);
    Image32 *open(Image32 *img, AdjRel *A);

	Image8 *close_holes(Image8 *img);
	Image32 *close_holes(Image32 *img);
	Image8 *sup_rec(Image8 *img, Image32 *marker, AdjRel *A);
	Image32 *sup_rec(Image32 *img, Image32 *marker, AdjRel *A);
    Image8 *inf_rec(Image8 *img, Image32 *marker, AdjRel *A);
    Image32 *inf_rec(Image32 *img, Image32 *marker, AdjRel *A);

    Image8 *close_rec(Image8 *img, AdjRel *A);
    Image8 *open_rec(Image8 *img, AdjRel *A);
    Image32 *open_rec(Image32 *img, AdjRel *A);

    Image32 *label_comp(Image32 *img, AdjRel *A, int thres);
    Image8 *remove_region_by_area(Image8 *label, int area);
    Image32 *remove_region_by_area(Image32 *label, int area);

	Image8 *close_label_holes(Image8 *label, Image32 *root, int deflabel);
	Image8 *label_sup_rec(Image8 *img, Image32 *marker, Image32 *root, int deflabel, AdjRel *A);

	Image8 *image_xor(Image8 *img0, Image8 *img1);

	/// Signed euclidean distance transform (squared)
	/// For pixels where the label I->val[p] == 0
	/// the EDT value will be positive
	Image32 *signed_edt2(Image8 *B, Image8 *I, Image32 **root = NULL, int thrsh = 0);
	Image32 *signed_edt(Image8 *B, Image8 *I, Image32 **root = NULL, int thrsh = 0);

    Image32 *edt(Image8 *I, Image32 **root = NULL, int thrsh = 0);
    void edt_inplace(Image8 *I, Image32 *edt, Image32 **root = NULL, int thrsh = 0);


	DbImage* signed_edtdb(Image8 *B, Image8 *I, Image32 **root = NULL, int thrsh = 0);
    DbImage *edtdb(Image8 *I, Image32 **root = NULL, int thrsh = 0);
    void edt_inplace(Image8 *I, DbImage *edt, Image32 **root = NULL, int thrsh = 0);


    Image32 *edt(Image32 *I, Image32 **root = NULL, int thrsh = 0);
    void edt_inplace(Image32 *I, Image32 *edt, Image32 **root = NULL, int thrsh = 0);

    Forest *edt2(Image8 *I, int thrsh = 0);
    Forest *edt2_inplace(Image8 *I, Image32 *edt, int thrsh = 0);
    Forest *edt2(Image32 *I, int thrsh = 0);
    Forest *edt2_inplace(Image32 *I, Image32 *edt, int thrsh = 0);

    Forest *create_forest(int ncols, int nrows);
    void destroy_forest(Forest **F);

    /** Multiscale Skeletonization **/
	struct MedialAxisData
	{
		int lb;
		double dist;
	};

	typedef map<int, MedialAxisData> MedialAxis;

	bool valid_cont_point(Image8 *bin, AdjRel *L, AdjRel *R, int p, int thrsh);

    DbImage *ms_geodesic_skel(Image8 *I, Image32 **distances = NULL);

	MedialAxis medial_axis_from_skiz(DbImage *skiz, Image32 *eucl_distance2,
									Image8 *label, int lb, double thresh);

	void fast_dilate_medial_axis(MedialAxis axis, Image8 *dil_label, double dist_perc);

	// Dilates the medial axis using a fraction of the distance
	// to the original border (@param dist_perc) and original
	// label values
	void dilate_medial_axis(MedialAxis axis, Image8 *dil_label,	double dist_perc);


    double density(int p, usis::Image8 *mask, int lb, AdjRel *A);
    tr1::unordered_map<int, pair<double, double> > avg_density(usis::Image8 *mask, AdjRel *A);
    double avg_density(usis::Image8 *mask, int lb, AdjRel *A);
    usis::Image8* filter_by_density(usis::Image8 *mask, AdjRel *A);
    usis::Image8* filter_by_density(usis::Image8 *mask, int lb, AdjRel *A);


}

#endif


