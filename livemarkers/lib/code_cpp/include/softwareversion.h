#ifndef _SOFTWAREVERSION_H_
#define _SOFTWAREVERSION_H_

#include <string>
#include <sstream>

using namespace std;

class SoftwareVersion
{
	public:
		SoftwareVersion();
		SoftwareVersion(int major,
										int minor,
										int revision);
		SoftwareVersion(const SoftwareVersion &ver);

		string *GetVersion();
		int  GetMajor();
		int  GetMinor();
		int  GetRevision();
	private:
		int m_major;
		int m_minor;
		int m_revision;
};

#endif



