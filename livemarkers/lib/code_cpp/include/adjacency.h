#ifndef _ADJACENCY_H_
#define _ADJACENCY_H_

#include "usiscommon.h"
#include "usisimage.h"
#include "usisfeatures.h"

struct AdjRel
{
  int *dx;
  int *dy;
  int n;
};

struct AdjPxl
{
  int *dp;
  int n;
};


/* All adjacency relations must place the central pixel at first. The
   central pixel must be skipped when visiting the neighbors during
   the IFT. It must be considered in the other operations. */

AdjRel *create_adjrel(int n);
void    destroy_adjrel(AdjRel **A);
AdjRel *clone_adjrel(AdjRel *A);

inline Pixel adjacent(AdjRel *A, Pixel v, int i)
{
	return (Pixel){v.x + A->dx[i], v.y + A->dy[i]};
}

template <typename T> Pixel next_adjacent(T img, AdjRel *A, Pixel v_o, int &cur_idx)
{
	Pixel u = (Pixel){NIL, NIL};
	bool valid = false;

	for(;cur_idx < A->n && !valid; cur_idx++)
	{
		u = adjacent(A, v_o, cur_idx);
		valid = usis::valid_pixel(img, u);
	}

	return (valid) ? u : (Pixel){NIL, NIL};
}


Pixel next_adjacent(usis::Image8 *img, AdjRel *A, Pixel v, int &cur_idx);
Pixel next_adjacent(usis::Image32 *img, AdjRel *A, Pixel v, int &cur_idx);
Pixel next_adjacent(usis::CImage8 *img, AdjRel *A, Pixel v, int &cur_idx);
Pixel next_adjacent(usis::CImage32 *img, AdjRel *A, Pixel v, int &cur_idx);
Pixel next_adjacent(usis::DbImage *img, AdjRel *A, Pixel v, int &cur_idx);
Pixel next_adjacent(Features *img, AdjRel *A, Pixel v, int &cur_idx);


AdjRel *right_side(AdjRel *A);
AdjRel *left_side(AdjRel *A);
AdjRel *right_side2(AdjRel *A, float r);
AdjRel *left_side2(AdjRel *A, float r);
AdjRel *circular(float r);
AdjRel *fast_circular(float r);

AdjRel *fast_ring_sector(double radius1, double radius2, double theta1, double theta2);

AdjRel *transform_adjacency(AdjRel *A, float direct_transform[4][4]);

int adjacent_index(AdjRel *A, Pixel px, Pixel px_adj);

void    DestroyAdjPxl(AdjPxl **N);
int     FrameSize(AdjRel *A);

usis::Image8  *adjrel2image(AdjRel *A);
AdjRel *box(int ncols, int nrows);

#endif
