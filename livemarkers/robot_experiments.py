# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
#!/usr/bin/python2.7
from usis.common import *
from usis.npimage import *
from usis.morphology import *
from usis.segmentation import *
from usis.view import *
from usis.adjacency import *

from robot_gui import *
from simulation_data import *
from robot_collection import *

import numpy as np

import os, sys, select, shutil
from os.path import *
import pylab as pl
import matplotlib.pyplot as plt
import time, copy, pickle, argparse as ap

from ConfigParser import *
from operator import itemgetter,attrgetter
from math import *

import multiprocessing as mp

def main():
	parser = ap.ArgumentParser(formatter_class=ap.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--input',type=str, required=False, default='experiments.cfg', help='The input configuration file with the description of all experiments/datasets')
	parser.add_argument('--methods',type=str, nargs='+', required=False, default=None, help='Overrides the methods selected in the configuration file for all datasets')
	parser.add_argument('--show_images',action='store_true', default = False, help='Displays the robot users in action using a GUI. If --save_figure is active, intermediate images will be saved.')
	parser.add_argument('--dont_save_data',action='store_true', default = False, help='Does not save the experimental results')
	parser.add_argument('--continue_experiment',action='store_true', default = False, help='Continues the last interrupted experiment')
	parser.add_argument('--threaded',action='store_true', default = False, help='Runs the robot user experiments in parallel. The GUI is not displayed in this case')
	parser.add_argument('--nthreads',type=int, default = 4, help='The number of maximum threads to be used for parallel experiments')
	parser.add_argument('--verbose',action='store_true',default = False)
	parser.add_argument('--overlay_gt',action='store_true',default = False, help='Overlays the ground truth on the displayed images')
	parser.add_argument('--save_figures',action='store_true',default = False, help='Saves the final figures after segmentation. If --show_images is active, intermediate images will be saved.')
	parser.add_argument('--save_video',action='store_true',default = False, help='Saves multiple images to produce video sequences. Activates --shows_images by default.')
	parser.add_argument('--interactive',action='store_true',default = False, help='Executes the experiments interactively instead of using robots')
	parser.add_argument('--maximize_window',action='store_true',default = False, help='Maximizes the windows when showing the robot users')

	args = vars(parser.parse_args())
	input_file = args['input']
	user_selected_methods = args['methods']

	config = read_experiment_file(input_file)

	show_images = args['show_images']
	dont_save_data = args['dont_save_data']
	continue_experiment = args['continue_experiment']
	threaded = args['threaded']
	nthreads = args['nthreads']

	# Variables for simulation config from command line
	maximize_window = args['maximize_window']
	overlay_gt = args['overlay_gt']
	save_fig = args['save_figures']
	save_video = args['save_video']
	interactive = args['interactive']
	verbose = args['verbose']

	# Forcing GUI to be computed and all figures to be saved
	if(save_video):
		show_images = True
		save_fig = True

	if not interactive:
		plt.ion()

	for dataset in config.sections():
		# Skipping all method configurations
		if dataset.find('MethodConfig') < 0:
			print 'Starting simulation for dataset: ', dataset
			# (Re-)reading config for the simulation and methods
			# to avoid any alterations made to them by the
			# robots across different datasets
			exp_opts = get_experiments(config,dataset)

			print exp_opts['methods']
			if user_selected_methods is not None:
				exp_opts['methods'] = user_selected_methods

			method_config = get_method_config(config)

			# Assembling experiments to allow proper resuming
			if continue_experiment:
				assemble_experiment_files('.', exp_opts['methods'], interactive)

			run_experiments_on_dataset(args, exp_opts, method_config, show_images,
										dont_save_data,	continue_experiment,
										threaded, nthreads, interactive, maximize_window,
										overlay_gt, save_fig, save_video, verbose)

def run_experiments_on_dataset(args, exp_opts, method_config, show_images,
									dont_save_data,	continue_experiment, threaded,
									nthreads, interactive, maximize_window, overlay_gt, save_fig,
									save_video, verbose):
	# Variables from configuration file
	input_name = exp_opts['input_name']
	gt_name = exp_opts['gt_name']
	marker_input_name = exp_opts['marker_input_name']

	methods = exp_opts['methods']

	simulation_config = copy.deepcopy(exp_opts)

	del simulation_config['methods']

	simulation_config.update({'verbose':verbose,
							'show_images':show_images,
							'maximize_window':maximize_window,
							'overlay_gt': overlay_gt,
							'save_fig': save_fig,
							'save_video': save_video,
							'interactive':interactive})

	if simulation_config['save_labels']:
		answer = 'N'
		print 'Are you sure you want to save all labels? It will require a LOT of space... (s/N) [timeout 10s]'
		answer,_,_ = select.select([sys.stdin],[],[],10)
		if answer:
			answer = sys.stdin.readline().strip()
			if answer.lower() != 's':
				save_labels = False
				simulation_config['save_labels'] = save_labels
				print 'Not saving!'
			else:
				print 'Careful what you wish for...'

	if threaded:
		if simulation_config['show_images']:
			print 'Overriding --show_images decision due to multithreading'
		simulation_config['show_images'] = False
		simulation_config['nthreads'] = nthreads

	kwargs = {'methods':methods, 'simulation_config':simulation_config,
			'input_name':input_name, 'gt_input_name':gt_name,
			'dont_save_data':dont_save_data,'continue_experiment':continue_experiment,
			'marker_input_name':marker_input_name,
			'interactive': interactive,
			'threaded': threaded}

	tic = time.clock()

	simulation_data, exp_time = init_simulation_data(methods, continue_experiment,
													simulation_config,
													method_config,
													interactive = interactive)

	experiment_config = None
	variable_opt = None
	if simulation_config['variable_opt'] is not None:
		variable_opt = simulation_config['variable_opt']
		experiment_config = simulation_config
	elif method_config['variable_opt'] is not None:
		variable_opt = method_config['variable_opt']
		experiment_config = method_config
	else:
		print 'No variable selected for variation!'\
				'Please set a variable_opt option in the config file'
		return

	print experiment_config[variable_opt]

	for value, key_suffix in zip(experiment_config[variable_opt],
				     xrange(len(experiment_config[variable_opt]))):
		experiment_config[variable_opt] = value

		# Note that in case of multithreading/processing
		# locking is unnecessary since each process has a deep copy
		# of simulation_data and will save the experiments for chunks of
		# datasets in separate experiment files.
		exp_key = init_experiment(methods, simulation_data, simulation_config,
									method_config, exp_time, key_suffix,
									continue_experiment)


		kwargs['exp_key'] = exp_key # Avoiding issue when deep copying locks
		kwargs['simulation_data'] = simulation_data # Avoiding issue when deep copying locks

		print 'MethodConfig: ', method_config
		run_experiments(**kwargs)


	toc = time.clock()

	print 'Total runtime: %ss' % str(toc - tic)

	if not dont_save_data:
		assemble_experiment_files('.', methods, interactive)


def run_experiments(**kwargs):
	print 'Simulation config:', kwargs['simulation_config']

	# Simulation for a single image
	if isfile(kwargs['input_name']):
		img_name = kwargs['input_name']
		gt_name = kwargs['gt_input_name']
		methods = kwargs['methods']
		simulation_data = kwargs['simulation_data']
		exp_key = kwargs['exp_key']
		simulation_config = kwargs['simulation_config']
		dont_save_data = kwargs['dont_save_data']
		continue_experiment = kwargs['continue_experiment']
		marker_input_name = kwargs['marker_input_name']
		interactive = kwargs['interactive']

		simulate_for_image(methods, img_name, gt_name, simulation_data, exp_key,
						simulation_config, dont_save_data, continue_experiment,
						marker_input_name, interactive)
	# Simulation for multiple images in a folder
	else:
		walk(kwargs['input_name'],visit_dir,kwargs)

# Function that walks inside a folder executing
# the simulation for each image
def visit_dir(arg, dirname, names):
	methods = arg['methods']
	input_name = arg['input_name']
	gt_input_name = arg['gt_input_name']
	marker_input_name = arg['marker_input_name']
	simulation_data = arg['simulation_data']
	exp_key = arg['exp_key']
	simulation_config = arg['simulation_config']
	dont_save_data = arg['dont_save_data']
	continue_experiment = arg['continue_experiment']
	interactive = arg['interactive']
	threaded = arg['threaded']

	# Removing subdirs from the recursive visit, if necessary.
	# By altering variable 'names' the walk function won't visit
	# the removed subdirs.
	del_subdirs = set()
	for d in names:
		full_path = join(dirname,d)
		if isdir(full_path):
			del_subdirs.add(d)

	for d in del_subdirs:
		del names[names.index(d)]

	image_files = get_image_files(dirname, names, gt_input_name,
									marker_input_name)
	# Removing blacklisted images
	blacklisted_name = join(dirname,'blacklist.txt')
	if exists(blacklisted_name):
		blacklist = open(blacklisted_name)
		for i in blacklist.readlines():
			print 'Blacklisting: ' + i.strip()
			for j in xrange(len(image_files)):
				if image_files[j][0].find(i.strip()) >= 0:
					del image_files[j]
					break

		blacklist.close()

	# Sorting is important to ensure that all images
	# be selected the same way, to guarantee that
	# calls to random_seeds/random_integer be
	# issued sequentially
	image_files = sorted(image_files)

	if not threaded or interactive:
		simulation_config['interactive'] = interactive
		simulate_for_image_set(methods, image_files, simulation_data,
							exp_key, simulation_config, dont_save_data,
							continue_experiment, interactive,
							None)
	else:
		nmax_processes = None
		if threaded:
			ncpus = mp.cpu_count()
			if simulation_config['nthreads'] is not None:
				nmax_processes = min(simulation_config['nthreads'], ncpus)
			else:
				if ncpus <= 4:
					nmax_processes = max(ncpus/2,1)
				else:
					nmax_processes = ncpus/2

			print 'Maximum number of processes %d/%d' % (nmax_processes, ncpus)

		processes = []

		nfiles = len(image_files)
		chunk = max(1,nfiles/nmax_processes)
		nchunks = int(math.ceil(nfiles/float(chunk)))

		kwargs_i = {}

		for i,exp_tuple in zip(xrange(nchunks),image_files):
			k = i*chunk
			k1 = min((i+1)*chunk-1,len(image_files))

			kwargs_i[i] = {}
			kwargs_i[i]['methods'] = methods
			kwargs_i[i]['image_files'] = image_files[k:k1+1]
			# chunk_id will be used when saving the experiments
			# for a given image to a temporary file that will
			# later be merged.
			kwargs_i[i]['chunk_id'] = k
			# Deep copying data to avoid conflicts, but making the
			# simulation's lock None to avoid issues when spawning
			# a new process. The lock won't be necessary since each
			# dataset chunk has it's own copy of simulation data.
			for method in methods:
				simulation_data[method][exp_key].lock = None

			kwargs_i[i]['simulation_data'] = copy.deepcopy(simulation_data)
			kwargs_i[i]['exp_key'] = exp_key
			kwargs_i[i]['simulation_config'] = simulation_config
			kwargs_i[i]['dont_save_data'] = dont_save_data
			kwargs_i[i]['continue_experiment'] = continue_experiment
			kwargs_i[i]['interactive'] = interactive

			processes.append(mp.Process(target = simulate_for_image_set, kwargs = kwargs_i[i]))


		# Using at most nmax_processes at a time
		c = 0
		while c < len(processes):
			first_process = c
			last_process = min(first_process+nmax_processes, len(processes))
			for i in xrange(first_process,last_process):
				processes[i].start()
				c = c + 1

			# Blocking main thread until completion of
			# all processes between first_process and last_process
			for i in xrange(first_process,last_process):
				print 'Joining process for key %d' % i
				processes[i].join()

# Parsing directories for image files
def get_image_files(dirname, names, gt_dir,
						marker_dir = None):
	image_files = []
	for f in names:
		full_path = join(dirname,f)

		if isfile(full_path) and is_image(full_path):
			gt_name = find_file(f, gt_dir)

			if gt_name is None:
				continue

			exp_name_tuple = (full_path, gt_name)

			if marker_dir is not None:
				mk_name = find_file(f, marker_dir, suffix = ['-anno'])

				if mk_name is not None:
					exp_name_tuple = (full_path, gt_name, mk_name)

			image_files.append(exp_name_tuple)

	return image_files

def find_file(f, search_dir, suffix = ['_gt',''],
				img_extensions = ['bmp','jpg','png','pgm','ppm','txt']):
	img_name = f.split('.')[0]
	exp_name_tuple = None

	for s in suffix:
		for ext in img_extensions:
			name = join(search_dir,img_name + s +'.'+ext)

			if exists(name):
				return name

	return None

## Function that simulates for a given set of images
def simulate_for_image_set(methods, image_files, simulation_data,
							exp_key, simulation_config, dont_save_data,
							continue_experiment, interactive,
							chunk_id = None):

	print image_files
	for i,exp_tuple in zip(xrange(1,len(image_files)+1),image_files):
		print i, exp_tuple
		img_name = exp_tuple[0]
		gt_name = exp_tuple[1]

		# Testing if the
		mk_name = None

		if len(exp_tuple) >= 3:
			mk_name = exp_tuple[2]

		exp_time = time.ctime(float(exp_key.split('_')[0]))
		key_suffix = exp_key.split('_')[1]
		k = i
		k1 = len(image_files)
		if chunk_id is not None:
			k += chunk_id
			k1 += chunk_id

		print 'Image %d/%d experiment %s %s' % (k,k1, exp_time, key_suffix)

		simulate_for_image(methods, img_name, gt_name, simulation_data, exp_key,
							simulation_config, dont_save_data,
							continue_experiment, mk_name, interactive,
							chunk_id = chunk_id)

		print 'Done for Image %d/%d experiment %s %s' % (k,k1, exp_time, key_suffix)


## Simulating the segmentation of a single image with all selected methods
def simulate_for_image(methods, img_name, gt_name, simulation_data, exp_key,
						simulation_config, dont_save_data, continue_experiment,
						marker_input_name, interactive,
						chunk_id = None):
	global gui

	gui = None

	print '\n\n*** Simulating for image %s ***' % img_name

	img = CImage8(filename = img_name)
	gt = Image8(filename = gt_name)

	# Binarizing ground truth due to multi-object region-based segmentation
	gt = threshold(gt, 1, np.max(gt.arr))

	if simulation_config['close_holes']:
		# Closing all contour holes
		gt = close_holes(gt)
	else:
		# Closing small contour holes to avoid
		# issues with tiny contours
		gt = close_rec(gt, AdjRel(radius=5.0))

	frame_side = None
	##UNCOMMENT: sorts methods alphabetically
#	sorted_methods = sorted(methods)
	sorted_methods = methods

	if simulation_config['add_frame']:
		frame_side = 5
		img = img.add_frame(frame_side,0,0,0)
		gt = gt.add_frame(frame_side,0)
	try:
		# Computing a new random generator for each image in interactive experiments
		random_generator = random_seed()
		robots = RobotCollection(sorted_methods, img, gt, simulation_data,
									exp_key, random_generator, frame_side,
									marker_input_name)

		if simulation_config['save_fig'] or simulation_config['save_video']:
			disp_img = img.copy()
			colormap = initialize_colormap(True)

			robots.grad.write('robot_%s_grad.png' % splitext(basename(img_name))[0])

			if robots.markers is not None:
				display_seeds(disp_img, robots.markers, colormap)
				disp_img.write('robot_%s_seeds.png' % splitext(basename(img_name))[0])

			if robots.obj is not None:
				robots.obj.write('robot_%s_objmap.png' % splitext(basename(img_name))[0])

	except RuntimeError as e:
		print e
		print 'No robots computed for image %s' % img_name
		robots = None

	if robots is not None:
		# Ensuring that LWOF and Riverbed will be tested
		# first for this image, in case the other methods
		# should stop by accuracy, since they will
		# set the accuracy threshold for the others.
		boundary_methods = set()
		if(simulation_config['stop_criterion'] == 'accuracy'
			and 'common_accuracy_thresh' not in simulation_config):

			# Searching for all boundary-based methods
			# in order to have them excute before the others
			# to set the common_accuracy_thresh
			for m in sorted_methods:
				if robots[m].method.method_type == 'boundary':
					boundary_methods.add(m)

			for m in boundary_methods:
				# Moving the method to the beginning of sorted_methods
				# to ensure that it will be executed first
				sorted_methods.remove(m)
				sorted_methods.insert(0,m)

			# Setting maximum accuracy threshold
			# for this image, before setting the
			# new accuracy threshold as the minimum
			# between LWOF's and Riverbed's accuracy
			if len(boundary_methods) > 0:
				for m in sorted_methods:
					simulation_data[m][exp_key].simulation_config['common_accuracy_thresh'] = 1.0

		## UNCOMMENT: random shuffling the methods's order
		## for interactive experiments
#		if interactive:
#			random.shuffle(sorted_methods)

		if simulation_config['show_images'] or interactive:
			cur_bot = robots[sorted_methods[0]]
			gui = ContourTrackingGUI(cur_bot, img_name, interactive)

			# Copying arc-weight estimation markers for display if existent
			if hasattr(robots, 'markers') and robots.markers is not None:
				setattr(gui, 'markers', dict(robots.markers))

		for method in sorted_methods:
			run_experiment_for_robot(robots, method, img_name, exp_key,
										simulation_data,
										continue_experiment,
										interactive)

			# saving the final result
			if simulation_config['save_fig'] or simulation_config['save_video']:
				cur_bot = robots[method]
				cimg = cur_bot.orig_img.copy()
				label = cur_bot.method.label
				colormap = initialize_colormap(True)
				highlight_label(cimg, label, colormap, 0, 1.5, True)
				cimg.write('robot_%s_%s_final.png' %
							(method,splitext(basename(img_name))[0]))

				# Saving the raw label when finished
				label.write('robot_%s_%s_label.pgm' %
							(method, splitext(basename(img_name))[0]))

			if method in boundary_methods:
				# Selecting the last iteration's accuracy for
				# the current image if it was properly segmented
				# by the current method...
				if img_name in simulation_data[method][exp_key].accuracy:
					if len(simulation_data[method][exp_key].accuracy[img_name]) > 0:
						last_it = max(simulation_data[method][exp_key].accuracy[img_name])
						acc = simulation_data[method][exp_key].accuracy[img_name][last_it]
						for m in sorted_methods:
							if m not in boundary_methods:
								#... and setting it as the threshold for
								# non-boundary-based methods if it is the minimum
								# among all of the obtained from boundary-based methods
								old_acc = simulation_data[m][exp_key].simulation_config['common_accuracy_thresh']
								acc = min(old_acc, acc)
								simulation_data[m][exp_key].simulation_config['common_accuracy_thresh'] = acc

								print 'Setting accuracy threshold for %s as %f' % (m, acc)


		if not dont_save_data:
			for method in methods:
				# We save the experiments (of each method) started for
				# all images of the current dataset with the same timestamp
				# and suffix in a single file. Then, we assemble all experiment
				# files with different suffixes and timestamps into a single
				# larger file containing all data for each method. This allows
				# that multithreading.
				if interactive:
					filename = 'simulation_data_interactive_%s_%s.pkl' % (method, exp_key)
				else:
					filename = 'simulation_data_%s_%s.pkl' % (method, exp_key)

				# chunk_id represents the dataset's chunk id.
				# It will be set if multithreading is allowed. Hence,
				# to avoid having to deal with locks we simply save
				# the experiments for this chunk adding its id to the file.
				# Upon assembling the data all chunks will be merged into
				# one to compose the experiment for the corresponding dataset.
				if chunk_id is not None:
					filename = filename.replace('.pkl','_%d.pkl' % chunk_id)


				save_experiments(filename, simulation_data[method][exp_key],
								simulation_config['save_labels'])

		if simulation_config['show_images'] or interactive:
			gui.close()

## Running the simulation for a given image and robot/method
def run_experiment_for_robot(robots, method, img_name, exp_key, simulation_data,
								continue_experiment,
								interactive):
	global gui

	cur_bot = robots[method]
	simulation_config = simulation_data[method][exp_key].simulation_config
	method_config = simulation_data[method][exp_key].method_config

	do_experiment = not continue_experiment
	not_simulated = not verify_image_simulation(img_name, simulation_config,
												method_config,
												simulation_data[method][exp_key])

	print 'Experiment from %s for %s' % (time.ctime(float(simulation_data[method][exp_key].exp_time)),
											method.capitalize())
	do_experiment = do_experiment or not_simulated

	if do_experiment:
		simulation_data[method][exp_key].reset_image_data(img_name, robots.img)

		if simulation_config['show_images'] or simulation_config['interactive']:
			gui.set_robot(cur_bot, img_name)

		tic = time.clock()
		try:
			## Simulating the method with the robot
			if not interactive:
				print '\n** Simulating for method %s **' % method
				cur_bot.run_simulation(img_name, simulation_config,
										 gui = gui)
			## Allowing the user to segment interactively
			else:
				cur_bot.init_simulation(0,simulation_config)
				gui.display_default_images(simulation_config['maximize_window'])

		except DelineationError as e:
			# Erasing the experiment for img_name due to self loop
			print '\n' + str(e)
			print 'Erasing experiment for image %s at %s id %s\n' % (img_name,
					time.ctime(float(simulation_data[method][exp_key].exp_time)),
					simulation_data[method][exp_key].key_suffix)
			simulation_data[method][exp_key].remove_unsuccessful_experiment(img_name, str(e))
		else:
			toc = time.clock()
			runtime = toc - tic
			simulation_data[method][exp_key].add_runtime(img_name, runtime)

			print 'Run time: %fs for %s, experiment %s' % (runtime,method.capitalize(),
															time.ctime(float(simulation_data[method][exp_key].exp_time)))
	else:
		print 'Method %s already simulated for image %s' % (method.capitalize(), img_name)


## Verifying if the simulation has occurred for a given image
## to avoid redoing experiments in case of crashes
def verify_image_simulation(img_name, simulation_config,
								method_config, simulation_data):
	param_changes = False

	variable_opt = None
	if 'variable_opt' in simulation_config:
		variable_opt = simulation_config['variable_opt']
		if variable_opt is not None:
			param_changes = simulation_config[variable_opt] != simulation_data.simulation_config[variable_opt]

	if variable_opt is None:
		variable_opt = method_config['variable_opt']
		param_changes = method_config[variable_opt] != simulation_data.method_config[variable_opt]

	req_params = set(['contour_ori', 'err_thresh','strip_width',
						'strip_length'])

	for param in req_params:
		config = simulation_data.simulation_config[param]
		if simulation_config[param] != config:
			param_changes = True
			break

	do_simulation = (not param_changes) and img_name in simulation_data.accuracy_measures

	return do_simulation and img_name not in simulation_data.unsuccessful_experiments

## Configuration file parsing ##

def opt_str_to_bool(opt):
	return opt.lower() not in [0,'false','no']

def get_option(config, section, option, opt_type, variable_opt):
	cnv_to_type = opt_type
	if opt_type == bool:
		cnv_to_type = opt_str_to_bool
	if option == variable_opt:
		return map(cnv_to_type,config.get(section, option).split())
	else:
		return cnv_to_type(config.get(section, option))

def get_experiments(config, section):
	exp_opts = {}

	# Experiment configuration
	exp_opts['input_name'] = expanduser(config.get(section,'input_name'))
	exp_opts['gt_name'] = expanduser(config.get(section,'gt_name'))
	exp_opts['marker_input_name'] = expanduser(config.get(section,'marker_input_name'))
	exp_opts['methods'] = config.get(section,'methods').split()

	exp_opts['suffix'] = config.get(section,'suffix')
	if config.has_option(section,'nthreads'):
		exp_opts['nthreads'] = config.getint(section,'nthreads')
	else:
		exp_opts['nthreads'] = None

	try:
		variable_opt = config.get(section, 'variable_opt')
	except:
		variable_opt = None

	opt_type = {}

	# Overall configuration
	opt_type['save_labels'] = bool
	opt_type['save_seeds'] = bool

	# Image preparation configuration
	opt_type['add_frame'] = bool
	opt_type['close_holes'] = bool

	# Arc-weight enhancement configuration
	opt_type['intelligent_enhancement'] = bool
	opt_type['wobj'] = float

	# Error, iteration, and accuracy thresholds
	opt_type['err_thresh'] = float
	opt_type['iteration_thresh'] = int
	opt_type['stop_criterion'] = str

	opt_type['accuracy_measure'] = str

	# Boundary-based/hybrid robot configuration
	opt_type['contour_ori'] = str
	opt_type['strip_width'] = float
	opt_type['strip_length'] = int

	# Hybrid robot configuration
	opt_type['novice_user'] = bool
	opt_type['truly_random_first_anchor'] = bool
	opt_type['first_anchor_choice'] = str

	opt_type['marker_choice'] = str
	opt_type['first_marker_choice'] = str

	# Region-based robot configuration
	opt_type['border_distance'] = int
	opt_type['seeds_per_iteration'] = int
	opt_type['max_marker'] = int
	opt_type['min_marker'] = int
	opt_type['safe_distance'] = int

	for opt in opt_type:
		exp_opts[opt] = get_option(config, section, opt, opt_type[opt], variable_opt)

	# We add the accuracy threshold here because it is optional.
	# Therefore, we determine if it exists in this section.
	# If it is set then it cannot be overriden by any other method.
	# Otherwise, it will be determined automatically.
	opt = 'common_accuracy_thresh'

	if config.has_option(section, opt):
		exp_opts[opt] = get_option(config, section, opt, float, variable_opt)

	exp_opts['variable_opt'] = variable_opt

	return exp_opts


def get_method_config(config):
	section = 'MethodConfig'
	default_params = config.defaults()

	# Copying all options to a new dictionary.
	# IMPORTANT: In case a new method is added with specific configuration
	# options, they will be automatically copied as strings to the dictionary.
	# The options for LWOF, Riverbed, DIFT, and Max-Flow will be strongly
	# typed at the end of this function
	method_config = dict(config.items(section))

	# Removing default parameters since they are
	# pertinent only to the datasets.
	for key in default_params:
		del method_config[key]

	try:
		variable_opt = config.get(section, 'variable_opt')
		if 'variable_opt' in default_params:
			variable_opt = None
	except:
		variable_opt = None



	opt_type = {}
	opt_type['adj_radius'] = float
	opt_type['lwof_ori'] = int
	opt_type['lwof_power'] = float

	opt_type['dift_power'] = float
	opt_type['dift_heap'] = bool
	opt_type['dift_cost_function'] = str
	opt_type['mk_id'] = int

	opt_type['gc_lambda'] = float
	opt_type['gc_power'] = float

	opt_type['volume_threshold'] = int

	opt_type['force_ori'] = bool
	opt_type['primary_label'] = int
	opt_type['left_pixel_label'] = int
	opt_type['right_pixel_label'] = int
	opt_type['lm_mk_radius'] = float
	opt_type['lm_max_mk_radius'] = float

	for opt in opt_type:
		method_config[opt] = get_option(config, section, opt, opt_type[opt], variable_opt)

	if variable_opt is not None:
		method_config['variable_opt'] = variable_opt

	print method_config

	return method_config


## Calling Main function

if __name__ == '__main__':
	main()
