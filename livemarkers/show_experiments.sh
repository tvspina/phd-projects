#!/bin/bash
export LD_LIBRARY_PATH=$(pwd)/lib/cython:$(pwd)/lib/cython/usis:$(pwd)/lib/code_cpp/lib:$(pwd)/lib/code_c/lib:$(pwd)/lib/maxflow-v2.2.src/lib
export PYTHONPATH=$(pwd):$(pwd):$(pwd)/lib/cython:$(pwd)/lib/cython/usis


python show_experiments.py $@
