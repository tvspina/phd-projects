# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
from usis.evaluation import *

import pickle, copy, time

from ConfigParser import *
from operator import itemgetter,attrgetter
from os.path import *
import os

class SimulationData(object):
	def __init__(self, simulation_config, method_config,
					exp_time, key_suffix = '', lock = None):
		self.simulation_config = copy.deepcopy(simulation_config)
		self.method_config = copy.deepcopy(method_config)

		self.img_info = {}
		self.accuracy = {}
		self.accuracy_measures = {}
		self.interactions = {}
		self.control_measures = {}
		self.anchors = {}
		self.clocks = {}
		self.path = {}
		self.seeds = {}
		self.path_error = {}
		self.labels = {}
		self.runtime = {}
		self.lock = lock

		self.key_suffix = str(key_suffix)
		self.exp_time = exp_time

		self.unsuccessful_experiments = set()


	def reset_image_data(self, img_name, img):
		self.acquire_lock()

		self.img_info[img_name] = {'size':img.size()}
		self.accuracy[img_name] = {}
		self.accuracy_measures[img_name] = {}
		self.interactions[img_name] = 0
		self.control_measures[img_name] = {}
		self.anchors[img_name] = []
		self.clocks[img_name] = {}
		self.path[img_name] = {}
		self.seeds[img_name] = {}
		self.path_error[img_name] = {}
		self.labels[img_name] = {}
		self.runtime[img_name] = 0.0

		self.release_lock()

	def copy_sim_data(self, sim_data):
		self.acquire_lock()

		self.img_info.update(sim_data.img_info)
		self.accuracy.update(sim_data.accuracy)
		self.accuracy_measures.update(sim_data.accuracy_measures)
		self.interactions.update(sim_data.interactions)
		self.control_measures.update(sim_data.control_measures)
		self.anchors.update(sim_data.anchors)
		self.clocks.update(sim_data.clocks)
		self.path.update(sim_data.path)
		self.seeds.update(sim_data.seeds)
		self.path_error.update(sim_data.path_error)
		self.labels.update(sim_data.labels)
		self.runtime.update(sim_data.runtime)

		self.release_lock()

	def remove_experiment(self, img):
		self.acquire_lock()
		if img in self.img_info:
			del self.img_info[img]

		if img in self.accuracy:
			del self.accuracy[img]

		if img in self.accuracy_measures:
			del self.accuracy_measures[img]

		if img in self.control_measures:
			del self.control_measures[img]

		if img in self.interactions:
			del self.interactions[img]

		if img in self.anchors:
			del self.anchors[img]

		if img in self.clocks:
			del self.clocks[img]

		if img in self.seeds:
			del self.seeds[img]

		if img in self.path_error:
			del self.path_error[img]

		if img in self.labels:
			del self.labels[img]

		if img in self.runtime:
			del self.runtime[img]

		self.release_lock()

	def remove_unsuccessful_experiment(self, img, msg):
		self.remove_experiment(img)

		self.acquire_lock()
		print 'Adding %s to unsuccessful experiment list due to \"%s\".' % (img,msg)
		self.unsuccessful_experiments.add((img,msg))
		self.release_lock()

	@staticmethod
	def available_acc_measures():
		return ['f-measure', 'hamming', 'boundary-error',
				'tp', 'tn', 'fp', 'fn','plain_acc']

	def add_accuracy(self, img, label, gt, iteration,
						accuracy_measure):
		self.acquire_lock()

		fmeasure, tp, tn, fp, fn = compute_fmeasure(label, gt)
		tp /= float(label.n)
		tn /= float(label.n)
		fp /= float(label.n)
		fn /= float(label.n)
		hamming = compute_hamming_dist_norm_by_gt(label, gt)
		edt_error = compute_sgn_edt_error_measures(label, gt)

		self.accuracy_measures[img][iteration] = {'f-measure':fmeasure,
													'hamming': hamming,
													'boundary-error': edt_error[0],
													'boundary-measures':edt_error[1:],
													'tp':tp, 'tn':tn,
													'fp':fp, 'fn':fn,
													'plain_acc':tp+tn}

		acc = self.accuracy_measures[img][iteration][accuracy_measure]

		self.accuracy[img][iteration] = acc
		self.release_lock()

		return acc

	def add_anchor(self, img, anchor, path, seeds = None,
					path_error = None, coherent_path = None,
					in_front = False, anchor_time = None):
		self.acquire_lock()

		if path is not None:
			self.path[img][anchor] = copy.deepcopy(path)

			if path_error is not None:
				self.path_error[img][anchor] = {'path_error':path_error}

				if coherent_path is not None:
					self.path_error[img][anchor]['coherent_path'] = coherent_path

		# Only add the anchor if it is not part of
		# the list.
		if anchor not in self.anchors[img]:
			if not in_front:
				self.anchors[img].append(anchor)
			else:
				self.anchors[img].insert(0,anchor)

			if anchor_time is None:
				self.clocks[img][anchor] = time.time()
			else:
				self.clocks[img][anchor] = anchor_time


		self.release_lock()

		# NOTE!!! add_sees tries to lock the data if necessary
		self.add_seeds(img, anchor, seeds)

	def del_anchor(self, img, anchor):
		self.acquire_lock()

		if anchor in self.seeds[img]:
			del self.seeds[img][anchor]

		if anchor in self.path_error[img]:
			del self.path_error[img][anchor]

		if anchor in self.path[img]:
			del self.path[img][anchor]

		if anchor in self.anchors[img]:
			self.anchors[img].remove(anchor)

		if anchor in self.clocks[img]:
			del self.clocks[img][anchor]

		self.release_lock()

	def add_seeds(self, img, mk_id, seeds):
		self.acquire_lock()

		# If the marker id already exists, simply
		# update all of its info with the new one.
		if seeds is not None:
			self.seeds[img][mk_id] = copy.deepcopy(seeds)

		self.release_lock()

	def add_label(self, img, label, iteration):
		self.acquire_lock()
		self.labels[img][iteration] = label.copy()
		self.release_lock()

	def del_label(self, img, iteration):
		self.acquire_lock()

		if iteration in self.labels:
			del self.labels[img][iteration]

		self.release_lock()

	def add_control_measures(self, img):
		self.acquire_lock()
		if len(self.labels[img]) > 0:
			self.control_measures[img] = compute_control_measures(self.labels[img])
		self.release_lock()

	def add_interaction_amount(self, img, interactions):
		self.acquire_lock()
		self.interactions[img] = self.interactions[img] + interactions
		self.release_lock()

	def add_runtime(self, img, runtime):
		self.acquire_lock()
		self.runtime[img] = self.runtime[img] + runtime
		self.release_lock()

	def acquire_lock(self):
		if self.lock is not None:
			self.lock.acquire()

	def release_lock(self):
		if self.lock is not None:
			self.lock.release()

	def release_labels(self):
		for img_name in self.labels:
			for iteration in self.labels[img_name]:
				self.labels[img_name] = {}

	def save_data(self, filename):
		name = basename(filename).split('.')[0]
		folder_name = join(dirname(filename),name)
		zipfile_name = folder_name + '.zip'
		os.makedirs(folder_name)
		for img_name in self.labels:
			for iteration in self.labels[img_name]:
				lb_img = self.labels[img_name][iteration]
				img_basename = basename(img_name).split('.')[0]
				lb_img_name = join(folder_name,img_basename + ('_%02d.png' % iteration))
				lb_img.write(lb_img_name)
				# Replacing the label by the corresponding location
				# under which it was saved
				self.labels[img_name][iteration] = lb_img_name
				print lb_img_name


## Functions for saving and loading the results of experiments ##

def make_key(exp_time, key_suffix):
	key = str(exp_time)
	if key_suffix is not None and str(key_suffix) != '':
		key = key + '_' + str(key_suffix)

	return key

def split_key(exp_key):
	return float(exp_key.split('_')[0]),exp_key.split('_')[1]

def init_simulation_data(methods, continue_experiment, simulation_config,
							method_config, interactive):
	simulation_data = {}

	# Initializing simulation_data with None for each method so that
	# a new SimulationData object be created for each current experiment.
	# I.e., we do not need previous data unless we need to continue a previous
	# experiment.
	for m in methods:
		if interactive:
			filename = 'simulation_data_interactive_%s.pkl' % m
		else:
			filename = 'simulation_data_%s.pkl' % m

		simulation_data[m] = None

	if continue_experiment:
		# Loading methods to determine the last experiment carried out
		for m in methods:
			if interactive:
				filename = 'simulation_data_interactive_%s.pkl' % m
			else:
				filename = 'simulation_data_%s.pkl' % m
			simulation_data[m] = load_experiments(filename)

		if len(methods) > 0 and len(simulation_data) > 0:
			exp_time = -1
			# Finding the maximum key among all the methods.
			print simulation_config['suffix']
			for m in methods:
				if len(simulation_data[m]) > 0:
					for x in simulation_data[m]:
						sim_config_cpy = dict(simulation_data[m][x].simulation_config)
						method_config_cpy = dict(simulation_data[m][x].method_config)

						sim_config_cpy2 = dict(simulation_config)
						method_config_cpy2 = dict(method_config)

						if('variable_opt' in sim_config_cpy and
							sim_config_cpy['variable_opt'] is not None):
							del sim_config_cpy[sim_config_cpy['variable_opt']]

						if('variable_opt' in sim_config_cpy2 and
							sim_config_cpy2['variable_opt'] is not None):
							del sim_config_cpy2[sim_config_cpy2['variable_opt']]

						if('variable_opt' in method_config_cpy and
							method_config_cpy['variable_opt'] is not None):
							del method_config_cpy[method_config_cpy['variable_opt']]

						if('variable_opt' in method_config_cpy2 and
							method_config_cpy2['variable_opt'] is not None):
							del method_config_cpy2[method_config_cpy2['variable_opt']]

						if('common_accuracy_thresh' in sim_config_cpy):
							del sim_config_cpy['common_accuracy_thresh']

						if('common_accuracy_thresh' in sim_config_cpy2):
							del sim_config_cpy2['common_accuracy_thresh']

						if(sim_config_cpy == sim_config_cpy2 and
							method_config_cpy == method_config_cpy2):
							exp_time = max(exp_time, simulation_data[m][x].exp_time)

		if exp_time < 0:
			print 'Former experiment not found to be continued. Starting a new one.'
			exp_time = int(time.time())
		else:
			print 'Former experiment time: %s' % time.ctime(exp_time)
	else:
		exp_time = int(time.time()) # Using the current time rounded to an integer
										# as part of the experiment's key
	return simulation_data, exp_time

def init_experiment(methods, simulation_data, simulation_config, method_config,
						exp_time, key_suffix, continue_experiment,
						lock = None):
	for m in methods:
		if simulation_data[m] is None:
			simulation_data[m] = {}
		if (not continue_experiment):
			new_data = SimulationData(simulation_config, method_config,
										exp_time, key_suffix = key_suffix,
										lock = lock)
			exp_key = make_key(new_data.exp_time, new_data.key_suffix)
			simulation_data[m][exp_key] = new_data
		else:
			exp_key = make_key(exp_time, key_suffix)
			if exp_key in simulation_data[m]:
				simulation_data[m][exp_key].simulation_config = copy.deepcopy(simulation_config)
				simulation_data[m][exp_key].method_config = copy.deepcopy(method_config)
				simulation_data[m][exp_key].lock = lock
			else:
				new_data = SimulationData(simulation_config, method_config,
											exp_time, key_suffix = key_suffix,
											lock = lock)
				simulation_data[m][exp_key] = new_data

	return exp_key

# Loads a set of experiments for all selected methods
def load_simulation_data(methods, interactive):
	simulation_data = None

	if len(methods) == 1:
		if interactive:
			filename = 'simulation_data_interactive_%s.pkl' % methods[0]
		else:
			filename = 'simulation_data_%s.pkl' % methods[0]

		# Comparing a single method with itself
		simulation_data = {methods[0]: load_experiments(filename)}
	else:
		simulation_data = {}
		for i in methods:
			if interactive:
				filename = 'simulation_data_interactive_%s.pkl' % i
			else:
				filename = 'simulation_data_%s.pkl' % i
			simulation_data[i] = load_experiments(filename)

	return simulation_data

# Loads all the experiments for a given method
def load_experiments(filename):
	if not exists(filename) and exists(join(split(filename)[0],'simulation_data.zip')):
		os.system('unzip -u -o simulation_data.zip')
	try:
		sim_file = open(filename,'r')
	except:
		print 'File %s not found' % filename
		sim_file = None

	sim_data = None
	if sim_file is not None:
		sim_data = pickle.load(sim_file)

		sim_file.close()

	return sim_data

# Saves experiments using pickle. Note that sim_data may contain
# the experiments for a single or multiple methods
def save_experiments(filename, sim_data, save_labels, lock = True):
	tmp_lock = None
	sucess = False
	if lock:
		sim_data.acquire_lock()
		# Temporarily removing the lock from
		# sim_data since it cannot be pickled
		# together with it
		tmp_lock = sim_data.lock
		sim_data.lock = None

	try:
		sim_file = open(filename,'w')
	except:
		print 'File %s not found' % filename
	else:
		if save_labels:
			sim_data.save_data(filename)
		# Release labels if sim_data is a SimulationData type object
		elif hasattr(sim_data,'release_labels'):
			sim_data.release_labels()

		pickle.dump(sim_data, sim_file)
		sim_file.close()

		success = True

		if lock:
			# Returning the lock to sim_data
			# after pickling.
			sim_data.lock = tmp_lock
			sim_data.release_lock()

	return success

# This function helps multithreading by zipping the pickle files
# together
def assemble_experiment_files(dirname, methods, interactive):
	files = os.listdir(dirname)

	pkl_files = {}
	for m in methods:
		pkl_files[m] = []

	# Searching for all pickle files in dirname
	for f in files:
		if f.rfind('.pkl') >= 0:
			for m in methods:
				if f.find(m) >= 0:
					if interactive and f.find('interactive') >= 0:
						pkl_files[m].append(f)
					elif not interactive and f.find('interactive') < 0:
						pkl_files[m].append(f)

	# Separating each pickle file of each method
	for m in pkl_files:
		if interactive:
			final_file = join(dirname,'simulation_data_interactive_%s.pkl' % m)
		else:
			final_file = join(dirname,'simulation_data_%s.pkl' % m)

		# Loading previous experiments
		method_simulation = load_experiments(final_file)
		if method_simulation is None:
			method_simulation = {}

		zipped_folders = ''
		for f in pkl_files[m]:
			# We only select the files whose names
			# are not the same as the final file, hence,
			# we are only selecting the experiment files
			# created with timestamps (i.e., representing fragmented
			# experiments)
			if join(dirname,f) != final_file:
				simulation_data = load_experiments(join(dirname,f))
				key = make_key(simulation_data.exp_time, simulation_data.key_suffix)

				# If key already exists in simulation_data than
				# the current file is most likely a chunk of a greater dataset.
				# Therefore, we only update the simulation's data with whatever
				# is lodaded from the chunk/file.
				if key in method_simulation:
					method_simulation[key].copy_sim_data(simulation_data)
				else:
					method_simulation[key] = simulation_data

				if simulation_data.simulation_config['save_labels']:
					# Saving data folders if they exist and save_label == True
					data_folder = join(dirname,basename(f).replace('.pkl',''))
					if exists(data_folder):
						zipped_folders += data_folder + ' '

		# Since we've loaded all fragmented experiment files and stored
		# them in method_simulation, we can now pickle that file normally
		save_experiments(final_file, method_simulation, False, False)

		# Removing fragmented experiment files and
		# saving data folders if they exist
		for f in pkl_files[m]:
			if join(dirname,f) != final_file:
				print 'removing', join(dirname,f)
				os.remove(join(dirname,f))

		if zipped_folders != '':
			os.system('zip -r %s %s' % (final_file.replace('.pkl','.zip'),
											zipped_folders))
			for folder in zipped_folders.split():
				shutil.rmtree(folder)

		# Zipping .pkl files to decrease storage size
		os.system('zip -r simulation_data.zip %s' % final_file)



def read_experiment_file(filename):
	config = SafeConfigParser()
	config.read(filename)

	return config
