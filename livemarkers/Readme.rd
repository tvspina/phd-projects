=========================================================================
#### Required packages (Ubuntu 12.04) ####

The next packages must be installed via apt-get prior to compiling our
source code. We use a mix of Python/Cython and C/C++. 

g++/gcc
libboost-random-dev
libboost-system-dev
python (version 2.7)
cython (version >= 0.20, it is better to install it via pip install -U cython)
python-numpy
python-scipy
python-matplotlib
liblapack-dev
libblas-dev
libatlas-base-dev
swig
make

=========================================================================
#### Compilation ####

After installing the packages above, in the main folder type: 

-> make

=========================================================================
#### The software ####

File 'experiments.sh' runs both our robot users and the graphical user
interface for all of the methods available (listed in the next
section). See the next sections for details on how to use ./experiments.sh
interactively and with the robot users.

The core functions to our methods are implemented in Cython and C++, in files:

lib/cython/usis/segmentation.pyx # Python interface that actually implements the hybrid methods while exposing them to Python
lib/code_cpp/src/usissegmentation.cpp # C++ code for IFT-based standalone methods (LWOF, Riverbed, DIFT-SC) and interface for the min-cut/max-flow's library

Our robot users are implemented in Python, in files:

robot_collection.py # Class that groups and initializes all of our robot users. It also computes the arc-weigh estimation step for the current test image doing the supervised fuzzy pixel learning when requested.

boundary_robots/boundary_robots.py # boundary-tracking robots
boundary_robots/hybrid_methods_robots.py # robots for live markers/hybrid methods
region_robots/region_robots.py # region-based delineation robots

=========================================================================
#### Testing the software interactively ####

-> ./experiments.sh --interactive --dont_save_data

The default configuration file 'experiments.cfg' contains all options
for segmenting the images in the 'test' folder with each of the
selected methods in the [Test] section/dataset. See the dataset
section below for more information on adding new datasets. 

After segmenting an image, close the window to proceed to the next one
in the dataset. A new window will be automatically displayed next.

The methods we have implemented are the following:

KEYWORD - METHOD
lwof - live-wire-on-the-fly (LWOF, boundary-tracking)
riverbed - Riverbed (boundary-tracking)
dift - Differential Image Foresting Transform with Seed Competition (DIFT-SC, region-based delineation)
mflow - Graph Cuts by the Min-Cut/Max-Flow algorithm (GCMF, region-based delineation)

livemarkers - LiveMarkers (hybrid, combines LWOF and DIFT-SC)
livecut - LiveCut (hybrid, combines LWOF and GCMF)
rivercut - RiverCut (hybrid, combines Riverbed and GCMF)
rivermarkers - RiverMarkers (hybrid, combines Riverbed and DIFT-SC)
 
The following options are available for ./experiments.sh:

  -h, --help            show this help message and exit
  --input INPUT         The input configuration file with the description of
                        all experiments/datasets (default: experiments.cfg)
  --methods METHODS [METHODS ...]
                        Overrides the methods selected in the configuration
                        file for all datasets (default: None)
  --show_images         Displays the robot users in action using a GUI. If
                        --save_figure is active, intermediate images will be
                        saved. (default: False)
  --dont_save_data      Does not save the experimental results (default:
                        False)
  --continue_experiment
                        Continues the last interrupted experiment (default:
                        False)
  --threaded            Runs the robot user experiments in parallel. The GUI
                        is not displayed in this case (default: False)
  --nthreads NTHREADS   The number of maximum threads to be used for parallel
                        experiments (default: 4)
  --verbose
  --overlay_gt          Overlays the ground truth on the displayed images
                        (default: False)
  --save_figures        Saves the final figures after segmentation. If
                        --show_images is active, intermediate images will be
                        saved. (default: False)
  --save_video          Saves multiple images to produce video sequences.
                        Activates --shows_images by default. (default: False)
  --interactive         Executes the experiments interactively instead of
                        using robots (default: False)
  --maximize_window     Maximizes the windows when showing the robot users
                        (default: False)

=========================================================================
####  GUI commands to be used in interactive mode ####

*** Common commands ***

0) Close window: proceeds to the segmentation of the next image in the
dataset. A new window will be automatically displayed next.

- Keyboard:

Display commands: 

1) Key '1': displays the main image, brush strokes, and anchor points.

2) Key '2': displays the main image with only the segmentation mask overlayed.

3) Key '3': displays the main image overlayed with the soft colored background from the segmentation mask.

4) Key '4': displays the segmentation mask.

5) Key '5': displays the linearly combined image gradient used in arc-weight estimation.

6) Key '6': displays the object membership map if computed.

7) Key '7': displays the markers used for arc-weight estimation if performed.

8) Key '8': displays the main image overlayed with the hard colored background from the segmentation mask.

9) Key 'a': hides anchor points (green dots).

10) Key 'f': toggles the foreground segmentation color.

11) Key 'w': saves the current raw label to file 'robot_<image_name>_<method>_label.pgm'

Segmentation commands:

1) Key 'enter': SMOOTHS THE CURRENT SEGMENTATION USING RelaxedIFT.

2) Keys 'arrow up' and 'arrow down': increase/decrease the live marker/brush stroke radius.

*** Boundary-tracking specific commands ***

- Mouse:

1) Left click: selects anchor point.

2) Middle click (anywhere): deselects the last anchor point.

3) Right click: adds new anchor and closes the contour from it.

4) Motion: guides the boundary-tracking algorithm that computes the
path until the current mouse position.

- Keyboard:

1) Keys '<' and '>': change the boundary-tracking orientation.

2) Key 'i': inverts the current path color to facilitate display.

*** Region-based delineation specific commands ***

- Mouse:

1) Left click and drag: adds a foreground brush stroke. Upon releasing
the button the region-based method is executed.

2) Right click and drag: adds a background brush stroke. Upon
releasing the button the region-based method is executed.

3) Left click and drag + 'control key': deletes a piece of a brush
stroke. Upon release the region-based method is executed.

*** Hybrid methods specific commands ***

- Mouse

1) Left click: selects an anchor point and issues the region-based
delineation.

2) Middle click (anywhere): ends the current path segment, freeing the
mouse to select another.

3) Right click: adds new anchor and closes the contour from it.

4) Motion: guides the boundary-tracking algorithm that computes the
path until the current mouse position.

5) Left click and drag + 'shift key': draws a foreground brush
stroke. Upon release the region-based method is executed.

6) Left click and drag + 'control key': deletes a piece of a brush
stroke. Upon release the region-based method is executed.

7) Middle click and 'control key': erases the last live marker/path
segment and runs region-based delineation to update the segmentation
mask.

=========================================================================
#### Testing the software with robot users ####

-> ./experiments.sh --show_images

The robot users will run for all methods datasets and methods listed
in experiments.cfg. Please read experiments.cfg for greater details on
every parameter.

=========================================================================
#### Dataset structure ####

Each dataset should contain between two and three files for each image:

dataset folder/image.ppm REQUIRED (main image file, it can also be .bmp, .png, .jpg)
dataset/gt/image.pgm REQUIRED (ground truth image, .bmp, .png, .jpg)
dataset/markers/image-anno.png OPTIONAL (an image or .txt file containing the object and background brush strokes selected inside and outside the ground truth for arc weight estimation)

The markers will be used for supervised learning during arc-weight
estimation. If they are given inside an image file, they should be
drawn with color on a black background. See the test dataset
examples. If the markers are given as a text file it should have the
following structure:

nseeds image_w image_h
x y mk_id lb
x1 y1 mk_id lb
x2 y2 mk_id lb
.
.
.

where x,y are the seed's coordinates, mk_id identifies seeds in a same
brush stroke and lb is the corresponding marker label (i.e., 0 for
background and 1, 2, ... for the objects).

Each dataset should be explicitly added in the experiments.cfg file,
following the structure of the [Test] section. The experiments.cfg
file also contains a [Default] section for configuring common
parameters for the experiments and robot users, and another to
configure each method ([MethodConfig]).

=========================================================================
#### Evaluating the results ####

IMPORTANT: the experiments are saved in simulation_data.zip for robot
users and simulation_data_interactive.zip for interactive
experiments IF AND ONLY IF flag --dont_save_data was NOT USED. 

Run:

-> ./show_experiments --methods <select methods>

And follow the instructions on screen. Note: option 'Raw data to txt'
will save the methods' raw data in text files to be used with
util/friedman_nemenyi.py to generate the Nemenyi post-hoc test graphs,
after the Friedman statistical test.

Options:

  -h, --help            show this help message and exit
  --methods METHODS [METHODS ...]
                        Overrides the methods selected in the configuration
                        file for all datasets (default: None)
  --show_stdev          Shows the standard deviation on the graphs (default:
                        False)
  --interactive         Shows the results of interactive experiments (default:
                        False)
  --save_fig SAVE_FIG   Saves the figures in .eps format instead of displaying
                        them (default: None)
  --niterations NITERATIONS
                        Number of iterations to be displayed (default: -1)
  --acc_limits ACC_LIMITS ACC_LIMITS
                        Sets the accuracy limits for the graphs (default:
                        [0.0, 1.0])


