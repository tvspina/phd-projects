# -*- coding: utf-8 -*-
'''
Copyright (C) 2014 Thiago Vallin Spina, Paulo André Vechiatto de Miranda, and Alexandre Xavier Falcão.
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software for the sole purpose of reviewing
the following journal article subject to the stated conditions:

Thiago Vallin Spina, Paulo André Vechiatto de Miranda, Alexandre
Xavier Falcão, "Hybrid approaches for interactive image segmentation
using the Live Markers paradigm."

The Software cannot be sold, redistributed, nor used within any type
of application without explicit approval by the authors. Use is
permited strictly during the confidential review process.

This permission notice shall be included in all copies or substantial
portions of the Software, along with authorship credit to T. V. Spina,
P. A. V. de Miranda, and A. X. Falcão. Please see full copyright in
COPYING file.
'''
#!/usr/bin/python
from robot_experiments import *
from robot_gui import *
from usis.geometry import *
from usis.draw import *

import pylab as pl
import matplotlib as mpl

import argparse as ap
from operator import itemgetter
from math import *

# TrueType font for generating vectorial graphs that
# do not use Type 3 fonts (matplotlib's default)
# Tip from: http://www.phyletica.com/?p=308
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

method_nicknames = [('lwof','LWOF'),('riverbed','Riverbed'),('dift','DIFT-SC'),('mflow','GCMF'),
					('livemarkers','LiveMarkers'), ('rivercut','RiverCut'),('livecut','LiveCut'),
					('rivermarkers','RiverMarkers')]


def average_accuracy(experiments, exp_key, valid_imgs = None,
						**kwargs):
	acc_measure = kwargs['acc_measure']

	iteration = -1
	if 'iteration' in kwargs:
		iteration = kwargs['iteration']

	nelems = len(experiments[exp_key].accuracy_measures) if valid_imgs is None else len(valid_imgs)
	acc = np.zeros(nelems,np.float)

	i = 0

	if valid_imgs is not None:
		iterate_over = valid_imgs
	else:
		iterate_over = experiments[exp_key].accuracy_measures

	for x in sorted(iterate_over):
		acc[i] = 0.0
		# Some robots might not segment an image if
		# certain criteria are not met (e.g., superpixel
		# robot). Hence, we only measure accuracy for
		# segmented images.

		if len(experiments[exp_key].accuracy_measures[x]) > 0:
			# Negative iteration means that the last one (largest)
			# iteration/key should be used.
			if iteration < 0:
				sel_iteration = max(experiments[exp_key].accuracy_measures[x])
			else:
				# Selecting the last iteration in case the method
				# took less iterations than the required iteration.
				sel_iteration = min(iteration,max(experiments[exp_key].accuracy_measures[x]))

			acc[i] = experiments[exp_key].accuracy_measures[x][sel_iteration][acc_measure]
		i = i + 1

	return np.average(acc), np.std(acc)

def average_interaction(experiments, exp_key, valid_imgs = None, **kwargs):
	nelems = len(experiments[exp_key].accuracy_measures) if valid_imgs is None else len(valid_imgs)
	measures = np.zeros(nelems,np.float)

	i = 0

	if valid_imgs is not None:
		iterate_over = valid_imgs
	else:
		iterate_over = experiments[exp_key].accuracy_measures

	for x in sorted(iterate_over):
		measures[i] = 0

		interactions = experiments[exp_key].interactions

		if len(interactions) > 0:
			measures[i] = interactions[x]

		i = i + 1


	return np.average(measures), np.std(measures)

def average_control(experiments, exp_key, valid_imgs = None, **kwargs):
	nelems = len(experiments[exp_key].accuracy_measures) if valid_imgs is None else len(valid_imgs)
	measures = np.zeros(nelems,np.float)

	i = 0
	if valid_imgs is not None:
		iterate_over = valid_imgs
	else:
		iterate_over = experiments[exp_key].accuracy_measures

	for x in sorted(iterate_over):
		measures[i] = 0.0

		control_measures = experiments[exp_key].control_measures

		if len(control_measures) > 0:
			measures[i] = control_measures[x][0]

		i = i + 1

	return np.average(measures), np.std(measures)

def average_path_data(experiments, exp_key, valid_imgs = None, **kwargs):
	nelems = len(experiments[exp_key].accuracy_measures) if valid_imgs is None else len(valid_imgs)
	measures = np.zeros(nelems,np.float)

	i = 0
	if valid_imgs is not None:
		iterate_over = valid_imgs
	else:
		iterate_over = experiments[exp_key].accuracy_measures

	for x in sorted(iterate_over):
		measures[i] = 0.0

		tmp_img = Image8(size = experiments[exp_key].img_info[x]['size'])

		path_error = experiments[exp_key].path_error[x]

		if len(experiments[exp_key].seeds[x]) > 0:
			for anchor in experiments[exp_key].seeds[x]:
				if kwargs['path_data'] == 'length':
					measures[i] += len(experiments[exp_key].path[x][anchor])
				else:
					if len(path_error) > 0 and anchor in path_error:
						if type(path_error[anchor]) == dict:
							measures[i] += path_error[anchor][kwargs['path_data']]
						else:
							measures[i] += path_error[anchor]

			measures[i] /= max(len(experiments[exp_key].seeds[x]),1.0)
		else:
			for anchor in experiments[exp_key].anchors[x]:
				if anchor in experiments[exp_key].path[x]:
					if kwargs['path_data'] == 'length':
						measures[i] += len(experiments[exp_key].path[x][anchor])
					else:
						if len(path_error) > 0 and anchor in path_error:
							if type(path_error[anchor]) == dict:
								measures[i] += path_error[anchor][kwargs['path_data']]
							else:
								measures[i] += path_error[anchor]


			measures[i] /= max(len(experiments[exp_key].path[x]),1.0)

		i = i + 1

	return np.average(measures), np.std(measures)


# Computes the average number of interactions required for
# all the images accross the different parameter configurations
# in exp_keys
def experiment_average_measure(experiments, exp_keys, measure_fun,
									valid_imgs = None,	**kwargs):
	avg = np.zeros(len(exp_keys),np.float)
	stdev = np.zeros(len(exp_keys),np.float)
	err = np.zeros(len(exp_keys),np.float)

	variable_opt = 'err_thresh'
	return_variable_opt = True

	for i,x in zip(xrange(len(exp_keys)),sorted(exp_keys,key=lambda x: map(float, x.split('_')))):
		# Checking if 'variable_opt' is in the simulation configuration
		# for backwards compatibility
		if 'variable_opt' in experiments[x].simulation_config:
			variable_opt = experiments[x].simulation_config['variable_opt']
			if variable_opt is not None:
				err[i] = experiments[x].simulation_config[variable_opt]
			else:
				variable_opt = experiments[x].method_config['variable_opt']
				err[i] = experiments[x].method_config[variable_opt]
		else:
			err[i] = experiments[x].simulation_config[variable_opt]

		if hasattr(experiments[x],'link_orig'):
			return_variable_opt = False

		avg[i],stdev[i] = measure_fun(experiments, x, valid_imgs = valid_imgs,
										**kwargs)

	if not return_variable_opt:
		variable_opt = None

	return (avg, stdev, err), variable_opt


#Computing graphs per experiment parameter configuration
def compute_statistics(acc_measure, experiments, sel_key, valid_imgs = None):
	exp_keys = set()
	for x in experiments:
		if x.find(sel_key) >= 0:
			exp_keys.add(x)

	acc_data, variable_opt = experiment_average_measure(experiments, exp_keys,
														average_accuracy,
														valid_imgs = valid_imgs,
														iteration = -1,
														acc_measure = acc_measure)

	int_data, _ = experiment_average_measure(experiments, exp_keys,
												average_interaction,
												valid_imgs = valid_imgs)

	cont_data, _ = experiment_average_measure(experiments, exp_keys,
												average_control,
												valid_imgs = valid_imgs)

	path_error, _ = experiment_average_measure(experiments, exp_keys,
												average_path_data,
												valid_imgs = valid_imgs,
												path_data = 'path_error')

	return acc_data, int_data, cont_data, path_error, variable_opt


# Computes, for every parameter configuration in exp_keys,
# the average accuracy per iteration.
def experiment_avg_accuracy_per_iteration(acc_measure, experiments, exp_keys,
												niterations = -1, valid_imgs = None):
	avg = {}
	stdev = {}
	err = {}

	variable_opt = 'err_thresh'

	for key in sorted(exp_keys,key=lambda x: map(float, x.split('_'))):
		# Computing the maximum number of iterations required among
		# all the valid images in the experiment.
		max_niter = 0
		for img_name in experiments[key].accuracy_measures:
			if valid_imgs is None or img_name in valid_imgs:
				max_niter = max(max_niter, len(experiments[key].accuracy_measures[img_name]))

		if niterations > 0:
			max_niter = min(max_niter, niterations)

		avg[key] = np.zeros(max_niter,np.float)
		stdev[key] = np.zeros(max_niter,np.float)

		for iteration in xrange(max_niter):
			# Since we want the accuracy per iteration per parameter configuration,
			# we pass the key in a list to the function that computes the average
			# accuracy for a given iteration
			(a, std, e), variable_opt = experiment_average_measure(experiments, [key],
																	average_accuracy,
																	valid_imgs = valid_imgs,
																	iteration = iteration,
																	acc_measure = acc_measure)
			avg[key][iteration] = a[0]
			stdev[key][iteration] = std[0]
			err[key] = e[0]

	return (avg, stdev, err), variable_opt

#Computing graphs per iteration
def compute_statistics_per_iteration(acc_measure, experiments, sel_key, niterations,
										valid_imgs = None):
	exp_keys = set()
	for x in experiments:
		if x.find(sel_key) >= 0:
			exp_keys.add(x)

	acc_data, variable_opt = experiment_avg_accuracy_per_iteration(acc_measure, experiments, exp_keys,
																	niterations,
																	valid_imgs = valid_imgs)
	return acc_data, variable_opt

def show_graphs(acc_data, data, show_stdev = True, ax = None, ax1 = None, legend = None,
					valid_imgs = None, ax_title = None, ax1_title = None,	acc_limits = None,
					**lineconfig):

	avg_acc, stdev_acc, x_axis = acc_data

	if not show_stdev:
		stdev_acc = None

	do_show = ax is None and ax1 is None

	if ax is None:
		fig = pl.figure()
		ax = fig.add_subplot(111)

	if legend is not None:
		ax.legend(legend).draggable(state = True)


	ax.errorbar(x_axis, avg_acc, yerr = stdev_acc, **lineconfig)

	if acc_limits is not None:
		ax.set_ylim(acc_limits)

	if ax_title is not None:
		ax.set_title(ax_title)

	if legend is not None:
		ax.legend(legend).draggable(state = True)

	if data is not None:
		avg, stdev, x_axis = data

		if not show_stdev:
			stdev = None

		if ax1 is None:
			fig1 = pl.figure()
			ax1 = fig1.add_subplot(111)

		ax1.errorbar(x_axis, avg, yerr = stdev, **lineconfig)

		if ax1_title is not None:
			ax1.set_title(ax1_title)

		if legend is not None:
			ax1.legend(legend).draggable(state = True)

	if do_show:
		pl.show()


# Filtering all images excluding those that were not segment by
# at least one of the methods for at least one of the parameter
# configurations.
def valid_images(simulation_data, sel_key):

	invalid_imgs = set()
	valid_imgs = None
	for method in simulation_data:
		for exp in simulation_data[method]:
			exp_imgs = set()
			if exp.find(sel_key) >= 0:
				for img in simulation_data[method][exp].accuracy_measures:
					exp_imgs.add(img)

				invalid_imgs |= simulation_data[method][exp].unsuccessful_experiments

				# Getting only the images the are common to all groups (i.e., that
				# are "intersected" by all experiments and methods)
				if valid_imgs is None:
					valid_imgs = exp_imgs
				else:
					valid_imgs &= exp_imgs

	for img,_ in invalid_imgs:
		if img in valid_imgs:
			valid_imgs.remove(img)


	return valid_imgs,invalid_imgs

def data_to_txt(acc_measure, simulation_data, sel_key, control_or_interactions, iteration):
	acc_file = open('accuracy.txt','w')
	effort_file = open('user_effort.txt','w')

	acc_file.write(acc_measure + '\n')
	acc_file.write(sel_key + '\n')

	effort_file.write(control_or_interactions + '\n')
	effort_file.write(sel_key + '\n')

	valid_imgs, invalid_imgs = valid_images(simulation_data, sel_key)

	method_legend = dict(method_nicknames)

	for i,method in zip(xrange(len(simulation_data)),sorted(simulation_data)):
		acc_file.write(method_legend[method])
		effort_file.write(method_legend[method])

		if i < len(simulation_data) - 1:
			acc_file.write(' ')
			effort_file.write(' ')
		else:
			acc_file.write('\n')
			effort_file.write('\n')

	for img in sorted(valid_imgs):
		# Saving the image's file name for reference
		acc_file.write('%s ' % img)
		effort_file.write('%s ' % img)
		for i,method in zip(xrange(len(simulation_data)),sorted(simulation_data)):

			if len(simulation_data[method][sel_key].accuracy_measures[img]) > 0:
				# Negative iteration means that the last one (largest)
				# iteration/key should be used.
				if iteration < 0:
					sel_iteration = max(simulation_data[method][sel_key].accuracy_measures[img])
				else:
					# Selecting the last iteration in case the method
					# took less iterations than the required iteration.
					sel_iteration = min(iteration,max(simulation_data[method][sel_key].accuracy_measures[img]))


				value = simulation_data[method][sel_key].accuracy_measures[img][sel_iteration][acc_measure]

				if i < len(simulation_data) - 1:
					acc_file.write('%s ' % str(value))
				else:
					acc_file.write('%s\n' % str(value))

				if control_or_interactions == 'control':
					value = simulation_data[method][sel_key].control_measures[img][0]
				else:
					value = simulation_data[method][sel_key].interactions[img]

				if i < len(simulation_data) - 1:
					effort_file.write('%s ' % str(value))
				else:
					effort_file.write('%s\n' % str(value))

	effort_file.close()
	acc_file.close()

def compare_methods(acc_measure, simulation_data, sel_key,
						control_or_interactions, colormap, show_stdev,
						acc_limits):
	fig = pl.figure()
	ax = fig.add_subplot(111)
	fig1 = pl.figure()
	ax1 = fig1.add_subplot(111)

	legend = []

	valid_imgs, invalid_imgs = valid_images(simulation_data, sel_key)
	print 'Invalid images:', invalid_imgs

	method_legend = dict(method_nicknames)

	lw = 3.0
	ms = 7.0
	marker_fmt = ('-o','-s','-^', '-p','-v','-D','-d','-H')

	variable_opt = None
	for l,fmt,method in zip(xrange(len(simulation_data)),marker_fmt,simulation_data):
		(acc_data, int_data, cont_data,
		path_error, new_variable_opt) = compute_statistics(acc_measure,	simulation_data[method],
														sel_key, valid_imgs = valid_imgs)
		# Linked experiments will return None.
		if new_variable_opt is not None:
			variable_opt = new_variable_opt
			variable_opt = variable_opt.capitalize()

		data = int_data

		if len(acc_data) > 0 and method not in legend:
			legend.append(method_legend[method])

		for i in xrange(len(acc_data[0])):
			# Latex
			print '\n & %s & Num. of Interactions & Control\\\\' % acc_measure.capitalize()
			print '\n%s & $%0.2f \pm %0.2f$ & $%.0f \pm %.0f$ & $%.2f \pm %.2f$\\\\' % (str(method).capitalize(),
																	acc_data[0][i],acc_data[1][i],
																	int_data[0][i],int_data[1][i],
																	cont_data[0][i],cont_data[1][i])


		if control_or_interactions == 'control':
			data = cont_data

		ax_title = '%s x %s' % (variable_opt, acc_measure.capitalize())
		ax1_title = 'N. Interactions x %s' % variable_opt
		if variable_opt == 'Err_thresh':
			ax_title = '%s x Error threshold'  % (acc_measure.capitalize())
			ax1_title = 'N. Interactions x Error threshold'

		show_graphs(acc_data, data, show_stdev = show_stdev,
					ax=ax, ax1=ax1, valid_imgs = valid_imgs,
					ax_title = ax_title,
					ax1_title = ax1_title,
					color = triplet_to_mp_color(colormap[l]),
					linewidth = lw, fmt = fmt,
					acc_limits = acc_limits)

	ax.legend(legend).draggable(state = True)
	ax1.legend(legend).draggable(state = True)

	pl.show()

def compare_methods_by_iteration(acc_measure, simulation_data, sel_key,
									niterations, colormap, show_stdev,
									acc_limits, save_fig):
	legend = []

	valid_imgs, invalid_imgs = valid_images(simulation_data, sel_key)

	print 'Invalid images:', invalid_imgs

	acc_data = {}

	# Precomputing graph data for all methods

	variable_opt = {}

	method_legend = dict(method_nicknames)

	for method,_ in method_nicknames:
		if method in simulation_data:
			print '\nMethod: ', method
			for key in simulation_data[method]:
				if key.find(sel_key) >= 0:
					print 'Simulation Config: ', simulation_data[method][key].simulation_config
					print '\nMethodConfig: ', simulation_data[method][key].method_config

			print '\n'

			acc_data[method], variable_opt[method] = compute_statistics_per_iteration(acc_measure,
																				simulation_data[method],
																				sel_key,
																				niterations,
																				valid_imgs = valid_imgs)
			if len(acc_data[method][0]) > 0 and method not in legend:
				legend.append(method_legend[method])


	method = simulation_data.keys()[0]
	# We display at most 3 axes per row
	max_ncols = 3
	# Determining the number of subplots necessary for displaying
	# all the graphs
	nrows = int(ceil(len(acc_data[method][0])/float(max_ncols)))
	ncols = max_ncols

	if len(acc_data[method][0]) < max_ncols:
		ncols = len(acc_data[method][0])

	axes_data = {}
	if save_fig is None:
		fig, axes = plt.subplots(nrows=nrows, ncols=ncols)
		# If a single axis was created then we transform 'axes' into a
		# unary tuple to make sure it will be iterated to display
		# a figure with a single graph
		try:
			iter(axes)
		except:
			axes = (axes,)

		# Adding the subplots necessary for displaying
		# the data to a dict so they can be easily referred to
		# in the future.
		i = 0
		for row in axes:
			if nrows > 1:
				for ax in row:
					if i < len(acc_data[method][0]):
						axes_data[i] = ax
					i = i + 1
			else:
				if i < len(acc_data[method][0]):
					# row is now an axis object since
					# a single row was needed
					axes_data[i] = row
				i = i + 1
	else:
		# Creating multiple figures to save the data
		fig = {}

		for i in xrange(len(acc_data[method][0])):
			fig[i] = pl.figure()
			axes_data[i] = fig[i].add_subplot(111)

	lw = 3.0
	ms = 7.0
	marker_fmt = ('-o','-s','-^', '-p','-v','-D','-d','-H')

	l = 0
	for method,_ in method_nicknames:
		if method in simulation_data:
			fmt = marker_fmt[l]
			# Plotting the data for each method
			for i,key in zip(xrange(len(acc_data[method][0])),
							sorted(acc_data[method][0], key=lambda x: map(float, x.split('_')))):

				# Computing a triple containing the average accuracy, standard
				# deviation and array with the number of iterations performed
				# by the robot user.
				data = (acc_data[method][0][key], # accuracy
						acc_data[method][1][key], # stdev
						range(1,len(acc_data[method][0][key])+1)) # iterations

				show_graphs(data, None, show_stdev = show_stdev,
							ax=axes_data[i], ax1=None,
							valid_imgs = valid_imgs,
							color = triplet_to_mp_color(colormap[l]),
							linewidth = lw, fmt = fmt, ms = ms,
							acc_limits = acc_limits)

				if save_fig is None:
					var_opt = variable_opt[method]
					if var_opt is not None:
						axes_data[i].set_title('%s: %f' % (var_opt, acc_data[method][2][key]))
				else:
					axes_data[i].set_title('%s x Iterations' % (acc_measure.capitalize()))

				#positioning legend on the lower right corner
				axes_data[i].legend(legend, loc=4).draggable(state = True)

				i = i + 1
		# Forcing the color label and marker format to
		# follow the sequence in method_nicknames always,
		# regardless of wether method is in simulation_data or not.
		l = l + 1

		if save_fig is None:
			plt.tight_layout()

	if save_fig is not None:
		for i in xrange(len(acc_data[method][0])):
			fig[i].savefig('experiment_%02d.%s' % (i,save_fig),bbox_inches='tight',pad_inches=0)
			fig[i].clf()

	else:
		pl.show()


def opt_menu(keys, msg, callable_fun = None, sort = False,
				extra_opt = None):
	key_pair = {}
	final_keys = keys if not sort else sorted(keys)

	for i,x in zip(xrange(len(final_keys)),final_keys):
		if callable_fun is not None:
			print '%d) %s' % (i,str(callable_fun(x)))
		else:
			print '%d) %s' % (i,str(x))

		key_pair[i] = x

	if extra_opt is not None:
		print '%d) %s' % (len(final_keys), extra_opt)
		key_pair[len(final_keys)] = extra_opt

	opt = raw_input(msg)

	sel_key = key_pair[int(opt)]

	return sel_key


def choose_experiment(simulation_data, methods,
						common_keys_only = True,
						msg = 'Choose experiment: '):
	# Selecting only the experiment keys common to all methods
	common_keys = set()

	unique_suffix = {}
	keys = {}
	for method in methods:
		experiments = simulation_data[method]

		unique_keys = set()

		for x in experiments:
			prefix = x.split('_')[0]
			unique_keys.add(prefix)
			if prefix not in keys:
				keys[prefix] = set([x])
			else:
				keys[prefix].add(x)

		if len(common_keys) == 0:
			common_keys = unique_keys
		else:
			if common_keys_only:
				common_keys &= unique_keys
			else:
				common_keys |= unique_keys

		unique_suffix.update(dict([(x.split('_')[0],experiments[x].simulation_config['suffix'])
								for x in experiments]))

	try:
		exp_key = opt_menu(common_keys, msg,
							lambda x: time.ctime(float(x))
							+ ' - ' + str(unique_suffix[x]),
							sort = True)
	except:
		return None, None
	else:

		return exp_key, keys[exp_key]


def show_failed_experiments(experiments, sel_key):
	exp_keys = set()
	for x in experiments:
		if x.find(sel_key) >= 0:
			exp_keys.add(x)

	for key in exp_keys:
		nfailed = len(experiments[key].unsuccessful_experiments)
		exp_key_and_id = key.split('_')
		print 'Experiment %s %s' % (time.ctime(float(exp_key_and_id[0])), exp_key_and_id[1])

		if nfailed <= 0:
			print '-->No experiment failed.\n'
		else:
			for i,img in zip(xrange(nfailed),
								experiments[key].unsuccessful_experiments):
				print '%d) %s' % (i, img)

			print '\n'

def reduce_file_size(simulation_data, methods, interactive):
	for method in methods:
		for exp_key in simulation_data[method]:
			sim_data = simulation_data[method][exp_key]

			if hasattr(sim_data,'seeds'):
				for img in sim_data.seeds:
					for it in sim_data.seeds[img]:
						sim_data.seeds[img][it] = None
			if hasattr(sim_data,'path_seeds'):
				for img in sim_data.path_seeds:
					for it in sim_data.path_seeds[img]:
						sim_data.path_seeds[img][it] = None

			for img in sim_data.labels:
				for it in sim_data.labels[img]:
					if type(sim_data.labels[img][it]) != str:
						sim_data.labels[img][it] = None

		if interactive:
			filename = 'simulation_data_interactive_%s.pkl' % method
		else:
			filename = 'simulation_data_%s.pkl' % method

		save_experiments(filename, simulation_data[method], False, False)

def anchor_cloud(simulation_data, method, exp_key):
	print simulation_data[method][exp_key].simulation_config
	gt_dir = simulation_data[method][exp_key].simulation_config['gt_name']
	home = expanduser('~')
	if gt_dir.find(home) < 0:
		if gt_dir.find('tvspina') >= 0:
			gt_dir = gt_dir.replace('tvspina','spyder')
		elif gt_dir.find('spyder') >= 0:
			gt_dir = gt_dir.replace('spyder','tvspina')


	vec_anchors = []
	max_w = 0
	max_h = 0

	for img_name in simulation_data[method][exp_key].anchors:
		filename = join(gt_dir,split(img_name)[1].replace('.ppm','.pgm'))

		img = Image8(filename = filename)

		w_orig,h_orig = img.size()

		w, h = w_orig, h_orig
		if simulation_data[method][exp_key].simulation_config['add_frame']:
			w = w + 10
			h = h + 10

		max_w = max(max_w,w)
		max_h = max(max_h,h)

		px_centroid = mask_centroid(img, -1)
		px_centroid = px_centroid + (w - w_orig)/2

		for anchor in simulation_data[method][exp_key].anchors[img_name]:
			px_anchor = (anchor % w, anchor / w)
			vec_anchors.append(px_anchor - px_centroid)

	img = Image32(size = (max_w, max_h))

	px_centroid = np.array((max_w/2,max_h/2),np.int32)
	for anchor in vec_anchors:
		anchor = img.toindex(anchor + px_centroid)

		img[anchor] += 1

	img.write('anchor_cloud.png')

def methods_by_key(simulation_data, sel_key):
	method_experiments = {}
	for method in simulation_data:
		for exp_key in simulation_data[method]:
			if exp_key.find(sel_key) >= 0:
				if method not in method_experiments:
					method_experiments[method] = set()

				method_experiments[method].add(exp_key)

	return method_experiments

# Asks the user which experiments should be linked for comparison.
# This is useful in case we want to compare experiments done in
# different times, for example, but having the same charactestics.
def add_links(simulation_data, methods):

	try:
		exp_key_from, _ = choose_experiment(simulation_data, methods, common_keys_only = False,
									msg = 'Choose experiment to link from: ')
	except:
		raise
	else:
		method_experiments = methods_by_key(simulation_data, exp_key_from)

		all_methods = set(method_experiments.keys())
		chosen_methods = raw_input('Choose methods to link %s: ' % str(all_methods))

		chosen_methods = set(chosen_methods.split())

		for m in chosen_methods:
			if m not in all_methods:
				raise RuntimeException('Method %s not in %s',m, all_methods)

		try:
			exp_key_link, keys_link = choose_experiment(simulation_data, all_methods, common_keys_only = False,
														msg = 'Choose experiment to link to: ')
		except:
			raise RuntimeException('Experiment key %s invalid', exp_key_link)
		else:

			for method in all_methods:
				if method not in chosen_methods:
					del method_experiments[method]

			print method_experiments
			print exp_key_link
			print 'Exp key link: ' + time.ctime(float(exp_key_link))

			link_experiments(simulation_data, method_experiments, exp_key_link, keys_link)


# Links the experiments chosen by the user for each method.
def link_experiments(simulation_data, method_experiments, exp_key_link, keys_link):
	for method in method_experiments:
		exp_keys = method_experiments[method]
		nkeys = len(method_experiments[method])
		print nkeys,method_experiments,len(keys_link)
		# If there is a single experiment key to be linked from,
		# while there are more experiment keys to be linked to, then
		# we link the single experiment from to all others
		if (len(keys_link) > nkeys and nkeys == 1):
			# Getting the first and only key
			exp_key = method_experiments[method].pop()
			method_experiments[method].add(exp_key)

			for exp_key_link_final in keys_link:
				if exp_key_link_final not in simulation_data[method]:
					simulation_data[method][exp_key_link_final] = simulation_data[method][exp_key]
					setattr(simulation_data[method][exp_key_link_final],'link_orig',exp_key)
					print 'Adding link to %s' % time.ctime(float(exp_key_link_final.split('_')[0]))
				else:
					print 'Experiment %s %s already exists for %s, arr!' % (time.ctime(float(exp_key_link_final.split('_')[0])),
																			exp_key_link_final.split('_')[1], method)
		else:
			for exp_key in method_experiments[method]:
				exp_key_link_final = str(exp_key_link) + '_' + exp_key.split('_')[1]
				if exp_key_link_final not in simulation_data[method]:
					simulation_data[method][exp_key_link_final] = simulation_data[method][exp_key]
					setattr(simulation_data[method][exp_key_link_final],'link_orig',exp_key)
					print 'Adding link to %s' % time.ctime(float(exp_key_link_final.split('_')[0]))
				else:
					print 'Experiment %s %s already exists for %s, arr!' % (time.ctime(float(exp_key_link_final.split('_')[0])),
																			exp_key_link_final.split('_')[1], method)

# Removes all links. This must be done when comparison is finished or
# before saving the files once more.
def remove_links(simulation_data):
	method_experiments = {}
	for method in simulation_data:
		method_experiments[method] = set()
		for exp_key in simulation_data[method]:
			if hasattr(simulation_data[method][exp_key],'link_orig'):
				if simulation_data[method][exp_key].link_orig != exp_key:
					method_experiments[method].add(exp_key)

	for method in method_experiments:
		for exp_key in method_experiments[method]:
			print 'Removing link %s' % time.ctime(float(exp_key.split('_')[0]))
			delattr(simulation_data[method][exp_key], 'link_orig')
			del simulation_data[method][exp_key]

def main():
	parser = ap.ArgumentParser(formatter_class=ap.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--methods',type=str, nargs='+', required = True, help='Overrides the methods selected in the configuration file for all datasets')
	parser.add_argument('--show_stdev',action='store_true',default=False, help='Shows the standard deviation on the graphs')
	parser.add_argument('--interactive',action='store_true',default=False, help='Shows the results of interactive experiments')
	parser.add_argument('--save_fig',type=str,default=None, help='Saves the figures in .eps format instead of displaying them')
	parser.add_argument('--niterations',type=int,default=-1, help='Number of iterations to be displayed')
	parser.add_argument('--acc_limits',type=float,nargs=2,default=[0.0, 1.0], help='Sets the accuracy limits for the graphs')

	args = vars(parser.parse_args())
	methods = args['methods']
	show_stdev = args['show_stdev']
	interactive = args['interactive']
	niterations = args['niterations']
	acc_limits = args['acc_limits']
	save_fig = args['save_fig']

	simulation_data = load_simulation_data(methods, interactive)

	# Setting matplotlib's EPS generation method to produce
	# vectorized graphs.
	mpl.rcParams['ps.usedistiller'] = 'xpdf'

	what_to_do = ''
	colormap = initialize_colormap(True)
	colormap[1] = triplet(95,95,95)

	while what_to_do != 'Exit':
		try:
#			if len(methods) == 1:
#				what_to_do = opt_menu(['Remove experiment', 'Show failed experiments',
#										'Show graphs', 'Raw data to txt',
#										'Anchor cloud', 'Merge experiments',
#										'Remove labels and seeds','Exit'],
#										'Select function: ')
#			else:
#				what_to_do = opt_menu(['Compare methods','Compare methods by iteration',
#										'Link experiments', 'Raw data to txt','Merge experiments',
#										'Remove experiment','Remove labels and seeds',
#										'Exit'], 'Select function: ')
			if len(methods) == 1:
				what_to_do = opt_menu(['Show graphs', 'Raw data to txt','Exit'],
										'Select function: ')
			else:
				what_to_do = opt_menu(['Compare methods by overall measures','Compare methods by iteration',
										'Raw data to txt','Exit'], 'Select function: ')
		except:
			continue

		if what_to_do == 'Remove experiment':
			img_name = None
			try:
				if len(methods) > 1:
					exp_key, keys = choose_experiment(simulation_data, methods,
													common_keys_only = True)
					if exp_key == None:
						continue
				else:
					exp_key = opt_menu(simulation_data[methods[0]], 'Choose experiment: ',
									lambda x: '%s %s %s' % (time.ctime(float(x.split('_')[0])),x.split('_')[1],
														simulation_data[methods[0]][x].simulation_config['suffix']),
									sort = True)

					for img in sorted(simulation_data[methods[0]][exp_key].accuracy):
						max_iter = max(simulation_data[methods[0]][exp_key].accuracy[img])
						print img, 'Accuracy: ', simulation_data[methods[0]][exp_key].accuracy[img][max_iter]

					img_name = opt_menu(simulation_data[methods[0]][exp_key].accuracy_measures, 'Choose image: ',
										sort = True, extra_opt = 'All')
			except:
				print '-->No experiment removed<--'
				continue

			if img_name is not None:
				if img_name != 'All':
					simulation_data[methods[0]][exp_key].remove_experiment(img_name)

				if img_name == 'All' or len(simulation_data[methods[0]][exp_key].accuracy_measures) == 0:
					del simulation_data[methods[0]][exp_key]

				save_experiments(filename, simulation_data[methods[0]], False, False)
			else:
				for method in methods:
					for key in keys:
						del simulation_data[method][key]

				for method in methods:
					save_experiments(filename, simulation_data[method], False, False)

			assemble_experiment_files('.', methods, interactive)

		elif what_to_do == 'Show failed experiments':
			print simulation_data[methods[0]]
			for i in simulation_data[methods[0]]:
				if i is not None:
					print i
			unique_keys = set([x.split('_')[0] for x in simulation_data[methods[0]]])

			try:
				exp_key = opt_menu(unique_keys, 'Choose experiment: ',
								lambda x: time.ctime(float(x)),
								sort = True)
			except:
				continue

			show_failed_experiments(simulation_data[methods[0]], exp_key)

		elif what_to_do == 'Show graphs':
			unique_keys = set([x.split('_')[0] for x in simulation_data[methods[0]]])

			try:
				exp_key = opt_menu(unique_keys, 'Choose experiment: ',
								lambda x: time.ctime(float(x)),
								sort = True)
			except:
				continue

			print time.ctime(float(exp_key))

			try:
				acc_measure = opt_menu(SimulationData.available_acc_measures(),
									'Choose accuracy measure: ', sort = True)
			except:
				continue

			try:
				sec_graph_data = opt_menu(['control', 'interactions'],
									'Control or interactions: ', sort = True)
			except:
				continue

			compare_methods(acc_measure, simulation_data, exp_key,
							sec_graph_data, colormap, show_stdev,
							acc_limits = acc_limits)

		elif what_to_do == 'Compare methods by overall measures':
			exp_key, _ = choose_experiment(simulation_data, methods,
											common_keys_only = False)

			if exp_key == None:
				continue

			try:
				acc_measure = opt_menu(SimulationData.available_acc_measures(),
										'Choose accuracy measure: ', sort = True)
			except:
				continue

			try:
				sec_graph_data = opt_menu(['control', 'interactions'],
									'Control or interactions: ', sort = True)
			except:
				continue

			compare_methods(acc_measure, simulation_data, exp_key,
							sec_graph_data, colormap, show_stdev,
							acc_limits = acc_limits)

		elif what_to_do == 'Compare methods by iteration':
			exp_key, _ = choose_experiment(simulation_data, methods,
										common_keys_only = False)

			if exp_key == None:
				continue

			try:
				acc_measure = opt_menu(SimulationData.available_acc_measures(),
										'Choose accuracy measure: ', sort = True)
			except:
				continue

			compare_methods_by_iteration(acc_measure, simulation_data, exp_key,
											niterations, colormap = colormap,
											show_stdev = show_stdev,
											acc_limits = acc_limits,
											save_fig = save_fig)

		elif what_to_do == 'Raw data to txt':
			try:
				exp_key = opt_menu(simulation_data[methods[0]], 'Choose experiment: ',
								lambda x: '%s %s %s' % (time.ctime(float(x.split('_')[0])),x.split('_')[1],
														simulation_data[methods[0]][x].simulation_config['suffix']),
								sort = True)
				if exp_key == None:
					continue
				acc_measure = opt_menu(SimulationData.available_acc_measures(),
										'Choose accuracy measure: ', sort = True)
				sec_graph_data = opt_menu(['control', 'interactions'],
									'Control or interactions: ', sort = True)

			except:
				continue

			data_to_txt(acc_measure, simulation_data, exp_key, sec_graph_data, -1)

		elif what_to_do == 'Anchor cloud':

			try:
				exp_key = opt_menu(simulation_data[methods[0]], 'Choose experiment: ',
								lambda x: str(time.ctime(float(x.split('_')[0])))+' ' +x.split('_')[1],
								sort = True)
			except:
				continue

			anchor_cloud(simulation_data, methods[0], exp_key)

		elif what_to_do == 'Merge experiments':
#			remove_links(simulation_data)
			assemble_experiment_files('.', methods, interactive)
		elif what_to_do == 'Remove labels and seeds':
			remove_links(simulation_data)
			reduce_file_size(simulation_data, methods, interactive)
		elif what_to_do == 'Link experiments':
			try:
				add_links(simulation_data, methods)
			except:
				continue

if __name__ == "__main__":
	main()
